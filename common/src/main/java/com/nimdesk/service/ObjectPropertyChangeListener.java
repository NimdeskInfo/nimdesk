/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.service;

import java.util.EventListener;

public interface ObjectPropertyChangeListener extends EventListener {

    /**
     * Called when the object is newly created.
     * @param event A PropertyChangeEvent object describing the event source.
     */
    void newObject(ObjectPropertyChangeEvent event);

    /**
     * Called when the object is being deleted.
     * @param event A PropertyChangeEvent object describing the event source.
     */
    void deleteObject(ObjectPropertyChangeEvent event);

    /**
     * Called when a bound property is changes.
     * @param event A PropertyChangeEvent object describing the event source
     *      and the property that has changed.
     */
    void propertyChange(ObjectPropertyChangeEvent event);
}
