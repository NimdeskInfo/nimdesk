/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.service;

import java.util.List;

import com.nimdesk.util.Pagination;

public interface PersistenceService<OBJECT_TYPE> {
    Long count();

    OBJECT_TYPE newInstance();

    List<? extends OBJECT_TYPE> getAll(Pagination pagination);

    OBJECT_TYPE getByUuid(String uuid);

    void commit(OBJECT_TYPE obj);

    boolean delete(OBJECT_TYPE obj);
}

