/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.service;

import com.nimdesk.event.AsyncCallback;

/**
 * A generic service to provide run-late service to all requests.
 * This can help improve system responsiveness by not blocking key threads.
 */
public interface AsyncExecutor {
    public static final String BEAN_NAME = "asyncExecutor";

    public interface Job {
        Object run();
    }

    public void run(Job job, final AsyncCallback callback);
}

