/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.service;

import java.util.ArrayList;

import com.nimdesk.event.AsyncCallback;
import com.nimdesk.util.ActionDelegate;

public class Workflow {
    public interface StateRunnable {
        void run(AsyncCallback callback);
        void setResult(Object result);
    }

    private final ArrayList<State> _states;
    private AsyncCallback _finalCallback = null;

    private int _currentStateIndex = 0;
    private Object _lastResult = null;

    public Workflow() {
        _states = new ArrayList<State>();
    }

    public Workflow(StateRunnable runnable, AsyncCallback callback) {
        this();
        append(runnable, callback);
    }

    public Workflow append(StateRunnable runnable, AsyncCallback callback) {
        _states.add(new State(runnable, callback));
        return this;
    }

    public Workflow setCallback(AsyncCallback callback) {
        _finalCallback = callback;
        return this;
    }

    public String getRequestor() {
        return (_finalCallback != null) ? _finalCallback.getRequestor() : null;
    }

    public void run() {
        runCurrentState();
    }

    private void runCurrentState() {
        State state = getCurrentState();
        if (state == null) {
            if (_finalCallback != null) {
                _finalCallback.onSuccess(_lastResult);
            }
            return;
        }

        if (state.runnable == null) {
            if (_finalCallback != null) {
                _finalCallback.onFailure(new Exception("Undefined runnable for state"));
            }
            return;
        }

        try {
            state.runnable.run(new AsyncCallbackWrapper(state));
        } catch (Throwable t) {
            if (_finalCallback != null) {
                _finalCallback.onFailure(t);
            }
        }
    }

    private synchronized State getCurrentState() {
        if (_currentStateIndex >= _states.size()) {
            return null;
        }

        State state = _states.get(_currentStateIndex);
        ++_currentStateIndex;

        return state;
    }

    private class State {
        final StateRunnable runnable;
        final AsyncCallback callback;

        State(StateRunnable runnable, AsyncCallback callback) {
            this.runnable = runnable;
            this.callback = callback;
        }
    }

    private class AsyncCallbackWrapper implements AsyncCallback {
        private final State _state;

        AsyncCallbackWrapper(State state) {
            _state = state;
        }

        @Override
        public void onSuccess(Object result) {
            _lastResult = result;

            if (_state != null) {
                if (_state.runnable != null) {
                    _state.runnable.setResult(result);
                }
                if (_state.callback != null) {
                    _state.callback.onSuccess(result);
                }
            }

            runCurrentState();
        }

        @Override
        public void onFailure(Throwable t) {
            if (_state != null && _state.callback != null) {
                _state.callback.onFailure(t);
            }

            // Run final callback too
            if (_finalCallback != null) {
                _finalCallback.onFailure(t);
            }
        }

        @Override
        public void progress(int percentage) {
            if (_state != null && _state.callback != null) {
                _state.callback.progress(percentage);
            }

            // Run final callback too
            if (_finalCallback != null) {
                _finalCallback.progress(percentage);
            }
        }

        @Override
        public String getRequestor() {
            return Workflow.this.getRequestor();
        }
    }

    public static AsyncCallback wrapCallbackWithProgressUpdater(final AsyncCallback callback,
            final ActionDelegate<Integer> progressUpdater) {
        if (progressUpdater == null) {
            return callback;
        }

        return new AsyncCallback() {
            @Override
            public void onSuccess(Object result) {
                if (callback != null) {
                    callback.onSuccess(result);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                if (callback != null) {
                    callback.onFailure(t);
                }
            }

            @Override
            public void progress(int percentage) {
                progressUpdater.action(percentage);

                if (callback != null) {
                    callback.progress(percentage);
                }
            }

            @Override
            public String getRequestor() {
                return (callback != null) ? callback.getRequestor() : null;
            }
        };
    }
}

