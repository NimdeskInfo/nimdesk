/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.ldap;

public class LdapHelper {
    public static String composeUrl(String server, int port, boolean useSsl) {
        StringBuilder sb = new StringBuilder();

        if (useSsl) {
            sb.append("ldaps://");
        } else {
            sb.append("ldap://");
        }

        sb.append(server);

        if (port > 0) {
            sb.append(":").append(port);
        }

        return sb.toString();
    }

    public static String convertDomain(String domain) {
        if (domain.toLowerCase().startsWith("dc=")) {
            return domain;
        }

        StringBuilder sb = new StringBuilder();

        String[] parts = domain.split("\\.");
        for (String part : parts) {
            if (sb.length() > 0) {
                sb.append(",");
            }
            sb.append("dc=").append(part);
        }

        return sb.toString();
    }
}
