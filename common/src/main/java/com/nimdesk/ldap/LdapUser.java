/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.ldap;

import java.io.Serializable;

public class LdapUser implements Serializable {

    private static final long serialVersionUID = 1L;

    private String _name;
    private String _domain;
    private boolean _isUser;
    private String _displayName;
    private String _logonName = "";
    private String _email = "";
    private String _description;
    private String _distinguishedName;

    public String getName() {
        return _name;
    }

    public void setName(String name) {
        _name = name;
    }

    public String getDomain() {
        return _domain;
    }

    public void setDomain(String domain) {
        _domain = domain;
    }

    public boolean isUser() {
        return _isUser;
    }

    public void setUser(boolean isUser) {
        _isUser = isUser;
    }

    public String getDisplayName() {
        return _displayName;
    }

    public void setDisplayName(String displayName) {
        _displayName = displayName;
    }

    public String getLogonName() {
        return _logonName;
    }

    public void setLogonName(String logonName) {
        _logonName = logonName;
    }

    public String getEmail() {
        return _email;
    }

    public void setEmail(String email) {
        _email = email;
    }

    public String getDescription() {
        return _description;
    }

    public void setDescription(String description) {
        _description = description;
    }

    public String getDistinguishedName() {
        return _distinguishedName;
    }

    public void setDistinguishedName(String distinguishedName) {
        _distinguishedName = distinguishedName;
    }
}
