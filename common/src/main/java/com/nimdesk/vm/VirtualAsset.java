/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.vm;

import java.io.Serializable;

public class VirtualAsset implements Serializable {

    private static final long serialVersionUID = 7695655556771024198L;

    private String _name;
    private String _refId;

    public String getName() {
        return _name;
    }

    public void setName(String name) {
        _name = name;
    }

    public String getRefId() {
        return _refId;
    }

    public void setRefId(String refId) {
        _refId = refId;
    }
}
