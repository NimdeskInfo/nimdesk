/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.vm;

import java.io.Serializable;
import java.util.List;

import com.nimdesk.model.Host;

public class ImportableVm implements Serializable {

    private static final long serialVersionUID = -2693248601973556012L;

    private Host _host;
    private String _vmRefId;
    private String _vmName;
    private String _osName;
    private long _diskCapacityInKB;
    private List<String> _reasons;

    public ImportableVm() {
    }

    public Host getHost() {
        return _host;
    }

    public void setHost(Host host) {
        _host = host;
    }

    public String getHostName() {
        return String.format("%s(%s)", _host.getAddress(), _host.getName());
    }

    public String getVmRefId() {
        return _vmRefId;
    }

    public void setVmRefId(String vmRefId) {
        _vmRefId = vmRefId;
    }

    public String getVmName() {
        return _vmName;
    }

    public void setVmName(String vmName) {
        _vmName = vmName;
    }

    public String getOsName() {
        return _osName;
    }

    public void setOsName(String osName) {
        _osName = osName;
    }

    public long getDiskCapacityInKB() {
        return _diskCapacityInKB;
    }

    public void setDiskCapacityInKB(long diskCapacityInKB) {
        _diskCapacityInKB = diskCapacityInKB;
    }

    public List<String> getReasons() {
        return _reasons;
    }

    public void setReasons(List<String> reasons) {
        _reasons = reasons;
    }

    public boolean isImportable() {
        return _reasons == null || _reasons.isEmpty();
    }
}

