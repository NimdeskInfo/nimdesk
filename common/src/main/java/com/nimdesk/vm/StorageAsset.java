/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.vm;

public class StorageAsset extends VirtualAsset {

    private static final long serialVersionUID = 4093452873297622726L;

    private static final long GB_FACTOR = 1024L * 1024L * 1024L;

    private long _capacity;
    private long _freeSpace;

    public StorageAsset() {
    }

    public long getCapacity() {
        return _capacity;
    }

    public void setCapacity(long capacity) {
        _capacity = capacity;
    }

    public long getFreeSpace() {
        return _freeSpace;
    }

    public void setFreeSpace(long freeSpace) {
        _freeSpace = freeSpace;
    }

    public String getDisplayName() {
        return String.format("%s (%,.2f GB free/%,.2f GB total)",
                getName(), ((float) getFreeSpace()) / GB_FACTOR, ((float) getCapacity()) / GB_FACTOR);
    }
}
