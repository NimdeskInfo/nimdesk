/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.commons.lang.StringUtils;
import org.springframework.util.ClassUtils;

public class RdpDescriptor {

    protected static final long REDIRECT_DRIVE = 0x01;
    protected static final long REDIRECT_PRINTER = 0x02;
    protected static final long REDIRECT_COMPORT = 0x04;
    protected static final long REDIRECT_SMARTCARD = 0x08;

    public RdpDescriptor() {
    }

    /*
     * screen mode id:i:2
     * session bpp:i:32
     * full address:s:192.168.1.10:3389
     * username:s:administrator
     * domain:s:somedomain
     * compression:i:1
     * keyboardhook:i:2
     * audiomode:i:0
     * autoreconnection enabled:i:0
     * authentication level:i:0
     * prompt for credentials:i:1
     * negotiate security layer:i:1
     * remoteapplicationmode:i:0
     * alternate shell:s:
     * shell working directory:s:
     * allow desktop composition:i:1
     * allow font smoothing:i:1
     * bitmapcachepersistenable:i:1
     * redirectprinters:i:0
     * redirectsmartcards:i:0
     * redirectcomports:i:0
     * redirectclipboard:i:0
     * redirectdrives:i:0
     * disable full window drag:i:0
     * disable menu anims:i:0
     * disable themes:i:0
     * disable wallpaper:i:0
     * span monitors:i:0
     * Use Multimon:i:0
     */
    public static String getRdpDescriptor(String server, int port,
            boolean multimon, boolean fullscreen, long width, long height, int colorBpp, long redirects,
            String username, String password, String domain, String gatewayAddr) {
        StringBuilder rdpSb = new StringBuilder();

        if (multimon) {
            writeln(rdpSb, "use multimon:i:1");
            writeln(rdpSb, "span monitors:i:0");
            writeln(rdpSb, "screen mode id:i:2");
        } else if (fullscreen) {
            writeln(rdpSb, "use multimon:i:1");
            // writeln(rdpSb, "use multimon:i:0");
            writeln(rdpSb, "span monitors:i:0");
            writeln(rdpSb, "screen mode id:i:2");
        } else {
            writeln(rdpSb, "screen mode id:i:1");
            if (width > 0 && height > 0) {
                writeln(rdpSb, String.format("desktopwidth:i:%d", width));
                writeln(rdpSb, String.format("desktopheight:i:%d", height));

                writeln(rdpSb, String.format("winposstr:s:0,1,%d,%d,%d,%d", 0, 0, width, height));
            }
        }
        writeln(rdpSb, String.format("session bpp:i:%d", colorBpp));

        if (port <= 0) {
            port = 3389;
        }

        writeln(rdpSb, String.format("full address:s:%s:%d", server, port));
        writeln(rdpSb, "connect to console:i:0");
        writeln(rdpSb, "compression:i:1");
        writeln(rdpSb, "keyboardhook:i:2");
        writeln(rdpSb, "audiomode:i:0");
        writeln(rdpSb, "audiocapturemode:i:1");
        writeln(rdpSb, "videoplaybackmode:i:1");
        writeln(rdpSb, "disable cursor setting:i:0");

        writeln(rdpSb, String.format("redirectdrives:i:%d", ((redirects & REDIRECT_DRIVE) != 0) ? 1 : 0));
        writeln(rdpSb, String.format("drivestoredirect:s:%s", ((redirects & REDIRECT_DRIVE) != 0) ? "*" : ""));
        writeln(rdpSb, String.format("devicestoredirect:s:%s", ((redirects & REDIRECT_DRIVE) != 0) ? "*" : ""));
        // writeln(rdpSb, String.format("usbdevicestoredirect:s:%s", ((_redirects & REDIRECT_DRIVE) != 0) ? "*" : ""));
        writeln(rdpSb, String.format("redirectprinters:i:%d", ((redirects & REDIRECT_PRINTER) != 0) ? 1 : 0));
        writeln(rdpSb, String.format("redirectcomports:i:%d", ((redirects & REDIRECT_COMPORT) != 0) ? 1 : 0));
        writeln(rdpSb, String.format("redirectsmartcards:i:%d", ((redirects & REDIRECT_SMARTCARD) != 0) ? 1 : 0));
        writeln(rdpSb, "redirectclipboard:i:1");
        writeln(rdpSb, "redirectdirectx:i:1");

        writeln(rdpSb, "displayconnectionbar:i:0");
        writeln(rdpSb, "pinconnectionbar:i:0");
        writeln(rdpSb, "autoreconnection enabled:i:0");
        // writeln(rdpSb, "enablecredsspsupport:i:0"); // Must disable RDP network level authentication (done by installer).
        writeln(rdpSb, "authentication level:i:0");

        writeln(rdpSb, String.format("username:s:%s", username));
        if (!StringUtils.isEmpty(domain)) {
            writeln(rdpSb, String.format("domain:s:%s", domain));
        }
        // writeln(rdpSb, String.format("password 51:b:%s", _hashedPassword));
        //writeln(rdpSb, "prompt for credentials:i:1");

        writeln(rdpSb, "networkautodetect:i:1");
        writeln(rdpSb, "bandwidthautodetect:i:1");

        if (!StringUtils.isEmpty(gatewayAddr)) {
            writeln(rdpSb, String.format("gatewayhostname:s:%s", gatewayAddr));
            writeln(rdpSb, "gatewayusagemethod:i:2");
            writeln(rdpSb, "gatewayprofileusagemethod:i:1");
        }

        return rdpSb.toString();
    }

    private static void writeln(StringBuilder sb, String str) {
        sb.append(str);
        sb.append("\n");
    }
}
