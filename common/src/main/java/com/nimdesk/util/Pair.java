/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.util;

public class Pair<T, U> {
    private T _t;
    private U _u;

    public Pair() {
        _t = null;
        _u = null;
    }

    public Pair(T t, U u) {
        _t = t;
        _u = u;
    }

    public T getFirst() {
        return _t;
    }

    public void setFirst(T t) {
        _t = t;
    }

    public U getSecond() {
        return _u;
    }

    public void setSecond(U u) {
        _u = u;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Pair<?, ?>)) {
            return false;
        }

        Pair<?, ?> that = (Pair<?, ?>) o;
        return (_t != null ? _t.equals(that._t) : that._t == null) &&
                (_u != null ? _u.equals(that._u) : that._u == null);
    }

    @Override
    public int hashCode() {
        return (_t != null ? _t.hashCode() : 0) * 13 + (_u != null ? _u.hashCode() : 0) * 7;
    }
}
