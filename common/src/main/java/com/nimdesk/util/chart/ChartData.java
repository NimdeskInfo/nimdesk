/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.util.chart;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class ChartData implements Serializable {

    private static final long serialVersionUID = 1L;

    private String _title;
    private String _yLabel;
    private int _base;
    private Double _min;
    private Double _max;
    private Date _startTime;
    private Date _endTime;
    private List<Date> _timestamps;
    private List<ChartLine> _lines;

    public ChartData() {
    }

    public String getTitle() {
        return _title;
    }

    public void setTitle(String title) {
        _title = title;
    }

    public String getYLabel() {
        return _yLabel;
    }

    public void setYLabel(String yLabel) {
        _yLabel = yLabel;
    }

    public int getBase() {
        return _base;
    }

    public void setBase(int base) {
        _base = base;
    }

    public Double getMin() {
        return _min;
    }

    public void setMin(Double min) {
        _min = min;
    }

    public Double getMax() {
        return _max;
    }

    public void setMax(Double max) {
        _max = max;
    }

    public Date getStartTime() {
        return _startTime;
    }

    public void setStartTime(Date startTime) {
        _startTime = startTime;
    }

    public Date getEndTime() {
        return _endTime;
    }

    public void setEndTime(Date endTime) {
        _endTime = endTime;
    }

    public List<Date> getTimestamps() {
        return _timestamps;
    }

    public void setTimestamps(List<Date> timestamps) {
        _timestamps = timestamps;
    }

    public List<ChartLine> getLines() {
        return _lines;
    }

    public void setLines(List<ChartLine> lines) {
        _lines = lines;
    }
}
