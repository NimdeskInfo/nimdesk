/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.util.chart;

import java.util.Date;
import java.util.List;

public class ChartUtils {
    public static String convertChartData(ChartData chartData) {
        if (chartData == null) {
            return "";
        }

        processLines(chartData.getLines());

        StringBuilder xmlBuilder = new StringBuilder();
        xmlBuilder.append("<chart");
        buildXMLHeader(xmlBuilder, chartData, null);
        xmlBuilder.append(">\n");

        buildXMLCategories(xmlBuilder, chartData.getTimestamps());

        for (ChartLine line : chartData.getLines()) {
            buildXMLDataset(xmlBuilder, line, Double.NaN, Double.NaN, chartData.getTimestamps(), chartData.getBase() == 1024);
        }

        xmlBuilder.append("<styles>")
                .append(" <definition>")
                .append("  <style name='myLegendFont' type='font' size='9' leftMargin='0' letterSpacing='0' />")
                .append("  <style name='myCapFont' type='font' size='10' bold='1' />")
                .append("  <style name='myLabelFont' type='font' size='8' bold='0' />")
                .append(" </definition>")
                .append(" <application>")
                .append("  <apply toObject='Legend' styles='myLegendFont' />")
                .append("  <apply toObject='Caption' styles='myCapFont' />")
                .append("  <apply toObject='DataLabels' styles='myLabelFont' />")
                .append(" </application>")
                .append("</styles>");

        xmlBuilder.append("</chart>");

        return xmlBuilder.toString();
    }

    private static void processLines(List<ChartLine> lines) {
    }

    @SuppressWarnings("deprecation")
    private static void buildXMLCategories(StringBuilder xmlBuilder, List<Date> timestamps) {
        xmlBuilder.append("<categories>");

        long step = Math.round(timestamps.size() / 7.0);
        if (step == 0) {
            step = timestamps.size();
        }

        for (int i = 0; i < timestamps.size(); i++) {
            Date timestamp = timestamps.get(i);

            if ((i != 0) && (i % step) == 0) {
                xmlBuilder.append("<category showLabel='1' ");
            } else {
                xmlBuilder.append("<category showLabel='0' ");
            }

            xmlBuilder.append("label='")
                    .append(timestamp.toLocaleString())
                    .append("' />");
        }

        xmlBuilder.append("</categories>\n");
    }

    private static void buildXMLHeader(StringBuilder xmlBuilder, ChartData chartData, Range range) {
        xmlBuilder.append(" animation='0'")
                .append(" anchorAlpha='0'") // Hide anchors but still enable tool tips.
                .append(" areaOverColumns='0'")
                .append(" canvasBorderThickness='1'") // Otherwise lines on border won't be shown.
                .append(" numVDivLines='")
                .append(chartData.getTimestamps().size())
                .append("'")
                .append(" showValues='0'")
                .append(" showToolTip='1'")
                .append(" yAxisName='")
                .append(chartData.getYLabel())
                .append("'");

        if (chartData.getBase() == 1024) {
            xmlBuilder.append(" numberScaleValue='")
                    .append(chartData.getBase())
                    .append(",")
                    .append(chartData.getBase())
                    .append(",")
                    .append(chartData.getBase())
                    .append("'")
                    .append(" numberScaleUnit='K,M,G'");
        }

        xmlBuilder.append(" caption='").append(chartData.getTitle()).append("'");
        //xmlBuilder.append(" xAxisName='").append(chartData.getXAxisName()).append("'");

        //xmlBuilder.append(" yAxisMaxValue='").append(range.getMax()).append("'");
        //xmlBuilder.append(" yAxisMinValue='").append(range.getMin()).append("'");

        // Default 4 horizontal divid lines.
        xmlBuilder.append(" numDivLines='4'");

        // Show maximal two decimal diginal.
        xmlBuilder.append(" yAxisValueDecimals='2'");

        // No fusion chart adjust.
        xmlBuilder.append(" adjustDiv='0'");
    }

    @SuppressWarnings("deprecation")
    private static void buildXMLDataset(StringBuilder xmlBuilder, ChartLine line, Double max, Double min, List<Date> timestamps, boolean readableValue) {
        xmlBuilder.append("<dataset seriesName='")
                .append(line.getName())
                .append("' color='")
                .append(line.getColor())
                .append("' >");

        for (int i = 0; i < line.getData().size(); i++) {
            Double data = line.getData().get(i);
            if (Double.isNaN(data)) {
                xmlBuilder.append("<set />");
                continue;
            }

            if (!Double.isNaN(max) && data > max) {
                data = max;
            } else if (!Double.isNaN(min) && data < min) {
                data = min;
            }

            Date timestamp = timestamps.get(i);
            String toolText = timestamp.toLocaleString() + " " + line.getName() + ":";
            if (readableValue && data > 1024) {
                toolText += bytesToString(Math.round(data)) + "(" + Math.round(data) + ")";
            } else {
                toolText += Math.round(data * 100) / 100;
            }

            xmlBuilder.append("<set value='")
                    .append(data)
                    .append("'")
                    .append(" toolText='")
                    .append(toolText)
                    .append("' />");
        }

        xmlBuilder.append("</dataset>\n");
    }

    private static String bytesToString(double bytes) {
        double exp = Math.floor(Math.log(bytes) / Math.log(1024));
        return Double.toString(bytes / Math.pow(1024, Math.floor(exp)));
    }
}
