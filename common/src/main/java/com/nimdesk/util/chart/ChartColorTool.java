/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.util.chart;

import java.awt.Color;
import java.util.Random;


public class ChartColorTool {
    private static final Color COLORS[] = {
            new Color(0xeb, 0x5e, 0x66),
            new Color(0x20, 0xb2, 0xaa),

            new Color(0xb1, 0x71, 0xfa),
            new Color(0xb6, 0xea, 0x7d),

            new Color(0x58, 0x4e, 0x56),
            new Color(0x4d, 0xff, 0x70),

            new Color(0x98, 0xb5, 0xff),
            new Color(0xf2, 0xdc, 0xa5),

            new Color(0xcd, 0x60, 0x90),
            new Color(0x59, 0x73, 0x68),

            new Color(0x00, 0x00, 0xcd),
            new Color(0x9a, 0x9a, 0x5c)
    };

    private int _next = 0;
    private Color _prevColor = null;
    private final Random _rand;

    public ChartColorTool() {
        _rand = new Random(System.currentTimeMillis());
    }

    synchronized public Color getNext() {
        if ( _next >= COLORS.length ) {
            if ( _prevColor == null ) {
                int r = _rand.nextInt(256);
                int g = _rand.nextInt(256);
                int b = _rand.nextInt(256);
                _prevColor = new Color(r, g, b);
                return _prevColor;
            }
            else {
                Color color = new Color(255 - _prevColor.getRed(),
                                 255 - _prevColor.getGreen(),
                                 255 - _prevColor.getBlue());
                _prevColor = null;
                return color;
            }
        }

        Color c = COLORS[_next];
        _next++;

        return c;
    }
}
