/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.util;

public final class Pagination {
    private final int _first;
    private final int _max;
    private final String _orderBy;
    private final boolean _ascending;

    public Pagination(int first, int max, String orderBy, boolean ascending) {
        _first = first;
        _max = max;
        _orderBy = orderBy;
        _ascending = ascending;
    }

    public int getFirst() {
        return _first;
    }

    public int getMax() {
        return _max;
    }

    public String getOrderBy() {
        return _orderBy;
    }

    public boolean isAscending() {
        return _ascending;
    }
}
