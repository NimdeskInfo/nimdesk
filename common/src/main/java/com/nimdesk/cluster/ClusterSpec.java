/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.cluster;

import java.io.Serializable;
import java.util.Properties;

public class ClusterSpec implements Serializable {

    private static final long serialVersionUID = -7391759794591021719L;

    private String _clusterName;
    private String _clusterId;
    private String _sharedDatastoreName;
    private Properties _properties;

    public String getClusterName() {
        return _clusterName;
    }

    public void setClusterName(String clusterName) {
        _clusterName = clusterName;
    }

    public String getClusterId() {
        return _clusterId;
    }

    public void setClusterId(String clusterId) {
        _clusterId = clusterId;
    }

    public String getSharedDatastoreName() {
        return _sharedDatastoreName;
    }

    public void setSharedDatastoreName(String sharedDatastoreName) {
        _sharedDatastoreName = sharedDatastoreName;
    }

    public Properties getProperties() {
        return _properties;
    }

    public void setProperties(Properties properties) {
        _properties = properties;
    }
}
