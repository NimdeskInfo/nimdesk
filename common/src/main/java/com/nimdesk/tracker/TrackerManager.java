/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.tracker;

public interface TrackerManager {
    public static final String BEAN_NAME = "trackerManager";

    interface ProgressChangeListener {
        void onProgressChange(String entityUuid, ProgressTracker progress);
        void onProgressRemove(String entityUuid, ProgressTracker progress);
    }

    void addProgressChangeListener(ProgressChangeListener listener);

    void removeProgressChangeListener(ProgressChangeListener listener);

    ProgressTracker getProgress(String entityUuid);

    void updateProgress(String entityUuid, ProgressTracker progress);
    void updateProgress(String entityUuid, int percentage);
    void updateProgress(String entityUuid, int percentage, String description);
    void updateProgress(String entityUuid, int percentage, String description, String details);

    void updateLocalProgress(String entityUuid, int percentage, String description);

    void removeProgress(String entityUuid);


    interface HostUsageChangeListener {
        void onUsageChange(String hostUuid, HostUsageTracker hostUsage);
        void onUsageRemove(String hostUuid, HostUsageTracker hostUsage);
    }

    void addHostUsageChangeListener(HostUsageChangeListener listener);

    void removeHostUsageChangeListener(HostUsageChangeListener listener);

    HostUsageTracker getHostUsage(String hostUuid);

    void updateHostUsage(String hsotUuid, HostUsageTracker hostUsage);
    void updateHostUsage(String hostUuid, int cpuUsageMHz, int memoryUsageMB);

    void removeHostUsage(String hostUuid);
}
