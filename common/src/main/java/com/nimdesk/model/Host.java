/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.model;

import java.util.List;

import com.nimdesk.service.ObjectPropertyChangeNotifier;
import com.nimdesk.service.PersistenceService;

/**
 * Host: hypervisor host such as ESXi.
 */
public interface Host extends Base {

    public static final Status[] GREEN_STATUSES = new Status[] {
        Status.Connected
    };
    public static final Status[] YELLOW_STATUSES = new Status[] {
        Status.Connecting
    };
    public static final Status[] RED_STATUSES = new Status[] {
        Status.Disconnected
    };

    public enum Status {
        Connecting("connecting"),
        Connected("connected"),
        Disconnected("disconnected");

        private final String _description;

        Status(String description) {
            _description = description;
        }

        @Override
        public String toString() {
            return _description;
        }
    }

    /**
     * The hypervisor type.
     */
    HypervisorType getType();

    void setType(HypervisorType type);

    /**
     * The hypervisor server address. Normally it's the host itself.
     */
    String getAddress();

    void setAddress(String address);

    /**
     * The user name to login the hypervisor server.
     */
    String getUsername();

    void setUsername(String username);

    /**
     * The password to login the hypervisor server.
     */
    String getPassword();

    void setPassword(String password);

    /**
     * The name of this host.
     */
    String getName();

    void setName(String name);

    /**
     * The uuid of the Nimdesk server that manages this host.
     */
    String getServerUuid();

    void setServerUuid(String serverUuid);

    /**
     * Hypervisor specify ref id
     */
    String getRefId();

    void setRefId(String refId);

    /**
     * The name of the network to which desktop VMs are connected.
     */
    String getVmNetwork();

    void setVmNetwork(String vmNetwork);

    /**
     * The ref id of the network to which desktop VMs are connected.
     */
    String getVmNetworkRefId();

    void setVmNetworkRefId(String vmNetworkRefId);

    /**
     * Whether the host CBRC is enabled.
     */
    boolean isCbrcEnabled();

    void setCbrcEnabled(boolean cbrcEnabled);

    /**
     * Whether the server is the where the current process runs.
     */
    boolean isLocal();

    void setLocal(boolean local);

    /**
     * Status of the host.
     */
    Status getStatus();

    void setStatus(Status status);

    /**
     * CPU capacity in MHz.
     */
    int getCpuCapacityMHz();

    void setCpuCapacityMHz(int cpuMHz);

    /**
     * Memory capacity in MB.
     */
    int getMemoryCapacityMB();

    void setMemoryCapacityMB(int memoryMB);

    /**
     * UUID that comes with each esx host in summary.hardware.uuid.
     * @return
     */
    String getEsxUuid();

    void setEsxUuid(String esxUuid);

    /**
     * vCenter server IP address and username/password.
     */
    String getVCenterIp();

    void setVCenterIp(String vCenterIp);

    // The address of vCenter could be different from what shows in "managementServerIp" field
    // if there are multiple networks.
    String getVCenterAddress();

    void setVCenterAddress(String vCenterAddress);

    String getVCenterUsername();

    void setVCenterUsername(String vCenterUsername);

    String getVCenterPassword();

    void setVCenterPassword(String vCenterPassword);

    /**
     * Shared datastore name.
     */
    String getSharedDatastoreName();

    void setSharedDatastoreName(String sharedDatastoreName);

    /**
     * Shared datastore refId.
     */
    String getSharedDatastoreRefId();

    void setSharedDatastoreRefId(String sharedDatastoreRefId);

    /**
     * Whether to put all (including non-persistent) desktops on the shared datastore.
     */
    boolean isSharedDatastoreForAllDesktops();

    void setSharedDatastoreForAllDesktops(boolean sharedDatastoreForAllDesktops);

    public interface Service extends PersistenceService<Host>, ObjectPropertyChangeNotifier {
        public static final String BEAN_NAME = "hostService";

        List<? extends Host> getLocalHosts();

        Host getLocalHost();

        boolean isLocalHost(String hostUuid);

        List<? extends Host> getHostsForServer(String serverUuid);

        Long countByStatus(Status... status);

        void invalidateNonLocalHosts();

        void removeHost(Host host);
    }
}

