/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.model;

import java.util.List;

import com.nimdesk.event.AsyncCallback;
import com.nimdesk.service.ObjectPropertyChangeNotifier;
import com.nimdesk.service.PersistenceService;


/**
 * Pool defines characteristics of a group of homogeneous desktop VMs.
 */
public interface Pool extends Base {
    public static final String PROP_NAME = "name";
    public static final String PROP_DESCRIPTION ="description";
    public static final String PROP_OSNAME = "osName";
    public static final String PROP_ENABLED = "enabled";
    public static final String PROP_PREFIX = "prefix";
    public static final String PROP_BASEIMAGE_GUID = "baseDiskImageGuid";
    public static final String PROP_MAXSIZE = "maxSize";
    public static final String PROP_MINSIZE = "minSize";
    public static final String PROP_CPUCOUNT = "cpuCount";
    public static final String PROP_MEMORYMB = "memoryMB";
    public static final String PROP_DOMAIN = "domain";
    public static final String PROP_DOMAINORWORKGROUP = "domainOrWorkgroup";
    public static final String PROP_BASE_DISKIMAGE = "baseDiskImageUuid";

    public static final long PROTOCOL_RDP = 0x01;

    public static final long[] PROTOCOLS = new long[] {
        PROTOCOL_RDP,
    };

    public static final long REDIRECT_DRIVE = 0x01;
    public static final long REDIRECT_PRINTER = 0x02;
    public static final long REDIRECT_COMPORT = 0x04;
    public static final long REDIRECT_SMARTCARD = 0x08;

    public static final long REDIRECT_NONE = 0x0;
    public static final long REDIRECT_ALL = REDIRECT_DRIVE | REDIRECT_PRINTER | REDIRECT_COMPORT | REDIRECT_SMARTCARD;

    public static final long[] REDIRECTS = new long[] {
        REDIRECT_DRIVE, REDIRECT_PRINTER, REDIRECT_COMPORT, REDIRECT_SMARTCARD
    };

    public static final Status[] GREEN_STATUSES = new Status[] {
        Status.Ready
    };
    public static final Status[] YELLOW_STATUSES = new Status[] {
        Status.New,
        Status.Provisioning,
        Status.Recomposing,
        Status.Deleting
    };
    public static final Status[] RED_STATUSES = new Status[] {
        //Status.Unknown,
        Status.Error
    };

    public enum Type {
        NonPersistent(true, "Dynamic"),
        Persistent(true, "Persistent"),
        Application(false, "Application");

        private final boolean _isDesktop;
        private final String _description;

        Type(boolean isDesktop, String description) {
            _isDesktop = isDesktop;
            _description = description;
        }

        @Override
        public String toString() {
            return _description;
        }

        public boolean isDesktop() {
            return _isDesktop;
        }

        public boolean isApplication() {
            return !_isDesktop;
        }
    }

    public enum Status {
        New("pending"),
        Provisioning("provisioning"),
        Recomposing("upgrading"),
        Ready("ready"),
        Deleting("deleting"),
        Error("error");

        private final String _description;

        Status(String description) {
            _description = description;
        }

        @Override
        public String toString() {
            return _description;
        }
    }

    /**
     * The type of the pool.
     */
    Type getType();

    void setType(Type type);

    /**
     * The last sequence number of the pool.
     */
    long getSequence();

    void setSequence(long sequence);

    /**
     * The display name of the pool.
     */
    String getName();

    void setName(String name);

    /**
     * The description of the pool.
     */
    String getDescription();

    void setDescription(String description);

    /**
     * The operating system of the pool.
     */
    String getOsName();

    void setOsName(String osName);

    /**
     * The last update time of the pool.
     */
    long getUpdateTime();

    void setUpdateTime(long updateTime);

    /**
     * Whether the pool is enabled.
     */
    boolean isEnabled();

    void setEnabled(boolean enabled);

    /**
     * The prefix of the VMs and machine names created for the pool.
     */
    String getPrefix();

    void setPrefix(String prefix);

    /**
     * The maximum size of VMs in the pool.
     */
    long getMaxSize();

    void setMaxSize(long max);

    /**
     * The minumum pre-started VMs in the pool.
     */
    long getMinSize();

    void setMinSize(long min);

    /**
     * CPU count of each VM in the pool.
     */
    long getCpuCount();

    void setCpuCount(long cpuCount);

    /**
     * Memory size of each VM in the pool.
     */
    long getMemoryMB();

    void setMemoryMB(long memoryMB);

    /**
     * Whether the memory is reserved for desktops.
     */
    boolean isReserveMemory();

    void setReserveMemory(boolean reserveMemory);

    /**
     * Whether the VMs of the pool are put in a domain or workgroup.
     */
    boolean isDomain();

    void setDomain(boolean domain);

    /**
     * The name of the domain or workgroup the VMs are put into.
     */
    String getDomainOrWorkGroup();

    void setDomainOrWorkGroup(String getDomainOrWorkGroup);

    /**
     * Product key of the OS.
     */
    String getProductKey();

    void setProductKey(String productKey);

    /**
     * Timezone index.
     */
    String getTimeZone();

    void setTimeZone(String timezone);

    /**
     * Remote protocols supported by the pool.
     */
    long getProtocols();

    void setProtocols(long protocols);

    Long[] getAllProtocols();

    void setAllProtocols(long... protocol);

    /**
     * Redirect options supported by the pool.
     */
    long getRedirects();

    void setRedirects(long redirects);

    Long[] getAllRedirects();

    void setAllRedirects(long... redirect);

    /**
     * User data disk info.
     */
    long getUserDataDiskSizeInMB();

    void setUserDataDiskSizeInMB(long userDataDiskSizeMB);

    String getUserDataDiskDriveLetter();

    void setUserDataDiskDriveLetter(String driveLetter);

    /**
     * Image uuid of the pool.
     */
    String getVmImageUuid();

    void setVmImageUuid(String vmImageUuid);

    /**
     * Published applications if the pool is in Application mode.
     */
    String[] getApplicationUuids();

    void setApplicationUuids(String[] applicationUuids);

    /**
     * Status of the pool.
     */
    Status getStatus();

    void setStatus(Status status);

    /**
     * Base disk image of the pool on the local server/host.
     */
    //String getBaseDiskImageUuid();

    //void setBaseDiskImageUuid(String diskImageUuid);

    /**
     * Guid of base disk image of the pool.
     */
    String getBaseDiskImageGuid();

    void setBaseDiskImageGuid(String diskImageGuid);

    /**
     * Instantiate host uuid. For persistent pool, only instantiate on one host
     * and replicate pool base diskImage to all other hosts.
     */
    String getInstantiateHostUuid();

    void setInstantiateHostUuid(String hostUuid);

    /**
     * Whether this pool is the local host only orphan.
     */
    boolean isOrphaned();

    void setOrphaned(boolean orphaned);

    public interface Service extends PersistenceService<Pool>, ObjectPropertyChangeNotifier {
        public static final String BEAN_NAME = "poolService";

        Long countByVmImage(String vmImageUuid);

        Long countByStatus(Status... status);

        Pool getPoolByName(String poolName);

        DiskImage getBaseDiskImage(Pool pool);

        List<? extends Pool> getByStatus(Status... status);

        List<? extends Pool> getByVmImage(String vmImageUuid);

        List<? extends Pool> getAllPoolsForUser(User user);

        void instantiatePool(Pool pool, AsyncCallback callback);

        void deletePool(Pool pool, AsyncCallback callback);

        void provisionDesktops(Pool pool, boolean rebalance, AsyncCallback callback);

        void recomposeDesktops(Pool pool, boolean force, AsyncCallback callback);

        void rebalanceDesktops(Pool pool, AsyncCallback callback);

        void provisionLocalDesktop(Pool pool, VmInstance vmInstance, AsyncCallback callback);
    }
}

