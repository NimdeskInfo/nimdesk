/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

public class TimeZone {
    public static TimeZone getTimeZone(String index) {
        for (TimeZone timezone : TIMEZONES) {
            if (StringUtils.equals(timezone.getIndex(), index)) {
                return timezone;
            }
        }
        return null;
    }

    private final String _index;
    private final String _time;

    public TimeZone(String index, String time) {
        _index = index;
        _time = time;
    }

    public String getIndex() {
        return _index;
    }

    public String getTime() {
        return _time;
    }

    public static final List<TimeZone> TIMEZONES = new ArrayList<TimeZone>();
    static {
        // http://msdn.microsoft.com/en-us/library/ms912391%28v=winembedded.11%29.aspx

        TIMEZONES.add(new TimeZone("000", "(GMT-12:00) International Date Line West"));
        TIMEZONES.add(new TimeZone("001", "(GMT-11:00) Midway Island, Samoa"));
        TIMEZONES.add(new TimeZone("002", "(GMT-10:00) Hawaii"));
        TIMEZONES.add(new TimeZone("003", "(GMT-09:00) Alaska"));
        TIMEZONES.add(new TimeZone("004", "(GMT-08:00) Pacific Time (US and Canada); Tijuana"));
        TIMEZONES.add(new TimeZone("010", "(GMT-07:00) Mountain Time (US and Canada)"));
        TIMEZONES.add(new TimeZone("013", "(GMT-07:00) Chihuahua, La Paz, Mazatlan"));
        TIMEZONES.add(new TimeZone("015", "(GMT-07:00) Arizona"));
        TIMEZONES.add(new TimeZone("020", "(GMT-06:00) Central Time (US and Canada)"));
        TIMEZONES.add(new TimeZone("025", "(GMT-06:00) Saskatchewan"));
        TIMEZONES.add(new TimeZone("030", "(GMT-06:00) Guadalajara, Mexico City, Monterrey"));
        TIMEZONES.add(new TimeZone("033", "(GMT-06:00) Central America"));
        TIMEZONES.add(new TimeZone("035", "(GMT-05:00) Eastern Time (US and Canada)"));
        TIMEZONES.add(new TimeZone("040", "(GMT-05:00) Indiana (East)"));
        TIMEZONES.add(new TimeZone("045", "(GMT-05:00) Bogota, Lima, Quito"));
        TIMEZONES.add(new TimeZone("050", "(GMT-04:00) Atlantic Time (Canada)"));
        TIMEZONES.add(new TimeZone("055", "(GMT-04:00) Caracas, La Paz"));
        TIMEZONES.add(new TimeZone("056", "(GMT-04:00) Santiago"));
        TIMEZONES.add(new TimeZone("060", "(GMT-03:30) Newfoundland and Labrador"));
        TIMEZONES.add(new TimeZone("065", "(GMT-03:00) Brasilia"));
        TIMEZONES.add(new TimeZone("070", "(GMT-03:00) Buenos Aires, Georgetown"));
        TIMEZONES.add(new TimeZone("073", "(GMT-03:00) Greenland"));
        TIMEZONES.add(new TimeZone("075", "(GMT-02:00) Mid-Atlantic"));
        TIMEZONES.add(new TimeZone("080", "(GMT-01:00) Azores"));
        TIMEZONES.add(new TimeZone("083", "(GMT-01:00) Cape Verde Islands"));
        TIMEZONES.add(new TimeZone("085", "(GMT) Greenwich Mean Time : Dublin, Edinburgh, Lisbon, London"));
        TIMEZONES.add(new TimeZone("090", "(GMT) Casablanca, Monrovia"));
        TIMEZONES.add(new TimeZone("095", "(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague"));
        TIMEZONES.add(new TimeZone("100", "(GMT+01:00) Sarajevo, Skopje, Warsaw, Zagreb"));
        TIMEZONES.add(new TimeZone("105", "(GMT+01:00) Brussels, Copenhagen, Madrid, Paris"));
        TIMEZONES.add(new TimeZone("110", "(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna"));
        TIMEZONES.add(new TimeZone("113", "(GMT+01:00) West Central Africa"));
        TIMEZONES.add(new TimeZone("115", "(GMT+02:00) Bucharest"));
        TIMEZONES.add(new TimeZone("120", "(GMT+02:00) Cairo"));
        TIMEZONES.add(new TimeZone("125", "(GMT+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius"));
        TIMEZONES.add(new TimeZone("130", "(GMT+02:00) Athens, Istanbul, Minsk"));
        TIMEZONES.add(new TimeZone("135", "(GMT+02:00) Jerusalem"));
        TIMEZONES.add(new TimeZone("140", "(GMT+02:00) Harare, Pretoria"));
        TIMEZONES.add(new TimeZone("145", "(GMT+03:00) Moscow, St. Petersburg, Volgograd"));
        TIMEZONES.add(new TimeZone("150", "(GMT+03:00) Kuwait, Riyadh"));
        TIMEZONES.add(new TimeZone("155", "(GMT+03:00) Nairobi"));
        TIMEZONES.add(new TimeZone("158", "(GMT+03:00) Baghdad"));
        TIMEZONES.add(new TimeZone("160", "(GMT+03:30) Tehran"));
        TIMEZONES.add(new TimeZone("165", "(GMT+04:00) Abu Dhabi, Muscat"));
        TIMEZONES.add(new TimeZone("170", "(GMT+04:00) Baku, Tbilisi, Yerevan"));
        TIMEZONES.add(new TimeZone("175", "(GMT+04:30) Kabul"));
        TIMEZONES.add(new TimeZone("180", "(GMT+05:00) Ekaterinburg"));
        TIMEZONES.add(new TimeZone("185", "(GMT+05:00) Islamabad, Karachi, Tashkent"));
        TIMEZONES.add(new TimeZone("190", "(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi"));
        TIMEZONES.add(new TimeZone("193", "(GMT+05:45) Kathmandu"));
        TIMEZONES.add(new TimeZone("195", "(GMT+06:00) Astana, Dhaka"));
        TIMEZONES.add(new TimeZone("200", "(GMT+06:00) Sri Jayawardenepura"));
        TIMEZONES.add(new TimeZone("201", "(GMT+06:00) Almaty, Novosibirsk"));
        TIMEZONES.add(new TimeZone("203", "(GMT+06:30) Yangon (Rangoon)"));
        TIMEZONES.add(new TimeZone("205", "(GMT+07:00) Bangkok, Hanoi, Jakarta"));
        TIMEZONES.add(new TimeZone("207", "(GMT+07:00) Krasnoyarsk"));
        TIMEZONES.add(new TimeZone("210", "(GMT+08:00) Beijing, Chongqing, Hong Kong SAR, Urumqi"));
        TIMEZONES.add(new TimeZone("215", "(GMT+08:00) Kuala Lumpur, Singapore"));
        TIMEZONES.add(new TimeZone("220", "(GMT+08:00) Taipei"));
        TIMEZONES.add(new TimeZone("225", "(GMT+08:00) Perth"));
        TIMEZONES.add(new TimeZone("227", "(GMT+08:00) Irkutsk, Ulaan Bataar"));
        TIMEZONES.add(new TimeZone("230", "(GMT+09:00) Seoul"));
        TIMEZONES.add(new TimeZone("235", "(GMT+09:00) Osaka, Sapporo, Tokyo"));
        TIMEZONES.add(new TimeZone("240", "(GMT+09:00) Yakutsk"));
        TIMEZONES.add(new TimeZone("245", "(GMT+09:30) Darwin"));
        TIMEZONES.add(new TimeZone("250", "(GMT+09:30) Adelaide"));
        TIMEZONES.add(new TimeZone("255", "(GMT+10:00) Canberra, Melbourne, Sydney"));
        TIMEZONES.add(new TimeZone("260", "(GMT+10:00) Brisbane"));
        TIMEZONES.add(new TimeZone("265", "(GMT+10:00) Hobart"));
        TIMEZONES.add(new TimeZone("270", "(GMT+10:00) Vladivostok"));
        TIMEZONES.add(new TimeZone("275", "(GMT+10:00) Guam, Port Moresby"));
        TIMEZONES.add(new TimeZone("280", "(GMT+11:00) Magadan, Solomon Islands, New Caledonia"));
        TIMEZONES.add(new TimeZone("285", "(GMT+12:00) Fiji Islands, Kamchatka, Marshall Islands"));
        TIMEZONES.add(new TimeZone("290", "(GMT+12:00) Auckland, Wellington"));
        TIMEZONES.add(new TimeZone("300", "(GMT+13:00) Nuku'alofa"));
    }
}
