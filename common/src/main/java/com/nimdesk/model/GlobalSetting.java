/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.model;

import com.nimdesk.service.ObjectPropertyChangeNotifier;
import com.nimdesk.service.PersistenceService;


/**
 * Setting global to the whole cluster.
 */
public interface GlobalSetting extends Base {
    public static final String PARAM_VALUE = "value";

    String getKey();

    void setKey(String key);

    String getValue();

    void setValue(String value);

    public interface Service extends PersistenceService<GlobalSetting>, ObjectPropertyChangeNotifier {
        public static final String BEAN_NAME = "globalSettingService";

        GlobalSetting getByKey(String key);

        void deleteByKey(String key);
    }
}
