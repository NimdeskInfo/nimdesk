/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.model;

import java.util.List;

import com.nimdesk.service.ObjectPropertyChangeNotifier;
import com.nimdesk.service.PersistenceService;

/**
 * Storage represents local datastore in Vmware or VBD in Xen.
 */
public interface Storage extends Base {
    public static final String SHARED_STORAGE_UUID = "SHARED";

    /**
     * The display name of this storage.
     */
    public String getName();

    public void setName(String name);

    /**
     * The host the storage connects to.
     */
    public String getHostUuid();

    public void setHostUuid(String hostUuid);

    /**
     *  The refId of this storage.
     */
    public String getRefId();

    public void setRefId(String refId);

    /**
     * The capacity of this storage.
     */
    public long getCapacity();

    public void setCapacity(long capacity);

    /**
     * The free space of this storage.
     */
    public long getFreeSpace();

    public void setFreeSpace(long freeSpace);

    public interface Service extends PersistenceService<Storage>, ObjectPropertyChangeNotifier {
        public static final String BEAN_NAME = "storageService";

        List<? extends Storage> getHostStorages(String hostUuid);

        List<? extends Storage> getLocalStorages();

        boolean isLocalStorage(Storage storage);

        Storage getSharedStorage();

        Storage allocateStorage(String hostUuid);

        void updateStorageSizes(Storage storage, long capacity, long freeSpace);
    }
}
