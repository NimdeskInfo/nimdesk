/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.model;

import com.nimdesk.event.AsyncCallback;
import com.nimdesk.service.ObjectPropertyChangeNotifier;
import com.nimdesk.service.PersistenceService;

/**
 * Represents VM (logical) images. One VmImage has one or more different versions
 * of DiskImages
 */
public interface VmImage extends Base {
    public static final Status[] GREEN_STATUSES = new Status[] {
        Status.Ready
    };
    public static final Status[] YELLOW_STATUSES = new Status[] {
        Status.Importing,
        Status.Updating,
        Status.Deleting
    };
    public static final Status[] RED_STATUSES = new Status[] {
        Status.Broken
    };

    public enum Status {
        Importing("importing"),
        Updating("updating"),
        Ready("ready"),
        Deleting("deleting"),
        Broken("error");

        private final String _description;

        Status(String description) {
            _description = description;
        }

        @Override
        public String toString() {
            return _description;
        }
    }

    /**
     * The display name of the image.
     */
    String getName();

    void setName(String name);

    /**
     * The description of the image.
     */
    String getDescription();

    void setDescription(String description);

    /**
     * The operating system of the image.
     */
    String getOsName();

    void setOsName(String osName);

    /**
     * The size of the image disk capacity.
     */
    long getImageSizeInKB();

    void setImageSizeInKB(long imageSizeKB);

    /**
     * The last update time of the image.
     */
    long getUpdateTime();

    void setUpdateTime(long updateTime);

    /**
     * The display version of the image.
     */
    long getVersion();

    void setVersion(long version);

    /**
     * The image guid (not DiskImage uuid). Guid identifies a disk image
     * independent of its location.
     */
    String getCurrentDiskImageGuid();

    void setCurrentDiskImageGuid(String Guid);

    /**
     * Current status of the image.
     */
    Status getStatus();

    void setStatus(Status status);

    /**
     * The uuid of the updating disk image.
     */
    String getUpdatingDiskImageUuid();

    void setUpdatingDiskImageUuid(String diskImageUuid);

    /**
     * The ip of the updating disk image (for connecting from ui).
     */
    String getUpdatingDiskImageIp();

    void setUpdatingDiskImageIp(String diskImageIp);

    /**
     * The parameters this vmImage is associated to.
     */
    //List<? extends VmImageParam> getParams();

    //void setParams(List<? extends VmImageParam> params);

    /**
     * Whether this vmImage is the local host only orphan.
     */
    boolean isOrphaned();

    void setOrphaned(boolean orphaned);

    public interface Service extends PersistenceService<VmImage>, ObjectPropertyChangeNotifier {
        public static final String BEAN_NAME = "vmImageService";

        Long countByStatus(Status... status);

        VmImage getVmImageByName(String vmImageName);

        DiskImage getCurrentDiskImage(VmImage vmImage, String hostUuid);

        void importVm(VmImage vmImage, String vmName, String vmRefId, AsyncCallback callback);

        void cloneUpdateVmImage(VmImage vmImage, AsyncCallback callback);

        void cancelUpdateVmImage(VmImage vmImage, AsyncCallback callback);

        void importUpdatedVmImage(VmImage vmImage, AsyncCallback callback);

        void deleteVmImage(VmImage vmImage, AsyncCallback callback);

        void revertVmImage(VmImage vmImage, AsyncCallback callback);
    }
}
