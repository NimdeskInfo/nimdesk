/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.model;

import java.util.List;

import com.nimdesk.service.ObjectPropertyChangeNotifier;
import com.nimdesk.service.PersistenceService;
import com.nimdesk.util.Pagination;

public interface Task extends Base {

    public enum Type {
        Generic("Task"),
        ImportImageFromVm("Import image"),
        CloneImageForUpdate("Update image"),
        ImportUpdatedImage("Import updated image"),
        CopyImageFromPeer("Copy image"),
        DeleteImage("Delete image"),
        InstantiatePool("Instantiate pool"),
        DeletePool("Delete pool"),
        ProvisionDesktop("Provision desktop"),
        RefreshDesktop("Refresh desktop"),
        Link2LocalReplica("Link to local replica"),
        DeleteDesktop("Delete desktop");

        private final String _description;

        Type(String description) {
            _description = description;
        }

        @Override
        public String toString() {
            return _description;
        }
    }

    public enum Status {
        Pending("Pending"),
        InProgress("In progress"),
        Completed("Completed"),
        Failed("Failed");

        private final String _description;

        Status(String description) {
            _description = description;
        }

        public String getDescription() {
            return _description;
        }
    };

    Type getType();

    void setType(Type type);

    String getTargetType();

    void setTargetType(String targetType);

    String getTargetUuid();

    void setTargetUuid(String targetUuid);

    String getTargetName();

    void setTargetName(String targetName);

    String getAssociateType();

    void setAssociateType(String associateType);

    String getAssociateUuid();

    void setAssociateUuid(String associateUuid);

    String getAssociateName();

    void setAssociateName(String associateName);

    long getStartTime();

    void setStartTime(long startTime);

    long getEndTime();

    void setEndTime(long endTime);

    Status getStatus();

    void setStatus(Status status);

    String getDetails();

    void setDetails(String details);

    String getServerUuid();

    void setServerUuid(String serverUuid);

    public interface Service extends PersistenceService<Task>, ObjectPropertyChangeNotifier {
        public static final String BEAN_NAME = "taskService";

        Long countAllPoolTasks();

        Long countPoolTasks(String poolUuid);

        Long countAllVmImageTasks();

        Long countVmImageTasks(String vmImageUuid);

        Long countAllVmInstanceTasks();

        Long countVmInstanceTasks(String vmInstanceUuid);

        List<? extends Task> getAllPoolTasks(Pagination pagination);

        List<? extends Task> getPoolTasks(String poolUuid, Pagination pagination);

        List<? extends Task> getAllVmImageTasks(Pagination pagination);

        List<? extends Task> getVmImageTasks(String vmImageUuid, Pagination pagination);

        List<? extends Task> getAllVmInstanceTasks(Pagination pagination);

        List<? extends Task> getVmInstanceTasks(String vmInstanceUuid, Pagination pagination);

        List<? extends Task> getLocalTasks();

        List<? extends Task> getLocalTasksByTime(boolean before, long time);

        boolean isLocalTask(Task task);

        void invalidateLocalInProgressTasks();

        void deleteOldTasks(long time);
    }
}

