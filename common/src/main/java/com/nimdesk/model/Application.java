/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.model;

import java.util.List;

import com.nimdesk.service.ObjectPropertyChangeNotifier;
import com.nimdesk.service.PersistenceService;

public interface Application extends Base {
    /**
     * The name of the application.
     */
    String getName();

    void setName(String name);

    /**
     * The last update time of the application.
     */
    long getUpdateTime();

    void setUpdateTime(long updateTime);

    /**
     * The command line of the application executable.
     */
    String getCmdLine();

    void setCmdLine(String cmdLine);

    /**
     * The Base64 encoding of icon (png).
     */
    String getIcon();

    void setIcon(String icon);

    /**
     * The uuid of the vmImage this application published on.
     */
    String getVmImageUuid();

    void setVmImageUuid(String vmImageUuid);

    public interface Service extends PersistenceService<Application>, ObjectPropertyChangeNotifier {
        public static final String BEAN_NAME = "applicationService";

        List<? extends Application> getApplicationsByVmImage(String vmImageUuid);
    }
}
