/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.model;

import java.io.Serializable;

/**
 * Base for all objects. Only when uuid is not empty, will the object be
 * eligible for cross cluster replication.
 */
public interface Base extends Serializable {
    public static final String PROP_STATUS = "status";
    public static final String PROP_UPDATETIME = "updateTime";
    public static final String PROP_LASTERROR = "lastError";

    String getUuid();

    void setUuid(String uuid);
}

