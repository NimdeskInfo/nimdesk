/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.model;

import java.util.List;

import com.nimdesk.service.ObjectPropertyChangeNotifier;
import com.nimdesk.service.PersistenceService;

public interface User extends Base {

    public enum Type {
        User,
        Group
    }

    Type getType();

    void setType(Type type);

    String getName();

    void setName(String name);

    String getLogonName();

    void setLogonName(String logonName);

    String getPassword();

    void setPassword(String password);

    String getDomain();

    void setDomain(String domain);

    String getDistinguishedName();

    void setDistinguishedName(String distinguishedName);

    public interface Service extends PersistenceService<User>, ObjectPropertyChangeNotifier {
        public static final String BEAN_NAME = "userService";

        List<String> getAllDomains();

        List<? extends User> getAllUsers();

        List<? extends User> getAllGroups(String domain);

        User getByLogonNameDomain(String logonName, String domain);

        User getByDistinguishedName(String distinguishedName);

        List <? extends User> getByFilter(boolean includeUsers, boolean includeGroups, String prefix, String domain);

        List<? extends User> getAllUsersForPool(Pool pool);

        void assignUsersToPool(Pool pool, List<User> users);

        void unassignUsersFromPool(Pool pool, List<User> users);
    }
}
