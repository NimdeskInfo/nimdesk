/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.model;

import com.nimdesk.service.ObjectPropertyChangeNotifier;
import com.nimdesk.service.PersistenceService;

public interface LdapServer extends Base {

    public static final LdapServer WORKGROUP = new LdapServer() {

        private static final long serialVersionUID = 1L;

        @Override
        public String getUuid() {
            return "";
        }
        @Override
        public void setUuid(String uuid) {
        }
        @Override
        public String getAddress() {
            return null;
        }
        @Override
        public void setAddress(String address) {
        }
        @Override
        public int getPort() {
            return 0;
        }
        @Override
        public void setPort(int port) {
        }
        @Override
        public boolean isSsl() {
            return false;
        }
        @Override
        public void setSsl(boolean ssl) {
        }
        @Override
        public String getDomain() {
            return "WORKGROUP";
        }
        @Override
        public void setDomain(String domain) {
        }
        @Override
        public String getUsername() {
            return null;
        }
        @Override
        public void setUsername(String username) {
        }
        @Override
        public String getPassword() {
            return null;
        }
        @Override
        public void setPassword(String password) {
        }
    };

    /**
     * The address of the ldap server.
     */
    String getAddress();

    void setAddress(String address);

    /**
     * The port of the ldap server (default 389).
     */
    int getPort();

    void setPort(int port);

    /**
     * Whether using ssl (default false).
     */
    boolean isSsl();

    void setSsl(boolean ssl);

    /**
     * The domain name of the ldap server.
     */
    String getDomain();

    void setDomain(String domain);

    /**
     * The user name of an ldap admin.
     */
    String getUsername();

    void setUsername(String username);

    /**
     * The password of an ldap admin.
     */
    String getPassword();

    void setPassword(String password);

    public interface Service extends PersistenceService<LdapServer>, ObjectPropertyChangeNotifier {
        public static final String BEAN_NAME = "ldapServerService";

        LdapServer getByDomain(String domain);
    }
}
