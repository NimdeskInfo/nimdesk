/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.nimdesk.service.ObjectPropertyChangeNotifier;
import com.nimdesk.service.PersistenceService;
import com.nimdesk.util.Pagination;
import com.nimdesk.util.Pair;

public interface EventLog extends Base {

    public enum Severity {
        Info,
        Warning,
        Error
    }

    /**
     * The description template. The description can be formated with template
     * and eventLogParams.
     */
    String getDescriptionTemplate();

    void setDescriptionTemplate(String template);

    /**
     * Severity of the eventLog.
     */
    Severity getSeverity();

    void setSeverity(Severity severity);

    /**
     * The create date time of the eventLog.
     */
    long getDateTime();

    void setDateTime(long dateTime);

    /**
     * The type of the target this eventLog is associated to.
     */
    String getTargetType();

    void setTargetType(String targetType);

    /**
     * The uuid of the target this eventLog is associated to.
     */
    String getTargetUuid();

    void setTargetUuid(String targetUuid);

    /**
     * The display name of the target this eventLog is associated to.
     */
    String getTargetName();

    void setTargetName(String targetName);

    /**
     * The user that raises this eventLog.
     */
    String getUser();

    void setUser(String user);

    /**
     * The uuid of the task this eventLog is associated to.
     */
    String getTaskUuid();

    void setTaskUuid(String taskUuid);

    /**
     * The parameters this eventLog is associated to.
     */
    String getParameter(String key);

    void setParameter(String key, String value);

    Properties retrieveParameters();

    public interface Service extends PersistenceService<EventLog>, ObjectPropertyChangeNotifier {
        public static final String BEAN_NAME = "eventLogService";

        Long countAllPoolEvents();

        Long countPoolEvents(String poolUuid);

        Long countAllVmImageEvents();

        Long countVmImageEvents(String vmImageUuid);

        Long countAllVmInstanceEvents();

        Long countVmInstanceEvents(String vmInstanceUuid);

        List<? extends EventLog> getAllPoolEvents(Pagination pagination);

        List<? extends EventLog> getPoolEvents(String poolUuid, Pagination pagination);

        List<? extends EventLog> getAllVmImageEvents(Pagination pagination);

        List<? extends EventLog> getVmImageEvents(String vmImageUuid, Pagination pagination);

        List<? extends EventLog> getAllVmInstanceEvents(Pagination pagination);

        List<? extends EventLog> getVmInstanceEvents(String vmInstanceUuid, Pagination pagination);

        void addEventLog(Severity severity, String template, String user, Task task, Pair<String, String>... param);

        void addPoolEventLog(Severity severity, Pool pool, String template, String user, Task task, Pair<String, String>... param);

        void addVmImageEventLog(Severity severity, VmImage vmImage, String template, String user, Task task, Pair<String, String>... param);

        void addVmInstanceEventLog(Severity severity, VmInstance vmInstance, String template, String user, Task task, Pair<String, String>... param);

        List<? extends EventLog> getLocalEventLogsByTime(boolean before, long time);

        void deleteOldEventLogs(long time);
    }

    public final static Map<String, String> TEMPLATE_MAP = new HashMap<String, String>();
}
