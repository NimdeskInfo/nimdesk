/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.model;

import java.util.List;

import com.nimdesk.event.AsyncCallback;
import com.nimdesk.service.ObjectPropertyChangeNotifier;
import com.nimdesk.service.PersistenceService;

public interface DiskImage extends Base {
    public static final String IMAGE_INFIX = "-M";
    public static final String POOL_INFIX = "-P";

    public static enum Status {
        Unknown("unknown"),
        Importing("importing"),
        RequestingCopy("pending"),
        Copying("replicating"),
        Preparing("preparing"),
        Prepared("prepared"),
        Cloning("cloning"),
        Ready("ready"),
        Updating("updating"),
        Deleting("deleting"),
        ImportingUpdate("importing update"),
        Error("error");

        private final String _description;

        Status(String description) {
            _description = description;
        }

        @Override
        public String toString() {
            return _description;
        }
    }

    public static enum Type {
        ImageFullBase(true, true),
        ImageDelta(true, true),
        PoolBase(false, true),
        PoolBase2(false, true); // For persistent pool

        private final boolean _imageBase;
        private final boolean _replicatable;

        Type(boolean imageBase, boolean replicatable) {
            _imageBase = imageBase;
            _replicatable = replicatable;
        }

        public boolean isImageBase() {
            return _imageBase;
        }

        public boolean isReplicatable() {
            return _replicatable;
        }

        public String getInfix() {
            return _imageBase ? IMAGE_INFIX : POOL_INFIX;
        }
    }

    /**
     * The guid that uniquely identifies the same disk image across
     * all nodes.
     */
    String getGuid();

    void setGuid(String guid);

    /**
     * The uuid of the host this disk image resides on.
     */
    String getHostUuid();

    void setHostUuid(String hostUuid);

    /**
     * The uuid of the storage this disk image resides on.
     */
    String getStorageUuid();

    void setStorageUuid(String storageUuid);

    /**
     * The refId of the vm if this disk image has been registered.
     */
    String getVmRefId();

    void setVmRefId(String vmRefId);

    /**
     * The ip of the vm if this disk image is in updating stage.
     */
    String getIpAddress();

    void setIpAddress(String ipAddress);

    /**
     * The create time of the disk image.
     */
    long getCreateTime();

    void setCreateTime(long createTime);

    /**
     * The name of the disk image which is also the name of the
     * directory on datastore.
     */
    String getName();

    void setName(String name);

    /**
     * The uuid of the owning VmImage or Pool.
     */
    String getOwnerUuid();

    void setOwnerUuid(String uuid);

    /**
     * The display version of the disk image.
     */
    long getVersion();

    void setVersion(long version);

    /**
     * The type of the disk image.
     */
    Type getType();

    void setType(Type type);

    /**
     * The guid of the parent disk image if exists.
     */
    String getParentGuid();

    void setParentGuid(String parentGuid);

    /**
     * Current status of the image.
     */
    Status getStatus();

    void setStatus(Status status);

    /**
     * The physical size of this image.
     */
    long getDiskSize();

    void setDiskSize(long diskSize);

    /**
     * Indicate whether CBRC digest has been configured.
     */
    boolean isCbrcEnabled();

    void setCbrcEnabled(boolean cbrcEnabled);

    public interface Service extends PersistenceService<DiskImage>, ObjectPropertyChangeNotifier {
        public static final String BEAN_NAME = "diskImageService";

        Long countByOwner(String ownerUuid);

        List<? extends DiskImage> getDiskImagesByGuid(String guid, DiskImage.Status... status);

        List<? extends DiskImage> getDiskImagesByOwner(String ownerUuid, DiskImage.Status... status);

        List<? extends DiskImage> getDiskImagesByName(String name);

        DiskImage getLocalDiskImageByGuid(String guid);

        List<? extends DiskImage> getLocalDiskImagesByOwner(String ownerUuid);

        List<? extends DiskImage> getLocalDiskImages(DiskImage.Status... status);

        List<? extends DiskImage> getDiskImagesOnHost(String hostUuid);

        boolean isLocalDiskImage(DiskImage diskImage);

        void invalidateNonLocalDiskImages();

        void deleteDiskImage(DiskImage diskImage, AsyncCallback callback);
    }
}

