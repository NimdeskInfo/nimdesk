/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.model;

import java.util.List;

import com.nimdesk.service.ObjectPropertyChangeNotifier;
import com.nimdesk.service.PersistenceService;

/**
 * This class represents one user-group association. Both user and group
 * are represented with User class with different types.
 */
public interface UserGroup extends Base {

    /**
     * The uuid of the user.
     */
    String getUserUuid();

    void setUserUuid(String userUuid);

    /**
     * The uuid of the group.
     */
    String getGroupUuid();

    void setGroupUuid(String groupUuid);

    public interface Service extends PersistenceService<UserGroup>, ObjectPropertyChangeNotifier {
        public static final String BEAN_NAME = "userGroupService";

        List<? extends User> getGroupsForUser(String userUuid);

        List<? extends User> getUserInGroup(String groupUuid);

        void removeUsersFromGroup(String groupUuid, String... userUuid);

        void removeGroupsFromUser(String userUuid, String... groupUuid);

        void deleteUserGroupsForUser(String userUuid);

        void deleteUserGroupsForGroup(String groupUuid);
    }
}
