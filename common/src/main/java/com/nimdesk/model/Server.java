/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.model;

import java.util.List;

import com.nimdesk.event.AsyncCallback;
import com.nimdesk.service.ObjectPropertyChangeNotifier;
import com.nimdesk.service.PersistenceService;

/**
 * Server object that represents Nimdesk server node (virtual appliance).
 */
public interface Server extends Base {
    public static final Status[] GREEN_STATUSES = new Status[] {
        Status.Running
    };
    public static final Status[] YELLOW_STATUSES = new Status[] {
        Status.Initializing,
        Status.Maintenance,
        Status.Disjoined
    };
    public static final Status[] RED_STATUSES = new Status[] {
        Status.Disconnected
    };

    public static final String PROP_LIC_VERSION = "licVersion";

    public static enum Status {
        Initializing("initializing"),
        Running("running"),
        Maintenance("maintenance"),
        Disconnected("disconnected"),
        Disjoined("standalone");

        private final String _description;

        Status(String description) {
            _description = description;
        }

        @Override
        public String toString() {
            return _description;
        }
    }

    /**
     * The cluster name this server joined.
     */
    String getClusterName();

    void setClusterName(String clusterName);

    /**
     * The cluster id this server joined.
     */
    String getClusterId();

    void setClusterId(String clusterId);

    /**
     * The cluster address this server is assigned.
     */
    String getClusterAddress();

    void setClusterAddress(String clusterAddress);

    /**
     * The public facing address for management and vdi client connection.
     */
    String getPublicAddress();

    void setPublicAddress(String publicAddress);

    /**
     * (Optional) the private address to communicate with isolated VMs.
     */
    String getPrivateAddress();

    void setPrivateAddress(String privateAddress);

    /**
     * The ref id of the server VM.
     */
    String getVmRefId();

    void setVmRefId(String vmRefId);

    /**
     * The software version of this server.
     */
    String getVersion();

    void setVersion(String version);

    /**
     * whether the server is the where the current process runs.
     */
    boolean isLocal();

    void setLocal(boolean local);

    /**
     * The status of this server.
     */
    Status getStatus();

    void setStatus(Status status);

    public interface Service extends PersistenceService<Server>, ObjectPropertyChangeNotifier {
        public static final String BEAN_NAME = "serverService";

        Long countByStatus(Status... status);

        List<? extends Server> getRunningServers();

        Server getLocalServer();

        boolean isLocalServer(String serverUuid);

        Server getByPublicAddress(String publicAddress);

        Server getByClusterAddress(String clusterAddress);

        void invalidateNonLocalServers();

        void maintainServer(String serverUuid, boolean enter, AsyncCallback callback);

        void removeServer(String serverUuid, boolean disjoinCluster, AsyncCallback callback);

        void disjoinCluster(String serverUuid, AsyncCallback callback);
    }
}
