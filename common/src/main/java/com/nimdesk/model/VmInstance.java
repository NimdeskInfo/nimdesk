/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.model;

import java.util.List;

import com.nimdesk.event.AsyncCallback;
import com.nimdesk.service.ObjectPropertyChangeNotifier;
import com.nimdesk.service.PersistenceService;
import com.nimdesk.util.Pagination;


/**
 * VMInstance represents a provisioned (or being provisioned) VM instance.
 */
public interface VmInstance extends Base {
    public static final Status[] GREEN_STATUSES = new Status[] {
        Status.Ready,
        Status.Stopping,
        Status.Starting,
        Status.Started,
        Status.Assigning,
        Status.Assigned,
        Status.AssignError,
        Status.InSession,
        Status.LoggedOff
    };
    public static final Status[] YELLOW_STATUSES = new Status[] {
        Status.New,
        Status.Provisioning,
        Status.Creating,
        Status.ObtainingIp,
        Status.TakingSnapshot,
        Status.Refreshing,
        Status.Destroying,
        Status.Destroyed
    };
    public static final Status[] RED_STATUSES = new Status[] {
        Status.Unknown,
        Status.Error
    };

    public static final Status[] PRESTARTING_STATUSES = new Status[] {
        Status.New,
        Status.Provisioning,
        Status.Creating,
        Status.ObtainingIp,
        Status.TakingSnapshot,
        Status.AssignError,
        Status.Refreshing
    };
    public static final Status[] STARTING_STATUSES = new Status[] {
        Status.New,
        Status.Refreshing,
        Status.Provisioning,
        Status.Creating,
        Status.ObtainingIp,
        Status.TakingSnapshot,
        Status.AssignError,
        Status.Stopping,
        Status.LoggedOff,
        Status.Ready,
        Status.Starting,
        Status.Started
    };
    public static final Status[] ORDERED_STATUSES = new Status[] {
        // This is the order of being deleted if the pool size over limit.
        Status.Error,
        Status.Destroying,
        Status.Destroyed,
        Status.Unknown,
        Status.New,
        Status.Provisioning,
        Status.Creating,
        Status.ObtainingIp,
        Status.TakingSnapshot,
        Status.Refreshing,
        Status.Ready,
        Status.Starting,
        Status.Started,
        Status.Stopping,
        Status.Assigning,
        Status.Assigned,
        Status.AssignError,
        Status.InSession,
        Status.LoggedOff
        //Status.Unknown // Be conservative by treating "Unknown" as running
    };
    public static final Status[] READY_STATUSES = new Status[] {
        Status.Ready,
        Status.LoggedOff,
        Status.Starting,
        Status.Started,
        Status.Stopping,
        Status.Assigning,
        Status.Assigned
    };
    public static final Status[] UNAVAILABLE_STATUES = new Status[] {
        Status.Unknown,
        Status.Error,
        Status.Destroying,
        Status.Destroyed
    };

    public enum Status {
        Unknown("unknown"),
        New("pending"),
        Provisioning("provisioning"),
        Creating("creating"),
        ObtainingIp("preparing"),
        TakingSnapshot("standby"),
        Ready("ready"),
        Starting("starting"),
        Started("started"),
        Stopping("stopping"),
        Assigning("assigning user"),
        Assigned("user assigned"),
        AssignError("assign user error"),
        InSession("in session"),
        LoggedOff("logged off"),
        Refreshing("refreshing"),
        Destroying("deleting"),
        Destroyed("deleted"),
        Error("error"),

        // The following is only for display purpose.
        Recomposing("rebuilding");

        private final String _description;

        Status(String description) {
            _description = description;
        }

        @Override
        public String toString() {
            return _description;
        }
    }

    public enum Error {
        UnknownError,
        CreateError,
        SysprepTimeout,
        JoinDomainError,
        SnapshotError,
        PowerOnError,
        PowerOffError,
        RefreshError,
        DeleteError
    }

    public enum LogOffMode {
        DisconnectNow,
        Disconnect,
        LogOffNow,
        LogOff
    }

    long getUpdateTime();

    void setUpdateTime(long updateTime);

    String getName();

    void setName(String name);

    String getPoolUuid();

    void setPoolUuid(String poolUuid);

    String getDiskImageUuid();

    void setDiskImageUuid(String diskImageUuid);

    String getHostUuid();

    void setHostUuid(String hostUuid);

    String getStorageUuid();

    void setStorageUuid(String storageUuid);

    String getVmRefId();

    void setVmRefId(String vmRefId);

    boolean isCbrcEnabled();

    void setCbrcEnabled(boolean cbrcEnabled);

    String getDnsName();

    void setDnsName(String dnsName);

    String getIpAddress();

    void setIpAddress(String ipAddress);

    String getMacAddress();

    void setMacAddress(String macAddress);

    boolean isInDomin();

    void setInDomain(boolean inDomin);

    String getUserUuid();

    void setUserUuid(String userUuid);

    boolean isAssigned();

    void setAssigned(boolean assigned);

    LogOffMode getLogOffMode();

    void setLogOffMode(LogOffMode logOffMode);

    String getUserDataDiskOwner();

    void setUserDataDiskOwner(String userDataDiskOwner);

    long getLoginTime();

    void setLoginTime(long loginTime);

    Status getStatus();

    void setStatus(Status status);

    Error getLastError();

    void setLastError(Error error);

    boolean isRecompose();

    void setRecompose(boolean recompose);

    public interface Service extends PersistenceService<VmInstance>, ObjectPropertyChangeNotifier {
        public static final String BEAN_NAME = "vmInstanceService";

        Long countByHost(String hostUuid);

        Long countByPool(String poolUuid);

        Long countByPoolAndStatus(String poolUuid, Status... status);

        Long countByPoolAndStatusOnHost(String hostUuid, String poolUuid, Status... status);

        Long countLocalByPoolAndStatus(String poolUuid, Status... status);

        Long countAllInSession();

        Long countLocalByStatus(Status... status);

        Long countByStatus(Status... status);

        Long countByStatusOnHost(String hostUuid, Status... status);

        List<? extends VmInstance> getAllByPool(String poolUuid, Pagination pagination);

        List<? extends VmInstance> getUnassignedByPool(String poolUuid, Pagination pagination);

        List<? extends VmInstance> getAllInPoolByUser(String poolUuid, String userUuid);

        List<? extends VmInstance> getAllInPoolWithNoUser(String poolUuid);

        List<? extends VmInstance> getByStatus(Pagination pagination, Status... status);

        List<? extends VmInstance> getByPoolAndStatus(Pagination pagination, String poolUuid, Status... status);

        List<? extends VmInstance> getByPoolAndStatusOnHost(String hostUuid, String poolUuid, Status... status);

        List<? extends VmInstance> getLocalByPoolAndStatus(String poolUuid, Status... status);

        List<? extends VmInstance> getLocalVmInstances();

        List<? extends VmInstance> getLocalVmInstancesByPool(String poolUuid);

        VmInstance getLocalVmInstanceByVmRefId(String vmRefId);

        List<? extends VmInstance> getVmInstancesOnHost(String hostUuid);

        boolean isLocalVmInstance(VmInstance vmInstance);

        boolean isOrphanedVmInstance(VmInstance vmInstance);

        void invalidateNonLocalVmInstances();

        DiskImage getBaseDiskImage(VmInstance vmInstance);

        void refreshVm(Pool pool, VmInstance vmInstance, AsyncCallback callback);

        void powerOnVm(Pool pool, VmInstance vmInstance, AsyncCallback callback);

        void powerOffVm(Pool pool, VmInstance vmInstance, AsyncCallback callback);

        void deleteVm(Pool pool, VmInstance vmInstance, AsyncCallback callback);

        void unassignVm(VmInstance vmInstance, AsyncCallback callback);
    }
}

