/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.model;

import java.util.List;

import com.nimdesk.service.ObjectPropertyChangeNotifier;
import com.nimdesk.service.PersistenceService;

public interface UserSession extends Base {

    String getUserUuid();

    void setUserUuid(String userUuid);

    String getVmInstanceUuid();

    void setVmInstanceUuid(String vmInstanceUuid);

    String getPoolName();

    void setPoolName(String poolName);

    String getVmInstanceName();

    void setVmInstanceName(String vmInstanceName);

    String getVmInstanceDns();

    void setVmInstanceDns(String vmInstanceDns);

    String getVmInstanceIp();

    void setVmInstanceIp(String vmInstanceIp);

    long getStartTime();

    void setStartTime(long startTime);

    long getEndTime();

    void setEndTime(long endTime);

    String getHostUuid();

    void setHostUuid(String hostUuid);

    public interface Service extends PersistenceService<UserSession>, ObjectPropertyChangeNotifier {
        public static final String BEAN_NAME = "userSessionService";

        void startUserSession(String userUuid, Pool pool, VmInstance vmInstance);

        void completeUserSession(VmInstance vmInstance);

        List<? extends UserSession> getUserSessionsByHost(String hostUuid);

        List<? extends UserSession> getUserSessionsByHostAndTime(String hostUuid, boolean before, long time);

        List<? extends UserSession> getLocalUserSessions();

        List<? extends UserSession> getLocalUserSessionsByTime(boolean before, long time);

        boolean isLocalUserSession(UserSession userSession);

        void invalidateLocalActiveSessions();

        void deleteOldUserSessions(long time);
    }
}
