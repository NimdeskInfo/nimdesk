/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.model;

import java.util.List;

import com.nimdesk.service.ObjectPropertyChangeNotifier;
import com.nimdesk.service.PersistenceService;

/**
 * This class represents one pool assignment.
 */
public interface PoolUser extends Base {

    /**
     * The uuid of the pool.
     */
    String getPoolUuid();

    void setPoolUuid(String poolUuid);

    /**
     * This uuid of the user.
     */
    String getUserUuid();

    void setUserUuid(String userUuid);

    public interface Service extends PersistenceService<PoolUser>, ObjectPropertyChangeNotifier {
        public static final String BEAN_NAME = "poolUserService";

        List<? extends PoolUser> getByPool(String poolUuid);

        List<? extends PoolUser> getByUser(String userUuid);

        void deleteByPool(String poolUuid);

        void deleteByUser(String userUuid);

        void assignUserToPool(User user, String poolUuid);

        void unassignUserFromPool(User user, String poolUuid);
    }
}
