/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.ui.server;

import java.util.Date;

import com.nimdesk.model.VmImage;

public class ImageSummary {
    private VmImage _vmImage;
    private String _updatingHost = null;
    private String _updatingDiskImageName = null;
    private Object _data = null;

    public ImageSummary(VmImage vmImage) {
        _vmImage = vmImage;
    }

    public VmImage getVmImage() {
        return _vmImage;
    }

    public void setVmImage(VmImage vmImage) {
        _vmImage = vmImage;
    }

    public String getUuid() {
        return _vmImage.getUuid();
    }

    public String getName() {
        return _vmImage.getName();
    }

    public long getVersion() {
        return _vmImage.getVersion();
    }

    public String getDescription() {
        return _vmImage.getDescription();
    }

    public String getOperatingSystem() {
        return _vmImage.getOsName();
    }

    public VmImage.Status getStatus() {
        return _vmImage.getStatus();
    }

    public float getSizeGB() {
        return _vmImage.getImageSizeInKB() / (1024f * 1024f);
    }

    public Date getUpdateTime() {
        return new Date(_vmImage.getUpdateTime());
    }

    public boolean isReady() {
        return _vmImage.getStatus() == VmImage.Status.Ready;
    }

    public boolean isUpdating() {
        return _vmImage.getStatus() == VmImage.Status.Updating;
    }

    public String getUpdatingHost() {
        return _updatingHost;
    }

    public void setUpdatingHost(String updatingHost) {
        _updatingHost = updatingHost;
    }

    public String getUpdatingDiskImageName() {
        return _updatingDiskImageName;
    }

    public void setUpdatingDiskImageName(String updatingDiskImageName) {
        _updatingDiskImageName = updatingDiskImageName;
    }

    public String getDiskImageIp() {
        return _vmImage.getUpdatingDiskImageIp();
    }

    public Object getData() {
        return _data;
    }

    public void setData(Object data) {
        _data = data;
    }
}

