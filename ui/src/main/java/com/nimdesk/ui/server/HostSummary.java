/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.ui.server;

import java.util.List;

import com.nimdesk.model.Host;
import com.nimdesk.model.HypervisorType;
import com.nimdesk.model.Server;
import com.nimdesk.model.Storage;

public class HostSummary {
    private Host _host;
    private final Server _server;
    private final List<? extends Storage> _storages;
    private Object _data = null;

    public HostSummary(Host host, Server server, List<? extends Storage> storages) {
        _host = host;
        _server = server;
        _storages = storages;
    }

    public Host getHost() {
        return _host;
    }

    public void setHost(Host host) {
        _host = host;
    }

    public Server getServer() {
        return _server;
    }

    public List<? extends Storage> getStorages() {
        return _storages;
    }

    public String getUuid() {
        return _host.getUuid();
    }

    public HypervisorType getType() {
        return _host.getType();
    }

    public Host.Status getStatus() {
        return _host.getStatus();
    }

    public String getHostName() {
        return String.format("%s(%s)", _host.getAddress(), _host.getName());
    }

    public String getName() {
        return _host.getName();
    }

    public String getAddress() {
        return _host.getAddress();
    }

    public String getServerAddress() {
        return (_server != null) ? _server.getPublicAddress() : "";
    }

    public String getVCenterIp() {
        return _host.getVCenterIp();
    }

    public int getCpuCapacityMHz() {
        return _host.getCpuCapacityMHz();
    }

    public int getMemoryCapacityMB() {
        return _host.getMemoryCapacityMB();
    }

    public String getUsername() {
        return _host.getUsername();
    }

    public String getVmNetwork() {
        return _host.getVmNetwork();
    }

    public boolean isCbrcEnabled() {
        return _host.isCbrcEnabled();
    }

    public Object getData() {
        return _data;
    }

    public void setData(Object data) {
        _data = data;
    }
}
