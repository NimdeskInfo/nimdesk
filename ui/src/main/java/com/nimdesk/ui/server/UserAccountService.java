/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.ui.server;

import java.util.List;

import com.nimdesk.event.AsyncCallback;
import com.nimdesk.ldap.LdapContext;
import com.nimdesk.ldap.LdapUser;
import com.nimdesk.model.Pool;
import com.nimdesk.model.User;
import com.nimdesk.model.UserSession;

public interface UserAccountService {
    public static final String BEAN_NAME = "userAccountService";

    void listDomains(AsyncCallback callback);

    void connectLdapService(String providerUrl, String domain, String username, String password,
            AsyncCallback callback);

    void findLdapUsers(LdapContext context, boolean includeUsers, boolean includeGroups, String prefix,
            AsyncCallback callback);

    void authenticateLdaUser(LdapContext context, String username, String password, AsyncCallback callback);

    void authenticateWorkGroupUser(String username, String password, AsyncCallback callback);

    User syncLdapUserWithDb(LdapUser ldapUser);

    void getUsers(int first, int max, String orderBy, boolean ascending,
            AsyncCallback callback);

    void getUsersForPool(Pool pool, AsyncCallback callback);

    void updateUsersForPool(Pool pool, List<UserSummary> entitledUsers, List<UserSummary> distitledUsers,
            AsyncCallback callback);

    void getUser(String username, String domain, AsyncCallback callback);

    void createLocalUser(LocalUserSpec localUserSpec, AsyncCallback callback);

    void updateLocalUser(LocalUserSpec localUserSpec, AsyncCallback callback);

    void deleteUser(User user, AsyncCallback callback);

    void getPoolsForUser(User user, AsyncCallback callback);

    void getPoolsForUserWithGroups(List<LdapUser> userWithGroups, AsyncCallback callback);

    UserSessionSummary createUserSessionSummary(UserSession userSession);

    void getAllUserSessions(int first, int max, String orderBy, boolean ascending,
            AsyncCallback callback);
}
