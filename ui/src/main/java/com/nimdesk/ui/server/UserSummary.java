/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.ui.server;

import org.apache.commons.lang.StringUtils;

import com.nimdesk.model.User;

public class UserSummary {
    private User _user;
    private Object _data = null;

    public UserSummary(User user) {
        _user = user;
    }

    public User getUser() {
        return _user;
    }

    public void setUser(User user) {
        _user = user;
    }

    public String getUuid() {
        return _user.getUuid();
    }

    public User.Type getType() {
        return _user.getType();
    }

    public String getName() {
        return _user.getName();
    }

    public String getLogonName() {
        if (User.Type.User == _user.getType()) {
            return _user.getLogonName();
        } else {
            if (StringUtils.equals(_user.getName(), _user.getLogonName())) {
                return "";
            } else {
                return _user.getLogonName();
            }
        }
    }

    public String getDomain() {
        return _user.getDomain();
    }

    public String getDistinguishedName() {
        return _user.getDistinguishedName();
    }

    public Object getData() {
        return _data;
    }

    public void setData(Object data) {
        _data = data;
    }
}
