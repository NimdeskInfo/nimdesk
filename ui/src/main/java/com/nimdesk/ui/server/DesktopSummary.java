/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.ui.server;

import java.util.Date;

import org.apache.commons.lang.StringUtils;

import com.nimdesk.model.Host;
import com.nimdesk.model.Pool;
import com.nimdesk.model.Storage;
import com.nimdesk.model.User;
import com.nimdesk.model.VmInstance;

public class DesktopSummary {
    private VmInstance _vmInstance;
    private Pool _pool;
    private Host _host;
    private Storage _storage;
    private User _user;
    private Object _data = null;

    public DesktopSummary(VmInstance vmInstance, User user, Pool pool, Host host, Storage storage) {
        _vmInstance = vmInstance;
        _user = user;
        _pool = pool;
        _host = host;
        _storage = storage;
    }

    public VmInstance getVmInstance() {
        return _vmInstance;
    }

    public void setVmInstance(VmInstance vmInstance) {
        _vmInstance = vmInstance;
    }

    public User getUser() {
        return _user;
    }

    public void setUser(User user) {
        _user = user;
    }

    public Pool getPool() {
        return _pool;
    }

    public void setPool(Pool pool) {
        _pool = pool;
    }

    public Host getHost() {
        return _host;
    }

    public void setHost(Host host) {
        _host = host;
    }

    public Storage getStorage() {
        return _storage;
    }

    public void setStorage(Storage storage) {
        _storage = storage;
    }

    public String getUuid() {
        return _vmInstance.getUuid();
    }

    public String getName() {
        return _vmInstance.getName();
    }

    public Date getUpdateTime() {
        return new Date(_vmInstance.getUpdateTime());
    }

    public String getDnsName() {
        return _vmInstance.getDnsName();
    }

    public String getIpAddress() {
        String ipAddress = _vmInstance.getIpAddress();

        if (!StringUtils.isEmpty(ipAddress)) {
            return ipAddress;
        } else {
            return _vmInstance.getMacAddress();
        }
    }

    public String getDomain() {
        return (_pool != null) ? _pool.getDomainOrWorkGroup() : "";
    }

    public String getUserName() {
        if (_user == null) {
            return "";
        }

        if (StringUtils.isEmpty(_user.getDomain())) {
            return _user.getLogonName();
        } else {
            return _user.getLogonName() + "@" + _user.getDomain();
        }
    }

    public Date getLoginTime() {
        long loginTime = _vmInstance.getLoginTime();
        return (loginTime > 0) ? new Date(loginTime) : null;
    }

    public VmInstance.Status getStatus() {
        VmInstance.Status status = _vmInstance.getStatus();
        if (VmInstance.Status.Destroying == status && _vmInstance.isRecompose()) {
            status = VmInstance.Status.Recomposing;
        }

        return status;
    }

    public String getPoolName() {
        return (_pool != null) ? _pool.getName() : "";
    }

    public String getHostName() {
        if (_host == null) {
            return "";
        }

        if (_storage != null) {
            return String.format("%s/%s", _host.getAddress(), _storage.getName());
        } else {
            return _host.getAddress();
        }
    }

    public Object getData() {
        return _data;
    }

    public void setData(Object data) {
        _data = data;
    }
}
