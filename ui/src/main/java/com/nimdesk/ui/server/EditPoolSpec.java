/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.ui.server;

import com.nimdesk.model.Pool;

public class EditPoolSpec {
    private String _poolUuid;
    private String _name = null;
    private Pool.Type _type = null;
    private String _description = "";
    private boolean _enabled = true;
    private Long _minSize = null;
    private Long _maxSize = null;
    private boolean _supportRdp = true;
    private boolean _redirectDrives = false;
    private boolean _redirectPrinters = false;
    private boolean _redirectComPorts = false;
    private boolean _redirectSmartCards = false;

    public String getPoolUuid() {
        return _poolUuid;
    }

    public void setPoolUuid(String poolUuid) {
        _poolUuid = poolUuid;
    }

    public String getName() {
        return _name;
    }

    public void setName(String name) {
        _name = name;
    }

    public Pool.Type getType() {
        return _type;
    }

    public void setType(Pool.Type type) {
        _type = type;
    }

    public String getDescription() {
        return _description;
    }

    public void setDescription(String description) {
        _description = description;
    }

    public boolean isEnabled() {
        return _enabled;
    }

    public void setEnabled(boolean enabled) {
        _enabled = enabled;
    }

    public Long getMinSize() {
        return _minSize;
    }

    public void setMinSize(Long minSize) {
        _minSize = minSize;
    }

    public Long getMaxSize() {
        return _maxSize;
    }

    public void setMaxSize(Long maxSize) {
        _maxSize = maxSize;
    }

    public boolean isSupportRdp() {
        return _supportRdp;
    }

    public void setSupportRdp(boolean supportRdp) {
        _supportRdp = supportRdp;
    }

    public boolean isRedirectDrives() {
        return _redirectDrives;
    }

    public void setRedirectDrives(boolean redirectDrives) {
        _redirectDrives = redirectDrives;
    }

    public boolean isRedirectPrinters() {
        return _redirectPrinters;
    }

    public void setRedirectPrinters(boolean redirectPrinters) {
        _redirectPrinters = redirectPrinters;
    }

    public boolean isRedirectComPorts() {
        return _redirectComPorts;
    }

    public void setRedirectComPorts(boolean redirectComPorts) {
        _redirectComPorts = redirectComPorts;
    }

    public boolean isRedirectSmartCards() {
        return _redirectSmartCards;
    }

    public void setRedirectSmartCards(boolean redirectSmartCards) {
        _redirectSmartCards = redirectSmartCards;
    }
}
