/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.ui.server;

import java.util.Collection;

import com.nimdesk.model.HypervisorType;
import com.nimdesk.vm.HostAsset;
import com.nimdesk.vm.NetworkAsset;
import com.nimdesk.vm.StorageAsset;

public class ServerConfigSpec {

    private HypervisorType _hypervisorType;
    private String _hypervisorServer;
    private String _hypervisorUsername;
    private String _hypervisorPassword;
    private boolean _hypervisorCacheEnabled = true;

    /*
    private String _ldapServer;
    private boolean _ldapSsl;
    private String _ldapDomain;
    private String _ldapUsername;
    private String _ldapPassword;
    */

    private HostAsset _hostMO;
    private Collection<StorageAsset> _datastoreMOs;
    private NetworkAsset _networkMO;

    private String _serverPublicAddress;
    private String _serverPrivateAddress;

    private String _clusterName;
    private String _clusterId = null;
    private boolean _existingCluster = false;
    private String _peerAddress;
    private String _peerUsername;
    private String _peerPassword;

    private StorageAsset _sharedStorage;
    private boolean _sharedStorageForAll = false;

    private String _vCenterAddress;
    private String _vCenterUsername;
    private String _vCenterPassword;

    /**
     * The following methods are for hypervisor
     */
    public HypervisorType getHypervisorType() {
        return _hypervisorType;
    }

    public void setHypervisorType(HypervisorType type) {
        _hypervisorType = type;
    }

    public String getHypervisorServer() {
        return _hypervisorServer;
    }

    public void setHypervisorServer(String server) {
        _hypervisorServer = server;
    }

    public String getHypervisorUsername() {
        return _hypervisorUsername;
    }

    public void setHypervisorUsername(String username) {
        _hypervisorUsername = username;
    }

    public String getHypervisorPassword() {
        return _hypervisorPassword;
    }

    public void setHypervisorPassword(String password) {
        _hypervisorPassword = password;
    }

    public boolean getHypervisorCacheEnabled() {
        return _hypervisorCacheEnabled;
    }

    public void setHypervisorCacheEnabled(boolean hypervisorCacheEnabled) {
        _hypervisorCacheEnabled = hypervisorCacheEnabled;
    }

    /**
     * The following methods are for ldap
     */
    /*
    public String getLdapServer() {
        return _ldapServer;
    }

    public void setLdapServer(String ldapServer) {
        _ldapServer = ldapServer;
    }

    public boolean isLdapSsl() {
        return _ldapSsl;
    }

    public void setLdapSsl(boolean ldapSsl) {
        _ldapSsl = ldapSsl;
    }

    public String getLdapDomain() {
        return _ldapDomain;
    }

    public void setLdapDomain(String domain) {
        _ldapDomain = domain;
    }

    public String getLdapUsername() {
        return _ldapUsername;
    }

    public void setLdapUsername(String username) {
        _ldapUsername = username;
    }

    public String getLdapPassword() {
        return _ldapPassword;
    }

    public void setLdapPassword(String password) {
        _ldapPassword = password;
    }
    */

    /**
     * The following methods are for host assets
     */
    public HostAsset getHostMO() {
        return _hostMO;
    }

    public void setHostMO(HostAsset hostMO) {
        _hostMO = hostMO;
    }

    public Collection<StorageAsset> getDatastoreMOs() {
        return _datastoreMOs;
    }

    public void setDatastoreMOs(Collection<StorageAsset> datastoreMOs) {
        _datastoreMOs = datastoreMOs;
    }

    public NetworkAsset getNetworkMO() {
        return _networkMO;
    }

    public void setNetworkMO(NetworkAsset networkMO) {
        _networkMO = networkMO;
    }

    /**
     * The following methods are for server
     */
    public String getServerPublicAddress() {
        return _serverPublicAddress;
    }

    public void setServerPublicAddress(String serverPublicAddress) {
        _serverPublicAddress = serverPublicAddress;
    }

    public String getServerPrivateAddress() {
        return _serverPrivateAddress;
    }

    public void setServerPrivateAddress(String serverPrivateAddress) {
        _serverPrivateAddress = serverPrivateAddress;
    }

    /**
     * The following methods are for cluster
     */
    public String getClusterName() {
        return _clusterName;
    }

    public void setClusterName(String clusterName) {
        _clusterName = clusterName;
    }

    public String getClusterId() {
        return _clusterId;
    }

    public void setClusterId(String clusterId) {
        _clusterId = clusterId;
    }

    public boolean isExistingCluster() {
        return _existingCluster;
    }

    public void setExistingCluster(boolean existingCluster) {
        _existingCluster = existingCluster;
    }

    public String getPeerAddress() {
        return _peerAddress;
    }

    public void setPeerAddress(String peerAddress) {
        _peerAddress = peerAddress;
    }

    public String getPeerUsername() {
        return _peerUsername;
    }

    public void setPeerUsername(String peerUsername) {
        _peerUsername = peerUsername;
    }

    public String getPeerPassword() {
        return _peerPassword;
    }

    public void setPeerPassword(String peerPassword) {
        _peerPassword = peerPassword;
    }

    public StorageAsset getSharedStorage() {
        return _sharedStorage;
    }

    public void setSharedStorage(StorageAsset sharedStorage) {
        _sharedStorage = sharedStorage;
    }

    public boolean isSharedStorageForAll() {
        return _sharedStorageForAll;
    }

    public void setSharedStorageForAll(boolean sharedStorageForAll) {
        _sharedStorageForAll = sharedStorageForAll;
    }

    public String getVCenterAddress() {
        return _vCenterAddress;
    }

    public void setVCenterAddress(String vCenterAddress) {
        _vCenterAddress = vCenterAddress;
    }

    public String getVCenterUsername() {
        return _vCenterUsername;
    }

    public void setVCenterUsername(String vCenterUsername) {
        _vCenterUsername = vCenterUsername;
    }

    public String getVCenterPassword() {
        return _vCenterPassword;
    }

    public void setVCenterPassword(String vCenterPassword) {
        _vCenterPassword = vCenterPassword;
    }
}
