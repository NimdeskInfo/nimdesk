/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.ui.server;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

public class ClusterDesktopSummary {
    public enum DesktopStatus {
        unknown, inSession, ready, provisioning, error
    }

    public static class Counts {
        private String _hostName = "";
        private String _hostUuid = "";
        private long _totalCount = 0;
        private long _inSessionCount = 0;
        private long _readyCount = 0;
        private long _provisioningCount = 0;
        private long _errorCount = 0;

        public String getHostName() {
            return _hostName;
        }

        public void setHostName(String hostName) {
            _hostName = hostName;
        }

        public String getHostUuid() {
            return _hostUuid;
        }

        public void setHostUuid(String hostUuid) {
            _hostUuid = hostUuid;
        }

        public long getTotalCount() {
            return _totalCount;
        }

        public void setTotalCount(long totalCount) {
            _totalCount = totalCount;
        }

        public void updateTotalCount(long delta) {
            _totalCount += delta;
        }

        public long getInSessionCount() {
            return _inSessionCount;
        }

        public void setInSessionCount(long inSessionCount) {
            _inSessionCount = inSessionCount;
        }

        public void updateInSessionCount(long delta) {
            _inSessionCount += delta;
        }

        public long getReadyCount() {
            return _readyCount;
        }

        public void setReadyCount(long readyCount) {
            _readyCount = readyCount;
        }

        public void updateReadyCount(long delta) {
            _readyCount += delta;
        }

        public long getProvisioningCount() {
            return _provisioningCount;
        }

        public void setProvisioningCount(long provisioningCount) {
            _provisioningCount = provisioningCount;
        }

        public void updateProvisioningCount(long delta) {
            _provisioningCount += delta;
        }

        public long getErrorCount() {
            return _errorCount;
        }

        public void setErrorCount(long errorCount) {
            _errorCount = errorCount;
        }

        public void updateErrorCount(long delta) {
            _errorCount += delta;
        }
    }

    private Counts _totalCount = new Counts();
    private Map<String, Counts> _hostCountsUuidMap = null;
    private List<Counts> _hostCountsList = null;

    public ClusterDesktopSummary() {
    }

    public Counts getTotalCount() {
        return _totalCount;
    }

    public void setTotalCount(Counts totalCount) {
        _totalCount = totalCount;
    }

    public Map<String, Counts> getHostCountsMap() {
        return _hostCountsUuidMap;
    }

    public void setHostCountsMap(Map<String, Counts> hostCountsUuidMap) {
        _hostCountsUuidMap = hostCountsUuidMap;

        _hostCountsList = new ArrayList<Counts>();
        for (Counts counts : hostCountsUuidMap.values()) {
            _hostCountsList.add(counts);
        }

        Collections.sort(_hostCountsList, new Comparator<Counts>() {
            @Override
            public int compare(Counts o1, Counts o2) {
                return o1.getHostName().compareToIgnoreCase(o2.getHostName());
            }
        });
    }

    public List<Counts> getHostCountsList() {
        return _hostCountsList;
    }

    public static void copy(ClusterDesktopSummary copyTo, ClusterDesktopSummary copyFrom) {
        if (copyTo == null || copyFrom == null) {
            return;
        }

        copyTo.setTotalCount(copyFrom.getTotalCount());
        copyTo.setHostCountsMap(copyFrom.getHostCountsMap());
    }
}
