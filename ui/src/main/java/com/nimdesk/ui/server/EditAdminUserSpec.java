/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.ui.server;

import com.nimdesk.model.AdminUser;

public class EditAdminUserSpec {
    private final AdminUser _adminUser;
    private String _username = null;
    private String _oldPassword = null;
    private String _password = null;
    private String _password2 = null;
    private String _fullName = null;
    private String _email = null;
    private boolean _notify = true;

    public EditAdminUserSpec() {
        this(null);
    }

    public EditAdminUserSpec(AdminUser adminUser) {
        _adminUser = adminUser;

        if (_adminUser != null) {
            _username = _adminUser.getUsername();
            _fullName = _adminUser.getFullName();
            _email = _adminUser.getEmail();
            _notify = _adminUser.isNotify();
        }
    }

    public AdminUser getAdminUser() {
        return _adminUser;
    }

    public String getUsername() {
        return _username;
    }

    public void setUsername(String username) {
        _username = username;
    }

    public String getOldPassword() {
        return _oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        _oldPassword = oldPassword;
    }

    public String getPassword() {
        return _password;
    }

    public void setPassword(String password) {
        _password = password;
    }

    public String getPassword2() {
        return _password2;
    }

    public void setPassword2(String password2) {
        _password2 = password2;
    }

    public String getFullName() {
        return _fullName;
    }

    public void setFullName(String fullName) {
        _fullName = fullName;
    }

    public String getEmail() {
        return _email;
    }

    public void setEmail(String email) {
        _email = email;
    }

    public boolean isNotify() {
        return _notify;
    }

    public void setNotify(boolean notify) {
        _notify = notify;
    }
}
