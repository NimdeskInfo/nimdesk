/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.ui.server;

import org.apache.commons.lang.StringUtils;

import com.nimdesk.model.AdminUser;

public class AdminUserSummary {
    private final AdminUser _adminUser;

    public AdminUserSummary(AdminUser adminUser) {
        _adminUser = adminUser;
    }

    public AdminUser getAdminUser() {
        return _adminUser;
    }

    public String getUsername() {
        return _adminUser.getUsername();
    }

    public String getFullName() {
        return _adminUser.getFullName();
    }

    public String getEmail() {
        return _adminUser.getEmail();
    }

    public boolean isNotify() {
        if (StringUtils.isEmpty(_adminUser.getEmail())) {
            return false;
        }

        return _adminUser.isNotify();
    }
}
