/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.ui.server;

import java.util.List;
import java.util.Properties;

import com.nimdesk.event.AsyncCallback;
import com.nimdesk.model.AdminUser;
import com.nimdesk.model.Host;
import com.nimdesk.model.LdapServer;
import com.nimdesk.model.Server;

public interface AdminService {
    public static final String BEAN_NAME = "adminService";

    void verifyJoiningCluster(String peerAddress, String peerUsername, String peerPassword, Properties properties, AsyncCallback callback);

    List<AdminUserSummary> getAdminUsers();

    boolean validateAdminUserPassword(AdminUser adminUser, String password);

    void updateAdminUser(EditAdminUserSpec editAdminUserSpec, AsyncCallback callback);

    void deleteAdminUser(AdminUser adminUser, AsyncCallback callback);

    ServerSummary getLocalServer();

    void setServerConfig(ServerConfigSpec serverConfig);

    void getSystemSummary(AsyncCallback callback);

    void getClusterDesktopSummary(AsyncCallback callback);

    List<ServerSummary> getServers();

    ServerSummary createServerSummary(Server server);

    void maintainServer(Server server, boolean enter, AsyncCallback callback);

    //void removeServer(Server server, AsyncCallback callback);

    void disjoinCluster(Server server, AsyncCallback callback);

    List<HostSummary> getHosts();

    HostSummary createHostSummary(Host host);

    void updateHost(EditHostSpec editHostSpec, AsyncCallback callback);

    List<? extends LdapServer> getLdapServers();

    LdapServer addLdapServer(String ldapServer, int port, boolean ssl, String ldapDomain, String ldapUsername, String ldapPassword);

    void updateLdapServer(LdapServer ldapServer);

    void deleteLdapServer(LdapServer ldapServer);
}
