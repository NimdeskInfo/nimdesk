/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.ui.server;

import java.util.Date;

import com.nimdesk.model.Task;

public class TaskSummary {
    private final Task _task;
    private final String _type;
    private Object _data = null;

    public TaskSummary(Task task, String type) {
        _task = task;
        _type = type;
    }

    public Task getTask() {
        return _task;
    }

    public String getUuid() {
        return _task.getUuid();
    }

    public String getName() {
        return _task.getType().toString();
    }

    public String getTarget() {
        if (_type != null) {
            if (_type.equals(_task.getTargetType())) {
                return _task.getTargetName();
            } else if (_type.equals(_task.getAssociateType())) {
                return _task.getAssociateName();
            }
        }

        return _task.getTargetName();
    }

    public Task.Status getStatus() {
        return _task.getStatus();
    }

    public String getDetails() {
        return _task.getDetails();
    }

    public Date getStartTime() {
        long time = _task.getStartTime();
        return (time == 0) ? null : new Date(time);
    }

    public Date getCompletedTime() {
        long time = _task.getEndTime();
        return (time == 0) ? null : new Date(time);
    }

    public Object getData() {
        return _data;
    }

    public void setData(Object data) {
        _data = data;
    }
}
