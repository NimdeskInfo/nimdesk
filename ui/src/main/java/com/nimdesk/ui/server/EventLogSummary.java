/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.ui.server;

import java.util.Date;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;

import com.nimdesk.model.EventLog;
import com.nimdesk.model.EventLog.Severity;

public class EventLogSummary {
    private final EventLog _eventLog;
    private String _description = null;

    public EventLogSummary(EventLog eventLog) {
        _eventLog = eventLog;
    }

    public EventLog getEventLog() {
        return _eventLog;
    }

    public String getUuid() {
        return _eventLog.getUuid();
    }

    public String getDescription() {
        if (_description != null) {
            return _description;
        }

        String templateId = _eventLog.getDescriptionTemplate();
        _description = EventLog.TEMPLATE_MAP.get(templateId);
        if (_description == null) {
            _description = templateId;
        }

        Properties params = _eventLog.retrieveParameters();

        for (Map.Entry<Object, Object> entry : params.entrySet()) {
            if (!(entry.getKey() instanceof String) || !(entry.getValue() instanceof String)) {
                continue;
            }

            String key = (String) entry.getKey();
            String value = (String) entry.getValue();

            if (StringUtils.isEmpty(value)) {
                if ("user".equals(key)) {
                    _description = StringUtils.replace(_description, "${user}", "(system)");
                }
            } else {
                _description = StringUtils.replace(_description, "${" + key + "}", value);
            }
        }

        return _description;
    }

    public Severity getSeverity() {
        return _eventLog.getSeverity();
    }

    public Date getDateTime() {
        return new Date(_eventLog.getDateTime());
    }

    public String getTarget() {
        return _eventLog.getTargetName();
    }

    public String getUser() {
        return _eventLog.getUser();
    }
}
