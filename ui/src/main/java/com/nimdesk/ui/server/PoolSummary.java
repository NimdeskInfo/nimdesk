/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.ui.server;

import java.util.Date;

import com.nimdesk.model.Pool;
import com.nimdesk.model.VmImage;

public class PoolSummary {
    private Pool _pool;
    private long _poolVersion = 0;
    private VmImage _vmImage;
    private Object _data = null;

    public PoolSummary(Pool pool, long poolVersion, VmImage vmImage) {
        _pool = pool;
        _poolVersion = poolVersion;
        _vmImage = vmImage;
    }

    public Pool getPool() {
        return _pool;
    }

    public void setPool(Pool pool) {
        _pool = pool;
    }

    public VmImage getImage() {
        return _vmImage;
    }

    public void setImage(VmImage vmImage) {
        _vmImage = vmImage;
    }

    public long getPoolVersion() {
        return _poolVersion;
    }

    public void setPoolVersion(long poolVersion) {
        _poolVersion = poolVersion;
    }

    public String getUuid() {
        return _pool.getUuid();
    }

    public String getName() {
        return _pool.getName();
    }

    public Pool.Type getType() {
        return _pool.getType();
    }

    public String getDescription() {
        return _pool.getDescription();
    }

    public String getOsName() {
        return _pool.getOsName();
    }

    public boolean isEnabled() {
        return _pool.isEnabled();
    }

    public Pool.Status getStatus() {
        return _pool.getStatus();
    }

    public Date getUpdateTime() {
        return new Date(_pool.getUpdateTime());
    }

    public String getDomain() {
        return _pool.isDomain() ? _pool.getDomainOrWorkGroup() : "";
    }

    public long getCpuCount() {
        return _pool.getCpuCount();
    }

    public long getMemoryMB() {
        return _pool.getMemoryMB();
    }

    public long getMaxSize() {
        return _pool.getMaxSize();
    }

    public long getMinSize() {
        return _pool.getMinSize();
    }

    public long getCurrentSize() {
        return 0;
    }

    public String getImageName() {
        return (_vmImage != null) ? _vmImage.getName() : "Invalid";
    }

    public Object getData() {
        return _data;
    }

    public void setData(Object data) {
        _data = data;
    }
}

