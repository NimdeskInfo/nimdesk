/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.ui.server;

import java.io.Serializable;

import com.nimdesk.vm.ImportableVm;

public class ImportVmSpec implements Serializable {

    private static final long serialVersionUID = -460278879925613996L;

    private ImportableVm _vm = null;
    private String _imageName = null;
    private String _imageDescription = "";

    public ImportableVm getVm() {
        return _vm;
    }

    public void setVm(ImportableVm vm) {
        _vm = vm;
    }

    public String getImageName() {
        return _imageName;
    }

    public void setImageName(String imageName) {
        _imageName = imageName;
    }

    public String getImageDescription() {
        return _imageDescription;
    }

    public void setImageDescription(String imageDescription) {
        _imageDescription = imageDescription;
    }

    public String getVmName() {
        return _vm.getVmName();
    }

    public String getVmRefId() {
        return _vm.getVmRefId();
    }

    public String getVmOsName() {
        return _vm.getOsName();
    }

    public long getImageSizeInKB() {
        return _vm.getDiskCapacityInKB();
    }
}

