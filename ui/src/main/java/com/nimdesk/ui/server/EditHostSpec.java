/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.ui.server;

import com.nimdesk.model.Host;

public class EditHostSpec {
    private final Host _host;
    private String _username = null;
    private String _password = null;
    private boolean _cbrcEnabled = false;

    public EditHostSpec(Host host) {
        _host = host;

        if (_host != null) {
            _username = _host.getUsername();
            _password = _host.getPassword();
            _cbrcEnabled = _host.isCbrcEnabled();
        }
    }

    public Host getHost() {
        return _host;
    }

    public String getAddress() {
        return _host.getAddress();
    }

    public String getUsername() {
        return _username;
    }

    public void setUsername(String username) {
        _username = username;
    }

    public String getPassword() {
        return _password;
    }

    public void setPassword(String password) {
        _password = password;
    }

    public boolean isCbrcEnabled() {
        return _cbrcEnabled;
    }

    public void setCbrcEnabled(boolean cbrcEnabled) {
        _cbrcEnabled = cbrcEnabled;
    }
}
