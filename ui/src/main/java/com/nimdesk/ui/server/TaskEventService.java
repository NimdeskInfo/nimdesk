/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.ui.server;

import com.nimdesk.event.AsyncCallback;

public interface TaskEventService {
    public static final String BEAN_NAME = "taskEventService";

    void getAllTasks(int first, int max, String orderBy, boolean ascending,
            AsyncCallback callback);

    void getAllPoolTasks(int first, int max, String orderBy, boolean ascending,
            AsyncCallback callback);

    void getPoolTasks(String poolUuid, int first, int max, String orderBy, boolean ascending,
            AsyncCallback callback);

    void getAllImageTasks(int first, int max, String orderBy, boolean ascending,
            AsyncCallback callback);

    void getImageTasks(String imageUuid, int first, int max, String orderBy, boolean ascending,
            AsyncCallback callback);

    void getAllDesktopTasks(int first, int max, String orderBy, boolean ascending,
            AsyncCallback callback);

    void getDesktopTasks(String desktopUuid, int first, int max, String orderBy, boolean ascending,
            AsyncCallback callback);

    void getAllEvents(int first, int max, String orderBy, boolean ascending,
            AsyncCallback callback);

    void getAllPoolEvents(int first, int max, String orderBy, boolean ascending,
            AsyncCallback callback);

    void getPoolEvents(String poolUuid, int first, int max, String orderBy, boolean ascending,
            AsyncCallback callback);

    void getAllImageEvents(int first, int max, String orderBy, boolean ascending,
            AsyncCallback callback);

    void getImageEvents(String imageUuid, int first, int max, String orderBy, boolean ascending,
            AsyncCallback callback);

    void getAllDesktopEvents(int first, int max, String orderBy, boolean ascending,
            AsyncCallback callback);

    void getDesktopEvents(String desktopUuid, int first, int max, String orderBy, boolean ascending,
            AsyncCallback callback);
}
