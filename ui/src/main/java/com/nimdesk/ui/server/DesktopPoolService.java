/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.ui.server;

import com.nimdesk.event.AsyncCallback;
import com.nimdesk.model.DiskImage;
import com.nimdesk.model.Pool;
import com.nimdesk.model.VmImage;
import com.nimdesk.model.VmInstance;

public interface DesktopPoolService {
    public static final String BEAN_NAME = "desktopPoolService";

    boolean validateImageName(String imageName);

    long getValidVmimageCount();

    void getVmImages(int first, int max, String orderBy, boolean ascending, AsyncCallback callback);

    ImageSummary createVmImageSummary(VmImage vmImage);

    void getDiskImagesByVmImage(String vmImageUuid, AsyncCallback callback);

    DiskImageSummary createDiskImageSummary(DiskImage diskImage);

    void importImageFromVm(ImportVmSpec importVmSpec, AsyncCallback callback);

    void editImage(EditImageSpec editImageSpec, AsyncCallback callback);

    void cloneUpdateImage(ImageSummary image, AsyncCallback callback);

    void cancelUpdateImage(ImageSummary image, AsyncCallback callback);

    void importUpdatedImage(ImageSummary image, AsyncCallback callback);

    void deleteImage(ImageSummary image, AsyncCallback callback);

    void recomposePoolsForImage(ImageSummary image, AsyncCallback callback);

    void revertImage(ImageSummary image, AsyncCallback callback);

    void retryDiskImageCopy(DiskImageSummary diskImage, AsyncCallback callback);

    boolean validatePoolName(String poolName);

    long getValidPoolCount();

    long getValidPoolDesktopCount();

    void getPools(int first, int max, String orderBy, boolean ascending, AsyncCallback callback);

    PoolSummary createPoolSummary(Pool pool, VmImage vmImage);

    void createPool(CreatePoolSpec createPoolSpec, AsyncCallback callback);

    void updatePool(EditPoolSpec editPoolSpec, AsyncCallback callback);

    void deletePool(PoolSummary pool, AsyncCallback callback);

    void recomposePool(PoolSummary pool, boolean force, AsyncCallback callback);

    void rebalancePool(PoolSummary pool, AsyncCallback callback);

    void getDesktops(int first, int max, String orderBy, boolean ascending,
            boolean includeDerived, AsyncCallback callback);

    void refreshDesktop(DesktopSummary desktop, AsyncCallback callback);

    void deleteDesktop(DesktopSummary desktop, boolean rebuild, AsyncCallback callback);

    void unassignDesktop(DesktopSummary desktop, AsyncCallback callback);

    void disconnectDesktop(VmInstance desktop, boolean logoff, boolean isNow, AsyncCallback callback);

    void repairDesktop(DesktopSummary desktop, AsyncCallback callback);
}

