/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.ui.server;

public class SystemSummary {
    public static class Counts {
        private long _greenCount = 0;
        private long _yellowCount = 0;
        private long _redCount = 0;

        public Counts(long green, long yellow, long red) {
            _greenCount = green;
            _yellowCount = yellow;
            _redCount = red;
        }

        public synchronized long getGreenCount() {
            return _greenCount;
        }

        public synchronized void setGreenCount(long greenCount) {
            _greenCount = greenCount;
        }

        public synchronized long getYellowCount() {
            return _yellowCount;
        }

        public synchronized void setYellowCount(long yellowCount) {
            _yellowCount = yellowCount;
        }

        public synchronized long getRedCount() {
            return _redCount;
        }

        public synchronized void setRedCount(long redCount) {
            _redCount = redCount;
        }
    }

    public static void changeGreenCount(Counts counts, long delta) {
        if (counts != null) {
            synchronized(counts) {
                counts._greenCount += delta;
                if (counts._greenCount < 0) {
                    counts._greenCount = 0;
                }
            }
        }
    }

    public static void changeYellowCount(Counts counts, long delta) {
        if (counts != null) {
            synchronized(counts) {
                counts._yellowCount += delta;
                if (counts._yellowCount < 0) {
                    counts._yellowCount = 0;
                }
            }
        }
    }

    public static void changeRedCount(Counts counts, long delta) {
        if (counts != null) {
            synchronized(counts) {
                counts._redCount += delta;
                if (counts._redCount < 0) {
                    counts._redCount = 0;
                }
            }
        }
    }

    private String _clusterName = "";
    private Counts _serverCount;
    private Counts _hypervisorCount;
    private Counts _imageCount;
    private Counts _poolCount;
    private Counts _desktopCount;
    private Counts _domainCount;
    private Counts _userSessionCount;

    public String getClusterName() {
        return _clusterName;
    }

    public void setClusterName(String clusterName) {
        _clusterName = clusterName;
    }

    public Counts getServerCounts() {
        return _serverCount;
    }

    public void setServerCounts(Counts serverCount) {
        _serverCount = serverCount;
    }

    public Counts getHypervisorCounts() {
        return _hypervisorCount;
    }

    public void setHypervisorCounts(Counts hypervisorCount) {
        _hypervisorCount = hypervisorCount;
    }

    public Counts getImageCounts() {
        return _imageCount;
    }

    public void setImageCounts(Counts imageCount) {
        _imageCount = imageCount;
    }

    public Counts getPoolCounts() {
        return _poolCount;
    }

    public void setPoolCounts(Counts poolCount) {
        _poolCount = poolCount;
    }

    public Counts getDesktopCounts() {
        return _desktopCount;
    }

    public void setDesktopCounts(Counts desktopCount) {
        _desktopCount = desktopCount;
    }

    public Counts getDomainCounts() {
        return _domainCount;
    }

    public void setDomainCounts(Counts domainCount) {
        _domainCount = domainCount;
    }

    public Counts getUserSessionCounts() {
        return _userSessionCount;
    }

    public void setUserSessionCounts(Counts userSessionCount) {
        _userSessionCount = userSessionCount;
    }

    public static void copy(SystemSummary copyTo, SystemSummary copyFrom) {
        if (copyTo == null || copyFrom == null) {
            return;
        }

        copyTo.setClusterName(copyFrom.getClusterName());
        copyTo.setServerCounts(copyFrom.getServerCounts());
        copyTo.setHypervisorCounts(copyFrom.getHypervisorCounts());
        copyTo.setImageCounts(copyFrom.getImageCounts());
        copyTo.setPoolCounts(copyFrom.getPoolCounts());
        copyTo.setDesktopCounts(copyFrom.getDesktopCounts());
        copyTo.setDomainCounts(copyFrom.getDomainCounts());
        copyTo.setUserSessionCounts(copyFrom.getUserSessionCounts());
    }
}
