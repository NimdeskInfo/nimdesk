/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.ui.server;

import org.apache.commons.lang.StringUtils;

import com.nimdesk.model.Host;
import com.nimdesk.model.Server;

public class ServerSummary {
    private Server _server;
    private Host _host;
    private Object _data = null;

    public ServerSummary(Server server, Host host) {
        _server = server;
        _host = host;
    }

    public Server getServer() {
        return _server;
    }

    public void setServer(Server server) {
        _server = server;
    }

    public Host getHost() {
        return _host;
    }

    public void setHost(Host host) {
        _host = host;
    }

    public String getUuid() {
        return _server.getUuid();
    }

    public String getClusterName() {
        return _server.getClusterName();
    }

    public String getPublicAddress() {
        return _server.getPublicAddress();
    }

    public String getPrivateAddress() {
        if (StringUtils.equals(_server.getPublicAddress(), _server.getPrivateAddress())) {
            return "";
        } else {
            return _server.getPrivateAddress();
        }
    }

    public String getVersion() {
        return _server.getVersion();
    }

    public Server.Status getStatus() {
        return _server.getStatus();
    }

    public String getHostName() {
        return (_host != null) ? String.format("%s(%s)", _host.getAddress(), _host.getName()) : "";
    }

    public Object getData() {
        return _data;
    }

    public void setData(Object data) {
        _data = data;
    }
}
