/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.ui.server;

import java.util.List;

import com.nimdesk.model.LdapServer;
import com.nimdesk.model.Pool;
import com.nimdesk.model.TimeZone;
import com.nimdesk.model.User;

public class CreatePoolSpec {
    private ImageSummary _image = null;
    private Pool.Type _type = Pool.Type.NonPersistent;
    private String _name = null;
    private String _prefix = null;
    private String _description = "";
    private boolean _enabled = true;
    private Long _minSize = null;
    private Long _maxSize = null;
    private long _cpuCount = 1;
    private long _memoryMB = 1024;
    private boolean _reserveMemory = false;
    private LdapServer _ldapServer = null;
    private String _workGroup = "";
    private String _productKey = "";
    private TimeZone _timeZone = TimeZone.getTimeZone("004");
    private String _uddDrive = null;
    private float _uddSizeInGB = 10;
    private boolean _supportRdp = true;
    private boolean _redirectDrives = false;
    private boolean _redirectPrinters = false;
    private boolean _redirectComPorts = false;
    private boolean _redirectSmartCards = false;
    private boolean _isRemoteAppMode = false;
    private List<User> _users = null;

    public ImageSummary getImage() {
        return _image;
    }

    public void setImage(ImageSummary image) {
        _image = image;
    }

    public String getImageName() {
        return _image.getName();
    }

    public Pool.Type getType() {
        return _type;
    }

    public void setType(Pool.Type type) {
        _type = type;
    }

    public String getName() {
        return _name;
    }

    public void setName(String name) {
        _name = name;
    }

    public String getPrefix() {
        return _prefix;
    }

    public void setPrefix(String prefix) {
        _prefix = prefix;
    }

    public String getDescription() {
        return _description;
    }

    public void setDescription(String description) {
        _description = description;
    }

    public boolean isEnabled() {
        return _enabled;
    }

    public void setEnabled(boolean enabled) {
        _enabled = enabled;
    }

    public Long getMinSize() {
        return _minSize;
    }

    public void setMinSize(Long minSize) {
        _minSize = minSize;
    }

    public Long getMaxSize() {
        return _maxSize;
    }

    public void setMaxSize(Long maxSize) {
        _maxSize = maxSize;
    }

    public long getCpuCount() {
        return _cpuCount;
    }

    public void setCpuCount(long cpuCount) {
        _cpuCount = cpuCount;
    }

    public long getMemoryMB() {
        return _memoryMB;
    }

    public void setMemoryMB(long memoryMB) {
        _memoryMB = memoryMB;
    }

    public boolean isReserveMemory() {
        return _reserveMemory;
    }

    public void setReserveMemory(boolean reserveMemory) {
        _reserveMemory = reserveMemory;
    }

    public LdapServer getLdapServer() {
        return _ldapServer;
    }

    public void setLdapServer(LdapServer ldapServer) {
        _ldapServer = ldapServer;
    }

    public String getWorkGroup() {
        return _workGroup;
    }

    public void setWorkGroup(String workGroup) {
        _workGroup = workGroup;
    }

    public String getProductKey() {
        return _productKey;
    }

    public void setProductKey(String productKey) {
        _productKey = productKey;
    }

    public TimeZone getTimeZone() {
        return _timeZone;
    }

    public void setTimeZone(TimeZone timeZone) {
        _timeZone = timeZone;
    }

    public String getUddDrive() {
        return _uddDrive;
    }

    public void setUddDrive(String uddDrive) {
        _uddDrive = uddDrive;
    }

    public float getUddSizeInGB() {
        return _uddSizeInGB;
    }

    public void setUddSizeInGB(float uddSizeInGB) {
        _uddSizeInGB = uddSizeInGB;
    }

    public boolean isSupportRdp() {
        return _supportRdp;
    }

    public void setSupportRdp(boolean supportRdp) {
        _supportRdp = supportRdp;
    }

    public boolean isRedirectDrives() {
        return _redirectDrives;
    }

    public void setRedirectDrives(boolean redirectDrives) {
        _redirectDrives = redirectDrives;
    }

    public boolean isRedirectPrinters() {
        return _redirectPrinters;
    }

    public void setRedirectPrinters(boolean redirectPrinters) {
        _redirectPrinters = redirectPrinters;
    }

    public boolean isRedirectComPorts() {
        return _redirectComPorts;
    }

    public void setRedirectComPorts(boolean redirectComPorts) {
        _redirectComPorts = redirectComPorts;
    }

    public boolean isRedirectSmartCards() {
        return _redirectSmartCards;
    }

    public void setRedirectSmartCards(boolean redirectSmartCards) {
        _redirectSmartCards = redirectSmartCards;
    }

    public boolean isRemoteAppMode() {
        return _isRemoteAppMode;
    }

    public void setRemoteAppMode(boolean isRemoteAppMode) {
        _isRemoteAppMode = isRemoteAppMode;
    }

    public List<User> getUsers() {
        return _users;
    }

    public void setUsers(List<User> users) {
        _users = users;
    }
}

