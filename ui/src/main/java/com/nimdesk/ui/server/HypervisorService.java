/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.ui.server;

import com.nimdesk.event.AsyncCallback;
import com.nimdesk.hypervisor.HypervisorContext;
import com.nimdesk.model.Host;

public interface HypervisorService {
    public static final String BEAN_NAME = "hypervisorService";

    void verifyConnection(String hostname, int port, String username, String password, AsyncCallback callback);

    void findStandaloneHost(HypervisorContext ctx, AsyncCallback callback);

    void getHostAssets(HypervisorContext ctx, String hostRefId, AsyncCallback callback);

    void findImportableVms(Host host, AsyncCallback callback);
}

