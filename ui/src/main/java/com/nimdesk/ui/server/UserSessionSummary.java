/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.ui.server;

import java.util.Date;

import org.apache.commons.lang.StringUtils;

import com.nimdesk.model.Host;
import com.nimdesk.model.User;
import com.nimdesk.model.UserSession;
import com.nimdesk.model.VmInstance;

public class UserSessionSummary {
    private UserSession _userSession;
    private final User _user;
    private final VmInstance _vmInstance;
    private final Host _host;
    private Object _data = null;

    public UserSessionSummary(UserSession userSession, User user, VmInstance vmInstance, Host host) {
        _userSession = userSession;
        _user = user;
        _vmInstance = vmInstance;
        _host = host;
    }

    public String getUuid() {
        return _userSession.getUuid();
    }

    public UserSession getUserSession() {
        return _userSession;
    }

    public void setUserSession(UserSession userSession) {
        _userSession = userSession;
    }

    public User getUser() {
        return _user;
    }

    public VmInstance getVmInstance() {
        return _vmInstance;
    }

    public Host getHost() {
        return _host;
    }

    public String getUserName() {
        if (_user == null) {
            return null;
        }

        if (StringUtils.isEmpty(_user.getDomain())) {
            return _user.getLogonName();
        } else {
            return _user.getLogonName() + "@" + _user.getDomain();
        }
    }

    public String getPoolName() {
        return _userSession.getPoolName();
    }

    public String getDesktopName() {
        return _userSession.getVmInstanceName();
    }

    public String getVmInstanceDns() {
        return _userSession.getVmInstanceDns();
    }

    public String getVmInstanceIp() {
        return _userSession.getVmInstanceIp();
    }

    public boolean isInSession() {
        return _userSession.getEndTime() == 0;
    }

    public String getState() {
        return _userSession.getEndTime() == 0 ? "Active" : "Disconnected";
    }

    public Date getStartTime() {
        long time = _userSession.getStartTime();
        return (time == 0) ? null : new Date(time);
    }

    public Date getEndTime() {
        long time = _userSession.getEndTime();
        return (time == 0) ? null : new Date(time);
    }

    public String getHostName() {
        if (_host == null) {
            return "";
        }

        return String.format("%s(%s)", _host.getAddress(), _host.getName());
    }

    public Object getData() {
        return _data;
    }

    public void setData(Object data) {
        _data = data;
    }
}
