/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.ui.server;

import java.util.Date;

import com.nimdesk.model.DiskImage;
import com.nimdesk.model.Host;
import com.nimdesk.model.Storage;

public class DiskImageSummary {
    private DiskImage _diskImage;
    private final Host _host;
    private Storage _storage;
    private Object _data = null;

    public DiskImageSummary(DiskImage diskImage, Host host, Storage storage) {
        _diskImage = diskImage;
        _host = host;
        _storage = storage;
    }

    public DiskImage getDiskImage() {
        return _diskImage;
    }

    public void setDiskImage(DiskImage diskImage) {
        _diskImage = diskImage;
    }

    public Storage getStorage() {
        return _storage;
    }

    public void setStorage(Storage storage) {
        _storage = storage;
    }

    public String getUuid() {
        return _diskImage.getUuid();
    }

    public String getName() {
        return _diskImage.getName();
    }

    public Date getCreateTime() {
        long time = _diskImage.getCreateTime();
        return (time == 0) ? null : new Date(time);
    }

    public Long getVersion() {
        return _diskImage.getVersion();
    }

    public String getHostName() {
        return (_host != null) ? String.format("%s(%s)", _host.getAddress(), _host.getName()) : "";
    }

    public String getStorageName() {
        return (_storage != null) ? _storage.getName() : "";
    }

    public DiskImage.Status getStatus() {
        return _diskImage.getStatus();
    }

    public Object getData() {
        return _data;
    }

    public void setData(Object data) {
        _data = data;
    }
}
