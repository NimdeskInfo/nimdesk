-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

Nimdesk VDI Broker allows users to rapidly and easily stand up Secure, Reliable, and Highly Available Virtual Desktop Infrastructures on top of VMware ESXi, supporting multi-AD, multi-tenancy and spanning multi-hosts without requiring vCenter. Due to the Grid Distributed nature of Nimdesk, you can deploy VDI, meeting your organizational HA requirements, while using all Local Storage - no SAN necessary.

-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

```
BUILDING
under source root:
 mvn clean install

RUNNING
under server:
 mvn jetty:run-war
```