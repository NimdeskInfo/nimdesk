<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@page import="org.springframework.security.web.savedrequest.SavedRequest"%>
<%@page import="org.springframework.security.web.savedrequest.DefaultSavedRequest"%>

<%
SavedRequest savedRequest = (SavedRequest) session.getAttribute(DefaultSavedRequest.SPRING_SECURITY_SAVED_REQUEST_KEY);
if(savedRequest != null && savedRequest.getRedirectUrl().indexOf("/UIDL") != -1) {
    session.removeAttribute(DefaultSavedRequest.SPRING_SECURITY_SAVED_REQUEST_KEY);

    response.setContentType("application/json; charset=UTF-8");
    //for(;;);[realjson]
    out.print("       {\"redirect\" : {\"url\" : \"" + request.getContextPath() + "/loginform.jsp" + "\"}} ");
} else {
    if (request.getQueryString() != null) {
        response.sendRedirect(request.getContextPath() + "/loginform.jsp?" + request.getQueryString());
    } else {
        response.sendRedirect(request.getContextPath() + "/loginform.jsp");
    }
}
%>
