/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.vmware.vim25;

public class CbrcDeviceSpec extends DynamicData {
    public ManagedObjectReference vm;
    public int deviceKey;

    public ManagedObjectReference getVm() {
        return this.vm;
    }

    public int getDeviceKey() {
        return this.deviceKey;
    }

    public void setVm(ManagedObjectReference vm) {
        this.vm = vm;
    }

    public void setDeviceKey(int deviceKey) {
        this.deviceKey = deviceKey;
    }
}
