/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.vmware.vim25.mo;

import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;

import com.vmware.vim25.InternalServiceInstanceContent;
import com.vmware.vim25.ManagedObjectReference;
import com.vmware.vim25.RuntimeFault;
import com.vmware.vim25.mo.util.MorUtil;
import com.vmware.vim25.ws.Argument;

public class ServiceInstanceWithInternalService extends ServiceInstance {
    private InternalServiceInstanceContent internalServiceInstanceContent = null;

    public ServiceInstanceWithInternalService(URL url, String username, String password)
            throws RemoteException, MalformedURLException {
        this(url, username, password, false);
    }

    public ServiceInstanceWithInternalService(URL url, String username, String password, boolean ignoreCert)
            throws RemoteException, MalformedURLException {
        this(url, username, password, ignoreCert, VIM25_NAMESPACE);
    }

    public ServiceInstanceWithInternalService(URL url, String username, String password, boolean ignoreCert, String namespace)
            throws RemoteException, MalformedURLException  {
        super(url, username, password, ignoreCert, namespace);
    }

    public ServiceInstanceWithInternalService(URL url, String sessionStr, boolean ignoreCert)
            throws RemoteException, MalformedURLException {
        this(url, sessionStr, ignoreCert, VIM25_NAMESPACE);
    }

    public ServiceInstanceWithInternalService(URL url, String sessionStr, boolean ignoreCert, String namespace)
            throws RemoteException, MalformedURLException {
        super(url, sessionStr, ignoreCert, namespace);
    }

    public ServiceInstanceWithInternalService(ServerConnection sc) {
        super(sc);
    }

    public InternalServiceInstanceContent getInternalServiceInstanceContent() {
        if (internalServiceInstanceContent == null) {
            try {
                internalServiceInstanceContent = retrieveInternalContent();
            } catch(Exception e)
            {
                System.out.println("Exceptoin: " + e);
            }
        }
        return internalServiceInstanceContent;
    }

    private InternalServiceInstanceContent retrieveInternalContent()
            throws RuntimeFault, RemoteException {
        Argument[] paras = new Argument[1];
        paras[0] = new Argument("_this", "ManagedObjectReference", getMOR());
        return (InternalServiceInstanceContent) getVimService().getWsc().invoke("RetrieveInternalContent", paras, "InternalServiceInstanceContent");
    }

    public CbrcManager getCbrcManager() {
        return (CbrcManager) createMO(getInternalServiceInstanceContent().getCbrcManager());
    }

    private ManagedObject createMO(ManagedObjectReference mor)
    {
        return MorUtil.createExactManagedObject(getServerConnection(), mor);
    }
}
