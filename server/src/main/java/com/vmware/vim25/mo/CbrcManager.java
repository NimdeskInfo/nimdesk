/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.vmware.vim25.mo;

import java.rmi.RemoteException;

import com.vmware.vim25.CbrcDeviceSpec;
import com.vmware.vim25.CbrcDigestInfoResult;
import com.vmware.vim25.CbrcDigestRuntimeInfoResult;
import com.vmware.vim25.CbrcVmdkLockFailure;
import com.vmware.vim25.InvalidPowerState;
import com.vmware.vim25.InvalidState;
import com.vmware.vim25.ManagedObjectNotFound;
import com.vmware.vim25.ManagedObjectReference;
import com.vmware.vim25.NotSupported;
import com.vmware.vim25.VmConfigFault;
import com.vmware.vim25.ws.Argument;

public class CbrcManager extends ManagedObject {
    public CbrcManager(ServerConnection sc, ManagedObjectReference mor) {
        super(sc, mor);
    }

    public Task configureDigest_Task(CbrcDeviceSpec[] spec, boolean enabled, boolean disableFullChain, boolean enablePartitionAlignment)
            throws RemoteException, ManagedObjectNotFound, VmConfigFault, InvalidState, NotSupported, InvalidPowerState, CbrcVmdkLockFailure {
        Argument[] paras = new Argument[5];
        paras[0] = new Argument("_this", "ManagedObjectReference", getMOR());
        paras[1] = new Argument("spec", "CbrcDeviceSpec[]", spec);
        paras[2] = new Argument("enabled", "boolean", enabled);
        paras[3] = new Argument("disableFullChain", "boolean", disableFullChain);
        paras[4] = new Argument("enablePartitionAlignment", "boolean", enablePartitionAlignment);
        ManagedObjectReference taskMor = (ManagedObjectReference) getVimService().getWsc().invoke("ConfigureDigest_Task", paras, "ManagedObjectReference");

        return new Task(getServerConnection(), taskMor);
    }

    public Task recomputeDigest_Task(CbrcDeviceSpec[] spec, boolean fullRecompute)
            throws RemoteException, ManagedObjectNotFound, VmConfigFault, InvalidState, NotSupported {
        Argument[] paras = new Argument[3];
        paras[0] = new Argument("_this", "ManagedObjectReference", getMOR());
        paras[1] = new Argument("spec", "CbrcDeviceSpec[]", spec);
        paras[2] = new Argument("fullRecompute", "boolean", fullRecompute);
        ManagedObjectReference taskMor = (ManagedObjectReference) getVimService().getWsc().invoke("RecomputeDigest_Task", paras, "ManagedObjectReference");

        return new Task(getServerConnection(), taskMor);
    }

    public CbrcDigestInfoResult[] queryDigestInfo(CbrcDeviceSpec[] spec)
            throws RemoteException, ManagedObjectNotFound, VmConfigFault, InvalidState, NotSupported {
        Argument[] paras = new Argument[2];
        paras[0] = new Argument("_this", "ManagedObjectReference", getMOR());
        paras[1] = new Argument("spec", "CbrcDeviceSpec[]", spec);
        return (CbrcDigestInfoResult[]) getVimService().getWsc().invoke("QueryDigestInfo", paras, "CbrcDigestInfoResult[]");
    }

    public CbrcDigestRuntimeInfoResult[] queryDigestRuntimeInfo(CbrcDeviceSpec[] spec)
            throws RemoteException, ManagedObjectNotFound, VmConfigFault, InvalidState, NotSupported {
        Argument[] paras = new Argument[2];
        paras[0] = new Argument("_this", "ManagedObjectReference", getMOR());
        paras[1] = new Argument("spec", "CbrcDeviceSpec[]", spec);
        return (CbrcDigestRuntimeInfoResult[]) getVimService().getWsc().invoke("QueryDigestRuntimeStatus", paras, "CbrcDigestRuntimeInfoResult[]");
    }
}
