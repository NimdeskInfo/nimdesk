/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.vmware.vim25;


public class InternalServiceInstanceContent extends DynamicData {
    public ManagedObjectReference agentManager;
    public ManagedObjectReference cbrcManager;
    public ManagedObjectReference diskManager;
    public ManagedObjectReference drsStatsManager;
    public ManagedObjectReference esxAgentConfigManager;
    public ManagedObjectReference ftManager;
    public ManagedObjectReference hbrManager;
    public ManagedObjectReference hostDistributedVirtualSwitchManager;
    public ProfileHostProfileEngineHostProfileEngine hostProfileEngine;
    public ManagedObjectReference ilManager;
    public ManagedObjectReference internalStatsCollector;
    public ManagedObjectReference legacyTemplateManager;
    public ManagedObjectReference llProvisioningManager;
    public ManagedObjectReference nfcService;
    public ManagedObjectReference proxyService;
    public ManagedObjectReference resourcePlanningManager;
    public ManagedObjectReference serviceDirectory;
    public ManagedObjectReference serviceManager;
    public ManagedObjectReference tpmManager;
    public ManagedObjectReference vmciAccessManager;

    public ManagedObjectReference getAgentManager() {
        return this.agentManager;
    }

    public ManagedObjectReference getCbrcManager() {
      return this.cbrcManager;
    }

    public ManagedObjectReference getDiskManager() {
        return this.diskManager;
    }

    public ManagedObjectReference getDrsStatsManager() {
        return this.drsStatsManager;
    }

    public ManagedObjectReference getEsxAgentConfigManager() {
        return this.esxAgentConfigManager;
    }

    public ManagedObjectReference getFtManager() {
        return this.ftManager;
    }

    public ManagedObjectReference getHbrManager() {
        return this.hbrManager;
    }

    public ManagedObjectReference getHostDistributedVirtualSwitchManager() {
        return this.hostDistributedVirtualSwitchManager;
    }

    public ProfileHostProfileEngineHostProfileEngine getHostProfileEngine() {
        return this.hostProfileEngine;
    }

    public ManagedObjectReference getIlManager() {
        return this.ilManager;
    }

    public ManagedObjectReference getInternalStatsCollector() {
        return this.internalStatsCollector;
    }

    public ManagedObjectReference getLegacyTemplateManager() {
        return this.legacyTemplateManager;
    }

    public ManagedObjectReference getLlProvisioningManager() {
        return this.llProvisioningManager;
    }

    public ManagedObjectReference getNfcService() {
        return this.nfcService;
    }

    public ManagedObjectReference getProxyService() {
        return this.proxyService;
    }

    public ManagedObjectReference getResourcePlanningManager() {
        return this.resourcePlanningManager;
    }

    public ManagedObjectReference getServiceDirectory() {
        return this.serviceDirectory;
    }

    public ManagedObjectReference getServiceManager() {
        return this.serviceManager;
    }

    public ManagedObjectReference getTpmManager() {
        return this.tpmManager;
    }

    public ManagedObjectReference getVmciAccessManager() {
        return this.vmciAccessManager;
    }
}
