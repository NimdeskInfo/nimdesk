/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.vmware.vim25;

public class CbrcDigestInfo extends DynamicData {
    public int digestVersion;
    public int digestBlockSize;
    public int numberHashes;
    public int numberValidHashes;
    public boolean partitionOffsetEnabled;
    public boolean hashCollisionDetectionEnabled;
    public int hashKeyLength;
    public int journalCoverageArea;
    public int baseDiskCid;
    public String baseDiskPath;
    public String baseDigestPath;
    public boolean recomputeNeeded;

    public int getDigestVersion() {
        return this.digestVersion;
    }

    public int getDigestBlockSize() {
        return this.digestBlockSize;
    }

    public int getNumberHashes() {
        return this.numberHashes;
    }

    public int getNumberValidHashes() {
        return this.numberValidHashes;
    }

    public boolean getPartitionOffsetEnabled() {
        return this.partitionOffsetEnabled;
    }

    public boolean getHashCollisionDetectionEnabled() {
        return this.hashCollisionDetectionEnabled;
    }

    public int getHashKeyLength() {
        return this.hashKeyLength;
    }

    public int getJournalCoverageArea() {
        return this.journalCoverageArea;
    }

    public int getBaseDiskCid() {
        return this.baseDiskCid;
    }

    public String getBaseDiskPath() {
        return this.baseDiskPath;
    }

    public String getBaseDigestPath() {
        return this.baseDigestPath;
    }

    public boolean getRecomputeNeeded() {
        return this.recomputeNeeded;
    }
}
