/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.rrd;

import java.io.IOException;

public class RrdSystem {
    //private static final Logger LOGGER = Logger.getLogger(RrdSystem.class);

    private static final String RRD_DIRECTORY = "/var/nimdesk/rrd";

    private RrdStorage _rrdStorage = null;
    private final Object _rrdStorageLock = new Object();

    public RrdSystem() {
    }

    public RrdFile getRrdFileById(String id) throws IOException {
        synchronized(_rrdStorageLock) {
            if (_rrdStorage == null) {
                _rrdStorage = new RrdStorage(RRD_DIRECTORY);
            }
        }

        return _rrdStorage.getRrdFile(id);
    }
}
