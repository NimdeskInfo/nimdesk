/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.rrd;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.rrd4j.ConsolFun;
import org.rrd4j.core.DsDef;
import org.rrd4j.core.RrdDb;
import org.rrd4j.core.RrdDef;
import org.rrd4j.core.Sample;
import org.springframework.util.Assert;

public class RrdFile {
    public interface RrdFileLocker {
        void lock();
        void unlock();
    }

    public interface RenderGraphCallback {
        void onCallback(BufferedImage image);
    }

    private static final Logger LOGGER = Logger.getLogger(RrdFile.class);

    public static final long DEFAULT_RRDSTEP = 60L;
    public static final long DEFAULT_HEARTBEAT = 2700L;

    private static final String RRD_FILE_SUFFIX = ".rrd";
    private static final String INTERNAL_DATASOURCE_NAME = "data";

    private final String _id;
    private final File _rrdFile;
    private final RrdFileLocker _locker;

    public RrdFile(String parentPath, String id, RrdFileLocker locker) {
        Assert.notNull(locker);
        _id = id;
        _rrdFile = new File(parentPath + File.separator + id + RRD_FILE_SUFFIX);
        _locker = locker;
    }

    public interface AddRrdDatasourceCallback {
        void addDatasources(RrdDef rrdDef);
    }

    public void create(long time, AddRrdDatasourceCallback callback) throws IOException {
        if (callback == null) {
            return;
        }

        _locker.lock();

        try {
            if (_rrdFile.exists() && !_rrdFile.delete()) {
                LOGGER.error("Failed to delete file " + _rrdFile.getAbsolutePath());
                throw new IOException("Failed to delete file " + _rrdFile.getAbsolutePath());
            }

            //long startTime = (System.currentTimeMillis() + 500L) / 1000L;

            RrdDef rrdDef = new RrdDef(_rrdFile.getAbsolutePath(), time / 1000L/*startTime*/ - 1, DEFAULT_RRDSTEP);
            callback.addDatasources(rrdDef);

            rrdDef.addArchive(ConsolFun.AVERAGE, 0.5, 1, 300);
            rrdDef.addArchive(ConsolFun.AVERAGE, 0.5, 5, 1150);
            rrdDef.addArchive(ConsolFun.AVERAGE, 0.5, 180, 250);
            rrdDef.addArchive(ConsolFun.AVERAGE, 0.5, 720, 730);

            rrdDef.addArchive(ConsolFun.MIN, 0.5, 1, 300);
            rrdDef.addArchive(ConsolFun.MIN, 0.5, 5, 1150);
            rrdDef.addArchive(ConsolFun.MIN, 0.5, 180, 250);
            rrdDef.addArchive(ConsolFun.MIN, 0.5, 720, 730);

            rrdDef.addArchive(ConsolFun.MAX, 0.5, 1, 300);
            rrdDef.addArchive(ConsolFun.MAX, 0.5, 5, 1150);
            rrdDef.addArchive(ConsolFun.MAX, 0.5, 180, 250);
            rrdDef.addArchive(ConsolFun.MAX, 0.5, 720, 730);

            RrdDb rrdDb = new RrdDb(rrdDef);
            rrdDb.close();
        } finally {
            _locker.unlock();
        }
    }

    public String getId() {
        return _id;
    }

    public File getFile() {
        return _rrdFile;
    }

    public DsDef[] getRrdDsDefs() throws IOException {
        RrdDb rrdDb = null;
        DsDef[] dsDefs = null;

        _locker.lock();

        try {
            if (!_rrdFile.exists()) {
                throw new IOException("Rrd file " + _rrdFile.getAbsolutePath() + " doesn't exist");
            }

            rrdDb = new RrdDb(_rrdFile.getAbsolutePath());
            dsDefs = rrdDb.getRrdDef().getDsDefs();
        } finally {
            if (rrdDb != null) {
                try {
                    rrdDb.close();
                } catch (Exception e) {
                    LOGGER.error("Failed to close RrdDb for id " + _id);
                }
            }

            _locker.unlock();
        }

        return dsDefs;
    }

    /*
    public void updateData(long time, List<CollectDataItem> items) throws Exception {
        LOGGER.info(String.format("updateData time=%d", time / 1000L));

        RrdDb rrdDb = null;

        _locker.lock();

        try {
            if (!_rrdFile.exists()) {
                throw new IOException("Rrd file " + _rrdFile.getAbsolutePath() + " doesn't exist");
            }

            rrdDb = new RrdDb(_rrdFile.getAbsolutePath());
            Sample sample = rrdDb.createSample();
            sample.setTime(time / 1000L);
            for (CollectDataItem item : items) {
                double value;
                try {
                    value = Double.valueOf(item.getValue());
                    sample.setValue(getRrdDsNameById(item.getDataSourceId()), value);
                } catch (Exception e) {
                    sample.setValue(getRrdDsNameById(item.getDataSourceId()), Double.NaN);
                }
            }
            sample.update();
        } catch (Exception e) {
            String msg = e.getMessage();
            if ( msg != null && msg.indexOf("Bad sample time") >= 0 ) {
                LOGGER.error("Failed to update RrdDb for id " + _id, e);
            } else {
                throw e;
            }
        } finally {
            if (rrdDb != null) {
                try {
                    rrdDb.close();
                } catch (Exception e) {
                    LOGGER.error("Failed to close RrdDb for id " + _id);
                }
            }

            _locker.unlock();
        }
    }
    */

    public interface UpdateDataCallback {
        void updateData(Sample sample);
    }

    public void updateData(long time, UpdateDataCallback callback) throws Exception {
        if (callback == null) {
            return;
        }

        RrdDb rrdDb = null;

        _locker.lock();

        try {
            if (!_rrdFile.exists()) {
                throw new IOException("Rrd file " + _rrdFile.getAbsolutePath() + " doesn't exist");
            }

            rrdDb = new RrdDb(_rrdFile.getAbsolutePath());
            Sample sample = rrdDb.createSample();
            sample.setTime(time / 1000L);
            callback.updateData(sample);
            sample.update();
        } catch (Exception e) {
            String msg = e.getMessage();
            if ( msg != null && msg.indexOf("Bad sample time") >= 0 ) {
                LOGGER.error("Failed to update RrdDb for id " + _id, e);
            } else {
                throw e;
            }
        } finally {
            if (rrdDb != null) {
                try {
                    rrdDb.close();
                } catch (Exception e) {
                    LOGGER.error("Failed to close RrdDb for id " + _id);
                }
            }

            _locker.unlock();
        }
    }

    /*
    public void renderGraph(DataSource dataSource, RenderGraphCallback callback) throws IOException {
        Random rand = new Random(System.currentTimeMillis());
        File graphFile = new File(_rrdFile.getParent() + File.separator + _id + "." + rand.nextInt() + ".png");

        int graphWidth = 360;
        int graphHeight = 120;

        RrdGraphDef graphDef = new RrdGraphDef();
        graphDef.setTimeSpan(-2 * 3600L, -1L);
        graphDef.setVerticalLabel(dataSource.getName());
        graphDef.datasource(dataSource.getName(), _rrdFile.getAbsolutePath(), getRrdDsNameById(dataSource.getId()), ConsolFun.AVERAGE);
        graphDef.line(dataSource.getName(), new Color(0xFF, 0, 0), null, 2);
        graphDef.setWidth(graphWidth);
        graphDef.setHeight(graphHeight);
        graphDef.setFilename(graphFile.getAbsolutePath());

        RrdGraph graph = new RrdGraph(graphDef);
        BufferedImage bi = new BufferedImage(graphWidth, graphHeight, BufferedImage.TYPE_INT_RGB);

        _locker.lock();
        try {
            graph.render(bi.getGraphics());

            if (callback != null) {
                callback.onCallback(bi);
            }
        } finally {
            _locker.unlock();

            if (graphFile.exists()) {
                try {
                    graphFile.delete();
                } catch (Throwable throwable) {
                    LOGGER.warn("Failed to delete file " + graphFile.getAbsolutePath(), throwable);
                }
            }
        }
    }
    */

    //public String getRrdDsNameById(long dataSourceId) {
    //    return INTERNAL_DATASOURCE_NAME + dataSourceId;
    //}

    @SuppressWarnings("unused")
    private String getRrdDsNameByName(String name) {
        return INTERNAL_DATASOURCE_NAME + name;
    }

    /*
    private DsType convertDsType(DataSourceType type) {
        switch (type) {
        case GAUGE:
        default:
            return DsType.GAUGE;

        case COUNTER:
            return DsType.COUNTER;

        case DERIVE:
            return DsType.DERIVE;

        case ABSOLUTE:
            return DsType.ABSOLUTE;
        }
    }
    */
}
