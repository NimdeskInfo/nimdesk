/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.rrd;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;

import org.apache.log4j.Logger;

import com.nimdesk.rrd.RrdFile.RrdFileLocker;

class RrdStorage {
    private static final Logger LOGGER = Logger.getLogger(RrdStorage.class);

    private final File _storageDirectory;
    private final HashSet<String> _lockedIds = new HashSet<String>();

    public RrdStorage(String storageDir)
            throws IOException {
        _storageDirectory = new File(storageDir);
        if (!_storageDirectory.exists() && !_storageDirectory.mkdirs()) {
            throw new IOException("Failed to create directory " + _storageDirectory.getAbsolutePath());
        }
    }

    public RrdFile getRrdFile(final String id) {
        RrdFile rrdFile = new RrdFile(_storageDirectory.getAbsolutePath(), id, new RrdFileLocker() {
            @Override
            public void lock() {
                lockRrd(id);
            }

            @Override
            public void unlock() {
                unlockRrd(id);
            }
        });

        return rrdFile;
    }

    private void lockRrd(String id) {
        synchronized(_lockedIds) {
            while (_lockedIds.contains(id)) {
                try {
                    _lockedIds.wait();
                } catch (InterruptedException e) {
                    LOGGER.error("Waiting for RrdFile id " + id + " is interrupted");
                }
            }
            _lockedIds.add(id);
        }
    }

    private void unlockRrd(String id) {
        synchronized(_lockedIds) {
            _lockedIds.remove(id);
            _lockedIds.notifyAll();
        }
    }
}
