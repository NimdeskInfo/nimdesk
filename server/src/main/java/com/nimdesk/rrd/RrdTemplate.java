/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.rrd;

import java.io.IOException;
import java.util.List;

import org.rrd4j.ConsolFun;
import org.rrd4j.core.DsDef;
import org.rrd4j.data.DataProcessor;
import org.springframework.util.Assert;

public class RrdTemplate {
    //private static final Logger LOGGER = Logger.getLogger(RrdTemplate.class);

    public class DataResult {
        public String metricName;
        public String rrdDsName;
        public long[] timestamps = null;
        public double[] values = null;
    }

    private final RrdFile _rrdFile;

    public RrdTemplate(RrdFile rrdFile) {
        _rrdFile = rrdFile;
        Assert.notNull(_rrdFile);
    }

    public void getData(long startTime, long endTime, List<String> metricNames, List<DataResult> results)
            throws IOException {
        Assert.notNull(metricNames);
        Assert.notNull(results);

        DataProcessor dataProcessor = new DataProcessor(startTime, endTime);

        DsDef[] dsDefs = _rrdFile.getRrdDsDefs();

        dsDefLoop:
        for (DsDef dsDef : dsDefs) {
            String dsName = dsDef.getDsName();

            for (String metric : metricNames) {
                if (!metric.equals(dsName) && dsName.indexOf(metric + "-") != 0) {
                    continue;
                }

                dataProcessor.addDatasource(dsName, _rrdFile.getFile().getAbsolutePath(), dsName, ConsolFun.AVERAGE);

                DataResult dataResult = new DataResult();
                dataResult.metricName = metric;
                dataResult.rrdDsName = dsName;
                results.add(dataResult);

                continue dsDefLoop;
            }
        }

        dataProcessor.processData();

        long[] timestamps = dataProcessor.getTimestamps();

        for (DataResult dataResult : results) {
            dataResult.timestamps = timestamps;
            dataResult.values = dataProcessor.getValues(dataResult.rrdDsName);
        }
    }

    /*
    public DataResult getData(long startTime, long endTime, long dataSourceId)
        throws IOException {
        DataProcessor dataProcessor = new DataProcessor(startTime, endTime);

        String dsName = _rrdFile.getRrdDsNameById(dataSourceId);

        dataProcessor.addDatasource(dsName, _rrdFile.getFile().getAbsolutePath(), dsName, ConsolFun.AVERAGE);
        dataProcessor.processData();

        DataResult dataResult = new DataResult();
        dataResult.rrdDsName = dsName;
        dataResult.timestamps = dataProcessor.getTimestamps();
        dataResult.values = dataProcessor.getValues(dataResult.rrdDsName);

        return dataResult;
    }
    */
}
