/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.vm;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import com.nimdesk.event.AddRemoveVmStateListenerEvent;
import com.nimdesk.event.AsyncCallback;
import com.nimdesk.event.AttachVmDiskEvent;
import com.nimdesk.event.ConnectHostEvent;
import com.nimdesk.event.DetachVmAllUserDisksEvent;
import com.nimdesk.event.EnableHostCbrcEvent;
import com.nimdesk.event.EnableVmCbrcEvent;
import com.nimdesk.event.EventHandler;
import com.nimdesk.event.FindImportableVmsEvent;
import com.nimdesk.event.FindStandaloneHostEvent;
import com.nimdesk.event.GetHostAssetsEvent;
import com.nimdesk.event.InstantiateVmEvent;
import com.nimdesk.event.PowerOpVmEvent;
import com.nimdesk.event.RefreshVmEvent;
import com.nimdesk.event.SnapshotVmEvent;
import com.nimdesk.event.StartHostMonitorEvent;
import com.nimdesk.event.TemplatizeVmEvent;
import com.nimdesk.event.UninstantiateVmEvent;
import com.nimdesk.hypervisor.vmware.VmwareContext;
import com.nimdesk.hypervisor.vmware.mo.HostMO;
import com.nimdesk.hypervisor.vmware.util.DatastoreFile;
import com.nimdesk.model.Host;
import com.nimdesk.model.Storage;
import com.nimdesk.model.User;
import com.nimdesk.service.EventBusService;
import com.nimdesk.util.ActionDelegate;
import com.nimdesk.util.Pair;
import com.nimdesk.vm.vmware.VmwareHostManager;
import com.nimdesk.vm.vmware.VmwareHostWatcher;
import com.nimdesk.vm.vmware.VmwareVmManager;
import com.google.common.eventbus.AllowConcurrentEvents;
import com.google.common.eventbus.Subscribe;

/**
 *
 */
public class HostController implements EventHandler {
    private static final Logger LOGGER = Logger.getLogger(HostController.class);

    private static final int CORE_HOST_POOL_SIZE = 2;
    private static final int MAX_HOST_POOL_SIZE = CORE_HOST_POOL_SIZE + 1;//200;

    private final ExecutorService _executor = new ThreadPoolExecutor(CORE_HOST_POOL_SIZE,
            MAX_HOST_POOL_SIZE, 30L, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>());

    private static final int CORE_VMOPS_POOL_SIZE = 10;//2;
    private static final int MAX_VMOPS_POOL_SIZE = CORE_VMOPS_POOL_SIZE + 1;//10;

    private final ExecutorService _vmOpExecutor = new ThreadPoolExecutor(CORE_VMOPS_POOL_SIZE,
            MAX_VMOPS_POOL_SIZE, 30L, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>());

    private final ExecutorService _cbrcExecutor = new ThreadPoolExecutor(CORE_VMOPS_POOL_SIZE,
            MAX_VMOPS_POOL_SIZE, 30L, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>());

    private final EventBusService _eventBusService;
    private final Host.Service _hostService;

    private final VmwareHostManager _hostManager = new VmwareHostManager();
    private final VmwareVmManager _vmManager = new VmwareVmManager();
    private VmwareHostWatcher _hostWatcher = null;
    private final Object _hostWatcherMutext = new Object();

    @Autowired
    public HostController(EventBusService eventBusService, Host.Service hostService) {
        Assert.notNull(eventBusService, "An EventBusService must be set");
        _eventBusService = eventBusService;

        Assert.notNull(hostService, "A Host.Service must be set");
        _hostService = hostService;

        LOGGER.info("Register HostController with event bus");
        _eventBusService.register(this);
    }

    @Subscribe
    @AllowConcurrentEvents
    public void handle(final ConnectHostEvent event) {
        _executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    VmwareContext context = VmwareContext.connect(event.getHostname(), 0, event.getUsername(), event.getPassword());

                    if (event.getCallback() != null) {
                        event.getCallback().onSuccess(context);
                    }
                } catch (Exception e) {
                    LOGGER.error(String.format("Failed to connect to host \"%s\"", event.getHostname()), e);

                    if (event.getCallback() != null) {
                        event.getCallback().onFailure(e);
                    }
                }
            }
        });
    }

    @Subscribe
    @AllowConcurrentEvents
    public void handle(final FindStandaloneHostEvent event) {
        _executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    HostAsset host = _hostManager.findStandaloneHost(event.getContext());

                    if (event.getCallback() != null) {
                        event.getCallback().onSuccess(host);
                    }
                } catch (Exception e) {
                    LOGGER.error(String.format("Failed to find datastores \"%s\"", event.getContext().getServerAddress()), e);

                    if (event.getCallback() != null) {
                        event.getCallback().onFailure(e);
                    }
                }
            }
        });
    }

    @Subscribe
    @AllowConcurrentEvents
    public void handle(final GetHostAssetsEvent event) {
        _executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    Assert.hasText(event.getHostRefId(), "Empty host refId specified");

                    Map<String, Object> assets =
                            _hostManager.getHostAssets(event.getContext(), event.getHostRefId());

                    if (event.getCallback() != null) {
                        event.getCallback().onSuccess(assets);
                    }
                } catch (Exception e) {
                    LOGGER.error(String.format("Failed to get host assets \"%s\"", event.getContext().getServerAddress()), e);

                    if (event.getCallback() != null) {
                        event.getCallback().onFailure(e);
                    }
                }
            }
        });
    }

    @Subscribe
    @AllowConcurrentEvents
    public void handle(final FindImportableVmsEvent event) {
        _executor.execute(new Runnable() {
            @Override
            public void run() {
                Host host = event.getHost();
                VmwareContext context = null;

                try {
                    context = VmwareContext.connect(host.getAddress(), 0, host.getUsername(), host.getPassword());
                    if (context == null) {
                        throw new Exception(String.format("Unable to connect to the host \"%s\"", host.getAddress()));
                    }

                    List<ImportableVm> vmList = _hostManager.findImportableVms(context, host.getRefId());

                    if (event.getCallback() != null) {
                        event.getCallback().onSuccess(vmList);
                    }
                } catch (Exception e) {
                    LOGGER.error(String.format("Failed to find importable VMs on host \"%s\"", host.getAddress()), e);

                    if (event.getCallback() != null) {
                        event.getCallback().onFailure(e);
                    }
                } finally {
                    if (context != null) {
                        try {
                            context.disconnect();
                        } catch (Exception e) {
                            // Ignore error.
                        }
                    }
                }
            }
        });
    }

    @Subscribe
    @AllowConcurrentEvents
    public void handle(final EnableHostCbrcEvent event) {
        _executor.execute(new Runnable() {
            @Override
            public void run() {
                Host host = event.getHost();

                try {
                    VmwareContext context = VmwareContext.connect(host.getAddress(), 0, host.getUsername(), host.getPassword());
                    HostMO hostMo = new HostMO(context, host.getRefId());

                    @SuppressWarnings("unchecked")
                    boolean succeeded = hostMo.updateOptions(new Pair<String, Object>("CBRC.Enable", event.isEnable()));
                    if (!succeeded) {
                        throw new Exception("Failed to update option CBRC.Enable");
                    }

                    if (event.getCallback() != null) {
                        event.getCallback().onSuccess(context);
                    }
                } catch (Exception e) {
                    LOGGER.error(String.format("Failed to enable CBRC for host \"%s\"", host.getName()), e);

                    if (event.getCallback() != null) {
                        event.getCallback().onFailure(e);
                    }
                }
            }
        });
    }

    @Subscribe
    @AllowConcurrentEvents
    public void handle(final EnableVmCbrcEvent event) {
        _cbrcExecutor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    final AsyncCallback callback = event.getCallback();

                    boolean cbrcEnabled = _vmManager.enableVmCbrc(event.getContext(), event.getVmRefId(),
                            event.isEnable(), event.getVcContext(), event.getHostEsxUuid(),
                            (callback == null) ? null : new ActionDelegate<Integer>() {
                                @Override
                                public void action(Integer param) {
                                    callback.progress(param);
                                }
                    });

                    if (callback != null) {
                        callback.onSuccess(cbrcEnabled);
                    }
                } catch (Exception e) {
                    LOGGER.error(String.format("Failed to %s CBRC for vm id=\"%s\"",
                            event.isEnable() ? "enable" : "disable", event.getVmRefId()), e);

                    if (event.getCallback() != null) {
                        event.getCallback().onFailure(e);
                    }
                }
            }
        });
    }

    @Subscribe
    @AllowConcurrentEvents
    public void handle(final StartHostMonitorEvent event) {
        synchronized(_hostWatcherMutext) {
            if (_hostWatcher == null) {
                _hostWatcher = new VmwareHostWatcher(event.getLocalServer(), event.getHost(), _hostService);
                _hostWatcher.start();
            }
        }
    }

    @Subscribe
    @AllowConcurrentEvents
    public void handle(final AddRemoveVmStateListenerEvent event) {
        synchronized(_hostWatcherMutext) {
            if (_hostWatcher != null) {
                if (event.isAdd()) {
                    _hostWatcher.addVmStateListener(event.getListener());
                } else {
                    _hostWatcher.removeVmStateListener(event.getListener());
                }
            }
        }
    }

    @Subscribe
    @AllowConcurrentEvents
    public void handle(final InstantiateVmEvent event) {
        // Use _vmOpExecutor to throttle vm operations.
        _vmOpExecutor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    final AsyncCallback callback = event.getCallback();

                    InstantiatedVm results = _vmManager.instantiateVm(event.getContext(), event.getHostRefId(), event.isPoweronVm(),
                            event.getVmName(), event.getVmxPath(), event.getCpuCount(), event.getMemoryMB(),
                            event.isReserveMemory(), null/*event.getMacAddress()*/, event.isEnableCbrc(),
                            event.getVcContext(), event.getHostEsxUuid(),
                            (callback == null) ? null : new ActionDelegate<Integer>() {
                                @Override
                                public void action(Integer param) {
                                    callback.progress(param);
                                }
                    });

                    if (callback != null) {
                        callback.onSuccess(results);
                    }
                } catch (Exception e) {
                    LOGGER.error(String.format("Failed to instantiate VM \"%s\"", event.getVmName()), e);

                    if (event.getCallback() != null) {
                        event.getCallback().onFailure(e);
                    }
                }
            }
        });
    }

    @Subscribe
    @AllowConcurrentEvents
    public void handle(final UninstantiateVmEvent event) {
        // Use _vmOpExecutor to throttle vm operations.
        _vmOpExecutor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    final AsyncCallback callback = event.getCallback();

                    String vmxPath = _vmManager.uninstantiateVm(event.getContext(), event.getVmRefId(),
                            event.getVcContext(), event.getHostEsxUuid(),
                            (callback == null) ? null : new ActionDelegate<Integer>() {
                                @Override
                                public void action(Integer param) {
                                    callback.progress(param);
                                }
                    });

                    if (callback != null) {
                        callback.onSuccess(vmxPath);
                    }
                } catch (Exception e) {
                    LOGGER.error(String.format("Failed to uninstantiate VM id=\"%s\"", event.getVmRefId()), e);

                    if (event.getCallback() != null) {
                        event.getCallback().onFailure(e);
                    }
                }
            }
        });
    }

    @Subscribe
    @AllowConcurrentEvents
    public void handle(final TemplatizeVmEvent event) {
        // Use _vmOpExecutor to throttle vm operations.
        _vmOpExecutor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    final AsyncCallback callback = event.getCallback();

                    TemplatizedVm templatizedVm = _vmManager.templatizeVm(event.getContext(), event.getVmRefId(),
                            event.isEnableCbrc(), event.getVcContext(), event.getHostEsxUuid(),
                            (callback == null) ? null : new ActionDelegate<Integer>() {
                                @Override
                                public void action(Integer param) {
                                    callback.progress(param);
                                }
                    });

                    if (callback != null) {
                        callback.onSuccess(templatizedVm);
                    }
                } catch (Exception e) {
                    LOGGER.error(String.format("Failed to templatize VM id=\"%s\"", event.getVmRefId()), e);

                    if (event.getCallback() != null) {
                        event.getCallback().onFailure(e);
                    }
                }
            }
        });
    }

    @Subscribe
    @AllowConcurrentEvents
    public void handle(final PowerOpVmEvent event) {
        // Use _vmOpExecutor to throttle vm operations.
        _vmOpExecutor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    final AsyncCallback callback = event.getCallback();

                    switch (event.getPowerOp()) {
                    case PowerOn:
                        _vmManager.powerOnVm(event.getContext(), event.getVmRefId(),
                                (callback == null) ? null : new ActionDelegate<Integer>() {
                                    @Override
                                    public void action(Integer param) {
                                        callback.progress(param);
                                    }
                        });
                        break;

                    case PowerOff:
                        _vmManager.shutdownVm(event.getContext(), event.getVmRefId(),
                                (callback == null) ? null : new ActionDelegate<Integer>() {
                                    @Override
                                    public void action(Integer param) {
                                        callback.progress(param);
                                    }
                        });
                        break;

                    case Suspend:
                        _vmManager.suspendVm(event.getContext(), event.getVmRefId(),
                                (callback == null) ? null : new ActionDelegate<Integer>() {
                                    @Override
                                    public void action(Integer param) {
                                        callback.progress(param);
                                    }
                        });
                        break;
                    case Reset:
                        _vmManager.resetVm(event.getContext(), event.getVmRefId(),
                                (callback == null) ? null : new ActionDelegate<Integer>() {
                                    @Override
                                    public void action(Integer param) {
                                        callback.progress(param);
                                    }
                        });
                        break;
                    }

                    if (callback != null) {
                        callback.onSuccess(null);
                    }
                } catch (Exception e) {
                    LOGGER.error(String.format("Failed to %s VM id=\"%s\"", event.getPowerOp(), event.getVmRefId()), e);

                    if (event.getCallback() != null) {
                        event.getCallback().onFailure(e);
                    }
                }
            }
        });
    }

    @Subscribe
    @AllowConcurrentEvents
    public void handle(final RefreshVmEvent event) {
        // Use _vmOpExecutor to throttle vm operations.
        _vmOpExecutor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    final AsyncCallback callback = event.getCallback();

                    _vmManager.refreshVm(event.getContext(), event.getVmRefId(),
                            (callback == null) ? null : new ActionDelegate<Integer>() {
                                @Override
                                public void action(Integer param) {
                                    callback.progress(param);
                                }
                    });

                    if (callback != null) {
                        callback.onSuccess(null);
                    }
                } catch (Exception e) {
                    LOGGER.error(String.format("Failed to refresh VM id=\"%s\"", event.getVmRefId()), e);

                    if (event.getCallback() != null) {
                        event.getCallback().onFailure(e);
                    }
                }
            }
        });
    }

    @Subscribe
    @AllowConcurrentEvents
    public void handle(final SnapshotVmEvent event) {
        // Use _vmOpExecutor to throttle vm operations.
        _vmOpExecutor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    final AsyncCallback callback = event.getCallback();

                    _vmManager.snapshotVm(event.getContext(), event.getVmRefId(), event.isIncludeMemory(),
                            (callback == null) ? null : new ActionDelegate<Integer>() {
                                @Override
                                public void action(Integer param) {
                                    callback.progress(param);
                                }
                    });

                    if (callback != null) {
                        callback.onSuccess(null);
                    }
                } catch (Exception e) {
                    LOGGER.error(String.format("Failed to snapshot VM id=\"%s\"", event.getVmRefId()), e);

                    if (event.getCallback() != null) {
                        event.getCallback().onFailure(e);
                    }
                }
            }
        });
    }

    @Subscribe
    @AllowConcurrentEvents
    public void handle(final AttachVmDiskEvent event) {
        // Use _vmOpExecutor to throttle vm operations.
        _vmOpExecutor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    final User user = event.getUser();
                    final Storage storage = event.getStorage();
                    final AsyncCallback callback = event.getCallback();

                    String uddDiskPath = String.format("UDD/%s/%s.vmdk", user.getDistinguishedName(), event.getDiskName());
                    DatastoreFile uddDsFile = new DatastoreFile(storage.getName(), uddDiskPath);

                    boolean attachedDisk = _vmManager.attachVmDisk(event.getContext(), event.getVmRefId(), uddDsFile,
                            event.getSizeInMB(), storage,
                            (callback == null) ? null : new ActionDelegate<Integer>() {
                                @Override
                                public void action(Integer param) {
                                    callback.progress(param);
                                }
                    });

                    if (callback != null) {
                        callback.onSuccess(attachedDisk);
                    }
                } catch (Exception e) {
                    LOGGER.error(String.format("Failed to snapshot VM id=\"%s\"", event.getVmRefId()), e);

                    if (event.getCallback() != null) {
                        event.getCallback().onFailure(e);
                    }
                }
            }
        });
    }

    @Subscribe
    @AllowConcurrentEvents
    public void handle(final DetachVmAllUserDisksEvent event) {
        // Use _vmOpExecutor to throttle vm operations.
        _vmOpExecutor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    final Storage storage = event.getStorage();
                    final AsyncCallback callback = event.getCallback();

                    DatastoreFile uddDsParentDir = new DatastoreFile(storage.getName(), "UDD/");

                    _vmManager.detachVmAllDisks(event.getContext(), event.getVmRefId(), uddDsParentDir.getFullPath(),
                            (callback == null) ? null : new ActionDelegate<Integer>() {
                                @Override
                                public void action(Integer param) {
                                    callback.progress(param);
                                }
                    });

                    if (callback != null) {
                        callback.onSuccess(null);
                    }
                } catch (Exception e) {
                    LOGGER.error(String.format("Failed to snapshot VM id=\"%s\"", event.getVmRefId()), e);

                    if (event.getCallback() != null) {
                        event.getCallback().onFailure(e);
                    }
                }
            }
        });
    }
}

