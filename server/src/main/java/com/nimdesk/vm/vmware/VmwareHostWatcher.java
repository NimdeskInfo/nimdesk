/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.vm.vmware;

import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.util.Assert;

import com.nimdesk.hypervisor.vmware.VmwareContext;
import com.nimdesk.hypervisor.vmware.mo.HostMO;
import com.nimdesk.hypervisor.vmware.mo.ResourcePoolMO;
import com.nimdesk.hypervisor.vmware.mo.VirtualMachineMO;
import com.nimdesk.hypervisor.vmware.util.VmwareUtils;
import com.nimdesk.model.Host;
import com.nimdesk.model.Server;
import com.nimdesk.vm.VmStateListener;
import com.vmware.vim25.HostHardwareSummary;
import com.vmware.vim25.HostListSummary;
import com.vmware.vim25.InvalidProperty;
import com.vmware.vim25.ManagedObjectReference;
import com.vmware.vim25.ObjectContent;
import com.vmware.vim25.ObjectSpec;
import com.vmware.vim25.ObjectUpdate;
import com.vmware.vim25.PropertyChange;
import com.vmware.vim25.PropertyChangeOp;
import com.vmware.vim25.PropertyFilterSpec;
import com.vmware.vim25.PropertyFilterUpdate;
import com.vmware.vim25.PropertySpec;
import com.vmware.vim25.ResourceAllocationInfo;
import com.vmware.vim25.ResourceConfigSpec;
import com.vmware.vim25.RuntimeFault;
import com.vmware.vim25.SelectionSpec;
import com.vmware.vim25.SharesInfo;
import com.vmware.vim25.SharesLevel;
import com.vmware.vim25.TraversalSpec;
import com.vmware.vim25.UpdateSet;
import com.vmware.vim25.VirtualMachinePowerState;
import com.vmware.vim25.mo.ClusterComputeResource;
import com.vmware.vim25.mo.ContainerView;
import com.vmware.vim25.mo.Folder;
import com.vmware.vim25.mo.HostSystem;
import com.vmware.vim25.mo.ManagedEntity;
import com.vmware.vim25.mo.PropertyCollector;
import com.vmware.vim25.mo.PropertyFilter;
import com.vmware.vim25.mo.ServiceInstance;
import com.vmware.vim25.mo.View;
import com.vmware.vim25.mo.ViewManager;
import com.vmware.vim25.mo.util.MorUtil;

public class VmwareHostWatcher {
    private static final Logger LOGGER = Logger.getLogger(VmwareHostWatcher.class);

    private static final String MOR_VIRTUALMACHINE = "VirtualMachine";

    private static final String PROP_RUNTIME_POWERSTATE = "runtime.powerState";
    private static final String PROP_NAME = "name";

    private final Server _localServer;
    private Host _host;
    private final Host.Service _hostService;
    private final ExecutorService _watcherExceutor = Executors.newSingleThreadExecutor();

    private static final Object PRESENT = new Object();

    private final Map<VmStateListener, Object> _listeners =
            Collections.synchronizedMap(new WeakHashMap<VmStateListener, Object>());

    public VmwareHostWatcher(Server localServer, Host host, Host.Service hostService) {
        _localServer = localServer;
        Assert.notNull(_localServer);

        _host = host;
        Assert.notNull(_host);

        _hostService = hostService;
        Assert.notNull(_hostService, "A Host.Service must be set");
    }

    private VmwareContext connect() throws MalformedURLException {
        try {
            return VmwareContext.connect(_host.getAddress(), 0,
                    _host.getUsername(), _host.getPassword());
        } catch (MalformedURLException e) {
            LOGGER.error(String.format("Wrong address for host: %s - %s",
                    _host.getAddress(), e.getMessage()));
            throw e;
        } catch (Exception e) {
            LOGGER.warn(String.format("Unable to connect to server \"%s\": %s",
                    _host.getAddress(), e.getMessage()));
            return null;
        }
    }

    public void addVmStateListener(VmStateListener listener) {
        _listeners.put(listener, PRESENT);
    }

    public void removeVmStateListener(VmStateListener listener) {
        _listeners.remove(listener);
    }

    protected void fireNewEvent(String vmRefId, VmStateListener.State state) {
        Set<VmStateListener> listenerSet = new HashSet<VmStateListener>();

        synchronized(_listeners) {
            listenerSet.addAll(_listeners.keySet());
        }

        for (VmStateListener listener : listenerSet) {
            try {
                listener.onNew(vmRefId, state);
            } catch (Throwable t) {
                LOGGER.error("Listener.onNew failed", t);
            }
        }
    }

    protected void fireDeleteEvent(String vmRefId) {
        Set<VmStateListener> listenerSet = new HashSet<VmStateListener>();

        synchronized(_listeners) {
            listenerSet.addAll(_listeners.keySet());
        }

        for (VmStateListener listener : listenerSet) {
            try {
                listener.onDelete(vmRefId);
            } catch (Throwable t) {
                LOGGER.error("Listener.onDelete failed", t);
            }
        }
    }

    protected void firePowerStateChangeEvent(String vmRefId, VmStateListener.State state) {
        Set<VmStateListener> listenerSet = new HashSet<VmStateListener>();

        synchronized(_listeners) {
            listenerSet.addAll(_listeners.keySet());
        }

        for (VmStateListener listener : listenerSet) {
            try {
                listener.onPowerStateChange(vmRefId, state);
            } catch (Throwable t) {
                LOGGER.error("Listener.onPowerStateChange failed", t);
            }
        }
    }

    // Check resource pool and if not exist, create one.
    private void prepareHost(VmwareContext context, HostMO hostMO)
            throws InvalidProperty, RuntimeFault, RemoteException {
        LOGGER.info(String.format("Preparing host \"%s\"...", _host.getAddress()));

        ResourcePoolMO ownerRp;
        if (hostMO == null) {
            ownerRp = context.getRootResourcePool();
        } else {
            ownerRp = hostMO.getOwnerResourcePool();
        }

        ResourcePoolMO nimdeskRp = context.getNimdeskResourcePool(ownerRp);

        if (nimdeskRp == null) {
            int hostCount = 1;

            // If this is cluster, find out the number of hosts on it.
            if (hostMO != null) {
                ManagedEntity parent = hostMO.getHostSystem().getParent();
                if (parent instanceof ClusterComputeResource) {
                    ClusterComputeResource cluster = (ClusterComputeResource) parent;
                    HostSystem[] hosts = cluster.getHosts();
                    if (hosts != null && hosts.length > 0) {
                        hostCount = hosts.length;
                    }
                }
            }

            // Create nimdesk resource pool.
            LOGGER.info(String.format("No nimdesk resource pool on Host \"%s\". Creating one...", _host.getAddress()));
            ResourceConfigSpec rootConfigSpec = ownerRp.getConfig();

            SharesInfo cpuSharesInfo = new SharesInfo();
            cpuSharesInfo.setShares(0/*rootConfigSpec.getCpuAllocation().getShares().getShares()*/);
            cpuSharesInfo.setLevel(SharesLevel.normal);

            ResourceAllocationInfo cpuAllocation = new ResourceAllocationInfo();
            cpuAllocation.setReservation(0L);
            cpuAllocation.setLimit(rootConfigSpec.getCpuAllocation().getLimit() - hostCount * 1024); // 1024 MHz
            cpuAllocation.setExpandableReservation(true);
            cpuAllocation.setShares(cpuSharesInfo);

            SharesInfo memorySharesInfo = new SharesInfo();
            memorySharesInfo.setShares(0/*rootConfigSpec.getMemoryAllocation().getShares().getShares()*/);
            memorySharesInfo.setLevel(SharesLevel.normal);

            ResourceAllocationInfo memoryAllocation = new ResourceAllocationInfo();
            memoryAllocation.setReservation(0L);
            memoryAllocation.setLimit(rootConfigSpec.getMemoryAllocation().getLimit() - hostCount * 4 * 1024); // 4096 MB
            memoryAllocation.setExpandableReservation(true);
            memoryAllocation.setShares(memorySharesInfo);

            ResourceConfigSpec nimdeskConfigSpec = new ResourceConfigSpec();
            nimdeskConfigSpec.setCpuAllocation(cpuAllocation);
            nimdeskConfigSpec.setMemoryAllocation(memoryAllocation);

            ownerRp.createResourcePool("nimdesk", nimdeskConfigSpec);
        }
    }

    public void start() {
        LOGGER.info("Starting watcher for host - " + _host.getName());

        _watcherExceutor.execute(new Runnable() {
            @Override
            public void run() {
                if (Host.Status.Connecting != _host.getStatus()) {
                    _host.setStatus(Host.Status.Connecting);

                    Host host = _hostService.getByUuid(_host.getUuid());
                    if (host != null) {
                        host.setStatus(Host.Status.Connecting);
                        _hostService.commit(host);
                    }
                }

                VmwareContext context = null;

                while (true) {
                    PropertyFilter propFilter = null;

                    try {
                        if (context != null) {
                            context.disconnect();
                        }

                        context = connect();

                        ServiceInstance serviceInstance = null;
                        if (context != null) {
                            serviceInstance = context.getServiceInstance();
                        }
                        if (serviceInstance == null) {
                            if (Host.Status.Disconnected != _host.getStatus()) {
                                _host.setStatus(Host.Status.Disconnected);

                                Host host = _hostService.getByUuid(_host.getUuid());
                                if (host != null) {
                                    host.setStatus(Host.Status.Disconnected);
                                    _hostService.commit(host);
                                }
                            }

                            if (context != null) {
                                context.disconnect();
                                context = null;
                            }

                            Thread.sleep(300 * 1000); // Wait 5 minutes and retry
                            continue;
                        }

                        Folder rootFolder = serviceInstance.getRootFolder();
                        ViewManager viewManager = serviceInstance.getViewManager();
                        PropertyCollector propCollector = serviceInstance.getPropertyCollector();

                        HostSystem hostSystem = null;
                        ManagedEntity container;
                        if (StringUtils.isEmpty(_host.getRefId())) {
                            container = rootFolder;
                        } else {
                            ManagedObjectReference hostMor = new ManagedObjectReference();
                            hostMor.set_value(_host.getRefId());
                            hostMor.setType("HostSystem");
                            container = MorUtil.createExactManagedEntity(rootFolder.getServerConnection(), hostMor);

                            hostSystem = (HostSystem) container;
                        }

                        if (Host.Status.Connected != _host.getStatus()) {
                            _host.setStatus(Host.Status.Connected);

                            String esxUuid = null;
                            int cpuCapacityMHz = -1;
                            int memoryCapacityMB = -1;
                            String vCenterIp = null;

                            if (hostSystem != null) {
                                HostListSummary hostSummary = hostSystem.getSummary();
                                HostHardwareSummary hostHwSummary;
                                if (hostSummary != null) {
                                    if ((hostHwSummary = hostSummary.getHardware()) != null) {
                                        esxUuid = hostHwSummary.getUuid();
                                        cpuCapacityMHz = hostHwSummary.getCpuMhz() * hostHwSummary.getNumCpuCores();
                                        memoryCapacityMB = (int) (hostHwSummary.getMemorySize() / (1024 * 1024));
                                    }

                                    vCenterIp = hostSummary.getManagementServerIp();

                                    // For debugging:
                                    /*
                                    if (StringUtils.isEmpty(vCenterIp)) {
                                        vCenterIp = _host.getAddress();
                                    }
                                    */
                                }
                            }

                            Host host = _hostService.getByUuid(_host.getUuid());
                            if (host != null) {
                                host.setStatus(Host.Status.Connected);
                                host.setEsxUuid(esxUuid);
                                if (cpuCapacityMHz >= 0) {
                                    host.setCpuCapacityMHz(cpuCapacityMHz);
                                }
                                if (memoryCapacityMB >= 0) {
                                    host.setMemoryCapacityMB(memoryCapacityMB);
                                }
                                host.setVCenterIp(vCenterIp);
                                //if (StringUtils.isEmpty(vCenterIp)) {
                                //    host.setVCenterUsername(null);
                                //    host.setVCenterPassword(null);
                                //}

                                _hostService.commit(host);
                            }
                            _host = host;

                            VmwareContext prepContext = context;
                            try {
                                if (!StringUtils.isEmpty(host.getVCenterIp())) {
                                    try {
                                        String vCenterAddress = host.getVCenterAddress();
                                        if (StringUtils.isEmpty(vCenterAddress)) {
                                            vCenterAddress = host.getVCenterIp();
                                        }

                                        prepContext = VmwareContext.connect(vCenterAddress, 0,
                                                host.getVCenterUsername(), host.getVCenterPassword());
                                    } catch (Exception e) {
                                        LOGGER.warn(String.format("Failed to connect to vCenter \"%s\"", host.getVCenterIp()), e);
                                    }
                                }
                                HostMO vcHostMO = null;
                                if (prepContext != context) {
                                    vcHostMO = HostMO.findByEsxUuid(prepContext, host.getEsxUuid());
                                }

                                // Reserver resource (512MHz cpu, 1024MB mem) for VA itself.
                                try {
                                    HostMO hostMO;
                                    if (vcHostMO == null) {
                                        hostMO = new HostMO(context, host.getRefId());
                                    } else {
                                        hostMO = vcHostMO;
                                    }

                                    ObjectContent[] ocs = hostMO.getVmProperties(new String[] {"guest.ipAddress"});
                                    if (ocs != null) {
                                        for (ObjectContent oc : ocs) {
                                            String ipAddress = (String) VmwareUtils.getPropValue(oc, "guest.ipAddress");
                                            if (StringUtils.equals(ipAddress, _localServer.getPublicAddress())) {
                                                VirtualMachineMO vm = new VirtualMachineMO(hostMO.getContext(), oc.getObj());
                                                vm.reserveResource(512, 1024, null);
                                                break;
                                            }
                                        }
                                    }
                                } catch (Exception e) {
                                    LOGGER.warn(String.format("Failed to reserve resource for VA on host \"%s\", continue.",
                                            _host.getAddress()), e);
                                }

                                // Prepare host to create ResourcePool "nimdesk".
                                try {
                                    prepareHost(prepContext, vcHostMO);
                                } catch (Exception e) {
                                    LOGGER.warn(String.format("Failed to prepare host \"%s\", continue.", _host.getAddress()), e);
                                }
                            } finally {
                                if (prepContext != context && prepContext != null) {
                                    prepContext.disconnect();
                                }
                            }
                        }

                        // Create a ContainerView with all VirtualMachine objects
                        // covered recursively
                        ContainerView containerView = viewManager.createContainerView(container/*rootFolder*/,
                                  new String[] { MOR_VIRTUALMACHINE }, true);
                        PropertyFilterSpec propFilterSpec = new PropertyFilterSpec();
                        propFilterSpec.setObjectSet(new ObjectSpec[] {
                                createObjSpec(containerView)
                        });
                        propFilterSpec.setPropSet(new PropertySpec[]{
                                createPropSpec(MOR_VIRTUALMACHINE, new String[]{
                                        PROP_NAME,
                                        PROP_RUNTIME_POWERSTATE
                                })
                        });

                        // Create a Property with partialUpdate as true
                        propFilter = propCollector.createFilter(propFilterSpec, true);

                        //wait for initial update with empty version string
                        //WaitOptions waitOps = new WaitOptions();
                        //waitOps.setMaxWaitSeconds(Integer.MAX_VALUE);
                        //waitOps.setMaxObjectUpdates(Integer.MAX_VALUE);
                        String version = "";
                        boolean refresh = true;

                        try {
                            while(true) {
                                @SuppressWarnings("deprecation")
                                UpdateSet updateSet = propCollector.waitForUpdates(version);
                                version = updateSet.getVersion();

                                processUpdateSet(updateSet, refresh);
                                if (refresh) {
                                    refresh = false;
                                }
                            }
                        } finally {
                            if (propFilter != null) {
                                propFilter.destroyPropertyFilter();
                            }
                        }
                    } catch (Exception e) {
                        if (e instanceof InterruptedException) {
                            break;
                        } else if (e instanceof MalformedURLException) {
                            break;
                        }
                    } finally {
                        if (context != null) {
                            context.disconnect();
                            context = null;
                        }
                    }

                    _host.setStatus(Host.Status.Connecting);

                    Host host = _hostService.getByUuid(_host.getUuid());
                    if (host != null) {
                        host.setStatus(Host.Status.Connecting);
                        _hostService.commit(host);
                    }
                }
            }

            private ObjectSpec createObjSpec(View view) {
                ObjectSpec oSpec = new ObjectSpec();
                oSpec.setSkip(true); //skip this ContainerView object
                oSpec.setObj(view.getMOR());
                TraversalSpec tSpec = new TraversalSpec();
                tSpec.setType(view.getMOR().getType());
                tSpec.setPath("view");
                oSpec.setSelectSet(new SelectionSpec[] {tSpec});
                return oSpec;
            }

            private PropertySpec createPropSpec(String type, String[] props) {
                PropertySpec pSpec = new PropertySpec();
                pSpec.setType(type);
                pSpec.setAll(Boolean.FALSE);
                pSpec.setPathSet(props);
                return pSpec;
            }
        });
    }

    public void stop() {
        LOGGER.info("Stoping watcher for host - " + _host.getName());

        _watcherExceutor.shutdownNow();
        try {
            _watcherExceutor.awaitTermination(10, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            LOGGER.warn("Wait for watcher timeout for host - " + _host.getName());
        }
    }

    private void processUpdateSet(UpdateSet updateSet, boolean refresh) {
        PropertyFilterUpdate[] propFilterUpdates = updateSet.getFilterSet();
        if (propFilterUpdates == null) {
            return;
        }

        ObjectAddRemoveHandler handler = new ObjectAddRemoveHandler();

        for (PropertyFilterUpdate propFilterUpdate : propFilterUpdates) {
            ObjectUpdate[] objUpdatess = propFilterUpdate.getObjectSet();
            if (objUpdatess == null) {
                continue;
            }

            for (ObjectUpdate objUpdate : objUpdatess) {
                switch (objUpdate.kind) {
                case enter:
                    handler.addObject(objUpdate);
                    break;

                case modify:
                    handler.changeObject(objUpdate);
                    break;

                case leave:
                    handler.removeObject(objUpdate);
                    continue;
                }
            }
        }
    }

    class ObjectAddRemoveHandler {
        public void addObject(ObjectUpdate objUpdate) {
            PropertyChange[] propChanges = objUpdate.getChangeSet();
            if (propChanges == null) {
                return;
            }

            if (MOR_VIRTUALMACHINE.equalsIgnoreCase(objUpdate.getObj().getType())) {
                ManagedObjectReference vmMor = objUpdate.getObj();
                VirtualMachinePowerState powerState = null;

                for (PropertyChange propChange : propChanges) {
                    if ((propChange.op == PropertyChangeOp.remove)
                            || (propChange.op == PropertyChangeOp.indirectRemove)) {
                        continue;
                    }
                    if (propChange.getVal() == null) {
                        continue;
                    }

                    if (PROP_RUNTIME_POWERSTATE.equalsIgnoreCase(propChange.getName())) {
                        powerState = (VirtualMachinePowerState) propChange.getVal();
                    }/* else if (PROP_RUNTIME_HOST.equalsIgnoreCase(propChange.getName())) {
                        vmwareVm.hostMor = (ManagedObjectReference) propChange.getVal();
                    } else if (PROP_GUEST_HOSTNAME.equalsIgnoreCase(propChange.getName())) {
                        vmwareVm.hostName = (String) propChange.getVal();
                    } else if (PROP_GUEST_IPADDRESS.equalsIgnoreCase(propChange.getName())) {
                        vmwareVm.ip = (String) propChange.getVal();
                    }*/
                }

                VmStateListener.State vmState = null;
                if (powerState != null) {
                    switch (powerState) {
                    case poweredOn:
                        vmState = VmStateListener.State.PoweredOn;
                        break;

                    case suspended:
                        vmState = VmStateListener.State.Suspended;
                        break;

                    case poweredOff:
                    default:
                        vmState = VmStateListener.State.PoweredOff;
                        break;
                    }
                }

                fireNewEvent(vmMor.get_value(), vmState);
            }
        }

        public void removeObject(ObjectUpdate objUpdate) {
            if (MOR_VIRTUALMACHINE.equalsIgnoreCase(objUpdate.getObj().getType())) {
                ManagedObjectReference vmMor = objUpdate.getObj();
                fireDeleteEvent(vmMor.get_value());
            }
        }

        private void changeObject(ObjectUpdate objUpdate) {
            PropertyChange[] propChanges = objUpdate.getChangeSet();
            if (propChanges == null) {
                return;
            }

            if (MOR_VIRTUALMACHINE.equalsIgnoreCase(objUpdate.getObj().getType())) {
                ManagedObjectReference vmMor = objUpdate.getObj();
                VmStateListener.State vmState = null;

                for (PropertyChange propChange : propChanges) {
                    Object value;
                    if ((propChange.op == PropertyChangeOp.remove)
                            || (propChange.op == PropertyChangeOp.indirectRemove)) {
                        value = null;
                    } else {
                        value = propChange.getVal();
                    }

                    if (PROP_RUNTIME_POWERSTATE.equalsIgnoreCase(propChange.getName())) {
                        if (value != null) {
                            switch ((VirtualMachinePowerState) value) {
                            case poweredOn:
                                vmState = VmStateListener.State.PoweredOn;
                                break;

                            case suspended:
                                vmState = VmStateListener.State.Suspended;
                                break;

                            case poweredOff:
                            default:
                                vmState = VmStateListener.State.PoweredOff;
                                break;
                            }
                            firePowerStateChangeEvent(vmMor.get_value(), vmState);
                        }
                    }
                }
            }
        }
    }
}
