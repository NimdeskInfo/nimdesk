/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.vm.vmware;

import java.rmi.RemoteException;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.util.Assert;

import com.nimdesk.hypervisor.HypervisorContext;
import com.nimdesk.hypervisor.vmware.VmwareContext;
import com.nimdesk.hypervisor.vmware.mo.CbrcManagerMO;
import com.nimdesk.hypervisor.vmware.mo.DatacenterMO;
import com.nimdesk.hypervisor.vmware.mo.DatastoreMO;
import com.nimdesk.hypervisor.vmware.mo.HostMO;
import com.nimdesk.hypervisor.vmware.mo.VirtualMachineMO;
import com.nimdesk.hypervisor.vmware.util.DatastoreFile;
import com.nimdesk.hypervisor.vmware.util.VmwareUtils;
import com.nimdesk.model.Storage;
import com.nimdesk.util.ActionDelegate;
import com.nimdesk.vm.InstantiatedVm;
import com.nimdesk.vm.TemplatizedVm;
import com.vmware.vim25.AlreadyExists;
import com.vmware.vim25.CbrcDeviceSpec;
import com.vmware.vim25.DuplicateName;
import com.vmware.vim25.FileFault;
import com.vmware.vim25.InsufficientResourcesFault;
import com.vmware.vim25.InvalidDatastore;
import com.vmware.vim25.InvalidName;
import com.vmware.vim25.NotFound;
import com.vmware.vim25.ObjectContent;
import com.vmware.vim25.OutOfBounds;
import com.vmware.vim25.RuntimeFault;
import com.vmware.vim25.VirtualDevice;
import com.vmware.vim25.VirtualDisk;
import com.vmware.vim25.VirtualEthernetCard;
import com.vmware.vim25.VirtualMachineConfigInfo;
import com.vmware.vim25.VmConfigFault;

public class VmwareVmManager {
    private static final Logger LOGGER = Logger.getLogger(VmwareVmManager.class);

    public void powerOnVm(HypervisorContext ctx, String vmRefId, ActionDelegate<Integer> progressUpdater)
            throws Exception {
        LOGGER.info(String.format("Powering on VM id=\"%s\"...", vmRefId));

        if (StringUtils.isEmpty(vmRefId)) {
            return;
        }

        Assert.isInstanceOf(VmwareContext.class, ctx, "Invalid hypervisor context - expect Vmware");

        VmwareContext context = (VmwareContext) ctx;
        VirtualMachineMO vm = new VirtualMachineMO(context, vmRefId);

        if (!vm.powerOn(progressUpdater)) {
            LOGGER.error(String.format("Failed to power on VM id=\"%s\"", vmRefId));
            throw new Exception("Failed to power on VM");
        }
    }

    public void shutdownVm(HypervisorContext ctx, String vmRefId, ActionDelegate<Integer> progressUpdater)
            throws Exception {
        LOGGER.info(String.format("Shuting down VM id=\"%s\"...", vmRefId));

        if (StringUtils.isEmpty(vmRefId)) {
            return;
        }

        Assert.isInstanceOf(VmwareContext.class, ctx, "Invalid hypervisor context - expect Vmware");

        VmwareContext context = (VmwareContext) ctx;
        VirtualMachineMO vm = new VirtualMachineMO(context, vmRefId);

        if (!vm.safePowerOff(30 * 1000, progressUpdater)) {
            // Wait 30 seconds for safe shutdown
            LOGGER.error(String.format("Failed to shut down VM id=\"%s\"", vmRefId));
            throw new Exception("Failed to shut down VM");
        }
    }

    private void shutdownVm(VirtualMachineMO vm, ActionDelegate<Integer> progressUpdater)
            throws Exception {
        LOGGER.info(String.format("Shuting down VM id=\"%s\"...", vm.getMor().get_value()));

        if (!vm.safePowerOff(30 * 1000, progressUpdater)) {
            // Wait 30 seconds for safe shutdown
            LOGGER.error(String.format("Failed to shut down VM id=\"%s\"", vm.getMor().get_value()));
            throw new Exception("Failed to shut down VM");
        }
    }

    public void resetVm(HypervisorContext ctx, String vmRefId, ActionDelegate<Integer> progressUpdater)
            throws Exception {
        LOGGER.info(String.format("Power reset VM id=\"%s\"...", vmRefId));

        if (StringUtils.isEmpty(vmRefId)) {
            return;
        }

        Assert.isInstanceOf(VmwareContext.class, ctx, "Invalid hypervisor context - expect Vmware");

        VmwareContext context = (VmwareContext) ctx;
        VirtualMachineMO vm = new VirtualMachineMO(context, vmRefId);

        // try reset, if fails (might be in power-off state), try powerOn.
        if (!vm.safePowerOff(30 * 1000, progressUpdater) || !vm.powerOn(progressUpdater)) {
            LOGGER.error(String.format("Failed to power reset VM id=\"%s\"", vmRefId));
            throw new Exception("Failed to power reset VM");
        }
    }

    public void suspendVm(HypervisorContext ctx, String vmRefId, ActionDelegate<Integer> progressUpdater)
            throws Exception {
        LOGGER.info(String.format("Suspending VM id=\"%s\"...", vmRefId));

        if (StringUtils.isEmpty(vmRefId)) {
            return;
        }

        Assert.isInstanceOf(VmwareContext.class, ctx, "Invalid hypervisor context - expect Vmware");

        VmwareContext context = (VmwareContext) ctx;
        VirtualMachineMO vm = new VirtualMachineMO(context, vmRefId);

        if (!vm.suspend(progressUpdater)) {
            LOGGER.error(String.format("Failed to suspend VM id=\"%s\"", vmRefId));
            throw new Exception("Failed to suspend VM");
        }
    }

    public InstantiatedVm instantiateVm(HypervisorContext ctx, String hostRefId, boolean poweronVm,
            String vmName, String vmxPath, long cpuCount, long memoryMB, boolean reserveMemory, String macAddress, boolean enableCbrc,
            HypervisorContext vcCtx, String hostEsxUuid, ActionDelegate<Integer> progressUpdater)
            throws Exception {
        LOGGER.info(String.format("Instantiating VM \"%s\"...", vmName));

        Assert.isInstanceOf(VmwareContext.class, ctx, "Invalid hypervisor context - expect Vmware");

        VmwareContext context = (VmwareContext) ctx;
        HostMO host = new HostMO(context, hostRefId);

        VmwareContext vcContext = null;
        HostMO vcHost = null;
        if (vcCtx instanceof VmwareContext && !StringUtils.isEmpty(hostEsxUuid)) {
            vcContext = (VmwareContext) vcCtx;
            vcHost = HostMO.findByEsxUuid(vcContext, hostEsxUuid);
        }

        VirtualMachineMO vm = doRegisterVM(context, host, vcHost, vmName, vmxPath, progressUpdater);

        try {
            vm.detachIso(progressUpdater);
        } catch (Exception e) {
            LOGGER.warn(String.format("Failed to detach ISO for VM \"%s\" in instantiateVm",  vmName), e);
        }

        if (cpuCount > 0 || memoryMB > 0 || !StringUtils.isEmpty(macAddress)) {
            VirtualMachineMO configVm = vm;

            if (vcHost != null) {
                // Find the vm.
                ObjectContent[] ocs = vcHost.getVmProperties(new String[] {"summary.config.vmPathName"});
                if (ocs != null) {
                    for (ObjectContent oc : ocs) {
                        String path = (String) VmwareUtils.getPropValue(oc, "summary.config.vmPathName");
                        if (StringUtils.equals(path, vmxPath)) {
                            configVm = new VirtualMachineMO(vcContext, oc.getObj());
                            break;
                        }
                    }
                }
            }

            if (!configVm.reconfig(cpuCount, memoryMB, reserveMemory, macAddress, progressUpdater)) {
                LOGGER.error(String.format("Unable to reconfig the registered VM \"%s\"", vmName));
                throw new Exception("Unable to reconfig the registered VM");
            }
        }

        if (enableCbrc) {
            try {
                enableCbrc = enableVmCbrc(ctx, vm.getMor().get_value(), true, vcCtx, hostEsxUuid, progressUpdater);
            } catch (Exception e) {
                LOGGER.warn(String.format("Failed to enable CBRC for VM \"%s\" in instantiateVm",  vmName), e);
            }
        }

        // Power on the VM
        if (poweronVm && !vm.powerOn(progressUpdater)) {
            LOGGER.error(String.format("Unable to power on the registered VM \"%s\"", vmName));
            throw new Exception("Unable to power on the registered VM");
        }

        if (StringUtils.isEmpty(macAddress)) {
            VirtualMachineConfigInfo vmConfigInfo = vm.getConfigInfo();
            VirtualDevice devices[] = vmConfigInfo.getHardware().getDevice();
            if (devices != null) {
                for (VirtualDevice device : devices) {
                    if (device instanceof VirtualEthernetCard) {
                        VirtualEthernetCard nic = (VirtualEthernetCard) device;
                        macAddress = nic.getMacAddress();
                        break;
                    }
                }
            }
        }

        InstantiatedVm instantiatedVm = new InstantiatedVm();
        instantiatedVm.setRefId(vm.getMor().get_value());
        instantiatedVm.setMacAddress(macAddress);
        instantiatedVm.setCbrcEnabled(enableCbrc);

        return instantiatedVm;
    }

    public String uninstantiateVm(HypervisorContext ctx, String vmRefId,
            HypervisorContext vcCtx, String hostEsxUuid, ActionDelegate<Integer> progressUpdater)
            throws Exception {
        LOGGER.info(String.format("Uninstantiating VM id=\"%s\"...", vmRefId));

        if (StringUtils.isEmpty(vmRefId)) {
            return null;
        }

        Assert.isInstanceOf(VmwareContext.class, ctx, "Invalid hypervisor context - expect Vmware");

        VmwareContext context = (VmwareContext) ctx;
        VirtualMachineMO vm = new VirtualMachineMO(context, vmRefId);

        String vmxPath = null;
        try {
            vmxPath = vm.getVmxPath();
        } catch (Exception e) {
            LOGGER.warn(String.format("Failed to get vmx path for VM id=\"%s\", continue.", vmRefId));
        }

        boolean vmUnregistered = false;

        if (vcCtx instanceof VmwareContext
                && !StringUtils.isEmpty(hostEsxUuid)
                && !StringUtils.isEmpty(vmxPath)) {
            // Try to unregister the vm from vCenter first.
            VmwareContext vcContext = (VmwareContext) vcCtx;
            HostMO vcHost = HostMO.findByEsxUuid(vcContext, hostEsxUuid);
            if (vcHost != null) {
                // Find the vm.
                ObjectContent[] ocs = vcHost.getVmProperties(new String[] {"summary.config.vmPathName"});
                if (ocs != null) {
                    for (ObjectContent oc : ocs) {
                        String path = (String) VmwareUtils.getPropValue(oc, "summary.config.vmPathName");
                        if (StringUtils.equals(path, vmxPath)) {
                            VirtualMachineMO vcVm = new VirtualMachineMO(vcContext, oc.getObj());
                            try {
                                try {
                                    shutdownVm(vcVm, progressUpdater);
                                } catch (Exception e) {
                                    LOGGER.warn(String.format("Failed to shut down vm id=\"%s\", continue.", vmRefId));
                                }

                                vcVm.unregister();
                                vmUnregistered = true;
                            } catch (Exception e) {
                                LOGGER.warn(String.format("Failed to un-register from vCenter: vmx=\"%s\", try on host.", vmxPath));
                            }
                            break;
                        }
                    }
                }
            }
        }

        if (!vmUnregistered) {
            try {
                shutdownVm(vm, progressUpdater);
            } catch (Exception e) {
                LOGGER.warn(String.format("Failed to shut down vm id=\"%s\", continue.", vmRefId));
            }

            // If vm is not unregistered, try to do it on the host directly.
            try {
                // Unregister VM
                vm.unregister();
            } catch (Exception e) {
                LOGGER.warn(String.format("Failed to un-register vm id=\"%s\", continue.", vmRefId));
            }
        }

        return vmxPath;
    }

    public TemplatizedVm templatizeVm(HypervisorContext ctx, String vmRefId, boolean enableCbrc,
            HypervisorContext vcCtx, String hostEsxUuid, ActionDelegate<Integer> progressUpdater)
            throws Exception {
        LOGGER.info(String.format("Templatizing VM id=\"%s\"...", vmRefId));

        Assert.isInstanceOf(VmwareContext.class, ctx, "Invalid hypervisor context - expect Vmware");

        VmwareContext context = (VmwareContext) ctx;
        VirtualMachineMO vm = new VirtualMachineMO(context, vmRefId);

        if (!vm.safePowerOff(30 * 1000, null/*progressUpdater*/)) {
            // Wait 30 seconds for safe shutdown
            LOGGER.error(String.format("Failed to shut down VM id=\"%s\"", vmRefId));
            throw new Exception("Failed to shut down VM");
        }

        // Take a snapshot
        if (!vm.createSnapshot("Base", "", false, false, null/*progressUpdater*/)) {
            LOGGER.error(String.format("Failed to take snapshot for VM id=\"%s\"", vmRefId));
            throw new Exception("Failed to take VM snapshot");
        }

        if (enableCbrc) {
            try {
                enableCbrc = enableVmCbrc(ctx, vmRefId, true, vcCtx, hostEsxUuid, progressUpdater);
            } catch (Exception e) {
                LOGGER.warn(String.format("Failed to enable CBRC for VM id=\"%s\" in templatizeVm",  vmRefId), e);
            }
        }

        String vmxPath = null;
        try {
            vmxPath = vm.getVmxPath();
        } catch (Exception e) {
            LOGGER.warn(String.format("Failed to get vmx path for vm id=\"%s\", continue.", vmRefId));
        }

        boolean vmUnregistered = false;

        if (vcCtx instanceof VmwareContext
                && !StringUtils.isEmpty(hostEsxUuid)
                && !StringUtils.isEmpty(vmxPath)) {
            // Try to unregister the vm from vCenter first.
            VmwareContext vcContext = (VmwareContext) vcCtx;
            HostMO vcHost = HostMO.findByEsxUuid(vcContext, hostEsxUuid);
            if (vcHost != null) {
                // Find the vm.
                ObjectContent[] ocs = vcHost.getVmProperties(new String[] {"summary.config.vmPathName"});
                if (ocs != null) {
                    for (ObjectContent oc : ocs) {
                        String path = (String) VmwareUtils.getPropValue(oc, "summary.config.vmPathName");
                        if (StringUtils.equals(path, vmxPath)) {
                            VirtualMachineMO vcVm = new VirtualMachineMO(vcContext, oc.getObj());
                            try {
                                vcVm.unregister();
                                vmUnregistered = true;
                            } catch (Exception e) {
                                LOGGER.warn(String.format("Failed to un-register from vCenter: vmx=\"%s\", try on host.", vmxPath));
                            }
                            break;
                        }
                    }
                }
            }
        }

        if (!vmUnregistered) {
            // If vm is not unregistered, try to do it on the host directly.
            try {
                // Unregister VM
                vm.unregister();
            } catch (Exception e) {
                LOGGER.warn(String.format("Failed to un-register vm id=\"%s\", continue.", vmRefId));
            }
        }

        TemplatizedVm templatizedVm = new TemplatizedVm();
        templatizedVm.setCbrcEnabled(enableCbrc);

        return templatizedVm;
    }

    private VirtualMachineMO doRegisterVM(VmwareContext context, HostMO host, HostMO vcHost, String vmName, String vmxPath,
            ActionDelegate<Integer> progressUpdater)
            throws VmConfigFault, InvalidName, DuplicateName, FileFault, OutOfBounds, InsufficientResourcesFault, InvalidDatastore, AlreadyExists, NotFound, RuntimeFault, RemoteException, Exception {
        LOGGER.info(String.format("Registering VM \"%s\"...", vmName));

        // Register VM
        boolean vmRegistered;
        if (vcHost != null) {
            vmRegistered = vcHost.registerVmOnHost(vmName, vmxPath, true, progressUpdater);
        } else {
            vmRegistered = host.registerVmOnHost(vmName, vmxPath, true, progressUpdater);
        }
        if (!vmRegistered) {
            LOGGER.warn(String.format("Failed to register VM %s (%s)", vmName, vmxPath));
        }

        // Find the VM
        VirtualMachineMO vm = null;
        ObjectContent[] ocs = host.getVmProperties(new String[] {"summary.config.vmPathName"});
        if (ocs != null) {
            for (ObjectContent oc : ocs) {
                String path = (String) VmwareUtils.getPropValue(oc, "summary.config.vmPathName");
                if (StringUtils.equals(path, vmxPath)) {
                    vm = new VirtualMachineMO(context, oc.getObj());
                    break;
                }
            }
        }

        if (vm == null) {
            LOGGER.error(String.format("Unable to find the registered VM \"%s\"", vmName));
            throw new Exception("Unable to find the registered VM");
        }

        return vm;
    }

    public boolean enableVmCbrc(HypervisorContext ctx, String vmRefId, boolean enable,
            HypervisorContext vcCtx, String hostEsxUuid, ActionDelegate<Integer> progressUpdater)
            throws Exception {
        LOGGER.info(String.format("%s CBRC for VM id=\"%s\"...", enable ? "Enable" : "Disable", vmRefId));

        if (StringUtils.isEmpty(vmRefId)) {
            return false;
        }

        Assert.isInstanceOf(VmwareContext.class, ctx, "Invalid hypervisor context - expect Vmware");

        VmwareContext context = (VmwareContext) ctx;
        VirtualMachineMO vm = new VirtualMachineMO(context, vmRefId);

        VirtualDisk virtualDisk = null;
        VirtualMachineConfigInfo vmConfigInfo = vm.getConfigInfo();
        VirtualDevice devices[] = vmConfigInfo.getHardware().getDevice();
        if (devices != null) {
            for (VirtualDevice device : devices) {
                if (device instanceof VirtualDisk) {
                    virtualDisk = (VirtualDisk) device;
                    break;
                }
            }
        }

        if (virtualDisk == null) {
            throw new Exception("Failed to find disk for vm");
        }

        String vmxPath = null;
        try {
            vmxPath = vm.getVmxPath();
        } catch (Exception e) {
            LOGGER.warn(String.format("Failed to get vmx path for vm id=\"%s\", continue.", vmRefId));
        }

        CbrcDeviceSpec[] specs = new CbrcDeviceSpec[1];
        CbrcDeviceSpec spec = specs[0] = new CbrcDeviceSpec();
        spec.setDeviceKey(virtualDisk.getKey());

        CbrcManagerMO cbrcManager = null;

        boolean foundVcVm = false;

        if (vcCtx instanceof VmwareContext
                && !StringUtils.isEmpty(hostEsxUuid)
                && !StringUtils.isEmpty(vmxPath)) {
            // Try to unregister the vm from vCenter first.
            VmwareContext vcContext = (VmwareContext) vcCtx;
            HostMO vcHost = HostMO.findByEsxUuid(vcContext, hostEsxUuid);
            if (vcHost != null) {
                // Find the vm.
                ObjectContent[] ocs = vcHost.getVmProperties(new String[] {"summary.config.vmPathName"});
                if (ocs != null) {
                    for (ObjectContent oc : ocs) {
                        String path = (String) VmwareUtils.getPropValue(oc, "summary.config.vmPathName");
                        if (StringUtils.equals(path, vmxPath)) {
                            spec.setVm(oc.getObj());
                            foundVcVm = true;
                            break;
                        }
                    }
                }
            }

            if (foundVcVm) {
                cbrcManager = vcContext.getCbrcManager();
            }
        }

        if (!foundVcVm) {
            spec.setVm(vm.getMor());
            cbrcManager = context.getCbrcManager();
        }

        if (cbrcManager == null || !cbrcManager.configureDigest(specs, enable, null)) {
            LOGGER.error(String.format("Failed to %s CBRC for VM id=\"%s\"", enable ? "enable" : "disable", vmRefId));
            throw new Exception("Failed to config digest");
        }

        return true;
    }

    public void refreshVm(HypervisorContext ctx, String vmRefId, ActionDelegate<Integer> progressUpdater)
            throws Exception {
        LOGGER.info(String.format("Reverting snapshot of VM id=\"%s\"...", vmRefId));

        if (StringUtils.isEmpty(vmRefId)) {
            return;
        }

        Assert.isInstanceOf(VmwareContext.class, ctx, "Invalid hypervisor context - expect Vmware");

        VmwareContext context = (VmwareContext) ctx;
        VirtualMachineMO vm = new VirtualMachineMO(context, vmRefId);

        if (!vm.revertToCurrentSnapshot(progressUpdater)) {
            LOGGER.error(String.format("Failed to revert VM id=\"%s\" to current snapshot", vmRefId));
            throw new Exception("Invalid snapshot");
        }

        //LOGGER.info(String.format("Reverted snapshot of VM id=\"%s\"", vmRefId));
    }

    public void snapshotVm(HypervisorContext ctx, String vmRefId, boolean includeMemory,
            ActionDelegate<Integer> progressUpdater)
            throws Exception {
        LOGGER.info(String.format("Taking snapshot of VM id=\"%s\"...", vmRefId));

        if (StringUtils.isEmpty(vmRefId)) {
            return;
        }

        Assert.isInstanceOf(VmwareContext.class, ctx, "Invalid hypervisor context - expect Vmware");

        VmwareContext context = (VmwareContext) ctx;
        VirtualMachineMO vm = new VirtualMachineMO(context, vmRefId);

        if (!vm.createSnapshot("nimdesk", "", includeMemory, false, progressUpdater)) {
            LOGGER.error(String.format("Failed to take snapshot for VM id=\"%s\"", vmRefId));
            throw new Exception("Failed to take VM snapshot");
        }

        //LOGGER.info(String.format("Took snapshot of VM id=\"%s\"", vmRefId));
    }

    public boolean attachVmDisk(HypervisorContext ctx, String vmRefId, DatastoreFile vmdkDsFile, long sizeInMb,
            Storage storage, ActionDelegate<Integer> progressUpdater)
            throws Exception {
        LOGGER.info(String.format("Attaching disk for VM id=\"%s\"...", vmRefId));

        if (StringUtils.isEmpty(vmRefId)) {
            return false;
        }

        Assert.isInstanceOf(VmwareContext.class, ctx, "Invalid hypervisor context - expect Vmware");

        VmwareContext context = (VmwareContext) ctx;
        VirtualMachineMO vm = new VirtualMachineMO(context, vmRefId);
        DatastoreMO datastore = new DatastoreMO(context, storage.getRefId(), storage.getName());

        boolean vmdkExists = false;
        try {
            vmdkExists = datastore.fileExists(vmdkDsFile.getFullPath(), null);
        } catch (Exception e) {
            LOGGER.warn(String.format("Failed to check file exists for \"%s\" on datastore \"%s\"",
                    vmdkDsFile.getFullPath(), storage.getName()), e);
        }

        if (!vmdkExists) {
            DatastoreFile vmdkDsDir = new DatastoreFile(vmdkDsFile.getDatastoreName(), vmdkDsFile.getDir());

            DatacenterMO datacenter = context.getOwnerDatacenter(datastore.getMor());

            LOGGER.debug(String.format("attachVmDisk: Make remote image dir \"%s\"", vmdkDsDir.getFullPath()));
            datastore.makeDirectory(vmdkDsDir.getFullPath(), datacenter);

            return vm.createDisk(vmdkDsFile.getFullPath(), sizeInMb, datastore, -1, progressUpdater);
        } else {
            return vm.attachDisk(new String[] {vmdkDsFile.getFullPath()}, datastore, progressUpdater);
        }
    }

    public void detachVmDisk(HypervisorContext ctx, String vmRefId, String vmdkDatastorePath,
            ActionDelegate<Integer> progressUpdater)
            throws Exception {
        LOGGER.info(String.format("Detaching disk for VM id=\"%s\"...", vmRefId));

        if (StringUtils.isEmpty(vmRefId)) {
            return;
        }

        Assert.isInstanceOf(VmwareContext.class, ctx, "Invalid hypervisor context - expect Vmware");

        VmwareContext context = (VmwareContext) ctx;
        VirtualMachineMO vm = new VirtualMachineMO(context, vmRefId);

        vm.detachDisk(vmdkDatastorePath, false, progressUpdater);
    }

    public void detachVmAllDisks(HypervisorContext ctx, String vmRefId, String vmdkDatastorePath,
            ActionDelegate<Integer> progressUpdater)
            throws Exception {
        LOGGER.info(String.format("Detaching all user disks for VM id=\"%s\"...", vmRefId));

        if (StringUtils.isEmpty(vmRefId)) {
            return;
        }

        Assert.isInstanceOf(VmwareContext.class, ctx, "Invalid hypervisor context - expect Vmware");

        VmwareContext context = (VmwareContext) ctx;
        VirtualMachineMO vm = new VirtualMachineMO(context, vmRefId);

        vm.detachAllDisks(vmdkDatastorePath, false, progressUpdater);
    }
}
