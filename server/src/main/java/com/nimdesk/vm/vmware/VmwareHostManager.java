/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.vm.vmware;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.util.Assert;

import com.nimdesk.hypervisor.HypervisorContext;
import com.nimdesk.hypervisor.vmware.VmwareContext;
import com.nimdesk.hypervisor.vmware.mo.DatacenterMO;
import com.nimdesk.hypervisor.vmware.mo.HostMO;
import com.nimdesk.hypervisor.vmware.util.VmwareUtils;
import com.nimdesk.util.Pair;
import com.nimdesk.vm.HostAsset;
import com.nimdesk.vm.ImportableVm;
import com.nimdesk.vm.NetworkAsset;
import com.nimdesk.vm.StorageAsset;
import com.vmware.vim25.ArrayOfOptionValue;
import com.vmware.vim25.ArrayOfVirtualDevice;
import com.vmware.vim25.ArrayOfVirtualMachineConfigInfoDatastoreUrlPair;
import com.vmware.vim25.ManagedObjectReference;
import com.vmware.vim25.ObjectContent;
import com.vmware.vim25.OptionValue;
import com.vmware.vim25.VirtualDevice;
import com.vmware.vim25.VirtualDisk;
import com.vmware.vim25.VirtualEthernetCard;
import com.vmware.vim25.VirtualMachineConfigInfoDatastoreUrlPair;
import com.vmware.vim25.VirtualMachineGuestOsIdentifier;

public class VmwareHostManager {
    private static final Logger LOGGER = Logger.getLogger(VmwareHostManager.class);

    public HostAsset findStandaloneHost(HypervisorContext ctx)
            throws Exception {
        Assert.isInstanceOf(VmwareContext.class, ctx, "Invalid hypervisor context - expect Vmware");

        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("Find standalone host");
        }

        VmwareContext context = (VmwareContext) ctx;
        DatacenterMO datacenter = context.getRootDatacenter();

        ObjectContent[] ocs = datacenter.getHostProperties(new String[] {"name", "summary.managementServerIp"});
        if (ocs == null) {
            throw new Exception("Unable to find any host");
        }
        if (ocs.length > 1) {
            throw new Exception("This is not a standalone ESXi host");
        }

        ManagedObjectReference morHost = ocs[0].getObj();
        String hostName = (String) VmwareUtils.getPropValue(ocs[0], "name");
        String vCenterIp = (String) VmwareUtils.getPropValue(ocs[0], "summary.managementServerIp");

        HostAsset host = new HostAsset();
        host.setName(hostName);
        host.setRefId(morHost.get_value());
        host.setVCenterIp(vCenterIp);

        // For debugging:
        /*
        if (StringUtils.isEmpty(vCenterIp)) {
            host.setVCenterIp(context.getServerAddress());
        }
        */

        return host;
    }

    public Map<String, Object> getHostAssets(HypervisorContext ctx, String hostRefId)
            throws Exception {
        Assert.isInstanceOf(VmwareContext.class, ctx, "Invalid hypervisor context - expect Vmware");
        Assert.hasText(hostRefId, "Empty host refId specified");

        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("Get host assets");
        }

        VmwareContext context = (VmwareContext) ctx;
        HostMO host = new HostMO(context, hostRefId);

        ObjectContent[] ocs = host.getDatastoreProperties(new String[] {
            "name", "summary.capacity", "summary.freeSpace"
        });
        if (ocs == null) {
            throw new Exception("Unable to find any datastore");
        }

        ArrayList<StorageAsset> dsList = new ArrayList<StorageAsset>();
        List<Pair<ManagedObjectReference, Map<String, Object>>> objPropsList = VmwareUtils.getObjectProps(ocs);

        for (Pair<ManagedObjectReference, Map<String, Object>> objProps : objPropsList) {
            ManagedObjectReference morDs = objProps.getFirst();
            Map<String, Object> props = objProps.getSecond();

            StorageAsset storage = new StorageAsset();
            storage.setName((String) props.get("name"));
            storage.setRefId(morDs.get_value());
            storage.setCapacity((Long) props.get("summary.capacity"));
            storage.setFreeSpace((Long) props.get("summary.freeSpace"));

            dsList.add(storage);
        }

        ocs = host.getNetworkProperties(new String[] {"name"});
        if (ocs == null) {
            throw new Exception("Unable to find any network");
        }

        ArrayList<NetworkAsset> networkList = new ArrayList<NetworkAsset>();

        for (ObjectContent oc : ocs) {
            ManagedObjectReference morNetwork = oc.getObj();
            String networkName = (String) VmwareUtils.getPropValue(oc, "name");

            NetworkAsset network = new NetworkAsset();
            network.setName(networkName);
            network.setRefId(morNetwork.get_value());

            networkList.add(network);
        }

        HashMap<String, Object> result = new HashMap<String, Object>();
        result.put("datastores", dsList);
        result.put("networks", networkList);

        return result;
    }

    public List<ImportableVm> findImportableVms(HypervisorContext ctx, String hostRefId)
            throws Exception {
        Assert.isInstanceOf(VmwareContext.class, ctx, "Invalid hypervisor context - expect Vmware");
        Assert.hasText(hostRefId, "Empty host refId specified");

        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("Find networks");
        }

        VmwareContext context = (VmwareContext) ctx;
        HostMO host = new HostMO(context, hostRefId);

        ObjectContent[] ocs = host.getVmProperties(new String[] {
            "config.name", "config.files.vmPathName", "config.guestFullName", "config.guestId",
            "config.datastoreUrl", "config.hardware.device", "config.version", "config.extraConfig",
            "guest.guestState", "guest.ipAddress", "guest.disk", "guest.toolsVersionStatus",
            "snapshot"
        });

        List<Pair<ManagedObjectReference, Map<String, Object>>> objPropsList = VmwareUtils.getObjectProps(ocs);

        ArrayList<ImportableVm> vmList = new ArrayList<ImportableVm>();
        for (Pair<ManagedObjectReference, Map<String, Object>> objProps : objPropsList) {
            ImportableVm importableVm = checkVmImportability(objProps);
            if (importableVm != null) {
                vmList.add(importableVm);
            }
        }

        return vmList;
    }

    private ImportableVm checkVmImportability(Pair<ManagedObjectReference, Map<String, Object>> objProps) {
        ArrayList<String> reasons = new ArrayList<String>();

        ManagedObjectReference morVm = objProps.getFirst();
        Map<String, Object> props = objProps.getSecond();

        String vmName = (String) props.get("config.name");

        // Check OS name
        String guestOsName = (String) props.get("config.guestFullName");
        String guestId = (String) props.get("config.guestId");
        VirtualMachineGuestOsIdentifier guestOsId =
                StringUtils.isEmpty(guestId) ? null : VirtualMachineGuestOsIdentifier.valueOf(guestId);
        //VmwareGuestOsMapper.getGuestOsIdentifier(guestOsName);
        if (guestOsId == null) {
            guestOsId = VirtualMachineGuestOsIdentifier.otherGuest;
        }
        if (guestOsId == null
                || !StringUtils.startsWithIgnoreCase(guestOsId.toString(), "win")) {
            reasons.add(String.format("The OS \"%s\" of the VM is not supported.", guestOsName));
        }
        /*
        switch (guestOsId) {
            case winXPProGuest:
            case winXPPro64Guest:
            case winVistaGuest:
            case winVista64Guest:
            case winNetEnterpriseGuest:
            case winNetEnterprise64Guest:
            case winNetDatacenterGuest:
            case winNetDatacenter64Guest:
            case winNetStandardGuest:
            case winNetStandard64Guest:
            case winNetWebGuest:
            case winNetBusinessGuest:
            case winLonghornGuest:
            case winLonghorn64Guest:
            case windows7Guest:
            case windows7_64Guest:
            case windows7Server64Guest:
            case windows8Guest:
            case windows8_64Guest:
            case windows8Server64Guest:
                break;

            default:
                reasons.add(String.format("The OS \"%s\" of the VM is not supported.", guestOsName));
                break;
        }
        */

        // Don't import nimdesk generated desktop
        ArrayOfOptionValue optionValueArr = (ArrayOfOptionValue) props.get("config.extraConfig");
        if (optionValueArr != null) {
            OptionValue[] optionValues = optionValueArr.getOptionValue();
            if (optionValues != null) {
                for (OptionValue optionValue : optionValues) {
                    if ("guestinfo.nimdesk.type".equals(optionValue.getKey())
                            && "desktop".equals(optionValue.getValue())) {
                        return null;
                    }
                }
            }
        }

        // Check datastore
        @SuppressWarnings("unused")
        VirtualMachineConfigInfoDatastoreUrlPair[] datastoreUrl = null;
        ArrayOfVirtualMachineConfigInfoDatastoreUrlPair datastoreUrlArr =
                (ArrayOfVirtualMachineConfigInfoDatastoreUrlPair) props.get("config.datastoreUrl");
        if (datastoreUrlArr != null) {
            datastoreUrl = datastoreUrlArr.getVirtualMachineConfigInfoDatastoreUrlPair();
        }

        // Check NIC number (only support 1 NIC) & disk (only support 1 disk)
        ArrayOfVirtualDevice deviceArr = (ArrayOfVirtualDevice) props.get("config.hardware.device");
        VirtualDevice[] devices;
        int nicNumber = 0;
        int diskNumber = 0;
        VirtualDisk virtualDisk = null;
        if (deviceArr != null && (devices = deviceArr.getVirtualDevice()) != null) {
            for (VirtualDevice device : devices) {
                if (device instanceof VirtualEthernetCard) {
                    ++nicNumber;
                }
                if (device instanceof VirtualDisk) {
                    ++diskNumber;
                    virtualDisk = (VirtualDisk) device;
                }
            }
        }

        if (nicNumber == 0) {
            reasons.add("The VM does not have a network card.");
        } else if (nicNumber > 1) {
            reasons.add(String.format("The VM has %d network cards. Only one network card is supported.", nicNumber));
        }
        if (diskNumber == 0) {
            reasons.add("The VM does not have a disk.");
        } else if (diskNumber > 1) {
            virtualDisk = null;
            reasons.add(String.format("The VM has %d disks. Only one disk is supported.", diskNumber));
        }

        // Check VMware Tools (guest tools)
        String toolsVersionStatus = (String) props.get("guest.toolsVersionStatus");
        if ("guestToolsNotInstalled".equals(toolsVersionStatus)) {
            reasons.add("VMware Tools is not installed on this VM.");
        }

        // Check storage capacity

        ImportableVm vm = new ImportableVm();
        vm.setVmRefId(morVm.get_value());
        vm.setVmName(vmName);
        vm.setOsName(guestOsName);
        vm.setDiskCapacityInKB((virtualDisk==null)?0:virtualDisk.getCapacityInKB());
        vm.setReasons(reasons);
        return vm;
    }
}
