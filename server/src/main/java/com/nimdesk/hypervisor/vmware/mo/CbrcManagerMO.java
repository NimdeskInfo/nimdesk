/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.hypervisor.vmware.mo;

import java.rmi.RemoteException;

import org.apache.log4j.Logger;

import com.nimdesk.hypervisor.vmware.VmwareContext;
import com.nimdesk.util.ActionDelegate;
import com.vmware.vim25.ArrayOfCbrcDigestConfigureResult;
import com.vmware.vim25.CbrcDeviceSpec;
import com.vmware.vim25.CbrcDigestConfigureResult;
import com.vmware.vim25.CbrcVmdkLockFailure;
import com.vmware.vim25.InvalidPowerState;
import com.vmware.vim25.InvalidState;
import com.vmware.vim25.LocalizedMethodFault;
import com.vmware.vim25.ManagedObjectNotFound;
import com.vmware.vim25.ManagedObjectReference;
import com.vmware.vim25.MethodFault;
import com.vmware.vim25.NotSupported;
import com.vmware.vim25.TaskInfo;
import com.vmware.vim25.VmConfigFault;
import com.vmware.vim25.mo.CbrcManager;
import com.vmware.vim25.mo.Task;

public class CbrcManagerMO extends BaseMO {
    private static final Logger LOGGER = Logger.getLogger(CbrcManagerMO.class);

    public CbrcManagerMO(VmwareContext context, ManagedObjectReference mor) {
        super(context, mor);
    }

    public CbrcManagerMO(VmwareContext context, String morType, String morRefId) {
        super(context, morType, morRefId);
    }

    public CbrcManager getCbrcManager() {
        return (CbrcManager) getManagedObject();
    }

    public boolean configureDigest(CbrcDeviceSpec[] specs, boolean enable, ActionDelegate<Integer> progressUpdater)
            throws InvalidPowerState, ManagedObjectNotFound, VmConfigFault, InvalidState, NotSupported, CbrcVmdkLockFailure, RemoteException {
        LOGGER.info(String.format("Configuring virtual disk digest (enable=%s, vm=%s)...",
                enable ? "true" : "false", specs[0].getVm().get_value()));

        String error = null;

        int retried = 0;

        while (true) {
            Task task = getCbrcManager().configureDigest_Task(specs, enable, true, true);

            try {
                error = getContext().waitForTask(task, progressUpdater);
                if (error != null) {
                    break;
                }

                TaskInfo taskInfo = task.getTaskInfo();
                if (taskInfo != null) {
                    Object result = taskInfo.getResult();
                    if (result instanceof ArrayOfCbrcDigestConfigureResult) {
                        ArrayOfCbrcDigestConfigureResult digestConfigureResultArray = (ArrayOfCbrcDigestConfigureResult) result;
                        CbrcDigestConfigureResult[] digestConfigureResult = digestConfigureResultArray.getCbrcDigestConfigureResult();
                        if (digestConfigureResult.length > 0) {
                            LocalizedMethodFault localizedFault = digestConfigureResult[0].getFault();
                            MethodFault fault;
                            if (localizedFault != null
                                    && ((fault = localizedFault.getFault()) != null)
                                    && (fault instanceof CbrcVmdkLockFailure)
                                    && retried < 3) {
                                ++retried;
                                LOGGER.warn(String.format("Failed to configureDigest (vm=%s), retry %d...",
                                        specs[0].getVm().get_value(), retried));
                                try {
                                    Thread.sleep(20000); // Sleep 20 seconds and retry
                                } catch (InterruptedException e) {
                                    // Ignore
                                }
                                continue;
                            }
                        }
                    }
                }

                break;
            } catch (Exception e) {
                LOGGER.error(String.format("Failed to config virtual disk digest (enable=%s, vm=%s)",
                        enable ? "true" : "false", specs[0].getVm().get_value()), e);
                return false;
            }
        }

        if (error != null) {
            LOGGER.error(String.format("Failed to config virtual disk digest (enable=%s, vm=%s): %s",
                    enable ? "true" : "false", specs[0].getVm().get_value(), error));
            return false;
        }

        LOGGER.info(String.format("Configured virtual disk digest (enable=%s, vm=%s)",
                enable ? "true" : "false", specs[0].getVm().get_value()));
        return true;
    }
}
