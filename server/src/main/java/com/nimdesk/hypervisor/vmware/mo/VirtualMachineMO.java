/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.hypervisor.vmware.mo;

import java.io.File;
import java.io.FileWriter;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.nimdesk.hypervisor.vmware.VmwareContext;
import com.nimdesk.hypervisor.vmware.util.DatastoreFile;
import com.nimdesk.hypervisor.vmware.util.VmwareUtils;
import com.nimdesk.util.ActionDelegate;
import com.nimdesk.util.Pair;
import com.vmware.vim25.ConcurrentAccess;
import com.vmware.vim25.DuplicateName;
import com.vmware.vim25.FileFault;
import com.vmware.vim25.GuestInfo;
import com.vmware.vim25.HttpNfcLeaseDeviceUrl;
import com.vmware.vim25.HttpNfcLeaseInfo;
import com.vmware.vim25.HttpNfcLeaseState;
import com.vmware.vim25.InsufficientResourcesFault;
import com.vmware.vim25.InvalidDatastore;
import com.vmware.vim25.InvalidName;
import com.vmware.vim25.InvalidPowerState;
import com.vmware.vim25.InvalidState;
import com.vmware.vim25.ManagedObjectReference;
import com.vmware.vim25.OvfCreateDescriptorParams;
import com.vmware.vim25.OvfCreateDescriptorResult;
import com.vmware.vim25.OvfFile;
import com.vmware.vim25.ResourceAllocationInfo;
import com.vmware.vim25.RuntimeFault;
import com.vmware.vim25.SnapshotFault;
import com.vmware.vim25.TaskInProgress;
import com.vmware.vim25.ToolsUnavailable;
import com.vmware.vim25.VirtualCdrom;
import com.vmware.vim25.VirtualCdromRemotePassthroughBackingInfo;
import com.vmware.vim25.VirtualDevice;
import com.vmware.vim25.VirtualDeviceBackingInfo;
import com.vmware.vim25.VirtualDeviceConfigSpec;
import com.vmware.vim25.VirtualDeviceConfigSpecFileOperation;
import com.vmware.vim25.VirtualDeviceConfigSpecOperation;
import com.vmware.vim25.VirtualDeviceFileBackingInfo;
import com.vmware.vim25.VirtualDisk;
import com.vmware.vim25.VirtualDiskFlatVer2BackingInfo;
import com.vmware.vim25.VirtualDiskMode;
import com.vmware.vim25.VirtualDiskRawDiskMappingVer1BackingInfo;
import com.vmware.vim25.VirtualDiskType;
import com.vmware.vim25.VirtualEthernetCard;
import com.vmware.vim25.VirtualIDEController;
import com.vmware.vim25.VirtualMachineConfigInfo;
import com.vmware.vim25.VirtualMachineConfigSpec;
import com.vmware.vim25.VirtualMachinePowerState;
import com.vmware.vim25.VirtualMachineRuntimeInfo;
import com.vmware.vim25.VirtualMachineSnapshotInfo;
import com.vmware.vim25.VirtualMachineSnapshotTree;
import com.vmware.vim25.VirtualSCSIController;
import com.vmware.vim25.VmConfigFault;
import com.vmware.vim25.mo.HttpNfcLease;
import com.vmware.vim25.mo.OvfManager;
import com.vmware.vim25.mo.Task;
import com.vmware.vim25.mo.VirtualMachine;
import com.vmware.vim25.mo.VirtualMachineSnapshot;
import com.vmware.vim25.mo.util.MorUtil;

/**
 *
 */
public class VirtualMachineMO extends BaseMO {
    private static final Logger LOGGER = Logger.getLogger(VirtualMachineMO.class);

    public static final String MORTYPE_VIRTUALMACHINE = "VirtualMachine";

    private static final int MAX_RETRY = 5;
    private static final int RETRY_INTERVAL = 3000; // 3 seconds

    // Cached objects;
    String _vmxPath = null;

    public VirtualMachineMO(VmwareContext context, ManagedObjectReference mor) {
        super(context, mor);
    }

    public VirtualMachineMO(VmwareContext context, ManagedObjectReference mor, String name) {
        super(context, mor, name);
    }

    public VirtualMachineMO(VmwareContext context, String vmRefId) {
        super(context, MORTYPE_VIRTUALMACHINE, vmRefId);
    }

    public VirtualMachineMO(VmwareContext context, String vmRefId, String name) {
        super(context, MORTYPE_VIRTUALMACHINE, vmRefId, name);
    }

    public VirtualMachine getVirtualMachine() {
        return (VirtualMachine) getManagedEntity();
    }

    public VirtualMachineConfigInfo getConfigInfo() {
        return getVirtualMachine().getConfig();
    }

    public VirtualMachineSnapshotInfo getSnapshotInfo() {
        return getVirtualMachine().getSnapshot();
    }

    public GuestInfo getGuestInfo() {
        return getVirtualMachine().getGuest();
    }

    public boolean isTemplate() {
        return getConfigInfo().isTemplate();
    }

    public VirtualMachineRuntimeInfo getRuntimeInfo() {
        return getVirtualMachine().getRuntime();
    }

    public VirtualMachinePowerState getPowerState() {
        return (VirtualMachinePowerState) getVirtualMachine().getPropertyByPath("runtime.powerState");
    }

    public String getVmxPath() {
        if (_vmxPath == null) {
            _vmxPath = (String) getVirtualMachine().getPropertyByPath("summary.config.vmPathName");
        }

        return _vmxPath;
    }

    public boolean isVmwareToolsRunning() {
        GuestInfo guestInfo = getGuestInfo();
        return guestInfo != null && "guestToolsRunning".equalsIgnoreCase(guestInfo.getToolsRunningStatus());
    }

    public ManagedObjectReference getSnapshotMor(String snapshotName) {
        VirtualMachineSnapshotInfo snapshotInfo = getSnapshotInfo();
        if (snapshotInfo == null) {
            return null;
        }

        VirtualMachineSnapshotTree[] snapshotTree = snapshotInfo.getRootSnapshotList();
        return VmwareUtils.findSnapshotInTree(snapshotTree, snapshotName);
    }

    public boolean reconfig(long cpuCount, long memoryMB, boolean reserveMemory, String macAddress, ActionDelegate<Integer> progressUpdater)
            throws InvalidName, VmConfigFault, DuplicateName, TaskInProgress,
            FileFault, InvalidState, ConcurrentAccess, InvalidDatastore,
            InsufficientResourcesFault, RuntimeFault, RemoteException {
        LOGGER.info(String.format("VM %s (%s) reconfig (CPUs=%d, mem=%dMB, mac=%s)...",
                getMor().get_value(), getNameNoFetching(), cpuCount, memoryMB, macAddress));

        if (cpuCount == 0 && memoryMB == 0) {
            LOGGER.info(String.format("VM %s (%s) no need to reconfig",
                    getMor().get_value(), getNameNoFetching()));
        }

        boolean editConfig = false;

        VirtualMachineConfigSpec spec = new VirtualMachineConfigSpec();
        if (cpuCount > 0) {
            if (cpuCount <= 4) {
                spec.setNumCPUs(1);
                spec.setNumCoresPerSocket((int) cpuCount);
            } else {
                spec.setNumCPUs(((int) cpuCount + 3) / 4);
                spec.setNumCoresPerSocket(8);
            }
            editConfig = true;
        }
        if (memoryMB > 0) {
            spec.setMemoryMB(memoryMB);
            if (reserveMemory) {
                ResourceAllocationInfo memAlloc = new ResourceAllocationInfo();
                memAlloc.setReservation(memoryMB);
                spec.setMemoryAllocation(memAlloc);
            }
            editConfig = true;
        }
        if (!StringUtils.isEmpty(macAddress)) {
            VirtualDeviceConfigSpec vda[] = new VirtualDeviceConfigSpec[1];
            VirtualDeviceConfigSpec vd = vda[0] = new VirtualDeviceConfigSpec();
            VirtualEthernetCard nic = null;
            VirtualMachineConfigInfo vmConfigInfo = getConfigInfo();
            VirtualDevice devices[] = vmConfigInfo.getHardware().getDevice();
            if (devices != null) {
                for (VirtualDevice device : devices) {
                    if (device instanceof VirtualEthernetCard) {
                        nic = (VirtualEthernetCard) device;
                        break;
                    }
                }
            }

            if (nic != null) {
                nic.setAddressType("Manual");
                nic.setMacAddress(macAddress);

                vd.setDevice(nic);
                vd.setOperation(VirtualDeviceConfigSpecOperation.edit);
                spec.setDeviceChange(vda);

                editConfig = true;
            }
        }

        if (!editConfig) {
            LOGGER.warn(String.format("VM %s (%s) no need to reconfig", getMor().get_value(), getNameNoFetching()));
            return true;
        }

        return reconfig(spec, progressUpdater);
    }

    public boolean reserveResource(long cpuMHz, long memoryMB, ActionDelegate<Integer> progressUpdater)
            throws InvalidName, VmConfigFault, DuplicateName, TaskInProgress,
            FileFault, InvalidState, ConcurrentAccess, InvalidDatastore, InsufficientResourcesFault, RuntimeFault, RemoteException {
        LOGGER.info(String.format("VM %s (%s) reserver resource (cpu=%dMHz, mem=%dMB)...",
                getMor().get_value(), getNameNoFetching(), cpuMHz, memoryMB));

        if (cpuMHz <= 0 && memoryMB <= 0) {
            LOGGER.info(String.format("VM %s (%s) no need to reserver resource",
                    getMor().get_value(), getNameNoFetching()));
        }

        VirtualMachineConfigSpec spec = new VirtualMachineConfigSpec();
        if (cpuMHz > 0L) {
            ResourceAllocationInfo cpuAlloc = new ResourceAllocationInfo();
            cpuAlloc.setReservation(cpuMHz);
            spec.setCpuAllocation(cpuAlloc);
        }
        if (memoryMB > 0L) {
            ResourceAllocationInfo memAlloc = new ResourceAllocationInfo();
            memAlloc.setReservation(memoryMB);
            spec.setMemoryAllocation(memAlloc);
        }

        return reconfig(spec, progressUpdater);
    }

    private boolean reconfig(VirtualMachineConfigSpec spec, ActionDelegate<Integer> progressUpdater)
            throws InvalidName, VmConfigFault, DuplicateName, TaskInProgress, FileFault, InvalidState,
                   ConcurrentAccess, InvalidDatastore, InsufficientResourcesFault, RuntimeFault, RemoteException {
        String error;

        int retry = 0;
        while (true) {
            try {
                Task task = getVirtualMachine().reconfigVM_Task(spec);
                error = getContext().waitForTask(task, progressUpdater);
                break;
            } catch (Exception e) {
                if ((e instanceof TaskInProgress)
                        || (e instanceof FileFault)
                        || (e instanceof InvalidState)
                        || (e instanceof ConcurrentAccess)){
                    if (++retry <= MAX_RETRY) {
                        LOGGER.warn(String.format("%s. Retry reconfig (%d)...",
                                e.getClass().getName(), retry));
                        try {
                            Thread.sleep(RETRY_INTERVAL);
                        } catch (InterruptedException e1) {
                            // Ignore
                        }
                        continue;
                    }
                }

                LOGGER.error(String.format("VM %s (%s) failed to reconfig",
                        getMor().get_value(), getNameNoFetching()), e);
                return false;
            }
        }

        if (error != null) {
            LOGGER.error(String.format("VM %s (%s) failed to reconfig: %s",
                    getMor().get_value(), getNameNoFetching(), error));
            return false;
        }

        LOGGER.info(String.format("VM %s (%s) reconfig'ed", getMor().get_value(), getNameNoFetching()));
        return true;
    }

    public boolean powerOn(ActionDelegate<Integer> progressUpdater)
            throws VmConfigFault, TaskInProgress, FileFault, InvalidState,
            InsufficientResourcesFault, RuntimeFault, RemoteException {
        LOGGER.info(String.format("VM %s (%s) powering on...",
                getMor().get_value(), getNameNoFetching()));

        String error;

        int retry = 0;
        while (true) {
            try {
                if (getPowerState() == VirtualMachinePowerState.poweredOn) {
                    LOGGER.info(String.format("VM %s (%s) already powered on",
                            getMor().get_value(), getNameNoFetching()));
                    return true;
                }

                Task task = getVirtualMachine().powerOnVM_Task(null);
                error = getContext().waitForTask(task, progressUpdater);
                break;
            } catch (Exception e) {
                /*
                if ((e instanceof TaskInProgress)
                        // Some shared storage could have delay.
                        || (e instanceof FileFault)
                        // VM may still be in previous INVALID_CONFIG state.
                        || (e instanceof InvalidState)) {
                */
                    // powerOn is too important to fail. So retry it no matter what.
                    if (++retry <= MAX_RETRY) {
                        LOGGER.warn(String.format("%s. Retry powerOn (%d)...",
                                e.getClass().getName(), retry));
                        try {
                            Thread.sleep(RETRY_INTERVAL);
                        } catch (InterruptedException e1) {
                            // Ignore
                        }
                        continue;
                    }
                //}

                LOGGER.error(String.format("VM %s (%s) failed to power on",
                        getMor().get_value(), getNameNoFetching()), e);
                return false;
            }
        }

        if (error != null) {
            LOGGER.error(String.format("VM %s (%s) failed to power on: %s",
                    getMor().get_value(), getNameNoFetching(), error));
            return false;
        }

        LOGGER.info(String.format("VM %s (%s) powered on", getMor().get_value(), getNameNoFetching()));
        return true;
    }

    public boolean powerOff(ActionDelegate<Integer> progressUpdater)
            throws TaskInProgress, InvalidState, RuntimeFault, RemoteException {
        if (getPowerState() == VirtualMachinePowerState.poweredOff) {
            LOGGER.info(String.format("VM %s (%s) already powered off",
                    getMor().get_value(), getNameNoFetching()));
            return true;
        }

        return forcePowerOff(progressUpdater);
    }

    public boolean safePowerOff(int waitMs, ActionDelegate<Integer> progressUpdater)
            throws TaskInProgress, InvalidState, RuntimeFault, RemoteException {
        if (getPowerState() == VirtualMachinePowerState.poweredOff) {
            LOGGER.info(String.format("VM %s (%s) already powered off",
                    getMor().get_value(), getNameNoFetching()));
            return true;
        }

        if (isVmwareToolsRunning()) {
            try {
                LOGGER.info(String.format("VM %s (%s) safely shutting down...",
                        getMor().get_value(), getNameNoFetching()));
                getVirtualMachine().shutdownGuest();

                long startTime = System.currentTimeMillis();
                while ((System.currentTimeMillis() - startTime < waitMs)
                        && (getPowerState() != VirtualMachinePowerState.poweredOff)) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        // Ignore
                    }
                }

                try {
                    if (getPowerState() == VirtualMachinePowerState.poweredOff) {
                        LOGGER.info(String.format("VM %s (%s) safely shutted down", getMor().get_value(), getNameNoFetching()));
                        return true;
                    }
                } catch (Exception e) {
                    // Ignore error and try hard power off.
                }

                LOGGER.info(String.format("VM %s (%s) failed to safely shut down. Try force powering off...",
                        getMor().get_value(), getNameNoFetching()));
            } catch (Exception e) {
                LOGGER.warn(String.format("VM %s (%s) failed to safe power off",
                        getMor().get_value(), getNameNoFetching()), e);
            }
        }

        return forcePowerOff(progressUpdater);
    }

    private boolean forcePowerOff(ActionDelegate<Integer> progressUpdater)
            throws TaskInProgress, InvalidState, RuntimeFault, RemoteException {
        LOGGER.info(String.format("VM %s (%s) powering off...",
                getMor().get_value(), getNameNoFetching()));

        String error;

        int retry = 0;
        while (true) {
            try {
                if (getPowerState() == VirtualMachinePowerState.poweredOff) {
                    LOGGER.info(String.format("VM %s (%s) already powered off",
                            getMor().get_value(), getNameNoFetching()));
                    return true;
                }

                Task task = getVirtualMachine().powerOffVM_Task();
                error = getContext().waitForTask(task, progressUpdater);
                break;
            } catch (Exception e) {
                if ((e instanceof TaskInProgress)
                        || (e instanceof InvalidState)){
                    if (++retry <= MAX_RETRY) {
                        LOGGER.warn(String.format("%s. Retry forcePowerOff (%d)...",
                                e.getClass().getName(), retry));
                        try {
                            Thread.sleep(RETRY_INTERVAL);
                        } catch (InterruptedException e1) {
                            // Ignore
                        }
                        continue;
                    }
                }

                LOGGER.warn(String.format("VM %s (%s) failed to power off",
                        getMor().get_value(), getNameNoFetching()), e);
                error = "error"; // Always check powerstate to see if vm's powered off.
                break;
            }
        }

        // Wait for up to 5 seconds to make sure the power state is poweredOff.
        long startTime = System.currentTimeMillis();

        VirtualMachinePowerState powerState = null;

        while ((System.currentTimeMillis() - startTime < 5000)
                && ((powerState = getPowerState()) != VirtualMachinePowerState.poweredOff)) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                // Ignore
            }
        }

        if (error != null) {
            if (powerState == VirtualMachinePowerState.poweredOff) {
                LOGGER.info(String.format("VM %s (%s) powered off with error: %s",
                        getMor().get_value(), getNameNoFetching(), error));
                return true;
            }

            LOGGER.error(String.format("VM %s (%s) failed to power off: %s",
                    getMor().get_value(), getNameNoFetching(), error));
            return false;
        }

        LOGGER.info(String.format("VM %s (%s) powered off", getMor().get_value(), getNameNoFetching()));
        return true;
    }

    public boolean reset(ActionDelegate<Integer> progressUpdater)
            throws VmConfigFault, TaskInProgress, FileFault, InvalidState, InsufficientResourcesFault,
                   RuntimeFault, RemoteException {
        LOGGER.info(String.format("VM %s (%s) resetting...", getMor().get_value(), getNameNoFetching()));

        String error;

        int retry = 0;
        while (true) {
            try {
                Task task = getVirtualMachine().resetVM_Task();
                error = getContext().waitForTask(task, progressUpdater);
                break;
            } catch (Exception e) {
                if ((e instanceof TaskInProgress)
                        || (e instanceof FileFault)
                        || (e instanceof InvalidState)){
                    if (++retry <= MAX_RETRY) {
                        LOGGER.warn(String.format("%s. Retry rest (%d)...",
                                e.getClass().getName(), retry));
                        try {
                            Thread.sleep(RETRY_INTERVAL);
                        } catch (InterruptedException e1) {
                            // Ignore
                        }
                        continue;
                    }
                }

                LOGGER.error(String.format("VM %s (%s) failed to reset",
                        getMor().get_value(), getNameNoFetching()), e);
                return false;
            }
        }

        if (error != null) {
            LOGGER.error(String.format("VM %s (%s) failed to reset: %s",
                    getMor().get_value(), getNameNoFetching(), error));
            return false;
        }

        LOGGER.info(String.format("VM %s (%s) reset", getMor().get_value(), getNameNoFetching()));
        return true;
    }

    public boolean suspend(ActionDelegate<Integer> progressUpdater)
            throws VmConfigFault, TaskInProgress, FileFault, InvalidState, InsufficientResourcesFault,
                   RuntimeFault, RemoteException {
        LOGGER.info(String.format("VM %s (%s) suspending...", getMor().get_value(), getNameNoFetching()));

        String error;

        int retry = 0;
        while (true) {
            try {
                if (getPowerState() == VirtualMachinePowerState.suspended) {
                    LOGGER.info(String.format("VM %s (%s) already suspended",
                            getMor().get_value(), getNameNoFetching()));
                    return true;
                }

                Task task = getVirtualMachine().suspendVM_Task();
                error = getContext().waitForTask(task, progressUpdater);
                break;
            } catch (Exception e) {
                if ((e instanceof TaskInProgress)
                        || (e instanceof FileFault)
                        || (e instanceof InvalidState)) {
                    if (++retry <= MAX_RETRY) {
                        LOGGER.warn(String.format("%s. Retry suspend (%d)...",
                                e.getClass().getName(), retry));
                        try {
                            Thread.sleep(RETRY_INTERVAL);
                        } catch (InterruptedException e1) {
                            // Ignore
                        }
                        continue;
                    }
                }

                LOGGER.error(String.format("VM %s (%s) failed to suspend",
                        getMor().get_value(), getNameNoFetching()), e);
                return false;
            }
        }

        if (error != null) {
            LOGGER.error(String.format("VM %s (%s) failed to suspend: %s",
                    getMor().get_value(), getNameNoFetching(), error));
            return false;
        }

        LOGGER.info(String.format("VM %s (%s) suspended", getMor().get_value(), getNameNoFetching()));
        return true;
    }

    public void rebootGuest()
            throws TaskInProgress, InvalidState, ToolsUnavailable, RuntimeFault, RemoteException {
        LOGGER.info(String.format("VM %s (%s) reboot guest", getMor().get_value(), getNameNoFetching()));
        getVirtualMachine().rebootGuest();
    }

    public boolean createSnapshot(String snapshotName, String snapshotDesc, boolean includeMemory, boolean quiesce,
            ActionDelegate<Integer> progressUpdater)
            throws InvalidName, VmConfigFault, SnapshotFault, TaskInProgress, FileFault,
                   InvalidState, RuntimeFault, RemoteException {
        LOGGER.info(String.format("VM %s (%s) creating snapshot \"%s\"...",
                getMor().get_value(), getNameNoFetching(), snapshotName));

        String error;

        int retry = 0;
        while (true) {
            try {
                Task task = getVirtualMachine().createSnapshot_Task(snapshotName, snapshotDesc, includeMemory, quiesce);
                error = getContext().waitForTask(task, progressUpdater);
                break;
            } catch (Exception e) {
                if ((e instanceof TaskInProgress)
                        || (e instanceof FileFault)
                        || (e instanceof InvalidState)
                        || (e instanceof VmConfigFault)) {
                    if (++retry <= MAX_RETRY) {
                        LOGGER.warn(String.format("%s. Retry createSnapshot (%d)...",
                                e.getClass().getName(), retry));
                        try {
                            Thread.sleep(RETRY_INTERVAL);
                        } catch (InterruptedException e1) {
                            // Ignore
                        }
                        continue;
                    }
                }

                LOGGER.error(String.format("VM %s (%s) failed to create snapshot \"%s\"",
                        getMor().get_value(), getNameNoFetching(), snapshotName), e);
                return false;
            }
        }

        if (error != null) {
            LOGGER.error(String.format("VM %s (%s) failed to create snapshot \"%s\": %s",
                    getMor().get_value(), getNameNoFetching(), snapshotName, error));
            return false;
        }

        // Wait for up to 10 seconds to make sure the snapshot appears.
        ManagedObjectReference morSnapshot = null;
        long startTime = System.currentTimeMillis();
        while (System.currentTimeMillis() - startTime < 10000) {
            morSnapshot = getSnapshotMor(snapshotName);
            if (morSnapshot != null) {
                break;
            }

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                // Ignore
            }
        }

        if (morSnapshot == null) {
            LOGGER.error(String.format("VM %s (%s) created snapshot \"%s\" hasn't shown up after 10 seconds",
                    getMor().get_value(), getNameNoFetching(), snapshotName));
            return false;
        }

        LOGGER.info(String.format("VM %s (%s) created snapshot \"%s\"",
                getMor().get_value(), getNameNoFetching(), snapshotName));
        return true;
    }

    public boolean removeSnapshot(String snapshotName, boolean removeChildren, ActionDelegate<Integer> progressUpdater)
            throws TaskInProgress, RuntimeFault, RemoteException {
        LOGGER.info(String.format("VM %s (%s) removing snapshot \"%s\"...",
                getMor().get_value(), getNameNoFetching(), snapshotName));

        ManagedObjectReference morSnapshot = getSnapshotMor(snapshotName);
        if (morSnapshot == null) {
            LOGGER.warn(String.format("VM %s (%s) unable to find snapshot \"%s\"",
                    getMor().get_value(), getNameNoFetching(), snapshotName));
            return false;
        }

        VirtualMachineSnapshot snapshot = (VirtualMachineSnapshot)
                MorUtil.createExactManagedObject(getContext().getServiceInstance().getServerConnection(), morSnapshot);

        String error;

        int retry = 0;
        while (true) {
            try {
                Task task = snapshot.removeSnapshot_Task(removeChildren);
                error = getContext().waitForTask(task, progressUpdater);
                break;
            } catch (Exception e) {
                if (e instanceof TaskInProgress) {
                    if (++retry <= MAX_RETRY) {
                        LOGGER.warn(String.format("%s. Retry removeSnapshot (%d)...",
                                e.getClass().getName(), retry));
                        try {
                            Thread.sleep(RETRY_INTERVAL);
                        } catch (InterruptedException e1) {
                            // Ignore
                        }
                        continue;
                    }
                }

                LOGGER.error(String.format("VM %s (%s) failed to remove snapshot \"%s\"",
                        getMor().get_value(), getNameNoFetching(), snapshotName), e);
                return false;
            }
        }

        if (error != null) {
            LOGGER.error(String.format("VM %s (%s) failed to remove snapshot \"%s\": %s",
                    getMor().get_value(), getNameNoFetching(), snapshotName, error));
            return false;
        }

        LOGGER.info(String.format("VM %s (%s) removed snapshot \"%s\"",
                getMor().get_value(), getNameNoFetching(), snapshotName));
        return true;
    }

    public boolean removeAllSnapshot(ActionDelegate<Integer> progressUpdater)
            throws SnapshotFault, TaskInProgress, InvalidState, RuntimeFault, RemoteException {
        LOGGER.info(String.format("VM %s (%s) removing all snapshots...",
                getMor().get_value(), getNameNoFetching()));

        String error;

        int retry = 0;
        while (true) {
            try {
                Task task = getVirtualMachine().removeAllSnapshots_Task();
                error = getContext().waitForTask(task, progressUpdater);
                break;
            } catch (Exception e) {
                if ((e instanceof TaskInProgress)
                        || (e instanceof InvalidState)) {
                    if (++retry <= MAX_RETRY) {
                        LOGGER.warn(String.format("%s. Retry removeAllSnapshot (%d)...",
                                e.getClass().getName(), retry));
                        try {
                            Thread.sleep(RETRY_INTERVAL);
                        } catch (InterruptedException e1) {
                            // Ignore
                        }
                        continue;
                    }
                }

                LOGGER.error(String.format("VM %s (%s) failed to remove all snapshots",
                        getMor().get_value(), getNameNoFetching()), e);
                return false;
            }
        }

        if (error != null) {
            LOGGER.error(String.format("VM %s (%s) failed to remove all snapshots: %s",
                    getMor().get_value(), getNameNoFetching(), error));
            return false;
        }

        LOGGER.info(String.format("VM %s (%s) removed all snapshots",
                getMor().get_value(), getNameNoFetching()));
        return true;
    }

    public boolean revertToCurrentSnapshot(ActionDelegate<Integer> progressUpdater)
            throws SnapshotFault, TaskInProgress, InvalidState, RuntimeFault, RemoteException {
        LOGGER.info(String.format("VM %s (%s) revert to current snapshots...",
                getMor().get_value(), getNameNoFetching()));

        String error;

        int retry = 0;
        while (true) {
            try {
                Task task = getVirtualMachine().revertToCurrentSnapshot_Task(null, null);
                error = getContext().waitForTask(task, progressUpdater);
                break;
            } catch (Exception e) {
                if ((e instanceof TaskInProgress)
                        || (e instanceof InvalidState)) {
                    if (++retry <= MAX_RETRY) {
                        LOGGER.warn(String.format("%s. Retry revertToCurrentSnapshot (%d)...",
                                e.getClass().getName(), retry));
                        try {
                            Thread.sleep(RETRY_INTERVAL);
                        } catch (InterruptedException e1) {
                            // Ignore
                        }
                        continue;
                    }
                }

                LOGGER.error(String.format("VM %s (%s) failed to revert to the current snapshot",
                        getMor().get_value(), getNameNoFetching()), e);
                return false;
            }
        }

        if (error != null) {
            LOGGER.error(String.format("VM %s (%s) failed to revert to the current snapshot: %s",
                    getMor().get_value(), getNameNoFetching(), error));
            return false;
        }

        LOGGER.info(String.format("VM %s (%s) reverted to the current snapshot",
                getMor().get_value(), getNameNoFetching()));
        return true;
    }

    public boolean revertToSnapshot(String snapshotName, ActionDelegate<Integer> progressUpdater)
            throws VmConfigFault, TaskInProgress, FileFault, InvalidState, InsufficientResourcesFault,
                   RuntimeFault, RemoteException, InterruptedException {
        LOGGER.info(String.format("VM %s (%s) reverting to snapshot \"%s\"...",
                getMor().get_value(), getNameNoFetching(), snapshotName));

        ManagedObjectReference morSnapshot = getSnapshotMor(snapshotName);
        if (morSnapshot == null) {
            LOGGER.warn(String.format("VM %s (%s) unable to find snapshot \"%s\"",
                    getMor().get_value(), getNameNoFetching(), snapshotName));
            return false;
        }

        VirtualMachineSnapshot snapshot = (VirtualMachineSnapshot)
                MorUtil.createExactManagedObject(getContext().getServiceInstance().getServerConnection(), morSnapshot);

        String error;

        int retry = 0;
        while (true) {
            try {
                Task task = snapshot.revertToSnapshot_Task(null);
                error = getContext().waitForTask(task, progressUpdater);
                break;
            } catch (Exception e) {
                if ((e instanceof TaskInProgress)
                        || (e instanceof InvalidState)
                        || (e instanceof InsufficientResourcesFault)) {
                    if (++retry <= MAX_RETRY) {
                        LOGGER.warn(String.format("%s. Retry revertToSnapshot (%d)...",
                                e.getClass().getName(), retry));
                        try {
                            Thread.sleep(RETRY_INTERVAL);
                        } catch (InterruptedException e1) {
                            // Ignore
                        }
                        continue;
                    }
                }

                LOGGER.error(String.format("VM %s (%s) failed to revert to snapshot \"%s\"",
                        getMor().get_value(), getNameNoFetching(), snapshotName), e);
                return false;
            }
        }

        if (error != null) {
            LOGGER.error(String.format("VM %s (%s) failed to revert to snapshot \"%s\": %s",
                    getMor().get_value(), getNameNoFetching(), snapshotName, error));
            return false;
        }

        LOGGER.info(String.format("VM %s (%s) reverted to snapshot \"%s\"",
                getMor().get_value(), getNameNoFetching(), snapshotName));
        return true;
    }

    public void unregister()
            throws InvalidPowerState, RuntimeFault, RemoteException {
        LOGGER.info(String.format("VM %s (%s) unregistering...", getMor().get_value(), getNameNoFetching()));

        int retry = 0;
        while (true) {
            try {
                getVirtualMachine().unregisterVM();
                break;
            } catch (Exception e) {
                if ((e instanceof TaskInProgress)
                        || (e instanceof InvalidState)) {
                    if (++retry <= MAX_RETRY) {
                        LOGGER.warn(String.format("%s. Retry unregister (%d)...",
                                e.getClass().getName(), retry));
                        try {
                            Thread.sleep(RETRY_INTERVAL);
                        } catch (InterruptedException e1) {
                            // Ignore
                        }
                        continue;
                    }
                }

                LOGGER.error(String.format("VM %s (%s) failed to unregister",
                        getMor().get_value(), getNameNoFetching()), e);
                return;
            }
        }
    }

    /*
     * return the full DS path (i.e. [dsName] <path>/<fileName>.vmdk) of this
     * VM's VMDK file under current snapshot (if any).
     */
    public String getVirtualDiskDsFileName() {
        VirtualMachineConfigInfo configInfo = null;

        //if (getSnapshotInfo() != null) {
        //    // there is snapshot, get the current snapshot's disk
        //    VirtualMachineSnapshot currentSnapshot = new VirtualMachineSnapshot(
        //            getContext().getServiceInstance().getServerConnection(),
        //            getSnapshotInfo().getCurrentSnapshot());
        //    configInfo = currentSnapshot.getConfig();
        //} else {
            configInfo = getConfigInfo();
        //}

        VirtualDevice devices[] = configInfo.getHardware().getDevice();

        for (VirtualDevice device : devices) {
            if (device instanceof VirtualDisk &&
                    ((VirtualDisk)device).getBacking() instanceof VirtualDeviceFileBackingInfo) {
                return ((VirtualDeviceFileBackingInfo)((VirtualDisk)device).getBacking()).getFileName();
            }
        }

        return null;
    }

    public boolean exportVm(String exportDir, String exportName, final ActionDelegate<Integer> progressUpdater)
            throws InvalidPowerState, TaskInProgress, InvalidState, FileFault, RuntimeFault, RemoteException {
        LOGGER.info(String.format("VM %s (%s) exporting to dir \"%s\" with name \"%s\"...",
                getMor().get_value(), getNameNoFetching(), exportDir, exportName));

        VmwareContext context = getContext();
        VirtualMachine virtualMachine = getVirtualMachine();

        String hostIp = context.getServerAddress();

        HttpNfcLease hnLease;
        try {
            hnLease = virtualMachine.exportVm();
        } catch (Exception e) {
            LOGGER.error(String.format("VM %s (%s) failed to exportVm",
                    getMor().get_value(), getNameNoFetching()), e);
            return false;
        }

        if (hnLease == null) {
            LOGGER.error(String.format("VM %s (%s) failed to exportVm - no lease",
                    getMor().get_value(), getNameNoFetching()));
            return false;
        }

        // Wait until the HttpNfcLeaseState is ready.
        HttpNfcLeaseState hnlState;
        while (true) {
            hnlState = hnLease.getState();
            if (hnlState == HttpNfcLeaseState.ready) {
                break;
            } else if(hnlState == HttpNfcLeaseState.error) {
                LOGGER.error(String.format("VM %s (%s) failed to exportVm - lease error",
                        getMor().get_value(), getNameNoFetching()));
                hnLease.httpNfcLeaseProgress(100);
                hnLease.httpNfcLeaseComplete();
                return false;
            }
        }

        try {
            final ProgressReporter progressReporter = new ProgressReporter(hnLease);

            List<String> fileNames = new ArrayList<String>();
            try {
                HttpNfcLeaseInfo leaseInfo = hnLease.getInfo();
                leaseInfo.setLeaseTimeout(300 * 1000 * 1000);

                final long totalBytes = leaseInfo.getTotalDiskCapacityInKB() * 1024;
                long totalBytesDownloaded = 0;

                //System.out.println(String.format("Start: %s", new Date()));

                HttpNfcLeaseDeviceUrl[] deviceUrls = leaseInfo.getDeviceUrl();
                if (deviceUrls != null) {
                    OvfFile[] ovfFiles = new OvfFile[deviceUrls.length];
                    for (int i = 0; i < deviceUrls.length; i++) {
                        String deviceId = deviceUrls[i].getKey();
                        String deviceUrlStr = deviceUrls[i].getUrl();
                        String orgDiskFileName = deviceUrlStr.substring(deviceUrlStr.lastIndexOf("/") + 1);
                        String diskFileName = String.format("%s-disk%d%s", exportName, i, getFileExtension(orgDiskFileName, ".vmdk"));
                        String diskUrlStr = deviceUrlStr.replace("*", hostIp);
                        String diskLocalPath = exportDir + File.separator + diskFileName;
                        fileNames.add(diskLocalPath);

                        LOGGER.info("Download VMDK file for export. url: " + deviceUrlStr);

                        final long tmpTotalBytesDownloaded = totalBytesDownloaded;

                        long lengthOfDiskFile = VmwareUtils.getDatastoreFile(context, diskUrlStr, diskLocalPath,
                            new ActionDelegate<Long>() {
                                @Override
                                public void action(Long param) {
                                    int percent = (int)((tmpTotalBytesDownloaded + param) * 100 / totalBytes);
                                    progressReporter.reportProgress(percent);
                                    //System.out.print(String.format("%d / %d\r", tmpTotalBytesDownloaded + param, totalBytes));

                                    if (progressUpdater != null) {
                                        progressUpdater.action(percent);
                                    }
                                }
                        });
                        totalBytesDownloaded += lengthOfDiskFile;

                        OvfFile ovfFile = new OvfFile();
                        ovfFile.setPath(diskFileName);
                        ovfFile.setDeviceId(deviceId);
                        ovfFile.setSize(lengthOfDiskFile);
                        ovfFiles[i] = ovfFile;
                    }

                    OvfManager ovfManager = context.getServiceInstance().getOvfManager();

                    // write OVF descriptor file
                    OvfCreateDescriptorParams ovfDescParams = new OvfCreateDescriptorParams();
                    ovfDescParams.setOvfFiles(ovfFiles);
                    OvfCreateDescriptorResult ovfCreateDescriptorResult = ovfManager.createDescriptor(this.getManagedEntity(), ovfDescParams);
                    String ovfPath = exportDir + File.separator + exportName + ".ovf";
                    fileNames.add(ovfPath);

                    FileWriter out = new FileWriter(ovfPath);
                    out.write(ovfCreateDescriptorResult.getOvfDescriptor());
                    out.close();
                }

                //System.out.println(String.format("Complete: %s", new Date()));
            } catch (Throwable e) {
                LOGGER.error(String.format("VM %s (%s) failed to exportVm",
                        getMor().get_value(), getNameNoFetching()), e);
                return false;
            } finally {
                progressReporter.close();
            }
        } finally {
            hnLease.httpNfcLeaseProgress(100);
            hnLease.httpNfcLeaseComplete();
        }

        LOGGER.info(String.format("VM %s (%s) exported to dir \"%s\" with name \"%s\"",
                getMor().get_value(), getNameNoFetching(), exportDir, exportName));
        return true;
    }

    private static String getFileExtension(String fileName, String defaultExtension) {
        int pos = fileName.lastIndexOf('.');
        if (pos < 0) {
            return defaultExtension;
        }

        return fileName.substring(pos);
    }

    class ProgressReporter extends Thread {
        private final HttpNfcLease _httpNfcLease;
        volatile int _percent;
        volatile boolean _done;

        public ProgressReporter(HttpNfcLease httpNfcLease) {
            _httpNfcLease = httpNfcLease;
            _percent = 0;
            _done = false;

            setDaemon(true);
            start();
        }

        public void reportProgress(int percent) {
            _percent = percent;
        }

        public void close() {
            LOGGER.info("Closing ProgressReporter...");

            _done = true;
            interrupt();
        }

        private void updateLeaseProgress(int percent) throws Exception {
            // make sure percentage is in right range
            if(percent < 0)
                percent = 0;
            else if(percent > 100)
                percent = 100;

            _httpNfcLease.httpNfcLeaseProgress(percent);
        }

        @Override
        public void run() {
            while(!_done) {
                try {
                    Thread.sleep(1000);         // update progess every 1 second
                    updateLeaseProgress(_percent);
                } catch(InterruptedException e) {
                    LOGGER.info("ProgressReporter is interrupted");
                    break;
                } catch(Exception e) {
                    LOGGER.warn("Unexpected exception ", e);
                }
            }

            LOGGER.info("ProgressReporter stopped");
        }
    }

    // vmdkDatastorePath: [datastore name] vmdkFilePath
    public boolean createDisk(String vmdkDatastorePath, long sizeInMB, DatastoreMO datastore, int controllerKey,
            ActionDelegate<Integer> progressUpdater) throws Exception {
        return createDisk(vmdkDatastorePath, VirtualDiskType.thin, VirtualDiskMode.persistent, null,
                sizeInMB, datastore, controllerKey, progressUpdater);
    }

    // vmdkDatastorePath: [datastore name] vmdkFilePath
    public boolean createDisk(String vmdkDatastorePath, VirtualDiskType diskType, VirtualDiskMode diskMode,
            String rdmDeviceName, long sizeInMB, DatastoreMO datastore, int controllerKey,
            ActionDelegate<Integer> progressUpdater) throws Exception {
        LOGGER.info(String.format("VM %s (%s) creating disk: vmdkDatastorePath=%s, sizeInMb=%d, diskType=%s, diskMode=%s, rdmDeviceName=%s, datastore=\"%s\", controllerKey=%d...",
                getMor().get_value(), getNameNoFetching(), vmdkDatastorePath, sizeInMB, diskType, diskMode, rdmDeviceName, datastore.getNameNoFetching(), controllerKey));

        assert(vmdkDatastorePath != null);
        assert(datastore != null);

        if (controllerKey < 0) {
            try {
                controllerKey = getScsiDeviceControllerKey();
            } catch (Exception e) {
                LOGGER.warn(String.format("VM %s (%s) creating disk: SCSI controller not found. Looking for IDE controller",
                        getMor().get_value(), getNameNoFetching()));

                controllerKey = getIDEDeviceControllerKey();
            }
        }

        VirtualDisk newDisk = new VirtualDisk();
        switch (diskType) {
        case thin:
        case preallocated:
        case eagerZeroedThick:
            {
                VirtualDiskFlatVer2BackingInfo backingInfo = new VirtualDiskFlatVer2BackingInfo();
                backingInfo.setDiskMode(VirtualDiskMode.persistent.toString());
                if (diskType == VirtualDiskType.thin) {
                    backingInfo.setThinProvisioned(true);
                } else {
                    backingInfo.setThinProvisioned(false);
                }

                if (diskType == VirtualDiskType.eagerZeroedThick) {
                    backingInfo.setEagerlyScrub(true);
                } else {
                    backingInfo.setEagerlyScrub(false);
                }

                backingInfo.setDatastore(datastore.getMor());
                backingInfo.setFileName(vmdkDatastorePath);
                newDisk.setBacking(backingInfo);
            }

            break;

        case rdm:
        case rdmp:
            {
                VirtualDiskRawDiskMappingVer1BackingInfo backingInfo = new VirtualDiskRawDiskMappingVer1BackingInfo();
                if(diskType == VirtualDiskType.rdm) {
                    backingInfo.setCompatibilityMode("virtualMode");
                } else {
                    backingInfo.setCompatibilityMode("physicalMode");
                }
                backingInfo.setDeviceName(rdmDeviceName);
                if(diskType == VirtualDiskType.rdm) {
                    backingInfo.setDiskMode(VirtualDiskMode.persistent.toString());
                }

                backingInfo.setDatastore(datastore.getMor());
                backingInfo.setFileName(vmdkDatastorePath);
                newDisk.setBacking(backingInfo);
            }

            break;
        }

        int deviceNumber = getNextDeviceNumber(controllerKey);

        newDisk.setControllerKey(controllerKey);
        newDisk.setKey(-deviceNumber);
        newDisk.setUnitNumber(deviceNumber);
        newDisk.setCapacityInKB(sizeInMB * 1024);

        VirtualMachineConfigSpec reConfigSpec = new VirtualMachineConfigSpec();
        VirtualDeviceConfigSpec[] deviceConfigSpecArray = new VirtualDeviceConfigSpec[1];
        VirtualDeviceConfigSpec deviceConfigSpec = new VirtualDeviceConfigSpec();

        deviceConfigSpec.setDevice(newDisk);
        deviceConfigSpec.setFileOperation(VirtualDeviceConfigSpecFileOperation.create);
        deviceConfigSpec.setOperation(VirtualDeviceConfigSpecOperation.add);

        deviceConfigSpecArray[0] = deviceConfigSpec;
        reConfigSpec.setDeviceChange(deviceConfigSpecArray);

        return reconfig(reConfigSpec, progressUpdater);
    }

    public boolean attachDisk(String[] vmdkDatastorePathChain, DatastoreMO datastore,
            ActionDelegate<Integer> progressUpdater) throws Exception {
        LOGGER.info(String.format("VM %s (%s) attaching disk: vmdkDatastorePathChain=\"%s\"..., datastore=\"%s\"...",
                getMor().get_value(), getNameNoFetching(), vmdkDatastorePathChain[0], datastore.getNameNoFetching()));

        int controllerKey;
        try {
            controllerKey = getScsiDeviceControllerKey();
        } catch (Exception e) {
            LOGGER.warn(String.format("VM %s (%s) creating disk: SCSI controller not found. Looking for IDE controller",
                    getMor().get_value(), getNameNoFetching()));

            controllerKey = getIDEDeviceControllerKey();
        }

        VirtualDevice newDisk = VmwareUtils.prepareDiskDevice(this, controllerKey,
            vmdkDatastorePathChain, datastore, -1, 1);

        VirtualMachineConfigSpec reConfigSpec = new VirtualMachineConfigSpec();
        VirtualDeviceConfigSpec[] deviceConfigSpecArray = new VirtualDeviceConfigSpec[1];
        VirtualDeviceConfigSpec deviceConfigSpec = new VirtualDeviceConfigSpec();

        deviceConfigSpec.setDevice(newDisk);
        deviceConfigSpec.setOperation(VirtualDeviceConfigSpecOperation.add);

        deviceConfigSpecArray[0] = deviceConfigSpec;
        reConfigSpec.setDeviceChange(deviceConfigSpecArray);

        return reconfig(reConfigSpec, progressUpdater);
    }

    // vmdkDatastorePath: [datastore name] vmdkFilePath
    public boolean detachDisk(String vmdkDatastorePath, boolean deleteBackingFile,
            ActionDelegate<Integer> progressUpdater) throws Exception {
        LOGGER.info(String.format("VM %s (%s) detaching disk: vmdkDatastorePath=\"%s\", deleteBackingFile=%s...",
                getMor().get_value(), getNameNoFetching(), vmdkDatastorePath, deleteBackingFile ? "true" : "false"));

        // Note: if VM has been taken snapshot, original backing file will be renamed, therefore, when we try to find the matching
        // VirtualDisk, we only perform prefix matching
        List<Pair<VirtualDisk, String>> deviceInfos = getDiskDevices(vmdkDatastorePath, false);
        if (deviceInfos == null || deviceInfos.isEmpty()) {
            LOGGER.info(String.format("VM %s (%s) has no such disk device: \"%s\"",
                    getMor().get_value(), getNameNoFetching(), vmdkDatastorePath));
            return true;
        }

        VirtualMachineConfigSpec reConfigSpec = new VirtualMachineConfigSpec();
        VirtualDeviceConfigSpec[] deviceConfigSpecArray = new VirtualDeviceConfigSpec[1];
        VirtualDeviceConfigSpec deviceConfigSpec = new VirtualDeviceConfigSpec();

        deviceConfigSpec.setDevice(deviceInfos.get(0).getFirst());
        if (deleteBackingFile) {
            deviceConfigSpec.setFileOperation(VirtualDeviceConfigSpecFileOperation.destroy);
        }
        deviceConfigSpec.setOperation(VirtualDeviceConfigSpecOperation.remove);

        deviceConfigSpecArray[0] = deviceConfigSpec;
        reConfigSpec.setDeviceChange(deviceConfigSpecArray);

        return reconfig(reConfigSpec, progressUpdater);
    }

    // vmdkDatastorePath: [datastore name] vmdkFilePath
    public boolean detachAllDisks(String vmdkDatastorePath, boolean deleteBackingFile,
            ActionDelegate<Integer> progressUpdater) throws Exception {
        LOGGER.info(String.format("VM %s (%s) detaching all disks: vmdkDatastorePath=\"%s\", deleteBackingFile=%s...",
                getMor().get_value(), getNameNoFetching(), vmdkDatastorePath, deleteBackingFile ? "true" : "false"));

        // Note: if VM has been taken snapshot, original backing file will be renamed, therefore, when we try to find the matching
        // VirtualDisk, we only perform prefix matching
        List<Pair<VirtualDisk, String>> deviceInfos = getDiskDevices(vmdkDatastorePath, false);
        if (deviceInfos == null || deviceInfos.isEmpty()) {
            LOGGER.info(String.format("VM %s (%s) has no such disk device: \"%s\"",
                    getMor().get_value(), getNameNoFetching(), vmdkDatastorePath));
            return true;
        }

        VirtualMachineConfigSpec reConfigSpec = new VirtualMachineConfigSpec();
        VirtualDeviceConfigSpec[] deviceConfigSpecArray = new VirtualDeviceConfigSpec[deviceInfos.size()];

        int idx = 0;
        for (Pair<VirtualDisk, String> deviceInfo : deviceInfos) {
            VirtualDeviceConfigSpec deviceConfigSpec = new VirtualDeviceConfigSpec();
            deviceConfigSpec.setDevice(deviceInfo.getFirst());
            if (deleteBackingFile) {
                deviceConfigSpec.setFileOperation(VirtualDeviceConfigSpecFileOperation.destroy);
            }
            deviceConfigSpec.setOperation(VirtualDeviceConfigSpecOperation.remove);

            deviceConfigSpecArray[idx] = deviceConfigSpec;

            ++idx;
        }

        reConfigSpec.setDeviceChange(deviceConfigSpecArray);

        return reconfig(reConfigSpec, progressUpdater);
    }

    public boolean detachIso(ActionDelegate<Integer> progressUpdater)
            throws Exception {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("VM %s (%s) detaching iso...", getMor().get_value(), getNameNoFetching()));
        }

        VirtualDevice device = getIsoDevice();
        if (device == null) {
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace(String.format("VM %s (%s) no iso", getMor().get_value(), getNameNoFetching()));
            }
            return true;
        }

        VirtualCdromRemotePassthroughBackingInfo backingInfo = new VirtualCdromRemotePassthroughBackingInfo();
        backingInfo.setDeviceName("");
        device.setBacking(backingInfo);

        VirtualMachineConfigSpec reConfigSpec = new VirtualMachineConfigSpec();
        VirtualDeviceConfigSpec[] deviceConfigSpecArray = new VirtualDeviceConfigSpec[1];
        VirtualDeviceConfigSpec deviceConfigSpec = new VirtualDeviceConfigSpec();

        deviceConfigSpec.setDevice(device);
        deviceConfigSpec.setOperation(VirtualDeviceConfigSpecOperation.edit);

        deviceConfigSpecArray[0] = deviceConfigSpec;
        reConfigSpec.setDeviceChange(deviceConfigSpecArray);

        return reconfig(reConfigSpec, progressUpdater);
    }

    public VirtualDevice getIsoDevice() throws Exception {
        VirtualDevice[] devices = getConfigInfo().getHardware().getDevice();

        if (devices != null && devices.length > 0) {
            for (VirtualDevice device : devices) {
                if (device instanceof VirtualCdrom) {
                    return device;
                }
            }
        }

        return null;
    }

    public int getScsiDeviceControllerKey() throws Exception {
        VirtualDevice[] devices = getConfigInfo().getHardware().getDevice();

        if (devices != null && devices.length > 0) {
            for (VirtualDevice device : devices) {
                if (device instanceof VirtualSCSIController) {
                    return device.getKey();
                }
            }
        }

        assert(false);
        throw new Exception("SCSI Controller Not Found");
    }

    public int getIDEDeviceControllerKey() throws Exception {
        VirtualDevice[] devices = getConfigInfo().getHardware().getDevice();

        if (devices != null && devices.length > 0) {
            for (VirtualDevice device : devices) {
                if (device instanceof VirtualIDEController) {
                    return ((VirtualIDEController) device).getKey();
                }
            }
        }

        assert(false);
        throw new Exception("IDE Controller Not Found");
    }

    public int getNextDeviceNumber(int controllerKey) throws Exception {
        VirtualDevice[] devices = getConfigInfo().getHardware().getDevice();

        int deviceNumber = -1;
        if (devices != null && devices.length > 0) {
            for (VirtualDevice device : devices) {
                if (device.getControllerKey() != null && device.getControllerKey().intValue() == controllerKey) {
                    if (device.getUnitNumber() != null && device.getUnitNumber().intValue() > deviceNumber) {
                        deviceNumber = device.getUnitNumber().intValue();
                    }
                }
            }
        }

        return ++deviceNumber;
    }

    // return pair of VirtualDisk and disk device bus name(ide0:0, etc)
    public List<Pair<VirtualDisk, String>> getDiskDevices(String vmdkDatastorePath, boolean matchExactly) throws Exception {
        VirtualDevice[] devices = getConfigInfo().getHardware().getDevice();

        LOGGER.info(String.format("VM %s (%s) looking for disk device info from volume: \"%s\"...",
                getMor().get_value(), getNameNoFetching(), vmdkDatastorePath));

        List<Pair<VirtualDisk, String>> diskDevices = new LinkedList<Pair<VirtualDisk, String>>();

        DatastoreFile dsSrcFile = new DatastoreFile(vmdkDatastorePath);
        String srcFilePath = dsSrcFile.getFullPath();

        if (devices != null && devices.length > 0) {
            for (VirtualDevice device : devices) {
                if (device instanceof VirtualDisk) {
                    LOGGER.info("Test against disk device, controller key: " + device.getControllerKey() + ", unit number: " + device.getUnitNumber());

                    VirtualDeviceBackingInfo backingInfo = ((VirtualDisk)device).getBacking();
                    if (backingInfo instanceof VirtualDiskFlatVer2BackingInfo) {
                        VirtualDiskFlatVer2BackingInfo diskBackingInfo = (VirtualDiskFlatVer2BackingInfo)backingInfo;
                        do {
                            LOGGER.info("Test against disk backing : " + diskBackingInfo.getFileName());

                            DatastoreFile dsBackingFile = new DatastoreFile(diskBackingInfo.getFileName());
                            String backingFilePath = dsBackingFile.getFullPath();
                            if (matchExactly) {
                                if (backingFilePath.equalsIgnoreCase(srcFilePath)) {
                                    String deviceNumbering = getDeviceBusName(devices, device);

                                    LOGGER.info("Disk backing : " + diskBackingInfo.getFileName() + " matches ==> " + deviceNumbering);
                                    diskDevices.add(new Pair<VirtualDisk, String>((VirtualDisk) device, deviceNumbering));

                                    break;
                                }
                            } else {
                                if (backingFilePath.contains(srcFilePath)) {
                                    String deviceNumbering = getDeviceBusName(devices, device);

                                    LOGGER.info("Disk backing : " + diskBackingInfo.getFileName() + " matches ==> " + deviceNumbering);
                                    diskDevices.add(new Pair<VirtualDisk, String>((VirtualDisk) device, deviceNumbering));

                                    break;
                                }
                            }

                            diskBackingInfo = diskBackingInfo.getParent();
                        } while (diskBackingInfo != null);
                    }
                }
            }
        }

        return diskDevices;
    }

    private String getDeviceBusName(VirtualDevice[] allDevices, VirtualDevice theDevice) throws Exception {
        for (VirtualDevice device : allDevices) {
            if (device.getKey() == theDevice.getControllerKey().intValue()) {
                if (device instanceof VirtualIDEController) {
                    return String.format("ide%d:%d", ((VirtualIDEController) device).getBusNumber(), theDevice.getUnitNumber());
                } else if (device instanceof VirtualSCSIController) {
                    return String.format("scsi%d:%d", ((VirtualSCSIController) device).getBusNumber(), theDevice.getUnitNumber());
                } else {
                    throw new Exception("Device controller is not supported yet");
                }
            }
        }
        throw new Exception("Unable to find device controller");
    }
}

