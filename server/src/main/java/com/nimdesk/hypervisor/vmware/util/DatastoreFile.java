/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.hypervisor.vmware.util;

import org.apache.commons.lang.StringUtils;

/**
 *
 */
public class DatastoreFile {
    private String _filePath;

    public DatastoreFile(String filePath) {
        assert(!StringUtils.isEmpty(filePath));
        _filePath = filePath.trim();
    }

    public DatastoreFile(String datastoreName, String filePath) {
        _filePath = String.format("[%s] %s", datastoreName, filePath);
    }

    public DatastoreFile(String datastoreName, String fileDir, String fileName) {
        if (StringUtils.isEmpty(fileDir)) {
            _filePath = String.format("[%s] %s", datastoreName, fileName);
        } else {
            _filePath = String.format("[%s] %s/%s", datastoreName, fileDir, fileName);
        }
    }

    public String getFullPath() {
        return _filePath;
    }

    public String getDatastoreName() {
        return getDatastoreNameFromPath(_filePath);
    }

    public String getRelativePath() {
        int pos = _filePath.indexOf(']');
        if (pos < 0) {
            return _filePath;
        }
        return _filePath.substring(pos + 1).trim();
    }

    public String getDirFullPath() {
        int pos = _filePath.lastIndexOf('/');
        if (pos < 0) {
            return _filePath;
        }
        return _filePath.substring(0, pos).trim();
    }

    public String getDir() {
        int startPos = _filePath.indexOf("]");
        if (startPos < 0) {
            startPos = 0;
        }

        int endPos = _filePath.lastIndexOf('/');
        if (endPos < 0) {
            endPos = 0;
        }

        if (endPos > startPos) {
            return _filePath.substring(startPos + 1, endPos).trim();
        }

        return "";
    }

    public String getFileName() {
        int pos = _filePath.lastIndexOf('/');
        if (pos < 0) {
            return getRelativePath();
        }

        return _filePath.substring(pos + 1).trim();
    }

    public String getFileBaseName() {
        String name = getFileName();

        int pos = name.lastIndexOf('.');
        if (pos < 0) {
            return name;
        }

        return name.substring(0, pos);
    }

    public static boolean isFullDatastorePath(String path) {
        return path.matches("^\\[.*\\].*");
    }

    public static String composeFullDatastorePath(String datastoreName, String path) {
        return String.format("[%s] %s", datastoreName, path);
    }

    public static String getDatastoreNameFromPath(String path) {
        if (!isFullDatastorePath(path)) {
            return null;
        }

        int startPos = path.indexOf('[');
        if (startPos < 0) {
            return null;
        }

        int endPos = path.indexOf(']', startPos);
        if (endPos < 0) {
            return null;
        }

        return path.substring(startPos + 1, endPos).trim();
    }
}
