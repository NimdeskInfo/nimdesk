/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.hypervisor.vmware.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

import org.apache.log4j.Logger;


/**
 * VmdkDescriptorFile handles .vmdk parsing
 */
public class VmdkDescriptorFile {
    private static final Logger LOGGER = Logger.getLogger(VmdkDescriptorFile.class);

    private final Properties _properties = new Properties();
    private String _baseFileName;

    public VmdkDescriptorFile() {
    }

    public void parse(File vmdkFile) throws IOException {
        InputStream ins = null;
        BufferedReader reader = null;

        try {
            ins = new FileInputStream(vmdkFile);
            reader = new BufferedReader(new InputStreamReader(ins));

            String line;
            while ((line = reader.readLine()) != null) {
                // ignore empty and comment lines
                line = line.trim();
                if (line.isEmpty()) {
                    continue;
                }
                if (line.charAt(0) == '#') {
                    continue;
                }

                String[] tokens = line.split("=");
                if (tokens.length == 2) {
                    String name = tokens[0].trim();
                    String value = tokens[1].trim();
                    if(value.charAt(0) == '\"')
                        value = value.substring(1, value.length() -1);

                    _properties.put(name, value);
                } else {
                    if (line.startsWith("RW")) {
                        int startPos = line.indexOf('\"');
                        int endPos = line.lastIndexOf('\"');
                        assert(startPos > 0);
                        assert(endPos > 0);

                        _baseFileName = line.substring(startPos + 1, endPos);
                    } else {
                        LOGGER.warn("Unrecognized vmdk line content: " + line);
                    }
                }
            }
        } finally {
            if (ins != null) {
                try {
                    ins.close();
                } catch (IOException e) {
                    // Ignore
                }
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    // Ignore
                }
            }
        }
    }

    public String getBaseFileName() {
        return _baseFileName;
    }

    public String getParentFileName() {
        return _properties.getProperty("parentFileNameHint");
    }

    public String getCID() {
        return _properties.getProperty("CID");
    }

    public String getParentCID() {
        return _properties.getProperty("parentCID");
    }
}
