/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.hypervisor.vmware.mo;

import java.rmi.RemoteException;

import com.nimdesk.hypervisor.vmware.VmwareContext;
import com.vmware.vim25.ConcurrentAccess;
import com.vmware.vim25.DuplicateName;
import com.vmware.vim25.InsufficientResourcesFault;
import com.vmware.vim25.InvalidName;
import com.vmware.vim25.ManagedObjectReference;
import com.vmware.vim25.ResourceConfigSpec;
import com.vmware.vim25.RuntimeFault;
import com.vmware.vim25.mo.ResourcePool;

/**
 *
 */
public class ResourcePoolMO extends BaseMO {
    public static final String MORTYPE_RESOURCEPOOL = "ResourcePool";

    public ResourcePoolMO(VmwareContext context, ManagedObjectReference mor) {
        super(context, mor);
    }

    public ResourcePoolMO(VmwareContext context, ManagedObjectReference mor, String name) {
        super(context, mor, name);
    }

    public ResourcePoolMO(VmwareContext context, String rpRefId) {
        super(context, MORTYPE_RESOURCEPOOL, rpRefId);
    }

    public ResourcePoolMO(VmwareContext context, String rpRefId, String name) {
        super(context, MORTYPE_RESOURCEPOOL, rpRefId, name);
    }

    public ResourcePool getResourcePool() {
        return (ResourcePool) getManagedEntity();
    }

    public ResourceConfigSpec getConfig() {
        return getResourcePool().getConfig();
    }

    public ResourcePoolMO createResourcePool(String name, ResourceConfigSpec spec)
            throws InvalidName, DuplicateName, InsufficientResourcesFault, RuntimeFault, RemoteException {
        ResourcePool resourcePool = getResourcePool().createResourcePool(name, spec);
        if (resourcePool == null) {
            return null;
        }

        return new ResourcePoolMO(getContext(), resourcePool.getMOR(), name);
    }

    public void updateConfig(String name, ResourceConfigSpec spec)
            throws InvalidName, DuplicateName, ConcurrentAccess, InsufficientResourcesFault, RuntimeFault, RemoteException {
        getResourcePool().updateConfig(name, spec);
    }
}

