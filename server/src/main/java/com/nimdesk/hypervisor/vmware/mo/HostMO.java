/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.hypervisor.vmware.mo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.util.Assert;

import com.nimdesk.hypervisor.vmware.VmwareContext;
import com.nimdesk.hypervisor.vmware.util.VmwareUtils;
import com.nimdesk.util.ActionDelegate;
import com.nimdesk.util.Pair;
import com.vmware.vim25.AlreadyExists;
import com.vmware.vim25.DatastoreSummary;
import com.vmware.vim25.DuplicateName;
import com.vmware.vim25.FileFault;
import com.vmware.vim25.HostListSummary;
import com.vmware.vim25.HostNasVolumeSpec;
import com.vmware.vim25.HttpNfcLeaseDeviceUrl;
import com.vmware.vim25.HttpNfcLeaseInfo;
import com.vmware.vim25.HttpNfcLeaseState;
import com.vmware.vim25.InsufficientResourcesFault;
import com.vmware.vim25.InvalidDatastore;
import com.vmware.vim25.InvalidName;
import com.vmware.vim25.InvalidProperty;
import com.vmware.vim25.ManagedObjectReference;
import com.vmware.vim25.NotFound;
import com.vmware.vim25.ObjectContent;
import com.vmware.vim25.ObjectSpec;
import com.vmware.vim25.OptionValue;
import com.vmware.vim25.OutOfBounds;
import com.vmware.vim25.OvfCreateImportSpecParams;
import com.vmware.vim25.OvfCreateImportSpecResult;
import com.vmware.vim25.OvfFileItem;
import com.vmware.vim25.PropertyFilterSpec;
import com.vmware.vim25.PropertySpec;
import com.vmware.vim25.RuntimeFault;
import com.vmware.vim25.SelectionSpec;
import com.vmware.vim25.TraversalSpec;
import com.vmware.vim25.VmConfigFault;
import com.vmware.vim25.mo.ComputeResource;
import com.vmware.vim25.mo.Datastore;
import com.vmware.vim25.mo.Folder;
import com.vmware.vim25.mo.HostDatastoreSystem;
import com.vmware.vim25.mo.HostSystem;
import com.vmware.vim25.mo.HttpNfcLease;
import com.vmware.vim25.mo.ManagedEntity;
import com.vmware.vim25.mo.OptionManager;
import com.vmware.vim25.mo.OvfManager;
import com.vmware.vim25.mo.PropertyCollector;
import com.vmware.vim25.mo.ResourcePool;
import com.vmware.vim25.mo.Task;
import com.vmware.vim25.mo.util.PropertyCollectorUtil;

/**
 *
 */
public class HostMO extends BaseMO {
    private static final Logger LOGGER = Logger.getLogger(HostMO.class);

    public static final String MORTYPE_HOST = "HostSystem";

    public HostMO(VmwareContext context, ManagedObjectReference mor) {
        super(context, mor);
    }

    public HostMO(VmwareContext context, ManagedObjectReference mor, String name) {
        super(context, mor, name);
    }

    public HostMO(VmwareContext context, String hostRefId) {
        super(context, MORTYPE_HOST, hostRefId);
    }

    public HostMO(VmwareContext context, String hostRefId, String name) {
        super(context, MORTYPE_HOST, hostRefId, name);
    }

    public HostSystem getHostSystem() {
        return (HostSystem) getManagedEntity();
    }

    public static HostMO findByEsxUuid(VmwareContext context, String targetEsxUuid)
            throws Exception {
        Assert.hasText(targetEsxUuid, "Empty ESX uuid");

        DatacenterMO datacenter = context.getRootDatacenter();
        if (datacenter == null) {
            throw new Exception("Unable to find root datacenter");
        }

        ObjectContent[] ocs = datacenter.getHostProperties(new String[] {"name", "summary.hardware.uuid"});
        if (ocs == null || ocs.length == 0) {
            throw new Exception("Unable to find any host");
        }

        for (ObjectContent oc : ocs) {
            String esxUuid = (String) VmwareUtils.getPropValue(oc, "summary.hardware.uuid");
            if (StringUtils.equals(targetEsxUuid, esxUuid)) {
                ManagedObjectReference morHost = oc.getObj();
                String hostName = (String) VmwareUtils.getPropValue(oc, "name");
                return new HostMO(context, morHost, hostName);
            }
        }

        return null;
    }

    public List<DatastoreMO> getLocalDatastores()
            throws InvalidProperty, RuntimeFault, RemoteException {
        List<DatastoreMO> dsList = new ArrayList<DatastoreMO>();

        ObjectContent[] ocs = getDatastoreProperties(new String[] {"name", "summary"});
        if (ocs != null) {
            for (ObjectContent oc : ocs) {
                DatastoreSummary dsSummary = (DatastoreSummary) VmwareUtils.getPropValue(oc, "summary");
                if (!dsSummary.getMultipleHostAccess()) {
                    ManagedObjectReference morDs = oc.getObj();
                    String dsName = (String) VmwareUtils.getPropValue(oc, "name");

                    dsList.add(new DatastoreMO(getContext(), morDs, dsName));
                }
            }
        }
        return dsList;
    }

    public HostListSummary getSummary() {
        return getHostSystem().getSummary();
    }

    public ResourcePoolMO getOwnerResourcePool() {
        ManagedEntity parent = getHostSystem().getParent();
        if (parent instanceof ComputeResource) {
            ComputeResource computeResource = (ComputeResource) parent;
            ResourcePool resourcePool = computeResource.getResourcePool();
            if (resourcePool != null) {
                return new ResourcePoolMO(getContext(), resourcePool.getMOR());
            }
        }

        return null;
    }

    @SuppressWarnings("deprecation")
    public ObjectContent[] getDatastoreProperties(String[] propPaths)
            throws InvalidProperty, RuntimeFault, RemoteException {
        PropertySpec propSpec = PropertyCollectorUtil.createPropertySpec(DatastoreMO.MORTYPE_DATASTORE, false, propPaths);

        // Traversal through datastore
        TraversalSpec hToDs = PropertyCollectorUtil.createTraversalSpec("hToDs",
                "HostSystem", "datastore",
                new String[] {});

        ObjectSpec objSpec = new ObjectSpec();
        objSpec.setObj(getMor());
        objSpec.setSkip(Boolean.TRUE);
        objSpec.setSelectSet(new SelectionSpec[] { hToDs });

        PropertyFilterSpec pfSpec = new PropertyFilterSpec();
        pfSpec.setPropSet(new PropertySpec[] { propSpec });
        pfSpec.setObjectSet(new ObjectSpec[] { objSpec });

        PropertyCollector propCollector = getContext().getServiceInstance().getPropertyCollector();

        return propCollector.retrieveProperties(new PropertyFilterSpec[] { pfSpec });
    }

    @SuppressWarnings("deprecation")
    public ObjectContent[] getVmProperties(String[] propPaths)
            throws InvalidProperty, RuntimeFault, RemoteException {
        PropertySpec propSpec = PropertyCollectorUtil.createPropertySpec(VirtualMachineMO.MORTYPE_VIRTUALMACHINE, false, propPaths);

        // Traversal through vm
        TraversalSpec hToVm = PropertyCollectorUtil.createTraversalSpec("hToVm",
                "HostSystem", "vm",
                new String[] {});

        ObjectSpec objSpec = new ObjectSpec();
        objSpec.setObj(getMor());
        objSpec.setSkip(Boolean.TRUE);
        objSpec.setSelectSet(new SelectionSpec[] { hToVm });

        PropertyFilterSpec pfSpec = new PropertyFilterSpec();
        pfSpec.setPropSet(new PropertySpec[] { propSpec });
        pfSpec.setObjectSet(new ObjectSpec[] { objSpec });

        PropertyCollector propCollector = getContext().getServiceInstance().getPropertyCollector();

        return propCollector.retrieveProperties(new PropertyFilterSpec[] { pfSpec });
    }

    public boolean isVmRegistered(String vmName)
            throws InvalidProperty, RuntimeFault, RemoteException {
        ObjectContent[] ocs = getVmProperties(new String[] {"name"});
        if (ocs != null) {
            for (ObjectContent oc : ocs) {
                String name = (String) VmwareUtils.getPropValue(oc, "name");
                if (StringUtils.equals(name, vmName)) {
                    return true;
                }
            }
        }
        return false;
    }

    @SuppressWarnings("deprecation")
    public ObjectContent[] getNetworkProperties(String[] propPaths)
            throws InvalidProperty, RuntimeFault, RemoteException {
        PropertySpec propSpec = PropertyCollectorUtil.createPropertySpec("Network", false, propPaths);

        // Traversal through network
        TraversalSpec hToNetwork = PropertyCollectorUtil.createTraversalSpec("hToNetwork",
                "HostSystem", "network",
                new String[] {});

        ObjectSpec objSpec = new ObjectSpec();
        objSpec.setObj(getMor());
        objSpec.setSkip(Boolean.TRUE);
        objSpec.setSelectSet(new SelectionSpec[] { hToNetwork });

        PropertyFilterSpec pfSpec = new PropertyFilterSpec();
        pfSpec.setPropSet(new PropertySpec[] { propSpec });
        pfSpec.setObjectSet(new ObjectSpec[] { objSpec });

        PropertyCollector propCollector = getContext().getServiceInstance().getPropertyCollector();

        return propCollector.retrieveProperties(new PropertyFilterSpec[] { pfSpec });
    }

    public boolean registerVmOnHost(String vmName, String vmxPath, boolean renameOnDuplicateName, ActionDelegate<Integer> progressUpdater)
            throws VmConfigFault, InvalidName, DuplicateName, FileFault, OutOfBounds, InsufficientResourcesFault,
                   InvalidDatastore, AlreadyExists, NotFound, RuntimeFault, RemoteException {
        VmwareContext context = getContext();

        DatacenterMO datacenter = context.getOwnerDatacenter(getMor());
        ResourcePoolMO ownerResourcePool = getOwnerResourcePool();
        ResourcePoolMO resourcePool = context.getNimdeskResourcePool(ownerResourcePool);
        if (resourcePool == null
                && (resourcePool = ownerResourcePool) == null) {
            resourcePool = context.getRootResourcePool();
        }
        Folder vmFolder = datacenter.getDatacenter().getVmFolder();

        String origVmName = vmName;
        String error;

        int retry = 0;
        while (true) {
            try {
                Task task = vmFolder.registerVM_Task(vmxPath, vmName, false, resourcePool.getResourcePool(), getHostSystem());
                error = getContext().waitForTask(task, progressUpdater);
                break;
            } catch (DuplicateName e) {
                if (renameOnDuplicateName) {
                    if (++retry <= 100) {
                        LOGGER.warn(String.format("VM \"%s\" (%s) name duplicate. Retry...", vmName, vmxPath));
                        vmName = String.format("%s (%d)", origVmName, retry);
                        continue;
                    } else {
                        LOGGER.error(String.format("VM \"%s\" (%s) name duplicate. Failed.", vmName, vmxPath));
                        return false;
                    }
                }

            } catch (AlreadyExists e) {
                LOGGER.info(String.format("VM \"%s\" (%s) already registered", vmName, vmxPath));
                return true;
            } catch (Exception e) {
                LOGGER.error(String.format("Failed to register VM \"%s\" (%s)", vmName, vmxPath), e);
                return false;
            }
        }

        if (error != null) {
            LOGGER.error(String.format("Failed to register VM \"%s\" (%s): %s", vmName, vmxPath, error));
            return false;
        }

        LOGGER.info(String.format("Registered VM \"%s\" (%s)", vmName, vmxPath));
        return true;
    }

    public boolean importVmFromOvf(String ovfFilePath, String vmName, DatastoreMO datastore, String diskOption,
            final ActionDelegate<Integer> progressUpdater)
            throws IOException {
        LOGGER.info(String.format("Importing ovf \"%s\" as VM \"%s\" on datastore \"%s\", diskOption: \"%s\"...",
                ovfFilePath, vmName, datastore.getNameNoFetching(), diskOption));

        VmwareContext context = getContext();

        String hostIp = context.getServerAddress();

        DatacenterMO datacenter = context.getOwnerDatacenter(getMor());
        ResourcePoolMO ownerResourcePool = getOwnerResourcePool();
        ResourcePoolMO resourcePool = context.getNimdeskResourcePool(ownerResourcePool);
        if (resourcePool == null) {
            resourcePool = context.getRootResourcePool();
        }
        Folder vmFolder = datacenter.getDatacenter().getVmFolder();

        /*
        OvfConsumerOstNode instantiationOst = new OvfConsumerOstNode();
        instantiationOst.setId("");
        instantiationOst.setType("envelope");
        */

        OvfCreateImportSpecParams importSpecParams = new OvfCreateImportSpecParams();
        importSpecParams.setHostSystem(getMor());
        importSpecParams.setLocale("US");
        importSpecParams.setEntityName(vmName);
        importSpecParams.setDeploymentOption("");
        importSpecParams.setDiskProvisioning(diskOption); // diskOption: thin, thick, etc
        importSpecParams.setPropertyMapping(null);
        //importSpecParams.setInstantiationOst(instantiationOst);

        String ovfDescriptor = readOvfContent(ovfFilePath);

        OvfManager ovfManager = context.getServiceInstance().getOvfManager();

        OvfCreateImportSpecResult ovfImportResult;
        try {
            ovfImportResult = ovfManager.createImportSpec(ovfDescriptor, resourcePool.getResourcePool(), datastore.getDatastore(), importSpecParams);
        } catch (Exception e) {
            LOGGER.error(String.format("Failed to createImportSpec for ovf \"%s\" as VM \"%s\" in resource pool \"%s\" on datastore \"%s\", host \"%s\", ovfManager \"%s\"",
                    ovfFilePath, vmName, resourcePool.getMor().get_value(), datastore.getNameNoFetching(), getMor().get_value(), ovfManager.getMOR().get_value()), e);
            return false;
        }

        if (ovfImportResult == null) {
            LOGGER.error(String.format("Failed to createImportSpec for ovf \"%s\" as VM \"%s\" in resource pool \"%s\" on datastore \"%s\"",
                    ovfFilePath, vmName, resourcePool.getMor().get_value(), datastore.getNameNoFetching()));
            return false;
        }

        HttpNfcLease hnLease;
        try {
            hnLease = resourcePool.getResourcePool().importVApp(ovfImportResult.getImportSpec(), vmFolder, getHostSystem());
        } catch (Exception e) {
            LOGGER.error(String.format("Failed to importVApp for ovf \"%s\" as VM \"%s\" on datastore \"%s\"",
                    ovfFilePath, vmName, datastore.getNameNoFetching()), e);
            return false;
        }

        if (hnLease == null) {
            LOGGER.error(String.format("Failed to importVApp for ovf \"%s\" as VM \"%s\" on datastore \"%s\" - no lease",
                    ovfFilePath, vmName, datastore.getNameNoFetching()));
            return false;
        }

        // Wait until the HttpNfcLeaseState is ready.
        HttpNfcLeaseState hnlState;
        while (true) {
            hnlState = hnLease.getState();
            if (hnlState == HttpNfcLeaseState.ready) {
                break;
            } else if(hnlState == HttpNfcLeaseState.error) {
                LOGGER.error(String.format("Failed to importVApp for ovf \"%s\" as VM \"%s\" on datastore \"%s\" - lease error",
                        ovfFilePath, vmName, datastore.getNameNoFetching()));
                hnLease.httpNfcLeaseProgress(100);
                hnLease.httpNfcLeaseComplete();
                return false;
            }
        }

        try {
            final ProgressReporter progressReporter = new ProgressReporter(hnLease);

            try {
                HttpNfcLeaseInfo leaseInfo = hnLease.getInfo();
                leaseInfo.setLeaseTimeout(300 * 1000 * 1000);

                HttpNfcLeaseDeviceUrl[] deviceUrls = leaseInfo.getDeviceUrl();
                if (deviceUrls != null) {
                    final long totalBytes = calcTotalBytes(ovfImportResult);
                    File ovfFile = new File(ovfFilePath);

                    long totalBytesUploaded = 0;

                    for (HttpNfcLeaseDeviceUrl deviceUrl : deviceUrls) {
                        String deviceKey = deviceUrl.getImportKey();
                        for (OvfFileItem ovfFileItem : ovfImportResult.getFileItem()) {
                            if (deviceKey.equals(ovfFileItem.getDeviceId())) {
                                String absoluteFile = ovfFile.getParent() + File.separator + ovfFileItem.getPath();
                                String diskUrlStr = deviceUrl.getUrl().replace("*", hostIp);

                                LOGGER.info("Upload VMDK file for import. url: " + diskUrlStr);

                                final long tmpTotalBytesUploaded = totalBytesUploaded;

                                VmwareUtils.putDatastoreFile(context, ovfFileItem.isCreate() ? "PUT" : "POST", diskUrlStr, absoluteFile,
                                    new ActionDelegate<Long> () {
                                    @Override
                                    public void action(Long param) {
                                        int percent = (int)((tmpTotalBytesUploaded + param) * 100 / totalBytes);
                                        progressReporter.reportProgress(percent);
                                        //System.out.print(String.format("%d / %d\r", tmpTotalBytesUploaded + param, totalBytes));

                                        if (progressUpdater != null) {
                                            progressUpdater.action(percent);
                                        }
                                    }
                                });

                                totalBytesUploaded += ovfFileItem.getSize();
                             }
                         }
                    }
                }
            } catch (Throwable e) {
                LOGGER.error(String.format("Failed to import ovf \"%s\" as VM \"%s\" on datastore \"%s\"",
                        ovfFilePath, vmName, datastore.getNameNoFetching()), e);
                return false;
            } finally {
                progressReporter.close();
            }
        } finally {
            hnLease.httpNfcLeaseProgress(100);
            hnLease.httpNfcLeaseComplete();
        }

        LOGGER.info(String.format("Imported ovf \"%s\" as VM \"%s\" on datastore \"%s\", diskOption: \"%s\"",
                ovfFilePath, vmName, datastore.getNameNoFetching(), diskOption));
        return true;
    }

    private static String readOvfContent(String ovfFilePath)
            throws IOException {
        StringBuffer strContent = new StringBuffer();
        BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(ovfFilePath)));
        String lineStr;
        while ((lineStr = in.readLine()) != null) {
            strContent.append(lineStr);
        }

        in.close();
        return strContent.toString();
    }

    private static long calcTotalBytes(OvfCreateImportSpecResult ovfImportResult) {
        OvfFileItem[] fileItemArr = ovfImportResult.getFileItem();
        long totalBytes = 0;
        if (fileItemArr != null) {
            for (OvfFileItem fi : fileItemArr) {
                totalBytes += fi.getSize();
            }
        }
        return totalBytes;
    }

    class ProgressReporter extends Thread {
        private final HttpNfcLease _httpNfcLease;
        volatile int _percent;
        volatile boolean _done;

        public ProgressReporter(HttpNfcLease httpNfcLease) {
            _httpNfcLease = httpNfcLease;
            _percent = 0;
            _done = false;

            setDaemon(true);
            start();
        }

        public void reportProgress(int percent) {
            _percent = percent;
        }

        public void close() {
            LOGGER.info("Closing ProgressReporter...");

            _done = true;
            interrupt();
        }

        private void updateLeaseProgress(int percent) throws Exception {
            // make sure percentage is in right range
            if(percent < 0)
                percent = 0;
            else if(percent > 100)
                percent = 100;

            _httpNfcLease.httpNfcLeaseProgress(percent);
        }

        @Override
        public void run() {
            while(!_done) {
                try {
                    Thread.sleep(1000);         // update progess every 1 second
                    updateLeaseProgress(_percent);
                } catch(InterruptedException e) {
                    LOGGER.info("ProgressReporter is interrupted");
                    break;
                } catch(Exception e) {
                    LOGGER.warn("Unexpected exception ", e);
                }
            }

            LOGGER.info("ProgressReporter stopped");
        }
    }

    public boolean updateOptions(Pair<String, Object>... options)
            throws InvalidProperty, RuntimeFault, RemoteException {
        if (options == null || options.length == 0) {
            LOGGER.warn("updateOptions: No options are specified");
            return false;
        }

        OptionManager optionManager = getHostSystem().getOptionManager();
        if (optionManager == null) {
            LOGGER.warn("updateOptions: Failed to get OptionManager");
            return false;
        }

        OptionValue[] optionValues = new OptionValue[options.length];
        for (int i = 0; i < options.length; i++) {
            OptionValue optionValue = new OptionValue();
            optionValue.setKey(options[i].getFirst());
            optionValue.setValue(options[i].getSecond());
            optionValues[i] = optionValue;
        }

        try {
            optionManager.updateOptions(optionValues);
        } catch (Exception e) {
            LOGGER.error("Failed to update options", e);
            return false;
        }

        LOGGER.info("Host options updated");
        return true;
    }

    /**
     * Create a new NFS datastore on local host
     *
     * @param newNfsDatastoreName the name of the new NFS datastore. This name is
     *           also used as the directory path (under the /vmfs/volumes) of the
     *           new NFS datastore. For instance, if this name is "my-nfs-ds", the
     *           new datastore [my-nfs-ds] will be located in /vmfs/volumes/my-nfs-ds.
     * @param nfsServer the remote host name or dotted ip address string of the nfs server
     * @param nfsPath   the remote nfs server file system target directory (e.g. "abc/xyz")
     * @param nfsServerLogin optional
     * @param nfsServerPassword optional
     * @return the DaastoreMO of the new NFS datastore
     * @throws Exception (to be more specific)
     */
    public DatastoreMO createNfsDatastore(String newNfsDatastoreName,
            String nfsServer, String nfsPath)
            throws RemoteException {
        return createNfsDatastore(newNfsDatastoreName, nfsServer, nfsPath, null, null);
    }

    /**
     * Create a new NFS datastore on local host
     *
     * @param newNfsDatastoreName the name of the new NFS datastore. This name is
     *           also used as the directory path (under the /vmfs/volumes) of the
     *           new NFS datastore. For instance, if this name is "my-nfs-ds", the
     *           new datastore [my-nfs-ds] will be located in /vmfs/volumes/my-nfs-ds.
     * @param nfsServer the remote host name or dotted ip address string of the nfs server
     * @param nfsPath   the remote nfs server file system target directory (e.g. "abc/xyz")
     * @param nfsServerLogin optional
     * @param nfsServerPassword optional
     * @return the DaastoreMO of the new NFS datastore
     * @throws RemoteException
     */
    public DatastoreMO createNfsDatastore(String newNfsDatastoreName,
            String nfsServer, String nfsPath, String nfsServerLogin, String nfsServerPassword)
            throws RemoteException {
        HostNasVolumeSpec spec = new HostNasVolumeSpec();
        spec.setAccessMode("readWrite");
        spec.setLocalPath(newNfsDatastoreName);
        spec.setRemoteHost(nfsServer);
        spec.setRemotePath(nfsPath);
        spec.setType("NFS");
        if (nfsServerLogin != null) {
           spec.setUserName(nfsServerLogin);
           spec.setPassword(nfsServerPassword);
        }
        Datastore ds = getHostDatastoreSystem().createNasDatastore(spec);
        return new DatastoreMO(getContext(), ds.getMOR());
    }

    public HostDatastoreSystem getHostDatastoreSystem()
            throws RemoteException {
        return getHostSystem().getHostDatastoreSystem();
    }
}
