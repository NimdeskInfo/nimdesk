/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.hypervisor.vmware.mo;

import com.nimdesk.hypervisor.vmware.VmwareContext;
import com.vmware.vim25.ManagedObjectReference;
import com.vmware.vim25.mo.ManagedEntity;
import com.vmware.vim25.mo.ManagedObject;
import com.vmware.vim25.mo.util.MorUtil;

/**
 *
 */
public class BaseMO {

    private final VmwareContext _context;
    private final ManagedObjectReference _mor;

    // Cached properties
    private ManagedEntity _me = null;
    private ManagedObject _mo = null;
    private String _name = null;

    public BaseMO(VmwareContext context, ManagedObjectReference mor) {
        assert(context != null);

        _context = context;
        _mor = mor;
    }

    public BaseMO(VmwareContext context, ManagedObjectReference mor, String name) {
        this(context, mor);
        _name = name;
    }

    public BaseMO(VmwareContext context, String morType, String morRefId) {
        assert(context != null);
        assert(morType != null);
        assert(morRefId != null);

        _context = context;
        _mor = new ManagedObjectReference();
        _mor.setType(morType);
        _mor.set_value(morRefId);
    }

    public BaseMO(VmwareContext context, String morType, String morRefId, String name) {
        this(context, morType, morRefId);
        _name = name;
    }

    public VmwareContext getContext() {
        return _context;
    }

    public ManagedObjectReference getMor() {
        return _mor;
    }

    public ManagedEntity getManagedEntity() {
        if (_me == null) {
            _me = MorUtil.createExactManagedEntity(_context.getServiceInstance().getServerConnection(), _mor);
        }

        return _me;
    }

    public ManagedObject getManagedObject() {
        if (_mo == null) {
            _mo = MorUtil.createExactManagedObject(_context.getServiceInstance().getServerConnection(), _mor);
        }

        return _mo;
    }

    public String getName() {
        if (_name == null) {
            ManagedEntity me = getManagedEntity();
            assert(me != null);

            _name = me.getName();
        }

        return _name;
    }

    protected String getNameNoFetching() {
        return _name;
    }
}
