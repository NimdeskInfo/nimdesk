/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.hypervisor.vmware.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import org.apache.commons.lang.StringUtils;

/**
 * VmxDescriptorFile handles .vmx, .vmxf copy
 */
public class VmxDescriptorFile {
    public interface VmxContentHandler {
        String processLine(String line);
        void postProcess(BufferedWriter writer);
    }
    
    public interface VmxContentMergeHandler extends VmxContentHandler {
    	String processMergeLine(String line);
    }

    public static void copy(String srcVmxFile, String destVmxFile, VmxContentHandler handler)
            throws IOException {
        copy(new File(srcVmxFile), new File(destVmxFile), handler);
    }

    public static void copy(File srcVmxFile, File destVmxFile, VmxContentHandler handler)
            throws IOException {
        InputStream ins = null;
        BufferedReader reader = null;
        BufferedWriter writer = null;

        try {
            ins = new FileInputStream(srcVmxFile);
            reader = new BufferedReader(new InputStreamReader(ins));
            writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(destVmxFile)));

            String line;
            while ((line = reader.readLine()) != null) {
                line = line.trim();

                if (!StringUtils.isEmpty(line) && (line.charAt(0) != '#')) {
                    if (handler != null) {
                        line = handler.processLine(line);
                    }
                }

                if (line != null) {
                    writer.write(line);
                    writer.newLine();
                }
            }

            if (handler != null) {
                handler.postProcess(writer);
            }
        } finally {
            if (ins != null) {
                try {
                    ins.close();
                } catch (IOException e) {
                    // Ignore
                }
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    // Ignore
                }
            }
            if (writer != null) {
                writer.close();
            }
        }
    }
    
    // Have selected lines from the srcVmxFile and mrgVmxFile written to destVmxFile.
    public static void merge(File srcVmxFile, File mrgVmxFile, File destVmxFile, VmxContentMergeHandler handler)
            throws IOException {
        copy(srcVmxFile, destVmxFile, handler);
        
        InputStream ins = null;
        BufferedReader reader = null;
        BufferedWriter writer = null;

        try {
            ins = new FileInputStream(mrgVmxFile);
            reader = new BufferedReader(new InputStreamReader(ins));
            writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(destVmxFile, true)));

            String line;
            while ((line = reader.readLine()) != null) {
                line = line.trim();

                if (!StringUtils.isEmpty(line) && (line.charAt(0) != '#')) {
                    if (handler != null) {
                        line = handler.processMergeLine(line);
                    }
                }

                if (line != null) {
                    writer.write(line);
                    writer.newLine();
                }
            }

            if (handler != null) {
                handler.postProcess(writer);
            }
        } finally {
            if (ins != null) {
                try {
                    ins.close();
                } catch (IOException e) {
                    // Ignore
                }
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    // Ignore
                }
            }
            if (writer != null) {
                writer.close();
            }
        }
    }
}
