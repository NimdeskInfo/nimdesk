/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.hypervisor.vmware.mo;

import java.rmi.RemoteException;

import org.apache.log4j.Logger;

import com.nimdesk.hypervisor.vmware.VmwareContext;
import com.nimdesk.hypervisor.vmware.util.DatastoreFile;
import com.nimdesk.util.ActionDelegate;
import com.vmware.vim25.CannotCreateFile;
import com.vmware.vim25.FileAlreadyExists;
import com.vmware.vim25.FileFault;
import com.vmware.vim25.FileInfo;
import com.vmware.vim25.FileNotFound;
import com.vmware.vim25.FileQueryFlags;
import com.vmware.vim25.HostDatastoreBrowserSearchResults;
import com.vmware.vim25.HostDatastoreBrowserSearchSpec;
import com.vmware.vim25.InvalidDatastore;
import com.vmware.vim25.InvalidProperty;
import com.vmware.vim25.ManagedObjectReference;
import com.vmware.vim25.ObjectContent;
import com.vmware.vim25.ObjectSpec;
import com.vmware.vim25.PropertyFilterSpec;
import com.vmware.vim25.PropertySpec;
import com.vmware.vim25.RuntimeFault;
import com.vmware.vim25.SelectionSpec;
import com.vmware.vim25.TraversalSpec;
import com.vmware.vim25.mo.Datastore;
import com.vmware.vim25.mo.FileManager;
import com.vmware.vim25.mo.HostDatastoreBrowser;
import com.vmware.vim25.mo.PropertyCollector;
import com.vmware.vim25.mo.Task;
import com.vmware.vim25.mo.util.PropertyCollectorUtil;

/**
 *
 */
public class DatastoreMO extends BaseMO {
    private static final Logger LOGGER = Logger.getLogger(DatastoreMO.class);

    public static final String MORTYPE_DATASTORE = "Datastore";

    private static final int MAX_RETRY = 5;
    private static final int RETRY_INTERVAL = 3000; // 3 seconds

    // Cached objects
    private String _url = null;
    private DatacenterMO _datacenter = null;

    public DatastoreMO(VmwareContext context, ManagedObjectReference mor) {
        super(context, mor);
    }

    public DatastoreMO(VmwareContext context, ManagedObjectReference mor, String name) {
        super(context, mor, name);
    }

    public DatastoreMO(VmwareContext context, String dsRefId) {
        super(context, MORTYPE_DATASTORE, dsRefId);
    }

    public DatastoreMO(VmwareContext context, String dsRefId, String name) {
        super(context, MORTYPE_DATASTORE, dsRefId, name);
    }

    public Datastore getDatastore() {
        return (Datastore) getManagedEntity();
    }

    public String getUrl() {
        if (_url == null) {
            _url = (String) getDatastore().getPropertyByPath("summary.url");
        }

        return _url;
    }

    public DatacenterMO getDatacenter() throws InvalidProperty, RuntimeFault, RemoteException {
        if (_datacenter != null) {
            return _datacenter;
        }

        PropertySpec propSpec = PropertyCollectorUtil.createPropertySpec(DatacenterMO.MORTYPE_DATACENTER, false, new String[] {"name"});

        TraversalSpec visitParents = PropertyCollectorUtil.createTraversalSpec( "visitParents",
                "Folder",  "parent",
                new String[] {"visitParents"});

        TraversalSpec dsToP = PropertyCollectorUtil.createTraversalSpec( "dsToP",
                "Datastore",  "parent",
                new String[] {"visitParents"});

        ObjectSpec objSpec = new ObjectSpec();
        objSpec.setObj(getMor());
        objSpec.setSkip(Boolean.TRUE);
        objSpec.setSelectSet(new SelectionSpec[] { visitParents, dsToP });

        PropertyFilterSpec pfSpec = new PropertyFilterSpec();
        pfSpec.setPropSet(new PropertySpec[] { propSpec });
        pfSpec.setObjectSet(new ObjectSpec[] { objSpec });

        PropertyCollector propCollector = getContext().getServiceInstance().getPropertyCollector();

        @SuppressWarnings("deprecation")
        ObjectContent[] ocs = propCollector.retrieveProperties(new PropertyFilterSpec[] { pfSpec });
        assert(ocs != null);
        assert(ocs.length > 0);
        assert(ocs[0].getObj() != null);
        assert(ocs[0].getPropSet() != null);

        String dcName = ocs[0].getPropSet()[0].getVal().toString();
        _datacenter = new DatacenterMO(getContext(), ocs[0].getObj(), dcName);

        return _datacenter;
    }

    public void makeDirectory(String dir, DatacenterMO datacenter)
            throws FileFault, InvalidDatastore, RuntimeFault, RemoteException {
        LOGGER.info(String.format("Datastore %s (%s) make dir \"%s\"", getMor().get_value(), getNameNoFetching(), dir));

        String fullDsPath = dir;
        if (!DatastoreFile.isFullDatastorePath(fullDsPath)) {
            fullDsPath = DatastoreFile.composeFullDatastorePath(getName(), fullDsPath);
        }

        FileManager fm = getContext().getServiceInstance().getFileManager();

        int retried = 0;

        while (true) {
            try {
                fm.makeDirectory(fullDsPath, datacenter.getDatacenter(), true);
                break;
            } catch (FileAlreadyExists fault) {
                break;
            } catch (CannotCreateFile fault) {
                if (retried < 3) {
                    ++retried;
                    LOGGER.warn(String.format("Datastore %s (%s) failed to makeDirectory \"%s\", retry %d...",
                            getMor().get_value(), getNameNoFetching(), dir, retried));
                    try {
                        Thread.sleep(3000); // Sleep 3 seconds and retry
                    } catch (InterruptedException e) {
                        // Ignore
                    }
                    continue;
                } else {
                    throw fault;
                }
            }
        }
    }

    public boolean copyDatastoreFile(String srcFilePath, DatacenterMO srcDatacenter,
            DatastoreMO destDatastore, String destFilePath,
            DatacenterMO destDatacenter, boolean forceOverwrite,
            ActionDelegate<Integer> progressUpdater)
            throws FileFault, InvalidDatastore, RuntimeFault, RemoteException {
        return copyOrMoveDatastoreFile(true, srcFilePath, srcDatacenter, destDatastore,
                destFilePath, destDatacenter, forceOverwrite, progressUpdater);
    }

    public boolean moveDatastoreFile(String srcFilePath, DatacenterMO srcDatacenter,
            DatastoreMO destDatastore, String destFilePath,
            DatacenterMO destDatacenter, boolean forceOverwrite,
            ActionDelegate<Integer> progressUpdater)
            throws FileFault, InvalidDatastore, RuntimeFault, RemoteException {
        return copyOrMoveDatastoreFile(false, srcFilePath, srcDatacenter, destDatastore,
                destFilePath, destDatacenter, forceOverwrite, progressUpdater);
    }

    public boolean renameDatastoreFile(String oldFullPath, String newFullPath,
            DatacenterMO datacenter, ActionDelegate<Integer> progressUpdater)
            throws FileFault, InvalidDatastore, RuntimeFault, RemoteException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(String.format("Rename '%s' to '%s'", oldFullPath, newFullPath));
        }

        FileManager fm = getContext().getServiceInstance().getFileManager();
        Task task = fm.moveDatastoreFile_Task(oldFullPath, datacenter.getDatacenter(),
                oldFullPath, datacenter.getDatacenter(), true);

        String error;
        try {
            error = getContext().waitForTask(task, progressUpdater);
        } catch (Exception e) {
            LOGGER.error(String.format("Rename '%s' to '%s'", oldFullPath, newFullPath), e);
            return false;
        }

        if (error == null) {
            LOGGER.info(String.format("Rename '%s' to '%s' succeeded", oldFullPath, newFullPath));
            return true;
        } else {
            LOGGER.error(String.format("Rename '%s' to '%s' failed: ", oldFullPath, newFullPath, error));
            return false;
        }
    }

    private boolean copyOrMoveDatastoreFile(boolean copy, String srcFilePath, DatacenterMO srcDatacenter,
            DatastoreMO destDatastore, String destFilePath,
            DatacenterMO destDatacenter, boolean forceOverwrite,
            ActionDelegate<Integer> progressUpdater)
            throws FileFault, InvalidDatastore, RuntimeFault, RemoteException {
        if (destDatastore == null) {
            destDatastore = this;
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(String.format("Datastore %s (%s) %s file: src:\"%s\", dest:\"%s\", dest datastore: %s (%s)",
                    getMor().get_value(), getNameNoFetching(), copy ? "copying" : "moving", srcFilePath, destFilePath,
                    destDatastore.getMor().get_value(), destDatastore.getNameNoFetching()));
        }

        String srcDsName = getName();
        String destDsName = destDatastore.getName();

        String srcFullPath = srcFilePath;
        if (!DatastoreFile.isFullDatastorePath(srcFullPath)) {
            srcFullPath = DatastoreFile.composeFullDatastorePath(srcDsName, srcFilePath);
        }

        String destFullPath = destFilePath;
        if (!DatastoreFile.isFullDatastorePath(destFullPath)) {
            destFullPath = DatastoreFile.composeFullDatastorePath(destDsName, destFilePath);
        }

        FileManager fm = getContext().getServiceInstance().getFileManager();

        String action = copy ? "copyDatastoreFile" : "moveDatastoreFile";
        String error;

        int retry = 0;
        while (true) {
            try {
                Task task;
                if (copy) {
                    task = fm.copyDatastoreFile_Task(srcFullPath, srcDatacenter.getDatacenter(),
                        destFullPath, destDatacenter.getDatacenter(), forceOverwrite);
                } else {
                    task = fm.moveDatastoreFile_Task(srcFullPath, srcDatacenter.getDatacenter(),
                            destFullPath, destDatacenter.getDatacenter(), forceOverwrite);
                }
                error = getContext().waitForTask(task, progressUpdater);
                break;
            } catch (Exception e) {
                if (e instanceof FileFault) {
                    if (++retry <= MAX_RETRY) {
                        LOGGER.warn(String.format("%s. Retry copyOrMoveDatastoreFile (%d)...",
                                e.getClass().getName(), retry));
                        try {
                            Thread.sleep(RETRY_INTERVAL);
                        } catch (InterruptedException e1) {
                            // Ignore
                        }
                        continue;
                    }
                }

                LOGGER.error(String.format("Datastore %s (%s) %s error: src:%s, dest:%s",
                        getMor().get_value(), getNameNoFetching(), action, srcFullPath, destFullPath), e);
                return false;
            }
        }

        if (error == null) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug(String.format("Datastore %s (%s) %s succeeded: src:%s, dest:%s",
                        getMor().get_value(), getNameNoFetching(), action, srcFullPath, destFullPath));
            }
            return true;
        } else {
            LOGGER.error(String.format("Datastore %s (%s) %s failed: src:%s, dest:%s, error:%s",
                    getMor().get_value(), getNameNoFetching(), action, srcFullPath, destFullPath, error));
            return false;
        }
    }

    public boolean deleteDatastoreFile(String filePath, DatacenterMO datacenter,
            ActionDelegate<Integer> progressUpdater)
            throws FileFault, InvalidDatastore, RuntimeFault, RemoteException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(String.format("Datastore %s (%s) delete file: \"%s\"",
                    getMor().get_value(), getNameNoFetching(), filePath));
        }

        String dsName = getName();

        String fullPath = filePath;
        if (!DatastoreFile.isFullDatastorePath(fullPath)) {
            fullPath = DatastoreFile.composeFullDatastorePath(dsName, filePath);
        }

        FileManager fm = getContext().getServiceInstance().getFileManager();

        String error;
        try {
            Task task = fm.deleteDatastoreFile_Task(fullPath, datacenter.getDatacenter());
            error = getContext().waitForTask(task, progressUpdater);
        } catch (FileNotFound e) {
            LOGGER.info(String.format("Datastore %s (%s) file \"%s\" is not found",
                    getMor().get_value(), getNameNoFetching(), fullPath));
            return true;
        } catch (Exception e) {
            LOGGER.error(String.format("Datastore %s (%s) delete error: %s",
                    getMor().get_value(), getNameNoFetching(), fullPath), e);
            return false;
        }

        if (error == null) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug(String.format("Datastore %s (%s) delete succeeded: %s",
                        getMor().get_value(), getNameNoFetching(), fullPath));
            }
            return true;
        } else {
            LOGGER.error(String.format("Datastore %s (%s) delete failed: %s, error:%s",
                    getMor().get_value(), getNameNoFetching(), fullPath, error));
            return false;
        }
    }

    public boolean fileExists(String filePath, ActionDelegate<Integer> progressUpdater)
            throws FileFault, InvalidDatastore, RuntimeFault, RemoteException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(String.format("Datastore %s (%s) check file exists: \"%s\"",
                    getMor().get_value(), getNameNoFetching(), filePath));
        }

        HostDatastoreBrowserSearchResults results = searchDatastore(filePath, true, progressUpdater);
        if (results != null) {
            FileInfo[] info = results.getFile();
            if (info != null && info.length > 0) {
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug(String.format("Datastore %s (%s) file \"%s\" exists",
                            getMor().get_value(), getNameNoFetching(), filePath));
                }
                return true;
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(String.format("Datastore %s (%s) file \"%s\" doesn't exist",
                    getMor().get_value(), getNameNoFetching(), filePath));
        }

        return false;
    }

    public long getFileSize(String filePath, boolean caseInsensitive,
            ActionDelegate<Integer> progressUpdater)
            throws FileFault, InvalidDatastore, RuntimeFault, RemoteException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(String.format("Datastore %s (%s) check file exists: \"%s\"",
                    getMor().get_value(), getNameNoFetching(), filePath));
        }

        HostDatastoreBrowserSearchResults results = searchDatastore(filePath, caseInsensitive, progressUpdater);
        if (results != null) {
            FileInfo[] info = results.getFile();
            if (info != null && info.length > 0) {
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug(String.format("Datastore %s (%s) file \"%s\" exists",
                            getMor().get_value(), getNameNoFetching(), filePath));
                }
                Long fileSize = info[0].getFileSize();
                return (fileSize == null) ? -1 : fileSize.longValue();
            }
        }

        LOGGER.error(String.format("Datastore %s (%s) file \"%s\" doesn't exist",
                getMor().get_value(), getNameNoFetching(), filePath));
        return -1;
    }

    private HostDatastoreBrowserSearchResults searchDatastore(String filePath, boolean caseInsensitive,
            ActionDelegate<Integer> progressUpdater)
            throws FileFault, InvalidDatastore, RuntimeFault, RemoteException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(String.format("Datastore %s (%s) search file: \"%s\"",
                    getMor().get_value(), getNameNoFetching(), filePath));
        }

        String dsName = getName();

        String fullPath = filePath;
        if (!DatastoreFile.isFullDatastorePath(fullPath)) {
            fullPath = DatastoreFile.composeFullDatastorePath(dsName, filePath);
        }

        DatastoreFile dsFile = new DatastoreFile(fullPath);
        DatastoreFile dsDirFile = new DatastoreFile(dsFile.getDatastoreName(), dsFile.getDir());

        FileQueryFlags flags = new FileQueryFlags();
        flags.setFileOwner(true);
        flags.setFileSize(true);

        HostDatastoreBrowserSearchSpec searchSpec = new HostDatastoreBrowserSearchSpec();
        searchSpec.setSearchCaseInsensitive(caseInsensitive);
        searchSpec.setMatchPattern(new String[] { dsFile.getFileName() });
        searchSpec.setDetails(flags);

        HostDatastoreBrowser browser = getDatastore().getBrowser();
        Task task = browser.searchDatastore_Task(dsDirFile.getFullPath(), searchSpec);

        String error;
        try {
            error = getContext().waitForTask(task, progressUpdater);
        } catch (Exception e) {
            if (e instanceof FileNotFound) {
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug(String.format("Datastore %s (%s) search file not found: %s",
                            getMor().get_value(), getNameNoFetching(), fullPath));
                }
                return null;
            }

            LOGGER.error(String.format("Datastore %s (%s) search file error: %s",
                    getMor().get_value(), getNameNoFetching(), fullPath), e);
            return null;
        }

        if (error != null) {
            LOGGER.error(String.format("Datastore %s (%s) search file failed: %s, error:%s",
                    getMor().get_value(), getNameNoFetching(), fullPath, error));
            return null;
        }

        HostDatastoreBrowserSearchResults results =
                (HostDatastoreBrowserSearchResults) task.getPropertyByPath("info.result");
        return results;
    }

    public boolean umount() throws RemoteException {
        Datastore ds = getDatastore();
        if (!"NFS".equalsIgnoreCase(ds.getSummary().getType())) {
            return false;
        }

        ds.destroyDatastore();
        return true;
    }
}
