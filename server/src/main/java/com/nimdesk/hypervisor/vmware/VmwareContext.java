/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.hypervisor.vmware;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.rmi.RemoteException;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.nimdesk.hypervisor.HypervisorContext;
import com.nimdesk.hypervisor.vmware.mo.CbrcManagerMO;
import com.nimdesk.hypervisor.vmware.mo.DatacenterMO;
import com.nimdesk.hypervisor.vmware.mo.ResourcePoolMO;
import com.nimdesk.hypervisor.vmware.mo.VirtualDiskManagerMO;
import com.nimdesk.hypervisor.vmware.mo.VirtualMachineMO;
import com.nimdesk.hypervisor.vmware.util.DatastoreFile;
import com.nimdesk.util.ActionDelegate;
import com.nimdesk.util.Pair;
import com.vmware.vim25.CbrcDeviceSpec;
import com.vmware.vim25.DynamicProperty;
import com.vmware.vim25.InvalidProperty;
import com.vmware.vim25.LocalizedMethodFault;
import com.vmware.vim25.ManagedObjectReference;
import com.vmware.vim25.MethodFault;
import com.vmware.vim25.ObjectContent;
import com.vmware.vim25.ObjectSpec;
import com.vmware.vim25.PropertyFilterSpec;
import com.vmware.vim25.PropertySpec;
import com.vmware.vim25.RuntimeFault;
import com.vmware.vim25.SelectionSpec;
import com.vmware.vim25.TaskInfo;
import com.vmware.vim25.TaskInfoState;
import com.vmware.vim25.TraversalSpec;
import com.vmware.vim25.mo.CbrcManager;
import com.vmware.vim25.mo.Folder;
import com.vmware.vim25.mo.ManagedEntity;
import com.vmware.vim25.mo.PropertyCollector;
import com.vmware.vim25.mo.ServerConnection;
import com.vmware.vim25.mo.ServiceInstance;
import com.vmware.vim25.mo.ServiceInstanceWithInternalService;
import com.vmware.vim25.mo.Task;
import com.vmware.vim25.mo.util.MorUtil;
import com.vmware.vim25.mo.util.PropertyCollectorUtil;

/**
 * Connection context to ESX or vCenter
 */
public class VmwareContext implements HypervisorContext {
    private static final Logger LOGGER = Logger.getLogger(VmwareContext.class);

    private static final String NIMDESK_RESOURCE_POOL = "nimdesk";

    private final String _serviceUrl;
    private final String _userName;
    private final String _password;

    private ServiceInstance _serviceInstance = null;
    private Folder _rootFolder = null;

    private String _serverAddress;
    private int _serverPort;

    // Cached objects
    private DatacenterMO _rootDatacenter = null;
    private ResourcePoolMO _rootResourcePool = null;
    //private ResourcePoolMO _nimdeskResourcePool = null;
    private VirtualDiskManagerMO _vdm = null;
    private CbrcManagerMO _cbrcManager = null;

    public static VmwareContext connect(String serverAddress, int port, String userName, String password)
            throws Exception {
        StringBuilder serviceUrlSb = new StringBuilder();

        serviceUrlSb.append("https://").append(serverAddress);
        if (port > 0 && port != 443) {
            serviceUrlSb.append(":").append(port);
        }
        serviceUrlSb.append("/sdk");

        return connect(serviceUrlSb.toString(), userName, password);
    }

    public static VmwareContext connect(String serviceUrl, String userName, String password)
            throws Exception {
        ServiceInstance serviceInstance = doConnect(serviceUrl, userName, password);
        return new VmwareContext(serviceInstance, serviceUrl, userName, password);
    }

    private static ServiceInstance doConnect(String serviceUrl, String userName, String password)
            throws Exception {
        ServiceInstanceWithInternalService serviceInstance;

        try {
            serviceInstance = new ServiceInstanceWithInternalService(new URL(serviceUrl), userName, password, true);
        } catch (MalformedURLException e) {
            LOGGER.error(String.format("Invalid Vmware url \"%s\"", serviceUrl));
            throw e;
        } catch (RemoteException e) {
            LOGGER.error(String.format("Unable to connect to Vmware url \"%s\"", serviceUrl), e);
            throw e;
        }

        return serviceInstance;
    }

    private VmwareContext(ServiceInstance serviceInstance, String serviceUrl, String userName, String password)
            throws Exception {
        _serviceUrl = serviceUrl;
        _userName = userName;
        _password = password;

        init(serviceInstance);
    }

    private void init(ServiceInstance serviceInstance) throws Exception {
        _serviceInstance = serviceInstance;
        if (_serviceInstance != null) {
            _rootFolder = _serviceInstance.getRootFolder();
            _serverAddress = _serviceInstance.getServerConnection().getUrl().getHost();
            _serverPort = _serviceInstance.getServerConnection().getUrl().getPort();
        } else {
            LOGGER.error("VmwareContext: Invalid parameter");
            throw new Exception("Invalid parameter");
        }
    }

    @Override
    public String getServerAddress() {
        return _serverAddress;
    }

    @Override
    public int getServerPort() {
        return _serverPort;
    }

    @Override
    public void disconnect() {
        _rootFolder = null;

        if (_serviceInstance != null) {
            ServerConnection conn = _serviceInstance.getServerConnection();
            if (conn != null) {
                try {
                    conn.logout();
                } catch (Throwable t) {
                    LOGGER.warn(String.format("Failed to log out VMware host %s", _serverAddress));
                }
            }
            _serviceInstance = null;
        }
    }

    @Override
    public void reconnect() throws Exception {
        disconnect();

        ServiceInstance serviceInstance;
        try {
            serviceInstance = doConnect(_serviceUrl, _userName, _password);
            init(serviceInstance);
        } catch (Exception e) {
            LOGGER.warn(String.format("Failed to reconnect to VMware host %s", _serviceUrl));
            throw e;
        }
    }

    @Override
    public String getRefId(Object obj) {
        if (!(obj instanceof ManagedObjectReference)) {
            return null;
        }

        return ((ManagedObjectReference) obj).getVal();
    }

    public ServiceInstance getServiceInstance() {
        return _serviceInstance;
    }

    public Folder getRootFolder() {
        return _rootFolder;
    }

    public VirtualDiskManagerMO getVirtualDiskManager() {
        if (_vdm == null) {
            _vdm = new VirtualDiskManagerMO(this, _serviceInstance.getVirtualDiskManager().getMOR());
        }

        return _vdm;
    }

    public CbrcManagerMO getCbrcManager() {
        if (_cbrcManager == null) {
            if (_serviceInstance instanceof ServiceInstanceWithInternalService) {
                CbrcManager cbrcManager = ((ServiceInstanceWithInternalService) _serviceInstance).getCbrcManager();
                _cbrcManager = new CbrcManagerMO(this, cbrcManager.getMOR());
            }
        }

        return _cbrcManager;
    }

    public ManagedObjectReference getMor(String morType, String morRefId) {
        ManagedObjectReference mor = new ManagedObjectReference();
        mor.setType(morType);
        mor.set_value(morRefId);
        return mor;
    }

    public ManagedEntity getManagedEntity(ManagedObjectReference mor) {
        return MorUtil.createExactManagedEntity(_serviceInstance.getServerConnection(), mor);
    }

    public DatacenterMO getRootDatacenter()
            throws InvalidProperty, RuntimeFault, RemoteException {
        if (_rootDatacenter != null) {
            return _rootDatacenter;
        }

        PropertySpec propSpec = PropertyCollectorUtil.createPropertySpec(DatacenterMO.MORTYPE_DATACENTER,
                false, new String[] {"name"});

        // Traversal through childEntity
        TraversalSpec visitFolders = PropertyCollectorUtil.createTraversalSpec("visitFolders",
                "Folder",  "childEntity",
                new String[] {});

        ObjectSpec objSpec = new ObjectSpec();
        objSpec.setObj(_rootFolder.getMOR());
        objSpec.setSkip(Boolean.TRUE);
        objSpec.setSelectSet(new SelectionSpec[] { visitFolders });

        PropertyFilterSpec pfSpec = new PropertyFilterSpec();
        pfSpec.setPropSet(new PropertySpec[] { propSpec });
        pfSpec.setObjectSet(new ObjectSpec[] { objSpec });

        PropertyCollector propCollector = getServiceInstance().getPropertyCollector();

        @SuppressWarnings("deprecation")
        ObjectContent[] ocs = propCollector.retrieveProperties(new PropertyFilterSpec[] { pfSpec });
        assert(ocs != null);
        assert(ocs.length > 0);
        assert(ocs[0].getObj() != null);
        assert(ocs[0].getPropSet() != null);

        if (ocs == null || ocs.length == 0) {
            return null;
        }

        String dcName = null;
        DynamicProperty[] propSet;
        if ((propSet = ocs[0].getPropSet()) != null && propSet.length > 0
                && propSet[0].getVal() != null) {
            dcName = propSet[0].getVal().toString();
        }

        _rootDatacenter = new DatacenterMO(this, ocs[0].getObj(), dcName);

        return _rootDatacenter;
    }

    public DatacenterMO getOwnerDatacenter(ManagedObjectReference morEntity)
            throws InvalidProperty, RuntimeFault, RemoteException {
        PropertySpec propSpec = PropertyCollectorUtil.createPropertySpec(DatacenterMO.MORTYPE_DATACENTER,
                false, new String[] {"name"});

        // Traversal through parent
        TraversalSpec parentTraversal = PropertyCollectorUtil.createTraversalSpec("parentTraversal",
                "ManagedEntity",  "parent",
                new String[] {"parentTraversal"});

        ObjectSpec objSpec = new ObjectSpec();
        objSpec.setObj(morEntity);
        objSpec.setSkip(Boolean.TRUE);
        objSpec.setSelectSet(new SelectionSpec[] { parentTraversal });

        PropertyFilterSpec pfSpec = new PropertyFilterSpec();
        pfSpec.setPropSet(new PropertySpec[] { propSpec });
        pfSpec.setObjectSet(new ObjectSpec[] { objSpec });

        PropertyCollector propCollector = getServiceInstance().getPropertyCollector();

        @SuppressWarnings("deprecation")
        ObjectContent[] ocs = propCollector.retrieveProperties(new PropertyFilterSpec[] { pfSpec });
        assert(ocs != null);
        assert(ocs.length > 0);
        assert(ocs[0].getObj() != null);
        assert(ocs[0].getPropSet() != null);

        String dcName = ocs[0].getPropSet()[0].getVal().toString();
        return new DatacenterMO(this, ocs[0].getObj(), dcName);
    }

    public ResourcePoolMO getRootResourcePool()
            throws InvalidProperty, RuntimeFault, RemoteException {
        if (_rootResourcePool != null) {
            return _rootResourcePool;
        }

        ObjectContent[] ocs = searchResourcePool(null);
        if (ocs == null || ocs.length == 0) {
            return null;
        }

        String rpName = ocs[0].getPropSet()[0].getVal().toString();
        _rootResourcePool = new ResourcePoolMO(this, ocs[0].getObj(), rpName);

        return _rootResourcePool;
    }

    public ResourcePoolMO getNimdeskResourcePool(ResourcePoolMO ownerResourcePool)
            throws InvalidProperty, RuntimeFault, RemoteException {
        ObjectContent[] ocs = searchResourcePool((ownerResourcePool == null) ? null : ownerResourcePool.getMor());
        if (ocs == null || ocs.length == 0) {
            return null;
        }

        String rpName = ocs[0].getPropSet()[0].getVal().toString();

        for (ObjectContent oc : ocs) {
            rpName = oc.getPropSet()[0].getVal().toString();
            if (NIMDESK_RESOURCE_POOL.equals(rpName)) {
                return new ResourcePoolMO(this, oc.getObj(), rpName);
            }
        }

        return null;
    }

    private ObjectContent[] searchResourcePool(ManagedObjectReference morEntity)
            throws InvalidProperty, RuntimeFault, RemoteException {
        PropertySpec propSpec = PropertyCollectorUtil.createPropertySpec(ResourcePoolMO.MORTYPE_RESOURCEPOOL,
                false, new String[] {"name"});

        // Recurse through childEntity
        TraversalSpec visitFolders = PropertyCollectorUtil.createTraversalSpec("visitFolders",
                "Folder",  "childEntity",
                new String[] {"visitFolders", "dcToHf", "rpToRp", "crToRp"});

        // Traversal through all ResourcePools
        TraversalSpec rpToRp = PropertyCollectorUtil.createTraversalSpec("rpToRp",
                "ResourcePool", "resourcePool",
                new String[] {"rpToRp"});

        // Traversal through ResourcePool branch
        TraversalSpec crToRp = PropertyCollectorUtil.createTraversalSpec("crToRp",
                "ComputeResource", "resourcePool",
                new String[] {"rpToRp"});

        // Traversal through hostFolder branch
        TraversalSpec dcToHf = PropertyCollectorUtil.createTraversalSpec("dcToHf",
                "Datacenter", "hostFolder",
                new String[] {"visitFolders"});

        ObjectSpec objSpec = new ObjectSpec();
        if (morEntity != null) {
            objSpec.setObj(morEntity);
        } else {
            objSpec.setObj(_rootFolder.getMOR());
        }
        objSpec.setSkip(Boolean.TRUE);
        objSpec.setSelectSet(new SelectionSpec[] { visitFolders, rpToRp, crToRp, dcToHf });

        PropertyFilterSpec pfSpec = new PropertyFilterSpec();
        pfSpec.setPropSet(new PropertySpec[] { propSpec });
        pfSpec.setObjectSet(new ObjectSpec[] { objSpec });

        PropertyCollector propCollector = getServiceInstance().getPropertyCollector();

        @SuppressWarnings("deprecation")
        ObjectContent[] ocs = propCollector.retrieveProperties(new PropertyFilterSpec[] { pfSpec });
        /*
        assert(ocs != null);
        assert(ocs.length > 0);
        assert(ocs[0].getObj() != null);
        assert(ocs[0].getPropSet() != null);
        */

        return ocs;
    }

    public HttpURLConnection getHttpGetConnection(String urlStr, Map<String, String> properties) throws IOException {
        return getHttpConnection(urlStr, "GET", properties);
    }

    public HttpURLConnection getHttpPutConnection(String urlStr, Map<String, String> properties) throws IOException {
        return getHttpConnection(urlStr, "PUT", properties);
    }

    public HttpURLConnection getHttpPostConnection(String urlStr, Map<String, String> properties) throws IOException {
        return getHttpConnection(urlStr, "POST", properties);
    }

    public HttpURLConnection getHttpConnection(String urlStr, String httpMethod, Map<String, String> properties) throws IOException {
        String sessionStr = _serviceInstance.getServerConnection().getSessionStr();

        HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
            @Override
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        });

        URL url = new URL(urlStr);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        if ("PUT".equalsIgnoreCase(httpMethod) || "POST".equalsIgnoreCase(httpMethod)) {
            conn.setChunkedStreamingMode(1024 * 1024);
        }
        conn.setDoInput(true);
        conn.setDoOutput(true);
        conn.setAllowUserInteraction(true);
        conn.setRequestMethod(httpMethod);
        conn.setRequestProperty("Cookie", sessionStr);
        if (properties != null) {
            for (Map.Entry<String, String> entry : properties.entrySet()) {
                conn.setRequestProperty(entry.getKey(), entry.getValue());
            }
        }
        conn.connect();

        return conn;
    }

    public String getDatastoreBrowseUrl(String datacenterName, String fullPath) {
        DatastoreFile dsFile = new DatastoreFile(fullPath);
        return getDatastoreBrowseUrl(datacenterName, dsFile.getDatastoreName(), dsFile.getRelativePath());
    }

    @SuppressWarnings("deprecation")
    public String getDatastoreBrowseUrl(String datacenterName, String datastoreName, String relativePath) {
        assert(!StringUtils.isEmpty(datacenterName));
        assert(!StringUtils.isEmpty(datastoreName));
        assert(!StringUtils.isEmpty(relativePath));

        StringBuffer sb = new StringBuffer();
        sb.append("https://").append(_serverAddress);
        if (_serverPort > 0 && _serverPort != 443) {
            sb.append(":").append(_serverPort);
        }
        sb.append("/folder/").append(URLEncoder.encode(relativePath));
        sb.append("?dcPath=").append(URLEncoder.encode(datacenterName));
        sb.append("&dsName=").append(URLEncoder.encode(datastoreName));

        return sb.toString();
    }

    /**
     * Wait for task to complete.
     *
     * @param task
     *
     * @return null if succeeded. Otherwise error string.
     *
     * @throws InvalidProperty
     * @throws RuntimeFault
     * @throws RemoteException
     * @throws InterruptedException
     */
    public String waitForTask(Task task, ActionDelegate<Integer> progressUpdater)
            throws InvalidProperty, RuntimeFault, RemoteException, InterruptedException {
        // If there is another thread or client calling waitForUpdate(), the behavior of this
        // method is not predictable. This usually happens with VI Client plug-in which shares
        // the session with the VI Client which use waitForUpdate() extensively.
        // The safer way is to poll the related info.state and check its value.
        //task.waitForMe();

        TaskInfo taskInfo = null;
        TaskInfoState taskState = null;
        int tries = 0;
        int maxTries=3;
        Exception getInfoException = null;

        while ((taskState == null)
                || taskState.equals(TaskInfoState.running)
                || taskState.equals(TaskInfoState.queued)) {
            taskState = null;
            getInfoException = null;
            tries=0;

            // Under load getTaskInfo may return null when there really is valid task info, so we try 3 times to get it.
            while (taskState == null) {
                ++tries;
                if (tries > maxTries) {
                    if (getInfoException == null) {
                        throw new NullPointerException();
                    } else if (getInfoException instanceof RuntimeFault) {
                        throw (RuntimeFault) getInfoException;
                    } else if (getInfoException  instanceof RemoteException ) {
                        throw (RemoteException) getInfoException;
                    } else {
                        throw new RuntimeException(getInfoException);
                    }
                }

                try {
                    taskInfo = task.getTaskInfo();
                    taskState = taskInfo.getState();
                } catch (Exception e) {
                    //silently catch 3 exceptions
                    getInfoException = e;
                }
            }

            // Sleep for a specified time based on task state.
            if (taskState.equals(TaskInfoState.running)) {
                if (progressUpdater != null) {
                    Integer progress = taskInfo.getProgress();
                    if (progress != null && progress.intValue() <= 100) {
                        progressUpdater.action(progress);
                    }
                }

                Thread.sleep(1000);
            } else {
                Thread.sleep(500);
            }
        }

        String error = null;

        if (TaskInfoState.success != taskState) {
            Pair<String, MethodFault> failureInfo = getTaskFailureInfo(task);

            error = failureInfo.getFirst();
            if (!StringUtils.isEmpty(error)) {
                LOGGER.error(String.format("Task failed: %s", error));
            }

            MethodFault fault = failureInfo.getSecond();
            if (fault != null) {
                throw fault;
            }
        }

        return error;
    }

    private static Pair<String, MethodFault> getTaskFailureInfo(Task task) {
        StringBuilder sb = new StringBuilder();

        TaskInfo info = null;
        try {
            info = task.getTaskInfo();
        } catch (Exception e) {
            // Ignore error.
        }

        MethodFault methodFault = null;

        if (info != null) {
            LocalizedMethodFault fault = info.getError();
            if (fault != null) {
                methodFault = fault.getFault();

                sb.append(fault.getLocalizedMessage()).append(" ");

                if(fault.getFault() != null) {
                    sb.append(fault.getFault().getClass().getName());
                }
            }
        }

        return new Pair<String, MethodFault>(sb.length() == 0 ? "error" : sb.toString(), methodFault);
    }
}

