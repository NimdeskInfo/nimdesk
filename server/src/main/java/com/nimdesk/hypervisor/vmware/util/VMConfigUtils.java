/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.hypervisor.vmware.util;

import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.vmware.vim25.ManagedObjectReference;
import com.vmware.vim25.ToolsConfigInfo;
import com.vmware.vim25.VirtualController;
import com.vmware.vim25.VirtualDevice;
import com.vmware.vim25.VirtualDeviceBackingInfo;
import com.vmware.vim25.VirtualDeviceConfigSpec;
import com.vmware.vim25.VirtualDeviceConfigSpecFileOperation;
import com.vmware.vim25.VirtualDeviceConfigSpecOperation;
import com.vmware.vim25.VirtualDeviceDeviceBackingInfo;
import com.vmware.vim25.VirtualDeviceFileBackingInfo;
import com.vmware.vim25.VirtualDeviceRemoteDeviceBackingInfo;
import com.vmware.vim25.VirtualDisk;
import com.vmware.vim25.VirtualDiskFlatVer1BackingInfo;
import com.vmware.vim25.VirtualDiskFlatVer2BackingInfo;
import com.vmware.vim25.VirtualDiskSparseVer1BackingInfo;
import com.vmware.vim25.VirtualDiskSparseVer2BackingInfo;
import com.vmware.vim25.VirtualEthernetCard;
import com.vmware.vim25.VirtualEthernetCardNetworkBackingInfo;
import com.vmware.vim25.VirtualIDEController;
import com.vmware.vim25.VirtualKeyboard;
import com.vmware.vim25.VirtualMachineConfigInfo;
import com.vmware.vim25.VirtualMachineConfigSpec;
import com.vmware.vim25.VirtualMachineDefaultPowerOpInfo;
import com.vmware.vim25.VirtualMachineFileInfo;
import com.vmware.vim25.VirtualMachineFlagInfo;
import com.vmware.vim25.VirtualMachineVMCIDevice;
import com.vmware.vim25.VirtualPCIController;
import com.vmware.vim25.VirtualPS2Controller;
import com.vmware.vim25.VirtualPointingDevice;
import com.vmware.vim25.VirtualSCSIController;
import com.vmware.vim25.VirtualSCSISharing;
import com.vmware.vim25.VirtualSIOController;

/**
 * Utility class to help preserve VM configurations
 */
public class VMConfigUtils {
    private static final Logger LOGGER = Logger.getLogger(VMConfigUtils.class);

    private static final String NAME = "name";
    private static final String VERSION = "version";
    private static final String GUEST_ID = "guestId";

    private static final String HARDWARE_NUM_CPU = "hardware.numCPU";
    private static final String HARDWARE_MEMORY_MB = "hardware.memoryMB";

    private static final String FILES_VM_PATH_NAME = "files.vmPathName";
    private static final String FILES_SNAPSHOT_DIRECTORY = "files.snapshotDirectory";

    private static final String TOOLS_AFTER_POWER_ON = "tools.afterPowerOn";
    private static final String TOOLS_AFTER_RESUME = "tools.afterResume";
    private static final String TOOLS_BEFORE_GUEST_STANDBY = "tools.beforeGuestStandby";
    private static final String TOOLS_BEFORE_GUEST_SHUTDOWN = "tools.beforeGuestShutdown";
    private static final String TOOLS_BEFORE_GUEST_REBOOT = "tools.beforeGuestReboot";

    private static final String FLAGS_SNAPSHOT_POWER_OFF_BEHAVIOR = "flags.snapshotPowerOffBehavior";

    private static final String DEFAULTPOWEROPS_POWER_OFF_TYPE = "defaultPowerOps.powerOffType";
    private static final String DEFAULTPOWEROPS_SUSPEND_TYPE = "defaultPowerOps.suspendType";
    private static final String DEFAULTPOWEROPS_RESET_TYPE = "defaultPowerOps.resetType";
    private static final String DEFAULTPOWEROPS_STANDBY_ACTION = "defaultPowerOps.standbyAction";

    private static final String DEVICE_NUM = "device.num";

    private static final String DEVICE_CLASS = "device.%d.class";
    private static final String DEVICE_KEY = "device.%d.key";
    private static final String DEVICE_CONTROLLER_KEY = "device.%d.controllerKey";
    private static final String DEVICE_UNIT_NUMBER = "device.%d.unitNumber";
    private static final String DEVICE_CONTROLLER_BUS_NUMBER = "device.%d.controller.busNumber";
    private static final String DEVICE_SCSI_CONTROLLER_SHARED_BUS = "device.%d.scsiController.sharedBus";
    private static final String DEVICE_DISK_CAPACITY_IN_KB = "device.%d.disk.capacityInKB";
    private static final String DEVICE_NIC_ADDRESS_TYPE = "device.%d.nic.addressType";

    private static final String DEVICE_BACKING_CLASS = "device.%d.backing.class";
    private static final String DEVICE_REMOTE_BACKING_DEVICE_NAME = "device.%d.remoteBacking.deviceName";
    private static final String DEVICE_REMOTE_BACKING_USE_AUTO_DETECT = "device.%d.remoteBacking.useAutoDetect";
    private static final String DEVICE_FILE_BACKING_FILE_NAME = "device.%d.fileBacking.fileName";
    private static final String DEVICE_FILE_BACKING_DATASTORE = "device.%d.fileBacking.datastore";
    private static final String DEVICE_BACKING_DISK_MODE = "device.%d.backing.diskMode";
    private static final String DEVICE_BACKING_DISK_SPLIT = "device.%d.backing.diskSplit";
    private static final String DEVICE_BACKING_DISK_WRITE_THROUGH = "device.%d.backing.diskWriteThrough";
    private static final String DEVICE_BACKING_NIC_NETWORK = "device.%d.backing.nicNetwork";

    private static final int DEVICE_KEY_PCI_CONTROLLER = 100;
    private static final int DEVICE_KEY_PRIMARY_IDE_CONTROLLER = 200;
    private static final int DEVICE_KEY_SECONDARY_IDE_CONTROLLER = 201;
    private static final int DEVICE_KEY_PS2_CONTROLLER = 300;
    private static final int DEVICE_KEY_SIO_CONTROLLER = 400;

    public static Properties convert(VirtualMachineConfigInfo configInfo) {
        Properties properties = new Properties();

        // Save VM values
        setStringProperty(properties, NAME, configInfo.getName());
        setStringProperty(properties, VERSION, configInfo.getVersion());
        setStringProperty(properties, GUEST_ID, configInfo.getGuestId());
        setIntProperty(properties, HARDWARE_NUM_CPU, configInfo.getHardware().getNumCPU());
        setLongProperty(properties, HARDWARE_MEMORY_MB, configInfo.getHardware().getMemoryMB());

        // Save file storage info
        VirtualMachineFileInfo vmfileInfo = configInfo.getFiles();
        setStringProperty(properties, FILES_VM_PATH_NAME, vmfileInfo.getVmPathName());
        setStringProperty(properties, FILES_SNAPSHOT_DIRECTORY, vmfileInfo.getSnapshotDirectory());

        // Save tools config info
        ToolsConfigInfo tools = configInfo.getTools();
        setBoolProperty(properties, TOOLS_AFTER_POWER_ON, tools.getAfterPowerOn());
        setBoolProperty(properties, TOOLS_AFTER_RESUME, tools.getAfterResume());
        setBoolProperty(properties, TOOLS_BEFORE_GUEST_STANDBY, tools.getBeforeGuestStandby());
        setBoolProperty(properties, TOOLS_BEFORE_GUEST_SHUTDOWN, tools.getBeforeGuestShutdown());
        setBoolProperty(properties, TOOLS_BEFORE_GUEST_REBOOT, tools.getBeforeGuestReboot());

        // Save flags
        VirtualMachineFlagInfo flags = configInfo.getFlags();
        setStringProperty(properties, FLAGS_SNAPSHOT_POWER_OFF_BEHAVIOR, flags.getSnapshotPowerOffBehavior());

        // Save power op info
        VirtualMachineDefaultPowerOpInfo powerInfo = configInfo.getDefaultPowerOps();
        setStringProperty(properties, DEFAULTPOWEROPS_POWER_OFF_TYPE, powerInfo.getPowerOffType());
        setStringProperty(properties, DEFAULTPOWEROPS_SUSPEND_TYPE, powerInfo.getSuspendType());
        setStringProperty(properties, DEFAULTPOWEROPS_RESET_TYPE, powerInfo.getResetType());
        setStringProperty(properties, DEFAULTPOWEROPS_STANDBY_ACTION, powerInfo.getStandbyAction());

        // Save the devices
        VirtualDevice[] virtualDevices = configInfo.getHardware().getDevice();

        int idx = 0;
        for (VirtualDevice virtualDevice : virtualDevices) {
            if (virtualDevice instanceof VirtualIDEController
                    || virtualDevice instanceof VirtualPS2Controller
                    || virtualDevice instanceof VirtualPCIController
                    || virtualDevice instanceof VirtualSIOController
                    || virtualDevice instanceof VirtualKeyboard
                    || virtualDevice instanceof VirtualMachineVMCIDevice
                    || virtualDevice instanceof VirtualPointingDevice) {
                continue;
            }

            saveVirtualDevice(virtualDevice, properties, idx);

            VirtualDeviceBackingInfo backingInfo = virtualDevice.getBacking();
            if (backingInfo != null) {
                saveVirtualDeviceBacking(backingInfo, properties, idx);
            }

            ++idx;
        }
        setIntProperty(properties, DEVICE_NUM, idx);

        return properties;
    }

    private static void saveVirtualDevice(VirtualDevice virtualDevice, Properties properties, int idx) {
        setIndexedStringProperty(properties, DEVICE_CLASS, idx, virtualDevice.getClass().getName());

        int deviceKey = virtualDevice.getKey();
        if (!isKnownControllerKey(deviceKey)) {
            deviceKey = -deviceKey;
        }
        setIndexedIntProperty(properties, DEVICE_KEY, idx, deviceKey);

        int controllerKey = virtualDevice.getControllerKey();
        if (!isKnownControllerKey(controllerKey)) {
            controllerKey = -controllerKey;
        }
        setIndexedIntProperty(properties, DEVICE_CONTROLLER_KEY, idx, controllerKey);

        setIndexedIntProperty(properties, DEVICE_UNIT_NUMBER, idx, virtualDevice.getUnitNumber());

        if (virtualDevice instanceof VirtualController) {
            VirtualController virtualController = (VirtualController) virtualDevice;

            setIndexedIntProperty(properties, DEVICE_CONTROLLER_BUS_NUMBER, idx, virtualController.getBusNumber());

            if (virtualDevice instanceof VirtualSCSIController) {
                VirtualSCSIController virtualScsiController = (VirtualSCSIController) virtualDevice;

                VirtualSCSISharing sharedBus = virtualScsiController.getSharedBus();
                if (sharedBus != null) {
                    setIndexedStringProperty(properties, DEVICE_SCSI_CONTROLLER_SHARED_BUS, idx, sharedBus.toString());
                }
            }
        } else if (virtualDevice instanceof VirtualDisk) {
            VirtualDisk virtualDisk = (VirtualDisk) virtualDevice;

            setIndexedLongProperty(properties, DEVICE_DISK_CAPACITY_IN_KB, idx, virtualDisk.getCapacityInKB());
        } else if (virtualDevice instanceof VirtualEthernetCard) {
            VirtualEthernetCard virtualNic = (VirtualEthernetCard) virtualDevice;

            setIndexedStringProperty(properties, DEVICE_NIC_ADDRESS_TYPE, idx, virtualNic.getAddressType());
        }
    }

    private static void saveVirtualDeviceBacking(VirtualDeviceBackingInfo backingInfo, Properties properties, int idx) {
        setIndexedStringProperty(properties, DEVICE_BACKING_CLASS, idx, backingInfo.getClass().getName());

        if (backingInfo instanceof VirtualDeviceDeviceBackingInfo) {
            VirtualDeviceDeviceBackingInfo deviceBackingInfo = (VirtualDeviceDeviceBackingInfo) backingInfo;

            String deviceName = deviceBackingInfo.getDeviceName();
            if (!StringUtils.isEmpty(deviceName)) {
                setIndexedStringProperty(properties, DEVICE_REMOTE_BACKING_DEVICE_NAME, idx, deviceName);
            }

            setIndexedBoolProperty(properties, DEVICE_REMOTE_BACKING_USE_AUTO_DETECT, idx, deviceBackingInfo.getUseAutoDetect());

            if (backingInfo instanceof VirtualEthernetCardNetworkBackingInfo) {
                VirtualEthernetCardNetworkBackingInfo nicBackingInfo = (VirtualEthernetCardNetworkBackingInfo) backingInfo;

                ManagedObjectReference networkMor = nicBackingInfo.getNetwork();
                if (networkMor != null && !StringUtils.isEmpty(networkMor.get_value())) {
                    setIndexedStringProperty(properties, DEVICE_BACKING_NIC_NETWORK, idx, networkMor.get_value());
                }
            }
        } else if (backingInfo instanceof VirtualDeviceRemoteDeviceBackingInfo) {
            VirtualDeviceRemoteDeviceBackingInfo deviceBackingInfo = (VirtualDeviceRemoteDeviceBackingInfo) backingInfo;

            String deviceName = deviceBackingInfo.getDeviceName();
            if (!StringUtils.isEmpty(deviceName)) {
                setIndexedStringProperty(properties, DEVICE_REMOTE_BACKING_DEVICE_NAME, idx, deviceName);
            }

            setIndexedBoolProperty(properties, DEVICE_REMOTE_BACKING_USE_AUTO_DETECT, idx, deviceBackingInfo.getUseAutoDetect());
        } else if (backingInfo instanceof VirtualDeviceFileBackingInfo) {
            VirtualDeviceFileBackingInfo fileBackingInfo = (VirtualDeviceFileBackingInfo) backingInfo;

            ManagedObjectReference dsMor = fileBackingInfo.getDatastore();
            if (dsMor != null && !StringUtils.isEmpty(dsMor.get_value())) {
                setIndexedStringProperty(properties, DEVICE_FILE_BACKING_DATASTORE, idx, dsMor.get_value());
            }

            String fileName = fileBackingInfo.getFileName();
            if (!StringUtils.isEmpty(fileName)) {
                setIndexedStringProperty(properties, DEVICE_FILE_BACKING_FILE_NAME, idx, fileName);
            }

            if (backingInfo instanceof VirtualDiskFlatVer2BackingInfo) {
                VirtualDiskFlatVer2BackingInfo diskBackingInfo = (VirtualDiskFlatVer2BackingInfo) backingInfo;

                setIndexedStringProperty(properties, DEVICE_BACKING_DISK_MODE, idx, diskBackingInfo.getDiskMode());
                setIndexedBoolProperty(properties, DEVICE_BACKING_DISK_SPLIT, idx, diskBackingInfo.getSplit());
                setIndexedBoolProperty(properties, DEVICE_BACKING_DISK_WRITE_THROUGH, idx, diskBackingInfo.getWriteThrough());
            } else if (backingInfo instanceof VirtualDiskSparseVer2BackingInfo) {
                VirtualDiskSparseVer2BackingInfo diskBackingInfo = (VirtualDiskSparseVer2BackingInfo) backingInfo;

                setIndexedStringProperty(properties, DEVICE_BACKING_DISK_MODE, idx, diskBackingInfo.getDiskMode());
                setIndexedBoolProperty(properties, DEVICE_BACKING_DISK_SPLIT, idx, diskBackingInfo.getSplit());
                setIndexedBoolProperty(properties, DEVICE_BACKING_DISK_WRITE_THROUGH, idx, diskBackingInfo.getWriteThrough());
            } else if (backingInfo instanceof VirtualDiskFlatVer1BackingInfo) {
                VirtualDiskFlatVer1BackingInfo diskBackingInfo = (VirtualDiskFlatVer1BackingInfo) backingInfo;

                setIndexedStringProperty(properties, DEVICE_BACKING_DISK_MODE, idx, diskBackingInfo.getDiskMode());
                setIndexedBoolProperty(properties, DEVICE_BACKING_DISK_SPLIT, idx, diskBackingInfo.getSplit());
                setIndexedBoolProperty(properties, DEVICE_BACKING_DISK_WRITE_THROUGH, idx, diskBackingInfo.getWriteThrough());
            } else if (backingInfo instanceof VirtualDiskSparseVer1BackingInfo) {
                VirtualDiskSparseVer1BackingInfo diskBackingInfo = (VirtualDiskSparseVer1BackingInfo) backingInfo;

                setIndexedStringProperty(properties, DEVICE_BACKING_DISK_MODE, idx, diskBackingInfo.getDiskMode());
                setIndexedBoolProperty(properties, DEVICE_BACKING_DISK_SPLIT, idx, diskBackingInfo.getSplit());
                setIndexedBoolProperty(properties, DEVICE_BACKING_DISK_WRITE_THROUGH, idx, diskBackingInfo.getWriteThrough());
            }
        }
    }

    private static boolean isKnownControllerKey(int key) {
        return (key == DEVICE_KEY_PCI_CONTROLLER)
        || (key == DEVICE_KEY_PRIMARY_IDE_CONTROLLER)
        || (key == DEVICE_KEY_SECONDARY_IDE_CONTROLLER)
        || (key == DEVICE_KEY_PS2_CONTROLLER)
        || (key == DEVICE_KEY_SIO_CONTROLLER);
    }

    public static VirtualMachineConfigSpec convert(Properties properties) {
        VirtualMachineConfigSpec configSpec =  new VirtualMachineConfigSpec();

        // Set VM values
        configSpec.setName(getStringProperty(properties, NAME, "Restored VM"));
        configSpec.setVersion(getStringProperty(properties, VERSION, "vmx-07"));
        configSpec.setGuestId(getStringProperty(properties, GUEST_ID, "winNetStandardGuest"));
        configSpec.setNumCPUs(getIntProperty(properties, HARDWARE_NUM_CPU, 1));
        configSpec.setMemoryMB(getLongProperty(properties, HARDWARE_MEMORY_MB, 256));

        // Set up file storage info
        VirtualMachineFileInfo vmfileInfo = new VirtualMachineFileInfo();
        vmfileInfo.setVmPathName(getStringProperty(properties, FILES_VM_PATH_NAME, "[datastore]"));
        vmfileInfo.setSnapshotDirectory(getStringProperty(properties, FILES_SNAPSHOT_DIRECTORY, "[datastore]"));
        configSpec.setFiles(vmfileInfo);

        // Set up tools config info
        ToolsConfigInfo tools = new ToolsConfigInfo();
        tools.setAfterPowerOn(getBoolProperty(properties, TOOLS_AFTER_POWER_ON, true));
        tools.setAfterResume(getBoolProperty(properties, TOOLS_AFTER_RESUME, true));
        tools.setBeforeGuestStandby(getBoolProperty(properties, TOOLS_BEFORE_GUEST_STANDBY, true));
        tools.setBeforeGuestShutdown(getBoolProperty(properties, TOOLS_BEFORE_GUEST_SHUTDOWN, true));
        tools.setBeforeGuestReboot(getBoolProperty(properties, TOOLS_BEFORE_GUEST_REBOOT, true));
        configSpec.setTools(tools);

        // Set flags
        VirtualMachineFlagInfo flags = new VirtualMachineFlagInfo();
        flags.setSnapshotPowerOffBehavior(getStringProperty(properties, FLAGS_SNAPSHOT_POWER_OFF_BEHAVIOR, "powerOff"));
        configSpec.setFlags(flags);

        // Set power op info
        VirtualMachineDefaultPowerOpInfo powerInfo = new VirtualMachineDefaultPowerOpInfo();
        powerInfo.setPowerOffType(getStringProperty(properties, DEFAULTPOWEROPS_POWER_OFF_TYPE, "preset"));
        powerInfo.setSuspendType(getStringProperty(properties, DEFAULTPOWEROPS_SUSPEND_TYPE, "preset"));
        powerInfo.setResetType(getStringProperty(properties, DEFAULTPOWEROPS_RESET_TYPE, "preset"));
        powerInfo.setStandbyAction(getStringProperty(properties, DEFAULTPOWEROPS_STANDBY_ACTION, "powerOnSuspend"));
        configSpec.setPowerOpInfo(powerInfo);

        // Add in the devices
        int numDevice = getIntProperty(properties, DEVICE_NUM, 0);
        if (numDevice > 0) {
            VirtualDeviceConfigSpec[] deviceConfigSpecs = new VirtualDeviceConfigSpec[numDevice];
            configSpec.setDeviceChange(deviceConfigSpecs);

            for (int idx = 0; idx < numDevice; idx++) {
                deviceConfigSpecs[idx] = createDeviceConfigSpec(properties, idx);
            }
        }

        return configSpec;
    }

    private static VirtualDeviceConfigSpec createDeviceConfigSpec(Properties properties, int idx) {
        VirtualDevice virtualDevice = createVirtualDevice(properties, idx);
        if (virtualDevice == null) {
            return null;
        }

        VirtualDeviceBackingInfo backingInfo = createVirtualDeviceBacking(properties, idx);
        if (backingInfo != null) {
            virtualDevice.setBacking(backingInfo);
        }

        VirtualDeviceConfigSpec deviceConfigSpec = new VirtualDeviceConfigSpec();
        deviceConfigSpec.setOperation(VirtualDeviceConfigSpecOperation.add);
        if (backingInfo instanceof VirtualDeviceFileBackingInfo) {
            deviceConfigSpec.setFileOperation(VirtualDeviceConfigSpecFileOperation.create);
        }
        deviceConfigSpec.setDevice(virtualDevice);

        return deviceConfigSpec;
    }

    private static VirtualDevice createVirtualDevice(Properties properties, int idx) {
        String deviceClass = getIndexedStringProperty(properties, DEVICE_CLASS, idx, null);
        if (StringUtils.isEmpty(deviceClass)) {
            LOGGER.error(String.format("Unable to find device class for idx (%d)", idx));
            return null;
        }

        VirtualDevice virtualDevice = null;

        try {
            Class<?> vdClass = Class.forName(deviceClass);
            virtualDevice = (VirtualDevice) vdClass.newInstance();
        } catch (Exception e) {
            LOGGER.error(String.format("Failed to create device \"%s\"", deviceClass), e);
            return null;
        }

        int deviceKey = getIndexedIntProperty(properties, DEVICE_KEY, idx, 0);
        if (deviceKey != 0) {
            virtualDevice.setKey(deviceKey);
        }

        int controllerKey = getIndexedIntProperty(properties, DEVICE_CONTROLLER_KEY, idx, 0);
        if (controllerKey != 0) {
            virtualDevice.setControllerKey(controllerKey);
        }

        int unitNumber = getIndexedIntProperty(properties, DEVICE_UNIT_NUMBER, idx, 0);
        if (unitNumber >= 0) {
            virtualDevice.setUnitNumber(unitNumber);
        }

        if (virtualDevice instanceof VirtualController) {
            VirtualController virtualController = (VirtualController) virtualDevice;

            int busNumber = getIndexedIntProperty(properties, DEVICE_CONTROLLER_BUS_NUMBER, idx, 0);
            if (busNumber >= 0) {
                virtualController.setBusNumber(busNumber);
            }

            if (virtualDevice instanceof VirtualSCSIController) {
                VirtualSCSIController virtualScsiController = (VirtualSCSIController) virtualDevice;

                VirtualSCSISharing sharedBus = VirtualSCSISharing.noSharing;
                String sharedBusStr = getIndexedStringProperty(properties, DEVICE_SCSI_CONTROLLER_SHARED_BUS, idx, null);
                if (!StringUtils.isEmpty(sharedBusStr)) {
                    try {
                        sharedBus = VirtualSCSISharing.valueOf(sharedBusStr);
                    } catch (Throwable throwable) {
                    }
                }

                virtualScsiController.setSharedBus(sharedBus);
            }
        } else if (virtualDevice instanceof VirtualDisk) {
            VirtualDisk virtualDisk = (VirtualDisk) virtualDevice;

            long capacityInKB = getIndexedLongProperty(properties, DEVICE_DISK_CAPACITY_IN_KB, idx, 2 * 1024 * 1024);
            virtualDisk.setCapacityInKB(capacityInKB);
        } else if (virtualDevice instanceof VirtualEthernetCard) {
            VirtualEthernetCard virtualNic = (VirtualEthernetCard) virtualDevice;

            virtualNic.setAddressType(getIndexedStringProperty(properties, DEVICE_NIC_ADDRESS_TYPE, idx, "generated"));
        }

        return virtualDevice;
    }

    private static VirtualDeviceBackingInfo createVirtualDeviceBacking(Properties properties, int idx) {
        String deviceBackingClass = getIndexedStringProperty(properties, DEVICE_BACKING_CLASS, idx, null);
        if (StringUtils.isEmpty(deviceBackingClass)) {
            LOGGER.info(String.format("No device backing class for idx (%d)", idx));
            return null;
        }

        VirtualDeviceBackingInfo backingInfo = null;

        try {
            Class<?> vdbClass = Class.forName(deviceBackingClass);
            backingInfo = (VirtualDeviceBackingInfo) vdbClass.newInstance();
        } catch (Exception e) {
            LOGGER.error(String.format("Failed to create device backing \"%s\"", deviceBackingClass), e);
            return null;
        }

        if (backingInfo instanceof VirtualDeviceDeviceBackingInfo) {
            VirtualDeviceDeviceBackingInfo deviceBackingInfo = (VirtualDeviceDeviceBackingInfo) backingInfo;

            String deviceName = getIndexedStringProperty(properties, DEVICE_REMOTE_BACKING_DEVICE_NAME, idx, null);
            if (!StringUtils.isEmpty(deviceName)) {
                deviceBackingInfo.setDeviceName(deviceName);
            }

            deviceBackingInfo.setUseAutoDetect(getIndexedBoolProperty(properties, DEVICE_REMOTE_BACKING_USE_AUTO_DETECT, idx, false));

            if (backingInfo instanceof VirtualEthernetCardNetworkBackingInfo) {
                VirtualEthernetCardNetworkBackingInfo nicBackingInfo = (VirtualEthernetCardNetworkBackingInfo) backingInfo;

                String networkId = getIndexedStringProperty(properties, DEVICE_BACKING_NIC_NETWORK, idx, null);
                if (!StringUtils.isEmpty(networkId)) {
                    ManagedObjectReference networkMor = new ManagedObjectReference();
                    networkMor.set_value(networkId);
                    networkMor.setType("Network");
                    nicBackingInfo.setNetwork(networkMor);
                }
            }
        } else if (backingInfo instanceof VirtualDeviceRemoteDeviceBackingInfo) {
            VirtualDeviceRemoteDeviceBackingInfo deviceBackingInfo = (VirtualDeviceRemoteDeviceBackingInfo) backingInfo;

            String deviceName = getIndexedStringProperty(properties, DEVICE_REMOTE_BACKING_DEVICE_NAME, idx, null);
            if (!StringUtils.isEmpty(deviceName)) {
                deviceBackingInfo.setDeviceName(deviceName);
            }

            deviceBackingInfo.setUseAutoDetect(getIndexedBoolProperty(properties, DEVICE_REMOTE_BACKING_USE_AUTO_DETECT, idx, false));
        } else if (backingInfo instanceof VirtualDeviceFileBackingInfo) {
            VirtualDeviceFileBackingInfo fileBackingInfo = (VirtualDeviceFileBackingInfo) backingInfo;

            String datastoreId = getIndexedStringProperty(properties, DEVICE_FILE_BACKING_DATASTORE, idx, null);
            if (!StringUtils.isEmpty(datastoreId)) {
                ManagedObjectReference dsMor = new ManagedObjectReference();
                dsMor.set_value(datastoreId);
                dsMor.setType("Datastore");
                fileBackingInfo.setDatastore(dsMor);
            }

            String fileName = getIndexedStringProperty(properties, DEVICE_FILE_BACKING_FILE_NAME, idx, null);
            if (!StringUtils.isEmpty(fileName)) {
                fileBackingInfo.setFileName(fileName);
            }

            String diskMode = getIndexedStringProperty(properties, DEVICE_BACKING_DISK_MODE, idx, "persistent");
            boolean diskSplit = getIndexedBoolProperty(properties, DEVICE_BACKING_DISK_SPLIT, idx, false);
            boolean diskWriteThrough = getIndexedBoolProperty(properties, DEVICE_BACKING_DISK_WRITE_THROUGH, idx, false);

            if (backingInfo instanceof VirtualDiskFlatVer2BackingInfo) {
                VirtualDiskFlatVer2BackingInfo diskBackingInfo = (VirtualDiskFlatVer2BackingInfo) backingInfo;

                diskBackingInfo.setDiskMode(diskMode);
                diskBackingInfo.setSplit(diskSplit);
                diskBackingInfo.setWriteThrough(diskWriteThrough);
            } else if (backingInfo instanceof VirtualDiskSparseVer2BackingInfo) {
                VirtualDiskSparseVer2BackingInfo diskBackingInfo = (VirtualDiskSparseVer2BackingInfo) backingInfo;

                diskBackingInfo.setDiskMode(diskMode);
                diskBackingInfo.setSplit(diskSplit);
                diskBackingInfo.setWriteThrough(diskWriteThrough);
            } else if (backingInfo instanceof VirtualDiskFlatVer1BackingInfo) {
                VirtualDiskFlatVer1BackingInfo diskBackingInfo = (VirtualDiskFlatVer1BackingInfo) backingInfo;

                diskBackingInfo.setDiskMode(diskMode);
                diskBackingInfo.setSplit(diskSplit);
                diskBackingInfo.setWriteThrough(diskWriteThrough);
            } else if (backingInfo instanceof VirtualDiskSparseVer1BackingInfo) {
                VirtualDiskSparseVer1BackingInfo diskBackingInfo = (VirtualDiskSparseVer1BackingInfo) backingInfo;

                diskBackingInfo.setDiskMode(diskMode);
                diskBackingInfo.setSplit(diskSplit);
                diskBackingInfo.setWriteThrough(diskWriteThrough);
            }
        }

        return backingInfo;
    }

    private static void setStringProperty(Properties properties, String key, String value) {
        if (value != null) {
            properties.setProperty(key, value);
        }
    }

    private static void setIntProperty(Properties properties, String key, int value) {
        properties.setProperty(key, String.valueOf(value));
    }

    private static void setLongProperty(Properties properties, String key, long value) {
        properties.setProperty(key, String.valueOf(value));
    }

    private static void setBoolProperty(Properties properties, String key, Boolean value) {
        if (value != null) {
            properties.setProperty(key, value ? "1" : "0");
        }
    }

    private static void setIndexedStringProperty(Properties properties, String keyFormat, int idx, String value) {
        if (value != null) {
            String key = String.format(keyFormat, idx);
            setStringProperty(properties, key, value);
        }
    }

    private static void setIndexedIntProperty(Properties properties, String keyFormat, int idx, int value) {
        String key = String.format(keyFormat, idx);
        setIntProperty(properties, key, value);
    }

    private static void setIndexedLongProperty(Properties properties, String keyFormat, int idx, long value) {
        String key = String.format(keyFormat, idx);
        setLongProperty(properties, key, value);
    }

    private static void setIndexedBoolProperty(Properties properties, String keyFormat, int idx, Boolean value) {
        if (value != null) {
            String key = String.format(keyFormat, idx);
            setBoolProperty(properties, key, value);
        }
    }

    private static String getStringProperty(Properties properties, String key, String defaultValue) {
        return properties.getProperty(key, defaultValue);
    }

    private static int getIntProperty(Properties properties, String key, int defaultValue) {
        String strValue =  properties.getProperty(key);
        if (strValue == null) {
            return defaultValue;
        }

        try {
            return Integer.parseInt(strValue);
        } catch (Exception e) {
            return defaultValue;
        }
    }

    private static long getLongProperty(Properties properties, String key, long defaultValue) {
        String strValue =  properties.getProperty(key);
        if (strValue == null) {
            return defaultValue;
        }

        try {
            return Long.parseLong(strValue);
        } catch (Exception e) {
            return defaultValue;
        }
    }

    private static boolean getBoolProperty(Properties properties, String key, boolean defaultValue) {
        String strValue =  properties.getProperty(key);
        if (strValue == null) {
            return defaultValue;
        }

        return "1".equals(strValue);
    }

    private static String getIndexedStringProperty(Properties properties, String keyFormat, int idx, String defaultValue) {
        String key = String.format(keyFormat, idx);
        return properties.getProperty(key, defaultValue);
    }

    private static int getIndexedIntProperty(Properties properties, String keyFormat, int idx, int defaultValue) {
        String key = String.format(keyFormat, idx);

        String strValue =  properties.getProperty(key);
        if (strValue == null) {
            return defaultValue;
        }

        try {
            return Integer.parseInt(strValue);
        } catch (Exception e) {
            return defaultValue;
        }
    }

    private static long getIndexedLongProperty(Properties properties, String keyFormat, int idx, long defaultValue) {
        String key = String.format(keyFormat, idx);

        String strValue =  properties.getProperty(key);
        if (strValue == null) {
            return defaultValue;
        }

        try {
            return Long.parseLong(strValue);
        } catch (Exception e) {
            return defaultValue;
        }
    }

    private static boolean getIndexedBoolProperty(Properties properties, String keyFormat, int idx, boolean defaultValue) {
        String key = String.format(keyFormat, idx);

        String strValue =  properties.getProperty(key);
        if (strValue == null) {
            return defaultValue;
        }

        return "1".equals(strValue);
    }
}

