/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.hypervisor.vmware.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.nimdesk.hypervisor.vmware.VmwareContext;
import com.nimdesk.hypervisor.vmware.mo.DatastoreMO;
import com.nimdesk.hypervisor.vmware.mo.VirtualMachineMO;
import com.nimdesk.util.ActionDelegate;
import com.nimdesk.util.Pair;
import com.vmware.vim25.DynamicProperty;
import com.vmware.vim25.ManagedObjectReference;
import com.vmware.vim25.ObjectContent;
import com.vmware.vim25.VirtualDevice;
import com.vmware.vim25.VirtualDeviceConnectInfo;
import com.vmware.vim25.VirtualDisk;
import com.vmware.vim25.VirtualDiskFlatVer2BackingInfo;
import com.vmware.vim25.VirtualDiskMode;
import com.vmware.vim25.VirtualMachineSnapshotTree;

/**
 *
 */
public class VmwareUtils {
    private static final Logger LOGGER = Logger.getLogger(VmwareUtils.class);

    public static Object getPropValue(ObjectContent oc, String name) {
        DynamicProperty[] props = oc.getPropSet();

        if (props != null) {
            for (DynamicProperty prop : props) {
                if (prop.getName().equalsIgnoreCase(name)) {
                    return prop.getVal();
                }
            }
        }

        return null;
    }

    public static List<Pair<ManagedObjectReference, Map<String, Object>>> getObjectProps(ObjectContent[] ocs) {
        ArrayList<Pair<ManagedObjectReference, Map<String, Object>>> objPropsList =
                new ArrayList<Pair<ManagedObjectReference, Map<String, Object>>>();

        if (ocs != null) {
            for (ObjectContent oc : ocs) {
                ManagedObjectReference mor = oc.getObj();

                HashMap<String, Object> props = new HashMap<String, Object>();
                for (DynamicProperty prop : oc.getPropSet()) {
                    props.put(prop.getName(), prop.getVal());
                }

                objPropsList.add(new Pair<ManagedObjectReference, Map<String, Object>>(mor, props));
            }
        }

        return objPropsList;
    }

    public static ManagedObjectReference findSnapshotInTree(VirtualMachineSnapshotTree[] snapTree, String snapshotName) {
        assert(snapshotName != null);

        ManagedObjectReference snapMor = null;
        if (snapTree == null) {
            return snapMor;
        }

        for (int i = 0; i < snapTree.length && snapMor == null; i++) {
            VirtualMachineSnapshotTree node = snapTree[i];

            if (StringUtils.equals(node.getName(), snapshotName)) {
                snapMor = node.getSnapshot();
            } else {
                VirtualMachineSnapshotTree[] childTree = node.getChildSnapshotList();
                snapMor = findSnapshotInTree(childTree, snapshotName);
            }
        }

        return snapMor;
    }

    private static final int READ_CHUNK_SIZE = 1024 * 1024; // 1 Mi
    private static final int WRITE_CHUNK_SIZE = 9 * 1024; // 9 Ki // a bug in jdk6 which fails when > 10k

    public static long getDatastoreFile(VmwareContext context, String urlStr, String localFilePath,
            ActionDelegate<Long> progressUpdater)
                    throws IOException {
        HttpURLConnection conn = context.getHttpGetConnection(urlStr, null);
        InputStream ins = null;
        OutputStream outs = null;
        long bytesWritten = 0;

        try {
            ins = conn.getInputStream();
            outs = new FileOutputStream(new File(localFilePath));

            byte[] buf = new byte[READ_CHUNK_SIZE]; // 1 Mi
            int len = 0;

            while ((len = ins.read(buf)) > 0) {
                outs.write(buf, 0, len);
                bytesWritten += len;

                if (progressUpdater != null) {
                    progressUpdater.action(new Long(bytesWritten));
                }
            }
        } finally {
            if (ins != null) {
                try {
                    ins.close();
                } catch (IOException e) {
                    // Ignore
                }
            }
            if (outs != null) {
                try {
                    outs.close();
                } catch (IOException e) {
                    // Ignore
                }
            }
        }

        return bytesWritten;
    }

    public static void putDatastoreFile(VmwareContext context, String httpMethod, String urlStr, String localFilePath,
            ActionDelegate<Long> progressUpdater)
                    throws IOException {
        File localFile = new File(localFilePath);

        Map<String, String> properties = new HashMap<String, String>();
        properties.put("Content-Type", "application/octet-stream");
        properties.put("Content-Length", Long.toString(localFile.length()));

        if (StringUtils.isEmpty(httpMethod)) {
            httpMethod = "PUT";
        }

        HttpURLConnection conn = context.getHttpConnection(urlStr, httpMethod, properties);
        InputStream ins = null;
        OutputStream outs = null;
        //BufferedReader br = null;
        long bytesWritten = 0;

        try {
            outs = conn.getOutputStream();
            ins = new FileInputStream(localFile);

            byte[] buf = new byte[READ_CHUNK_SIZE];
            int len = 0;
            int offset;
            int leftover;

            while ((leftover = len = ins.read(buf)) > 0) {
                for (offset = 0; leftover >= WRITE_CHUNK_SIZE; offset += WRITE_CHUNK_SIZE, leftover -= WRITE_CHUNK_SIZE) {
                    outs.write(buf, offset, WRITE_CHUNK_SIZE);
                }
                if (leftover > 0) {
                    outs.write(buf, offset, leftover);
                }
                //outs.write(buf, 0, len);
                bytesWritten += len;

                if (progressUpdater != null) {
                    progressUpdater.action(new Long(bytesWritten));
                }
            }
            outs.flush();

            /*
            br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = br.readLine()) != null) {
                if (LOGGER.isTraceEnabled()) {
                    LOGGER.trace("Upload " + urlStr + " response: " + line);
                }
            }
            */
        } finally {
            if (ins != null) {
                try {
                    ins.close();
                } catch (IOException e) {
                    // Ignore
                }
            }
            if (outs != null) {
                try {
                    outs.close();
                } catch (IOException e) {
                    // Ignore
                }
            }
            /*
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    // Ignore
                }
            }
            */
        }
    }

    public static void copyDatastoreFile(VmwareContext srcContext, String srcUrlStr,
            VmwareContext destContext, String destUrlStr, long fileSize,
            ActionDelegate<Long> progressUpdater)
            throws IOException {
        HttpURLConnection srcConn = srcContext.getHttpGetConnection(srcUrlStr, null);

        Map<String, String> properties = new HashMap<String, String>();
        properties.put("Content-Type", "application/octet-stream");
        if (fileSize > 0) {
            properties.put("Content-Length", Long.toString(fileSize));
        }
        HttpURLConnection destConn = destContext.getHttpPutConnection(destUrlStr, properties);

        InputStream ins = null;
        OutputStream outs = null;
        //BufferedReader br = null;

        try {
            // Add retry logic to deal with another ds file copy after a length copy
            // in which case the previous context has timed out.

            try {
                ins = srcConn.getInputStream();
            } catch (IOException e) {
                LOGGER.warn(String.format("Failed to copyDatastoreFile getInputStream from \"%s\" to \"%s\"", srcUrlStr, destUrlStr), e);

                // Check if it's authentication exception.
                try {
                    srcContext.reconnect();
                } catch (Exception e1) {
                    throw new IOException("Failed to reconnect to ESXi", e1);
                }
                srcConn = srcContext.getHttpGetConnection(srcUrlStr, null);
                ins = srcConn.getInputStream();
            }

            try {
                outs = destConn.getOutputStream();
            } catch (IOException e) {
                LOGGER.warn(String.format("Failed to copyDatastoreFile getOutputStream from \"%s\" to \"%s\"", srcUrlStr, destUrlStr), e);

                // Check if it's authentication exception.
                try {
                    destContext.reconnect();
                } catch (Exception e1) {
                    throw new IOException("Failed to reconnect to ESXi", e1);
                }
                destConn = destContext.getHttpPutConnection(destUrlStr, properties);
                outs = destConn.getOutputStream();
            }

            byte[] buf = new byte[READ_CHUNK_SIZE];
            int len = 0;
            int offset;
            int leftover;
            long transferredSize = 0;

            if (fileSize > 0) {
                while (fileSize > 0 && (leftover = len = ins.read(buf)) > 0) {
                    for (offset = 0; leftover >= WRITE_CHUNK_SIZE; offset += WRITE_CHUNK_SIZE, leftover -= WRITE_CHUNK_SIZE) {
                        outs.write(buf, offset, WRITE_CHUNK_SIZE);
                    }
                    if (leftover > 0) {
                        outs.write(buf, offset, leftover);
                    }
                    //outs.write(buf, 0, len);
                    transferredSize += len;
                    fileSize -= len;

                    if (progressUpdater != null) {
                        progressUpdater.action(new Long(transferredSize));
                    }
                }
            } else {
                while ((leftover = len = ins.read(buf)) > 0) {
                    for (offset = 0; leftover >= WRITE_CHUNK_SIZE; offset += WRITE_CHUNK_SIZE, leftover -= WRITE_CHUNK_SIZE) {
                        outs.write(buf, offset, WRITE_CHUNK_SIZE);
                    }
                    if (leftover > 0) {
                        outs.write(buf, offset, leftover);
                    }
                    //outs.write(buf, 0, len);
                    transferredSize += len;

                    if (progressUpdater != null) {
                        progressUpdater.action(new Long(transferredSize));
                    }
                }
            }
            outs.flush();
            try {
                outs.close();
                outs = null;
            } catch (IOException e) {
                // Ignore
            }

            LOGGER.info(String.format("Transferred %d bytes for %s", transferredSize, srcUrlStr));

            /*
            try {
                br = new BufferedReader(new InputStreamReader(destConn.getInputStream()));
                String line;
                while ((line = br.readLine()) != null) {
                    if (LOGGER.isTraceEnabled()) {
                        LOGGER.trace("Upload " + destUrlStr + " response: " + line);
                    }
                }
            } catch (Exception e) {
                LOGGER.warn(String.format("Failed to get response - %s", e.getMessage()));
                // Ignore error
            }
            */
        } finally {
            if (ins != null) {
                try {
                    ins.close();
                } catch (IOException e) {
                    // Ignore
                }
            }
            if (outs != null) {
                try {
                    outs.close();
                } catch (IOException e) {
                    // Ignore
                }
            }
            /*
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    // Ignore
                }
            }
            */
        }
    }

    public static String[] listDatastoreDirContent(VmwareContext context, String urlStr)
            throws IOException {
        List<String> fileList = new ArrayList<String>();
        String content = new String(getResourceContent(context, urlStr));
        String marker = "</a></td><td ";

        int parsePos = -1;
        do {
            parsePos = content.indexOf(marker, parsePos < 0 ? 0 : parsePos);
            if (parsePos > 0) {
                int beginPos = content.lastIndexOf('>', parsePos -1);
                if (beginPos < 0) {
                    beginPos = 0;
                }

                fileList.add((content.substring(beginPos + 1, parsePos)));
                parsePos += marker.length();
            } else {
                break;
            }
        } while (parsePos > 0);

        return fileList.toArray(new String[fileList.size()]);
    }

    private static byte[] getResourceContent(VmwareContext context, String urlStr)
            throws IOException {
        HttpURLConnection conn = context.getHttpGetConnection(urlStr, null);
        InputStream ins = null;
        ByteArrayOutputStream outs = null;

        try {
            ins = conn.getInputStream();
            outs = new ByteArrayOutputStream();

            byte[] buf = new byte[READ_CHUNK_SIZE];
            int len = 0;

            while ((len = ins.read(buf)) > 0) {
                outs.write(buf, 0, len);
            }

            return outs.toByteArray();
        } finally {
            if (ins != null) {
                try {
                    ins.close();
                } catch (IOException e) {
                    // Ignore
                }
            }
            if (outs != null) {
                try {
                    outs.close();
                } catch (IOException e) {
                    // Ignore
                }
            }
        }
    }

    // vmdkDatastorePath: [datastore name] vmdkFilePath
    public static VirtualDevice prepareDiskDevice(VirtualMachineMO vmMo, int controllerKey,
            String vmdkDatastorePathChain[], DatastoreMO dsMo, int deviceNumber, int contextNumber)
            throws Exception {
        assert(vmdkDatastorePathChain != null);
        assert(vmdkDatastorePathChain.length >= 1);

        VirtualDisk disk = new VirtualDisk();

        VirtualDiskFlatVer2BackingInfo backingInfo = new VirtualDiskFlatVer2BackingInfo();
        backingInfo.setDatastore(dsMo.getMor());
        backingInfo.setFileName(vmdkDatastorePathChain[0]);
        backingInfo.setDiskMode(VirtualDiskMode.persistent.toString());
        if (vmdkDatastorePathChain.length > 1) {
            String[] parentDisks = new String[vmdkDatastorePathChain.length - 1];
            for(int i = 0; i < vmdkDatastorePathChain.length - 1; i++)
                parentDisks[i] = vmdkDatastorePathChain[i + 1];

            setParentBackingInfo(backingInfo, dsMo.getMor(), parentDisks);
        }

        disk.setBacking(backingInfo);

        if(controllerKey < 0)
            controllerKey = vmMo.getIDEDeviceControllerKey();
        if(deviceNumber < 0)
            deviceNumber = vmMo.getNextDeviceNumber(controllerKey);

        disk.setControllerKey(controllerKey);
        disk.setKey(-contextNumber);
        disk.setUnitNumber(deviceNumber);

        VirtualDeviceConnectInfo connectInfo = new VirtualDeviceConnectInfo();
        connectInfo.setConnected(true);
        connectInfo.setStartConnected(true);
        disk.setConnectable(connectInfo);

        return disk;
    }

    private static void setParentBackingInfo(VirtualDiskFlatVer2BackingInfo backingInfo,
        ManagedObjectReference morDs, String[] parentDatastorePathList) {

        VirtualDiskFlatVer2BackingInfo parentBacking = new VirtualDiskFlatVer2BackingInfo();
        parentBacking.setDatastore(morDs);
        parentBacking.setDiskMode(VirtualDiskMode.persistent.toString());

        if(parentDatastorePathList.length > 1) {
            String[] nextDatastorePathList = new String[parentDatastorePathList.length -1];
            for(int i = 0; i < parentDatastorePathList.length -1; i++)
                nextDatastorePathList[i] = parentDatastorePathList[i + 1];
            setParentBackingInfo(parentBacking, morDs, nextDatastorePathList);
        }
        parentBacking.setFileName(parentDatastorePathList[0]);

        backingInfo.setParent(parentBacking);
    }
}
