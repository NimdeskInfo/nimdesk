/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.hypervisor.vmware.mo;

import java.rmi.RemoteException;

import org.apache.log4j.Logger;

import com.nimdesk.hypervisor.vmware.VmwareContext;
import com.nimdesk.util.ActionDelegate;
import com.vmware.vim25.FileFault;
import com.vmware.vim25.ManagedObjectReference;
import com.vmware.vim25.RuntimeFault;
import com.vmware.vim25.VirtualDiskSpec;
import com.vmware.vim25.mo.Task;
import com.vmware.vim25.mo.VirtualDiskManager;

public class VirtualDiskManagerMO extends BaseMO {
    private static final Logger LOGGER = Logger.getLogger(VirtualDiskManager.class);

    public VirtualDiskManagerMO(VmwareContext context, ManagedObjectReference mor) {
        super(context, mor);
    }

    public VirtualDiskManagerMO(VmwareContext context, String morType, String morRefId) {
        super(context, morType, morRefId);
    }

    public VirtualDiskManager getVirtualDiskManager() {
        return (VirtualDiskManager) getManagedObject();
    }

    /*
     * copy virtual disk
     * @param sourceName "[sourceDs] <source_path>/<source_disk.vmdk>"
     * @param destName   "[destDs] <dest_path>/<dest_disk.vmdk>".
     */
    public boolean copyVirtualDisk(String srcName, String destName, ActionDelegate<Integer> progressUpdater)
            throws FileFault, RuntimeFault, RemoteException {
        VirtualDiskSpec diskSpec = new VirtualDiskSpec();
        diskSpec.setDiskType("thin");
        return copyVirtualDisk(srcName, null, destName, null, diskSpec, true, progressUpdater);
    }

    public boolean copyVirtualDisk(String srcName, DatacenterMO srcDatacenter, String destName,
            DatacenterMO destDatacenter, VirtualDiskSpec diskSpec, boolean force,
            ActionDelegate<Integer> progressUpdater)
            throws FileFault, RuntimeFault, RemoteException {
        LOGGER.info(String.format("Copying virtual disk: src:\"%s\", dest:\"%s\"...", srcName, destName));

        Task task = getVirtualDiskManager().copyVirtualDisk_Task(srcName, srcDatacenter.getDatacenter(),
                destName, destDatacenter.getDatacenter(), diskSpec, force);

        String error;
        try {
            error = getContext().waitForTask(task, progressUpdater);
        } catch (Exception e) {
            LOGGER.error(String.format("Failed to copy virtual disk: src:\"%s\", dest:\"%s\"", srcName, destName), e);
            return false;
        }

        if (error != null) {
            LOGGER.error(String.format("Failed to copy virtual disk: src:\"%s\", dest:\"%s\", error:%s", srcName, destName, error));
            return false;
        }

        LOGGER.info(String.format("Copied virtual disk: src:\"%s\", dest:\"%s\"", srcName, destName));
        return true;
    }

    public boolean defragmentVirtualDisk(String name, DatacenterMO datacenter, ActionDelegate<Integer> progressUpdater)
            throws FileFault, RuntimeFault, RemoteException {
        LOGGER.info(String.format("Defragmenting virtual disk: \"%s\"...", name));

        Task task = getVirtualDiskManager().defragmentVirtualDisk_Task(name, datacenter.getDatacenter());

        String error;
        try {
            error = getContext().waitForTask(task, progressUpdater);
        } catch (Exception e) {
            LOGGER.error(String.format("Failed to defragment virtual disk: \"%s\"", name), e);
            return false;
        }

        if (error != null) {
            LOGGER.error(String.format("Failed to defragment virtual disk: \"%s\", error:%s", name, error));
            return false;
        }

        LOGGER.info(String.format("Defragmented virtual disk: \"%s\"", name));
        return true;
    }
}

