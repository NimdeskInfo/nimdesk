/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.hypervisor.vmware.mo;

import java.io.IOException;
import java.rmi.RemoteException;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.nimdesk.hypervisor.vmware.VmwareContext;
import com.nimdesk.hypervisor.vmware.util.DatastoreFile;
import com.nimdesk.hypervisor.vmware.util.VmwareUtils;
import com.nimdesk.util.ActionDelegate;
import com.vmware.vim25.DynamicProperty;
import com.vmware.vim25.InvalidProperty;
import com.vmware.vim25.ManagedObjectReference;
import com.vmware.vim25.ObjectContent;
import com.vmware.vim25.ObjectSpec;
import com.vmware.vim25.PropertyFilterSpec;
import com.vmware.vim25.PropertySpec;
import com.vmware.vim25.RuntimeFault;
import com.vmware.vim25.SelectionSpec;
import com.vmware.vim25.TraversalSpec;
import com.vmware.vim25.mo.Datacenter;
import com.vmware.vim25.mo.PropertyCollector;
import com.vmware.vim25.mo.util.PropertyCollectorUtil;

/**
 *
 */
public class DatacenterMO extends BaseMO {
    private static final Logger LOGGER = Logger.getLogger(DatacenterMO.class);

    public static final String MORTYPE_DATACENTER = "Datacenter";

    public DatacenterMO(VmwareContext context, ManagedObjectReference mor) {
        super(context, mor);
    }

    public DatacenterMO(VmwareContext context, ManagedObjectReference mor, String name) {
        super(context, mor, name);
    }

    public DatacenterMO(VmwareContext context, String dcRefId) {
        super(context, MORTYPE_DATACENTER, dcRefId);
    }

    public DatacenterMO(VmwareContext context, String dcRefId, String name) {
        super(context, MORTYPE_DATACENTER, dcRefId, name);
    }

    public Datacenter getDatacenter() {
        return (Datacenter) getManagedEntity();
    }

    public VirtualMachineMO findVmByName(String vmName) throws InvalidProperty, RuntimeFault, RemoteException {
        ObjectContent[] ocs = getVmProperties(new String[] {"name"});
        if (ocs != null) {
            for (ObjectContent oc : ocs) {
                DynamicProperty[] props = oc.getPropSet();
                if (props == null) {
                    continue;
                }

                for (DynamicProperty prop : props) {
                    if (StringUtils.equals(prop.getVal().toString(), vmName)) {
                        return new VirtualMachineMO(getContext(), oc.getObj());
                    }
                }
            }
        }

        return null;
    }

    @SuppressWarnings("deprecation")
    public ObjectContent[] getHostProperties(String[] propPaths) throws InvalidProperty, RuntimeFault, RemoteException {
        PropertySpec propSpec = PropertyCollectorUtil.createPropertySpec(HostMO.MORTYPE_HOST, false, propPaths);

        // Traversal through host
        TraversalSpec crToH = PropertyCollectorUtil.createTraversalSpec("crToH",
                "ComputeResource",  "host",
                new SelectionSpec[] {});

        // Recurse through the folders
        TraversalSpec visitFolders = PropertyCollectorUtil.createTraversalSpec("visitFolders",
                "Folder",  "childEntity",
                new String[] {"visitFolders", "dcToHf", "crToH"});

        // Traversal through hostFolder
        TraversalSpec dcToHf = PropertyCollectorUtil.createTraversalSpec("dcToHf",
                "Datacenter", "hostFolder",
                new String[] {"visitFolders"});

        ObjectSpec objSpec = new ObjectSpec();
        objSpec.setObj(getMor());
        objSpec.setSkip(Boolean.TRUE);
        objSpec.setSelectSet(new SelectionSpec[] { visitFolders, crToH, dcToHf });

        PropertyFilterSpec pfSpec = new PropertyFilterSpec();
        pfSpec.setPropSet(new PropertySpec[] { propSpec });
        pfSpec.setObjectSet(new ObjectSpec[] { objSpec });

        PropertyCollector propCollector = getContext().getServiceInstance().getPropertyCollector();

        return propCollector.retrieveProperties(new PropertyFilterSpec[] { pfSpec });
    }

    @SuppressWarnings("deprecation")
    public ObjectContent[] getDatastoreProperties(String[] propPaths) throws InvalidProperty, RuntimeFault, RemoteException {
        PropertySpec propSpec = PropertyCollectorUtil.createPropertySpec(DatastoreMO.MORTYPE_DATASTORE, false, propPaths);

        // Traversal through datastoreFolder
        TraversalSpec dcToDs = PropertyCollectorUtil.createTraversalSpec("dcToDs",
                "Datacenter", "datastore",
                new String[] {});

        ObjectSpec objSpec = new ObjectSpec();
        objSpec.setObj(getMor());
        objSpec.setSkip(Boolean.TRUE);
        objSpec.setSelectSet(new SelectionSpec[] { dcToDs });

        PropertyFilterSpec pfSpec = new PropertyFilterSpec();
        pfSpec.setPropSet(new PropertySpec[] { propSpec });
        pfSpec.setObjectSet(new ObjectSpec[] { objSpec });

        PropertyCollector propCollector = getContext().getServiceInstance().getPropertyCollector();

        return propCollector.retrieveProperties(new PropertyFilterSpec[] { pfSpec });
    }

    @SuppressWarnings("deprecation")
    public ObjectContent[] getVmProperties(String[] propPaths) throws InvalidProperty, RuntimeFault, RemoteException {
        PropertySpec propSpec = PropertyCollectorUtil.createPropertySpec(VirtualMachineMO.MORTYPE_VIRTUALMACHINE, false, propPaths);

        // Traversal through vmFolder
        TraversalSpec dcToVmf = PropertyCollectorUtil.createTraversalSpec("dcToVmf",
                "Datacenter", "vmFolder",
                new String[] {"visitFolders"});

        // Recurse through the folders
        TraversalSpec visitFolders = PropertyCollectorUtil.createTraversalSpec("visitFolders",
                "Folder",  "childEntity",
                new String[] {"visitFolders", "dcToVmf"});

        ObjectSpec objSpec = new ObjectSpec();
        objSpec.setObj(getMor());
        objSpec.setSkip(Boolean.TRUE);
        objSpec.setSelectSet(new SelectionSpec[] { visitFolders, dcToVmf });

        PropertyFilterSpec pfSpec = new PropertyFilterSpec();
        pfSpec.setPropSet(new PropertySpec[] { propSpec });
        pfSpec.setObjectSet(new ObjectSpec[] { objSpec });

        PropertyCollector propCollector = getContext().getServiceInstance().getPropertyCollector();

        return propCollector.retrieveProperties(new PropertyFilterSpec[] { pfSpec });
    }

    public void getDatastoreFile(String datastoreFilePath, String localFilePath)
            throws IOException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(String.format("Get datastore file \"%s\" to local \"%s\"", datastoreFilePath, localFilePath));
        }

        if (!DatastoreFile.isFullDatastorePath(datastoreFilePath)) {
            throw new IOException(String.format("Not a full datastore path: \"%s\"", datastoreFilePath));
        }

        String urlStr = getContext().getDatastoreBrowseUrl(getName(), datastoreFilePath);
        VmwareUtils.getDatastoreFile(getContext(), urlStr, localFilePath, null);
    }

    public void putDatastoreFile(String datastoreFilePath, String localFilePath)
            throws IOException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(String.format("Put local file \"%s\" to datastore \"%s\"", localFilePath, datastoreFilePath));
        }

        if (!DatastoreFile.isFullDatastorePath(datastoreFilePath)) {
            throw new IOException(String.format("Not a full datastore path: \"%s\"", datastoreFilePath));
        }

        String urlStr = getContext().getDatastoreBrowseUrl(getName(), datastoreFilePath);
        VmwareUtils.putDatastoreFile(getContext(), null, urlStr, localFilePath, null);
    }

    public void copyDatastoreFile(String srcDsFilePath, DatacenterMO destDatacenter, String destDsFilePath, final long fileSize,
            final ActionDelegate<Integer> progressUpdater)
            throws IOException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(String.format("Copy file \"%s\"(%s) to \"%s\"(%s)", getContext().getServerAddress(),
                    srcDsFilePath, destDatacenter.getContext().getServerAddress(), destDsFilePath));
        }

        if (!DatastoreFile.isFullDatastorePath(srcDsFilePath)) {
            throw new IOException(String.format("Source not a full datastore path: \"%s\"", srcDsFilePath));
        }
        if (!DatastoreFile.isFullDatastorePath(destDsFilePath)) {
            throw new IOException(String.format("Destination not a full datastore path: \"%s\"", destDsFilePath));
        }

        String srcUrlStr = getContext().getDatastoreBrowseUrl(getName(), srcDsFilePath);
        String destUrlStr = destDatacenter.getContext().getDatastoreBrowseUrl(destDatacenter.getName(), destDsFilePath);

        VmwareUtils.copyDatastoreFile(getContext(), srcUrlStr, destDatacenter.getContext(), destUrlStr, fileSize,
                (progressUpdater == null || fileSize <= 0) ? null : new ActionDelegate<Long>() {
                    @Override
                    public void action(Long param) {
                        progressUpdater.action((int) ((float) param / fileSize));
                    }
        });
    }

    public String[] listDatastoreDirContent(String datastoreDirPath)
            throws IOException {
        LOGGER.info(String.format("List datastore dir \"%s\" content", datastoreDirPath));

        if (!DatastoreFile.isFullDatastorePath(datastoreDirPath)) {
            throw new IOException(String.format("Not a full datastore path: \"%s\"", datastoreDirPath));
        }

        String urlStr = getContext().getDatastoreBrowseUrl(getName(), datastoreDirPath);
        return VmwareUtils.listDatastoreDirContent(getContext(), urlStr);
    }
}
