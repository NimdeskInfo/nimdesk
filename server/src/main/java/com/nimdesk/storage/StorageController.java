/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.storage;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import com.nimdesk.event.AllocateStorageEvent;
import com.nimdesk.event.AsyncCallback;
import com.nimdesk.event.CopyDiskImageEvent;
import com.nimdesk.event.DeleteStorageDirectoryEvent;
import com.nimdesk.event.EventHandler;
import com.nimdesk.event.ImportDiskImageEvent;
import com.nimdesk.event.Link2LocalReplicaEvent;
import com.nimdesk.event.LinkedCloneImageEvent;
import com.nimdesk.model.DiskImage;
import com.nimdesk.model.Storage;
import com.nimdesk.service.EventBusService;
import com.nimdesk.storage.vmware.VmwareStorageManager;
import com.nimdesk.util.ActionDelegate;
import com.nimdesk.util.Pair;
import com.google.common.eventbus.AllowConcurrentEvents;
import com.google.common.eventbus.Subscribe;

/**
 *
 */
public class StorageController implements EventHandler {
    private static final Logger LOGGER = Logger.getLogger(StorageController.class);

    private static final int CORE_STORAGE_POOL_SIZE = 2;
    private static final int MAX_STORAGE_POOL_SIZE = 200;

    private final ExecutorService _executor = new ThreadPoolExecutor(CORE_STORAGE_POOL_SIZE,
            MAX_STORAGE_POOL_SIZE, 300L, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>());

    private final EventBusService _eventBusService;
    private final VmwareStorageManager _storageManager = new VmwareStorageManager();

    @Autowired
    public StorageController(EventBusService eventBusService) {
        Assert.notNull(eventBusService, "An EventBusService must be set");
        _eventBusService = eventBusService;

        LOGGER.info("Register StorageController with event bus");
        _eventBusService.register(this);
    }

    @Subscribe
    @AllowConcurrentEvents
    public void handle(final ImportDiskImageEvent event) {
        _executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    final AsyncCallback callback = event.getCallback();

                    String vmxPath = _storageManager.importDiskImage(event.getContext(), event.getVmRefId(),
                            event.getDestImageName(), event.getDestStorage(), event.getNetwork(), event.getCustomProps(),
                            (callback == null) ? null : new ActionDelegate<Integer>() {
                                @Override
                                public void action(Integer param) {
                                    callback.progress(param);
                                }
                    });

                    if (callback != null) {
                        callback.onSuccess(vmxPath);
                    }
                } catch (Exception e) {
                    LOGGER.error("Failed to handle import image", e);

                    if (event.getCallback() != null) {
                        event.getCallback().onFailure(e);
                    }
                }
            }
        });
    }

    @Subscribe
    @AllowConcurrentEvents
    public void handle(final CopyDiskImageEvent event) {
        _executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    DiskImage diskImage = event.getSourceDiskImage();
                    final AsyncCallback callback = event.getCallback();
                    Pair<String, String> result;

                    if (diskImage.getType() == DiskImage.Type.ImageFullBase) {
                        result = _storageManager.copyDiskImageWithOvf(event.getSourceContext(), event.getSourceHost(),
                                event.getSourceStorage(), diskImage.getName(),
                                event.getDestContext(), event.getDestHost(), event.getDestStorage(),
                                event.getNetwork(), event.getCustomProps(),
                                event.getSrcVcContext(), event.getSrcHostEsxUuid(),
                                event.getDestVcContext(), event.getDestHostEsxUuid(),
                                (callback == null) ? null : new ActionDelegate<Integer>() {
                                    @Override
                                    public void action(Integer param) {
                                        callback.progress(param);
                                    }
                        });
                    } else {
                        result = _storageManager.copyDiskImage(event.getSourceContext(), event.getSourceStorage(),
                                diskImage.getName(), event.getDestContext(), event.getDestStorage(),
                                event.getNetwork(), event.getCustomProps(),
                                (callback == null) ? null : new ActionDelegate<Integer>() {
                                    @Override
                                    public void action(Integer param) {
                                        callback.progress(param);
                                    }
                        });
                    }

                    if (callback != null) {
                        callback.onSuccess(result);
                    }
                } catch (Exception e) {
                    LOGGER.error("Failed to handle copy image", e);

                    if (event.getCallback() != null) {
                        event.getCallback().onFailure(e);
                    }
                }
            }
        });
    }

    @Subscribe
    @AllowConcurrentEvents
    public void handle(final LinkedCloneImageEvent event) {
        _executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    DiskImage baseImage = event.getBaseImage();
                    final AsyncCallback callback = event.getCallback();

                    String vmxPath = _storageManager.linkedCloneImage(event.getContext(), baseImage.getName(),
                            event.getBaseStorage(), event.getCloneImageParentDir(), event.getCloneImageName(),
                            event.getCloneStorage(), event.getNetwork(), event.getCustomProps(),
                            (callback == null) ? null : new ActionDelegate<Integer>() {
                                @Override
                                public void action(Integer param) {
                                    callback.progress(param);
                                }
                    });

                    if (callback != null) {
                        callback.onSuccess(vmxPath);
                    }
                } catch (Exception e) {
                    LOGGER.error("Failed to handle linked clone image", e);

                    if (event.getCallback() != null) {
                        event.getCallback().onFailure(e);
                    }
                }
            }
        });
    }

    @Subscribe
    @AllowConcurrentEvents
    public void handle(final DeleteStorageDirectoryEvent event) {
        _executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    _storageManager.deleteDatastoreDirectory(event.getContext(), event.getFileOrDirPath(), event.getStorage());

                    if (event.getCallback() != null) {
                        event.getCallback().onSuccess(null);
                    }
                } catch (Exception e) {
                    LOGGER.error("Failed to handle delete storage directory", e);

                    if (event.getCallback() != null) {
                        event.getCallback().onFailure(e);
                    }
                }
            }
        });
    }

    @Subscribe
    @AllowConcurrentEvents
    public void handle(final AllocateStorageEvent event) {
        _executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    Storage storage = _storageManager.allocateStorage(
                            event.getContext(), event.getStorages(), event.getPurpose());

                    if (event.getCallback() != null) {
                        event.getCallback().onSuccess(storage);
                    }
                } catch (Exception e) {
                    LOGGER.error("Failed to handle allocate storage", e);

                    if (event.getCallback() != null) {
                        event.getCallback().onFailure(e);
                    }
                }
            }
        });
    }

    @Subscribe
    @AllowConcurrentEvents
    public void handle(final Link2LocalReplicaEvent event) {
        _executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    final AsyncCallback callback = event.getCallback();

                    String vmxPath = _storageManager.relinkTolocalReplica(event.getContext(), event.getBaseImage().getName(),
                            event.getBaseStorage(), event.getCloneImageParentDir(), event.getCloneImageName(),
                            event.getCloneStorage(), event.getNetwork(), event.getCustomProps(),
                            (callback == null) ? null : new ActionDelegate<Integer>() {
                                @Override
                                public void action(Integer param) {
                                    callback.progress(param);
                                }
                    });

                    if (callback != null) {
                        callback.onSuccess(vmxPath);
                    }
                } catch (Exception e) {
                    LOGGER.error("Failed to handle relink to local replica", e);

                    if (event.getCallback() != null) {
                        event.getCallback().onFailure(e);
                    }
                }
            }
        });
    }
}

