/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.storage.vmware;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;

import com.nimdesk.hypervisor.vmware.util.VmxDescriptorFile.VmxContentHandler;
import com.nimdesk.hypervisor.vmware.util.VmxDescriptorFile.VmxContentMergeHandler;
import com.vmware.vim25.FileBackedVirtualDiskSpec;
import com.vmware.vim25.VirtualDiskAdapterType;
import com.vmware.vim25.VirtualDiskSpec;
import com.vmware.vim25.VirtualDiskType;

/**
 * Package internal class that defines helper methods for VmwareStorageManager class.
 */
class VmwareStorageManagerHelper {
    //private static final Logger LOGGER = Logger.getLogger(VmwareStorageManagerHelper.class);

    public static VmxContentHandler createImportImageVmxContentHandler(final String srcVmName,
            final String destImageName, final String network, final Map<String, String> customProps) {
        return new VmxContentHandler() {
            private final HashMap<String, String> _replaceMap = createReplaceMap();
            private final ArrayList<String> _excludeList = createExcludeList();

            private HashMap<String, String> createReplaceMap() {
                HashMap<String, String> replaceMap  = new HashMap<String, String>();
                replaceMap.put("displayName = \"" + srcVmName + "\"", "displayName = \"" + destImageName + "\"");
                replaceMap.put("ConfigFile = \"" + srcVmName + ".vmxf\"", "ConfigFile = \"" + destImageName + ".vmxf\"");
                replaceMap.put(srcVmName + "-[0-9]*.vmdk", destImageName + ".vmdk");
                replaceMap.put(srcVmName + ".vmdk", destImageName + ".vmdk");
                replaceMap.put("addressType = \"vpx\"", "addressType = \"generated\"");
                replaceMap.put("independent[-a-z]*", "persistent");
                if (!StringUtils.isEmpty(network)) {
                    replaceMap.put("ethernet0.networkName = \"(.*)\"", "ethernet0.networkName = \"" + network + "\"");
                }
                return replaceMap;
            }

            private ArrayList<String> createExcludeList() {
                ArrayList<String> excludeList = new ArrayList<String>();
                excludeList.add("ethernet0.generatedAddress");
                excludeList.add("ethernet0.present");
                excludeList.add("ethernet0.startConnected");
                excludeList.add("sched.swap.derivedName");
                excludeList.add("uuid.action");
                excludeList.add("nvram");

                if (customProps != null) {
                    for (String key : customProps.keySet()) {
                        excludeList.add(key + " =");
                    }
                }

                return excludeList;
            }

            @Override
            public String processLine(String line) {
                for (String exclude : _excludeList) {
                    if (line.indexOf(exclude) >= 0) {
                        return null;
                    }
                }

                for (Entry<String, String> entry : _replaceMap.entrySet()) {
                    line = line.replaceAll(entry.getKey(), entry.getValue());
                }
                return line;
            }

            @Override
            public void postProcess(BufferedWriter writer) {
                try {
                    writer.write("uuid.action = \"create\"");
                    writer.newLine();

                    writer.write("ethernet0.present = \"true\"");
                    writer.newLine();

                    writer.write("ethernet0.startConnected = \"true\"");
                    writer.newLine();

                    if (customProps != null) {
                        for (Entry<String, String> entry : customProps.entrySet()) {
                            writer.write(entry.getKey() + " = \"" + entry.getValue() + "\"");
                            writer.newLine();
                        }
                    }
                } catch (IOException e) {
                    // Ignore
                }
            }
        };
    }

    public static VmxContentHandler createImportImageVmxfContentHandler(final String srcVmName,
            final String destImageName) {
        return new VmxContentHandler() {
            private final HashMap<String, String> _replaceMap = createReplaceMap();

            private HashMap<String, String> createReplaceMap() {
                HashMap<String, String> replaceMap  = new HashMap<String, String>();
                replaceMap.put(srcVmName, destImageName);
                return replaceMap;
            }

            @Override
            public String processLine(String line) {
                for (Entry<String, String> entry : _replaceMap.entrySet()) {
                    line = line.replaceAll(entry.getKey(), entry.getValue());
                }
                return line;
            }

            @Override
            public void postProcess(BufferedWriter writer) {
            }
        };
    }

    public static VirtualDiskSpec createImportImageDiskSpec() {
        FileBackedVirtualDiskSpec diskSpec = new FileBackedVirtualDiskSpec();
        diskSpec.setCapacityKb(100000);
        diskSpec.setDiskType(VirtualDiskType.thin.toString());
        diskSpec.setAdapterType(VirtualDiskAdapterType.lsiLogic.toString());
        return diskSpec;
    }

    public static VmxContentHandler createLinkedCloneImageVmxContentHandler(final String srcImageName,
            final String destImageName, final String network, final Map<String, String> customProps) {
        return new VmxContentHandler() {
            private final HashMap<String, String> _replaceMap = createReplaceMap();
            private final ArrayList<String> _excludeList = createExcludeList();

            private HashMap<String, String> createReplaceMap() {
                HashMap<String, String> replaceMap  = new HashMap<String, String>();
                if (!StringUtils.equals(srcImageName, destImageName)) {
                    replaceMap.put("displayName = \"" + srcImageName + "\"", "displayName = \"" + destImageName + "\"");
                    replaceMap.put("ConfigFile = \"" + srcImageName + ".vmxf\"", "ConfigFile = \"" + destImageName + ".vmxf\"");
                    replaceMap.put(srcImageName + ".nvram", destImageName + ".nvram");
                    replaceMap.put(srcImageName + "-[0-9]*.vmdk", destImageName + ".vmdk");
                    replaceMap.put(srcImageName + ".vmdk", destImageName + ".vmdk");
                }
                replaceMap.put("addressType = \"vpx\"", "addressType = \"generated\"");
                if (!StringUtils.isEmpty(network)) {
                    replaceMap.put("ethernet0.networkName = \"(.*)\"", "ethernet0.networkName = \"" + network + "\"");
                }
                return replaceMap;
            }

            private ArrayList<String> createExcludeList() {
                ArrayList<String> excludeList = new ArrayList<String>();
                excludeList.add("ethernet0.generatedAddress");
                excludeList.add("ethernet0.present");
                excludeList.add("ethernet0.startConnected");
                excludeList.add("sched.swap.derivedName");
                excludeList.add("uuid.action");

                if (customProps != null) {
                    for (String key : customProps.keySet()) {
                        excludeList.add(key + " =");
                    }
                }

                return excludeList;
            }

            @Override
            public String processLine(String line) {
                for (String exclude : _excludeList) {
                    if (line.indexOf(exclude) >= 0) {
                        return null;
                    }
                }

                for (Entry<String, String> entry : _replaceMap.entrySet()) {
                    line = line.replaceAll(entry.getKey(), entry.getValue());
                }
                return line;
            }

            @Override
            public void postProcess(BufferedWriter writer) {
                try {
                    writer.write("uuid.action = \"create\"");
                    writer.newLine();

                    writer.write("ethernet0.present = \"true\"");
                    writer.newLine();

                    writer.write("ethernet0.startConnected = \"true\"");
                    writer.newLine();

                    if (customProps != null) {
                        for (Entry<String, String> entry : customProps.entrySet()) {
                            writer.write(entry.getKey() + " = \"" + entry.getValue() + "\"");
                            writer.newLine();
                        }
                    }
                } catch (IOException e) {
                    // Ignore
                }
            }
        };
    }

    public static VmxContentHandler createLinkedCloneImageVmxfContentHandler(final String srcImageName,
            final String destImageName) {
        return new VmxContentHandler() {
            private final HashMap<String, String> _replaceMap = createReplaceMap();

            private HashMap<String, String> createReplaceMap() {
                HashMap<String, String> replaceMap  = new HashMap<String, String>();
                replaceMap.put(srcImageName, destImageName);
                return replaceMap;
            }

            @Override
            public String processLine(String line) {
                for (Entry<String, String> entry : _replaceMap.entrySet()) {
                    line = line.replaceAll(entry.getKey(), entry.getValue());
                }
                return line;
            }

            @Override
            public void postProcess(BufferedWriter writer) {
            }
        };
    }

    public static VmxContentHandler createLinkedCloneImageVmdkContentHandler(final String srcDsUrl,
            final String srcImageName, final String destImageName) {
        return new VmxContentHandler() {
            private final HashMap<String, String> _replaceMap = createReplaceMap();

            private HashMap<String, String> createReplaceMap() {
                HashMap<String, String> replaceMap  = new HashMap<String, String>();
                replaceMap.put(srcImageName + "-000001-delta.vmdk", destImageName + "-delta.vmdk");
                replaceMap.put(srcImageName + ".vmdk", String.format("%s/%s/%s.vmdk", srcDsUrl, srcImageName, srcImageName));
                return replaceMap;
            }

            @Override
            public String processLine(String line) {
                for (Entry<String, String> entry : _replaceMap.entrySet()) {
                    line = line.replaceAll(entry.getKey(), entry.getValue());
                }
                return line;
            }

            @Override
            public void postProcess(BufferedWriter writer) {
            }
        };
    }

    public static VmxContentMergeHandler createRebaseLinkedCloneImageVmxContentMergeHandler(final String srcImageName,
            final String destImageName, final Map<String, String> customProps) {
        return new VmxContentMergeHandler() {
            private final HashMap<String, String> _replaceMap = createReplaceMap();
            private final ArrayList<String> _excludeList = createExcludeList();

            private HashMap<String, String> createReplaceMap() {
                HashMap<String, String> replaceMap  = new HashMap<String, String>();
                replaceMap.put("displayName = \"" + srcImageName + "\"", "displayName = \"" + destImageName + "\"");
                replaceMap.put("ConfigFile = \"" + srcImageName + ".vmxf\"", "ConfigFile = \"" + destImageName + ".vmxf\"");
                replaceMap.put(srcImageName + ".nvram", destImageName + ".nvram");
                replaceMap.put(srcImageName + "-[0-9]*.vmdk", destImageName + ".vmdk");
                replaceMap.put(srcImageName + ".vmdk", destImageName + ".vmdk");
                return replaceMap;
            }

            private ArrayList<String> createExcludeList() {
                ArrayList<String> excludeList = new ArrayList<String>();
                excludeList.add("ethernet0.generatedAddress");
                excludeList.add("ethernet0.present");
                excludeList.add("ethernet0.startConnected");
                excludeList.add("sched.swap.derivedName");
                excludeList.add("uuid.action");
                excludeList.add("nvram");

                if (customProps != null) {
                    for (String key : customProps.keySet()) {
                        excludeList.add(key + " =");
                    }
                }

                return excludeList;
            }

            @Override
            public String processLine(String line) {
                for (String exclude : _excludeList) {
                    if (line.startsWith(exclude)) {
                        return null;
                    }
                }

                for (Entry<String, String> entry : _replaceMap.entrySet()) {
                    line = line.replaceAll(entry.getKey(), entry.getValue());
                }
                return line;
            }

            @Override
            public String processMergeLine(String line) {
                for (String exclude : _excludeList) {
                    if (line.startsWith(exclude)) {
                        return line;
                    }
                }

            	return null;
            }

            @Override
            public void postProcess(BufferedWriter writer) {
                try {
                    writer.write("uuid.action = \"create\"");
                    writer.newLine();

                    writer.write("ethernet0.present = \"true\"");
                    writer.newLine();

                    writer.write("ethernet0.startConnected = \"true\"");
                    writer.newLine();

                    if (customProps != null) {
                        for (Entry<String, String> entry : customProps.entrySet()) {
                            writer.write(entry.getKey() + " = \"" + entry.getValue() + "\"");
                            writer.newLine();
                        }
                    }
                } catch (IOException e) {
                    // Ignore
                }
            }
        };
    }

    public static VmxContentMergeHandler createRelinkTolocalReplicaVmxContentMergeHandler(final String network,
            final Map<String, String> customProps) {
        return new VmxContentMergeHandler() {
            private final HashMap<String, String> _replaceMap = createReplaceMap();
            private final ArrayList<String> _excludeList = createExcludeList();

            private HashMap<String, String> createReplaceMap() {
                HashMap<String, String> replaceMap  = new HashMap<String, String>();
                if (!StringUtils.isEmpty(network)) {
                    replaceMap.put("ethernet0.networkName = \"(.*)\"", "ethernet0.networkName = \"" + network + "\"");
                }
                return replaceMap;
            }

            private ArrayList<String> createExcludeList() {
                ArrayList<String> excludeList = new ArrayList<String>();
                excludeList.add("ethernet0.generatedAddress");
                excludeList.add("ethernet0.present");
                excludeList.add("ethernet0.startConnected");
                excludeList.add("sched.swap.derivedName");
                excludeList.add("uuid.action");

                if (customProps != null) {
                    for (String key : customProps.keySet()) {
                        excludeList.add(key + " =");
                    }
                }

                return excludeList;
            }

            @Override
            public String processLine(String line) {
                for (String exclude : _excludeList) {
                    if (line.startsWith(exclude)) {
                        return null;
                    }
                }

                for (Entry<String, String> entry : _replaceMap.entrySet()) {
                    line = line.replaceAll(entry.getKey(), entry.getValue());
                }
                return line;
            }

            @Override
            public String processMergeLine(String line) {
                for (String exclude : _excludeList) {
                    if (line.startsWith(exclude)) {
                        return line;
                    }
                }

                return null;
            }

            @Override
            public void postProcess(BufferedWriter writer) {
                try {
                    writer.write("uuid.action = \"create\"");
                    writer.newLine();

                    writer.write("ethernet0.present = \"true\"");
                    writer.newLine();

                    writer.write("ethernet0.startConnected = \"true\"");
                    writer.newLine();

                    if (customProps != null) {
                        for (Entry<String, String> entry : customProps.entrySet()) {
                            writer.write(entry.getKey() + " = \"" + entry.getValue() + "\"");
                            writer.newLine();
                        }
                    }
                } catch (IOException e) {
                    // Ignore
                }
            }
        };
    }

    public static VmxContentHandler createRelinkTolocalReplicaVmdkContentHandler(final String localDsUrl,
            final String localImageName, final String parentCID) {
        return new VmxContentHandler() {
            private final HashMap<String, String> _replaceMap = createReplaceMap();

            private HashMap<String, String> createReplaceMap() {
                HashMap<String, String> replaceMap  = new HashMap<String, String>();
                if (!StringUtils.isEmpty(parentCID)) {
                    replaceMap.put("^parentCID.*", String.format("parentCID=%s", parentCID));
                }
                replaceMap.put("^parentFileNameHint.*", String.format("parentFileNameHint=\"%s/%s/%s.vmdk\"", localDsUrl, localImageName, localImageName));
                return replaceMap;
            }

            @Override
            public String processLine(String line) {
                for (Entry<String, String> entry : _replaceMap.entrySet()) {
                    line = line.replaceAll(entry.getKey(), entry.getValue());
                }
                return line;
            }

            @Override
            public void postProcess(BufferedWriter writer) {
            }
        };
    }
}

