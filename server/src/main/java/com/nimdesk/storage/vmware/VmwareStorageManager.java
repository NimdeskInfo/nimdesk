/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.storage.vmware;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.util.Assert;

import com.nimdesk.database.vo.HostVO;
import com.nimdesk.database.vo.StorageVO;
import com.nimdesk.exception.BaseException;
import com.nimdesk.exception.HostConnectionException;
import com.nimdesk.exception.storage.BrokenImageException;
import com.nimdesk.exception.storage.StorageException;
import com.nimdesk.hypervisor.HypervisorContext;
import com.nimdesk.hypervisor.vmware.VmwareContext;
import com.nimdesk.hypervisor.vmware.mo.DatacenterMO;
import com.nimdesk.hypervisor.vmware.mo.DatastoreMO;
import com.nimdesk.hypervisor.vmware.mo.HostMO;
import com.nimdesk.hypervisor.vmware.mo.VirtualDiskManagerMO;
import com.nimdesk.hypervisor.vmware.mo.VirtualMachineMO;
import com.nimdesk.hypervisor.vmware.util.DatastoreFile;
import com.nimdesk.hypervisor.vmware.util.VmdkDescriptorFile;
import com.nimdesk.hypervisor.vmware.util.VmwareUtils;
import com.nimdesk.hypervisor.vmware.util.VmxDescriptorFile;
import com.nimdesk.model.Host;
import com.nimdesk.model.Storage;
import com.nimdesk.util.ActionDelegate;
import com.nimdesk.util.Pair;
import com.nimdesk.util.Utils;
import com.vmware.vim25.FileFault;
import com.vmware.vim25.MethodFault;
import com.vmware.vim25.ObjectContent;

/**
 *
 */
public class VmwareStorageManager {
    private static final Logger LOGGER = Logger.getLogger(VmwareStorageManager.class);

    /*
     * Make a file system copy of an existing VM (the VM is not registered)
     *
     * @param ctx the hypervisor context
     *
     * @param vmRefId the source VM's refId (the string value of the MOR)
     *
     * @param destStorage the storage for new clone VM
     *
     * @param destImageName the name of the new clone VM
     *
     * @param network the network name
     *
     * @param customProps customization properties
     *
     * @return the cloned VM's full path name (i.e. "[<ds>] path/vmName.vmx")
     */
    public String importDiskImage(HypervisorContext ctx, String vmRefId, String destImageName,
            Storage destStorage, String network, Map<String, String> customProps,
            ActionDelegate<Integer> progressUpdater)
                    throws BaseException {
        Assert.isInstanceOf(VmwareContext.class, ctx, "Invalid hypervisor context - expect Vmware");
        Assert.hasText(vmRefId, "Empty VM refId");
        Assert.hasText(destImageName, "Empty destination image name");
        Assert.notNull(destStorage, "Null storage specified for image");

        LOGGER.info(String.format("Importing disk image from VM \"%s\" to %s...", vmRefId, destImageName));

        if (progressUpdater != null) {
            progressUpdater.action(0);
        }

        File tempDir = null;

        try {
            VmwareContext context = (VmwareContext) ctx;
            VirtualMachineMO vm = new VirtualMachineMO(context, vmRefId);
            String vmxPath = vm.getVmxPath();

            tempDir = Utils.createTempDirectory();
            LOGGER.debug(String.format("importDiskImage: Created local temp dir \"%s\"", tempDir.getPath()));

            if (progressUpdater != null) {
                progressUpdater.action(2);
            }

            VirtualDiskManagerMO vdm = context.getVirtualDiskManager();

            DatastoreMO datastore = new DatastoreMO(context, destStorage.getRefId(), destStorage.getName());
            String destDsName = datastore.getName();

            DatacenterMO datacenter = context.getOwnerDatacenter(vm.getMor());

            DatastoreFile srcVmxDsFile = new DatastoreFile(vmxPath);

            DatastoreFile srcVmDsDir = new DatastoreFile(srcVmxDsFile.getDirFullPath());
            String srcVmName = srcVmxDsFile.getFileBaseName();
            DatastoreFile destImageDsDir = new DatastoreFile(destDsName, destImageName);

            // Make remote image dir
            LOGGER.debug(String.format("importDiskImage: Make remote image dir \"%s\"", destImageDsDir.getFullPath()));
            datastore.makeDirectory(destImageDsDir.getFullPath(), datacenter);

            if (progressUpdater != null) {
                progressUpdater.action(5);
            }

            File tempVmxFile = new File(tempDir.getPath(), srcVmName + ".vmx");
            File tempDestVmxFile = new File(tempDir.getPath(), destImageName + ".vmx.temp");
            DatastoreFile destImageVmxDsFile = new DatastoreFile(destImageDsDir.getFullPath()
                    + "/" + destImageName + ".vmx");

            // Get remote source image .vmx to local temp dir
            LOGGER.debug(String.format("importDiskImage: Get remote vmx \"%s\" to local \"%s\"",
                    srcVmxDsFile.getFullPath(), tempVmxFile.getPath()));
            try {
                datacenter.getDatastoreFile(srcVmxDsFile.getFullPath(), tempVmxFile.getPath());
            } catch (FileNotFoundException e) {
                LOGGER.error(String.format("importDiskImage: Failed to get remote vmx \"%s\" to local \"%s\"",
                        srcVmxDsFile.getFullPath(), tempVmxFile.getPath()), e);
                throw new BrokenImageException("Invalid disk image", e);
            }

            // Modify local temp source image .vmx to local temp dest image .vmx
            LOGGER.debug(String.format("importDiskImage: Modify vmx \"%s\" to \"%s\"",
                    tempVmxFile.getPath(), tempDestVmxFile.getPath()));
            VmxDescriptorFile.copy(tempVmxFile, tempDestVmxFile,
                    VmwareStorageManagerHelper.createImportImageVmxContentHandler(srcVmName,
                            destImageName, network, customProps));

            // Put modified local image .vmx to remote dir
            LOGGER.debug(String.format("importDiskImage: Put local vmx \"%s\" to remote \"%s\"",
                    tempDestVmxFile.getPath(), destImageVmxDsFile.getFullPath()));
            datacenter.putDatastoreFile(destImageVmxDsFile.getFullPath(), tempDestVmxFile.getPath());

            DatastoreFile srcVmxfDsFile = new DatastoreFile(srcVmDsDir.getFullPath() + "/" + srcVmName + ".vmxf");
            File tempVmxfFile = new File(tempDir.getPath(), srcVmName + ".vmxf");
            File tempDestVmxfFile = new File(tempDir.getPath(), destImageName + ".vmxf.temp");
            DatastoreFile destImageVmxfDsFile = new DatastoreFile(destImageDsDir.getFullPath()
                    + "/" + destImageName + ".vmxf");

            // Get remote source image .vmxf to local temp dir
            LOGGER.debug(String.format("importDiskImage: Get remote vmxf \"%s\" to local \"%s\"",
                    srcVmxfDsFile.getFullPath(), tempVmxfFile.getPath()));
            try {
                datacenter.getDatastoreFile(srcVmxfDsFile.getFullPath(), tempVmxfFile.getPath());

                // Modify local temp source image .vmxf to local temp dest image .vmxf
                LOGGER.debug(String.format("importDiskImage: Modify vmxf \"%s\" to \"%s\"",
                        tempVmxfFile.getPath(), tempDestVmxfFile.getPath()));
                VmxDescriptorFile.copy(tempVmxfFile, tempDestVmxfFile,
                        VmwareStorageManagerHelper.createImportImageVmxfContentHandler(srcVmName, destImageName));

                // Put modified local image .vmxf to remote dir
                LOGGER.debug(String.format("importDiskImage: Put local vmxf \"%s\" to remote \"%s\"",
                        tempDestVmxfFile.getPath(), destImageVmxfDsFile.getFullPath()));
                datacenter.putDatastoreFile(destImageVmxfDsFile.getFullPath(), tempDestVmxfFile.getPath());
            } catch (Exception e) {
                LOGGER.warn(String.format("importDiskImage: Failed to copy vmxf \"%s\" - it's not necessarily an error",
                    srcVmxfDsFile.getFullPath()));
            }

            if (progressUpdater != null) {
                progressUpdater.action(10);
            }

            String srcVmdkDsFileName = vm.getVirtualDiskDsFileName();
            if (srcVmdkDsFileName == null) {
               throw new IOException("The VM has no virtual disk");
            }

            DatastoreFile srcVmdkDsFile = new DatastoreFile(srcVmdkDsFileName);
            DatastoreFile destImageVmdkDsFile = new DatastoreFile(destImageDsDir.getFullPath()
                    + "/" + destImageName + ".vmdk");

            LOGGER.debug(String.format("importDiskImage: Copy vmdk \"%s\" to \"%s\"",
                    srcVmdkDsFile.getFullPath(), destImageVmdkDsFile.getFullPath()));
            try {
                // Convert progress range [0, 100] to [10, 100]
                if (!vdm.copyVirtualDisk(srcVmdkDsFile.getFullPath(), datacenter, destImageVmdkDsFile.getFullPath(),
                        datacenter, VmwareStorageManagerHelper.createImportImageDiskSpec(), true,
                        Utils.wrapProgressUpdater(progressUpdater, 10, 100))) {
                    throw new IOException("Failed to copy virtual disk");
                }
            } catch (FileFault e) {
                LOGGER.error(String.format("importDiskImage: Failed to copy vmdk \"%s\" to \"%s\"",
                        srcVmdkDsFile.getFullPath(), destImageVmdkDsFile.getFullPath()), e);
                throw new BrokenImageException("Invalid disk image", e);
            }

            LOGGER.info(String.format("Imported disk image from VM \"%s\" to %s.", vmRefId, destImageName));

            return destImageVmxDsFile.getFullPath();
        } catch (MethodFault e) {
            LOGGER.error(String.format("Failed to import disk image from VM \"%s\" to %s", vmRefId, destImageName), e);
            throw new HostConnectionException("Failed to call VMware SDK", e);
        } catch (RemoteException e) {
            LOGGER.error(String.format("Failed to import disk image from VM \"%s\" to %s", vmRefId, destImageName), e);
            throw new HostConnectionException("Failed to connect to host", e);
        } catch (IOException e) {
            LOGGER.error(String.format("Failed to import disk image from VM \"%s\" to %s", vmRefId, destImageName), e);
            throw new StorageException("Invalid disk image", e);
        } finally {
            if (tempDir != null) {
                try {
                    FileUtils.deleteDirectory(tempDir);
                } catch (IOException e) {
                    LOGGER.warn("importDiskImage: Failed to delete temp directory", e);
                }
            }
        }
    }

    /*
     * Copy disk image from one host to another host
     *
     * @param ctx the hypervisor context
     *
     * @param vmRefId the source VM's refId (the string value of the MOR)
     *
     * @param destStorage the storage for new clone VM
     *
     * @param destImageName the name of the new clone VM
     *
     * @param network the network name
     *
     * @param customProps customization properties
     *
     * @return the cloned VM's full path name (i.e. "[<ds>] path/vmName.vmx")
     */
    public Pair<String, String> copyDiskImage(HypervisorContext srcCtx, Storage srcStorage, String imageName,
            HypervisorContext destCtx, Storage destStorage, String network, Map<String, String> customProps,
            ActionDelegate<Integer> progressUpdater)
                    throws BaseException {
        Assert.isInstanceOf(VmwareContext.class, srcCtx, "Invalid source hypervisor context - expect Vmware");
        Assert.isInstanceOf(VmwareContext.class, destCtx, "Invalid dest hypervisor context - expect Vmware");
        Assert.hasText(imageName, "Empty image name");
        Assert.notNull(srcStorage, "Null source storage specified for image");
        Assert.notNull(destStorage, "Null destination storage specified for image");

        LOGGER.info(String.format("Copying disk image \"%s\" from \"%s\" to \"%s\"...",
                imageName, srcCtx.getServerAddress(), destCtx.getServerAddress()));

        if (progressUpdater != null) {
            progressUpdater.action(0);
        }

        File tempDir = null;
        Timer timer = null;

        try {
            final VmwareContext srcContext = (VmwareContext) srcCtx;
            final VmwareContext destContext = (VmwareContext) destCtx;

            tempDir = Utils.createTempDirectory();
            LOGGER.debug(String.format("copyDiskImage: Created local temp dir \"%s\"", tempDir.getPath()));

            if (progressUpdater != null) {
                progressUpdater.action(2);
            }

            DatastoreMO srcDatastore = new DatastoreMO(srcContext, srcStorage.getRefId(), srcStorage.getName());
            String srcDsName = srcDatastore.getName();

            DatastoreMO destDatastore = new DatastoreMO(destContext, destStorage.getRefId(), destStorage.getName());
            String destDsName = destDatastore.getName();

            DatacenterMO srcDatacenter = srcContext.getOwnerDatacenter(srcDatastore.getMor());
            DatacenterMO destDatacenter = destContext.getOwnerDatacenter(destDatastore.getMor());

            DatastoreFile srcImageDsDir = new DatastoreFile(srcDsName, imageName);
            DatastoreFile destImageDsDir = new DatastoreFile(destDsName, imageName);

            // Try to delete remote image dir first
            boolean dirExists = true; // Try to delete if error.
            try {
                dirExists = destDatastore.fileExists(destImageDsDir.getFullPath(), null);
            } catch (Exception e) {
                // Ignore error.
            }
            if (dirExists) {
                LOGGER.debug(String.format("copyDiskImage: Delete remote image dir \"%s\"", destImageDsDir.getFullPath()));
                destDatastore.deleteDatastoreFile(destImageDsDir.getFullPath(), destDatacenter, null);
            }

            // Make remote image dir
            LOGGER.debug(String.format("copyDiskImage: Make remote image dir \"%s\"", destImageDsDir.getFullPath()));
            destDatastore.makeDirectory(destImageDsDir.getFullPath(), destDatacenter);

            if (progressUpdater != null) {
                progressUpdater.action(5);
            }

            String srcImageVmxFile = imageName + ".vmx";
            DatastoreFile srcImageVmxDsFile = new DatastoreFile(srcImageDsDir.getFullPath() + "/" + srcImageVmxFile);
            File tempVmxFile = new File(tempDir.getPath(), srcImageVmxFile);
            File tempDestVmxFile = new File(tempDir.getPath(), imageName + ".vmx.temp");
            DatastoreFile destImageVmxDsFile = new DatastoreFile(destImageDsDir.getFullPath()
                    + "/" + imageName + ".vmx");

            // Get remote source image .vmx to local temp dir
            LOGGER.debug(String.format("copyDiskImage: Get remote vmx \"%s\" to local \"%s\"",
                    srcImageVmxDsFile.getFullPath(), tempVmxFile.getPath()));
            try {
                srcDatacenter.getDatastoreFile(srcImageVmxDsFile.getFullPath(), tempVmxFile.getPath());
            } catch (FileNotFoundException e) {
                LOGGER.error(String.format("copyDiskImage: Failed to get remote vmx \"%s\" to local \"%s\"",
                        srcImageVmxDsFile.getFullPath(), tempVmxFile.getPath()), e);
                throw new BrokenImageException("Invalid disk image", e);
            }

            // Modify local temp source image .vmx to local temp dest image .vmx
            LOGGER.debug(String.format("copyDiskImage: Modify vmx \"%s\" to \"%s\"",
                    tempVmxFile.getPath(), tempDestVmxFile.getPath()));
            VmxDescriptorFile.copy(tempVmxFile, tempDestVmxFile,
                    VmwareStorageManagerHelper.createImportImageVmxContentHandler(imageName,
                            imageName, network, customProps));

            // Put modified local image .vmx to remote dir
            LOGGER.debug(String.format("copyDiskImage: Put local vmx \"%s\" to remote \"%s\"",
                    tempDestVmxFile.getPath(), destImageVmxDsFile.getFullPath()));
            destDatacenter.putDatastoreFile(destImageVmxDsFile.getFullPath(), tempDestVmxFile.getPath());

            String srcImageVmxfFile = imageName + ".vmxf";
            DatastoreFile srcImageVmxfDsFile = new DatastoreFile(srcImageDsDir.getFullPath() + "/" + srcImageVmxfFile);
            File tempVmxfFile = new File(tempDir.getPath(), srcImageVmxfFile);
            File tempDestVmxfFile = new File(tempDir.getPath(), imageName + ".vmxf.temp");
            DatastoreFile destImageVmxfDsFile = new DatastoreFile(destImageDsDir.getFullPath()
                    + "/" + imageName + ".vmxf");

            // Get remote source image .vmxf to local temp dir
            LOGGER.debug(String.format("copyDiskImage: Get remote vmxf \"%s\" to local \"%s\"",
                    srcImageVmxfDsFile.getFullPath(), tempVmxfFile.getPath()));
            try {
                srcDatacenter.getDatastoreFile(srcImageVmxfDsFile.getFullPath(), tempVmxfFile.getPath());

                // Modify local temp source image .vmxf to local temp dest image .vmxf
                LOGGER.debug(String.format("copyDiskImage: Modify vmxf \"%s\" to \"%s\"",
                        tempVmxfFile.getPath(), tempDestVmxfFile.getPath()));
                VmxDescriptorFile.copy(tempVmxfFile, tempDestVmxfFile,
                        VmwareStorageManagerHelper.createImportImageVmxfContentHandler(imageName, imageName));

                // Put modified local image .vmxf to remote dir
                LOGGER.debug(String.format("copyDiskImage: Put local vmxf \"%s\" to remote \"%s\"",
                        tempDestVmxfFile.getPath(), destImageVmxfDsFile.getFullPath()));
                destDatacenter.putDatastoreFile(destImageVmxfDsFile.getFullPath(), tempDestVmxfFile.getPath());
            } catch (Exception e) {
                LOGGER.warn(String.format("copyDiskImage: Failed to copy vmxf \"%s\" - it's not necessarily an error",
                        srcImageVmxfDsFile.getFullPath()));
            }

            if (progressUpdater != null) {
                progressUpdater.action(10);
            }

            String[] dirContent = srcDatacenter.listDatastoreDirContent(srcImageDsDir.getFullPath());
            if (dirContent == null) {
                throw new IOException(String.format("Empty source image directory \"%s\"", srcImageDsDir.getFullPath()));
            }

            List<String> largeFiles = new ArrayList<String>();

            for (String fileName : dirContent) {
                if (StringUtils.endsWith(fileName, ".vmx")
                        || StringUtils.endsWith(fileName, ".vmxf")
                        || StringUtils.endsWith(fileName, ".log")
                        || StringUtils.endsWith(fileName, ".vswp")) {
                    // Copied in the code above. or don't need to copy log files.
                    continue;
                }
                if (StringUtils.endsWith(fileName, "-flat.vmdk")
                        || StringUtils.endsWith(fileName, "-delta.vmdk")) {
                    // Large file, copy them last since VmwareContext could time out during copying.
                    largeFiles.add(fileName);
                    continue;
                }

                DatastoreFile srcDsFilePath = new DatastoreFile(srcImageDsDir.getFullPath() + "/" + fileName);
                DatastoreFile destDsFilePath = new DatastoreFile(destImageDsDir.getFullPath() + "/" + fileName);

                LOGGER.debug(String.format("copyDiskImage: Start to copy file \"%s\" to \"%s\"",
                        srcDsFilePath.getFullPath(), destDsFilePath.getFullPath()));

                srcDatacenter.copyDatastoreFile(srcDsFilePath.getFullPath(),
                        destDatacenter, destDsFilePath.getFullPath(), -1, null);

                LOGGER.debug(String.format("copyDiskImage: Finished to copy file \"%s\" to \"%s\"",
                        srcDsFilePath.getFullPath(), destDsFilePath.getFullPath()));
            }

            if (progressUpdater != null) {
                progressUpdater.action(30);
            }

            if (!largeFiles.isEmpty()) {
                timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        // Keep ESX sessions alive.
                        try {
                            srcContext.getServiceInstance().getServerClock();
                        } catch (Exception e) {
                            LOGGER.warn(String.format("Failed to ping source host \"%s\"", srcContext.getServerAddress()), e);
                        }
                        try {
                            destContext.getServiceInstance().getServerClock();
                        } catch (Exception e) {
                            LOGGER.warn(String.format("Failed to ping destination host \"%s\"", destContext.getServerAddress()), e);
                        }
                    }
                }, 120 * 1000, 120 * 1000);

                for (String fileName : largeFiles) {
                    DatastoreFile srcDsFilePath = new DatastoreFile(srcImageDsDir.getFullPath() + "/" + fileName);
                    DatastoreFile destDsFilePath = new DatastoreFile(destImageDsDir.getFullPath() + "/" + fileName);

                    long fileSize = -1;
                    try {
                        fileSize = srcDatastore.getFileSize(srcDsFilePath.getFullPath(), false, null);
                    } catch (Exception e) {
                        // Ignore error
                        LOGGER.warn(String.format("Failed to get size for file \"%s\"", srcDsFilePath.getFullPath()));
                    }

                    LOGGER.debug(String.format("copyDiskImage: Start to copy file \"%s\" to \"%s\" - size %d",
                            srcDsFilePath.getFullPath(), destDsFilePath.getFullPath(), fileSize));

                    srcDatacenter.copyDatastoreFile(srcDsFilePath.getFullPath(),
                            destDatacenter, destDsFilePath.getFullPath(), fileSize,
                            Utils.wrapProgressUpdater(progressUpdater, 30, 100));

                    LOGGER.debug(String.format("copyDiskImage: Finished to copy file \"%s\" to \"%s\"",
                            srcDsFilePath.getFullPath(), destDsFilePath.getFullPath()));
                }
            }

            return new Pair<String, String>(null, destImageVmxDsFile.getFullPath());
        } catch (MethodFault e) {
            LOGGER.error(String.format("Failed to copy disk image \"%s\" from \"%s\" to \"%s\"",
                    imageName, srcCtx.getServerAddress(), destCtx.getServerAddress()));
            throw new HostConnectionException("Failed to call VMware SDK", e);
        } catch (RemoteException e) {
            LOGGER.error(String.format("Failed to copy disk image \"%s\" from \"%s\" to \"%s\"",
                    imageName, srcCtx.getServerAddress(), destCtx.getServerAddress()));
            throw new HostConnectionException("Failed to connect to host", e);
        } catch (IOException e) {
            LOGGER.error(String.format("Failed to copy disk image \"%s\" from \"%s\" to \"%s\"",
                    imageName, srcCtx.getServerAddress(), destCtx.getServerAddress()));
            throw new StorageException("Invalid disk image", e);
        } finally {
            if (tempDir != null) {
                try {
                    FileUtils.deleteDirectory(tempDir);
                } catch (IOException e) {
                    LOGGER.warn("copyDiskImage: Failed to delete temp directory", e);
                }
            }

            if (timer != null) {
                timer.cancel();
            }
        }
    }

    /*
     * Copy disk image from one host to another host with ovf
     *
     * @param ctx the hypervisor context
     *
     * @param vmRefId the source VM's refId (the string value of the MOR)
     *
     * @param destStorage the storage for new clone VM
     *
     * @param destImageName the name of the new clone VM
     *
     * @param network the network name
     *
     * @param customProps customization properties
     *
     * @return the cloned VM's full path name (i.e. "[<ds>] path/vmName.vmx")
     */
    public Pair<String, String> copyDiskImageWithOvf(HypervisorContext srcCtx, Host srcHost, Storage srcStorage, String imageName,
            HypervisorContext destCtx, Host destHost, Storage destStorage, String network, Map<String, String> customProps,
            HypervisorContext srcVcCtx, String srcHostEsxUuid, HypervisorContext destVcCtx, String destHostEsxUuid,
            ActionDelegate<Integer> progressUpdater)
                    throws Exception {
        Assert.isInstanceOf(VmwareContext.class, srcCtx, "Invalid source hypervisor context - expect Vmware");
        Assert.isInstanceOf(VmwareContext.class, destCtx, "Invalid dest hypervisor context - expect Vmware");
        Assert.hasText(imageName, "Empty image name");
        Assert.notNull(srcHost, "Null source host specified for image");
        Assert.notNull(srcStorage, "Null source storage specified for image");
        Assert.notNull(destHost, "Null destination host specified for image");
        Assert.notNull(destStorage, "Null destination storage specified for image");

        LOGGER.info(String.format("Copying (ovf) disk image \"%s\" from \"%s\" to \"%s\"...",
                imageName, srcCtx.getServerAddress(), destCtx.getServerAddress()));

        if (progressUpdater != null) {
            progressUpdater.action(0);
        }

        File tempDir = null;
        Timer timer = null;

        try {
            final VmwareContext srcContext = (VmwareContext) srcCtx;
            final VmwareContext destContext = (VmwareContext) destCtx;

            tempDir = Utils.createTempDirectory();
            LOGGER.debug(String.format("copyDiskImageWithOvf: Created local temp dir \"%s\"", tempDir.getPath()));

            if (progressUpdater != null) {
                progressUpdater.action(2);
            }

            DatastoreMO srcDatastore = new DatastoreMO(srcContext, srcStorage.getRefId(), srcStorage.getName());
            String srcDsName = srcDatastore.getName();

            DatastoreMO destDatastore = new DatastoreMO(destContext, destStorage.getRefId(), destStorage.getName());
            String destDsName = destDatastore.getName();

            HostMO srcHostMO = new HostMO(srcContext, srcHost.getRefId());
            HostMO destHostMO = new HostMO(destContext, destHost.getRefId());

            DatacenterMO destDatacenter = destContext.getOwnerDatacenter(srcHostMO.getMor());

            DatastoreFile srcImageDsDir = new DatastoreFile(srcDsName, imageName);
            DatastoreFile destImageDsDir = new DatastoreFile(destDsName, imageName);

            // Try to delete remote image dir first
            boolean dirExists = true; // Try to delete if error.
            try {
                dirExists = destDatastore.fileExists(destImageDsDir.getFullPath(), null);
            } catch (Exception e) {
                // Ignore error.
            }
            if (dirExists) {
                LOGGER.debug(String.format("copyDiskImageWithOvf: Delete remote image dir \"%s\"", destImageDsDir.getFullPath()));
                destDatastore.deleteDatastoreFile(destImageDsDir.getFullPath(), destDatacenter, null);
            }

            String srcImageVmxFile = imageName + ".vmx";
            DatastoreFile srcImageVmxDsFile = new DatastoreFile(srcImageDsDir.getFullPath() + "/" + srcImageVmxFile);
            DatastoreFile destImageVmxDsFile = new DatastoreFile(destImageDsDir.getFullPath()
                    + "/" + imageName + ".vmx");

            VmwareContext srcVcContext = null;
            HostMO srcVcHostMO = null;
            if (srcVcCtx instanceof VmwareContext && !StringUtils.isEmpty(srcHostEsxUuid)) {
                srcVcContext = (VmwareContext) srcVcCtx;
                srcVcHostMO = HostMO.findByEsxUuid(srcVcContext, srcHostEsxUuid);
            }

            VmwareContext destVcContext = null;

            String srcImageVmxPath = srcImageVmxDsFile.getFullPath();

            // Register the source image first.
            boolean vmRegistered;
            if (srcVcHostMO != null) {
                vmRegistered = srcVcHostMO.registerVmOnHost(imageName, srcImageVmxDsFile.getFullPath(), true, null);
            } else {
                vmRegistered = srcHostMO.registerVmOnHost(imageName, srcImageVmxDsFile.getFullPath(), true, null);
            }
            if (!vmRegistered) {
                LOGGER.warn(String.format("Failed to register disk image \"%s\"(%s) on host \"%s\". Check if it's been registered.",
                        imageName, srcImageVmxDsFile.getFullPath(), srcHost.getAddress()));
            }

            if (progressUpdater != null) {
                progressUpdater.action(5);
            }

            // Find the VM
            VirtualMachineMO srcImageVm = null;
            ObjectContent[] ocs = srcHostMO.getVmProperties(new String[] {"summary.config.vmPathName"});
            if (ocs != null) {
                for (ObjectContent oc : ocs) {
                    String vmxPath = (String) VmwareUtils.getPropValue(oc, "summary.config.vmPathName");
                    if (StringUtils.equals(vmxPath, srcImageVmxPath)) {
                        srcImageVm = new VirtualMachineMO(srcContext, oc.getObj());
                        break;
                    }
                }
            }

            if (srcImageVm == null) {
                LOGGER.error(String.format("Unable to find the registered disk image \"%s\" on host \"%s\"",
                        imageName, srcHost.getAddress()));
                throw new BrokenImageException("Unable to find the registered image");
            }

            if (progressUpdater != null) {
                progressUpdater.action(10);
            }

            final VmwareContext tmpSrcVcContext = srcVcContext;
            final VmwareContext tmpDestVcContext = destVcContext;

            timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                @SuppressWarnings("unused")
                public void run() {
                    // Keep ESX sessions alive.
                    try {
                        srcContext.getServiceInstance().getServerClock();
                    } catch (Exception e) {
                        LOGGER.warn(String.format("Failed to ping source host \"%s\"", srcContext.getServerAddress()), e);
                    }
                    try {
                        destContext.getServiceInstance().getServerClock();
                    } catch (Exception e) {
                        LOGGER.warn(String.format("Failed to ping destination host \"%s\"", destContext.getServerAddress()), e);
                    }

                    // Keep VC sessions alive.
                    if (tmpSrcVcContext != null) {
                        try {
                            tmpSrcVcContext.getServiceInstance().getServerClock();
                        } catch (Exception e) {
                            LOGGER.warn(String.format("Failed to ping source vCenter \"%s\"", tmpSrcVcContext.getServerAddress()), e);
                        }
                    }
                    if (tmpDestVcContext != null) {
                        try {
                            tmpDestVcContext.getServiceInstance().getServerClock();
                        } catch (Exception e) {
                            LOGGER.warn(String.format("Failed to ping destination vCenter \"%s\"", tmpDestVcContext.getServerAddress()), e);
                        }
                    }
                }
            }, 120 * 1000, 120 * 1000);

            try {
                LOGGER.info(String.format("copyDiskImageWithOvf: Exporting disk image to %s/%s...", tempDir.getPath(), imageName));
                if (!srcImageVm.exportVm(tempDir.getPath(), imageName,
                        Utils.wrapProgressUpdater(progressUpdater, 10, 50))) {
                    LOGGER.error(String.format("copyDiskImageWithOvf: Failed to export disk image to %s/%s", tempDir.getPath(), imageName));
                    throw new IOException(String.format("Failed to export image \"%s\"", imageName));
                }
            } finally {
                // Unregister VM.
                if (srcVcContext == null || srcVcHostMO == null
                        || !doUnregisterVmOnVcHost(srcImageVm, srcVcContext, srcVcHostMO)) {
                    try {
                        // Unregister on host directly.
                        srcImageVm.unregister();
                    } catch (Exception e) {
                        LOGGER.warn(String.format("copyDiskImageWithOvf: Failed to unregister disk image \"%s\" on host \"%s\"",
                                imageName, srcHost.getAddress()));
                    }
                }
            }

            LOGGER.info(String.format("copyDiskImageWithOvf: Importing ovf \"%s/%s\" to host \"%s\"...",
                    tempDir.getPath(), imageName, destHost.getAddress()));
            String ovfFilePath = String.format("%s/%s.ovf", tempDir.getPath(), imageName);

            // Import the ovf.
            // Note: always import on the host NOT on vCenter.
            boolean ovfImported;
            ovfImported = destHostMO.importVmFromOvf(ovfFilePath, imageName, destDatastore, "thin",
                    Utils.wrapProgressUpdater(progressUpdater, 50, 90));

            if (!ovfImported) {
                LOGGER.error(String.format("copyDiskImageWithOvf: Failed to import ovf \"%s/%s\" to host \"%s\"",
                        tempDir.getPath(), imageName, destHost.getAddress()));
                throw new IOException(String.format("Failed to import image \"%s\"", imageName));
            }

            // Find the VM
            VirtualMachineMO destImageVm = null;
            ocs = destHostMO.getVmProperties(new String[] {"name"});
            if (ocs != null) {
                for (ObjectContent oc : ocs) {
                    String name = (String) VmwareUtils.getPropValue(oc, "name");
                    if (StringUtils.equals(name, imageName)) {
                        destImageVm = new VirtualMachineMO(destContext, oc.getObj(), name);
                        break;
                    }
                }
            }

            if (destImageVm == null) {
                LOGGER.error(String.format("Unable to find the registered disk image \"%s\" after replication", imageName));
                throw new BrokenImageException("Unable to find the registered image after replication");
            }

            try {
                // Take a snapshot.
                if (!destImageVm.createSnapshot("Base", "", false, false, null)) {
                    LOGGER.error(String.format("Failed to take VM snapshot for image vm \"%s\"", imageName));
                    throw new BrokenImageException("Failed to take VM snapshot");
                }
            } finally {
            }

            if (progressUpdater != null) {
                progressUpdater.action(100);
            }

            return new Pair<String, String>(destImageVm.getMor().get_value(), destImageVmxDsFile.getFullPath());
        } catch (MethodFault e) {
            LOGGER.error(String.format("Failed to copy disk image \"%s\" from \"%s\" to \"%s\"",
                    imageName, srcCtx.getServerAddress(), destCtx.getServerAddress()));
            throw new HostConnectionException("Failed to call VMware SDK", e);
        } catch (RemoteException e) {
            LOGGER.error(String.format("Failed to copy disk image \"%s\" from \"%s\" to \"%s\"",
                    imageName, srcCtx.getServerAddress(), destCtx.getServerAddress()));
            throw new HostConnectionException("Failed to connect to host", e);
        } catch (IOException e) {
            LOGGER.error(String.format("Failed to copy disk image \"%s\" from \"%s\" to \"%s\"",
                    imageName, srcCtx.getServerAddress(), destCtx.getServerAddress()));
            throw new StorageException("Invalid disk image", e);
        } finally {
            if (tempDir != null) {
                try {
                    FileUtils.deleteDirectory(tempDir);
                } catch (IOException e) {
                    LOGGER.warn("copyDiskImageWithOvf: Failed to delete temp directory", e);
                }
            }

            if (timer != null) {
                timer.cancel();
            }
        }
    }

    private boolean doUnregisterVmOnVcHost(VirtualMachineMO vm, VmwareContext vcContext, HostMO vcHostMO) {
        String vmxPath = null;
        try {
            vmxPath = vm.getVmxPath();
        } catch (Exception e) {
            LOGGER.warn(String.format("Failed to get vmx path for vm id=\"%s\", continue.", vm.getMor().get_value()));
            return false;
        }

        if (vcHostMO != null) {
            // Find the vm.
            ObjectContent[] ocs;
            try {
                ocs = vcHostMO.getVmProperties(new String[] {"summary.config.vmPathName"});
            } catch (Exception e) {
                LOGGER.warn(String.format("Failed to find vm on vc host: vmx=\"%s\", try on host.", vmxPath));
                return false;
            }

            if (ocs != null) {
                for (ObjectContent oc : ocs) {
                    String path = (String) VmwareUtils.getPropValue(oc, "summary.config.vmPathName");
                    if (StringUtils.equals(path, vmxPath)) {
                        VirtualMachineMO vcVm = new VirtualMachineMO(vcContext, oc.getObj());
                        try {
                            vcVm.unregister();
                            return true;
                        } catch (Exception e) {
                            LOGGER.warn(String.format("Failed to un-register from vCenter: vmx=\"%s\", try on host.", vmxPath));
                            return false;
                        }
                    }
                }
            }
        }

        return false;
    }

    /*
     * Create a linked clone VM image from an existing VM's current snapshot
     *
     * @param ctx the hypervisor context
     *
     * @param srcImageName the source VM's Image name
     *
     * @param srcStorage the storage of the parent VM image
     *
     * @param destImageName the name of the to be created linked clone
     *
     * @param destStorage the storage of the to be created linked clone VM
     *
     * @param network the network name
     *
     * @param customProps customization properties
     *
     * @return the cloned VM's full path name (i.e. "[<ds>] path/vmName.vmx")
     */
    public String linkedCloneImage(HypervisorContext ctx, String baseImageName, Storage baseStorage,
            String cloneImageParentDir, String cloneImageName, Storage cloneStorage, String network,
            Map<String, String> customProps, ActionDelegate<Integer> progressUpdater)
            throws BaseException {
        Assert.isInstanceOf(VmwareContext.class, ctx, "Invalid hypervisor context - expect Vmware");
        Assert.hasText(baseImageName, "Empty base image name");
        Assert.notNull(baseStorage, "Null base storage specified for image");
        Assert.hasText(cloneImageName, "Empty clone image name");

        LOGGER.info(String.format("Linked cloning image %s to %s...", cloneImageName, baseImageName));

        if (progressUpdater != null) {
            progressUpdater.action(0);
        }

        File tempDir = null;

        /* This method creates a linked clone (deskImage) on destStorage from a parent VM (srcImage)'s
         * current snapshot (00001) on srcStorage in the following steps:
         *
         * 1. create a server side local temporary working directory.
         * 2. create a VM directory (srcImage) on destStorage.
         * 3. get a copy of the src VM's .vmx file to server's temp working directory created in step 1.
         * 4. modify this .vmx file.
         * 5. upload this modified .vmx file to the new clone's directory created in step 2.
         * 6. get a copy of the src VM's .vmxf file to server's temp working directory created in step 1.
         * 7. modify this .vmxf file.
         * 8. upload this modified .vmxf file to the new clone's directory created in step 2.
         * 9. get a copy of the src VM's -000001.vmdk file (the matedata file) to server's temp working dir.
         * 10. modify this .vmdk file.
         * 11. upload this modified .vmdk file to the new clone's directory created in step 2.
         * 12. copy the src VM's delta .vmdk to the clone's directory. (what change is needed from this delta?).
         */
        try {
            VmwareContext context = (VmwareContext) ctx;

            tempDir = Utils.createTempDirectory();
            LOGGER.debug(String.format("linkedCloneImage: Created local temp dir \"%s\"", tempDir.getPath()));

            if (progressUpdater != null) {
                progressUpdater.action(2);
            }

            DatastoreMO baseDatastore = new DatastoreMO(context, baseStorage.getRefId(), baseStorage.getName());
            String baseDsName = baseDatastore.getName();
            String baseDsUrl = baseDatastore.getUrl();

            if (cloneStorage == null) {
                cloneStorage = baseStorage;
            }
            DatastoreMO cloneDatastore = new DatastoreMO(context, cloneStorage.getRefId(), cloneStorage.getName());
            String cloneDsName = cloneDatastore.getName();

            DatastoreFile baseImageDsDir = new DatastoreFile(baseDsName, baseImageName);
            DatastoreFile cloneImageDsDir = new DatastoreFile(cloneDsName,
                    StringUtils.isEmpty(cloneImageParentDir) ? cloneImageName : String.format("%s/%s", cloneImageParentDir, cloneImageName));

            DatacenterMO datacenter = context.getOwnerDatacenter(baseDatastore.getMor());

            boolean dirExists = false; // Try to create if error.
            try {
                dirExists = cloneDatastore.fileExists(cloneImageDsDir.getFullPath(), null);
            } catch (Exception e) {
                // Ignore error.
            }
            if (!dirExists) {
                // Make remote image dir
                LOGGER.debug(String.format("linkedCloneImage: Make remote image dir \"%s\"", cloneImageDsDir.getFullPath()));
                cloneDatastore.makeDirectory(cloneImageDsDir.getFullPath(), datacenter);
            }

            if (progressUpdater != null) {
                progressUpdater.action(5);
            }

            String baseImageVmxFile = baseImageName + ".vmx";
            DatastoreFile baseImageVmxDsFile = new DatastoreFile(baseImageDsDir.getFullPath() + "/" + baseImageVmxFile);
            File tempVmxFile = new File(tempDir.getPath(), baseImageVmxFile);
            File tempCloneVmxFile = new File(tempDir.getPath(), cloneImageName + ".vmx.temp");
            DatastoreFile cloneImageVmxDsFile = new DatastoreFile(cloneImageDsDir.getFullPath()
                    + "/" + cloneImageName + ".vmx");

            // Get remote base image .vmx to local temp dir
            LOGGER.debug(String.format("linkedCloneImage: Get remote vmx \"%s\" to local \"%s\"",
                    baseImageVmxDsFile.getFullPath(), tempVmxFile.getPath()));
            try {
                datacenter.getDatastoreFile(baseImageVmxDsFile.getFullPath(), tempVmxFile.getPath());
            } catch (FileNotFoundException e) {
                LOGGER.error(String.format("linkedCloneImage: Failed to get remote vmx \"%s\" to local \"%s\"",
                        baseImageVmxDsFile.getFullPath(), tempVmxFile.getPath()), e);
                throw new BrokenImageException("Invalid disk image", e);
            }

            // Modify local temp base image .vmx to local temp clone image .vmx
            LOGGER.debug(String.format("linkedCloneImage: Modify vmx \"%s\" to \"%s\"",
                    tempVmxFile.getPath(), tempCloneVmxFile.getPath()));
            VmxDescriptorFile.copy(tempVmxFile, tempCloneVmxFile,
                    VmwareStorageManagerHelper.createLinkedCloneImageVmxContentHandler(baseImageName,
                            cloneImageName, network, customProps));

            // Put modified local image .vmx to remote dir
            LOGGER.debug(String.format("linkedCloneImage: Put local vmx \"%s\" to remote \"%s\"",
                    tempCloneVmxFile.getPath(), cloneImageVmxDsFile.getFullPath()));
            datacenter.putDatastoreFile(cloneImageVmxDsFile.getFullPath(), tempCloneVmxFile.getPath());

            if (StringUtils.equals(baseImageDsDir.getFullPath(), cloneImageDsDir.getFullPath())) {
                if (progressUpdater != null) {
                    progressUpdater.action(100);
                }

                return cloneImageVmxDsFile.getFullPath();
            }

            String baseImageVmxfFile = baseImageName + ".vmxf";
            DatastoreFile baseImageVmxfDsFile = new DatastoreFile(baseImageDsDir.getFullPath() + "/" + baseImageVmxfFile);
            File tempVmxfFile = new File(tempDir.getPath(), baseImageVmxfFile);
            File tempCloneVmxfFile = new File(tempDir.getPath(), cloneImageName + ".vmxf.temp");
            DatastoreFile cloneImageVmxfDsFile = new DatastoreFile(cloneImageDsDir.getFullPath()
                    + "/" + cloneImageName + ".vmxf");

            // Get remote base image .vmxf to local temp dir
            LOGGER.debug(String.format("linkedCloneImage: Get remote vmxf \"%s\" to local \"%s\"",
                    baseImageVmxfDsFile.getFullPath(), tempVmxfFile.getPath()));
            try {
                datacenter.getDatastoreFile(baseImageVmxfDsFile.getFullPath(), tempVmxfFile.getPath());

                // Modify local temp base image .vmxf to local temp clone image .vmxf
                LOGGER.debug(String.format("linkedCloneImage: Modify vmxf \"%s\" to \"%s\"",
                        tempVmxfFile.getPath(), tempCloneVmxfFile.getPath()));
                VmxDescriptorFile.copy(tempVmxfFile, tempCloneVmxfFile,
                        VmwareStorageManagerHelper.createLinkedCloneImageVmxfContentHandler(baseImageName, cloneImageName));

                // Put modified local image .vmxf to remote dir
                LOGGER.debug(String.format("linkedCloneImage: Put local vmxf \"%s\" to remote \"%s\"",
                        tempCloneVmxfFile.getPath(), cloneImageVmxfDsFile.getFullPath()));
                datacenter.putDatastoreFile(cloneImageVmxfDsFile.getFullPath(), tempCloneVmxfFile.getPath());
            } catch (Exception e) {
                LOGGER.warn(String.format("linkedCloneImage: Failed to copy vmxf \"%s\" - it's not necessarily an error",
                        baseImageVmxfDsFile.getFullPath()));
            }

            String baseImageVmdkFile = baseImageName + "-000001.vmdk";
            DatastoreFile baseImageVmdkDsFile = new DatastoreFile(baseImageDsDir.getFullPath() + "/" + baseImageVmdkFile);
            File tempVmdkFile = new File(tempDir.getPath(), baseImageVmdkFile);
            File tempCloneVmdkFile = new File(tempDir.getPath(), cloneImageName + ".vmdk.temp");
            DatastoreFile cloneImageVmdkDsFile = new DatastoreFile(cloneImageDsDir.getFullPath()
                    + "/" + cloneImageName + ".vmdk");

            // Get remote base image .vmdk to local temp dir
            LOGGER.debug(String.format("linkedCloneImage: Get remote vmdk \"%s\" to local \"%s\"",
                    baseImageVmdkDsFile.getFullPath(), tempVmdkFile.getPath()));
            try {
                datacenter.getDatastoreFile(baseImageVmdkDsFile.getFullPath(), tempVmdkFile.getPath());
            } catch (FileNotFoundException e) {
                LOGGER.error(String.format("linkedCloneImage: Failed to get remote vmdk \"%s\" to local \"%s\"",
                        baseImageVmdkDsFile.getFullPath(), tempVmdkFile.getPath()), e);
                throw new BrokenImageException("Invalid disk image", e);
            }

            // Modify local temp base image .vmdk to local temp clone image .vmdk
            LOGGER.debug(String.format("linkedCloneImage: Modify vmxf \"%s\" to \"%s\"",
                    tempVmdkFile.getPath(), tempCloneVmdkFile.getPath()));
            VmxDescriptorFile.copy(tempVmdkFile, tempCloneVmdkFile,
                    VmwareStorageManagerHelper.createLinkedCloneImageVmdkContentHandler(baseDsUrl,
                            baseImageName, cloneImageName));

            // Put modified local image .vmdk to remote dir
            LOGGER.debug(String.format("linkedCloneImage: Put local vmdk \"%s\" to remote \"%s\"",
                    tempCloneVmdkFile.getPath(), cloneImageVmdkDsFile.getFullPath()));
            datacenter.putDatastoreFile(cloneImageVmdkDsFile.getFullPath(), tempCloneVmdkFile.getPath());

            if (progressUpdater != null) {
                progressUpdater.action(10);
            }

            // Copy delta vmdk file
            baseImageVmdkDsFile = new DatastoreFile(baseImageDsDir.getFullPath()
                    + "/" + baseImageName + "-000001-delta.vmdk");
            cloneImageVmdkDsFile = new DatastoreFile(cloneImageDsDir.getFullPath() + "/" + cloneImageName + "-delta.vmdk");

            LOGGER.debug(String.format("linkedCloneImage: Copy vmdk \"%s\" to \"%s\"",
                    baseImageVmdkDsFile.getFullPath(), cloneImageVmdkDsFile.getFullPath()));
            try {
                if (!baseDatastore.copyDatastoreFile(baseImageVmdkDsFile.getFullPath() , datacenter, cloneDatastore,
                        cloneImageVmdkDsFile.getFullPath(), datacenter, true,
                        Utils.wrapProgressUpdater(progressUpdater, 10, 100))) {
                    throw new IOException("Failed to copy virtual disk");
                }
            } catch (FileFault e) {
                LOGGER.error(String.format("linkedCloneImage: Failed to copy vmdk \"%s\" to \"%s\"",
                        baseImageVmdkDsFile.getFullPath(), cloneImageVmdkDsFile.getFullPath()), e);
                throw new BrokenImageException("Invalid disk image", e);
            }

            return cloneImageVmxDsFile.getFullPath();
        } catch (MethodFault e) {
            LOGGER.error(String.format("Failed to linked clone image %s to %s", cloneImageName, baseImageName), e);
            throw new HostConnectionException("Failed to call VMware SDK", e);
        } catch (RemoteException e) {
            LOGGER.error(String.format("Failed to linked clone image %s to %s", cloneImageName, baseImageName), e);
            throw new HostConnectionException("Failed to connect to host", e);
        } catch (IOException e) {
            LOGGER.error(String.format("Failed to linked clone image %s to %s", cloneImageName, baseImageName), e);
            throw new StorageException("Invalid disk image", e);
        } finally {
            if (tempDir != null) {
                try {
                    FileUtils.deleteDirectory(tempDir);
                } catch (IOException e) {
                    LOGGER.warn("linkedCloneImage: Failed to delete temp directory", e);
                }
            }
        }
    }

    public void deleteDatastoreDirectory(HypervisorContext ctx, String filePath, Storage storage)
            throws BaseException {
        Assert.isInstanceOf(VmwareContext.class, ctx, "Invalid hypervisor context - expect Vmware");
        Assert.hasText(filePath, "Empty file path");
        Assert.notNull(storage, "Null storage specified for image");

        LOGGER.info(String.format("Deleting datastore directory for \"%s\" on storage \"%s\"(%s)...",
                filePath, storage.getName(), storage.getRefId()));

        VmwareContext context = (VmwareContext) ctx;
        String dirFullPath = null;

        try {
            DatastoreMO datastore = new DatastoreMO(context, storage.getRefId(), storage.getName());
            String dsName = datastore.getName();

            DatastoreFile dsFile;
            if (DatastoreFile.isFullDatastorePath(filePath)) {
                dsFile = new DatastoreFile(filePath);
            } else {
                dsFile = new DatastoreFile(dsName, filePath);
            }

            DatacenterMO datacenter = context.getRootDatacenter();// .getOwnerDatacenter(datastore.getMor());

            dirFullPath = dsFile.getDirFullPath();
            boolean dirExists = true; // Try to delete if error.
            try {
                dirExists = datastore.fileExists(dirFullPath, null);
            } catch (Exception e) {
                // Ignore error.
            }
            if (dirExists) {
                datastore.deleteDatastoreFile(dirFullPath, datacenter, null);
            }
        } catch (MethodFault e) {
            LOGGER.error(String.format("Failed to delete datastore directory for \"%s\"",
                    (dirFullPath == null) ? filePath : dirFullPath), e);
            throw new HostConnectionException("Failed to call VMware SDK", e);
        } catch (RemoteException e) {
            LOGGER.error(String.format("Failed to delete datastore directory for \"%s\"",
                    (dirFullPath == null) ? filePath : dirFullPath), e);
            throw new HostConnectionException("Failed to connect to host", e);
        }
    }

    /*
     * Called by Storage Controller to process the AllocateStorageEvent. I.e. chose a appropriate
     * datastore for VM import, pool creation, and clone instantiation.
     *
     * TODO: We should have more user configured information in the in put storage objects to
     * indicates the limitation on each storage (i.e. ideal purposes, etc.).
     *
     */
    public Storage allocateStorage(HypervisorContext ctx, List<? extends Storage> storages, String purpose) {
        if (storages == null || storages.isEmpty()) {
            throw new RuntimeException("Unable to allocate storage from empty list");
        }

        if (storages.size() == 1) {
            // this is the only storage
            return storages.get(0);
        }

        long totalFreeSpace = 0;
        for (Storage storage : storages) {
            totalFreeSpace += storage.getFreeSpace();
        }

        long randNum = (long) ((new Random()).nextDouble() * totalFreeSpace);
        if (randNum >= totalFreeSpace) {
            // Just in case it's rounded up.
            randNum = totalFreeSpace - 1;
        }

        Storage selected = storages.get(0);

        for (Storage storage : storages) {
            if (randNum < storage.getFreeSpace()) {
                selected = storage;
                break;
            }
            randNum -= storage.getFreeSpace();
        }

        if (selected == null) {
            throw new RuntimeException(String.format("Unable to find storage for \"%s\"", purpose));
        }

        LOGGER.debug(String.format("allocateStorage() returns storage %s", selected.getName()));
        return selected;
    }

    public String relinkTolocalReplica(HypervisorContext ctx, String newBaseImageName, Storage newBaseStorage,
            String cloneImageParentDir, String cloneImageName,
            Storage cloneStorage, String network, Map<String, String> customProps,
            ActionDelegate<Integer> progressUpdater)
            throws BaseException {
        Assert.isInstanceOf(VmwareContext.class, ctx, "Invalid hypervisor context - expect Vmware");
        Assert.hasText(newBaseImageName, "Empty new base image name");
        Assert.notNull(newBaseStorage, "Null new base storage specified for image");
        Assert.hasText(cloneImageName, "Empty clone image name");
        Assert.notNull(cloneStorage, "Null clone storage specified for image");

        LOGGER.info(String.format("Relinking clone image %s to %s...", cloneImageName, newBaseImageName));

        if (progressUpdater != null) {
            progressUpdater.action(0);
        }

        File tempDir = null;

        try {

            VmwareContext context = (VmwareContext)ctx;

            tempDir = Utils.createTempDirectory();
            LOGGER.debug(String.format("relinkTolocalReplica: Created local temp dir \"%s\"", tempDir.getPath()));

            if (progressUpdater != null) {
                progressUpdater.action(2);
            }

            DatastoreMO newBaseDatastore = new DatastoreMO(context, newBaseStorage.getRefId(), newBaseStorage.getName());
            String newBaseDsUrl = newBaseDatastore.getUrl();

            DatastoreMO cloneDatastore = new DatastoreMO(context, cloneStorage.getRefId(), cloneStorage.getName());
            String cloneDsName = cloneDatastore.getName();

            DatastoreFile cloneImageDsDir = new DatastoreFile(cloneDsName,
                    StringUtils.isEmpty(cloneImageParentDir) ? cloneImageName : String.format("%s/%s", cloneImageParentDir, cloneImageName));

            File tempVmxFile = new File(tempDir.getPath(), cloneImageName + ".old.vmx");
            File tempCloneVmxFile = new File(tempDir.getPath(), cloneImageName + ".vmx");
            DatastoreFile cloneImageVmxDsFile = new DatastoreFile(cloneImageDsDir.getFullPath()
                    + "/" + cloneImageName + ".vmx");

            DatacenterMO datacenter = context.getOwnerDatacenter(newBaseDatastore.getMor());

            // Get remote clone image .vmx to local temp dir
            LOGGER.debug(String.format("relinkTolocalReplica: Get remote vmx \"%s\" to local \"%s\"",
                    cloneImageVmxDsFile.getFullPath(), tempVmxFile.getPath()));
            try {
                datacenter.getDatastoreFile(cloneImageVmxDsFile.getFullPath(), tempVmxFile.getPath());
            } catch (FileNotFoundException e) {
                LOGGER.error(String.format("relinkTolocalReplica: Failed to get remote vmx \"%s\" to local \"%s\"",
                        cloneImageVmxDsFile.getFullPath(), tempVmxFile.getPath()), e);
                throw new BrokenImageException("Invalid disk image", e);
            }

            // Modify local temp image .vmx to local temp clone image .vmx
            LOGGER.debug(String.format("relinkTolocalReplica: Modify vmx \"%s\" to \"%s\"",
                    tempVmxFile.getPath(), tempCloneVmxFile.getPath()));
            VmxDescriptorFile.copy(tempVmxFile, tempCloneVmxFile,
                    VmwareStorageManagerHelper.createRelinkTolocalReplicaVmxContentMergeHandler(network, customProps));

            // Put modified local image .vmx to remote dir
            LOGGER.debug(String.format("relinkTolocalReplica: Put local vmx \"%s\" to remote \"%s\"",
                    tempCloneVmxFile.getPath(), cloneImageVmxDsFile.getFullPath()));
            datacenter.putDatastoreFile(cloneImageVmxDsFile.getFullPath(), tempCloneVmxFile.getPath());

            if (progressUpdater != null) {
                progressUpdater.action(50);
            }

            // Get base image .vmdk file to get CID and fix parentCID for the new linked clone.
            String newBaseDsName = newBaseDatastore.getName();
            DatastoreFile newBaseImageDsDir = new DatastoreFile(newBaseDsName, newBaseImageName);
            String newBaseImageVmdkFile = newBaseImageName + ".vmdk";
            DatastoreFile newBaseImageVmdkDsFile = new DatastoreFile(newBaseImageDsDir.getFullPath() + "/" + newBaseImageVmdkFile);
            File tempBaseVmdkFile = new File(tempDir.getPath(), newBaseImageVmdkFile);

            // Get remote base image .vmdk to local temp dir
            LOGGER.debug(String.format("relinkTolocalReplica: Get remote vmdk \"%s\" to local \"%s\"",
                    newBaseImageVmdkDsFile.getFullPath(), tempBaseVmdkFile.getPath()));
            try {
                datacenter.getDatastoreFile(newBaseImageVmdkDsFile.getFullPath(), tempBaseVmdkFile.getPath());
            } catch (FileNotFoundException e) {
                LOGGER.error(String.format("relinkTolocalReplica: Failed to get remote vmdk \"%s\" to local \"%s\"",
                        newBaseImageVmdkDsFile.getFullPath(), tempBaseVmdkFile.getPath()), e);
                throw new BrokenImageException("Invalid disk image", e);
            }

            VmdkDescriptorFile vmdkDescriptor = new VmdkDescriptorFile();
            try {
                vmdkDescriptor.parse(tempBaseVmdkFile);

                if (StringUtils.isEmpty(vmdkDescriptor.getCID())) {
                    throw new IOException("No CID value");
                }
            } catch (IOException e) {
                LOGGER.error(String.format("relinkTolocalReplica: Failed to parse vmdk \"%s\"",
                        newBaseImageVmdkDsFile.getFullPath()), e);
                throw new BrokenImageException("Invalid disk image", e);
            }

            File tempVmdkFile = new File(tempDir.getPath(), cloneImageName + ".old.vmdk");
            File tempCloneVmdkFile = new File(tempDir.getPath(), cloneImageName + ".vmdk");
            DatastoreFile cloneImageVmdkDsFile = new DatastoreFile(cloneImageDsDir.getFullPath()
                    + "/" + cloneImageName + ".vmdk");

            // Get remote clone image .vmdk to local temp dir
            LOGGER.debug(String.format("relinkTolocalReplica: Get remote vmdk \"%s\" to local \"%s\"",
                    cloneImageVmdkDsFile.getFullPath(), tempVmdkFile.getPath()));
            try {
                datacenter.getDatastoreFile(cloneImageVmdkDsFile.getFullPath(), tempVmdkFile.getPath());
            } catch (FileNotFoundException e) {
                LOGGER.error(String.format("relinkTolocalReplica: Failed to get remote vmdk \"%s\" to local \"%s\"",
                        cloneImageVmdkDsFile.getFullPath(), tempVmdkFile.getPath()), e);
                throw new BrokenImageException("Invalid disk image", e);
            }

            // Modify local temp base image .vmdk to local temp clone image .vmdk
            LOGGER.debug(String.format("relinkTolocalReplica: Modify vmxf \"%s\" to \"%s\"",
                    tempVmdkFile.getPath(), tempCloneVmdkFile.getPath()));
            VmxDescriptorFile.copy(tempVmdkFile, tempCloneVmdkFile,
                    VmwareStorageManagerHelper.createRelinkTolocalReplicaVmdkContentHandler(
                            newBaseDsUrl, newBaseImageName, vmdkDescriptor.getCID()));

            // Put modified local image .vmdk to remote dir
            LOGGER.debug(String.format("relinkTolocalReplica: Put local vmdk \"%s\" to remote \"%s\"",
                    tempCloneVmdkFile.getPath(), cloneImageVmdkDsFile.getFullPath()));
            datacenter.putDatastoreFile(cloneImageVmdkDsFile.getFullPath(), tempCloneVmdkFile.getPath());

            if (progressUpdater != null) {
                progressUpdater.action(100);
            }

            return cloneImageVmxDsFile.getFullPath();
        } catch (MethodFault e) {
            LOGGER.error(String.format("Failed to relink to local replica"), e);
            throw new HostConnectionException("Failed to call VMware SDK", e);
        } catch (RemoteException e) {
            LOGGER.error(String.format("Failed to relink to local replica"), e);
            throw new HostConnectionException("Failed to connect to host", e);
        } catch (IOException e) {
            LOGGER.error(String.format("Failed to relink to local replica"), e);
            throw new StorageException("Invalid disk image", e);
        } finally {
            if (tempDir != null) {
                try {
                    FileUtils.deleteDirectory(tempDir);
                } catch (IOException e) {
                    LOGGER.warn("relinkTolocalReplica: Failed to delete temp directory", e);
                }
            }
        }
    }
}

