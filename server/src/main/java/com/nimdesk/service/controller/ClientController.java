/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.service.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.nimdesk.event.AsyncCallback;
import com.nimdesk.event.RequestDesktopEvent;
import com.nimdesk.gateway.Gateway;
import com.nimdesk.ldap.LdapContext;
import com.nimdesk.ldap.LdapHelper;
import com.nimdesk.ldap.LdapUser;
import com.nimdesk.model.LdapServer;
import com.nimdesk.model.Pool;
import com.nimdesk.model.Server;
import com.nimdesk.model.User;
import com.nimdesk.model.VmInstance;
import com.nimdesk.service.EventBusService;
import com.nimdesk.service.Workflow;
import com.nimdesk.ui.server.UserAccountService;
import com.nimdesk.util.IdGenerator;
import com.nimdesk.util.RdpDescriptor;

@Controller
public class ClientController implements InitializingBean {
    private static final Logger LOGGER = Logger.getLogger(ClientController.class);

    private static final long INTERVAL = 120 * 1000; // 2 minutes

    private static final String KEY_ERROR = "error";

    private static final String KEY_SERVER_NUM = "server.num";
    private static final String KEY_SERVER_IP = "server.%d.ip";

    private static final String KEY_DOMAIN_NUM = "domain.num";
    private static final String KEY_DOMAIN_NAME = "domain.%d.name";
    private static final String KEY_DOMAIN_ID = "domain.%d.id";

    private static final String KEY_USER_UUID = "userid";
    private static final String KEY_USER_NAME = "user";
    private static final String KEY_USER_DOMAIN = "domain";
    private static final String KEY_POOL_NUM = "pool.num";
    private static final String KEY_POOL_NAME = "pool.%d.name";
    private static final String KEY_POOL_ID = "pool.%d.id";
    private static final String KEY_POOL_READY = "pool.%d.ready";

    private static final String KEY_REQUEST_DESKTOP_ID = "requestid";

    private static final String OS_MACOSX = "MacOSX";
    private static final String OS_LINUX = "Linux";

    private static final String LDAP_ERROR = "Unable to connect to the active directory service.";
    private static final String AUTHENTICATION_ERROR = "An incorrect user name or password was entered.";
    private static final String ENTITLEMENT_ERROR = "No desktop is assigned to the user.";
    private static final String TIMEOUT_ERROR = "Login time out.";

    @Autowired
    private EventBusService _eventBusService;

    @Autowired
    private Server.Service _serverService;

    @Autowired
    private Pool.Service _poolService;

    @Autowired
    private LdapServer.Service _ldapServerService;

    @Autowired
    private VmInstance.Service _vmInstanceService;

    @Autowired
    private User.Service _userService;

    @Autowired
    private UserAccountService _userAccountService;

    @Autowired
    private Gateway _gateway;

    private final Map<String, RequestDesktopResult> _requestResults = new HashMap<String, RequestDesktopResult>();

    private class RequestDesktopResult {
        User user;
        Pool pool;
        VmInstance desktop;
        long timestamp;
    }

    private final ScheduledExecutorService _executor = Executors.newScheduledThreadPool(1);

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(_eventBusService, "An EventBusService must be set");
        Assert.notNull(_serverService, "A Server.Service must be set");
        Assert.notNull(_poolService, "A Pool.Service must be set");
        Assert.notNull(_ldapServerService, "A LdapServer.Service must be set");
        Assert.notNull(_vmInstanceService, "A VmInstance.Service must be set");
        Assert.notNull(_userService, "A User.Service must be set");
        Assert.notNull(_userAccountService, "A UserAccountService must be set");
        Assert.notNull(_gateway, "A Gateway must be set");

        _executor.scheduleWithFixedDelay(new Runnable() {
            @Override
            public void run() {
                synchronized(_requestResults) {
                    long current = System.currentTimeMillis();
                    List<String> removedResults = new ArrayList<String>();

                    for (Map.Entry<String, RequestDesktopResult> entry : _requestResults.entrySet()) {
                        long timestamp = entry.getValue().timestamp;
                        if (timestamp == 0) {
                            continue;
                        }

                        if (current - timestamp >= INTERVAL) {
                            removedResults.add(entry.getKey());
                        }
                    }

                    for (String removed : removedResults) {
                        _requestResults.remove(removed);
                    }
                }
            }
        }, (new Random()).nextInt(60 * 1000), INTERVAL, TimeUnit.SECONDS);
    }

    @RequestMapping(value="/dc/getservers", method=RequestMethod.GET)
    public void getServers(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        LOGGER.info(String.format("getServers"));

        List<? extends Server> runningServers = _serverService.getRunningServers();

        response.setContentType("text/plain");
        PrintWriter writer = response.getWriter();

        if (runningServers == null) {
            println(writer, String.format("%s=%s", KEY_SERVER_NUM, 0));
            return;
        }

        println(writer, String.format("%s=%s", KEY_SERVER_NUM, runningServers.size()));

        int idx = 0;
        for (Server server : runningServers) {
            println(writer, String.format(KEY_SERVER_IP + "=%s", idx, server.getPublicAddress()));
            ++idx;
        }
    }

    @RequestMapping(value="/dc/getdomains", method=RequestMethod.GET)
    public void getDomains(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        LOGGER.info(String.format("getDomains"));

        List<? extends LdapServer> ldapServers = _ldapServerService.getAll(null);

        response.setContentType("text/plain");
        PrintWriter writer = response.getWriter();

        println(writer, String.format("%s=%s", KEY_DOMAIN_NUM, ldapServers.size() + 1));
        println(writer, String.format(KEY_DOMAIN_NAME + "=%s", 0, "WORKGROUP"));
        println(writer, String.format(KEY_DOMAIN_ID + "=%s", 0, ""));

        int idx = 1;
        for (LdapServer ldapServer : ldapServers) {
            println(writer, String.format(KEY_DOMAIN_NAME + "=%s", idx, ldapServer.getDomain()));
            println(writer, String.format(KEY_DOMAIN_ID + "=%s", idx, ldapServer.getUuid()));
            ++idx;
        }
    }

    @RequestMapping(value="/dc/login", method=RequestMethod.GET)
    public void login(@RequestParam(value="ver", required=false) String version,
            @RequestParam(value="user", required=true) String username,
            @RequestParam(value="ldap", required=false) String ldapServerId,
            @RequestParam(value="kiosk", required=false) String kiosk,
            @RequestParam(value="pool", required=false) String poolId,
            HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        LOGGER.info(String.format("login: ver=%s, user=%s, ldap=%s, kiosk=%s, pool=%s",
                version, username, ldapServerId, kiosk, poolId));

        String password = request.getHeader("password");

        UserInfo userInfo = loginUser(ldapServerId, username, password);

        response.setContentType("text/plain");
        PrintWriter writer = response.getWriter();

        if (userInfo.isError) {
            LOGGER.error(String.format("Failed to login user \"%s\" - %s", username, userInfo.message));
            println(writer, String.format("%s=%s", KEY_ERROR, userInfo.message));
            return;
        }

        println(writer, String.format("%s=%s", KEY_USER_UUID, userInfo.getUserUuid()));
        println(writer, String.format("%s=%s", KEY_USER_NAME, userInfo.getUsername()));
        println(writer, String.format("%s=%s", KEY_USER_DOMAIN, userInfo.getDomain()));
        println(writer, String.format("%s=%s", KEY_POOL_NUM, userInfo.pools.size()));

        int idx = 0;
        for (Pool pool : userInfo.pools) {
            println(writer, String.format(KEY_POOL_NAME + "=%s", idx, pool.getName()));
            println(writer, String.format(KEY_POOL_ID + "=%s", idx, pool.getUuid()));
            println(writer, String.format(KEY_POOL_READY + "=%s", idx,
                    (pool.getStatus() == Pool.Status.Ready && pool.isEnabled()) ? "1" : "0"));
            ++idx;
        }
    }

    @RequestMapping(value="/dc/request", method=RequestMethod.GET)
    public void requestDesktop(@RequestParam(value="ver", required=false) String version,
            @RequestParam(value="user", required=true) String userUuid,
            @RequestParam(value="pool", required=true) String poolId,
            HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        LOGGER.info(String.format("requestDesktop: ver=%s, userUuid=%s, pool=%s",
                version, userUuid, poolId));

        response.setContentType("text/plain");
        PrintWriter writer = response.getWriter();

        User user = _userService.getByUuid(userUuid);
        if (user == null) {
            LOGGER.error(String.format("Failed to get user for \"%s\"", userUuid));
            println(writer, String.format("%s=%s", KEY_ERROR, "Failed to get the user information"));
            return;
        }
        if (user.getType() != User.Type.User) {
            LOGGER.error(String.format("Invalid user type for \"%s\"", userUuid));
            println(writer, String.format("%s=%s", KEY_ERROR, "Invalid user type"));
            return;
        }

        Pool pool = _poolService.getByUuid(poolId);
        if (pool == null) {
            LOGGER.error(String.format("Failed to get pool for \"%s\"", poolId));
            println(writer, String.format("%s=%s", KEY_ERROR, "Failed to get the desktop pool information"));
            return;
        }

        final String requestId = IdGenerator.generateUuid();
        final RequestDesktopResult requestResult = new RequestDesktopResult();
        requestResult.timestamp = 0;
        requestResult.user = user;
        requestResult.pool = pool;
        requestResult.desktop = null;
        synchronized(_requestResults) {
            _requestResults.put(requestId, requestResult);
        }

        RequestDesktopEvent event = new RequestDesktopEvent(pool, userUuid, new AsyncCallback() {
            @Override
            public void onSuccess(Object result) {
                synchronized(_requestResults) {
                    requestResult.desktop = (VmInstance) result;
                    requestResult.timestamp = System.currentTimeMillis();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                synchronized(_requestResults) {
                    _requestResults.remove(requestId);
                }
            }

            @Override
            public void progress(int percentage) {
            }

            @Override
            public String getRequestor() {
                return null;
            }
        });
        _eventBusService.post(event);

        println(writer, String.format("%s=%s", KEY_REQUEST_DESKTOP_ID, requestId));
    }

    @RequestMapping(value="/dc/connect", method=RequestMethod.GET)
    public void connectDesktop(@RequestParam(value="ver", required=false) String version,
            @RequestParam(value="request", required=true) String requestId,
            @RequestParam(value="os", required=false) String os,
            @RequestParam(value="geom", required=false) String geom,
            HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        LOGGER.info(String.format("connectDesktop: ver=%s, request=%s, os=%s, geom=%s",
                version, requestId, os, geom));

        response.setContentType("text/plain");
        PrintWriter writer = response.getWriter();

        RequestDesktopResult requestResult;
        synchronized(_requestResults) {
            requestResult = _requestResults.get(requestId);
            if (requestResult != null && requestResult.desktop != null) {
                _requestResults.remove(requestId);
            }
        }

        if (requestResult == null) {
            LOGGER.error(String.format("No desktop available for request \"%s\"", requestId));
            println(writer, String.format("%s=%s", KEY_ERROR, "No desktop is available. Please contact the administrator."));
            return;
        }

        if (requestResult.desktop == null) {
            response.setStatus(HttpServletResponse.SC_ACCEPTED);
            return;
        }

        final String rdp;
        try {
            if (OS_MACOSX.equals(os)) {
                rdp = connectRdpForMac(response, requestResult.pool, requestResult.desktop,
                        requestResult.user.getLogonName(), requestResult.user.getDomain());
            } else if (OS_LINUX.equals(os)) {
                rdp = generateRdesktopParam(requestResult.pool, requestResult.desktop,
                        requestResult.user.getLogonName(), requestResult.user.getDomain(), geom);
            } else {
                rdp = generateRdpString(requestResult.pool, requestResult.desktop,
                        requestResult.user.getLogonName(), requestResult.user.getDomain(), geom);
            }
        } catch (Exception e) {
            LOGGER.error("Failed to generate rdp descriptor", e);
            println(writer, String.format("%s=%s", KEY_ERROR, "Unable to connect to the desktop. Please contact the administrator."));
            return;
        }

        writer.print(rdp);
    }

    private void println(PrintWriter writer, String s) {
        writer.print(s);
        writer.print('\n'); // Explicitly write \n to avoid mixing with \r\n on some platforms.
    }

    private class UserInfo {
        User user = null;
        Collection<? extends Pool> pools = null;
        boolean isError = true;
        String message = "";

        LdapContext ldapContext = null;
        List<LdapUser> userWithGroups = null;

        void setMessage(String message, boolean isError) {
            this.message = message;
            this.isError = isError;
        }

        String getUserUuid() {
            if (user != null) {
                return user.getUuid();
            }

            return null;
        }

        String getUsername() {
            if (user != null) {
                return user.getLogonName();
            }

            if (userWithGroups != null && !userWithGroups.isEmpty()) {
                LdapUser ldapUser = userWithGroups.get(0);
                if (ldapUser != null) {
                    return ldapUser.getLogonName();
                }
            }

            return null;
        }

        String getDomain() {
            if (user != null) {
                return user.getDomain();
            }

            if (userWithGroups != null && !userWithGroups.isEmpty()) {
                LdapUser ldapUser = userWithGroups.get(0);
                if (ldapUser != null) {
                    return ldapUser.getDomain();
                }
            }

            return null;
        }
    }

    private UserInfo loginUser(String ldapServerId, final String username, final String password) {
        final UserInfo userInfo = new UserInfo();

        final LdapServer ldapServer = StringUtils.isEmpty(ldapServerId) ?
                LdapServer.WORKGROUP : _ldapServerService.getByUuid(ldapServerId);
        if (ldapServer == null) {
            userInfo.setMessage(LDAP_ERROR, true);
            return userInfo;
        }

        final Object wait = new Object();

        Workflow wf = new Workflow();
        if (StringUtils.isEmpty(ldapServerId)) {
            // Authenticate user
            wf.append(new Workflow.StateRunnable() {
                @Override
                public void run(AsyncCallback callback) {
                    _userAccountService.authenticateWorkGroupUser(username, password, callback);
                }

                @Override
                public void setResult(Object result) {
                    userInfo.user = (User) result;
                }
            }, new AsyncCallback() {
                @Override
                public String getRequestor() {
                    return null;
                }

                @Override
                public void onSuccess(Object result) {
                }

                @Override
                public void onFailure(Throwable t) {
                    userInfo.setMessage(AUTHENTICATION_ERROR, true);
                }

                @Override
                public void progress(int percentage) {
                }
            });
            // Check entitled pools
            wf.append(new Workflow.StateRunnable() {
                @Override
                public void run(AsyncCallback callback) {
                    _userAccountService.getPoolsForUser(userInfo.user, callback);
                }

                @Override
                @SuppressWarnings("unchecked")
                public void setResult(Object result) {
                    userInfo.pools = (Collection<? extends Pool>) result;
                }
            }, new AsyncCallback() {
                @Override
                public String getRequestor() {
                    return null;
                }

                @Override
                public void onSuccess(Object result) {
                }

                @Override
                public void onFailure(Throwable t) {
                    userInfo.setMessage(ENTITLEMENT_ERROR, true);
                }

                @Override
                public void progress(int percentage) {
                }
            });
        } else {
            // Connect to ldap service
            wf.append(new Workflow.StateRunnable() {
                @Override
                public void run(AsyncCallback callback) {
                    _userAccountService.connectLdapService(LdapHelper.composeUrl(ldapServer.getAddress(), 389, false),
                            ldapServer.getDomain(), ldapServer.getUsername(),
                            ldapServer.getPassword(), callback);
                }

                @Override
                public void setResult(Object result) {
                    userInfo.ldapContext = (LdapContext) result;
                }
            }, new AsyncCallback() {
                @Override
                public String getRequestor() {
                    return null;
                }

                @Override
                public void onSuccess(Object result) {
                }

                @Override
                public void onFailure(Throwable t) {
                    userInfo.setMessage(LDAP_ERROR, true);
                }

                @Override
                public void progress(int percentage) {
                }
            });
            // Authenticate user
            wf.append(new Workflow.StateRunnable() {
                @Override
                public void run(AsyncCallback callback) {
                    _userAccountService.authenticateLdaUser(userInfo.ldapContext, username, password, callback);
                }

                @Override
                @SuppressWarnings("unchecked")
                public void setResult(Object result) {
                    userInfo.userWithGroups = (List<LdapUser>) result;
                }
            }, new AsyncCallback() {
                @Override
                public String getRequestor() {
                    return null;
                }

                @Override
                public void onSuccess(Object result) {
                    if (userInfo.userWithGroups != null && !userInfo.userWithGroups.isEmpty()) {
                        LdapUser ldapUser = userInfo.userWithGroups.get(0);
                        if (ldapUser != null) {
                            // Check if this LdapUser exists in DB.
                            userInfo.user = _userAccountService.syncLdapUserWithDb(ldapUser);
                        }
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    userInfo.setMessage(AUTHENTICATION_ERROR, true);
                }

                @Override
                public void progress(int percentage) {
                }
            });
            // Check entitled pools
            wf.append(new Workflow.StateRunnable() {
                @Override
                public void run(AsyncCallback callback) {
                    _userAccountService.getPoolsForUserWithGroups(userInfo.userWithGroups, callback);
                }

                @Override
                @SuppressWarnings("unchecked")
                public void setResult(Object result) {
                    userInfo.pools = (Collection<? extends Pool>) result;
                }
            }, new AsyncCallback() {
                @Override
                public String getRequestor() {
                    return null;
                }

                @Override
                public void onSuccess(Object result) {
                }

                @Override
                public void onFailure(Throwable t) {
                    userInfo.setMessage(ENTITLEMENT_ERROR, true);
                }

                @Override
                public void progress(int percentage) {
                }
            });
        }
        wf.setCallback(new AsyncCallback() {
            @Override
            public String getRequestor() {
                return null;
            }

            @Override
            public void onSuccess(Object result) {
                if (userInfo.pools == null || userInfo.pools.isEmpty()) {
                    userInfo.setMessage(ENTITLEMENT_ERROR, true);
                } else {
                    userInfo.setMessage("", false);
                }

                synchronized(wait) {
                    wait.notifyAll();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                synchronized(wait) {
                    wait.notifyAll();
                }
            }

            @Override
            public void progress(int percentage) {
            }
        });

        synchronized(wait) {
            // Preset timeout error first.
            userInfo.setMessage(TIMEOUT_ERROR, true);

            wf.run();

            // Wait 300 seconds
            try {
                wait.wait(300 * 1000);
            } catch (InterruptedException e) {
                // Ignore;
            }
        }

        return userInfo;
    }

    private String connectRdpForMac(HttpServletResponse response, Pool pool, VmInstance desktop,
            final String username, final String domain)
            throws IOException {
        final Address address = startGatewaySession(desktop);

        StringBuilder sb = new StringBuilder();
        sb.append("user=").append(username).append('\n');
        if (domain != null) {
            sb.append("domain=").append(domain).append('\n');
        }
        sb.append("server=").append(address.getIp()).append('\n');
        sb.append("port=").append(address.getPort()).append('\n');

        return sb.toString();
    }

    private String generateRdesktopParam(Pool pool, VmInstance desktop, String username, String domain, String geom)
            throws Exception {
        boolean fullscreen = false;

        if (StringUtils.isEmpty(geom)) {
            fullscreen = true;
        } else if (StringUtils.equals("multimon", geom)) {
            // rdesktop doesn't support multi-mon. Fall back to full screen.
            fullscreen = true;
        } else if (StringUtils.equals("fullscreen", geom)) {
            fullscreen = true;
        }

        StringBuilder sb = new StringBuilder();

        if (fullscreen) {
            sb.append(" -f");
        } else if (!StringUtils.isEmpty(geom)) {
            sb.append(" -g ").append(geom);
        } else {
            sb.append(" -g 1024x768");
        }

        sb.append(" -r sound:local");

        if (!StringUtils.isEmpty(username)) {
            username = username.replaceAll(" ", "_");
            sb.append(" -u ").append(username);
        }

        if (!StringUtils.isEmpty(domain)) {
            sb.append(" -d ").append(domain);
        }

        Address address = startGatewaySession(desktop);

        sb.append(" ").append(address.getIp()).append(":").append(address.getPort());

        return sb.toString();
    }

    private String generateRdpString(Pool pool, VmInstance desktop, String username, String domain, String geom)
            throws Exception {
        boolean multimon = false;
        boolean fullscreen = false;

        if (StringUtils.isEmpty(geom)) {
            fullscreen = true;
        } else if (StringUtils.equals("multimon", geom)) {
            multimon = true;
        } else if (StringUtils.equals("fullscreen", geom)) {
            fullscreen = true;
        }

        StringBuilder screen = new StringBuilder();

        if (multimon) {
            screen.append("use multimon:i:1\n");
            screen.append("span monitors:i:1\n");
            screen.append("screen mode id:i:2\n");
        } else if (fullscreen) {
            screen.append("use multimon:i:0\n");
            screen.append("span monitors:i:0\n");
            screen.append("screen mode id:i:2\n");
        } else if (!StringUtils.isEmpty(geom)) {
            String[] wh = geom.split("x");
            if (wh != null && wh.length == 2) {
                screen.append("screen mode id:i:1\n");
                screen.append("desktopwidth:i:").append(wh[0]).append('\n');
                screen.append("desktopheight:i:").append(wh[1]).append('\n');
            }
        }

        if (screen.length() == 0) {
            screen.append("screen mode id:i:1\n");
            screen.append("desktopwidth:i:1024\n");
            screen.append("desktopheight:i:768\n");
        }

        Address address = startGatewaySession(desktop);

        String rdp = RdpDescriptor.getRdpDescriptor(address.getIp(), address.getPort(),
                multimon, fullscreen, 0, 0, 32, pool.getRedirects(),  username, null, domain, null);
        return rdp;
    }

    private Address startGatewaySession(VmInstance desktop) {
        Server localServer = _serverService.getLocalServer();
        if (StringUtils.equals(localServer.getPublicAddress(), localServer.getPrivateAddress())) {
            return new Address(desktop.getIpAddress(), 3389);
        }

        int localPort = _gateway.startSession(desktop.getIpAddress(), 3389);
        return new Address(localServer.getPublicAddress(), localPort);
    }

    class Address {
        private final String _ip;
        private final int _port;

        public Address(String ip, int port) {
            _ip = ip;
            _port = port;
        }

        public String getIp() {
            return _ip;
        }

        public int getPort() {
            return _port;
        }
    }
}
