/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.service.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.nimdesk.model.DiskImage;
import com.nimdesk.model.LdapServer;
import com.nimdesk.model.Pool;
import com.nimdesk.model.User;
import com.nimdesk.model.VmImage;
import com.nimdesk.model.VmInstance;

@Controller
public class AgentController implements InitializingBean {
    private static final Logger LOGGER = Logger.getLogger(AgentController.class);

    private static final String RESP_ACTION = "da.action";

    private static final String RESP_ACTION_SYSPREP = "sysprep";
    private static final String RESP_ACTION_JOINDOMAIN = "joindomain";
    private static final String RESP_ACTION_ATTACHDISK = "attachdisk";
    private static final String RESP_ACTION_PREPLOGIN = "preplogin";
    private static final String RESP_ACTION_LOGGOFF = "logoff";

    private static final String RESP_REMOTEAPP_MODE = "remoteapp.mode";

    private static final String RESP_SYSPREP_PASSWORD = "sysprep.password";
    private static final String RESP_SYSPREP_PRODUCTKEY = "sysprep.productKey";
    private static final String RESP_SYSPREP_DOMAIN = "sysprep.domain";
    private static final String RESP_SYSPREP_DOMAIN_ADMIN = "sysprep.domainAdmin";
    private static final String RESP_SYSPREP_DOMAIN_PASSWORD = "sysprep.domainPassword";
    private static final String RESP_SYSPREP_ORGNAME = "sysprep.orgname";
    private static final String RESP_SYSPREP_TIMEZONE = "sysprep.timezone";
    private static final String RESP_SYSPREP_COPYPROFILE = "sysprep.copyProfile";

    private static final String RESP_ATTACHDISK_DRIVELETTER = "attachdisk.driveLetter";

    private static final String RESP_PREPLOGIN_USERNAME = "preplogin.username";
    private static final String RESP_PREPLOGIN_PASSWORD = "preplogin.password";

    private static final String RESP_LOGOFF_NOW = "logoff.now";
    private static final String RESP_LOGOFF_DISCONNECT = "logoff.disconnect";

    @Autowired
    private DiskImage.Service _diskImageService;

    @Autowired
    private VmImage.Service _vmImageService;

    @Autowired
    private Pool.Service _poolService;

    @Autowired
    private LdapServer.Service _ldapServerService;

    @Autowired
    private VmInstance.Service _vmInstanceService;

    @Autowired
    private User.Service _userService;

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(_diskImageService, "A DiskImage.Service must be set");
        Assert.notNull(_vmImageService, "A VmImage.Service must be set");
        Assert.notNull(_poolService, "A Pool.Service must be set");
        Assert.notNull(_ldapServerService, "A LdapServer.Service must be set");
        Assert.notNull(_vmInstanceService, "A VmInstance.Service must be set");
        Assert.notNull(_userService, "A User.Service must be set");
    }

    @RequestMapping(value="/da/ping", method=RequestMethod.GET)
    public void ping(@RequestParam(value="iid", required=false) String instanceId,
            @RequestParam(value="type", required=false) String type,
            @RequestParam(value="ver", required=false) String version,
            @RequestParam(value="proto", required=false) String protocol,
            @RequestParam(value="compname", required=false) String computerName,
            @RequestParam(value="compdomain", required=false) String computerDomain,
            @RequestParam(value="ip", required=false) String ipAddress,
            @RequestParam(value="user", required=false) String username,
            @RequestParam(value="domain", required=false) String domain,
            @RequestParam(value="status", required=false) String status,
            @RequestParam(value="update", required=false) String update,
            HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        if (update != null) {
            LOGGER.info(String.format("ping: instanceId=%s, type=%s, ver=%s, protocol=%s, compname=%s, compdomain=%s, ip=%s, " +
                    "user=%s, domain=%s, status=%s, update=%s",
                    instanceId, type, version, protocol, computerName, computerDomain, ipAddress,
                    username, domain, status, update));
        }

        if (StringUtils.isEmpty(instanceId)) {
            String error = "Agent sent empty iid";
            LOGGER.warn(error);
            response.sendError(401, error);
            return;
        }

        Map<String, String> responseParams = new HashMap<String, String>();
        responseParams.put(RESP_ACTION, "pong");

        boolean succeeded = true;

        if (StringUtils.equals("image", type)) {
            // Image
            succeeded = respondImageVmPing(instanceId, computerName, ipAddress, status, responseParams, response);
        } else if (StringUtils.equals("pool", type)) {
            // Pool template
            succeeded = respondPoolVmPing(instanceId, computerName, status, responseParams, response);
        } else {
            // Desktop
            succeeded = respondDesktopVmPing(instanceId, version, protocol,
                    computerName, computerDomain, ipAddress, username, domain, status, responseParams, response);
        }

        if (succeeded) {
            response.setContentType("text/plain");

            PrintWriter writer = response.getWriter();
            for (Map.Entry<String, String> entry : responseParams.entrySet()) {
                writer.print(entry.getKey() + "=" + entry.getValue());
                writer.print('\n'); // Explicitly write \n to avoid mixing with \r\n on some platforms.
            }
        }
    }

    private boolean respondImageVmPing(String instanceId, String computerName, String ipAddress, String status,
            Map<String, String> responseParams, HttpServletResponse response)
            throws IOException {
        DiskImage diskImage = _diskImageService.getByUuid(instanceId);
        if (diskImage == null) {
            String error = String.format("Agent sent invalid iid %s", instanceId);
            LOGGER.debug(error);
            response.sendError(401, error);
            return false;
        }

        try {
            switch (diskImage.getStatus()) {
            case Importing:
            case ImportingUpdate:
            case Preparing:
                if (StringUtils.equals("sysprep", status)) {
                    LOGGER.info(String.format("Image VM (%s) has started sysprep", computerName));

                    // The VM has initiated sysprep
                    diskImage.setStatus(DiskImage.Status.Preparing);
                    _diskImageService.commit(diskImage);
                } else if (StringUtils.equals("sysprep_done", status)) {
                    LOGGER.info(String.format("Image VM (%s) has completed sysprep", computerName));

                    // The VM has completed sysprep
                    diskImage.setStatus(DiskImage.Status.Prepared);
                    _diskImageService.commit(diskImage);
                } else if (StringUtils.equals("sysprep_failed", status)) {
                    LOGGER.warn(String.format("Image VM (%s) has failed sysprep", computerName));

                    // The VM has failed sysprep
                    diskImage.setStatus(DiskImage.Status.Error);
                    _diskImageService.commit(diskImage);
                } else {
                    LOGGER.info(String.format("Image VM (%s) needs to run sysprep", computerName));

                    VmImage vmImage = _vmImageService.getByUuid(diskImage.getOwnerUuid());
                    if (vmImage == null) {
                        throw new Exception(String.format("Failed to find VM image for disk image %s", instanceId));
                    }

                    // Need VM to sysprep. The VM is put into local workgroup.
                    responseParams.put(RESP_ACTION, RESP_ACTION_SYSPREP);
                    responseParams.put(RESP_SYSPREP_PASSWORD, "nimdesk!!!");
                    //responseParams.put(RESP_SYSPREP_PRODUCTKEY, "");
                    //responseParams.put(RESP_SYSPREP_DOMAIN, "");
                    //responseParams.put(RESP_SYSPREP_DOMAIN_ADMIN, "");
                    //responseParams.put(RESP_SYSPREP_DOMAIN_PASSWORD, "");
                    //responseParams.put(RESP_SYSPREP_DOMAIN_OU, "");
                    responseParams.put(RESP_SYSPREP_ORGNAME, vmImage.getName());
                    responseParams.put(RESP_SYSPREP_TIMEZONE, "004");
                    responseParams.put(RESP_SYSPREP_COPYPROFILE, "1");
                }
                break;

            case Updating:
                if (!StringUtils.isEmpty(ipAddress)
                        && !StringUtils.equals(diskImage.getIpAddress(), ipAddress)) {
                    diskImage.setIpAddress(ipAddress);
                    _diskImageService.commit(diskImage);

                    VmImage vmImage = _vmImageService.getByUuid(diskImage.getOwnerUuid());
                    if (vmImage != null) {
                        vmImage.setUpdatingDiskImageIp(ipAddress);
                        _vmImageService.commit(vmImage);
                    }
                }
                break;
            }

            return true;
        } catch (Exception e) {
            diskImage.setStatus(DiskImage.Status.Error);
            _diskImageService.commit(diskImage);

            LOGGER.warn(e.getMessage());
            response.sendError(401, e.getMessage());

            return false;
        }
    }

    private boolean respondPoolVmPing(String instanceId, String computerName, String status,
            Map<String, String> responseParams, HttpServletResponse response)
            throws IOException {
        DiskImage diskImage = _diskImageService.getByUuid(instanceId);
        if (diskImage == null) {
            String error = String.format("Agent sent invalid iid %s", instanceId);
            LOGGER.debug(error);
            response.sendError(401, error);
            return false;
        }

        try {
            if (diskImage.getStatus() == DiskImage.Status.Cloning
                    || diskImage.getStatus() == DiskImage.Status.Preparing) {
                if (StringUtils.equals("sysprep", status)) {
                    LOGGER.info(String.format("Pool VM (%s) has started sysprep", computerName));

                    // The VM has initiated sysprep
                    diskImage.setStatus(DiskImage.Status.Preparing);
                    _diskImageService.commit(diskImage);
                } else if (StringUtils.equals("sysprep_done", status)) {
                    LOGGER.info(String.format("Pool VM (%s) has completed sysprep", computerName));

                    // The VM has completed sysprep
                    diskImage.setStatus(DiskImage.Status.Prepared);
                    _diskImageService.commit(diskImage);
                } else if (StringUtils.equals("sysprep_failed", status)) {
                    LOGGER.warn(String.format("Pool VM (%s) has failed sysprep", computerName));

                    // The VM has failed sysprep
                    diskImage.setStatus(DiskImage.Status.Error);
                    _diskImageService.commit(diskImage);
                } else {
                    LOGGER.info(String.format("Pool VM (%s) needs to run sysprep", computerName));

                    Pool pool = _poolService.getByUuid(diskImage.getOwnerUuid());
                    if (pool == null) {
                        throw new Exception(String.format("Failed to find pool for disk image %s", instanceId));
                    }

                    if (pool.getType().isApplication()) {
                        responseParams.put(RESP_REMOTEAPP_MODE,  "1");
                    }

                    // Need VM to sysprep. The VM is put into local workgroup.
                    responseParams.put(RESP_ACTION, RESP_ACTION_SYSPREP);
                    responseParams.put(RESP_SYSPREP_PASSWORD, "nimdesk!!!");
                    if (!StringUtils.isEmpty(pool.getProductKey())) {
                        responseParams.put(RESP_SYSPREP_PRODUCTKEY, pool.getProductKey());
                    }
                    if (pool.isDomain()) {
                        LdapServer ldapServer = _ldapServerService.getByDomain(pool.getDomainOrWorkGroup());
                        if (ldapServer == null) {
                            throw new Exception(String.format("Failed to find AD for disk image %s", instanceId));
                        }

                        responseParams.put(RESP_SYSPREP_DOMAIN, ldapServer.getDomain());
                        responseParams.put(RESP_SYSPREP_DOMAIN_ADMIN, ldapServer.getUsername());
                        responseParams.put(RESP_SYSPREP_DOMAIN_PASSWORD, ldapServer.getPassword());
                        //responseParams.put(RESP_SYSPREP_DOMAIN_OU, "");
                    }
                    String prefix = pool.getPrefix();
                    if (prefix.endsWith("-")) {
                        responseParams.put(RESP_SYSPREP_ORGNAME, pool.getPrefix());
                    } else {
                        responseParams.put(RESP_SYSPREP_ORGNAME, pool.getPrefix() + '-');
                    }
                    responseParams.put(RESP_SYSPREP_TIMEZONE, pool.getTimeZone());
                    responseParams.put(RESP_SYSPREP_COPYPROFILE, "1");
                }
            }

            return true;
        } catch (Exception e) {
            diskImage.setStatus(DiskImage.Status.Error);
            _diskImageService.commit(diskImage);

            LOGGER.warn(e.getMessage());
            response.sendError(401, e.getMessage());

            return false;
        }
    }

    private boolean respondDesktopVmPing(String instanceId, String version, String protocol,
            String computerName, String computerDomain, String ipAddress, String username, String domain,
            String status, Map<String, String> responseParams, HttpServletResponse response)
            throws IOException {
        VmInstance vmInstance = _vmInstanceService.getByUuid(instanceId);
        if (vmInstance == null) {
            String error = String.format("Agent sent invalid iid %s", instanceId);
            LOGGER.debug(error);
            response.sendError(401, error);
            return false;
        }
        if (VmInstance.Status.Error == vmInstance.getStatus()) {
            // Don't process any requests if vmInstance is error.
            return true;
        }

        Pool pool = _poolService.getByUuid(vmInstance.getPoolUuid());;

        boolean modified = false;

        // version

        // protocol

        if (!StringUtils.isEmpty(computerName) && !StringUtils.equals(vmInstance.getDnsName(), computerName)) {
            vmInstance.setDnsName(computerName);
            modified = true;
        }

        if (pool.isDomain() && !vmInstance.isInDomin()) {
            // Old agent will send null computerDomain.
            if (computerDomain == null || !computerDomain.isEmpty()) {
                vmInstance.setInDomain(true);
                modified = true;
            } else {
                // Sysprep join domain failed, need to join again.
                if (StringUtils.equals("joindomain_done", status)) {
                    LOGGER.info(String.format("VmInstance (%s) has completed join domain", vmInstance.getName()));

                    // The VM has completed join domain.
                    vmInstance.setInDomain(true);
                    modified = true;
                } else if (StringUtils.equals("joindomain_failed", status)) {
                    LOGGER.error(String.format("VmInstance (%s) has failed to join domain", vmInstance.getName()));

                    // The VM has failed to join domain
                    vmInstance.setStatus(VmInstance.Status.Error);
                    vmInstance.setLastError(VmInstance.Error.JoinDomainError);
                    modified = true;
                } else {
                    LdapServer ldapServer = _ldapServerService.getByDomain(pool.getDomainOrWorkGroup());
                    if (ldapServer == null) {
                        LOGGER.error(String.format("VmInstance (%s) has failed to find domain info", vmInstance.getName()));

                        // The VM has failed to join domain
                        vmInstance.setStatus(VmInstance.Status.Error);
                        vmInstance.setLastError(VmInstance.Error.JoinDomainError);
                        modified = true;
                    } else {
                        LOGGER.info(String.format("VmInstance (%s) needs to join domain \"%s\"",
                                vmInstance.getName(), ldapServer.getDomain()));

                        responseParams.put(RESP_ACTION, RESP_ACTION_JOINDOMAIN);
                        responseParams.put(RESP_SYSPREP_DOMAIN, ldapServer.getDomain());
                        responseParams.put(RESP_SYSPREP_DOMAIN_ADMIN, ldapServer.getUsername());
                        responseParams.put(RESP_SYSPREP_DOMAIN_PASSWORD, ldapServer.getPassword());
                        //responseParams.put(RESP_SYSPREP_DOMAIN_OU, "");

                        return true;
                    }
                }
            }
        }

        if (!StringUtils.isEmpty(ipAddress)) {
            if (!StringUtils.equals(vmInstance.getIpAddress(), ipAddress)) {
                vmInstance.setIpAddress(ipAddress);
                modified = true;
            }

            if (vmInstance.getStatus() == VmInstance.Status.ObtainingIp) {
                vmInstance.setStatus(VmInstance.Status.TakingSnapshot);
                if (VmInstance.Error.SysprepTimeout == vmInstance.getLastError()) {
                    vmInstance.setLastError(null);
                }
                modified = true;
            } else if (vmInstance.getStatus() == VmInstance.Status.Starting) {
                vmInstance.setStatus(VmInstance.Status.Started);
                if (VmInstance.Error.PowerOnError == vmInstance.getLastError()) {
                    vmInstance.setLastError(null);
                }
                vmInstance.setLoginTime(0);
                vmInstance.setLogOffMode(null);
                modified = true;
            }
        }

        VmInstance.LogOffMode logOffMode = null;

        if (StringUtils.isEmpty(username)) {
            if (vmInstance.getStatus() == VmInstance.Status.InSession) {
                // VM user session ended
                vmInstance.setLoginTime(0);
                vmInstance.setLogOffMode(null);
                vmInstance.setStatus(VmInstance.Status.LoggedOff);
                modified = true;
            }
        } else {
            if (vmInstance.getStatus() == VmInstance.Status.Assigned) {
                if (pool != null && pool.isDomain()) {
                    domain = pool.getDomainOrWorkGroup();
                } else {
                    domain = "";
                }

                // VM user session started
                User user = _userService.getByLogonNameDomain(username, domain);
                if (user != null) {
                    vmInstance.setStatus(VmInstance.Status.InSession);
                    vmInstance.setUserUuid(user.getUuid());
                    vmInstance.setLoginTime(System.currentTimeMillis());
                    vmInstance.setLogOffMode(null);
                    modified = true;
                }
            } else if (vmInstance.getStatus() == VmInstance.Status.InSession) {
                logOffMode = vmInstance.getLogOffMode();
            }
        }

        if (modified) {
            vmInstance.setUpdateTime(System.currentTimeMillis());
            _vmInstanceService.commit(vmInstance);
        }

        try {
            if (vmInstance.getStatus() == VmInstance.Status.Assigning) {
                if (StringUtils.equals("preplogin_done", status)) {
                    LOGGER.info(String.format("VmInstance (%s) has completed prep login", vmInstance.getName()));

                    // The VM has completed login prep
                    vmInstance.setAssigned(true);
                    vmInstance.setStatus(VmInstance.Status.Assigned);
                    vmInstance.setUpdateTime(System.currentTimeMillis());
                    _vmInstanceService.commit(vmInstance);
                } else if (StringUtils.equals("preplogin_failed", status)) {
                    LOGGER.warn(String.format("VmInstance (%s) has failed prep login", vmInstance.getName()));

                    // The VM has failed login prep
                    vmInstance.setStatus(VmInstance.Status.AssignError);
                    vmInstance.setUpdateTime(System.currentTimeMillis());
                    _vmInstanceService.commit(vmInstance);
                } else {
                    LOGGER.info(String.format("VmInstance (%s) needs to assign user", vmInstance.getName()));

                    boolean attachedDisk = false;
                    if (StringUtils.equals("attachdisk_done", status)
                            || StringUtils.equals("attachdisk_failed", status)) {
                        attachedDisk = true;
                    }

                    if (!attachedDisk
                            && pool != null
                            && !StringUtils.isEmpty(vmInstance.getUserDataDiskOwner())) {
                        // Need VM to attachdisk.
                        responseParams.put(RESP_ACTION, RESP_ACTION_ATTACHDISK);
                        responseParams.put(RESP_ATTACHDISK_DRIVELETTER, pool.getUserDataDiskDriveLetter());
                    } else {
                        if (pool != null && pool.isDomain()) {
                            // This is domain user - don't really need to prep login.
                            vmInstance.setAssigned(true);
                            vmInstance.setStatus(VmInstance.Status.Assigned);
                            vmInstance.setUpdateTime(System.currentTimeMillis());
                            _vmInstanceService.commit(vmInstance);
                        } else {
                            User user = _userService.getByUuid(vmInstance.getUserUuid());
                            if (user == null) {
                                throw new Exception(String.format("Failed to find user \"%s\" in local directory", vmInstance.getUserUuid()));
                            }

                            // Need VM to prep login for local user.
                            responseParams.put(RESP_ACTION, RESP_ACTION_PREPLOGIN);
                            responseParams.put(RESP_PREPLOGIN_USERNAME, user.getLogonName());
                            responseParams.put(RESP_PREPLOGIN_PASSWORD, user.getPassword());
                        }
                    }
                }
            } else if (logOffMode != null) {
                if (StringUtils.equals("logoff_done", status)) {
                    LOGGER.warn(String.format("VmInstance (%s) has completed logoff", vmInstance.getName()));

                    vmInstance.setLoginTime(0);
                    vmInstance.setLogOffMode(null);
                    vmInstance.setStatus(VmInstance.Status.LoggedOff);
                    vmInstance.setUpdateTime(System.currentTimeMillis());
                    _vmInstanceService.commit(vmInstance);
                } else if (StringUtils.equals("logoff_failed", status)) {
                    LOGGER.warn(String.format("VmInstance (%s) has failed logoff", vmInstance.getName()));

                    // The VM has failed logoff
                    if (Pool.Type.Persistent == pool.getType()) {
                        _vmInstanceService.powerOffVm(pool, vmInstance, null);
                    } else {
                        vmInstance.setUserUuid("");
                        _vmInstanceService.refreshVm(pool, vmInstance, null);
                    }
                } else {
                    responseParams.put(RESP_ACTION, RESP_ACTION_LOGGOFF);
                    if (VmInstance.LogOffMode.DisconnectNow == logOffMode
                            || VmInstance.LogOffMode.LogOffNow == logOffMode) {
                        responseParams.put(RESP_LOGOFF_NOW, "1");
                    }
                    if (VmInstance.LogOffMode.Disconnect == logOffMode
                            || VmInstance.LogOffMode.DisconnectNow == logOffMode) {
                        responseParams.put(RESP_LOGOFF_DISCONNECT, "1");
                    }
                }
            }

            return true;
        } catch (Exception e) {
            vmInstance.setStatus(VmInstance.Status.AssignError);
            vmInstance.setUpdateTime(System.currentTimeMillis());
            _vmInstanceService.commit(vmInstance);

            LOGGER.warn(e.getMessage());
            response.sendError(401, e.getMessage());

            return false;
        }
    }
}
