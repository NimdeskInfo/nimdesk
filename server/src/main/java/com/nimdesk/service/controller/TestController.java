/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.service.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.nimdesk.model.Pool;
import com.nimdesk.model.VmInstance;

@Controller
public class TestController implements InitializingBean {
    private static final Logger LOGGER = Logger.getLogger(TestController.class);

    @Autowired
    private Pool.Service _poolService;

    @Autowired
    private VmInstance.Service _vmInstanceService;

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(_poolService, "A Pool.Service must be set");
        Assert.notNull(_vmInstanceService, "A VmInstance.Service must be set");
    }

    @RequestMapping(value="/ip_addrs/{poolName}", method=RequestMethod.GET)
    public void listPoolIps(@PathVariable String poolName, HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        LOGGER.info(String.format("listPoolIps: poolName=%s", poolName));

        Pool pool = _poolService.getPoolByName(poolName);
        if (pool == null) {
            String error = String.format("Invalid pool name \"%s\"", poolName);
            response.sendError(401, error);
            return;
        }

        response.setContentType("text/plain");
        PrintWriter writer = response.getWriter();

        List<? extends VmInstance> vmInstances = _vmInstanceService.getAllByPool(pool.getUuid(), null);
        if (vmInstances != null) {
            Collections.shuffle(vmInstances);

            for (VmInstance vmInstance : vmInstances) {
                if (!StringUtils.isEmpty(vmInstance.getIpAddress())) {
                    writer.println(vmInstance.getIpAddress());
                }
            }
        }
    }
}
