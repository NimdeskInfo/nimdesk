/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.nimdesk.database.dao.UserDao;
import com.nimdesk.database.vo.UserVO;
import com.nimdesk.model.Pool;
import com.nimdesk.model.PoolUser;
import com.nimdesk.model.User;
import com.nimdesk.service.ObjectPropertyChangeEvent;
import com.nimdesk.util.IdGenerator;
import com.nimdesk.util.Pagination;
import com.nimdesk.util.Pair;

@Service(User.Service.BEAN_NAME)
public class UserServiceImpl extends ObjectPropertyChangeSupport implements User.Service, InitializingBean {
    private static final Logger LOGGER = Logger.getLogger(UserServiceImpl.class);

    @Autowired
    private UserDao _userDao;

    @Autowired
    private PoolUser.Service _poolUserService;

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(_userDao, "A UserDao must be set");
        Assert.notNull(_poolUserService, "A PoolUser.Service must be set");
    }

    @Override
    public Long count() {
        return _userDao.countAll();
    }

    @Override
    public User newInstance() {
        User user = new UserVO();

        // Generate uuid
        user.setUuid(IdGenerator.generateUuid());

        return user;
    }

    @Override
    public List<? extends User> getAll(Pagination pagination) {
        return _userDao.findAll(pagination);
    }

    @Override
    public User getByUuid(String uuid) {
        return _userDao.findByUuid(uuid);
    }

    @Override
    public void commit(User obj) {
        if (!(obj instanceof UserVO)) {
            String error = String.format("commit: passing wrong type %s", obj.getClass().toString());
            LOGGER.error(error);
            throw new IllegalArgumentException(error);
        }

        UserVO user = (UserVO) obj;

        if (user.getId() == 0) {
            _userDao.createNew(user);
            user.resetChangedProperties();
            fireCreateEvent(new ObjectPropertyChangeEvent(user, null));
        } else {
            _userDao.update(user);
            Map<String, Pair<Object, Object>> changedProperties = user.resetChangedProperties();
            fireChangeEvent(new ObjectPropertyChangeEvent(user, changedProperties));
        }
    }

    @Override
    public boolean delete(User obj) {
        if (!(obj instanceof UserVO)) {
            String error = String.format("delete: passing wrong type %s", obj.getClass().toString());
            LOGGER.error(error);
            throw new IllegalArgumentException(error);
        }

        UserVO user = (UserVO) obj;

        try {
            if (user.getId() > 0) {
                return _userDao.delete(user.getId());
            }
        } finally {
            fireDeleteEvent(new ObjectPropertyChangeEvent(user));
        }

        return true;
    }

    @Override
    public List<String> getAllDomains() {
        return _userDao.findDomains();
    }

    @Override
    public List<? extends User> getAllUsers() {
        return _userDao.findAllUsers();
    }

    @Override
    public List<? extends User> getAllGroups(String domain) {
        return _userDao.findAllGroups(domain);
    }

    @Override
    public User getByLogonNameDomain(String logonName, String domain) {
        return _userDao.findByLogonNameDomain(logonName, domain);
    }

    @Override
    public User getByDistinguishedName(String distinguishedName) {
        return _userDao.findByDistinguishedName(distinguishedName);
    }

    @Override
    public List<? extends User> getByFilter(boolean includeUsers, boolean includeGroups,
            String prefix, String domain) {
        return _userDao.findByNameFilter(includeUsers, includeGroups, prefix, domain);
    }

    @Override
    public List<? extends User> getAllUsersForPool(Pool pool) {
        List<? extends PoolUser> poolUserList = _poolUserService.getByPool(pool.getUuid());

        ArrayList<UserVO> userList = new ArrayList<UserVO>();

        for (PoolUser poolUser : poolUserList) {
            UserVO user = _userDao.findByUuid(poolUser.getUserUuid());
            if (user != null) {
                userList.add(user);
            }
        }

        return userList;
    }

    @Override
    public void assignUsersToPool(Pool pool, List<User> users) {
        String poolUuid = pool.getUuid();

        for (User user : users) {
            if (!(user instanceof UserVO)) {
                continue;
            }

            UserVO userVO = (UserVO) user;

            if (userVO.getId() <= 0) {
                UserVO tmpUserVO = _userDao.findByDistinguishedName(userVO.getDistinguishedName());
                if (tmpUserVO == null) {
                    // Create a user entry
                    commit(userVO);
                    //_userDao.createNew(userVO);
                } else {
                    // Update the database entry if any change
                    boolean changed = false;
                    if (tmpUserVO.getType() != userVO.getType()) {
                        tmpUserVO.setType(userVO.getType());
                        changed = true;
                    }
                    if (StringUtils.equals(tmpUserVO.getName(), userVO.getName())) {
                        tmpUserVO.setName(userVO.getName());
                        changed = true;
                    }
                    if (StringUtils.equals(tmpUserVO.getDomain(), userVO.getDomain())) {
                        tmpUserVO.setDomain(userVO.getDomain());
                        changed = true;
                    }
                    if (changed) {
                        commit(tmpUserVO);
                        //_userDao.update(tmpUserVO);
                    }

                    user = tmpUserVO;
                }
            }

            assignPool(user, poolUuid);
        }
    }

    @Override
    public void unassignUsersFromPool(Pool pool, List<User> users) {
        String poolUuid = pool.getUuid();

        for (User user : users) {
            if (!(user instanceof UserVO)) {
                continue;
            }

            UserVO userVO = (UserVO) user;

            if (userVO.getId() <= 0) {
                UserVO tmpUserVO = _userDao.findByDistinguishedName(userVO.getDistinguishedName());
                if (tmpUserVO == null) {
                    // The user entry doesn't exist. Ignore...
                    continue;
                }

                // Update the database entry if any change
                boolean changed = false;
                if (tmpUserVO.getType() != userVO.getType()) {
                    tmpUserVO.setType(userVO.getType());
                    changed = true;
                }
                if (StringUtils.equals(tmpUserVO.getName(), userVO.getName())) {
                    tmpUserVO.setName(userVO.getName());
                    changed = true;
                }
                if (StringUtils.equals(tmpUserVO.getDomain(), userVO.getDomain())) {
                    tmpUserVO.setDomain(userVO.getDomain());
                    changed = true;
                }
                if (changed) {
                    _userDao.update(tmpUserVO);
                }

                user = tmpUserVO;
            }

            unassignPool(user, poolUuid);
        }
    }

    private void assignPool(User user, String poolUuid) {
        _poolUserService.assignUserToPool(user, poolUuid);
    }

    private void unassignPool(User user, String poolUuid) {
        _poolUserService.unassignUserFromPool(user, poolUuid);
    }
}
