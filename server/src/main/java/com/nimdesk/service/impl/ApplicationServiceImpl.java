/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.nimdesk.database.dao.ApplicationDao;
import com.nimdesk.database.vo.ApplicationVO;
import com.nimdesk.model.Application;
import com.nimdesk.service.ObjectPropertyChangeEvent;
import com.nimdesk.util.IdGenerator;
import com.nimdesk.util.Pagination;
import com.nimdesk.util.Pair;

@Service(Application.Service.BEAN_NAME)
public class ApplicationServiceImpl extends ObjectPropertyChangeSupport implements Application.Service, InitializingBean {
    private static final Logger LOGGER = Logger.getLogger(ApplicationServiceImpl.class);

    @Autowired
    private ApplicationDao _applicationDao;

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(_applicationDao, "An ApplicationDao must be set");
    }

    @Override
    public Long count() {
        return _applicationDao.countAll();
    }

    @Override
    public Application newInstance() {
        Application application = new ApplicationVO();

        // Generate uuid
        application.setUuid(IdGenerator.generateUuid());

        return application;
    }

    @Override
    public List<? extends Application> getAll(Pagination pagination) {
        return _applicationDao.findAll(pagination);
    }

    @Override
    public Application getByUuid(String uuid) {
        return _applicationDao.findByUuid(uuid);
    }

    @Override
    public void commit(Application obj) {
        if (!(obj instanceof ApplicationVO)) {
            String error = String.format("commit: passing wrong type %s", obj.getClass().toString());
            LOGGER.error(error);
            throw new IllegalArgumentException(error);
        }

        ApplicationVO application = (ApplicationVO) obj;

        if (application.getId() == 0) {
            _applicationDao.createNew(application);
            application.resetChangedProperties();
            fireCreateEvent(new ObjectPropertyChangeEvent(application, null));
        } else {
            _applicationDao.update(application);
            Map<String, Pair<Object, Object>> changedProperties = application.resetChangedProperties();
            fireChangeEvent(new ObjectPropertyChangeEvent(application, changedProperties));
        }
    }

    @Override
    public boolean delete(Application obj) {
        if (!(obj instanceof ApplicationVO)) {
            String error = String.format("delete: passing wrong type %s", obj.getClass().toString());
            LOGGER.error(error);
            throw new IllegalArgumentException(error);
        }

        ApplicationVO application = (ApplicationVO) obj;

        try {
            if (application.getId() > 0) {
                return _applicationDao.delete(application.getId());
            }
        } finally {
            fireDeleteEvent(new ObjectPropertyChangeEvent(application));
        }

        return true;
    }

    @Override
    public List<? extends Application> getApplicationsByVmImage(String vmImageUuid) {
        return _applicationDao.findByVmImage(vmImageUuid);
    }
}
