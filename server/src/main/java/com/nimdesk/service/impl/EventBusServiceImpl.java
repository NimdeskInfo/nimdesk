/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.service.impl;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.nimdesk.event.EventBase;
import com.nimdesk.event.EventHandler;
import com.nimdesk.service.EventBusService;
import com.google.common.eventbus.EventBus;

/**
 * Implemention for event bus service
 */
@Service(EventBusService.BEAN_NAME)
public class EventBusServiceImpl implements EventBusService {
    private static final Logger LOGGER = Logger.getLogger(EventBusServiceImpl.class);

    private final EventBus _eventBus = new EventBus("nimdesk");

    @Override
    public void register(EventHandler handler) {
        _eventBus.register(handler);
    }

    @Override
    public void unregister(EventHandler handler) {
        _eventBus.unregister(handler);
    }

    @Override
    public void post(EventBase event) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("Post event: " + event.getClass().getName());
        }

        _eventBus.post(event);
    }
}

