/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.nimdesk.database.dao.AdminUserDao;
import com.nimdesk.database.vo.AdminUserVO;
import com.nimdesk.model.AdminUser;
import com.nimdesk.service.ObjectPropertyChangeEvent;
import com.nimdesk.util.IdGenerator;
import com.nimdesk.util.Pagination;
import com.nimdesk.util.Pair;

@Service(AdminUser.Service.BEAN_NAME)
public class AdminUserServiceImpl extends ObjectPropertyChangeSupport implements AdminUser.Service, InitializingBean {
    private static final Logger LOGGER = Logger.getLogger(AdminUserServiceImpl.class);

    @Autowired
    private AdminUserDao _adminUserDao;

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(_adminUserDao, "An AdminUserDao must be set");
    }

    @Override
    public Long count() {
        return _adminUserDao.countAll();
    }

    @Override
    public AdminUser newInstance() {
        AdminUser adminUser = new AdminUserVO();

        // Generate uuid
        adminUser.setUuid(IdGenerator.generateUuid());

        return adminUser;
    }

    @Override
    public List<? extends AdminUser> getAll(Pagination pagination) {
        return _adminUserDao.findAll(pagination);
    }

    @Override
    public AdminUser getByUuid(String uuid) {
        return _adminUserDao.findByUuid(uuid);
    }

    @Override
    public void commit(AdminUser obj) {
        if (!(obj instanceof AdminUserVO)) {
            String error = String.format("commit: passing wrong type %s", obj.getClass().toString());
            LOGGER.error(error);
            throw new IllegalArgumentException(error);
        }

        AdminUserVO adminUser = (AdminUserVO) obj;

        if (adminUser.getId() == 0) {
            _adminUserDao.createNew(adminUser);
            adminUser.resetChangedProperties();
            fireCreateEvent(new ObjectPropertyChangeEvent(adminUser, null));
        } else {
            _adminUserDao.update(adminUser);
            Map<String, Pair<Object, Object>> changedProperties = adminUser.resetChangedProperties();
            fireChangeEvent(new ObjectPropertyChangeEvent(adminUser, changedProperties));
        }
    }

    @Override
    public boolean delete(AdminUser obj) {
        if (!(obj instanceof AdminUserVO)) {
            String error = String.format("delete: passing wrong type %s", obj.getClass().toString());
            LOGGER.error(error);
            throw new IllegalArgumentException(error);
        }

        AdminUserVO adminUser = (AdminUserVO) obj;

        try {
            if (adminUser.getId() > 0) {
                return _adminUserDao.delete(adminUser.getId());
            }
        } finally {
            fireDeleteEvent(new ObjectPropertyChangeEvent(adminUser));
        }

        return true;
    }

    @Override
    public AdminUser getByUsername(String username) {
        return _adminUserDao.findByUsername(username);
    }
}
