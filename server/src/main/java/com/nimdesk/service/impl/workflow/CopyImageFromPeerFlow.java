/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.service.impl.workflow;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.util.Assert;

import com.nimdesk.event.AllocateStorageEvent;
import com.nimdesk.event.AsyncCallback;
import com.nimdesk.event.ConnectHostEvent;
import com.nimdesk.event.CopyDiskImageEvent;
import com.nimdesk.event.Link2LocalReplicaEvent;
import com.nimdesk.event.TemplatizeVmEvent;
import com.nimdesk.hypervisor.HypervisorContext;
import com.nimdesk.model.DiskImage;
import com.nimdesk.model.Host;
import com.nimdesk.model.Pool;
import com.nimdesk.model.Server;
import com.nimdesk.model.Storage;
import com.nimdesk.model.Task;
import com.nimdesk.model.VmImage;
import com.nimdesk.service.EventBusService;
import com.nimdesk.service.Workflow;
import com.nimdesk.service.impl.ServiceLocator;
import com.nimdesk.tracker.TrackerManager;
import com.nimdesk.util.ActionDelegate;
import com.nimdesk.util.Pair;
import com.nimdesk.util.Utils;
import com.nimdesk.vm.TemplatizedVm;

public class CopyImageFromPeerFlow extends Workflow {
    private static final Logger LOGGER = Logger.getLogger(CopyImageFromPeerFlow.class);

    private final long _createTime = System.currentTimeMillis();

    private final EventBusService _eventBusService;
    private final Host.Service _hostService;
    private final Storage.Service _storageService;
    private final VmImage.Service _vmImageService;
    private final DiskImage.Service _diskImageService;
    private final Pool.Service _poolService;
    private final Task.Service _taskService;
    private final TrackerManager _trackerManager;

    private final Server _localServer;
    private final Host _localHost;
    private final DiskImage _diskImage;
    private DiskImage _localBaseDiskImage = null;

    private final Server _peerServer;
    private final Host _peerHost;
    private final DiskImage _peerDiskImage;
    private final Storage _peerStorage;

    private final Task _task;

    // ConnectHostsState result
    private HypervisorContext _peerContext = null;
    private HypervisorContext _localContext = null;

    // ConnectVcState result
    private HypervisorContext _peerVcContext = null;
    private HypervisorContext _localVcContext = null;

    // AllocateStorageState result
    private Storage _localStorage;

    // CopyDiskImageState result
    private String _imageVmRefId = null;

    // TemplatizeVmState result
    private boolean _vmCbrcEnabled;

    public CopyImageFromPeerFlow(Server peerServer, Host peerHost, Storage peerStorage, DiskImage peerDiskImage, DiskImage diskImage,
            final AsyncCallback callback) {
        super();

        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("CopyImageFromPeerFlow()");
        }

        ServiceLocator serviceLocator = ServiceLocator.getInstance();

        _eventBusService = serviceLocator.getEventBusService();
        _hostService = serviceLocator.getHostService();
        _storageService = serviceLocator.getStorageService();
        _vmImageService = serviceLocator.getVmImageService();
        _diskImageService = serviceLocator.getDiskImageService();
        _poolService = serviceLocator.getPoolService();
        _taskService = serviceLocator.getTaskService();
        _trackerManager = serviceLocator.getTrackerManager();

        _diskImage = diskImage;

        Server.Service serverService = serviceLocator.getServerService();
        _localServer = serverService.getLocalServer();
        if (_localServer == null) {
            throw new RuntimeException(String.format("Unable to find local server to copy image \"%s\"",
                    peerDiskImage.getName()));
        }

        _localHost = _hostService.getLocalHost();
        if (_localHost == null) {
            throw new RuntimeException(String.format("Unable to find local host to copy image \"%s\"",
                    peerDiskImage.getName()));
        }

        _peerServer = peerServer;
        _peerHost = peerHost;
        _peerStorage = peerStorage;
        _peerDiskImage = peerDiskImage;

        _vmCbrcEnabled = _peerDiskImage.isCbrcEnabled();

        _task = createTask(_peerDiskImage, _diskImage, (callback != null) ? callback.getRequestor() : null);

        append(new ConnectPeerHostState(2), null);
        append(new ConnectLocalHostState(5), null);
        if (!StringUtils.isEmpty(_peerHost.getVCenterIp())
                && !StringUtils.isEmpty(_peerHost.getVCenterUsername())
                && !StringUtils.isEmpty(_peerHost.getVCenterPassword())) {
            append(new ConnectPeerVCenterState(8), null);
        }
        if (!StringUtils.isEmpty(_localHost.getVCenterIp())
                && !StringUtils.isEmpty(_localHost.getVCenterUsername())
                && !StringUtils.isEmpty(_localHost.getVCenterPassword())) {
            append(new ConnectLocalVCenterState(10), null);
        }
        append(new AllocateStorageState(15), null);
        append(new CopyDiskImageState(20, 90), null);

        if (DiskImage.Type.ImageFullBase == _diskImage.getType()) {
            append(new TemplatizeVmState(90), null);
        } else {
            _localBaseDiskImage = _diskImageService.getLocalDiskImageByGuid(_diskImage.getParentGuid());
            if (_localBaseDiskImage != null) {
                append(new Relink2LocalParentImageState(90), null);
            }
        }

        setCallback(new AsyncCallback() {
            @Override
            public void onSuccess(Object result) {
                disconnectHosts();

                DiskImage diskImage = _diskImageService.getByUuid(_diskImage.getUuid());
                if (diskImage == null
                        || DiskImage.Status.Deleting == diskImage.getStatus()) {
                    LOGGER.error(String.format("Unable to find DiskImage id=\"%s\" in db", _diskImage.getUuid()));
                    throw new RuntimeException("Can't locate the entity");
                }

                diskImage.setCbrcEnabled(_vmCbrcEnabled);
                diskImage.setStatus(DiskImage.Status.Ready);
                _diskImageService.commit(diskImage);

                _task.setEndTime(System.currentTimeMillis());
                _task.setStatus(Task.Status.Completed);
                _task.setDetails("Succeeded");
                _taskService.commit(_task);

                _trackerManager.removeProgress(_diskImage.getUuid());
                _trackerManager.removeProgress(_task.getUuid());

                LOGGER.info(String.format("CopyImageFromPeerFlow: Copied disk image \"%s\" from server \"%s\"",
                        _diskImage.getName(), _peerServer.getPublicAddress()));

                if (callback != null) {
                    callback.onSuccess(result);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                disconnectHosts();

                DiskImage diskImage = _diskImageService.getByUuid(_diskImage.getUuid());
                if (diskImage == null
                        || DiskImage.Status.Deleting == diskImage.getStatus()) {
                    LOGGER.warn(String.format("Unable to find DiskImage id=\"%s\" in db", _diskImage.getUuid()));
                } else {
                    diskImage.setStatus(DiskImage.Status.Error);
                    _diskImageService.commit(diskImage);
                }

                _task.setEndTime(System.currentTimeMillis());
                _task.setStatus(Task.Status.Failed);
                _task.setDetails(t.getMessage());
                _taskService.commit(_task);

                _trackerManager.removeProgress(_diskImage.getUuid());
                _trackerManager.removeProgress(_task.getUuid());

                LOGGER.error(String.format("CopyImageFromPeerFlow: Failed to copy disk image \"%s\" from server \"%s\" - %s",
                        _diskImage.getName(), _peerServer.getPublicAddress(), t.getMessage()), t);

                if (callback != null) {
                    callback.onFailure(t);
                }
            }

            @Override
            public void progress(int percentage) {
                if (callback != null) {
                    callback.progress(percentage);
                }
            }

            @Override
            public String getRequestor() {
                return (callback != null) ? callback.getRequestor() : null;
            }
        });
    }

    public Task getTask() {
        return _task;
    }

    private void disconnectHosts() {
        if (_peerContext != null) {
            _peerContext.disconnect();
            _peerContext = null;
        }
        if (_localContext != null) {
            _localContext.disconnect();
            _localContext = null;
        }
        if (_peerVcContext != null) {
            _peerVcContext.disconnect();
            _peerVcContext = null;
        }
        if (_localVcContext != null) {
            _localVcContext.disconnect();
            _localVcContext = null;
        }
    }

    private Task createTask(DiskImage peerDiskImage, DiskImage diskImage, String requestor) {
        Assert.isTrue(StringUtils.equals(diskImage.getGuid(), peerDiskImage.getGuid()));
        Assert.isTrue(StringUtils.equals(diskImage.getHostUuid(), _localHost.getUuid()));
        Assert.isTrue(StringUtils.equals(diskImage.getOwnerUuid(), peerDiskImage.getOwnerUuid()));

        diskImage.setStorageUuid("");
        diskImage.setName(peerDiskImage.getName());
        diskImage.setVersion(peerDiskImage.getVersion());
        diskImage.setType(peerDiskImage.getType());
        diskImage.setStatus(DiskImage.Status.Copying);
        _diskImageService.commit(diskImage);

        Task task = _taskService.newInstance();

        task.setType(Task.Type.CopyImageFromPeer);
        task.setStartTime(_createTime);
        task.setServerUuid(_localServer.getUuid());

        if (diskImage.getType().isImageBase()) {
            VmImage vmImage = _vmImageService.getByUuid(diskImage.getOwnerUuid());
            if (vmImage != null) {
                task.setTargetType(VmImage.class.getSimpleName());
                task.setTargetUuid(vmImage.getUuid());
                task.setTargetName(vmImage.getName());
            }
        } else {
            Pool pool = _poolService.getByUuid(diskImage.getOwnerUuid());
            if (pool != null) {
                task.setTargetType(Pool.class.getSimpleName());
                task.setTargetUuid(pool.getUuid());
                task.setTargetName(pool.getName());
            }
        }

        if (StringUtils.isEmpty(task.getTargetType())) {
            task.setTargetType(DiskImage.class.getSimpleName());
            task.setTargetUuid(diskImage.getUuid());
            task.setTargetName(diskImage.getName());
        }

        task.setStatus(Task.Status.InProgress);
        task.setDetails("Starting");

        _taskService.commit(task);

        return task;
    }

    private class ConnectPeerHostState implements Workflow.StateRunnable {
        private final int _progressStart;

        ConnectPeerHostState(int progressStart) {
            _progressStart = progressStart;
        }

        @Override
        public void run(AsyncCallback callback) {
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace("ConnectPeerHostState.run()");
            }

            _task.setDetails("Preparing");
            _taskService.commit(_task);

            _trackerManager.updateProgress(_diskImage.getUuid(), _progressStart, null, "Preparing...");
            _trackerManager.updateProgress(_task.getUuid(), _progressStart, null, "Preparing...");

            ConnectHostEvent event = new ConnectHostEvent(_peerHost.getAddress(), 0,
                    _peerHost.getUsername(), _peerHost.getPassword(), callback);
            _eventBusService.post(event);
        }

        @Override
        public void setResult(Object result) {
            _peerContext = (HypervisorContext) result;
        }
    }

    private class ConnectLocalHostState implements Workflow.StateRunnable {
        private final int _progressStart;

        ConnectLocalHostState(int progressStart) {
            _progressStart = progressStart;
        }

        @Override
        public void run(AsyncCallback callback) {
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace("ConnectLocalHostState.run()");
            }

            _task.setDetails("Preparing");
            _taskService.commit(_task);

            _trackerManager.updateProgress(_diskImage.getUuid(), _progressStart, null, "Preparing...");
            _trackerManager.updateProgress(_task.getUuid(), _progressStart, null, "Preparing...");

            ConnectHostEvent event = new ConnectHostEvent(_localHost.getAddress(), 0,
                    _localHost.getUsername(), _localHost.getPassword(), callback);
            _eventBusService.post(event);
        }

        @Override
        public void setResult(Object result) {
            _localContext = (HypervisorContext) result;
        }
    }

    private class ConnectPeerVCenterState implements Workflow.StateRunnable {
        private final int _progressStart;

        ConnectPeerVCenterState(int progressStart) {
            _progressStart = progressStart;
        }

        @Override
        public void run(AsyncCallback callback) {
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace("ConnectPeerVCenterState.run()");
            }

            _task.setDetails("Preparing");
            _taskService.commit(_task);

            _trackerManager.updateProgress(_diskImage.getUuid(), _progressStart, null, "Preparing...");
            _trackerManager.updateProgress(_task.getUuid(), _progressStart, null, "Preparing...");

            String vCenterAddress = _peerHost.getVCenterAddress();
            if (StringUtils.isEmpty(vCenterAddress)) {
                vCenterAddress = _peerHost.getVCenterIp();
            }

            ConnectHostEvent event = new ConnectHostEvent(vCenterAddress, 0,
                    _peerHost.getVCenterUsername(), _peerHost.getVCenterPassword(), callback);
            _eventBusService.post(event);
        }

        @Override
        public void setResult(Object result) {
            _peerVcContext = (HypervisorContext) result;
        }
    }

    private class ConnectLocalVCenterState implements Workflow.StateRunnable {
        private final int _progressStart;

        ConnectLocalVCenterState(int progressStart) {
            _progressStart = progressStart;
        }

        @Override
        public void run(AsyncCallback callback) {
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace("ConnectLocalVCenterState.run()");
            }

            _task.setDetails("Preparing");
            _taskService.commit(_task);

            _trackerManager.updateProgress(_diskImage.getUuid(), _progressStart, null, "Preparing...");
            _trackerManager.updateProgress(_task.getUuid(), _progressStart, null, "Preparing...");

            String vCenterAddress = _localHost.getVCenterAddress();
            if (StringUtils.isEmpty(vCenterAddress)) {
                vCenterAddress = _localHost.getVCenterIp();
            }

            ConnectHostEvent event = new ConnectHostEvent(vCenterAddress, 0,
                    _localHost.getVCenterUsername(), _localHost.getVCenterPassword(), callback);
            _eventBusService.post(event);
        }

        @Override
        public void setResult(Object result) {
            _localVcContext = (HypervisorContext) result;
        }
    }

    private class AllocateStorageState implements Workflow.StateRunnable {
        private final int _progressStart;

        AllocateStorageState(int progressStart) {
            _progressStart = progressStart;
        }

        @Override
        public void run(AsyncCallback callback) {
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace("AllocateStorageState.run()");
            }

            _task.setDetails(String.format("Allocate storage for copying \"%s\"", _diskImage.getName()));
            _taskService.commit(_task);

            _trackerManager.updateProgress(_diskImage.getUuid(), _progressStart, null, "Allocating storage...");
            _trackerManager.updateProgress(_task.getUuid(), _progressStart, null, "Allocating storage...");

            AllocateStorageEvent event = new AllocateStorageEvent(_localContext,
                    _storageService.getHostStorages(_localHost.getUuid()),
                    CopyImageFromPeerFlow.class.getName(), callback);
            _eventBusService.post(event);
        }

        @Override
        public void setResult(Object result) {
            _localStorage = (Storage) result;
        }
    }

    private class CopyDiskImageState implements Workflow.StateRunnable {
        private final int _progressStart;
        private final int _progressEnd;
        private int _lastProgress = 0;

        CopyDiskImageState(int progressStart, int progressEnd) {
            _progressStart = progressStart;
            _progressEnd = progressEnd;
        }

        @Override
        public void run(AsyncCallback callback) {
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace("CopyDiskImageState.run()");
            }

            DiskImage diskImage = _diskImageService.getByUuid(_diskImage.getUuid());
            if (diskImage == null
                    || DiskImage.Status.Deleting == diskImage.getStatus()) {
                LOGGER.error(String.format("Unable to find DiskImage id=\"%s\" in db", _diskImage.getUuid()));
                throw new RuntimeException("Can't locate the entity");
            }

            diskImage.setStorageUuid(_localStorage.getUuid());
            _diskImageService.commit(diskImage);

            _task.setDetails(String.format("Copying image \"%s\"", _diskImage.getName()));
            _taskService.commit(_task);

            _trackerManager.updateProgress(_diskImage.getUuid(), _progressStart, null, "Replicating image...");
            _trackerManager.updateProgress(_task.getUuid(), _progressStart, null, "Replicating image...");

            _lastProgress = _progressStart;

            // Check if _peerStorage and _localStorage are the same shared storage.
            if (StringUtils.equals(_peerStorage.getName(), _peerHost.getSharedDatastoreName())
                    && StringUtils.equals(_peerStorage.getRefId(), _peerHost.getSharedDatastoreRefId())
                    && StringUtils.equals(_localStorage.getName(), _localHost.getSharedDatastoreName())
                    && StringUtils.equals(_localStorage.getRefId(), _localHost.getSharedDatastoreRefId())) {
                LOGGER.info(String.format("CopyImageFromPeerFlow: Peer storage \"%s\"(%s) and local storage \"%s\"(%s) are the same shared storage, no need to copy image",
                        _peerStorage.getName(), _peerStorage.getRefId(), _localStorage.getName(), _localStorage.getRefId()));

                if (callback != null) {
                    callback.onSuccess(null);
                }
                return;
            }

            Map<String, String> customProps = new HashMap<String, String>();
            customProps.put("guestinfo.nimdesk.type", "image");
            customProps.put("guestinfo.nimdesk.iid", _diskImage.getUuid());
            customProps.put("guestinfo.nimdesk.serverUrl",
                    String.format("http://%s:8080/", _localServer.getPrivateAddress()));

            CopyDiskImageEvent event = new CopyDiskImageEvent(_peerContext, _peerHost, _peerStorage,
                    _diskImage, _localContext, _localHost, _localStorage,
                    _localHost.getVmNetwork(), customProps,
                    _peerVcContext, _peerHost.getEsxUuid(),
                    _localVcContext, _localHost.getEsxUuid(),
                    Workflow.wrapCallbackWithProgressUpdater(callback, new ActionDelegate<Integer>() {
                        @Override
                        public void action(Integer param) {
                            int percentage = Utils.convertFullPercentage(param, _progressStart, _progressEnd);
                            if (_lastProgress != percentage) {
                                _trackerManager.updateProgress(_diskImage.getUuid(), percentage);
                                _trackerManager.updateProgress(_task.getUuid(), percentage);
                                _lastProgress = percentage;
                            }
                        }
                    }));
            _eventBusService.post(event);
        }

        @Override
        @SuppressWarnings("unchecked")
        public void setResult(Object result) {
            Pair<String, String> vmRefIdPath = (Pair<String, String>) result;
            if (vmRefIdPath != null) {
                _imageVmRefId = vmRefIdPath.getFirst();
            }
        }
    }

    private class Relink2LocalParentImageState implements Workflow.StateRunnable {
        private final int _progressStart;

        Relink2LocalParentImageState(int progressStart) {
            _progressStart = progressStart;
        }

        @Override
        public void run(AsyncCallback callback) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Relink2LocalParentImageState.run()");
            }

            _task.setDetails(String.format("Preparing new image", _diskImage.getName()));
            _taskService.commit(_task);

            _trackerManager.updateProgress(_diskImage.getUuid(), _progressStart, null, "Converting to image...");
            _trackerManager.updateProgress(_task.getUuid(), _progressStart, null, "Converting to image...");

            Storage baseStorage = _storageService.getByUuid(_localBaseDiskImage.getStorageUuid());

            Link2LocalReplicaEvent event = new Link2LocalReplicaEvent(_localContext, _localBaseDiskImage,
                    baseStorage, null, _diskImage.getName(), _localStorage, _localHost.getVmNetwork(), null, callback);
            _eventBusService.post(event);
        }

        @Override
        public void setResult(Object result) {
        }
    }

    private class TemplatizeVmState implements Workflow.StateRunnable {
        private final int _progressStart;

        TemplatizeVmState(int progressStart) {
            _progressStart = progressStart;
        }

        @Override
        public void run(final AsyncCallback callback) {
            LOGGER.info(String.format("TemplatizeVmState.run() - vm: %s", _diskImage.getName()));

            _task.setDetails(String.format("Converting vm \"%s\" to image", _diskImage.getName()));
            _taskService.commit(_task);

            _trackerManager.updateProgress(_diskImage.getUuid(), _progressStart, null, "Converting to image...");
            _trackerManager.updateProgress(_task.getUuid(), _progressStart, null, "Converting to image...");

            VmImage vmImage = _vmImageService.getByUuid(_diskImage.getOwnerUuid());
            if (vmImage == null) {
                LOGGER.error(String.format("Unable to find vmImage id=\"%s\" for diskImage \"%s\" in db",
                        _diskImage.getOwnerUuid(), _diskImage.getName()));
                if (callback != null) {
                    callback.onFailure(new Exception("No valid image"));
                    return;
                }
            }

            if (_imageVmRefId == null) {
                LOGGER.error(String.format("Failed to register vm for diskImage \"%s\" on the local host",
                        _diskImage.getName()));
                if (callback != null) {
                    callback.onFailure(new Exception("No valid disk image"));
                    return;
                }
            }

            DiskImage diskImage = _diskImageService.getByUuid(_diskImage.getUuid());
            if (diskImage == null
                    || DiskImage.Status.Deleting == diskImage.getStatus()) {
                LOGGER.error(String.format("Unable to find DiskImage id=\"%s\" in db", _diskImage.getUuid()));
                throw new RuntimeException("Can't locate the entity");
            }

            diskImage.setVmRefId(_imageVmRefId);
            _diskImageService.commit(diskImage);

            // No need to take snapshot as image copying has copied everything including snapshot.
            TemplatizeVmEvent event = new TemplatizeVmEvent(_localContext, _imageVmRefId, null,
                    _localHost.isCbrcEnabled(), _localVcContext, _localHost.getEsxUuid(),
                    new AsyncCallback() {
                @Override
                public void onSuccess(Object result) {
                    LOGGER.info(String.format("CopyImageFromPeerFlow: Templatized base image \"%s\"", _diskImage.getName()));

                    if (callback != null) {
                        callback.onSuccess(result);
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    LOGGER.warn(String.format("CopyImageFromPeerFlow: Failed to templatize base imagel \"%s\" - %s. Continue...",
                            _diskImage.getName(), t.getMessage()), t);

                    // Continue the flow even if this step fails.
                    if (callback != null) {
                        TemplatizedVm templatizedVm = new TemplatizedVm();
                        templatizedVm.setCbrcEnabled(false);
                        callback.onSuccess(templatizedVm);
                    }
                }

                @Override
                public void progress(int percentage) {
                }

                @Override
                public String getRequestor() {
                    return CopyImageFromPeerFlow.this.getRequestor();
                }
            });
            _eventBusService.post(event);
        }

        @Override
        public void setResult(Object result) {
            TemplatizedVm templatizedVm = (TemplatizedVm) result;
            _vmCbrcEnabled = templatizedVm.isCbrcEnabled();
        }
    }
}
