/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.service.impl.workflow;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.nimdesk.event.AsyncCallback;
import com.nimdesk.event.ConnectHostEvent;
import com.nimdesk.event.DeleteStorageDirectoryEvent;
import com.nimdesk.hypervisor.HypervisorContext;
import com.nimdesk.model.DiskImage;
import com.nimdesk.model.EventLog;
import com.nimdesk.model.Host;
import com.nimdesk.model.Pool;
import com.nimdesk.model.Server;
import com.nimdesk.model.Storage;
import com.nimdesk.model.Task;
import com.nimdesk.model.VmImage;
import com.nimdesk.service.EventBusService;
import com.nimdesk.service.Workflow;
import com.nimdesk.service.impl.ServiceLocator;
import com.nimdesk.util.Pair;

/**
 * Step 1: Connect to host
 * Step 2: Delete VM directory from datastore
 */
public class DeleteImageFlow extends Workflow {
    private static final Logger LOGGER = Logger.getLogger(DeleteImageFlow.class);

    private final long _createTime = System.currentTimeMillis();

    private final EventBusService _eventBusService;
    private final Host.Service _hostService;
    private final Storage.Service _storageService;
    private final Pool.Service _poolService;
    private final VmImage.Service _vmImageService;
    private final DiskImage.Service _diskImageService;
    private final Task.Service _taskService;
    private final EventLog.Service _eventLogService;

    private final Server _localServer;
    private final Host _host;
    private final VmImage _vmImage;
    private final List<? extends DiskImage> _diskImages;
    private final Task _task;

    // ConnectHostState result
    private HypervisorContext _hypervisorContext;

    public DeleteImageFlow(VmImage vmImage, final AsyncCallback callback) {
        super();

        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("DeleteImageFlow()");
        }

        ServiceLocator serviceLocator = ServiceLocator.getInstance();

        _eventBusService = serviceLocator.getEventBusService();
        _hostService = serviceLocator.getHostService();
        _storageService = serviceLocator.getStorageService();
        _poolService = serviceLocator.getPoolService();
        _vmImageService = serviceLocator.getVmImageService();
        _diskImageService = serviceLocator.getDiskImageService();
        _taskService = serviceLocator.getTaskService();
        _eventLogService = serviceLocator.getEventLogService();

        Server.Service serverService = serviceLocator.getServerService();
        _localServer = serverService.getLocalServer();
        if (_localServer == null) {
            throw new RuntimeException(String.format("Unable to find host to delete image \"%s\"", vmImage.getName()));
        }

        _host = _hostService.getLocalHost();
        if (_host == null) {
            throw new RuntimeException(String.format("Unable to find host to delete image \"%s\"", vmImage.getName()));
        }

        _vmImage = vmImage;

        if (VmImage.Status.Deleting != _vmImage.getStatus()) {
            _vmImage.setStatus(VmImage.Status.Deleting);
            _vmImage.setUpdateTime(System.currentTimeMillis());
            _vmImageService.commit(_vmImage);
        }

        _diskImages = getDiskImage(_vmImage, _host.getUuid());
        _task = createTask(_vmImage, (callback != null) ? callback.getRequestor() : null);

        append(new SanityCheckState(), null);
        if (_diskImages != null && !_diskImages.isEmpty()) {
            Set<DiskImage> deletedDiskImages = new HashSet<DiskImage>();

            for (DiskImage diskImage : _diskImages) {
                if ((DiskImage.Status.Updating == diskImage.getStatus())
                        || !StringUtils.isEmpty(diskImage.getVmRefId())) {
                    _diskImageService.deleteDiskImage(diskImage, null);
                    deletedDiskImages.add(diskImage);
                }
            }

            for (DiskImage diskImage : deletedDiskImages) {
                _diskImages.remove(diskImage);
            }

            if (!_diskImages.isEmpty()) {
                append(new ConnectHostState(), null);

                for (DiskImage diskImage : _diskImages) {
                    append(new DeleteDiskImageState(diskImage), null);
                }
            }
        }

        setCallback(new AsyncCallback() {
            @Override
            @SuppressWarnings("unchecked")
            public void onSuccess(Object result) {
                disconnectHost();

                if (_diskImages != null) {
                    for (DiskImage diskImage : _diskImages) {
                        try {
                            _diskImageService.delete(diskImage);
                        } catch (Exception e) {
                            // Ignore error.
                        }
                    }
                }

                _task.setEndTime(System.currentTimeMillis());
                _task.setStatus(Task.Status.Completed);
                _task.setDetails("Succeeded");
                _taskService.commit(_task);

                _eventLogService.addVmImageEventLog(EventLog.Severity.Info,
                        _vmImage, "info.image.delete.succeeded", getRequestor(), _task,
                        new Pair<String, String>("user", getRequestor()),
                        new Pair<String, String>("image", _vmImage.getName()));

                LOGGER.info(String.format("DeleteImageFlow: Deleted disk images for image \"%s\"", _vmImage.getName()));

                if (callback != null) {
                    callback.onSuccess(result);
                }
            }

            @Override
            @SuppressWarnings("unchecked")
            public void onFailure(Throwable t) {
                disconnectHost();

                if (_diskImages == null || _diskImages.isEmpty()) {
                    // If no disk image can be found, we consider the deletion is successful.
                    onSuccess(null);
                    return;
                }

                _task.setEndTime(System.currentTimeMillis());
                _task.setStatus(Task.Status.Failed);
                _task.setDetails(t.getMessage());
                _taskService.commit(_task);

                _eventLogService.addVmImageEventLog(EventLog.Severity.Error,
                        _vmImage, "error.image.delete.failed", getRequestor(), _task,
                        new Pair<String, String>("user", getRequestor()),
                        new Pair<String, String>("image", _vmImage.getName()));

                LOGGER.error(String.format("DeleteImageFlow: Failed to delete disk images for image \"%s\" - %s",
                        _vmImage.getName(), t.getMessage()), t);

                if (callback != null) {
                    callback.onFailure(t);
                }
            }

            @Override
            public void progress(int percentage) {
                if (callback != null) {
                    callback.progress(percentage);
                }
            }

            @Override
            public String getRequestor() {
                return (callback != null) ? callback.getRequestor() : null;
            }
        });
    }

    public Task getTask() {
        return _task;
    }

    private void disconnectHost() {
        if (_hypervisorContext != null) {
            _hypervisorContext.disconnect();
            _hypervisorContext = null;
        }
    }

    @SuppressWarnings("unchecked")
    private Task createTask(VmImage vmImage, String requestor) {
        Task task = _taskService.newInstance();

        task.setType(Task.Type.DeleteImage);
        task.setStartTime(_createTime);
        task.setServerUuid(_localServer.getUuid());

        task.setTargetType(VmImage.class.getSimpleName());
        task.setTargetUuid(vmImage.getUuid());
        task.setTargetName(vmImage.getName());

        task.setStatus(Task.Status.InProgress);
        task.setDetails("Starting");

        _taskService.commit(task);

        _eventLogService.addVmImageEventLog(EventLog.Severity.Info,
                vmImage, "info.image.delete.start", requestor, task,
                new Pair<String, String>("user", requestor),
                new Pair<String, String>("image", vmImage.getName()));

        return task;
    }

    private List<? extends DiskImage> getDiskImage(VmImage vmImage, String hostUuid) {
        return _diskImageService.getLocalDiskImagesByOwner(vmImage.getUuid());
    }

    private class SanityCheckState implements Workflow.StateRunnable {
        @Override
        public void run(AsyncCallback callback) {
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace("SanityCheckState.run()");
            }

            try {
                if (_diskImages == null || _diskImages.isEmpty()) {
                    LOGGER.error(String.format("Image (%s) doesn't have a valid disk image", _vmImage.getName()));
                    throw new Exception("No valid image on this server");
                }

                if (_poolService.countByVmImage(_vmImage.getUuid()) > 0) {
                    LOGGER.warn(String.format("Image (%s) still has spawned desktop pools", _vmImage.getName()));
                    throw new Exception("The image is still being used");
                }

                if (callback != null) {
                    callback.onSuccess(null);
                }
            } catch (Exception e) {
                if (callback != null) {
                    callback.onFailure(e);
                }
            }
        }

        @Override
        public void setResult(Object result) {
        }
    }

    private class ConnectHostState implements Workflow.StateRunnable {
        @Override
        public void run(AsyncCallback callback) {
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace("ConnectHostState.run()");
            }

            _task.setDetails("Preparing");
            _taskService.commit(_task);

            ConnectHostEvent event = new ConnectHostEvent(_host.getAddress(), 0,
                    _host.getUsername(), _host.getPassword(), callback);
            _eventBusService.post(event);
        }

        @Override
        public void setResult(Object result) {
            _hypervisorContext = (HypervisorContext) result;
        }
    }

    private class DeleteDiskImageState implements Workflow.StateRunnable {
        private final DiskImage _diskImage;

        DeleteDiskImageState(DiskImage diskImage) {
            _diskImage = diskImage;
        }

        @Override
        public void run(AsyncCallback callback) {
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace("DeleteDiskImageState.run()");
            }

            _task.setDetails(String.format("Deleting disk image for image \"%s\"", _vmImage.getName()));
            _taskService.commit(_task);

            Storage storage = _storageService.getByUuid(_diskImage.getStorageUuid());
            if (storage != null) {
                DeleteStorageDirectoryEvent event = new DeleteStorageDirectoryEvent(_hypervisorContext,
                        _diskImage.getName(), storage, callback);
                _eventBusService.post(event);

                return;
            }

            // If storage is not found, succeed.
            if (callback != null) {
                callback.onSuccess(null);
            }
        }

        @Override
        public void setResult(Object result) {
        }
    }
}
