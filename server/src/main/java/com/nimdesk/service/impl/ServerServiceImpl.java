/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.nimdesk.database.dao.ServerDao;
import com.nimdesk.database.vo.ServerVO;
import com.nimdesk.event.AsyncCallback;
import com.nimdesk.event.DisjoinClusterEvent;
import com.nimdesk.model.Host;
import com.nimdesk.model.Server;
import com.nimdesk.service.EventBusService;
import com.nimdesk.service.ObjectPropertyChangeEvent;
import com.nimdesk.util.IdGenerator;
import com.nimdesk.util.Pagination;
import com.nimdesk.util.Pair;

@Service(Server.Service.BEAN_NAME)
public class ServerServiceImpl extends ObjectPropertyChangeSupport implements Server.Service, InitializingBean {
    private static final Logger LOGGER = Logger.getLogger(ServerServiceImpl.class);

    @Autowired
    private EventBusService _eventBusService;

    @Autowired
    private ServerDao _serverDao;

    @Autowired
    private Host.Service _hostService;

    // Cached local server
    private Server _localServer = null;
    private String _localServerUuid = null;
    private final Object _localServerMutex = new Object();

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(_eventBusService, "An EventBusService must be set");
        Assert.notNull(_serverDao, "A ServerDao must be set");
        Assert.notNull(_hostService, "A Host.Service must be set");
    }

    @Override
    public Long count() {
        return _serverDao.countAll();
    }

    @Override
    public Server newInstance() {
        Server server = new ServerVO();

        // Generate uuid
        server.setUuid(IdGenerator.generateUuid());

        return server;
    }

    @Override
    public List<? extends Server> getAll(Pagination pagination) {
        return _serverDao.findAll(pagination);
    }

    @Override
    public Server getByUuid(String uuid) {
        return _serverDao.findByUuid(uuid);
    }

    @Override
    public void commit(Server obj) {
        if (!(obj instanceof ServerVO)) {
            String error = String.format("commit: passing wrong type %s", obj.getClass().toString());
            LOGGER.error(error);
            throw new IllegalArgumentException(error);
        }

        ServerVO server = (ServerVO) obj;

        if (server.getId() == 0) {
            _serverDao.createNew(server);
            server.resetChangedProperties();
            fireCreateEvent(new ObjectPropertyChangeEvent(server, null));
        } else {
            _serverDao.update(server);
            Map<String, Pair<Object, Object>> changedProperties = server.resetChangedProperties();
            fireChangeEvent(new ObjectPropertyChangeEvent(server, changedProperties));
        }
    }

    @Override
    public boolean delete(Server obj) {
        if (!(obj instanceof ServerVO)) {
            String error = String.format("delete: passing wrong type %s", obj.getClass().toString());
            LOGGER.error(error);
            throw new IllegalArgumentException(error);
        }

        ServerVO server = (ServerVO) obj;

        try {
            if (server.getId() > 0) {
                return _serverDao.delete(server.getId());
            }
        } finally {
            synchronized(_localServerMutex) {
                if (StringUtils.equals(_localServerUuid, server.getUuid())) {
                    _localServer = null;
                    _localServerUuid = null;
                }
            }

            fireDeleteEvent(new ObjectPropertyChangeEvent(server));
        }

        return true;
    }

    @Override
    public Long countByStatus(Server.Status... status) {
        return _serverDao.countByStatus(status);
    }

    @Override
    public List<? extends Server> getRunningServers() {
        return _serverDao.findByStatus(Server.Status.Running);
    }

    @Override
    public Server getLocalServer() {
        synchronized(_localServerMutex) {
            if (_localServer != null) {
                return _localServer;
            }
        }

        Server localServer = _serverDao.findLocalServer();
        if (localServer != null) {
            synchronized(_localServerMutex) {
                if (StringUtils.isEmpty(_localServerUuid)) {
                    _localServer = localServer;
                    _localServerUuid = localServer.getUuid();
                }
            }
        }

        return localServer;
    }

    @Override
    public boolean isLocalServer(String serverUuid) {
        synchronized(_localServerMutex) {
            if (_localServerUuid == null) {
                Server localServer = _serverDao.findLocalServer();
                if (localServer != null) {
                    _localServer = localServer;
                    _localServerUuid = localServer.getUuid();
                }
            }

            if (_localServerUuid != null && _localServerUuid.equals(serverUuid)) {
                return true;
            }
        }

        return false;
    }

    @Override
    public Server getByPublicAddress(String publicAddress) {
        return _serverDao.findByPublicAddress(publicAddress);
    }

    @Override
    public Server getByClusterAddress(String clusterAddress) {
        return _serverDao.findByClusterAddress(clusterAddress);
    }

    @Override
    public void invalidateNonLocalServers() {
        _serverDao.invalidateNonLocalServers();
    }

    @Override
    public void maintainServer(String serverUuid, boolean enter, AsyncCallback callback) {
        Server server = getByUuid(serverUuid);
        if (server != null) {
            server.setStatus(enter ? Server.Status.Maintenance : Server.Status.Running);
        }

        if (callback != null) {
            callback.onSuccess(null);
        }
    }

    @Override
    public void removeServer(String serverUuid, boolean disjoinCluster, AsyncCallback callback) {
        List<? extends Host> hosts = _hostService.getHostsForServer(serverUuid);
        if (hosts != null) {
            for (Host host : hosts) {
                _hostService.removeHost(host);
            }
        }

        Server server = getByUuid(serverUuid);
        if (server != null) {
            delete(server);
        }

        if (callback != null) {
            callback.onSuccess(null);
        }
    }

    @Override
    public void disjoinCluster(String serverUuid, AsyncCallback callback) {
        if (!isLocalServer(serverUuid)) {
            removeServer(serverUuid, true, callback);
            return;
        }

        // Remove current node from the cluster.
        List<? extends Server> servers = getAll(null);
        if (servers != null) {
            for (Server server : servers) {
                if (StringUtils.equals(server.getUuid(), serverUuid)) {
                    server.setClusterName("");
                    server.setClusterId("");
                    server.setStatus(Server.Status.Disjoined);
                    commit(server);
                } else {
                    // The other servers are not disjoining cluster.
                    removeServer(server.getUuid(), false, null);
                }
            }
        }

        DisjoinClusterEvent event = new DisjoinClusterEvent(callback);
        _eventBusService.post(event);
    }
}
