/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.nimdesk.database.dao.EventLogDao;
import com.nimdesk.database.vo.EventLogVO;
import com.nimdesk.model.EventLog;
import com.nimdesk.model.EventLog.Severity;
import com.nimdesk.model.Pool;
import com.nimdesk.model.Task;
import com.nimdesk.model.VmImage;
import com.nimdesk.model.VmInstance;
import com.nimdesk.service.ObjectPropertyChangeEvent;
import com.nimdesk.util.IdGenerator;
import com.nimdesk.util.Pagination;
import com.nimdesk.util.Pair;

@Service(EventLog.Service.BEAN_NAME)
public class EventLogServiceImpl extends ObjectPropertyChangeSupport implements EventLog.Service, InitializingBean {
    private static final Logger LOGGER = Logger.getLogger(EventLogServiceImpl.class);

    static {
        EventLog.TEMPLATE_MAP.put("info.image.import.start", "User \"${user}\" started to import image \"${image}\" from VM \"${vm}\"");
        EventLog.TEMPLATE_MAP.put("info.image.import.succeeded", "User \"${user}\" succeeded to import image \"${image}\" from VM \"${vm}\"");
        EventLog.TEMPLATE_MAP.put("error.image.import.failed", "User \"${user}\" failed to import image \"${image}\" from VM \"${vm}\"");

        EventLog.TEMPLATE_MAP.put("info.image.preupdate.start", "User \"${user}\" started to prepare image \"${image}\" update");
        EventLog.TEMPLATE_MAP.put("info.image.preupdate.succeeded", "User \"${user}\" succeeded to prepare image \"${image}\" update");
        EventLog.TEMPLATE_MAP.put("error.image.preupdate.failed", "User \"${user}\" failed to prepare image \"${image}\" update");

        EventLog.TEMPLATE_MAP.put("info.image.copy.start", "User \"${user}\" started to copy image \"${image}\" from peer \"${server}\"");
        EventLog.TEMPLATE_MAP.put("info.image.copy.succeeded", "User \"${user}\" succeeded to copy image \"${image}\" from peer \"${server}\"");
        EventLog.TEMPLATE_MAP.put("error.image.copy.failed", "User \"${user}\" failed to copy image \"${image}\" from peer \"${server}\"");

        EventLog.TEMPLATE_MAP.put("info.image.delete.start", "User \"${user}\" started to delete image \"${image}\"");
        EventLog.TEMPLATE_MAP.put("info.image.delete.succeeded", "User \"${user}\" succeeded to delete image \"${image}\"");
        EventLog.TEMPLATE_MAP.put("error.image.delete.failed", "User \"${user}\" failed to delete image \"${image}\"");

        EventLog.TEMPLATE_MAP.put("info.pool.create.start", "User \"${user}\" started to create pool \"${pool}\"");
        EventLog.TEMPLATE_MAP.put("info.pool.create.succeeded", "User \"${user}\" succeeded to create pool \"${pool}\"");
        EventLog.TEMPLATE_MAP.put("error.pool.create.failed", "User \"${user}\" failed to create pool \"${pool}\"");

        EventLog.TEMPLATE_MAP.put("info.pool.delete.start", "User \"${user}\" started to delete pool \"${pool}\"");
        EventLog.TEMPLATE_MAP.put("info.pool.delete.succeeded", "User \"${user}\" succeeded to delete pool \"${pool}\"");
        EventLog.TEMPLATE_MAP.put("error.pool.delete.failed", "User \"${user}\" failed to delete pool \"${pool}\"");

        EventLog.TEMPLATE_MAP.put("info.desktop.create.start", "User \"${user}\" started to provisioin desktop \"${desktop}\" for pool \"${pool}\"");
        EventLog.TEMPLATE_MAP.put("info.desktop.create.succeeded", "User \"${user}\" succeeded to provision desktop \"${desktop}\" for pool \"${pool}\"");
        EventLog.TEMPLATE_MAP.put("error.desktop.create.failed", "User \"${user}\" failed to provision desktop \"${desktop}\" for pool \"${pool}\"");

        EventLog.TEMPLATE_MAP.put("info.desktop.snapshot.start", "User \"${user}\" started to snapshot desktop \"${desktop}\" for pool \"${pool}\"");
        EventLog.TEMPLATE_MAP.put("info.desktop.snapshot.succeeded", "User \"${user}\" succeeded to snapshot desktop \"${desktop}\" for pool \"${pool}\"");
        EventLog.TEMPLATE_MAP.put("error.desktop.snapshot.failed", "User \"${user}\" failed to snapshot desktop \"${desktop}\" for pool \"${pool}\"");

        EventLog.TEMPLATE_MAP.put("info.desktop.refresh.start", "User \"${user}\" started to refresh desktop \"${desktop}\" for pool \"${pool}\"");
        EventLog.TEMPLATE_MAP.put("info.desktop.refresh.succeeded", "User \"${user}\" succeeded to refresh desktop \"${desktop}\" for pool \"${pool}\"");
        EventLog.TEMPLATE_MAP.put("error.desktop.refresh.failed", "User \"${user}\" failed to refresh desktop \"${desktop}\" for pool \"${pool}\"");

        EventLog.TEMPLATE_MAP.put("info.desktop.start.start", "User \"${user}\" started to start desktop \"${desktop}\" for pool \"${pool}\"");
        EventLog.TEMPLATE_MAP.put("info.desktop.start.succeeded", "User \"${user}\" succeeded to start desktop \"${desktop}\" for pool \"${pool}\"");
        EventLog.TEMPLATE_MAP.put("error.desktop.start.failed", "User \"${user}\" failed to start desktop \"${desktop}\" for pool \"${pool}\"");

        EventLog.TEMPLATE_MAP.put("info.desktop.delete.start", "User \"${user}\" started to delete desktop \"${desktop}\" from pool \"${pool}\"");
        EventLog.TEMPLATE_MAP.put("info.desktop.delete.succeeded", "User \"${user}\" succeeded to delete desktop \"${desktop}\" from pool \"${pool}\"");
        EventLog.TEMPLATE_MAP.put("error.desktop.delete.failed", "User \"${user}\" failed to delete desktop \"${desktop}\" from pool \"${pool}\"");
    }

    @Autowired
    private EventLogDao _eventLogDao;

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(_eventLogDao, "An EventLogDao must be set");
    }

    @Override
    public Long count() {
        return _eventLogDao.countAll();
    }

    @Override
    public EventLog newInstance() {
        EventLog eventLog = new EventLogVO();

        // Generate uuid
        eventLog.setUuid(IdGenerator.generateUuid());

        return eventLog;
    }

    @Override
    public List<? extends EventLog> getAll(Pagination pagination) {
        return _eventLogDao.findAll(pagination);
    }

    @Override
    public EventLog getByUuid(String uuid) {
        return _eventLogDao.findByUuid(uuid);
    }

    @Override
    public void commit(EventLog obj) {
        if (!(obj instanceof EventLogVO)) {
            String error = String.format("commit: passing wrong type %s", obj.getClass().toString());
            LOGGER.error(error);
            throw new IllegalArgumentException(error);
        }

        EventLogVO eventLog = (EventLogVO) obj;

        if (eventLog.getId() == 0) {
            _eventLogDao.createNew(eventLog);
            eventLog.resetChangedProperties();
            fireCreateEvent(new ObjectPropertyChangeEvent(eventLog, null));
        } else {
            _eventLogDao.update(eventLog);
        }
    }

    @Override
    public boolean delete(EventLog obj) {
        if (!(obj instanceof EventLogVO)) {
            String error = String.format("delete: passing wrong type %s", obj.getClass().toString());
            LOGGER.error(error);
            throw new IllegalArgumentException(error);
        }

        EventLogVO eventLog = (EventLogVO) obj;

        try {
            if (eventLog.getId() > 0) {
                return _eventLogDao.delete(eventLog.getId());
            }
        } finally {
            fireDeleteEvent(new ObjectPropertyChangeEvent(eventLog));
        }

        return true;
    }

    @Override
    public Long countAllPoolEvents() {
        return _eventLogDao.countAllPools();
    }

    @Override
    public Long countPoolEvents(String poolUuid) {
        return _eventLogDao.countByPool(poolUuid);
    }

    @Override
    public Long countAllVmImageEvents() {
        return _eventLogDao.countAllVmImages();
    }

    @Override
    public Long countVmImageEvents(String vmImageUuid) {
        return _eventLogDao.countByVmImage(vmImageUuid);
    }

    @Override
    public Long countAllVmInstanceEvents() {
        return _eventLogDao.countAllVmInstances();
    }

    @Override
    public Long countVmInstanceEvents(String vmInstanceUuid) {
        return _eventLogDao.countByVmInstance(vmInstanceUuid);
    }

    @Override
    public List<? extends EventLog> getAllPoolEvents(Pagination pagination) {
        return _eventLogDao.findAllPools(pagination);
    }

    @Override
    public List<? extends EventLog> getPoolEvents(String poolUuid, Pagination pagination) {
        return _eventLogDao.findByPool(poolUuid, pagination);
    }

    @Override
    public List<? extends EventLog> getAllVmImageEvents(Pagination pagination) {
        return _eventLogDao.findAllVmImages(pagination);
    }

    @Override
    public List<? extends EventLog> getVmImageEvents(String vmImageUuid, Pagination pagination) {
        return _eventLogDao.findByVmImage(vmImageUuid, pagination);
    }

    @Override
    public List<? extends EventLog> getAllVmInstanceEvents(Pagination pagination) {
        return _eventLogDao.findAllVmInstances(pagination);
    }

    @Override
    public List<? extends EventLog> getVmInstanceEvents(String vmInstanceUuid, Pagination pagination) {
        return _eventLogDao.findByVmInstance(vmInstanceUuid, pagination);
    }

    @Override
    public void addEventLog(Severity severity, String template, String user, Task task,
            Pair<String, String>... param) {
        EventLog eventLog = newInstance();

        eventLog.setDateTime(System.currentTimeMillis());
        eventLog.setSeverity(severity);
        eventLog.setDescriptionTemplate(template);
        eventLog.setUser(user);
        if (task != null) {
            eventLog.setTaskUuid(task.getUuid());
        }

        for (Pair<String, String> p : param) {
            if (p.getSecond() == null) {
                p.setSecond("");
            }

            eventLog.setParameter(p.getFirst(), p.getSecond());
        }

        commit(eventLog);
    }

    @Override
    public void addPoolEventLog(Severity severity, Pool pool, String template,
            String user, Task task, Pair<String, String>... param) {
        EventLog eventLog = newInstance();

        eventLog.setDateTime(System.currentTimeMillis());
        eventLog.setSeverity(severity);
        eventLog.setDescriptionTemplate(template);
        eventLog.setUser(user);
        if (pool != null) {
            eventLog.setTargetName(pool.getName());
            eventLog.setTargetType(Pool.class.getSimpleName());
            eventLog.setTargetUuid(pool.getUuid());
        }
        if (task != null) {
            eventLog.setTaskUuid(task.getUuid());
        }

        for (Pair<String, String> p : param) {
            if (p.getSecond() == null) {
                p.setSecond("");
            }

            eventLog.setParameter(p.getFirst(), p.getSecond());
        }

        commit(eventLog);
    }

    @Override
    public void addVmImageEventLog(Severity severity, VmImage vmImage, String template,
            String user, Task task, Pair<String, String>... param) {
        EventLog eventLog = newInstance();

        eventLog.setDateTime(System.currentTimeMillis());
        eventLog.setSeverity(severity);
        eventLog.setDescriptionTemplate(template);
        eventLog.setUser(user);
        if (vmImage != null) {
            eventLog.setTargetName(vmImage.getName());
            eventLog.setTargetType(VmImage.class.getSimpleName());
            eventLog.setTargetUuid(vmImage.getUuid());
        }
        if (task != null) {
            eventLog.setTaskUuid(task.getUuid());
        }

        for (Pair<String, String> p : param) {
            if (p.getSecond() == null) {
                p.setSecond("");
            }

            eventLog.setParameter(p.getFirst(), p.getSecond());
        }

        commit(eventLog);
    }

    @Override
    public void addVmInstanceEventLog(Severity severity, VmInstance vmInstance, String template,
            String user, Task task, Pair<String, String>... param) {
        EventLog eventLog = newInstance();

        eventLog.setDateTime(System.currentTimeMillis());
        eventLog.setSeverity(severity);
        eventLog.setDescriptionTemplate(template);
        eventLog.setUser(user);
        if (vmInstance != null) {
            eventLog.setTargetName(vmInstance.getName());
            eventLog.setTargetType(VmInstance.class.getSimpleName());
            eventLog.setTargetUuid(vmInstance.getUuid());
        }
        if (task != null) {
            eventLog.setTaskUuid(task.getUuid());
        }

        for (Pair<String, String> p : param) {
            if (p.getSecond() == null) {
                p.setSecond("");
            }

            eventLog.setParameter(p.getFirst(), p.getSecond());
        }

        commit(eventLog);
    }

    @Override
    public List<? extends EventLog> getLocalEventLogsByTime(boolean before, long time) {
        return _eventLogDao.findAllByTime(before, time);
    }

    @Override
    public void deleteOldEventLogs(long time) {
        _eventLogDao.deleteOldEventLogs(time);
    }
}
