/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.nimdesk.database.dao.GlobalSettingDao;
import com.nimdesk.database.vo.GlobalSettingVO;
import com.nimdesk.model.GlobalSetting;
import com.nimdesk.service.ObjectPropertyChangeEvent;
import com.nimdesk.util.IdGenerator;
import com.nimdesk.util.Pagination;
import com.nimdesk.util.Pair;

@Service(GlobalSetting.Service.BEAN_NAME)
public class GlobalSettingServiceImpl extends ObjectPropertyChangeSupport implements GlobalSetting.Service, InitializingBean {
    private static final Logger LOGGER = Logger.getLogger(GlobalSettingServiceImpl.class);

    @Autowired
    private GlobalSettingDao _globalSettingDao;

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(_globalSettingDao, "A GlobalSettingDao must be set");
    }

    @Override
    public Long count() {
        return _globalSettingDao.countAll();
    }

    @Override
    public GlobalSetting newInstance() {
        GlobalSetting globalSetting = new GlobalSettingVO();

        // Generate uuid
        globalSetting.setUuid(IdGenerator.generateUuid());

        return globalSetting;
    }

    @Override
    public List<? extends GlobalSetting> getAll(Pagination pagination) {
        return _globalSettingDao.findAll(pagination);
    }

    @Override
    public GlobalSetting getByUuid(String uuid) {
        return _globalSettingDao.findByUuid(uuid);
    }

    @Override
    public void commit(GlobalSetting obj) {
        if (!(obj instanceof GlobalSettingVO)) {
            String error = String.format("commit: passing wrong type %s", obj.getClass().toString());
            LOGGER.error(error);
            throw new IllegalArgumentException(error);
        }

        GlobalSettingVO globalSetting = (GlobalSettingVO) obj;

        if (globalSetting.getId() == 0) {
            _globalSettingDao.createNew(globalSetting);
            globalSetting.resetChangedProperties();
            fireCreateEvent(new ObjectPropertyChangeEvent(globalSetting, null));
        } else {
            _globalSettingDao.update(globalSetting);
            Map<String, Pair<Object, Object>> changedProperties = globalSetting.resetChangedProperties();
            fireChangeEvent(new ObjectPropertyChangeEvent(globalSetting, changedProperties));
        }
    }

    @Override
    public boolean delete(GlobalSetting obj) {
        if (!(obj instanceof GlobalSettingVO)) {
            String error = String.format("delete: passing wrong type %s", obj.getClass().toString());
            LOGGER.error(error);
            throw new IllegalArgumentException(error);
        }

        GlobalSettingVO globalSetting = (GlobalSettingVO) obj;

        try {
            if (globalSetting.getId() > 0) {
                return _globalSettingDao.delete(globalSetting.getId());
            }
        } finally {
            fireDeleteEvent(new ObjectPropertyChangeEvent(globalSetting));
        }

        return true;
    }

    @Override
    public GlobalSetting getByKey(String key) {
        return _globalSettingDao.findByKey(key);
    }

    @Override
    public void deleteByKey(String key) {
        GlobalSetting globalSetting = _globalSettingDao.findByKey(key);
        if (globalSetting != null) {
            _globalSettingDao.delete(((GlobalSettingVO) globalSetting).getId());
        }
    }
}
