/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.nimdesk.database.dao.StorageDao;
import com.nimdesk.database.vo.StorageVO;
import com.nimdesk.model.Host;
import com.nimdesk.model.Storage;
import com.nimdesk.service.ObjectPropertyChangeEvent;
import com.nimdesk.util.IdGenerator;
import com.nimdesk.util.Pagination;
import com.nimdesk.util.Pair;

@Service(Storage.Service.BEAN_NAME)
public class StorageServiceImpl extends ObjectPropertyChangeSupport implements Storage.Service, InitializingBean {
    private static final Logger LOGGER = Logger.getLogger(StorageServiceImpl.class);

    @Autowired
    private StorageDao _storageDao;

    @Autowired
    private Host.Service _hostService;

    private final Storage _sharedStorage = new Storage() {
        private static final long serialVersionUID = 1L;

        @Override
        public String getUuid() {
            return Storage.SHARED_STORAGE_UUID;
        }

        @Override
        public void setUuid(String uuid) {
        }

        @Override
        public String getName() {
            Host localHost = _hostService.getLocalHost();
            return (localHost != null) ? localHost.getSharedDatastoreName() : null;
        }

        @Override
        public void setName(String name) {
        }

        @Override
        public String getHostUuid() {
            Host localHost = _hostService.getLocalHost();
            return (localHost != null) ? localHost.getUuid() : null;
        }

        @Override
        public void setHostUuid(String hostUuid) {
        }

        @Override
        public String getRefId() {
            Host localHost = _hostService.getLocalHost();
            return (localHost != null) ? localHost.getSharedDatastoreRefId() : null;
        }

        @Override
        public void setRefId(String refId) {
        }

        @Override
        public long getCapacity() {
            return 0;
        }

        @Override
        public void setCapacity(long capacity) {
        }

        @Override
        public long getFreeSpace() {
            return 0;
        }

        @Override
        public void setFreeSpace(long freeSpace) {
        }
    };


    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(_storageDao, "A StorageDao must be set");
        Assert.notNull(_hostService, "A Host.Service must be set");
    }

    @Override
    public Long count() {
        return _storageDao.countAll();
    }

    @Override
    public Storage newInstance() {
        Storage storage = new StorageVO();

        // Generate uuid
        storage.setUuid(IdGenerator.generateUuid());

        return storage;
    }

    @Override
    public List<? extends Storage> getAll(Pagination pagination) {
        return _storageDao.findAll(pagination);
    }

    @Override
    public Storage getByUuid(String uuid) {
        if (Storage.SHARED_STORAGE_UUID.equals(uuid)) {
            return getSharedStorage();
        }

        return _storageDao.findByUuid(uuid);
    }

    @Override
    public void commit(Storage obj) {
        if (!(obj instanceof StorageVO)) {
            String error = String.format("commit: passing wrong type %s", obj.getClass().toString());
            LOGGER.error(error);
            throw new IllegalArgumentException(error);
        }

        StorageVO storage = (StorageVO) obj;

        if (storage.getId() == 0) {
            _storageDao.createNew(storage);
            storage.resetChangedProperties();
            fireCreateEvent(new ObjectPropertyChangeEvent(storage, null));
        } else {
            _storageDao.update(storage);
            Map<String, Pair<Object, Object>> changedProperties = storage.resetChangedProperties();
            fireChangeEvent(new ObjectPropertyChangeEvent(storage, changedProperties));
        }
    }

    @Override
    public boolean delete(Storage obj) {
        if (!(obj instanceof StorageVO)) {
            String error = String.format("delete: passing wrong type %s", obj.getClass().toString());
            LOGGER.error(error);
            throw new IllegalArgumentException(error);
        }

        StorageVO storage = (StorageVO) obj;

        try {
            if (storage.getId() > 0) {
                return _storageDao.delete(storage.getId());
            }
        } finally {
            fireDeleteEvent(new ObjectPropertyChangeEvent(storage));
        }

        return true;
    }

    @Override
    public List<? extends Storage> getHostStorages(String hostUuid) {
        return _storageDao.findByHost(hostUuid);
    }

    @Override
    public List<? extends Storage> getLocalStorages() {
        List<? extends Host> localHosts = _hostService.getLocalHosts();

        String[] hostUuids = new String[localHosts.size()];
        for (int idx = 0; idx < localHosts.size(); idx++) {
            hostUuids[idx] = localHosts.get(idx).getUuid();
        }

        return _storageDao.findByHosts(hostUuids);
    }

    @Override
    public boolean isLocalStorage(Storage storage) {
        return _hostService.isLocalHost(storage.getHostUuid());
    }

    @Override
    public Storage getSharedStorage() {
        final Host localHost = _hostService.getLocalHost();
        if (localHost == null || StringUtils.isEmpty(localHost.getSharedDatastoreRefId())) {
            return null;
        }

        return _sharedStorage;
    }

    @Override
    public Storage allocateStorage(String hostUuid) {
        List<? extends Storage> storages = getHostStorages(hostUuid);
        if (storages == null || storages.isEmpty()) {
            return null;
        }
        return storages.get(0);
    }

    @Override
    public void updateStorageSizes(Storage storage, long capacity, long freeSpace) {
        // Refetch the storage
        Storage curStorage = _storageDao.findByUuid(storage.getUuid());
        if (curStorage == null) {
            return;
        }

        boolean modified = false;

        if (capacity != curStorage.getCapacity()) {
            curStorage.setCapacity(capacity);
            modified = true;
        }
        if (freeSpace != curStorage.getFreeSpace()) {
            curStorage.setFreeSpace(freeSpace);
            modified = true;
        }

        if (modified) {
            commit(curStorage);
        }
    }
}
