/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.nimdesk.database.dao.DiskImageDao;
import com.nimdesk.database.vo.DiskImageVO;
import com.nimdesk.event.AsyncCallback;
import com.nimdesk.model.DiskImage;
import com.nimdesk.model.Host;
import com.nimdesk.service.ObjectPropertyChangeEvent;
import com.nimdesk.util.IdGenerator;
import com.nimdesk.util.Pagination;
import com.nimdesk.util.Pair;

@Service(DiskImage.Service.BEAN_NAME)
public class DiskImageServiceImpl extends ObjectPropertyChangeSupport implements DiskImage.Service, InitializingBean {
    private static final Logger LOGGER = Logger.getLogger(DiskImageServiceImpl.class);

    @Autowired
    private DiskImageDao _diskImageDao;

    @Autowired
    private Host.Service _hostService;

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(_diskImageDao, "A DiskImageDao must be set");
        Assert.notNull(_hostService, "A Host.Service must be set");
    }

    @Override
    public Long count() {
        LOGGER.error("count() not implemented");
        throw new RuntimeException("Not implemented");
    }

    @Override
    public DiskImage newInstance() {
        DiskImage diskImage = new DiskImageVO();

        // Generate uuid
        diskImage.setUuid(IdGenerator.generateUuid());

        return diskImage;
    }

    @Override
    public List<? extends DiskImage> getAll(Pagination pagination) {
        return _diskImageDao.findAll(pagination);
    }

    @Override
    public DiskImage getByUuid(String uuid) {
        return _diskImageDao.findByUuid(uuid);
    }

    @Override
    public void commit(DiskImage obj) {
        if (!(obj instanceof DiskImageVO)) {
            String error = String.format("commit: passing wrong type %s", obj.getClass().toString());
            LOGGER.error(error);
            throw new IllegalArgumentException(error);
        }

        DiskImageVO diskImage = (DiskImageVO) obj;

        if (diskImage.getId() == 0) {
            _diskImageDao.createNew(diskImage);
            diskImage.resetChangedProperties();
            fireCreateEvent(new ObjectPropertyChangeEvent(diskImage));
        } else {
            _diskImageDao.update(diskImage);
            Map<String, Pair<Object, Object>> changedProperties = diskImage.resetChangedProperties();
            fireChangeEvent(new ObjectPropertyChangeEvent(diskImage, changedProperties));
        }
    }

    @Override
    public boolean delete(DiskImage obj) {
        if (!(obj instanceof DiskImageVO)) {
            String error = String.format("delete: passing wrong type %s", obj.getClass().toString());
            LOGGER.error(error);
            throw new IllegalArgumentException(error);
        }

        DiskImageVO diskImage = (DiskImageVO) obj;

        try {
            if (diskImage.getId() > 0) {
                return _diskImageDao.delete(diskImage.getId());
            }
        } finally {
            fireDeleteEvent(new ObjectPropertyChangeEvent(diskImage));
        }

        return true;
    }

    @Override
    public Long countByOwner(String ownerUuid) {
        return _diskImageDao.countByOwner(ownerUuid);
    }

    @Override
    public List<? extends DiskImage> getDiskImagesByGuid(String guid, DiskImage.Status... status) {
        return _diskImageDao.findByGuid(guid, status);
    }

    @Override
    public List<? extends DiskImage> getDiskImagesByOwner(String ownerUuid, DiskImage.Status... status) {
        return _diskImageDao.findByOwner(ownerUuid, status);
    }

    @Override
    public List<? extends DiskImage> getDiskImagesByName(String name) {
        return _diskImageDao.findByName(name);
    }

    @Override
    public DiskImage getLocalDiskImageByGuid(String guid) {
        Host localHost = _hostService.getLocalHost();
        if (localHost == null) {
            return null;
        }

        return _diskImageDao.findByGuidOnHost(guid, localHost.getUuid());
    }

    @Override
    public List<? extends DiskImage> getLocalDiskImagesByOwner(String ownerUuid) {
        Host localHost = _hostService.getLocalHost();
        return _diskImageDao.findByOwnerOnHost(ownerUuid, localHost.getUuid());
    }

    @Override
    public List<? extends DiskImage> getLocalDiskImages(DiskImage.Status... status) {
        Host localHost = _hostService.getLocalHost();
        if (localHost == null) {
            return null;
        }

        return _diskImageDao.findByHost(localHost.getUuid(), status);
    }

    @Override
    public List<? extends DiskImage> getDiskImagesOnHost(String hostUuid) {
        return _diskImageDao.findByHosts(hostUuid);
    }

    @Override
    public boolean isLocalDiskImage(DiskImage diskImage) {
        return _hostService.isLocalHost(diskImage.getHostUuid());
    }

    @Override
    public void invalidateNonLocalDiskImages() {
        Host localHost = _hostService.getLocalHost();
        if (localHost != null) {
            _diskImageDao.invalidateNonLocalDiskImages(localHost.getUuid());
        }
    }

    @Override
    public void deleteDiskImage(DiskImage diskImage, AsyncCallback callback) {
        if (DiskImage.Status.Deleting != diskImage.getStatus()) {
            diskImage.setStatus(DiskImage.Status.Deleting);
            commit(diskImage);
        }

        if (callback != null) {
            callback.onSuccess(null);
        }
    }
}
