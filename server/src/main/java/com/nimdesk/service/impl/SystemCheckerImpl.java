/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.service.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.nimdesk.Version;
import com.nimdesk.event.ClusterViewChangeEvent;
import com.nimdesk.model.EventLog;
import com.nimdesk.model.Host;
import com.nimdesk.model.Server;
import com.nimdesk.model.Task;
import com.nimdesk.model.UserSession;
import com.nimdesk.service.Scheduler;
import com.nimdesk.service.SystemChecker;
import com.nimdesk.ui.server.DesktopPoolService;
import com.google.common.eventbus.AllowConcurrentEvents;
import com.google.common.eventbus.Subscribe;

@Service(SystemChecker.BEAN_NAME)
public class SystemCheckerImpl implements SystemChecker, InitializingBean {
    private static final Logger LOGGER = Logger.getLogger(SystemCheckerImpl.class);

    private static final long DATABASE_CHECKER_INTERVAL = 3600L; // 1 hour
    private static final long LOGFILE_CHECKER_INTERVAL = 24 * 3600L; // 24 hour

    private static final long USERSESSION_RETENTION_MILLISECONDS = 1 * 24 * 3600 * 1000L; // 1 days
    private static final long TASK_RETENTION_MILLISECONDS = 1 * 24 * 3600 * 1000L; // 1 days
    private static final long EVENTLOG_RETENTION_MILLISECONDS = 1 * 24 * 3600 * 1000L; // 1 days

    private static final long LOGFILE_RETENTION_MILLISECONDS = 30 * 24 * 3600 * 1000L; // 30 days
    private static final long MISC_LOGFILE_RETENTION_MILLISECONDS = 24 * 3600 * 1000L; // 1 days

    private static final String LOG_DIRECTORY = "/var/nimdesk/logs/";
    private static final String[] MISC_LOG_DIRECTORIES =
            new String[] { "/var/log/tomcat6/", "/var/log/" };

    private volatile boolean _isMaster = true;

    private static final List<CheckerTask> CHECKERS = new ArrayList<CheckerTask>();

    @Autowired
    private Scheduler _scheduler;

    @Autowired
    private DesktopPoolService _desktopPoolService;

    @Autowired
    private Server.Service _serverService;

    @Autowired
    private Host.Service _hostService;

    @Autowired
    private UserSession.Service _userSessionService;

    @Autowired
    private Task.Service _taskService;

    @Autowired
    private EventLog.Service _eventLogService;

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(_scheduler, "A Scheduler must be set");
        Assert.notNull(_desktopPoolService, "A DesktopPoolService must be set");
        Assert.notNull(_serverService, "A Server.Service must be set");
        Assert.notNull(_hostService, "A Host.Service must be set");
        Assert.notNull(_userSessionService, "A UserSession.Service must be set");
        Assert.notNull(_taskService, "A Task.Service must be set");
        Assert.notNull(_eventLogService, "An EventLog.Service must be set");

        CHECKERS.add(new DatabaseCheckerTask());
        CHECKERS.add(new LogFileCheckerTask());
    }

    @Override
    public void start() {
        Random random = new Random();

        for (final CheckerTask checker : CHECKERS) {
            LOGGER.info(String.format("Starting checker \"%s\"...", checker.getName()));

            // Start with random initial delay (within 5 minutes).
            _scheduler.scheduleWithFixedDelay(new Runnable() {
                @Override
                public void run() {
                    try {
                        checker.check();
                    } catch (Exception e) {
                        LOGGER.error(String.format("Failed to run checker \"%s\"", checker.getName()), e);
                    }
                }
            }, random.nextInt(300), checker.getInterval());
        }
    }

    @Override
    public void fastCheck() {
        for (CheckerTask checker : CHECKERS) {
            try {
                checker.fastCheck();
            } catch (Exception e) {
                LOGGER.error(String.format("Failed to fast run checker \"%s\"", checker.getName()), e);
            }
        }
    }

    static interface CheckerTask {
        String getName();
        long getInterval();
        void check();
        void fastCheck();
    }

    @Subscribe
    @AllowConcurrentEvents
    public void handle(ClusterViewChangeEvent event) {
        _isMaster = event.isMaster();
    }

    /**
     * Clean up old database data - such as Tasks, Events.
     */
    private class DatabaseCheckerTask implements CheckerTask {
        @Override
        public String getName() {
            return "DatabaseChecker";
        }

        @Override
        public long getInterval() {
            return DATABASE_CHECKER_INTERVAL;
        }

        @Override
        public void check() {
            long userSessionRetentionTime = System.currentTimeMillis() - USERSESSION_RETENTION_MILLISECONDS;
            long taskRetentionTime = System.currentTimeMillis() - TASK_RETENTION_MILLISECONDS;
            long eventLogRetentionTime = System.currentTimeMillis() - EVENTLOG_RETENTION_MILLISECONDS;

            LOGGER.info(String.format("Deleting userSession records older than %s...",
                    (new Date(userSessionRetentionTime)).toString()));

            List<? extends UserSession> oldUserSessions = _userSessionService.getLocalUserSessionsByTime(true, userSessionRetentionTime);
            if (oldUserSessions != null) {
                for (UserSession userSession : oldUserSessions) {
                    if (userSession.getEndTime() == 0) {
                        // Don't delete active sessions.
                        continue;
                    }
                    try {
                        _userSessionService.delete(userSession);
                    } catch (Exception e) {
                        // Ignore and keep trying
                        LOGGER.warn(String.format("Failed to delete old userSession - %s", e.getMessage()));
                    }
                }
            }

            LOGGER.info(String.format("Deleting task records older than %s...",
                    (new Date(taskRetentionTime)).toString()));

            List<? extends Task> oldTasks = _taskService.getLocalTasksByTime(true, taskRetentionTime);
            if (oldTasks != null) {
                for (Task task : oldTasks) {
                    switch (task.getStatus()) {
                    case InProgress:
                    case Pending:
                        // Don't delete un-finished tasks.
                        continue;
                    }
                    try {
                        _taskService.delete(task);
                    } catch (Exception e) {
                        // Ignore and keep trying
                        LOGGER.warn(String.format("Failed to delete old task - %s", e.getMessage()));
                    }
                }
            }

            LOGGER.info(String.format("Deleting eventLog records older than %s...",
                    (new Date(eventLogRetentionTime)).toString()));

            List<? extends EventLog> oldEventLogs = _eventLogService.getLocalEventLogsByTime(true, eventLogRetentionTime);
            if (oldEventLogs != null) {
                for (EventLog eventLog : oldEventLogs) {
                    try {
                        _eventLogService.delete(eventLog);
                    } catch (Exception e) {
                        // Ignore and keep trying
                        LOGGER.warn(String.format("Failed to delete old eventLog - %s", e.getMessage()));
                    }
                }
            }
        }

        @Override
        public void fastCheck() {
            long userSessionRetentionTime = System.currentTimeMillis() - USERSESSION_RETENTION_MILLISECONDS;
            long taskRetentionTime = System.currentTimeMillis() - TASK_RETENTION_MILLISECONDS;
            long eventLogRetentionTime = System.currentTimeMillis() - EVENTLOG_RETENTION_MILLISECONDS;

            try {
                LOGGER.info(String.format("Deleting userSession records older than %s...",
                        (new Date(userSessionRetentionTime)).toString()));
                _userSessionService.deleteOldUserSessions(userSessionRetentionTime);
            } catch (Exception e) {
                LOGGER.warn("Failed to delete old userSessions", e);
            }

            try {
                LOGGER.info(String.format("Deleting task records older than %s...",
                        (new Date(taskRetentionTime)).toString()));
                _taskService.deleteOldTasks(taskRetentionTime);
            } catch (Exception e) {
                LOGGER.warn("Failed to delete old tasks", e);
            }

            try {
                LOGGER.info(String.format("Deleting eventLog records older than %s...",
                        (new Date(eventLogRetentionTime)).toString()));
                _eventLogService.deleteOldEventLogs(eventLogRetentionTime);
            } catch (Exception e) {
                LOGGER.warn("Failed to delete old eventLogs", e);
            }
        }
    }

    /**
     * Clean up old log files.
     */
    private class LogFileCheckerTask implements CheckerTask {
        @Override
        public String getName() {
            return "LogFileChecker";
        }

        @Override
        public long getInterval() {
            return LOGFILE_CHECKER_INTERVAL;
        }

        @Override
        public void check() {
            Collection<File> logFiles = null;

            long logRetentionTime = System.currentTimeMillis() - LOGFILE_RETENTION_MILLISECONDS;

            try {
                logFiles = FileUtils.listFiles(new File(LOG_DIRECTORY), null, false);
            } catch (Exception e) {
                LOGGER.warn(String.format("Failed to list log files in directory \"%s\"", LOG_DIRECTORY));
            }
            if (logFiles != null) {
                for (File logFile : logFiles) {
                    long lastMod = logFile.lastModified();
                    if (lastMod < logRetentionTime) {
                        if (LOGGER.isTraceEnabled()) {
                            LOGGER.trace(String.format("Deleting log file \"%s\" older than %s...",
                                    logFile.getAbsolutePath(), (new Date(logRetentionTime)).toString()));
                        }

                        try {
                            if (!logFile.delete()) {
                                if (LOGGER.isDebugEnabled()) {
                                    LOGGER.debug(String.format("Failed to delete log file \"%s\"", logFile.getAbsolutePath()));
                                }
                            }
                        } catch (Exception e) {
                            LOGGER.warn(String.format("Failed to delete log file \"%s\"", logFile.getAbsolutePath()), e);
                        }
                    }
                }
            }

            logRetentionTime = System.currentTimeMillis() - MISC_LOGFILE_RETENTION_MILLISECONDS;
            for (String logFileDir : MISC_LOG_DIRECTORIES) {
                try {
                    logFiles = FileUtils.listFiles(new File(logFileDir), null, false);
                } catch (Exception e) {
                    LOGGER.warn(String.format("Failed to list log files in directory \"%s\"", logFileDir));
                }
                if (logFiles == null) {
                    continue;
                }

                for (File logFile : logFiles) {
                    long lastMod = logFile.lastModified();
                    if (lastMod < logRetentionTime) {
                        if (LOGGER.isTraceEnabled()) {
                            LOGGER.trace(String.format("Deleting log file \"%s\" older than %s...",
                                    logFile.getAbsolutePath(), (new Date(logRetentionTime)).toString()));
                        }

                        try {
                            if (!logFile.delete()) {
                                if (LOGGER.isDebugEnabled()) {
                                    LOGGER.debug(String.format("Failed to delete log file \"%s\"", logFile.getAbsolutePath()));
                                }
                            }
                        } catch (Exception e) {
                            LOGGER.warn(String.format("Failed to delete log file \"%s\"", logFile.getAbsolutePath()), e);
                        }
                    }
                }
            }
        }

        @Override
        public void fastCheck() {
            //check();
        }
    }
}
