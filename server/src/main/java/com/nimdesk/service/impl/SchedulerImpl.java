/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.service.impl;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.nimdesk.service.Scheduler;

@Service(Scheduler.BEAN_NAME)
public class SchedulerImpl implements Scheduler {
    private static final Logger LOGGER = Logger.getLogger(SchedulerImpl.class);

    private static final int CORE_POOL_SIZE = 1;

    private final ScheduledExecutorService _scheduler = Executors.newScheduledThreadPool(CORE_POOL_SIZE);

    @Override
    public ScheduleHandle schedule(Runnable command, long delayInSeconds) {
        LOGGER.info(String.format("Schedule job with %d seconds delay", delayInSeconds));
        return new ScheduleHandleImpl(_scheduler.schedule(
                new SafeRunnableWrapper(command), delayInSeconds, TimeUnit.SECONDS));
    }

    @Override
    public ScheduleHandle scheduleAtFixedRate(Runnable command, long initialDelayInSeconds, long periodInSeconds) {
        LOGGER.info(String.format("Schedule job at fixed rate %d with %d seconds delay",
                periodInSeconds, initialDelayInSeconds));
        return new ScheduleHandleImpl(_scheduler.scheduleAtFixedRate(
                new SafeRunnableWrapper(command), initialDelayInSeconds,
                periodInSeconds, TimeUnit.SECONDS));
    }

    @Override
    public ScheduleHandle scheduleWithFixedDelay(Runnable command, long initialDelayInSeconds, long delayInSeconds) {
        LOGGER.info(String.format("Schedule job with fixed delay %d with %d seconds delay",
                delayInSeconds, initialDelayInSeconds));
        return new ScheduleHandleImpl(_scheduler.scheduleWithFixedDelay(
                new SafeRunnableWrapper(command), initialDelayInSeconds,
                delayInSeconds, TimeUnit.SECONDS));
    }

    private class ScheduleHandleImpl implements ScheduleHandle {
        private final ScheduledFuture<?> _future;

        ScheduleHandleImpl(ScheduledFuture<?> future) {
            _future = future;
        }

        @Override
        public boolean cancel() {
            return _future.cancel(false);
        }

        @Override
        public boolean isCancelled() {
            return _future.isCancelled();
        }

        @Override
        public boolean isDone() {
            return _future.isDone();
        }
    }

    private class SafeRunnableWrapper implements Runnable {
        private final Runnable _command;

        SafeRunnableWrapper(Runnable command) {
            _command = command;
        }

        @Override
        public void run() {
            if (_command != null) {
                try {
                    _command.run();
                } catch (Throwable t) {
                    LOGGER.error("Failed to excute command", t);
                }
            }
        }
    }
}
