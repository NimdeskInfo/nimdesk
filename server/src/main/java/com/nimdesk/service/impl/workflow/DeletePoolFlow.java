/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.service.impl.workflow;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.nimdesk.event.AsyncCallback;
import com.nimdesk.event.ConnectHostEvent;
import com.nimdesk.event.DeleteStorageDirectoryEvent;
import com.nimdesk.event.UninstantiateVmEvent;
import com.nimdesk.hypervisor.HypervisorContext;
import com.nimdesk.model.DiskImage;
import com.nimdesk.model.EventLog;
import com.nimdesk.model.Host;
import com.nimdesk.model.Pool;
import com.nimdesk.model.Server;
import com.nimdesk.model.Storage;
import com.nimdesk.model.Task;
import com.nimdesk.model.VmInstance;
import com.nimdesk.service.EventBusService;
import com.nimdesk.service.Workflow;
import com.nimdesk.service.impl.ServiceLocator;
import com.nimdesk.util.Pair;

/**
 * Step 1: Connect to host
 * Step 2: Delete all desktops (another flow)
 * Step 3: Delete VM directory from datastore
 */
public class DeletePoolFlow extends Workflow {
    private static final Logger LOGGER = Logger.getLogger(DeletePoolFlow.class);

    private final long _createTime = System.currentTimeMillis();

    private final EventBusService _eventBusService;
    private final Host.Service _hostService;
    private final Storage.Service _storageService;
    private final Pool.Service _poolService;
    private final DiskImage.Service _diskImageService;
    private final VmInstance.Service _vmInstanceService;
    private final Task.Service _taskService;
    private final EventLog.Service _eventLogService;

    private final Server _localServer;
    private final Host _localHost;
    private final Pool _pool;
    private final DiskImage _diskImage;
    private final Task _task;

    private final String _diskImageVmRefId;

    // ConnectHostState result
    private HypervisorContext _hypervisorContext;

    // ConnectVcState result
    private HypervisorContext _vcHypervisorContext = null;

    public DeletePoolFlow(Pool pool, final AsyncCallback callback) {
        super();

        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("DeletePoolFlow()");
        }

        ServiceLocator serviceLocator = ServiceLocator.getInstance();

        _eventBusService = serviceLocator.getEventBusService();
        _hostService = serviceLocator.getHostService();
        _storageService = serviceLocator.getStorageService();
        _poolService = serviceLocator.getPoolService();
        _diskImageService = serviceLocator.getDiskImageService();
        _vmInstanceService = serviceLocator.getVmInstanceService();
        _taskService = serviceLocator.getTaskService();
        _eventLogService = serviceLocator.getEventLogService();

        Server.Service serverService = serviceLocator.getServerService();
        _localServer = serverService.getLocalServer();
        if (_localServer == null) {
            throw new RuntimeException(String.format("Unable to find host to delete pool \"%s\"", pool.getName()));
        }

        _localHost = _hostService.getLocalHost();
        if (_localHost == null) {
            throw new RuntimeException(String.format("Unable to find host to delete pool \"%s\"", pool.getName()));
        }

        _pool = pool;

        if (Pool.Status.Deleting != _pool.getStatus()) {
            _pool.setStatus(Pool.Status.Deleting);
            _pool.setUpdateTime(System.currentTimeMillis());
            _poolService.commit(_pool);
        }

        _diskImage = getDiskImage(_pool);
        if (_diskImage != null) {
            _diskImageVmRefId = _diskImage.getVmRefId();
        } else {
            _diskImageVmRefId = null;
        }
        _task = createTask(_pool, (callback != null) ? callback.getRequestor() : null);

        append(new ConnectHostState(), null);
        if (!StringUtils.isEmpty(_localHost.getVCenterIp())
                && !StringUtils.isEmpty(_localHost.getVCenterUsername())
                && !StringUtils.isEmpty(_localHost.getVCenterPassword())) {
            append(new ConnectVCenterState(), null);
        }
        append(new DeleteAllLocalDesktops(), null);
        //append(new DeleteOtherDesktops(), null); // Don't delete other desktops - their owning server will do it.
        if (!StringUtils.isEmpty(_diskImageVmRefId)) {
            append(new UninstantiateVmState(), null);
        }
        append(new DeleteDiskImageState(), null);
        append(new DeletePoolDsDirectoriesState(), null);

        setCallback(new AsyncCallback() {
            @Override
            @SuppressWarnings("unchecked")
            public void onSuccess(Object result) {
                disconnectHost();

                if (_diskImage != null) {
                    _diskImageService.delete(_diskImage);
                }

                _task.setEndTime(System.currentTimeMillis());
                _task.setStatus(Task.Status.Completed);
                _task.setDetails("Succeeded");
                _taskService.commit(_task);

                _eventLogService.addPoolEventLog(EventLog.Severity.Info,
                        _pool, "info.pool.delete.succeeded", getRequestor(), _task,
                        new Pair<String, String>("user", getRequestor()),
                        new Pair<String, String>("pool", _pool.getName()));

                LOGGER.info(String.format("DeletePoolFlow: Deleted pool \"%s\"", _pool.getName()));

                if (callback != null) {
                    callback.onSuccess(result);
                }
            }

            @Override
            @SuppressWarnings("unchecked")
            public void onFailure(Throwable t) {
                disconnectHost();

                _task.setEndTime(System.currentTimeMillis());
                _task.setStatus(Task.Status.Failed);
                _task.setDetails(t.getMessage());
                _taskService.commit(_task);

                _eventLogService.addPoolEventLog(EventLog.Severity.Error,
                        _pool, "error.pool.delete.failed", getRequestor(), _task,
                        new Pair<String, String>("user", getRequestor()),
                        new Pair<String, String>("pool", _pool.getName()));

                LOGGER.error(String.format("DeletePoolFlow: Failed to delete pool \"%s\" - %s",
                        _pool.getName(), t.getMessage()), t);

                if (callback != null) {
                    callback.onFailure(t);
                }
            }

            @Override
            public void progress(int percentage) {
                if (callback != null) {
                    callback.progress(percentage);
                }
            }

            @Override
            public String getRequestor() {
                return (callback != null) ? callback.getRequestor() : null;
            }
        });
    }

    public Task getTask() {
        return _task;
    }

    private void disconnectHost() {
        if (_hypervisorContext != null) {
            _hypervisorContext.disconnect();
            _hypervisorContext = null;
        }
        if (_vcHypervisorContext != null) {
            _vcHypervisorContext.disconnect();
            _vcHypervisorContext = null;
        }
    }

    @SuppressWarnings("unchecked")
    private Task createTask(Pool pool, String requestor) {
        Task task = _taskService.newInstance();

        task.setType(Task.Type.DeletePool);
        task.setStartTime(_createTime);
        task.setServerUuid(_localServer.getUuid());

        task.setTargetType(Pool.class.getSimpleName());
        task.setTargetUuid(pool.getUuid());
        task.setTargetName(pool.getName());

        task.setStatus(Task.Status.InProgress);
        task.setDetails("Starting");

        _taskService.commit(task);

        _eventLogService.addPoolEventLog(EventLog.Severity.Info,
                pool, "info.pool.delete.start", requestor, task,
                new Pair<String, String>("user", requestor),
                new Pair<String, String>("pool", pool.getName()));

        return task;
    }

    private DiskImage getDiskImage(Pool pool) {
        String diskImageGuid = pool.getBaseDiskImageGuid();
        if (StringUtils.isEmpty(diskImageGuid)) {
            List<? extends DiskImage> localDiskImages = _diskImageService.getLocalDiskImagesByOwner(pool.getUuid());
            if (localDiskImages != null && !localDiskImages.isEmpty()) {
                return localDiskImages.get(0);
            }
            return null;
        }

        return _diskImageService.getLocalDiskImageByGuid(diskImageGuid);
    }

    private class ConnectHostState implements Workflow.StateRunnable {
        @Override
        public void run(AsyncCallback callback) {
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace("ConnectHostState.run()");
            }

            _task.setDetails("Preparing");
            _taskService.commit(_task);

            ConnectHostEvent event = new ConnectHostEvent(_localHost.getAddress(), 0,
                    _localHost.getUsername(), _localHost.getPassword(), callback);
            _eventBusService.post(event);
        }

        @Override
        public void setResult(Object result) {
            _hypervisorContext = (HypervisorContext) result;
        }
    }

    private class ConnectVCenterState implements Workflow.StateRunnable {
        @Override
        public void run(AsyncCallback callback) {
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace("ConnectVCenterState.run()");
            }

            _task.setDetails("Preparing");
            _taskService.commit(_task);

            String vCenterAddress = _localHost.getVCenterAddress();
            if (StringUtils.isEmpty(vCenterAddress)) {
                vCenterAddress = _localHost.getVCenterIp();
            }

            ConnectHostEvent event = new ConnectHostEvent(vCenterAddress, 0,
                    _localHost.getVCenterUsername(), _localHost.getVCenterPassword(), callback);
            _eventBusService.post(event);
        }

        @Override
        public void setResult(Object result) {
            _vcHypervisorContext = (HypervisorContext) result;
        }
    }

    private class DeleteAllLocalDesktops implements Workflow.StateRunnable {
        @Override
        public void run(final AsyncCallback callback) {
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace("DeleteAllDesktops.run()");
            }

            _task.setDetails("Deleting desktops");
            _taskService.commit(_task);

            List<? extends VmInstance> vmInstances = _vmInstanceService.getLocalVmInstancesByPool(_pool.getUuid());
            if (vmInstances != null && vmInstances.size() > 0) {
                final Set<String> vmInstanceUuids = Collections.synchronizedSet(new HashSet<String>());
                for (VmInstance vmInstance : vmInstances) {
                    vmInstanceUuids.add(vmInstance.getUuid());
                }

                for (final VmInstance vmInstance : vmInstances) {
                    DeleteDesktopFlow wf = new DeleteDesktopFlow(_hypervisorContext, _vcHypervisorContext, _localHost, _pool, vmInstance,
                            new AsyncCallback() {
                                @Override
                                public void onSuccess(Object result) {
                                    // TODO Add eventlog here

                                    LOGGER.info(String.format("DeletePoolFlow: Deleted desktop \"%s\" from pool \"%s\"",
                                            vmInstance.getName(), _pool.getName()));

                                    vmInstanceUuids.remove(vmInstance.getUuid());
                                    if (vmInstanceUuids.isEmpty()) {
                                        if (callback != null) {
                                            callback.onSuccess(null);
                                        }
                                    }
                                }

                                @Override
                                public void onFailure(Throwable t) {
                                    LOGGER.warn(String.format("DeletePoolFlow: Failed to delete desktop \"%s\" from pool \"%s\" - %s. Continue...",
                                            vmInstance.getName(), _pool.getName(), t.getMessage()), t);

                                    vmInstanceUuids.remove(vmInstance.getUuid());
                                    if (vmInstanceUuids.isEmpty()) {
                                        if (callback != null) {
                                            callback.onSuccess(null);
                                        }
                                    }
                                }

                                @Override
                                public void progress(int percentage) {
                                }

                                @Override
                                public String getRequestor() {
                                    return DeletePoolFlow.this.getRequestor();
                                }
                    });
                    wf.run();
                }
            } else {
                LOGGER.info(String.format("DeletePoolFlow: No VmInstances found for pool \"%s\"", _pool.getName()));

                if (callback != null) {
                    callback.onSuccess(null);
                }
            }
        }

        @Override
        public void setResult(Object result) {
        }
    }

    private class UninstantiateVmState implements Workflow.StateRunnable {
        @Override
        public void run(final AsyncCallback callback) {
            LOGGER.info(String.format("UninstantiateVmState.run() - vmRefId: %s", _diskImageVmRefId));

            _task.setDetails("Uninstantiating pool vm");
            _taskService.commit(_task);

            UninstantiateVmEvent event = new UninstantiateVmEvent(_hypervisorContext, _diskImageVmRefId,
                    _vcHypervisorContext, _localHost.getEsxUuid(),
                    new AsyncCallback() {
                        @Override
                        public void onSuccess(Object result) {
                            LOGGER.info(String.format("DeletePoolFlow: Uninstantiated base image for pool \"%s\"", _pool.getName()));

                            if (callback != null) {
                                callback.onSuccess(null);
                            }
                        }

                        @Override
                        public void onFailure(Throwable t) {
                            LOGGER.warn(String.format("DeletePoolFlow: Failed to uninstantiate base image for pool \"%s\" - %s. Continue...",
                                    _pool.getName(), t.getMessage()), t);

                            // Continue the flow even if this step fails.
                            if (callback != null) {
                                callback.onSuccess(null);
                            }
                        }

                        @Override
                        public void progress(int percentage) {
                        }

                        @Override
                        public String getRequestor() {
                            return DeletePoolFlow.this.getRequestor();
                        }
                    });
            _eventBusService.post(event);
        }

        @Override
        public void setResult(Object result) {
        }
    }

    private class DeleteDiskImageState implements Workflow.StateRunnable {
        @Override
        public void run(AsyncCallback callback) {
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace("DeleteDiskImageState.run()");
            }

            _task.setDetails(String.format("Deleting disk image for pool \"%s\"", _pool.getName()));
            _taskService.commit(_task);

            if (_diskImage == null) {
                // If diskImage is not found, succeed.
                LOGGER.info(String.format("DeletePoolFlow: No disk image found for pool \"%s\"", _pool.getName()));
            } else {
                Storage storage = _storageService.getByUuid(_diskImage.getStorageUuid());
                if (storage != null) {
                    DeleteStorageDirectoryEvent event = new DeleteStorageDirectoryEvent(_hypervisorContext, _diskImage.getName(),
                        storage, callback);
                    _eventBusService.post(event);

                    return;
                }

                // If storage is not found, succeed.
                LOGGER.info(String.format("DeletePoolFlow: No Storages found for disk image \"%s\" for pool \"%s\"",
                        _diskImage.getName(), _pool.getName()));
            }

            if (callback != null) {
                callback.onSuccess(null);
            }
        }

        @Override
        public void setResult(Object result) {
        }
    }

    private class DeletePoolDsDirectoriesState implements Workflow.StateRunnable {
        @Override
        public void run(final AsyncCallback callback) {
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace("DeletePoolDsDirectoriesState.run()");
            }

            _task.setDetails(String.format("Deleting directories for pool \"%s\"", _pool.getName()));
            _taskService.commit(_task);

            List<? extends Storage> localStorages = _storageService.getLocalStorages();
            if (localStorages != null && !localStorages.isEmpty()) {
                final AtomicInteger storageCount = new AtomicInteger(localStorages.size());

                for (Storage storage : localStorages) {
                    DeleteStorageDirectoryEvent event = new DeleteStorageDirectoryEvent(_hypervisorContext,
                            _pool.getName(), storage,
                            new AsyncCallback() {
                                @Override
                                public void onSuccess(Object result) {
                                    int curSize = storageCount.decrementAndGet();
                                    if (curSize <= 0) {
                                        if (callback != null) {
                                            callback.onSuccess(null);
                                        }
                                    }
                                }

                                @Override
                                public void onFailure(Throwable t) {
                                    int curSize = storageCount.decrementAndGet();
                                    if (curSize <= 0) {
                                        if (callback != null) {
                                            callback.onSuccess(null);
                                        }
                                    }
                                }

                                @Override
                                public void progress(int percentage) {
                                }

                                @Override
                                public String getRequestor() {
                                    return DeletePoolFlow.this.getRequestor();
                                }
                    });
                    _eventBusService.post(event);
                }

                return;
            }

            // Don't need to care about the results.
            if (callback != null) {
                callback.onSuccess(null);
            }
        }

        @Override
        public void setResult(Object result) {
        }
    }
}
