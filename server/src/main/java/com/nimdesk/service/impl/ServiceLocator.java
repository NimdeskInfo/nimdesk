/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.service.impl;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.nimdesk.cluster.RemoteProxyFactory;
import com.nimdesk.model.DiskImage;
import com.nimdesk.model.EventLog;
import com.nimdesk.model.Host;
import com.nimdesk.model.LdapServer;
import com.nimdesk.model.Pool;
import com.nimdesk.model.PoolUser;
import com.nimdesk.model.Server;
import com.nimdesk.model.Storage;
import com.nimdesk.model.Task;
import com.nimdesk.model.User;
import com.nimdesk.model.UserSession;
import com.nimdesk.model.VmImage;
import com.nimdesk.model.VmInstance;
import com.nimdesk.service.AsyncExecutor;
import com.nimdesk.service.EventBusService;
import com.nimdesk.service.Scheduler;
import com.nimdesk.tracker.TrackerManager;

/**
 * This class is to help locate spring beans for all other classes.
 */
@Service("serviceLocator")
public class ServiceLocator implements InitializingBean {

    private static ServiceLocator _serviceLocator = null;

    @Autowired
    private EventBusService _eventBusService;

    @Autowired
    private AsyncExecutor _asyncExecutor;

    @Autowired
    private Scheduler _scheduler;

    @Autowired
    private RemoteProxyFactory _remoteProxyFactory;

    @Autowired
    private Server.Service _serverService;

    @Autowired
    private Host.Service _hostService;

    @Autowired
    private Storage.Service _storageService;

    @Autowired
    private LdapServer.Service _ldapServerService;

    @Autowired
    private Pool.Service _poolService;

    @Autowired
    private PoolUser.Service _poolUserService;

    @Autowired
    private VmImage.Service _vmImageService;

    @Autowired
    private DiskImage.Service _diskImageService;

    @Autowired
    private VmInstance.Service _vmInstanceService;

    @Autowired
    private User.Service _userService;

    @Autowired
    private UserSession.Service _userSessionService;

    @Autowired
    private Task.Service _taskService;

    @Autowired
    private EventLog.Service _eventLogService;

    @Autowired
    private TrackerManager _trackerManager;

    public static ServiceLocator getInstance() {
        return _serviceLocator;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(_eventBusService, "An EventBusService must be set");
        Assert.notNull(_asyncExecutor, "An AsyncExecutor must be set");
        Assert.notNull(_scheduler, "A Scheduler must be set");
        Assert.notNull(_remoteProxyFactory, "A RemoteProxyFactory must be set");
        Assert.notNull(_serverService, "A Server.Service must be set");
        Assert.notNull(_hostService, "A Host.Service must be set");
        Assert.notNull(_storageService, "A Storage.Service must be set");
        Assert.notNull(_ldapServerService, "A LdapServer.Service must be set");
        Assert.notNull(_poolService, "A Pool.Service must be set");
        Assert.notNull(_vmImageService, "A VmImage.Service must be set");
        Assert.notNull(_diskImageService, "A DiskImage.Service must be set");
        Assert.notNull(_vmInstanceService, "A VmInstance.Service must be set");
        Assert.notNull(_taskService, "A UserSession.Service must be set");
        Assert.notNull(_userSessionService, "A Task.Service must be set");
        Assert.notNull(_eventLogService, "An EventLog.Service must be set");

        _serviceLocator = this;
    }

    public EventBusService getEventBusService() {
        return _eventBusService;
    }

    public AsyncExecutor getAsyncExecutor() {
        return _asyncExecutor;
    }

    public Scheduler getScheduler() {
        return _scheduler;
    }

    public RemoteProxyFactory getRemoteProxyFactory() {
        return _remoteProxyFactory;
    }

    public Server.Service getServerService() {
        return _serverService;
    }

    public Host.Service getHostService() {
        return _hostService;
    }

    public Storage.Service getStorageService() {
        return _storageService;
    }

    public LdapServer.Service getLdapServerService() {
        return _ldapServerService;
    }

    public Pool.Service getPoolService() {
        return _poolService;
    }

    public PoolUser.Service getPoolUserService() {
        return _poolUserService;
    }

    public VmImage.Service getVmImageService() {
        return _vmImageService;
    }

    public DiskImage.Service getDiskImageService() {
        return _diskImageService;
    }

    public VmInstance.Service getVmInstanceService() {
        return _vmInstanceService;
    }

    public User.Service getUserService() {
        return _userService;
    }

    public UserSession.Service getUserSessionService() {
        return _userSessionService;
    }

    public Task.Service getTaskService() {
        return _taskService;
    }

    public EventLog.Service getEventLogService() {
        return _eventLogService;
    }

    public TrackerManager getTrackerManager() {
        return _trackerManager;
    }
}

