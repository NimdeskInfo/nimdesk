/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.service.impl;

import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;

import org.apache.log4j.Logger;

import com.nimdesk.service.ObjectPropertyChangeEvent;
import com.nimdesk.service.ObjectPropertyChangeListener;
import com.nimdesk.service.ObjectPropertyChangeNotifier;

public class ObjectPropertyChangeSupport implements ObjectPropertyChangeNotifier {
    private static final Logger LOGGER = Logger.getLogger(ObjectPropertyChangeSupport.class);

    private static final Object PRESENT = new Object();

    private final Map<ObjectPropertyChangeListener, Object> _listeners =
            Collections.synchronizedMap(new WeakHashMap<ObjectPropertyChangeListener, Object>());

    @Override
    public void addObjectPropertyChangeListener(ObjectPropertyChangeListener listener) {
        _listeners.put(listener, PRESENT);
    }

    @Override
    public void removeObjectPropertyChangeListener(ObjectPropertyChangeListener listener) {
        _listeners.remove(listener);
    }

    protected void fireCreateEvent(ObjectPropertyChangeEvent event) {
        Set<ObjectPropertyChangeListener> listenerSet = new HashSet<ObjectPropertyChangeListener>();

        synchronized(_listeners) {
            listenerSet.addAll(_listeners.keySet());
        }

        for (ObjectPropertyChangeListener listener : listenerSet) {
            try {
                listener.newObject(event);
            } catch (Throwable t) {
                LOGGER.error("Listener.newObject failed", t);
            }
        }
    }

    protected void fireDeleteEvent(ObjectPropertyChangeEvent event) {
        Set<ObjectPropertyChangeListener> listenerSet = new HashSet<ObjectPropertyChangeListener>();

        synchronized(_listeners) {
            listenerSet.addAll(_listeners.keySet());
        }

        for (ObjectPropertyChangeListener listener : listenerSet) {
            try {
                listener.deleteObject(event);
            } catch (Throwable t) {
                LOGGER.error("Listener.deleteObject failed", t);
            }
        }
    }

    protected void fireChangeEvent(ObjectPropertyChangeEvent event) {
        Set<ObjectPropertyChangeListener> listenerSet = new HashSet<ObjectPropertyChangeListener>();

        synchronized(_listeners) {
            listenerSet.addAll(_listeners.keySet());
        }

        for (ObjectPropertyChangeListener listener : listenerSet) {
            try {
                listener.propertyChange(event);
            } catch (Throwable t) {
                LOGGER.error("Listener.propertyChange failed", t);
            }
        }
    }
}
