/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.service.impl.workflow;

import org.apache.log4j.Logger;

import com.nimdesk.database.vo.VmInstanceVO;
import com.nimdesk.event.AsyncCallback;
import com.nimdesk.event.ConnectHostEvent;
import com.nimdesk.event.PowerOpVmEvent;
import com.nimdesk.hypervisor.HypervisorContext;
import com.nimdesk.model.Host;
import com.nimdesk.model.Pool;
import com.nimdesk.model.Server;
import com.nimdesk.model.Task;
import com.nimdesk.model.VmInstance;
import com.nimdesk.service.EventBusService;
import com.nimdesk.service.Workflow;
import com.nimdesk.service.impl.ServiceLocator;

public class StopDesktopFlow extends Workflow {
    private static final Logger LOGGER = Logger.getLogger(StopDesktopFlow.class);

    private final EventBusService _eventBusService;
    private final Host.Service _hostService;
    private final VmInstance.Service _vmInstanceService;

    private final Server _localServer;
    private final Host _localHost;
    private final Pool _pool;
    private final VmInstance _vmInstance;

    // ConnectHostState result
    private HypervisorContext _hypervisorContext;

    public StopDesktopFlow(Host localHost, Pool pool, VmInstance vmInstance, final AsyncCallback callback) {
        super();

        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("StopDesktopFlow()");
        }

        ServiceLocator serviceLocator = ServiceLocator.getInstance();

        _eventBusService = serviceLocator.getEventBusService();
        _hostService = serviceLocator.getHostService();
        _vmInstanceService = serviceLocator.getVmInstanceService();

        Server.Service serverService = serviceLocator.getServerService();
        _localServer = serverService.getLocalServer();
        if (_localServer == null) {
            throw new RuntimeException(String.format("Unable to find host to stop desktop for pool \"%s\"", pool.getName()));
        }

        if (localHost != null) {
            _localHost = localHost;
        } else {
            _localHost = _hostService.getLocalHost();
        }
        if (_localHost == null) {
            throw new RuntimeException(String.format("Unable to find host to stop desktop for pool \"%s\"", pool.getName()));
        }

        _pool = pool;

        _vmInstance = vmInstance;
        /*_task = */createTask(_vmInstance, _pool, (callback != null) ? callback.getRequestor() : null);

        append(new ConnectHostState(), null);
        append(new PowerOffVmState(), null);

        setCallback(new AsyncCallback() {
            @Override
            public void onSuccess(Object result) {
                disconnectHost();

                VmInstance vmInstance = _vmInstanceService.getByUuid(_vmInstance.getUuid());
                if (vmInstance == null
                        || VmInstance.Status.Destroying == vmInstance.getStatus()
                        || VmInstance.Status.Destroyed == vmInstance.getStatus()) {
                    LOGGER.error(String.format("Unable to find VmInstance id=\"%s\" in db", _vmInstance.getUuid()));
                } else {
                    if (VmInstance.Status.Stopping == vmInstance.getStatus()) {
                        vmInstance.setLastError(null);
                        vmInstance.setStatus(VmInstance.Status.Ready);
                        vmInstance.setUpdateTime(System.currentTimeMillis());
                        _vmInstanceService.commit(vmInstance);
                    }
                }

                LOGGER.info(String.format("StopDesktopFlow: Stopped desktop \"%s\" from pool \"%s\"",
                        _vmInstance.getName(), _pool.getName()));

                if (callback != null) {
                    callback.onSuccess(result);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                disconnectHost();

                // If failed to stop, refresh it.
                VmInstance vmInstance = _vmInstanceService.getByUuid(_vmInstance.getUuid());
                if (vmInstance == null
                        || VmInstance.Status.Destroying == vmInstance.getStatus()
                        || VmInstance.Status.Destroyed == vmInstance.getStatus()) {
                    LOGGER.error(String.format("Unable to find VmInstance id=\"%s\" in db", _vmInstance.getUuid()));
                } else {
                    switch (_pool.getType()) {
                    case Persistent:
                        vmInstance.setStatus(VmInstance.Status.Error);
                        vmInstance.setLastError(VmInstance.Error.PowerOffError);
                        vmInstance.setUpdateTime(System.currentTimeMillis());
                        _vmInstanceService.commit(vmInstance);
                        break;

                    default:
                        _vmInstanceService.refreshVm(_pool, vmInstance, null);
                        break;
                    }
                }

                LOGGER.error(String.format("StopDesktopFlow: Failed to stop desktop \"%s\" from pool \"%s\" - %s",
                        _vmInstance.getName(), _pool.getName(), t.getMessage()), t);

                if (callback != null) {
                    callback.onFailure(t);
                }
            }

            @Override
            public void progress(int percentage) {
                if (callback != null) {
                    callback.progress(percentage);
                }
            }

            @Override
            public String getRequestor() {
                return (callback != null) ? callback.getRequestor() : null;
            }
        });
    }

    private void disconnectHost() {
        if (_hypervisorContext != null) {
            _hypervisorContext.disconnect();
            _hypervisorContext = null;
        }
    }

    private Task createTask(VmInstance vmInstance, Pool pool, String requestor) {
        if (vmInstance instanceof VmInstanceVO) {
            // Suppressing PoolManager to start another vmInstance stopping.
            ((VmInstanceVO) vmInstance).inOperation(true);
        }
        if (VmInstance.Status.Stopping != vmInstance.getStatus()) {
            vmInstance.setLoginTime(0);
            vmInstance.setLogOffMode(null);
            vmInstance.setStatus(VmInstance.Status.Stopping);
            vmInstance.setUpdateTime(System.currentTimeMillis());
            _vmInstanceService.commit(_vmInstance);
        }

        return null;
    }

    private class ConnectHostState implements Workflow.StateRunnable {
        @Override
        public void run(AsyncCallback callback) {
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace("ConnectHostState.run()");
            }

            ConnectHostEvent event = new ConnectHostEvent(_localHost.getAddress(), 0,
                    _localHost.getUsername(), _localHost.getPassword(), callback);
            _eventBusService.post(event);
        }

        @Override
        public void setResult(Object result) {
            _hypervisorContext = (HypervisorContext) result;
        }
    }

    private class PowerOffVmState implements Workflow.StateRunnable {
        @Override
        public void run(AsyncCallback callback) {
            LOGGER.info(String.format("PowerOffVmState.run() - vmInstance: %s", _vmInstance.getName()));

            PowerOpVmEvent event = new PowerOpVmEvent(_hypervisorContext,
                    _vmInstance.getVmRefId(), PowerOpVmEvent.PowerOps.PowerOff, callback);;
            _eventBusService.post(event);
        }

        @Override
        public void setResult(Object result) {
        }
    }
}
