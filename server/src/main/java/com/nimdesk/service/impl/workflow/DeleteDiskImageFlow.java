/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.service.impl.workflow;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.nimdesk.event.AsyncCallback;
import com.nimdesk.event.ConnectHostEvent;
import com.nimdesk.event.DeleteStorageDirectoryEvent;
import com.nimdesk.event.UninstantiateVmEvent;
import com.nimdesk.hypervisor.HypervisorContext;
import com.nimdesk.model.DiskImage;
import com.nimdesk.model.Host;
import com.nimdesk.model.Server;
import com.nimdesk.model.Storage;
import com.nimdesk.service.EventBusService;
import com.nimdesk.service.Workflow;
import com.nimdesk.service.impl.ServiceLocator;

/**
 * Step 1: Connect to host
 * Step 2: Delete VM directory from datastore
 */
public class DeleteDiskImageFlow extends Workflow {
    private static final Logger LOGGER = Logger.getLogger(DeleteDiskImageFlow.class);

    private final EventBusService _eventBusService;
    private final Host.Service _hostService;
    private final Storage.Service _storageService;
    private final DiskImage.Service _diskImageService;

    private final Server _localServer;
    private final Host _localHost;
    private final DiskImage _diskImage;

    // ConnectHostState result
    private HypervisorContext _hypervisorContext;

    // ConnectVcState result
    private HypervisorContext _vcHypervisorContext = null;

    public DeleteDiskImageFlow(DiskImage diskImage, final AsyncCallback callback) {
        super();

        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("DeleteDiskImageFlow()");
        }

        ServiceLocator serviceLocator = ServiceLocator.getInstance();

        _eventBusService = serviceLocator.getEventBusService();
        _hostService = serviceLocator.getHostService();
        _storageService = serviceLocator.getStorageService();
        _diskImageService = serviceLocator.getDiskImageService();

        Server.Service serverService = serviceLocator.getServerService();
        _localServer = serverService.getLocalServer();
        if (_localServer == null) {
            throw new RuntimeException(String.format("Unable to find host to delete disk image \"%s\"", diskImage.getName()));
        }

        _localHost = _hostService.getLocalHost();
        if (_localHost == null) {
            throw new RuntimeException(String.format("Unable to find host to delete disk image \"%s\"", diskImage.getName()));
        }

        _diskImage = diskImage;

        append(new ConnectHostState(), null);
        if (!StringUtils.isEmpty(_localHost.getVCenterIp())
                && !StringUtils.isEmpty(_localHost.getVCenterUsername())
                && !StringUtils.isEmpty(_localHost.getVCenterPassword())) {
            append(new ConnectVCenterState(), null);
        }
        if (!StringUtils.isEmpty(diskImage.getVmRefId())) {
            // The following steps are needed only if VM exists.
            append(new UninstantiateVmState(), null);
        }
        append(new DeleteDiskImageState(), null);

        setCallback(new AsyncCallback() {
            @Override
            public void onSuccess(Object result) {
                disconnectHost();

                if (_diskImage != null) {
                    try {
                        _diskImageService.delete(_diskImage);
                    } catch (Exception e) {
                        // Ignore error.
                    }
                }

                LOGGER.info(String.format("DeleteDiskImageFlow: Deleted disk image \"%s\"",
                        (_diskImage != null) ? _diskImage.getName() : "(unknown)"));

                if (callback != null) {
                    callback.onSuccess(result);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                disconnectHost();

                if (_diskImage == null) {
                    // If no disk image can be found, we consider the deletion is successful.
                    onSuccess(null);
                    return;
                } else {
                    _diskImageService.delete(_diskImage);
                }

                LOGGER.error(String.format("DeleteDiskImageFlow: Failed to delete disk image \"%s\" - %s",
                        (_diskImage != null) ? _diskImage.getName() : "(unknown)", t.getMessage()));

                if (callback != null) {
                    callback.onFailure(t);
                }
            }

            @Override
            public void progress(int percentage) {
                if (callback != null) {
                    callback.progress(percentage);
                }
            }

            @Override
            public String getRequestor() {
                return (callback != null) ? callback.getRequestor() : null;
            }
        });
    }

    private void disconnectHost() {
        if (_hypervisorContext != null) {
            _hypervisorContext.disconnect();
            _hypervisorContext = null;
        }
        if (_vcHypervisorContext != null) {
            _vcHypervisorContext.disconnect();
            _vcHypervisorContext = null;
        }
    }

    private class ConnectHostState implements Workflow.StateRunnable {
        @Override
        public void run(AsyncCallback callback) {
            if (LOGGER.isTraceEnabled()) {
                LOGGER.info("ConnectHostState.run()");
            }

            ConnectHostEvent event = new ConnectHostEvent(_localHost.getAddress(), 0,
                    _localHost.getUsername(), _localHost.getPassword(), callback);
            _eventBusService.post(event);
        }

        @Override
        public void setResult(Object result) {
            _hypervisorContext = (HypervisorContext) result;
        }
    }

    private class ConnectVCenterState implements Workflow.StateRunnable {
        @Override
        public void run(AsyncCallback callback) {
            if (LOGGER.isTraceEnabled()) {
                LOGGER.info("ConnectVCenterState.run()");
            }

            String vCenterAddress = _localHost.getVCenterAddress();
            if (StringUtils.isEmpty(vCenterAddress)) {
                vCenterAddress = _localHost.getVCenterIp();
            }

            ConnectHostEvent event = new ConnectHostEvent(vCenterAddress, 0,
                    _localHost.getVCenterUsername(), _localHost.getVCenterPassword(), callback);
            _eventBusService.post(event);
        }

        @Override
        public void setResult(Object result) {
            _vcHypervisorContext = (HypervisorContext) result;
        }
    }

    private class UninstantiateVmState implements Workflow.StateRunnable {
        @Override
        public void run(final AsyncCallback callback) {
            LOGGER.info(String.format("UninstantiateVmState.run() - vmRefId: %s", _diskImage.getVmRefId()));

            UninstantiateVmEvent event = new UninstantiateVmEvent(_hypervisorContext, _diskImage.getVmRefId(),
                    _vcHypervisorContext, _localHost.getEsxUuid(),
                    new AsyncCallback() {
                        @Override
                        public void onSuccess(Object result) {
                            LOGGER.info(String.format("DeleteDiskImageFlow: Uninstantiated vm for image \"%s\"", _diskImage.getName()));

                            if (callback != null) {
                                callback.onSuccess(null);
                            }
                        }

                        @Override
                        public void onFailure(Throwable t) {
                            LOGGER.warn(String.format("DeleteDiskImageFlow: Failed to uninstantiate vm for image \"%s\" - %s. Continue...",
                                    _diskImage.getName(), t.getMessage()), t);

                            // Continue the flow even if this step fails.
                            if (callback != null) {
                                callback.onSuccess(null);
                            }
                        }

                        @Override
                        public void progress(int percentage) {
                        }

                        @Override
                        public String getRequestor() {
                            return DeleteDiskImageFlow.this.getRequestor();
                        }
            });
            _eventBusService.post(event);
        }

        @Override
        public void setResult(Object result) {
        }
    }

    private class DeleteDiskImageState implements Workflow.StateRunnable {
        @Override
        public void run(AsyncCallback callback) {
            if (LOGGER.isTraceEnabled()) {
                LOGGER.info("DeleteDiskImageState.run()");
            }

            Storage storage = _storageService.getByUuid(_diskImage.getStorageUuid());
            if (storage != null) {
                DeleteStorageDirectoryEvent event = new DeleteStorageDirectoryEvent(_hypervisorContext, _diskImage.getName(),
                    storage, callback);
                _eventBusService.post(event);

                return;
            }

            // If storage is not found, succeed.
            if (callback != null) {
                callback.onSuccess(null);
            }
        }

        @Override
        public void setResult(Object result) {
        }
    }
}
