/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.nimdesk.database.dao.LdapServerDao;
import com.nimdesk.database.vo.LdapServerVO;
import com.nimdesk.model.LdapServer;
import com.nimdesk.service.ObjectPropertyChangeEvent;
import com.nimdesk.util.IdGenerator;
import com.nimdesk.util.Pagination;
import com.nimdesk.util.Pair;

@Service(LdapServer.Service.BEAN_NAME)
public class LdapServerServiceImpl extends ObjectPropertyChangeSupport
         implements LdapServer.Service, InitializingBean {
    private static final Logger LOGGER = Logger.getLogger(LdapServerServiceImpl.class);

    @Autowired
    private LdapServerDao _ldapServerDao;

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(_ldapServerDao, "An LdapServerDao must be set");
    }

    @Override
    public Long count() {
        return _ldapServerDao.countAll();
    }

    @Override
    public LdapServer newInstance() {
        LdapServer ldapServer = new LdapServerVO();

        // Generate uuid
        ldapServer.setUuid(IdGenerator.generateUuid());

        return ldapServer;
    }

    @Override
    public List<? extends LdapServer> getAll(Pagination pagination) {
        return _ldapServerDao.findAll(pagination);
    }

    @Override
    public LdapServer getByUuid(String uuid) {
        return _ldapServerDao.findByUuid(uuid);
    }

    @Override
    public void commit(LdapServer obj) {
        if (!(obj instanceof LdapServerVO)) {
            String error = String.format("commit: passing wrong type %s", obj.getClass().toString());
            LOGGER.error(error);
            throw new IllegalArgumentException(error);
        }

        LdapServerVO ldapServer = (LdapServerVO) obj;

        if (ldapServer.getId() == 0) {
            _ldapServerDao.createNew(ldapServer);
            ldapServer.resetChangedProperties();
            fireCreateEvent(new ObjectPropertyChangeEvent(ldapServer, null));
        } else {
            _ldapServerDao.update(ldapServer);
            Map<String, Pair<Object, Object>> changedProperties = ldapServer.resetChangedProperties();
            fireChangeEvent(new ObjectPropertyChangeEvent(ldapServer, changedProperties));
        }
    }

    @Override
    public boolean delete(LdapServer obj) {
        if (!(obj instanceof LdapServerVO)) {
            String error = String.format("delete: passing wrong type %s", obj.getClass().toString());
            LOGGER.error(error);
            throw new IllegalArgumentException(error);
        }

        LdapServerVO ldapServer = (LdapServerVO) obj;

        try {
            if (ldapServer.getId() > 0) {
                return _ldapServerDao.delete(ldapServer.getId());
            }
        } finally {
            fireDeleteEvent(new ObjectPropertyChangeEvent(ldapServer));
        }

        return true;
    }

    @Override
    public LdapServer getByDomain(String domain) {
        return _ldapServerDao.findByDomain(domain);
    }
}
