/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.service.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.nimdesk.database.dao.DiskImageDao;
import com.nimdesk.database.dao.VmInstanceDao;
import com.nimdesk.database.vo.VmInstanceVO;
import com.nimdesk.event.AsyncCallback;
import com.nimdesk.model.DiskImage;
import com.nimdesk.model.Host;
import com.nimdesk.model.Pool;
import com.nimdesk.model.VmInstance;
import com.nimdesk.model.VmInstance.Status;
import com.nimdesk.service.EventBusService;
import com.nimdesk.service.ObjectPropertyChangeEvent;
import com.nimdesk.service.Scheduler;
import com.nimdesk.util.IdGenerator;
import com.nimdesk.util.Pagination;
import com.nimdesk.util.Pair;

@Service(VmInstance.Service.BEAN_NAME)
public class VmInstanceServiceImpl extends ObjectPropertyChangeSupport implements VmInstance.Service, InitializingBean {
    private static final Logger LOGGER = Logger.getLogger(VmInstanceServiceImpl.class);

    @Autowired
    private Scheduler _scheduler;

    @Autowired
    private EventBusService _eventBusService;

    @Autowired
    private VmInstanceDao _vmInstanceDao;

    @Autowired
    private DiskImageDao _diskImageDao;

    @Autowired
    private Host.Service _hostService;

    @Autowired
    private Pool.Service _poolService;

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(_scheduler, "A Scheduler must be set");
        Assert.notNull(_eventBusService, "An EventBusService must be set");
        Assert.notNull(_vmInstanceDao, "A VmInstanceDao must be set");
        Assert.notNull(_diskImageDao, "A DiskImageDao must be set");
        Assert.notNull(_hostService, "A Host.Service must be set");
        Assert.notNull(_poolService, "A Pool.Service must be set");
    }

    @Override
    public Long count() {
        return _vmInstanceDao.countAll();
    }

    @Override
    public VmInstance newInstance() {
        VmInstance vmInstance = new VmInstanceVO();

        // Generate uuid
        vmInstance.setUuid(IdGenerator.generateUuid());

        return vmInstance;
    }

    @Override
    public List<? extends VmInstance> getAll(Pagination pagination) {
        return _vmInstanceDao.findAll(pagination);
    }

    @Override
    public VmInstance getByUuid(String uuid) {
        return _vmInstanceDao.findByUuid(uuid);
    }

    @Override
    public void commit(VmInstance obj) {
        if (!(obj instanceof VmInstanceVO)) {
            String error = String.format("commit: passing wrong type %s", obj.getClass().toString());
            LOGGER.error(error);
            throw new IllegalArgumentException(error);
        }

        VmInstanceVO vmInstance = (VmInstanceVO) obj;

        if (vmInstance.getId() == 0) {
            _vmInstanceDao.createNew(vmInstance);
            vmInstance.resetChangedProperties();

            // Any listener that modifies the entity must synchronize it.
            synchronized (vmInstance) {
                fireCreateEvent(new ObjectPropertyChangeEvent(vmInstance));
            }
        } else {
            _vmInstanceDao.update(vmInstance);
            Map<String, Pair<Object, Object>> changedProperties = vmInstance.resetChangedProperties();

            // Any listener that modifies the entity must synchronize it.
            synchronized (vmInstance) {
                fireChangeEvent(new ObjectPropertyChangeEvent(vmInstance, changedProperties));
            }
        }
    }

    @Override
    public boolean delete(VmInstance obj) {
        if (!(obj instanceof VmInstanceVO)) {
            String error = String.format("delete: passing wrong type %s", obj.getClass().toString());
            LOGGER.error(error);
            throw new IllegalArgumentException(error);
        }

        VmInstanceVO vmInstance = (VmInstanceVO) obj;

        try {
            if (vmInstance.getId() > 0) {
                return _vmInstanceDao.delete(vmInstance.getId());
            }
        } finally {
            fireDeleteEvent(new ObjectPropertyChangeEvent(vmInstance));
        }

        return true;
    }

    @Override
    public Long countByHost(String hostUuid) {
        return _vmInstanceDao.countByHostAndStatus(hostUuid);
    }

    @Override
    public Long countByPool(String poolUuid) {
        return _vmInstanceDao.countByPoolAndStatus(poolUuid);
    }

    @Override
    public Long countByPoolAndStatus(String poolUuid, VmInstance.Status... status) {
        return _vmInstanceDao.countByPoolAndStatus(poolUuid, status);
    }

    @Override
    public Long countByPoolAndStatusOnHost(String hostUuid, String poolUuid, VmInstance.Status... status) {
        return _vmInstanceDao.countByPoolAndStatusOnHost(hostUuid, poolUuid, status);
    }

    @Override
    public Long countLocalByPoolAndStatus(String poolUuid, VmInstance.Status... status) {
        Host localHost = _hostService.getLocalHost();

        return _vmInstanceDao.countByPoolAndStatusOnHost(localHost.getUuid(), poolUuid, status);
    }

    @Override
    public Long countAllInSession() {
        return _vmInstanceDao.countAllInSession();
    }

    @Override
    public Long countLocalByStatus(VmInstance.Status... status) {
        Host localHost = _hostService.getLocalHost();

        return _vmInstanceDao.countByHostAndStatus(localHost.getUuid(), status);
    }

    @Override
    public Long countByStatus(VmInstance.Status... status) {
        return _vmInstanceDao.countByStatus(status);
    }

    @Override
    public Long countByStatusOnHost(String hostUuid, VmInstance.Status... status) {
        return _vmInstanceDao.countByHostAndStatus(hostUuid, status);
    }

    @Override
    public List<? extends VmInstance> getAllByPool(String poolUuid, Pagination pagination) {
        return _vmInstanceDao.findByPool(poolUuid, pagination);
    }

    @Override
    public List<? extends VmInstance> getUnassignedByPool(String poolUuid, Pagination pagination) {
        return _vmInstanceDao.findUnassignedByPool(poolUuid, pagination);
    }

    @Override
    public List<? extends VmInstance> getAllInPoolByUser(String poolUuid, String userUuid) {
        return _vmInstanceDao.findInPoolByUser(poolUuid, userUuid);
    }

    @Override
    public List<? extends VmInstance> getAllInPoolWithNoUser(String poolUuid) {
        return _vmInstanceDao.findInPoolWithNoUser(poolUuid);
    }

    @Override
    public List<? extends VmInstance> getByPoolAndStatus(Pagination pagination, String poolUuid, Status... status) {
        return _vmInstanceDao.findByPoolAndStatus(pagination, poolUuid, status);
    }

    @Override
    public List<? extends VmInstance> getByPoolAndStatusOnHost(String hostUuid, String poolUuid, Status... status) {
        return _vmInstanceDao.findByPoolAndStatusOnHost(hostUuid, poolUuid, status);
    }

    @Override
    public List<? extends VmInstance> getLocalByPoolAndStatus(String poolUuid, Status... status) {
        Host localHost = _hostService.getLocalHost();

        return _vmInstanceDao.findByPoolAndStatusOnHost(localHost.getUuid(), poolUuid, status);
    }

    @Override
    public List<? extends VmInstance> getByStatus(Pagination pagination, Status... status) {
        return _vmInstanceDao.findByStatus(pagination, status);
    }

    @Override
    public DiskImage getBaseDiskImage(VmInstance vmInstance) {
        String diskImageUuid = vmInstance.getDiskImageUuid();
        if (StringUtils.isEmpty(diskImageUuid)) {
            return null;
        }

        return _diskImageDao.findByUuid(diskImageUuid);
    }

    @Override
    public void refreshVm(Pool pool, VmInstance vmInstance, final AsyncCallback callback) {
        try {
            switch (vmInstance.getStatus()) {
            case Destroying:
            case Destroyed:
            case Refreshing:
                break;

            default:
                vmInstance.setStatus(VmInstance.Status.Refreshing);
                vmInstance.setUpdateTime(System.currentTimeMillis());
                commit(vmInstance);
                break;
            }

            if (callback != null) {
                callback.onSuccess(null);
            }
        } catch (Exception e) {
            LOGGER.error(String.format("Failed to refresh vmInstance \"%s\" for pool \"%s\"",
                    vmInstance.getName(), pool.getName()), e);

            if (callback != null) {
                callback.onFailure(new Exception("Failed to refresh desktop"));
            }
        }
    }

    @Override
    public void powerOnVm(Pool pool, VmInstance vmInstance, final AsyncCallback callback) {
        switch (vmInstance.getStatus()) {
        case Destroying:
        case Destroyed:
        case Starting:
            break;

        default:
            vmInstance.setStatus(VmInstance.Status.Starting);
            vmInstance.setUpdateTime(System.currentTimeMillis());
            commit(vmInstance);
            break;
        }

        if (callback != null) {
            callback.onSuccess(null);
        }
    }

    @Override
    public void powerOffVm(Pool pool, VmInstance vmInstance, AsyncCallback callback) {
        switch (vmInstance.getStatus()) {
        case Destroying:
        case Destroyed:
        case Stopping:
            break;

        default:
            vmInstance.setStatus(VmInstance.Status.Stopping);
            vmInstance.setUpdateTime(System.currentTimeMillis());
            commit(vmInstance);
            break;
        }

        if (callback != null) {
            callback.onSuccess(null);
        }
    }

    @Override
    public void deleteVm(final Pool pool, final VmInstance vmInstance, final AsyncCallback callback) {
        if (isOrphanedVmInstance(vmInstance)) {
            delete(vmInstance);
        } else {
            if (vmInstance.isRecompose()
                    && VmInstance.Status.InSession == vmInstance.getStatus()) {
                vmInstance.setLogOffMode(VmInstance.LogOffMode.LogOff);
                vmInstance.setUpdateTime(System.currentTimeMillis());
                commit(vmInstance);

                _scheduler.schedule(new Runnable() {
                    @Override
                    public void run() {
                        VmInstance vmInstance2 = getByUuid(vmInstance.getUuid());
                        if (vmInstance2 != null
                                && VmInstance.Status.InSession == vmInstance2.getStatus()) {
                            vmInstance2.setStatus(VmInstance.Status.LoggedOff);
                            deleteVm(pool, vmInstance2, callback);
                        }
                    }
                }, 90L); // Wait at most 2 minutes.

                return;
            }

            // Set vmInstance status to Destroying and let corresponding node to take
            // care of it automatically.
            switch (vmInstance.getStatus()) {
            case Destroying:
            case Destroyed:
                break;

            default:
                vmInstance.setAssigned(false);
                vmInstance.setLoginTime(0);
                vmInstance.setLogOffMode(null);
                vmInstance.setStatus(VmInstance.Status.Destroying);
                vmInstance.setUpdateTime(System.currentTimeMillis());
                commit(vmInstance);
                break;
            }
        }

        if (callback != null) {
            callback.onSuccess(null);
        }
    }

    @Override
    public void unassignVm(VmInstance vmInstance, AsyncCallback callback) {
        vmInstance.setUserUuid("");
        vmInstance.setAssigned(false);
        vmInstance.setUpdateTime(System.currentTimeMillis());
        commit(vmInstance);

        if (callback != null) {
            callback.onSuccess(null);
        }
    }

    @Override
    public List<? extends VmInstance> getLocalVmInstances() {
        List<? extends Host> localHosts = _hostService.getLocalHosts();

        String[] hostUuids = new String[localHosts.size()];
        for (int idx = 0; idx < localHosts.size(); idx++) {
            hostUuids[idx] = localHosts.get(idx).getUuid();
        }

        return _vmInstanceDao.findByHosts(hostUuids);
    }

    @Override
    public List<? extends VmInstance> getLocalVmInstancesByPool(String poolUuid) {
        List<? extends Host> localHosts = _hostService.getLocalHosts();

        String[] hostUuids = new String[localHosts.size()];
        for (int idx = 0; idx < localHosts.size(); idx++) {
            hostUuids[idx] = localHosts.get(idx).getUuid();
        }

        return _vmInstanceDao.findByPoolAndHosts(poolUuid, hostUuids);
    }

    @Override
    public boolean isLocalVmInstance(VmInstance vmInstance) {
        return _hostService.isLocalHost(vmInstance.getHostUuid());
    }

    @Override
    public boolean isOrphanedVmInstance(VmInstance vmInstance) {
        if (StringUtils.isEmpty(vmInstance.getHostUuid())) {
            return true;
        }

        return _hostService.getByUuid(vmInstance.getHostUuid()) == null;
    }

    @Override
    public VmInstance getLocalVmInstanceByVmRefId(String vmRefId) {
        Host host = _hostService.getLocalHost();
        if (host == null) {
            LOGGER.warn("Unable to find local host");
            return null;
        }

        return _vmInstanceDao.findByHostVmRefId(host.getUuid(), vmRefId);
    }

    @Override
    public List<? extends VmInstance> getVmInstancesOnHost(String hostUuid) {
        return _vmInstanceDao.findByHosts(hostUuid);
    }

    @Override
    public void invalidateNonLocalVmInstances() {
        Host localHost = _hostService.getLocalHost();
        if (localHost != null) {
            _vmInstanceDao.invalidateNonLocalVmInstances(localHost.getUuid());
        }

        // Remove all local orphaned VmInstances
        List<? extends VmInstance> vmInstances = getByStatus(null,
                VmInstance.Status.Unknown, VmInstance.Status.Destroying);
        if (vmInstances != null && !vmInstances.isEmpty()) {
            Set<String> unknownHostUuids = new HashSet<String>();
            Set<String> unknownPoolUuids = new HashSet<String>();

            for (VmInstance vmInstance : vmInstances) {
                String hostUuid = vmInstance.getHostUuid();
                if (unknownHostUuids.contains(hostUuid)) {
                    delete(vmInstance);
                    continue;
                } else {
                    Host host = _hostService.getByUuid(hostUuid);
                    if (host == null) {
                        unknownHostUuids.add(hostUuid);
                        delete(vmInstance);
                        continue;
                    }
                }

                String poolUuid = vmInstance.getPoolUuid();
                if (unknownPoolUuids.contains(poolUuid)) {
                    delete(vmInstance);
                    continue;
                } else {
                    Pool pool = _poolService.getByUuid(poolUuid);
                    if (pool == null) {
                        unknownPoolUuids.add(poolUuid);
                        delete(vmInstance);
                        continue;
                    }
                }
            }
        }
    }
}

