/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.nimdesk.database.dao.UserDao;
import com.nimdesk.database.dao.UserGroupDao;
import com.nimdesk.database.vo.UserGroupVO;
import com.nimdesk.database.vo.UserVO;
import com.nimdesk.model.User;
import com.nimdesk.model.UserGroup;
import com.nimdesk.service.ObjectPropertyChangeEvent;
import com.nimdesk.util.IdGenerator;
import com.nimdesk.util.Pagination;
import com.nimdesk.util.Pair;

@Service(UserGroup.Service.BEAN_NAME)
public class UserGroupServiceImpl extends ObjectPropertyChangeSupport
        implements UserGroup.Service, InitializingBean {
    private static final Logger LOGGER = Logger.getLogger(UserGroupServiceImpl.class);

    @Autowired
    private UserGroupDao _userGroupDao;

    @Autowired
    private UserDao _userDao;

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(_userGroupDao, "A UserGroupDao must be set");
        Assert.notNull(_userDao, "A UserDao must be set");
    }

    @Override
    public Long count() {
        LOGGER.error("count() not implemented");
        throw new RuntimeException("Not implemented");
    }

    @Override
    public UserGroup newInstance() {
        UserGroup userGroup = new UserGroupVO();

        // Generate uuid
        userGroup.setUuid(IdGenerator.generateUuid());

        return userGroup;
    }

    @Override
    public List<? extends UserGroup> getAll(Pagination pagination) {
        return _userGroupDao.findAll(pagination);
    }

    @Override
    public UserGroup getByUuid(String uuid) {
        return _userGroupDao.findByUuid(uuid);
    }

    @Override
    public void commit(UserGroup obj) {
        if (!(obj instanceof UserGroupVO)) {
            String error = String.format("commit: passing wrong type %s", obj.getClass().toString());
            LOGGER.error(error);
            throw new IllegalArgumentException(error);
        }

        UserGroupVO userGroup = (UserGroupVO) obj;

        if (userGroup.getId() == 0) {
            _userGroupDao.createNew(userGroup);
            userGroup.resetChangedProperties();
            fireCreateEvent(new ObjectPropertyChangeEvent(userGroup, null));
        } else {
            _userGroupDao.update(userGroup);
            Map<String, Pair<Object, Object>> changedProperties = userGroup.resetChangedProperties();
            fireChangeEvent(new ObjectPropertyChangeEvent(userGroup, changedProperties));
        }
    }

    @Override
    public boolean delete(UserGroup obj) {
        if (!(obj instanceof UserGroupVO)) {
            String error = String.format("delete: passing wrong type %s", obj.getClass().toString());
            LOGGER.error(error);
            throw new IllegalArgumentException(error);
        }

        UserGroupVO userGroup = (UserGroupVO) obj;

        try {
            if (userGroup.getId() > 0) {
                return _userGroupDao.delete(userGroup.getId());
            }
        } finally {
            fireDeleteEvent(new ObjectPropertyChangeEvent(userGroup));
        }

        return true;
    }

    @Override
    public List<? extends User> getGroupsForUser(String userUuid) {
        List<UserGroupVO> userGroups = _userGroupDao.findByUser(userUuid);
        if (userGroups == null) {
            return null;
        }

        List<UserVO> groups = new ArrayList<UserVO>();
        for (UserGroupVO userGroup : userGroups) {
            UserVO group = _userDao.findByUuid(userGroup.getGroupUuid());
            if (group != null && group.getType() == User.Type.Group) {
                groups.add(group);
            }
        }

        return groups;
    }

    @Override
    public List<? extends User> getUserInGroup(String groupUuid) {
        List<UserGroupVO> userGroups = _userGroupDao.findByGroup(groupUuid);
        if (userGroups == null) {
            return null;
        }

        List<UserVO> users = new ArrayList<UserVO>();
        for (UserGroupVO userGroup : userGroups) {
            UserVO user = _userDao.findByUuid(userGroup.getUserUuid());
            if (user != null && user.getType() == User.Type.User) {
                users.add(user);
            }
        }

        return users;
    }

    @Override
    public void removeUsersFromGroup(String groupUuid, String... userUuids) {
        for (String userUuid : userUuids) {
            UserGroupVO userGroup = _userGroupDao.findByUserGroup(userUuid, groupUuid);
            if (userGroup != null) {
                delete(userGroup);
            }
        }

    }

    @Override
    public void removeGroupsFromUser(String userUuid, String... groupUuids) {
        for (String groupUuid : groupUuids) {
            UserGroupVO userGroup = _userGroupDao.findByUserGroup(userUuid, groupUuid);
            if (userGroup != null) {
                delete(userGroup);
            }
        }
    }

    @Override
    public void deleteUserGroupsForUser(String userUuid) {
        List<UserGroupVO> userGroups = _userGroupDao.findByUser(userUuid);
        if (userGroups == null) {
            return;
        }

        for (UserGroupVO userGroup : userGroups) {
            delete(userGroup);
        }
    }

    @Override
    public void deleteUserGroupsForGroup(String groupUuid) {
        List<UserGroupVO> userGroups = _userGroupDao.findByGroup(groupUuid);
        if (userGroups == null) {
            return;
        }

        for (UserGroupVO userGroup : userGroups) {
            delete(userGroup);
        }
    }

}
