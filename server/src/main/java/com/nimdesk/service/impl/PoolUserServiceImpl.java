/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.nimdesk.database.dao.PoolUserDao;
import com.nimdesk.database.vo.PoolUserVO;
import com.nimdesk.model.PoolUser;
import com.nimdesk.model.User;
import com.nimdesk.service.ObjectPropertyChangeEvent;
import com.nimdesk.util.IdGenerator;
import com.nimdesk.util.Pagination;
import com.nimdesk.util.Pair;

@Service(PoolUser.Service.BEAN_NAME)
public class PoolUserServiceImpl extends ObjectPropertyChangeSupport
            implements PoolUser.Service, InitializingBean {
    private static final Logger LOGGER = Logger.getLogger(PoolUserServiceImpl.class);

    @Autowired
    private PoolUserDao _poolUserDao;

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(_poolUserDao, "A PoolUserDao must be set");
    }

    @Override
    public Long count() {
        LOGGER.error("count() not implemented");
        throw new RuntimeException("Not implemented");
    }

    @Override
    public PoolUser newInstance() {
        PoolUser poolUser = new PoolUserVO();

        // Generate uuid
        poolUser.setUuid(IdGenerator.generateUuid());

        return poolUser;
    }

    @Override
    public List<? extends PoolUser> getAll(Pagination pagination) {
        return _poolUserDao.findAll(pagination);
    }

    @Override
    public PoolUser getByUuid(String uuid) {
        return _poolUserDao.findByUuid(uuid);
    }

    @Override
    public void commit(PoolUser obj) {
        if (!(obj instanceof PoolUserVO)) {
            String error = String.format("commit: passing wrong type %s", obj.getClass().toString());
            LOGGER.error(error);
            throw new IllegalArgumentException(error);
        }

        PoolUserVO poolUser = (PoolUserVO) obj;

        if (poolUser.getId() == 0) {
            _poolUserDao.createNew(poolUser);
            poolUser.resetChangedProperties();
            fireCreateEvent(new ObjectPropertyChangeEvent(poolUser, null));
        } else {
            _poolUserDao.update(poolUser);
            Map<String, Pair<Object, Object>> changedProperties = poolUser.resetChangedProperties();
            fireChangeEvent(new ObjectPropertyChangeEvent(poolUser, changedProperties));
        }
    }

    @Override
    public boolean delete(PoolUser obj) {
        if (!(obj instanceof PoolUserVO)) {
            String error = String.format("delete: passing wrong type %s", obj.getClass().toString());
            LOGGER.error(error);
            throw new IllegalArgumentException(error);
        }

        PoolUserVO poolUser = (PoolUserVO) obj;

        try {
            if (poolUser.getId() > 0) {
                return _poolUserDao.delete(poolUser.getId());
            }
        } finally {
            fireDeleteEvent(new ObjectPropertyChangeEvent(poolUser));
        }

        return true;
    }

    @Override
    public List<? extends PoolUser> getByPool(String poolUuid) {
        return _poolUserDao.findByPool(poolUuid);
    }

    @Override
    public List<? extends PoolUser> getByUser(String userUuid) {
        return _poolUserDao.findByUser(userUuid);
    }

    @Override
    public void deleteByPool(String poolUuid) {
        _poolUserDao.deleteByPool(poolUuid);
    }

    @Override
    public void deleteByUser(String userUuid) {
        _poolUserDao.deleteByUser(userUuid);
    }

    @Override
    public void assignUserToPool(User user, String poolUuid) {
        PoolUser poolUser = _poolUserDao.findByPoolUser(poolUuid, user.getUuid());
        if (poolUser != null) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug(String.format("User (%s) has already been assigned to pool (%s)",
                        user.getName(), poolUuid));
            }
            return;
        }

        poolUser = newInstance();
        poolUser.setPoolUuid(poolUuid);
        poolUser.setUserUuid(user.getUuid());

        commit(poolUser);
    }

    @Override
    public void unassignUserFromPool(User user, String poolUuid) {
        PoolUser poolUser = _poolUserDao.findByPoolUser(poolUuid, user.getUuid());
        if (poolUser == null) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug(String.format("User (%s) is not assigned to pool (%s)",
                        user.getName(), poolUuid));
            }
            return;
        }

        delete(poolUser);
    }
}
