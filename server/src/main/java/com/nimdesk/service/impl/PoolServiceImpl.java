/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.nimdesk.cluster.RemoteProxyFactory;
import com.nimdesk.database.dao.PoolDao;
import com.nimdesk.database.vo.PoolVO;
import com.nimdesk.event.AsyncCallback;
import com.nimdesk.event.DeletePoolEvent;
import com.nimdesk.event.InstantiatePoolEvent;
import com.nimdesk.event.ProvisionDesktopsEvent;
import com.nimdesk.event.ProvisionLocalDesktopEvent;
import com.nimdesk.event.RebalanceDesktopsEvent;
import com.nimdesk.event.RecomposeDesktopsEvent;
import com.nimdesk.model.DiskImage;
import com.nimdesk.model.Host;
import com.nimdesk.model.Pool;
import com.nimdesk.model.PoolUser;
import com.nimdesk.model.Server;
import com.nimdesk.model.User;
import com.nimdesk.model.VmInstance;
import com.nimdesk.service.EventBusService;
import com.nimdesk.service.ObjectPropertyChangeEvent;
import com.nimdesk.util.IdGenerator;
import com.nimdesk.util.Pagination;
import com.nimdesk.util.Pair;

@Service(Pool.Service.BEAN_NAME)
public class PoolServiceImpl extends ObjectPropertyChangeSupport implements Pool.Service, InitializingBean {
    private static final Logger LOGGER = Logger.getLogger(PoolServiceImpl.class);

    @Autowired
    private RemoteProxyFactory _remoteProxyFactory;

    @Autowired
    private PoolDao _poolDao;

    @Autowired
    private DiskImage.Service _diskImageService;

    @Autowired
    private Server.Service _serverService;

    @Autowired
    private Host.Service _hostService;

    @Autowired
    private VmInstance.Service _vmInstanceService;

    @Autowired
    private PoolUser.Service _poolUserService;

    @Autowired
    private EventBusService _eventBusService;

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(_remoteProxyFactory, "A RemoteProxyFactory must be set");
        Assert.notNull(_poolDao, "A PoolDao must be set");
        Assert.notNull(_diskImageService, "A DiskImage.Service must be set");
        Assert.notNull(_serverService, "A Server.Service must be set");
        Assert.notNull(_hostService, "A Host.Service must be set");
        Assert.notNull(_vmInstanceService, "A VmInstance.Service must be set");
        Assert.notNull(_poolUserService, "A PoolUser.Service must be set");
        Assert.notNull(_eventBusService, "An EventBusService must be set");
    }

    @Override
    public Long count() {
        return _poolDao.countAll();
    }

    @Override
    public Pool newInstance() {
        Pool pool = new PoolVO();

        // Generate uuid
        pool.setUuid(IdGenerator.generateUuid());

        return pool;
    }

    @Override
    public List<? extends Pool> getAll(Pagination pagination) {
        return _poolDao.findAll(pagination);
    }

    @Override
    public Pool getByUuid(String uuid) {
        return _poolDao.findByUuid(uuid);
    }

    @Override
    public void commit(Pool obj) {
        if (!(obj instanceof PoolVO)) {
            String error = String.format("commit: passing wrong type %s", obj.getClass().toString());
            LOGGER.error(error);
            throw new IllegalArgumentException(error);
        }

        PoolVO pool = (PoolVO) obj;

        if (pool.getId() == 0) {
            _poolDao.createNew(pool);
            pool.resetChangedProperties();
            fireCreateEvent(new ObjectPropertyChangeEvent(pool, null));
        } else {
            _poolDao.update(pool);
            Map<String, Pair<Object, Object>> changedProperties = pool.resetChangedProperties();
            fireChangeEvent(new ObjectPropertyChangeEvent(pool, changedProperties));
        }
    }

    @Override
    public boolean delete(Pool obj) {
        if (!(obj instanceof PoolVO)) {
            String error = String.format("delete: passing wrong type %s", obj.getClass().toString());
            LOGGER.error(error);
            throw new IllegalArgumentException(error);
        }

        PoolVO pool = (PoolVO) obj;

        try {
            if (pool.getId() > 0) {
                return _poolDao.delete(pool.getId());
            }
        } finally {
            fireDeleteEvent(new ObjectPropertyChangeEvent(pool));
        }

        return true;
    }

    @Override
    public Long countByVmImage(String vmImageUuid) {
        return _poolDao.countByVmImage(vmImageUuid);
    }

    @Override
    public Long countByStatus(Pool.Status... status) {
        return _poolDao.countByStatus(status);
    }

    @Override
    public Pool getPoolByName(String poolName) {
        return _poolDao.findByName(poolName);
    }

    @Override
    public DiskImage getBaseDiskImage(Pool pool) {
        String diskImageGuid = pool.getBaseDiskImageGuid();
        if (StringUtils.isEmpty(diskImageGuid)) {
            List<? extends DiskImage> localDiskImages = _diskImageService.getLocalDiskImagesByOwner(pool.getUuid());
            if (localDiskImages != null && !localDiskImages.isEmpty()) {
                return localDiskImages.get(0);
            }
            return null;
        }

        return _diskImageService.getLocalDiskImageByGuid(diskImageGuid);
    }

    @Override
    public List<? extends Pool> getByStatus(Pool.Status... status) {
        return _poolDao.findByStatus(status);
    }

    @Override
    public List<? extends Pool> getByVmImage(String vmImageUuid) {
        return _poolDao.findByVmImage(vmImageUuid);
    }

    @Override
    public List<? extends Pool> getAllPoolsForUser(User user) {
        List<? extends PoolUser> poolUserList = _poolUserService.getByUser(user.getUuid());

        ArrayList<PoolVO> poolList = new ArrayList<PoolVO>();

        for (PoolUser poolUser : poolUserList) {
            PoolVO pool = _poolDao.findByUuid(poolUser.getPoolUuid());
            if (pool != null) {
                poolList.add(pool);
            }
        }

        return poolList;
    }

    @Override
    public void instantiatePool(Pool pool, AsyncCallback callback) {
        /*
        if (StringUtils.isEmpty(pool.getBaseDiskImageGuid())) {
            // Try to reconcile the local disk image.
            List<? extends DiskImage> localDiskImages = _diskImageService.getLocalDiskImagesByOwner(pool.getUuid());
            if (localDiskImages != null) {
                DiskImage foundDiskImage = null;
                for (DiskImage localDiskImage : localDiskImages) {
                    if (localDiskImage.getType() == null
                            || localDiskImage.getType().isImageBase()) {
                        continue;
                    }
                    if (!StringUtils.equals(localDiskImage.getOwnerUuid(), pool.getUuid())) {
                        continue;
                    }

                    // Now compare to foundDiskImage if it exists.
                    if (foundDiskImage != null) {
                        if (localDiskImage.getVersion() > foundDiskImage.getVersion()) {
                            foundDiskImage = localDiskImage;
                        }
                    } else {
                        foundDiskImage = localDiskImage;
                    }
                }

                if (foundDiskImage != null) {
                    LOGGER.info(String.format("Found base disk image \"%s\"(%s) for pool \"%s\".",
                            foundDiskImage.getName(), foundDiskImage.getUuid(), pool.getName()));
                    pool.setBaseDiskImageGuid(foundDiskImage.getGuid());
                    commit(pool);
                }
            }
        }
        */

        if (!StringUtils.isEmpty(pool.getBaseDiskImageGuid())) {
            DiskImage localDiskImage = _diskImageService.getLocalDiskImageByGuid(pool.getBaseDiskImageGuid());
            if (localDiskImage != null) {
                LOGGER.info(String.format("Pool \"%s\" has already started to instantiate/replicate on this host", pool.getName()));
                if (callback != null) {
                    callback.onSuccess(null);
                }
                return;
            }
        }

        if (/*Pool.Type.Persistent == pool.getType()
                && */!_hostService.isLocalHost(pool.getInstantiateHostUuid())) {
            LOGGER.info(String.format("Pool \"%s\" is persistent and not set to instantiate on this host", pool.getName()));
            if (callback != null) {
                callback.onSuccess(null);
            }
            return;
        }

        InstantiatePoolEvent event = new InstantiatePoolEvent(pool, callback);
        _eventBusService.post(event);
    }

    @Override
    public void deletePool(Pool pool, AsyncCallback callback) {
        // Can't delete it here. It's too early as ClusterReplicator will copy the objects
        // from other node. Do it in PoolManager.PoolChangeListenerImpl.deleteObject().
        //_poolUserService.deleteByPool(pool.getUuid());

        if (0 == _vmInstanceService.countByPool(pool.getUuid())) {
            delete(pool);

            if (callback != null) {
                callback.onSuccess(null);
            }

            return;
        }

        DeletePoolEvent event = new DeletePoolEvent(pool, callback);
        _eventBusService.post(event);
    }

    @Override
    public void provisionDesktops(Pool pool, boolean rebalance, AsyncCallback callback) {
        ProvisionDesktopsEvent event = new ProvisionDesktopsEvent(pool, rebalance, callback);
        _eventBusService.post(event);
    }

    @Override
    public void provisionLocalDesktop(Pool pool, VmInstance vmInstance, AsyncCallback callback) {
        Host localHost = _hostService.getLocalHost();

        ProvisionLocalDesktopEvent event = new ProvisionLocalDesktopEvent(localHost, pool, vmInstance, callback);
        _eventBusService.post(event);
    }

    @Override
    public void recomposeDesktops(Pool pool, boolean force, AsyncCallback callback) {
        RecomposeDesktopsEvent event = new RecomposeDesktopsEvent(pool, force, callback);
        _eventBusService.post(event);
    }

    @Override
    public void rebalanceDesktops(Pool pool, AsyncCallback callback) {
        RebalanceDesktopsEvent event = new RebalanceDesktopsEvent(pool, callback);
        _eventBusService.post(event);
    }
}

