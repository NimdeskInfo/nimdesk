/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.service.impl.workflow;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.nimdesk.event.AllocateStorageEvent;
import com.nimdesk.event.AsyncCallback;
import com.nimdesk.event.ConnectHostEvent;
import com.nimdesk.event.InstantiateVmEvent;
import com.nimdesk.event.LinkedCloneImageEvent;
import com.nimdesk.hypervisor.HypervisorContext;
import com.nimdesk.model.DiskImage;
import com.nimdesk.model.EventLog;
import com.nimdesk.model.Host;
import com.nimdesk.model.Server;
import com.nimdesk.model.Storage;
import com.nimdesk.model.Task;
import com.nimdesk.model.VmImage;
import com.nimdesk.service.EventBusService;
import com.nimdesk.service.Workflow;
import com.nimdesk.service.impl.ServiceLocator;
import com.nimdesk.util.IdGenerator;
import com.nimdesk.util.Pair;
import com.nimdesk.util.Utils;
import com.nimdesk.vm.InstantiatedVm;

/**
 * Step 1: Connect to host
 * Step 2: Linked clone disk image of VmImage
 * Step 3: Register and power on VM
 */
public class CloneImageForUpdateFlow extends Workflow {
    private static final Logger LOGGER = Logger.getLogger(CloneImageForUpdateFlow.class);

    private final long _createTime = System.currentTimeMillis();

    private final EventBusService _eventBusService;
    private final Host.Service _hostService;
    private final Storage.Service _storageService;
    private final VmImage.Service _vmImageService;
    private final DiskImage.Service _diskImageService;
    private final Task.Service _taskService;
    private final EventLog.Service _eventLogService;

    private final Server _localServer;
    private final Host _localHost;
    private final Task _task;

    private final VmImage _vmImage;
    private final DiskImage _origDiskImage;
    private final DiskImage _diskImage;

    // ConnectHostState result
    private HypervisorContext _hypervisorContext;

    // ConnectVcState result
    private HypervisorContext _vcHypervisorContext = null;

    // AllocateStorageState result
    private Storage _storage;

    // LinkedCloneDiskImageState result
    private String _imageVmxPath;

    // InstantiateVmState result
    private String _vmRefId;

    public CloneImageForUpdateFlow(Host localHost, VmImage vmImage, final AsyncCallback callback) {
        super();

        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("CloneImageForUpdateFlow()");
        }

        ServiceLocator serviceLocator = ServiceLocator.getInstance();

        _eventBusService = serviceLocator.getEventBusService();
        _hostService = serviceLocator.getHostService();
        _storageService = serviceLocator.getStorageService();
        _vmImageService = serviceLocator.getVmImageService();
        _diskImageService = serviceLocator.getDiskImageService();
        _taskService = serviceLocator.getTaskService();
        _eventLogService = serviceLocator.getEventLogService();

        Server.Service serverService = serviceLocator.getServerService();
        _localServer = serverService.getLocalServer();
        if (_localServer == null) {
            String error = String.format("Unable to find host to create clone for image \"%s\"", vmImage.getName());
            LOGGER.warn(error);
            throw new RuntimeException(error);
        }

        if (localHost != null) {
            _localHost = localHost;
        } else {
            _localHost = _hostService.getLocalHost();
        }
        if (_localHost == null) {
            String error = String.format("Unable to find host to create clone for image \"%s\"", vmImage.getName());
            LOGGER.warn(error);
            throw new RuntimeException(error);
        }

        _vmImage = vmImage;

        _origDiskImage = _vmImageService.getCurrentDiskImage(_vmImage, _localHost.getUuid());
        if (_origDiskImage == null) {
            String error = String.format("VM image (uuid=%s) doesn't have a valid disk image (uuid=%s)",
                    _vmImage.getUuid(), _vmImage.getCurrentDiskImageGuid());
            LOGGER.warn(error);
            throw new RuntimeException(error);
        }

        _diskImage = createUpdateDiskImage(_vmImage, _origDiskImage);

        _task = createTask(_diskImage, _vmImage, (callback != null) ? callback.getRequestor() : null);

        append(new SanityCheckState(), null);
        append(new ConnectHostState(), null);
        if (!StringUtils.isEmpty(_localHost.getVCenterIp())
                && !StringUtils.isEmpty(_localHost.getVCenterUsername())
                && !StringUtils.isEmpty(_localHost.getVCenterPassword())) {
            append(new ConnectVCenterState(), null);
        }
        append(new AllocateStorageState(), null);
        append(new LinkedCloneDiskImageState(), null);
        append(new InstantiateVmState(), null);

        setCallback(new AsyncCallback() {
            @Override
            @SuppressWarnings("unchecked")
            public void onSuccess(Object result) {
                disconnectHost();

                DiskImage diskImage = _diskImageService.getByUuid(_diskImage.getUuid());
                if (diskImage == null
                        || DiskImage.Status.Deleting == diskImage.getStatus()) {
                    LOGGER.error(String.format("Unable to find DiskImage id=\"%s\" in db", _diskImage.getUuid()));
                    throw new RuntimeException("Can't locate the entity");
                }

                diskImage.setVmRefId(_vmRefId);
                diskImage.setIpAddress(null);
                _diskImageService.commit(diskImage);

                _task.setEndTime(System.currentTimeMillis());
                _task.setStatus(Task.Status.Completed);
                _task.setDetails("Succeeded");
                _taskService.commit(_task);

                _eventLogService.addVmImageEventLog(EventLog.Severity.Info,
                        _vmImage, "info.image.preupdate.succeeded", getRequestor(), _task,
                        new Pair<String, String>("user", getRequestor()),
                        new Pair<String, String>("image", _vmImage.getName()));

                LOGGER.info(String.format("CloneImageForUpdateFlow: Cloned disk image \"%s\" for image \"%s\"",
                        _diskImage.getName(), _vmImage.getName()));

                if (callback != null) {
                    callback.onSuccess(result);
                }
            }

            @Override
            @SuppressWarnings("unchecked")
            public void onFailure(Throwable t) {
                disconnectHost();

                DiskImage diskImage = _diskImageService.getByUuid(_diskImage.getUuid());
                if (diskImage == null
                        || DiskImage.Status.Deleting == diskImage.getStatus()) {
                    LOGGER.warn(String.format("Unable to find DiskImage id=\"%s\" in db", _diskImage.getUuid()));
                } else {
                    _diskImageService.deleteDiskImage(diskImage, null);
                }

                VmImage vmImage = _vmImageService.getByUuid(_vmImage.getUuid());
                if (vmImage == null
                        || VmImage.Status.Deleting == vmImage.getStatus()) {
                    LOGGER.error(String.format("Unable to find VmImage id=\"%s\" in db", _vmImage.getUuid()));
                    throw new RuntimeException("Can't locate the entity");
                }

                vmImage.setUpdatingDiskImageUuid(null);
                vmImage.setUpdatingDiskImageIp(null);
                _vmImageService.commit(vmImage);

                _task.setEndTime(System.currentTimeMillis());
                _task.setStatus(Task.Status.Failed);
                _task.setDetails(t.getMessage());
                _taskService.commit(_task);

                _eventLogService.addVmImageEventLog(EventLog.Severity.Error,
                        _vmImage, "info.image.preupdate.failed", getRequestor(), _task,
                        new Pair<String, String>("user", getRequestor()),
                        new Pair<String, String>("image", _vmImage.getName()));

                LOGGER.error(String.format("CloneImageForUpdateFlow: Failed to clone disk image \"%s\" for image \"%s\" - %s",
                        _diskImage.getName(), _vmImage.getName(), t.getMessage()), t);

                if (callback != null) {
                    callback.onFailure(t);
                }
            }

            @Override
            public void progress(int percentage) {
                if (callback != null) {
                    callback.progress(percentage);
                }
            }

            @Override
            public String getRequestor() {
                return (callback != null) ? callback.getRequestor() : null;
            }
        });
    }

    public Task getTask() {
        return _task;
    }

    private void disconnectHost() {
        if (_hypervisorContext != null) {
            _hypervisorContext.disconnect();
            _hypervisorContext = null;
        }
        if (_vcHypervisorContext != null) {
            _vcHypervisorContext.disconnect();
            _vcHypervisorContext = null;
        }
    }

    @SuppressWarnings("unchecked")
    private Task createTask(DiskImage diskImage, VmImage vmImage, String requestor) {
        Task task = _taskService.newInstance();

        task.setType(Task.Type.CloneImageForUpdate);
        task.setStartTime(_createTime);
        task.setServerUuid(_localServer.getUuid());

        task.setTargetType(VmImage.class.getSimpleName());
        task.setTargetUuid(vmImage.getUuid());
        task.setTargetName(vmImage.getName());

        task.setStatus(Task.Status.InProgress);
        task.setDetails("Starting");

        _taskService.commit(task);

        _eventLogService.addVmImageEventLog(EventLog.Severity.Info,
                vmImage, "info.image.preupdate.start", requestor, task,
                new Pair<String, String>("user", requestor),
                new Pair<String, String>("image", vmImage.getName()));

        return task;
    }

    private DiskImage createUpdateDiskImage(VmImage vmImage, DiskImage origDiskImage) {
        DiskImage diskImage = _diskImageService.newInstance();
        diskImage.setType(DiskImage.Type.ImageDelta);
        diskImage.setGuid(IdGenerator.generateUuid());
        diskImage.setHostUuid(_localHost.getUuid());
        diskImage.setStorageUuid("");
        diskImage.setCreateTime(_createTime);
        diskImage.setName(vmImage.getName() + "-M" + Utils.convertVersion(System.currentTimeMillis()));
        diskImage.setOwnerUuid(vmImage.getUuid());
        diskImage.setVersion(origDiskImage.getVersion() + 1L);
        diskImage.setParentGuid(origDiskImage.getGuid());
        diskImage.setDiskSize(origDiskImage.getDiskSize());
        diskImage.setStatus(DiskImage.Status.Updating);
        _diskImageService.commit(diskImage);

        vmImage.setStatus(VmImage.Status.Updating);
        vmImage.setUpdatingDiskImageUuid(diskImage.getUuid());
        vmImage.setUpdateTime(_createTime);
        _vmImageService.commit(vmImage);

        return diskImage;
    }

    private class SanityCheckState implements Workflow.StateRunnable {
        @Override
        public void run(AsyncCallback callback) {
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace("SanityCheckState.run()");
            }

            try {
                if (DiskImage.Status.Error == _origDiskImage.getStatus()) {
                    LOGGER.error(String.format("VmImage (%s) base disk image (uuid=%s) is in error state",
                            _vmImage.getName(), _vmImage.getCurrentDiskImageGuid()));
                    throw new Exception("The image on this server is broken");
                }

                if (callback != null) {
                    callback.onSuccess(null);
                }
            } catch (Exception e) {
                if (callback != null) {
                    callback.onFailure(e);
                }
            }
        }

        @Override
        public void setResult(Object result) {
        }
    }

    private class ConnectHostState implements Workflow.StateRunnable {
        @Override
        public void run(AsyncCallback callback) {
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace("ConnectHostState.run()");
            }

            _task.setDetails("Preparing");
            _taskService.commit(_task);

            ConnectHostEvent event = new ConnectHostEvent(_localHost.getAddress(), 0,
                    _localHost.getUsername(), _localHost.getPassword(), callback);
            _eventBusService.post(event);
        }

        @Override
        public void setResult(Object result) {
            _hypervisorContext = (HypervisorContext) result;
        }
    }

    private class ConnectVCenterState implements Workflow.StateRunnable {
        @Override
        public void run(AsyncCallback callback) {
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace("ConnectVCenterState.run()");
            }

            _task.setDetails("Preparing");
            _taskService.commit(_task);

            String vCenterAddress = _localHost.getVCenterAddress();
            if (StringUtils.isEmpty(vCenterAddress)) {
                vCenterAddress = _localHost.getVCenterIp();
            }

            ConnectHostEvent event = new ConnectHostEvent(vCenterAddress, 0,
                    _localHost.getVCenterUsername(), _localHost.getVCenterPassword(), callback);
            _eventBusService.post(event);
        }

        @Override
        public void setResult(Object result) {
            _vcHypervisorContext = (HypervisorContext) result;
        }
    }

    private class AllocateStorageState implements Workflow.StateRunnable {
        @Override
        public void run(AsyncCallback callback) {
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace("AllocateStorageState.run()");
            }

            _task.setDetails(String.format("Allocate storage for updating image \"%s\"", _vmImage.getName()));
            _taskService.commit(_task);

            AllocateStorageEvent event = new AllocateStorageEvent(_hypervisorContext,
                    _storageService.getHostStorages(_localHost.getUuid()),
                    ProvisionDesktopFlow.class.getName(), callback);
            _eventBusService.post(event);
        }

        @Override
        public void setResult(Object result) {
            _storage = (Storage) result;
        }
    }

    private class LinkedCloneDiskImageState implements Workflow.StateRunnable {
        @Override
        public void run(AsyncCallback callback) {
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace("LinkedCloneDiskImageState.run()");
            }

            DiskImage diskImage = _diskImageService.getByUuid(_diskImage.getUuid());
            if (diskImage == null
                    || DiskImage.Status.Deleting == diskImage.getStatus()) {
                LOGGER.error(String.format("Unable to find DiskImage id=\"%s\" in db", _diskImage.getUuid()));
                throw new RuntimeException("Can't locate the entity");
            }

            diskImage.setStorageUuid(_storage.getUuid());
            _diskImageService.commit(diskImage);

            _task.setDetails(String.format("Cloning image \"%s\"", _diskImage.getName()));
            _taskService.commit(_task);

            Storage srcStorage = _storageService.getByUuid(_origDiskImage.getStorageUuid());

            Map<String, String> customProps = new HashMap<String, String>();
            customProps.put("guestinfo.nimdesk.type", "image");
            customProps.put("guestinfo.nimdesk.iid", _diskImage.getUuid());
            customProps.put("guestinfo.nimdesk.serverUrl",
                    String.format("http://%s:8080/", _localServer.getPrivateAddress()));

            LinkedCloneImageEvent event = new LinkedCloneImageEvent(_hypervisorContext, _origDiskImage,
                    srcStorage, null, _diskImage.getName(), _storage, _localHost.getVmNetwork(), customProps, callback);
            _eventBusService.post(event);
        }

        @Override
        public void setResult(Object result) {
            _imageVmxPath = (String) result;
        }
    }

    private class InstantiateVmState implements Workflow.StateRunnable {
        @Override
        public void run(AsyncCallback callback) {
            LOGGER.info(String.format("InstantiateVmState.run() - vmx: %s", _imageVmxPath));

            _task.setDetails(String.format("Instantiating image \"%s\"", _diskImage.getName()));
            _taskService.commit(_task);

            InstantiateVmEvent event = new InstantiateVmEvent(_hypervisorContext, _localHost.getRefId(),
                    _diskImage.getName(), _imageVmxPath, true, 0, 0, false, null, false,
                    _vcHypervisorContext, _localHost.getEsxUuid(), callback);
            _eventBusService.post(event);
        }

        @Override
        public void setResult(Object result) {
            InstantiatedVm instantiatedVm = (InstantiatedVm) result;
            _vmRefId = instantiatedVm.getRefId();
        }
    }
}

