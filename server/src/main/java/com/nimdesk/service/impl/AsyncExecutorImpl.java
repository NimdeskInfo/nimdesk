/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.service.impl;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.nimdesk.event.AsyncCallback;
import com.nimdesk.service.AsyncExecutor;

/**
 * A generic service to provide run-late service to all requests.
 * This can help improve system responsiveness by not blocking key threads.
 */
@Service(AsyncExecutor.BEAN_NAME)
public class AsyncExecutorImpl implements AsyncExecutor {
    private static final Logger LOGGER = Logger.getLogger(AsyncExecutorImpl.class);

    private static final int CORE_POOL_SIZE = 10;
    private static final int MAX_POOL_SIZE = CORE_POOL_SIZE + 1; //200;

    private final ExecutorService _executor = new ThreadPoolExecutor(CORE_POOL_SIZE,
            MAX_POOL_SIZE, 300L, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>());

    @Override
    public void run(final AsyncExecutor.Job job, final AsyncCallback callback) {
        _executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    Object result = job.run();

                    if (callback != null) {
                        callback.onSuccess(result);
                    }
                } catch (Exception e) {
                    LOGGER.error("Failed to run", e);

                    if (callback != null) {
                        callback.onFailure(e);
                    }
                }
            }
        });
    }
}

