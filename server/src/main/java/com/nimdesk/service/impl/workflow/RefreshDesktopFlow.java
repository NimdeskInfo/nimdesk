/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.service.impl.workflow;

import org.apache.log4j.Logger;

import com.nimdesk.database.vo.VmInstanceVO;
import com.nimdesk.event.AsyncCallback;
import com.nimdesk.event.ConnectHostEvent;
import com.nimdesk.event.RefreshVmEvent;
import com.nimdesk.hypervisor.HypervisorContext;
import com.nimdesk.model.Host;
import com.nimdesk.model.Pool;
import com.nimdesk.model.Server;
import com.nimdesk.model.Task;
import com.nimdesk.model.VmInstance;
import com.nimdesk.service.EventBusService;
import com.nimdesk.service.Workflow;
import com.nimdesk.service.impl.ServiceLocator;

public class RefreshDesktopFlow extends Workflow {
    private static final Logger LOGGER = Logger.getLogger(RefreshDesktopFlow.class);

    private final EventBusService _eventBusService;
    private final Host.Service _hostService;
    private final VmInstance.Service _vmInstanceService;

    private final Server _localServer;
    private final Host _host;
    private final Pool _pool;

    private final String _vmInstanceUuid;
    private final String _vmInstanceName;
    private final String _vmInstanceRefId;

    // ConnectHostState result
    private HypervisorContext _hypervisorContext;

    public RefreshDesktopFlow(Host host, Pool pool, VmInstance vmInstance, final AsyncCallback callback) {
        super();

        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("RefreshDesktopFlow()");
        }

        ServiceLocator serviceLocator = ServiceLocator.getInstance();

        _eventBusService = serviceLocator.getEventBusService();
        _hostService = serviceLocator.getHostService();
        _vmInstanceService = serviceLocator.getVmInstanceService();

        Server.Service serverService = serviceLocator.getServerService();
        _localServer = serverService.getLocalServer();
        if (_localServer == null) {
            throw new RuntimeException(String.format("Unable to find host to refresh desktop for pool \"%s\"", pool.getName()));
        }

        if (host != null) {
            _host = host;
        } else {
            _host = _hostService.getLocalHost();
        }
        if (_host == null) {
            throw new RuntimeException(String.format("Unable to find host to refresh desktop for pool \"%s\"", pool.getName()));
        }

        _pool = pool;

        _vmInstanceUuid = vmInstance.getUuid();
        _vmInstanceName = vmInstance.getName();
        _vmInstanceRefId = vmInstance.getVmRefId();
        /*_task = */createTask(vmInstance, _pool, (callback != null) ? callback.getRequestor() : null);

        append(new ConnectHostState(), null);
        append(new RefreshVmState(), null);

        setCallback(new AsyncCallback() {
            @Override
            public void onSuccess(Object result) {
                disconnectHost();

                VmInstance vmInstance = _vmInstanceService.getByUuid(_vmInstanceUuid);
                if (vmInstance == null
                        || VmInstance.Status.Destroying == vmInstance.getStatus()
                        || VmInstance.Status.Destroyed == vmInstance.getStatus()) {
                    LOGGER.error(String.format("Unable to find VmInstance id=\"%s\" in db", _vmInstanceUuid));
                    throw new RuntimeException("Can't locate the entity");
                }

                vmInstance.setLastError(null);
                vmInstance.setStatus(VmInstance.Status.Ready);
                vmInstance.setUpdateTime(System.currentTimeMillis());
                _vmInstanceService.commit(vmInstance);

                LOGGER.info(String.format("RefreshDesktopFlow: Refreshed desktop \"%s\" from pool \"%s\"",
                        _vmInstanceName, _pool.getName()));

                if (callback != null) {
                    callback.onSuccess(result);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                disconnectHost();

                VmInstance vmInstance = _vmInstanceService.getByUuid(_vmInstanceUuid);
                if (vmInstance == null
                        || VmInstance.Status.Destroying == vmInstance.getStatus()
                        || VmInstance.Status.Destroyed == vmInstance.getStatus()) {
                    LOGGER.error(String.format("Unable to find VmInstance id=\"%s\" in db", _vmInstanceUuid));
                } else {
                    vmInstance.setStatus(VmInstance.Status.Error);
                    vmInstance.setLastError(VmInstance.Error.RefreshError);
                    vmInstance.setUpdateTime(System.currentTimeMillis());
                    _vmInstanceService.commit(vmInstance);
                }

                LOGGER.error(String.format("RefreshDesktopFlow: Failed to refresh desktop \"%s\" from pool \"%s\" - %s",
                        _vmInstanceName, _pool.getName(), t.getMessage()), t);

                if (callback != null) {
                    callback.onFailure(t);
                }
            }

            @Override
            public void progress(int percentage) {
                if (callback != null) {
                    callback.progress(percentage);
                }
            }

            @Override
            public String getRequestor() {
                return (callback != null) ? callback.getRequestor() : null;
            }
        });
    }

    private void disconnectHost() {
        if (_hypervisorContext != null) {
            _hypervisorContext.disconnect();
            _hypervisorContext = null;
        }
    }

    private Task createTask(VmInstance vmInstance, Pool pool, String requestor) {
        if (vmInstance instanceof VmInstanceVO) {
            // Suppressing PoolManager to start another vmInstance refreshing.
            ((VmInstanceVO) vmInstance).inOperation(true);
        }
        if (VmInstance.Status.Refreshing != vmInstance.getStatus()) {
            vmInstance.setStatus(VmInstance.Status.Refreshing);
        }
        vmInstance.setLoginTime(0);
        vmInstance.setLogOffMode(null);
        vmInstance.setAssigned(false);
        vmInstance.setUpdateTime(System.currentTimeMillis());
        _vmInstanceService.commit(vmInstance);

        return null;
    }

    private class ConnectHostState implements Workflow.StateRunnable {
        @Override
        public void run(AsyncCallback callback) {
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace("ConnectHostState.run()");
            }

            ConnectHostEvent event = new ConnectHostEvent(_host.getAddress(), 0,
                    _host.getUsername(), _host.getPassword(), callback);
            _eventBusService.post(event);
        }

        @Override
        public void setResult(Object result) {
            _hypervisorContext = (HypervisorContext) result;
        }
    }

    private class RefreshVmState implements Workflow.StateRunnable {
        @Override
        public void run(AsyncCallback callback) {
            LOGGER.info(String.format("RefreshVmState.run() - vmx: %s", _vmInstanceName));

            RefreshVmEvent event = new RefreshVmEvent(_hypervisorContext,
                    _vmInstanceRefId, callback);
            _eventBusService.post(event);
        }

        @Override
        public void setResult(Object result) {
        }
    }
}
