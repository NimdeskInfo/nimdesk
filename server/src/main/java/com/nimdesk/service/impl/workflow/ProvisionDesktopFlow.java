/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.service.impl.workflow;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.nimdesk.event.AllocateStorageEvent;
import com.nimdesk.event.AsyncCallback;
import com.nimdesk.event.ConnectHostEvent;
import com.nimdesk.event.InstantiateVmEvent;
import com.nimdesk.event.LinkedCloneImageEvent;
import com.nimdesk.exception.storage.BrokenImageException;
import com.nimdesk.hypervisor.HypervisorContext;
import com.nimdesk.model.DiskImage;
import com.nimdesk.model.EventLog;
import com.nimdesk.model.Host;
import com.nimdesk.model.Pool;
import com.nimdesk.model.Server;
import com.nimdesk.model.Storage;
import com.nimdesk.model.Task;
import com.nimdesk.model.VmInstance;
import com.nimdesk.service.EventBusService;
import com.nimdesk.service.ObjectPropertyChangeEvent;
import com.nimdesk.service.ObjectPropertyChangeListener;
import com.nimdesk.service.Scheduler;
import com.nimdesk.service.Workflow;
import com.nimdesk.service.impl.ServiceLocator;
import com.nimdesk.util.Pair;
import com.nimdesk.vm.InstantiatedVm;

/**
 * Step 1: Connect to host
 * Step 2: Linked clone base disk image of pool
 * Step 3: Register and power on VM
 */
public class ProvisionDesktopFlow extends Workflow {
    private static final Logger LOGGER = Logger.getLogger(ProvisionDesktopFlow.class);

    // Strong references to keep weak referenced StateRunnable (such as SanityCheckState) in memory.
    private static final List<SanityCheckState> _pendingProvisions = new LinkedList<SanityCheckState>();
    private static ObjectPropertyChangeListener _vmInstanceChangeListener = null;
    private static final Map<String, Long> _provisioningDesktopUuids = new HashMap<String, Long>();
    private static Scheduler.ScheduleHandle _checkVmSchedule = null;

    private static final long VM_PROVISION_EXPIRE_PERIOD = 35 * 60 * 1000; // 35 min

    private static final int PREPARING_DESKTOP_CONCURRENCY = 8;

    private static final Object SERIALIZATION_LOCK = new Object();

    private static VmInstance.Status[] DESKTOP_PREPARING_STATUSES =
            new VmInstance.Status[] {
        /*VmInstance.Status.Provisioning,*/ VmInstance.Status.Creating, VmInstance.Status.ObtainingIp,
        VmInstance.Status.TakingSnapshot
    };

    private final long _createTime = System.currentTimeMillis();

    private final Scheduler _scheduler;
    private final EventBusService _eventBusService;
    private final Host.Service _hostService;
    private final Storage.Service _storageService;
    private final DiskImage.Service _diskImageService;
    private final VmInstance.Service _vmInstanceService;
    private final Task.Service _taskService;
    private final EventLog.Service _eventLogService;

    private final Server _localServer;
    private final Host _localHost;
    private final Pool _pool;
    private final DiskImage _baseDiskImage;

    private final String _vmInstanceUuid;
    private final String _vmInstanceName;
    private final Task _task;

    // ConnectHostState result
    private HypervisorContext _hypervisorContext;

    // ConnectVcState result
    private HypervisorContext _vcHypervisorContext = null;

    // AllocateStorageState result
    private Storage _storage = null;

    // LinkedCloneDiskImageState result
    private String _vmxPath;

    // InstantiateVmState result
    private String _vmRefId;
    private boolean _vmCbrcEnabled = false;

    public ProvisionDesktopFlow(Host localHost, Pool pool, DiskImage baseDiskImage, VmInstance vmInstance, final AsyncCallback callback) {
        super();

        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("ProvisionDesktopFlow()");
        }

        ServiceLocator serviceLocator = ServiceLocator.getInstance();

        _scheduler = serviceLocator.getScheduler();
        _eventBusService = serviceLocator.getEventBusService();
        _hostService = serviceLocator.getHostService();
        _storageService = serviceLocator.getStorageService();
        _diskImageService = serviceLocator.getDiskImageService();
        _vmInstanceService = serviceLocator.getVmInstanceService();
        _taskService = serviceLocator.getTaskService();
        _eventLogService = serviceLocator.getEventLogService();

        Server.Service serverService = serviceLocator.getServerService();
        _localServer = serverService.getLocalServer();
        if (_localServer == null) {
            throw new RuntimeException(String.format("Unable to find host to create desktop for pool \"%s\"", pool.getName()));
        }

        if (localHost != null) {
            _localHost = localHost;
        } else {
            _localHost = _hostService.getLocalHost();
        }
        if (_localHost == null) {
            throw new RuntimeException(String.format("Unable to find local host to create desktop for pool \"%s\"", pool.getName()));
        }

        _pool = pool;
        _baseDiskImage = baseDiskImage;

        _vmInstanceUuid = vmInstance.getUuid();
        _vmInstanceName = vmInstance.getName();
        _task = createTask(vmInstance, _pool, (callback != null) ? callback.getRequestor() : null);

        append(new SanityCheckState(), null);
        append(new ConnectHostState(), null);
        if (!StringUtils.isEmpty(_localHost.getVCenterIp())
                && !StringUtils.isEmpty(_localHost.getVCenterUsername())
                && !StringUtils.isEmpty(_localHost.getVCenterPassword())) {
            append(new ConnectVCenterState(), null);
        }
        if (!StringUtils.isEmpty(vmInstance.getStorageUuid())) {
            _storage = _storageService.getByUuid(vmInstance.getStorageUuid());
        }
        if (_storage == null) {
            append(new AllocateStorageState(), null);
        }
        append(new LinkedCloneDiskImageState(), null);
        append(new InstantiateVmState(), null);

        setCallback(new AsyncCallback() {
            @Override
            @SuppressWarnings("unchecked")
            public void onSuccess(Object result) {
                disconnectHost();

                VmInstance vmInstance = _vmInstanceService.getByUuid(_vmInstanceUuid);
                if (vmInstance == null
                        || VmInstance.Status.Destroying == vmInstance.getStatus()
                        || VmInstance.Status.Destroyed == vmInstance.getStatus()) {
                    LOGGER.error(String.format("Unable to find VmInstance id=\"%s\" in db", _vmInstanceUuid));
                    throw new RuntimeException("Can't locate the entity");
                }

                vmInstance.setVmRefId(_vmRefId);
                vmInstance.setCbrcEnabled(_vmCbrcEnabled);
                vmInstance.setStatus(VmInstance.Status.ObtainingIp);
                if (VmInstance.Error.CreateError == vmInstance.getLastError()) {
                    vmInstance.setLastError(null);
                }
                vmInstance.setUpdateTime(System.currentTimeMillis());
                _vmInstanceService.commit(vmInstance);

                _task.setEndTime(System.currentTimeMillis());
                _task.setStatus(Task.Status.Completed);
                _task.setDetails("Succeeded");
                _taskService.commit(_task);

                _eventLogService.addVmInstanceEventLog(EventLog.Severity.Info,
                        vmInstance, "info.desktop.create.succeeded", getRequestor(), _task,
                        new Pair<String, String>("user", getRequestor()),
                        new Pair<String, String>("desktop", _vmInstanceName),
                        new Pair<String, String>("pool", _pool.getName()));

                LOGGER.info(String.format("ProvisionDesktopFlow: Provisioned desktop \"%s\" from pool \"%s\"",
                        _vmInstanceName, _pool.getName()));

                if (callback != null) {
                    callback.onSuccess(result);
                }
            }

            @Override
            @SuppressWarnings("unchecked")
            public void onFailure(Throwable t) {
                disconnectHost();

                if (t instanceof BrokenImageException) {
                    DiskImage baseDiskImage = _diskImageService.getByUuid(_baseDiskImage.getUuid());
                    if (baseDiskImage == null) {
                        LOGGER.warn(String.format("Unable to find DiskImage id=\"%s\" for pool \"%s\"",
                                _baseDiskImage.getUuid(), _pool.getName()));
                    } else {
                        baseDiskImage.setStatus(DiskImage.Status.Error);
                        _diskImageService.commit(baseDiskImage);
                    }
                }

                VmInstance vmInstance = _vmInstanceService.getByUuid(_vmInstanceUuid);
                if (vmInstance == null
                        || VmInstance.Status.Destroying == vmInstance.getStatus()
                        || VmInstance.Status.Destroyed == vmInstance.getStatus()) {
                    LOGGER.error(String.format("Unable to find VmInstance id=\"%s\" in db", _vmInstanceUuid));
                } else {
                    vmInstance.setStatus(VmInstance.Status.Error);
                    vmInstance.setLastError(VmInstance.Error.CreateError);
                    vmInstance.setUpdateTime(System.currentTimeMillis());
                    _vmInstanceService.commit(vmInstance);
                }

                _task.setEndTime(System.currentTimeMillis());
                _task.setStatus(Task.Status.Failed);
                _task.setDetails(t.getMessage());
                _taskService.commit(_task);

                _eventLogService.addVmInstanceEventLog(EventLog.Severity.Error,
                        vmInstance, "error.desktop.create.failed", getRequestor(), _task,
                        new Pair<String, String>("user", getRequestor()),
                        new Pair<String, String>("desktop", _vmInstanceName),
                        new Pair<String, String>("pool", _pool.getName()));

                LOGGER.error(String.format("ProvisionDesktopFlow: Failed to provision desktop \"%s\" from pool \"%s\" - %s",
                        _vmInstanceName, _pool.getName(), t.getMessage()), t);

                if (callback != null) {
                    callback.onFailure(t);
                }
            }

            @Override
            public void progress(int percentage) {
                if (callback != null) {
                    callback.progress(percentage);
                }
            }

            @Override
            public String getRequestor() {
                return (callback != null) ? callback.getRequestor() : null;
            }
        });
    }

    public Task getTask() {
        return _task;
    }

    private void disconnectHost() {
        if (_hypervisorContext != null) {
            _hypervisorContext.disconnect();
            _hypervisorContext = null;
        }
        if (_vcHypervisorContext != null) {
            _vcHypervisorContext.disconnect();
            _vcHypervisorContext = null;
        }
    }

    @SuppressWarnings("unchecked")
    private Task createTask(VmInstance vmInstance, Pool pool, String requestor) {
        vmInstance.setRecompose(false);
        vmInstance.setInDomain(false);
        vmInstance.setAssigned(false);
        vmInstance.setDiskImageUuid(_baseDiskImage.getUuid());
        vmInstance.setStorageUuid("");
        vmInstance.setVmRefId("");
        vmInstance.setStatus(VmInstance.Status.Provisioning);
        vmInstance.setUpdateTime(_createTime);
        _vmInstanceService.commit(vmInstance);

        Task task = _taskService.newInstance();

        task.setType(Task.Type.ProvisionDesktop);
        task.setStartTime(_createTime);
        task.setServerUuid(_localServer.getUuid());

        task.setTargetType(VmInstance.class.getSimpleName());
        task.setTargetUuid(vmInstance.getUuid());
        task.setTargetName(vmInstance.getName());
        task.setAssociateType(Pool.class.getSimpleName());
        task.setAssociateUuid(pool.getUuid());
        task.setAssociateName(pool.getName());

        task.setStatus(Task.Status.InProgress);
        task.setDetails("Starting");

        _taskService.commit(task);

        _eventLogService.addVmInstanceEventLog(EventLog.Severity.Info,
                vmInstance, "info.desktop.create.start", requestor, task,
                new Pair<String, String>("user", requestor),
                new Pair<String, String>("desktop", vmInstance.getName()),
                new Pair<String, String>("pool", pool.getName()));

        return task;
    }

    private class SanityCheckState implements Workflow.StateRunnable {
        private String _provisioningDesktopUuid = null;
        private AsyncCallback _callback;

        @Override
        public void run(AsyncCallback callback) {
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace("SanityCheckState.run()");
            }

            _provisioningDesktopUuid = _vmInstanceUuid;

            try {
                synchronized (SERIALIZATION_LOCK) {
                    long provisioningCount = _provisioningDesktopUuids.size();
                    if (provisioningCount >= PREPARING_DESKTOP_CONCURRENCY) {
                        LOGGER.info(String.format("Pool \"%s\" (%s) has %d desktops in preparing - more than limit %d",
                                _pool.getName(), _pool.getUuid(), provisioningCount, PREPARING_DESKTOP_CONCURRENCY));

                        _callback = callback;

                        _pendingProvisions.add(this);

                        if (_vmInstanceChangeListener == null) {
                            _vmInstanceChangeListener = new VmInstanceChangeListenerImpl();

                            // Start listening on VmInstance Status change
                            _vmInstanceService.addObjectPropertyChangeListener(_vmInstanceChangeListener);
                        }

                        return;
                    } else {
                        VmInstance vmInstance = _vmInstanceService.getByUuid(_provisioningDesktopUuid);
                        if (vmInstance == null
                                || VmInstance.Status.Destroying == vmInstance.getStatus()
                                || VmInstance.Status.Destroyed == vmInstance.getStatus()) {
                            LOGGER.error(String.format("Unable to find VmInstance id=\"%s\" in db", _provisioningDesktopUuid));
                            throw new RuntimeException("Can't locate the entity");
                        }

                        if (_vmInstanceChangeListener == null) {
                            _vmInstanceChangeListener = new VmInstanceChangeListenerImpl();

                            // Start listening on VmInstance Status change
                            _vmInstanceService.addObjectPropertyChangeListener(_vmInstanceChangeListener);
                        }

                        // Add uuid to _provisioningDesktopUuids to throttle desktop provisioning.
                        _provisioningDesktopUuids.put(_provisioningDesktopUuid, System.currentTimeMillis());

                        if (_checkVmSchedule == null) {
                            _checkVmSchedule = _scheduler.scheduleWithFixedDelay(new Runnable() {
                                @Override
                                public void run() {
                                    List<String> expiredVms = new ArrayList<String>();

                                    long currentTime = System.currentTimeMillis();
                                    synchronized (SERIALIZATION_LOCK) {
                                        for (Map.Entry<String, Long> entry : _provisioningDesktopUuids.entrySet()) {
                                            if (currentTime - entry.getValue() > VM_PROVISION_EXPIRE_PERIOD) {
                                                expiredVms.add(entry.getKey());
                                            }
                                        }
                                    }

                                    for (String vmInstanceUuid : expiredVms) {
                                        provisionNextLocalDesktop(vmInstanceUuid);
                                    }
                                }
                            }, 300L, 300L); // Interval at 5 min
                        }
                    }
                }

                if (callback != null) {
                    callback.onSuccess(null);
                }
            } catch (Exception e) {
                if (callback != null) {
                    callback.onFailure(e);
                }
            }
        }

        @Override
        public void setResult(Object result) {
        }
    }

    private class VmInstanceChangeListenerImpl implements ObjectPropertyChangeListener {
        @Override
        public void newObject(ObjectPropertyChangeEvent event) {
        }

        @Override
        public void deleteObject(ObjectPropertyChangeEvent event) {
            final VmInstance vmInstance = (VmInstance) event.getSource();
            if (!_vmInstanceService.isLocalVmInstance(vmInstance)) {
                // Not local vmInstance.
                return;
            }

            if (isVmInstanceInPreparing(vmInstance.getStatus())) {
                provisionNextLocalDesktop(vmInstance.getUuid());
            }
        }

        @Override
        public void propertyChange(ObjectPropertyChangeEvent event) {
            final VmInstance vmInstance = (VmInstance) event.getSource();
            if (!_vmInstanceService.isLocalVmInstance(vmInstance)) {
                // Not local vmInstance.
                return;
            }

            final Map<String, Pair<Object, Object>> changedProperties = event.getChangedProperties();
            if (changedProperties != null) {
                Pair<Object, Object> statuses = changedProperties.get(VmInstance.PROP_STATUS);
                if (statuses != null
                        && !isVmInstanceInPreparing((VmInstance.Status) statuses.getSecond())
                        && isVmInstanceInPreparing((VmInstance.Status) statuses.getFirst())) {
                    provisionNextLocalDesktop(vmInstance.getUuid());
                }
            }
        }

        private boolean isVmInstanceInPreparing(VmInstance.Status vmInstanceStatus) {
            for (VmInstance.Status status : DESKTOP_PREPARING_STATUSES) {
                if (vmInstanceStatus == status) {
                    return true;
                }
            }
            return false;
        }
    }

    void provisionNextLocalDesktop(String completedVmInstanceUuid) {
        SanityCheckState state = null;

        synchronized (SERIALIZATION_LOCK) {
            _provisioningDesktopUuids.remove(completedVmInstanceUuid);

            if (_provisioningDesktopUuids.size() >= PREPARING_DESKTOP_CONCURRENCY) {
                // Still more than PREPARING_DESKTOP_CONCURRENCY desktops provisioning.
                return;
            }

            if (!_pendingProvisions.isEmpty()) {
                state = _pendingProvisions.remove(0);
            }

            if (state != null && !StringUtils.isEmpty(state._provisioningDesktopUuid)) {
                _provisioningDesktopUuids.put(state._provisioningDesktopUuid, System.currentTimeMillis());
            }

            // If no more pending provisioning, remove listener.
            if (_pendingProvisions.isEmpty() && _provisioningDesktopUuids.isEmpty()) {
                // Stop listening on VmInstance Status change
                _vmInstanceService.removeObjectPropertyChangeListener(_vmInstanceChangeListener);
                _vmInstanceChangeListener = null;

                if (_checkVmSchedule != null) {
                    _checkVmSchedule.cancel();
                    _checkVmSchedule = null;
                }
            }
        }

        if (state != null) {
            if (state._callback != null) {
                VmInstance vmInstance = _vmInstanceService.getByUuid(state._provisioningDesktopUuid);
                if (vmInstance == null
                        || VmInstance.Status.Destroying == vmInstance.getStatus()
                        || VmInstance.Status.Destroyed == vmInstance.getStatus()) {
                    state._callback.onFailure(new Exception("Can't locate the entity"));
                } else {
                    state._callback.onSuccess(null);
                }
            }
        }
    }

    private class ConnectHostState implements Workflow.StateRunnable {
        @Override
        public void run(AsyncCallback callback) {
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace("ConnectHostState.run()");
            }

            _task.setDetails("Preparing");
            _taskService.commit(_task);

            ConnectHostEvent event = new ConnectHostEvent(_localHost.getAddress(), 0,
                    _localHost.getUsername(), _localHost.getPassword(), callback);
            _eventBusService.post(event);
        }

        @Override
        public void setResult(Object result) {
            _hypervisorContext = (HypervisorContext) result;
        }
    }

    private class ConnectVCenterState implements Workflow.StateRunnable {
        @Override
        public void run(AsyncCallback callback) {
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace("ConnectVCenterState.run()");
            }

            _task.setDetails("Preparing");
            _taskService.commit(_task);

            String vCenterAddress = _localHost.getVCenterAddress();
            if (StringUtils.isEmpty(vCenterAddress)) {
                vCenterAddress = _localHost.getVCenterIp();
            }

            ConnectHostEvent event = new ConnectHostEvent(vCenterAddress, 0,
                    _localHost.getVCenterUsername(), _localHost.getVCenterPassword(), callback);
            _eventBusService.post(event);
        }

        @Override
        public void setResult(Object result) {
            _vcHypervisorContext = (HypervisorContext) result;
        }
    }

    private class AllocateStorageState implements Workflow.StateRunnable {
        @Override
        public void run(AsyncCallback callback) {
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace("AllocateStorageState.run()");
            }

            _task.setDetails(String.format("Allocate storage to provision desktop for pool \"%s\"", _pool.getName()));
            _taskService.commit(_task);

            if (Pool.Type.Persistent == _pool.getType()
                    || _localHost.isSharedDatastoreForAllDesktops()) {
                Storage sharedStorage = _storageService.getSharedStorage();
                if (sharedStorage != null) {
                    LOGGER.info(String.format("Allocated shared storage for VmInstance \"%s\"", _vmInstanceName));

                    if (callback != null) {
                        callback.onSuccess(sharedStorage);
                    }
                    return;
                }
            }

            LOGGER.info(String.format("Allocating local storage for VmInstance \"%s\"...", _vmInstanceName));

            AllocateStorageEvent event = new AllocateStorageEvent(_hypervisorContext,
                    _storageService.getHostStorages(_localHost.getUuid()),
                    ProvisionDesktopFlow.class.getName(), callback);
            _eventBusService.post(event);
        }

        @Override
        public void setResult(Object result) {
            _storage = (Storage) result;
        }
    }

    private class LinkedCloneDiskImageState implements Workflow.StateRunnable {
        @Override
        public void run(AsyncCallback callback) {
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace("LinkedCloneDiskImageState.run()");
            }

            VmInstance vmInstance = _vmInstanceService.getByUuid(_vmInstanceUuid);
            if (vmInstance == null
                    || VmInstance.Status.Destroying == vmInstance.getStatus()
                    || VmInstance.Status.Destroyed == vmInstance.getStatus()) {
                LOGGER.error(String.format("Unable to find VmInstance id=\"%s\" in db", _vmInstanceUuid));
                throw new RuntimeException("Can't locate the entity");
            }

            vmInstance.setStorageUuid(_storage.getUuid());
            vmInstance.setStatus(VmInstance.Status.Creating);
            vmInstance.setUpdateTime(System.currentTimeMillis());
            _vmInstanceService.commit(vmInstance);

            _task.setDetails(String.format("Cloning image \"%s\"", _vmInstanceName));
            _taskService.commit(_task);

            Storage baseStorage = _storageService.getByUuid(_baseDiskImage.getStorageUuid());

            Map<String, String> customProps = new HashMap<String, String>();
            customProps.put("guestinfo.nimdesk.type", "desktop");
            customProps.put("guestinfo.nimdesk.iid", _vmInstanceUuid);
            customProps.put("guestinfo.nimdesk.serverUrl",
                    String.format("http://%s:8080/", _localServer.getPrivateAddress()));

            LinkedCloneImageEvent event = new LinkedCloneImageEvent(_hypervisorContext, _baseDiskImage,
                    baseStorage, _pool.getName(), _vmInstanceName, _storage, _localHost.getVmNetwork(), customProps, callback);
            _eventBusService.post(event);
        }

        @Override
        public void setResult(Object result) {
            _vmxPath = (String) result;
        }
    }

    private class InstantiateVmState implements Workflow.StateRunnable {
        @Override
        public void run(AsyncCallback callback) {
            LOGGER.info(String.format("InstantiateVmState.run() - vmx: %s", _vmxPath));

            VmInstance vmInstance = _vmInstanceService.getByUuid(_vmInstanceUuid);
            if (vmInstance == null
                    || VmInstance.Status.Destroying == vmInstance.getStatus()
                    || VmInstance.Status.Destroyed == vmInstance.getStatus()) {
                LOGGER.error(String.format("Unable to find VmInstance id=\"%s\" in db", _vmInstanceUuid));
                throw new RuntimeException("Can't locate the entity");
            }

            _task.setDetails(String.format("Instantiating image \"%s\"", _vmInstanceName));
            _taskService.commit(_task);

            InstantiateVmEvent event = new InstantiateVmEvent(_hypervisorContext, _localHost.getRefId(),
                    _vmInstanceName, _vmxPath, true, 0, 0, false, vmInstance.getMacAddress(), _localHost.isCbrcEnabled(),
                    _vcHypervisorContext, _localHost.getEsxUuid(), callback);
            _eventBusService.post(event);
        }

        @Override
        public void setResult(Object result) {
            InstantiatedVm instantiatedVm = (InstantiatedVm) result;

            _vmRefId = instantiatedVm.getRefId();
            _vmCbrcEnabled = instantiatedVm.isCbrcEnabled();
        }
    }
}

