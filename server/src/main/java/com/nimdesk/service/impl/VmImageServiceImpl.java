/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.nimdesk.database.dao.DiskImageDao;
import com.nimdesk.database.dao.VmImageDao;
import com.nimdesk.database.vo.VmImageVO;
import com.nimdesk.event.AsyncCallback;
import com.nimdesk.event.CloneImageForUpdateEvent;
import com.nimdesk.event.DeleteVmImageEvent;
import com.nimdesk.event.ImportImageFromVmEvent;
import com.nimdesk.event.ImportUpdatedImageEvent;
import com.nimdesk.model.DiskImage;
import com.nimdesk.model.VmImage;
import com.nimdesk.service.EventBusService;
import com.nimdesk.service.ObjectPropertyChangeEvent;
import com.nimdesk.util.IdGenerator;
import com.nimdesk.util.Pagination;
import com.nimdesk.util.Pair;

@Service(VmImage.Service.BEAN_NAME)
public class VmImageServiceImpl extends ObjectPropertyChangeSupport implements VmImage.Service, InitializingBean {
    private static final Logger LOGGER = Logger.getLogger(VmImageServiceImpl.class);

    @Autowired
    private VmImageDao _vmImageDao;

    @Autowired
    private DiskImageDao _diskImageDao;

    @Autowired
    private DiskImage.Service _diskImageService;

    @Autowired
    private EventBusService _eventBusService;

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(_vmImageDao, "A VmImageDao must be set");
        Assert.notNull(_diskImageDao, "A DiskImageDao must be set");
        Assert.notNull(_diskImageService, "A DiskImage.Service must be set");
        Assert.notNull(_eventBusService, "An EventBusService must be set");
    }

    @Override
    public Long count() {
        return _vmImageDao.countAll();
    }

    @Override
    public VmImage newInstance() {
        VmImage vmImage = new VmImageVO();

        // Generate uuid
        vmImage.setUuid(IdGenerator.generateUuid());

        return vmImage;
    }

    @Override
    public List<? extends VmImage> getAll(Pagination pagination) {
        return _vmImageDao.findAll(pagination);
    }

    @Override
    public VmImage getByUuid(String uuid) {
        return _vmImageDao.findByUuid(uuid);
    }

    @Override
    public void commit(VmImage obj) {
        if (!(obj instanceof VmImageVO)) {
            String error = String.format("commit: passing wrong type %s", obj.getClass().toString());
            LOGGER.error(error);
            throw new IllegalArgumentException(error);
        }

        VmImageVO vmImage = (VmImageVO) obj;

        if (vmImage.getId() == 0) {
            _vmImageDao.createNew(vmImage);
            vmImage.resetChangedProperties();
            fireCreateEvent(new ObjectPropertyChangeEvent(vmImage, null));
        } else {
            _vmImageDao.update(vmImage);
            Map<String, Pair<Object, Object>> changedProperties = vmImage.resetChangedProperties();
            fireChangeEvent(new ObjectPropertyChangeEvent(vmImage, changedProperties));
        }
    }

    @Override
    public boolean delete(VmImage obj) {
        if (!(obj instanceof VmImageVO)) {
            String error = String.format("delete: passing wrong type %s", obj.getClass().toString());
            LOGGER.error(error);
            throw new IllegalArgumentException(error);
        }

        VmImageVO vmImage = (VmImageVO) obj;

        try {
            if (vmImage.getId() > 0) {
                return _vmImageDao.delete(vmImage.getId());
            }
        } finally {
            fireDeleteEvent(new ObjectPropertyChangeEvent(vmImage));
        }

        return true;
    }

    @Override
    public Long countByStatus(VmImage.Status... status) {
        return _vmImageDao.countByStatus(status);
    }

    @Override
    public VmImage getVmImageByName(String vmImageName) {
        return _vmImageDao.findByName(vmImageName);
    }

    @Override
    public DiskImage getCurrentDiskImage(VmImage vmImage, String hostUuid) {
        String diskImageGuid = vmImage.getCurrentDiskImageGuid();
        if (StringUtils.isEmpty(diskImageGuid)) {
            return null;
        }

        return _diskImageDao.findByGuidOnHost(diskImageGuid, hostUuid);
    }

    @Override
    public void importVm(VmImage vmImage, String vmName, String vmRefId, AsyncCallback callback) {
        ImportImageFromVmEvent event = new ImportImageFromVmEvent(vmImage, vmName, vmRefId, callback);
        _eventBusService.post(event);
    }

    @Override
    public void cloneUpdateVmImage(VmImage vmImage, AsyncCallback callback) {
        CloneImageForUpdateEvent event = new CloneImageForUpdateEvent(null, vmImage, callback);
        _eventBusService.post(event);
    }

    @Override
    public void cancelUpdateVmImage(VmImage vmImage, AsyncCallback callback) {
        DiskImage updatingDiskImage = null;

        String diskImageUuid = vmImage.getUpdatingDiskImageUuid();
        if (!StringUtils.isEmpty(diskImageUuid)) {
            updatingDiskImage = _diskImageDao.findByUuid(diskImageUuid);
        }

        vmImage.setUpdatingDiskImageUuid(null);
        vmImage.setUpdatingDiskImageIp(null);
        vmImage.setStatus(VmImage.Status.Ready);
        commit(vmImage);

        if (updatingDiskImage == null) {
            LOGGER.info(String.format("No onging updating for image \"%s\"", vmImage.getName()));

            if (callback != null) {
                callback.onSuccess(null);
            }

            return;
        }

        _diskImageService.deleteDiskImage(updatingDiskImage, callback);
    }

    @Override
    public void importUpdatedVmImage(VmImage vmImage, AsyncCallback callback) {
        ImportUpdatedImageEvent event = new ImportUpdatedImageEvent(vmImage, callback);
        _eventBusService.post(event);
    }

    @Override
    public void deleteVmImage(VmImage vmImage, AsyncCallback callback) {
        if (0 == _diskImageService.countByOwner(vmImage.getUuid())) {
            delete(vmImage);

            if (callback != null) {
                callback.onSuccess(null);
            }

            return;
        }

        DeleteVmImageEvent event = new DeleteVmImageEvent(vmImage, callback);
        _eventBusService.post(event);
    }

    @Override
    public void revertVmImage(VmImage vmImage, AsyncCallback callback) {
        String diskImageGuid = vmImage.getCurrentDiskImageGuid();
        if (!StringUtils.isEmpty(diskImageGuid)) {
            List<? extends DiskImage> diskImages = _diskImageService.getDiskImagesByGuid(diskImageGuid);
            if (diskImages != null && !diskImages.isEmpty()) {
                if (!StringUtils.isEmpty((diskImageGuid = diskImages.get(0).getParentGuid()))) {
                    // Get parent diskImages.
                    diskImages = _diskImageService.getDiskImagesByGuid(diskImageGuid);
                    if (diskImages != null && !diskImages.isEmpty()) {
                        // Set to parent diskImages.
                        vmImage.setVersion(diskImages.get(0).getVersion());
                        vmImage.setCurrentDiskImageGuid(diskImageGuid);
                        vmImage.setUpdatingDiskImageUuid(null);
                        vmImage.setUpdatingDiskImageIp(null);
                        vmImage.setStatus(VmImage.Status.Ready);
                        vmImage.setUpdateTime(System.currentTimeMillis());
                        commit(vmImage);
                    }
                }
            }
        }

        if (callback != null) {
            callback.onSuccess(null);
        }
    }
}

