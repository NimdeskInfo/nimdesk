/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.service.impl.workflow;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.nimdesk.event.AddRemoveVmStateListenerEvent;
import com.nimdesk.event.AllocateStorageEvent;
import com.nimdesk.event.AsyncCallback;
import com.nimdesk.event.ConnectHostEvent;
import com.nimdesk.event.ImportDiskImageEvent;
import com.nimdesk.event.InstantiateVmEvent;
import com.nimdesk.event.PowerOpVmEvent;
import com.nimdesk.event.TemplatizeVmEvent;
import com.nimdesk.hypervisor.HypervisorContext;
import com.nimdesk.model.DiskImage;
import com.nimdesk.model.EventLog;
import com.nimdesk.model.Host;
import com.nimdesk.model.Server;
import com.nimdesk.model.Storage;
import com.nimdesk.model.Task;
import com.nimdesk.model.VmImage;
import com.nimdesk.service.EventBusService;
import com.nimdesk.service.ObjectPropertyChangeEvent;
import com.nimdesk.service.ObjectPropertyChangeListener;
import com.nimdesk.service.Workflow;
import com.nimdesk.service.impl.ServiceLocator;
import com.nimdesk.tracker.TrackerManager;
import com.nimdesk.util.ActionDelegate;
import com.nimdesk.util.IdGenerator;
import com.nimdesk.util.Pair;
import com.nimdesk.util.Utils;
import com.nimdesk.vm.InstantiatedVm;
import com.nimdesk.vm.TemplatizedVm;
import com.nimdesk.vm.VmStateListener;

/**
 * Step 1: Connect to host
 * Step 2: Shutdown VM
 * Step 3: Import disk image
 * Step 4: Instantiate VM (register, power on)
 * Step 5: Sysprep VM (fix machine name, put it into local workgroup)
 * Step 6: Templatize VM (shutdown, snapshot, unregister)
 */
public class ImportImageFromVmFlow extends Workflow {
    private static final Logger LOGGER = Logger.getLogger(ImportImageFromVmFlow.class);

    // Strong references to keep weak referenced StateRunnable (such as SysprepState) in memory.
    private static final Set<Object> REFERENCES = Collections.synchronizedSet(new HashSet<Object>());

    private final long _createTime = System.currentTimeMillis();

    private final EventBusService _eventBusService;
    private final Host.Service _hostService;
    private final Storage.Service _storageService;
    private final VmImage.Service _vmImageService;
    private final DiskImage.Service _diskImageService;
    private final Task.Service _taskService;
    private final EventLog.Service _eventLogService;
    private final TrackerManager _trackerManager;

    private final String _vmName;
    private final String _vmRefId;

    private final Server _localServer;
    private final Host _localHost;
    private final VmImage _vmImage;
    private final DiskImage _diskImage;
    private final Task _task;

    // ConnectHostState result
    private HypervisorContext _hypervisorContext;

    // ConnectVcState result
    private HypervisorContext _vcHypervisorContext = null;

    // AllocateStorageState result
    private Storage _storage;

    // CopyDiskImageState result
    private String _imageVmxPath;

    // RegisterVmState result;
    private String _imageVmRefId = null;

    // TemplatizeVmState result
    private boolean _vmCbrcEnabled = false;

    public ImportImageFromVmFlow(VmImage vmImage, String vmName, String vmRefId,
            final AsyncCallback callback) {
        super();

        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("ImportImageFromVmFlow()");
        }

        ServiceLocator serviceLocator = ServiceLocator.getInstance();

        _eventBusService = serviceLocator.getEventBusService();
        _hostService = serviceLocator.getHostService();
        _storageService = serviceLocator.getStorageService();
        _vmImageService = serviceLocator.getVmImageService();
        _diskImageService = serviceLocator.getDiskImageService();
        _taskService = serviceLocator.getTaskService();
        _eventLogService = serviceLocator.getEventLogService();
        _trackerManager = serviceLocator.getTrackerManager();

        _vmName = vmName;
        _vmRefId = vmRefId;

        Server.Service serverService = serviceLocator.getServerService();
        _localServer = serverService.getLocalServer();
        if (_localServer == null) {
            throw new RuntimeException(String.format("Unable to find local server to import VM \"%s\"", _vmName));
        }

        _localHost = _hostService.getLocalHost();
        if (_localHost == null) {
            throw new RuntimeException(String.format("Unable to find local host to import VM \"%s\"", _vmName));
        }

        _vmImage = vmImage;
        _vmImage.setStatus(VmImage.Status.Importing);
        _vmImage.setUpdateTime(_createTime);
        _vmImageService.commit(_vmImage);

        _diskImage = createDiskImage(_vmImage);
        _task = createTask(_vmImage, (callback != null) ? callback.getRequestor() : null);

        append(new ConnectHostState(2), null);
        if (!StringUtils.isEmpty(_localHost.getVCenterIp())
                && !StringUtils.isEmpty(_localHost.getVCenterUsername())
                && !StringUtils.isEmpty(_localHost.getVCenterPassword())) {
            append(new ConnectVCenterState(3), null);
        }
        append(new ShutdownVmState(5), null);
        append(new AllocateStorageState(8), null);
        append(new ImportDiskImageState(10, 60), null);
        append(new InstantiateVmState(60), null);
        append(new SysprepVmState(70, 90), null);
        append(new TemplatizeVmState(90), null);

        setCallback(new AsyncCallback() {
            @Override
            @SuppressWarnings("unchecked")
            public void onSuccess(Object result) {
                disconnectHost();

                DiskImage diskImage = _diskImageService.getByUuid(_diskImage.getUuid());
                if (diskImage == null
                        || DiskImage.Status.Deleting == diskImage.getStatus()) {
                    LOGGER.error(String.format("Unable to find DiskImage id=\"%s\" in db", _diskImage.getUuid()));
                    throw new RuntimeException("Can't locate the entity");
                }

                diskImage.setVmRefId(null);
                diskImage.setCbrcEnabled(_vmCbrcEnabled);
                diskImage.setStatus(DiskImage.Status.Ready);
                _diskImageService.commit(diskImage);

                VmImage vmImage = _vmImageService.getByUuid(_vmImage.getUuid());
                if (vmImage == null
                        || VmImage.Status.Deleting == vmImage.getStatus()) {
                    LOGGER.error(String.format("Unable to find VmImage id=\"%s\" in db", _vmImage.getUuid()));
                    throw new RuntimeException("Can't locate the entity");
                }

                vmImage.setStatus(VmImage.Status.Ready);
                _vmImageService.commit(vmImage);

                _task.setEndTime(System.currentTimeMillis());
                _task.setStatus(Task.Status.Completed);
                _task.setDetails("Succeeded");
                _taskService.commit(_task);

                _trackerManager.removeProgress(_diskImage.getUuid());
                _trackerManager.removeProgress(_vmImage.getUuid());
                _trackerManager.removeProgress(_task.getUuid());

                _eventLogService.addVmImageEventLog(EventLog.Severity.Info,
                        _vmImage, "info.image.import.succeeded", getRequestor(), _task,
                        new Pair<String, String>("user", getRequestor()),
                        new Pair<String, String>("image", _vmImage.getName()),
                        new Pair<String, String>("vm", _vmName));

                LOGGER.info(String.format("ImportImageFromVmFlow: Imported disk image \"%s\" for image \"%s\" from VM \"%s\"",
                        _diskImage.getName(), _vmImage.getName(), _vmName));

                if (callback != null) {
                    callback.onSuccess(result);
                }
            }

            @Override
            @SuppressWarnings("unchecked")
            public void onFailure(Throwable t) {
                disconnectHost();

                DiskImage diskImage = _diskImageService.getByUuid(_diskImage.getUuid());
                if (diskImage == null
                        || DiskImage.Status.Deleting == diskImage.getStatus()) {
                    LOGGER.error(String.format("Unable to find DiskImage id=\"%s\" in db", _diskImage.getUuid()));
                } else {
                    diskImage.setStatus(DiskImage.Status.Error);
                    _diskImageService.commit(diskImage);
                }

                VmImage vmImage = _vmImageService.getByUuid(_vmImage.getUuid());
                if (vmImage == null
                        || VmImage.Status.Deleting == vmImage.getStatus()) {
                    LOGGER.error(String.format("Unable to find VmImage id=\"%s\" in db", _vmImage.getUuid()));
                } else {
                    vmImage.setStatus(VmImage.Status.Broken);
                    _vmImageService.commit(vmImage);
                }

                _task.setEndTime(System.currentTimeMillis());
                _task.setStatus(Task.Status.Failed);
                _task.setDetails(t.getMessage());
                _taskService.commit(_task);

                _trackerManager.removeProgress(_diskImage.getUuid());
                _trackerManager.removeProgress(_vmImage.getUuid());
                _trackerManager.removeProgress(_task.getUuid());

                _eventLogService.addVmImageEventLog(EventLog.Severity.Error,
                        _vmImage, "error.image.import.failed", getRequestor(), _task,
                        new Pair<String, String>("user", getRequestor()),
                        new Pair<String, String>("image", _vmImage.getName()),
                        new Pair<String, String>("vm", _vmName));

                LOGGER.error(String.format("ImportImageFromVmFlow: Failed to import disk image \"%s\" for image \"%s\" from VM \"%s\" - %s",
                        _diskImage.getName(), _vmImage.getName(), _vmName, t.getMessage()), t);

                if (callback != null) {
                    callback.onFailure(t);
                }
            }

            @Override
            public void progress(int percentage) {
                if (callback != null) {
                    callback.progress(percentage);
                }
            }

            @Override
            public String getRequestor() {
                return (callback != null) ? callback.getRequestor() : null;
            }
        });
    }

    public Task getTask() {
        return _task;
    }

    private void disconnectHost() {
        if (_hypervisorContext != null) {
            _hypervisorContext.disconnect();
            _hypervisorContext = null;
        }
        if (_vcHypervisorContext != null) {
            _vcHypervisorContext.disconnect();
            _vcHypervisorContext = null;
        }
    }

    @SuppressWarnings("unchecked")
    private Task createTask(VmImage vmImage, String requestor) {
        Task task = _taskService.newInstance();

        task.setType(Task.Type.ImportImageFromVm);
        task.setStartTime(_createTime);
        task.setServerUuid(_localServer.getUuid());

        task.setTargetType(VmImage.class.getSimpleName());
        task.setTargetUuid(vmImage.getUuid());
        task.setTargetName(vmImage.getName());

        task.setStatus(Task.Status.InProgress);
        task.setDetails("Starting");

        _taskService.commit(task);

        _eventLogService.addVmImageEventLog(EventLog.Severity.Info,
                _vmImage, "info.image.import.start", requestor, _task,
                new Pair<String, String>("user", requestor),
                new Pair<String, String>("image", _vmImage.getName()),
                new Pair<String, String>("vm", _vmName));

        return task;
    }

    private DiskImage createDiskImage(VmImage vmImage) {
        String version = Utils.convertVersion(_createTime);

        DiskImage diskImage = _diskImageService.newInstance();
        diskImage.setType(DiskImage.Type.ImageFullBase);
        diskImage.setGuid(IdGenerator.generateUuid());
        diskImage.setHostUuid(_localHost.getUuid());
        diskImage.setStorageUuid("");
        diskImage.setCreateTime(_createTime);
        diskImage.setName(vmImage.getName() + "-M" + version);
        diskImage.setOwnerUuid(vmImage.getUuid());
        diskImage.setVersion(1L);
        diskImage.setStatus(DiskImage.Status.Importing);
        _diskImageService.commit(diskImage);

        vmImage.setVersion(diskImage.getVersion());
        vmImage.setCurrentDiskImageGuid(diskImage.getGuid());
        vmImage.setStatus(VmImage.Status.Importing);
        vmImage.setUpdateTime(System.currentTimeMillis());
        _vmImageService.commit(vmImage);

        return diskImage;
    }

    private class ConnectHostState implements Workflow.StateRunnable {
        private final int _progressStart;

        ConnectHostState(int progressStart) {
            _progressStart = progressStart;
        }

        @Override
        public void run(AsyncCallback callback) {
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace("ConnectHostState.run()");
            }

            _task.setDetails("Preparing");
            _taskService.commit(_task);

            _trackerManager.updateProgress(_diskImage.getUuid(), _progressStart, null, "Preparing...");
            _trackerManager.updateProgress(_vmImage.getUuid(), _progressStart, null, "Preparing...");
            _trackerManager.updateProgress(_task.getUuid(), _progressStart, null, "Preparing...");

            ConnectHostEvent event = new ConnectHostEvent(_localHost.getAddress(), 0,
                    _localHost.getUsername(), _localHost.getPassword(), callback);
            _eventBusService.post(event);
        }

        @Override
        public void setResult(Object result) {
            _hypervisorContext = (HypervisorContext) result;
        }
    }

    private class ConnectVCenterState implements Workflow.StateRunnable {
        private final int _progressStart;

        ConnectVCenterState(int progressStart) {
            _progressStart = progressStart;
        }

        @Override
        public void run(AsyncCallback callback) {
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace("ConnectVCenterState.run()");
            }

            _task.setDetails("Preparing");
            _taskService.commit(_task);

            _trackerManager.updateProgress(_diskImage.getUuid(), _progressStart, null, "Preparing...");
            _trackerManager.updateProgress(_vmImage.getUuid(), _progressStart, null, "Preparing...");
            _trackerManager.updateProgress(_task.getUuid(), _progressStart, null, "Preparing...");

            String vCenterAddress = _localHost.getVCenterAddress();
            if (StringUtils.isEmpty(vCenterAddress)) {
                vCenterAddress = _localHost.getVCenterIp();
            }

            ConnectHostEvent event = new ConnectHostEvent(vCenterAddress, 0,
                    _localHost.getVCenterUsername(), _localHost.getVCenterPassword(), callback);
            _eventBusService.post(event);
        }

        @Override
        public void setResult(Object result) {
            _vcHypervisorContext = (HypervisorContext) result;
        }
    }

    private class ShutdownVmState implements Workflow.StateRunnable {
        private final int _progressStart;

        ShutdownVmState(int progressStart) {
            _progressStart = progressStart;
        }

        @Override
        public void run(AsyncCallback callback) {
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace("ShutdownVmState.run()");
            }

            _task.setDetails(String.format("Shutting down vm \"%s\"", _vmName));
            _taskService.commit(_task);

            _trackerManager.updateProgress(_diskImage.getUuid(), _progressStart, null, "Shutting down VM...");
            _trackerManager.updateProgress(_vmImage.getUuid(), _progressStart, null, "Shutting down VM...");
            _trackerManager.updateProgress(_task.getUuid(), _progressStart, null, "Shutting down VM...");

            PowerOpVmEvent event = new PowerOpVmEvent(_hypervisorContext,
                    _vmRefId, PowerOpVmEvent.PowerOps.PowerOff, callback);;
            _eventBusService.post(event);
        }

        @Override
        public void setResult(Object result) {
        }
    }

    private class AllocateStorageState implements Workflow.StateRunnable {
        private final int _progressStart;

        AllocateStorageState(int progressStart) {
            _progressStart = progressStart;
        }

        @Override
        public void run(AsyncCallback callback) {
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace("AllocateStorageState.run()");
            }

            _task.setDetails(String.format("Allocate storage for importing \"%s\"", _vmName));
            _taskService.commit(_task);

            _trackerManager.updateProgress(_diskImage.getUuid(), _progressStart, null, "Allocating storage...");
            _trackerManager.updateProgress(_vmImage.getUuid(), _progressStart, null, "Allocating storage...");
            _trackerManager.updateProgress(_task.getUuid(), _progressStart, null, "Allocating storage...");

            AllocateStorageEvent event = new AllocateStorageEvent(_hypervisorContext,
                    _storageService.getHostStorages(_localHost.getUuid()),
                    ImportImageFromVmFlow.class.getName(), callback);
            _eventBusService.post(event);
        }

        @Override
        public void setResult(Object result) {
            _storage = (Storage) result;
        }
    }

    private class ImportDiskImageState implements Workflow.StateRunnable {
        private final int _progressStart;
        private final int _progressEnd;
        private int _lastProgress = 0;

        ImportDiskImageState(int progressStart, int progressEnd) {
            _progressStart = progressStart;
            _progressEnd = progressEnd;
        }

        @Override
        public void run(AsyncCallback callback) {
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace("ImportDiskImageState.run()");
            }

            DiskImage diskImage = _diskImageService.getByUuid(_diskImage.getUuid());
            if (diskImage == null
                    || DiskImage.Status.Deleting == diskImage.getStatus()) {
                LOGGER.error(String.format("Unable to find DiskImage id=\"%s\" in db", _diskImage.getUuid()));
                throw new RuntimeException("Can't locate the entity");
            }

            diskImage.setStorageUuid(_storage.getUuid());
            _diskImageService.commit(diskImage);

            _task.setDetails(String.format("Importing vm \"%s\"", _vmName));
            _taskService.commit(_task);

            _trackerManager.updateProgress(_diskImage.getUuid(), _progressStart, null, "Importing VM...");
            _trackerManager.updateProgress(_vmImage.getUuid(), _progressStart, null, "Importing VM...");
            _trackerManager.updateProgress(_task.getUuid(), _progressStart, null, "Importing VM...");

            _lastProgress = _progressStart;

            Map<String, String> customProps = new HashMap<String, String>();
            customProps.put("guestinfo.nimdesk.type", "image");
            customProps.put("guestinfo.nimdesk.iid", _diskImage.getUuid());
            customProps.put("guestinfo.nimdesk.serverUrl",
                    String.format("http://%s:8080/", _localServer.getPrivateAddress()));

            ImportDiskImageEvent event = new ImportDiskImageEvent(_hypervisorContext, _vmRefId,
                    _storage, _diskImage.getName(), _localHost.getVmNetwork(), customProps,
                    Workflow.wrapCallbackWithProgressUpdater(callback, new ActionDelegate<Integer>() {
                        @Override
                        public void action(Integer param) {
                            int percentage = Utils.convertFullPercentage(param, _progressStart, _progressEnd);
                            if (_lastProgress != percentage) {
                                _trackerManager.updateProgress(_diskImage.getUuid(), percentage);
                                _trackerManager.updateProgress(_vmImage.getUuid(), percentage);
                                _trackerManager.updateProgress(_task.getUuid(), percentage);
                                _lastProgress = percentage;
                            }
                        }
                    }));
            _eventBusService.post(event);
        }

        @Override
        public void setResult(Object result) {
            _imageVmxPath = (String) result;
        }
    }

    private class InstantiateVmState implements Workflow.StateRunnable {
        private final int _progressStart;

        InstantiateVmState(int progressStart) {
            _progressStart = progressStart;
        }

        @Override
        public void run(AsyncCallback callback) {
            LOGGER.info(String.format("InstantiateVmState.run() - vmx: %s", _imageVmxPath));

            _task.setDetails(String.format("Instantiating image vm \"%s\"", _diskImage.getName()));
            _taskService.commit(_task);

            _trackerManager.updateProgress(_diskImage.getUuid(), _progressStart, null, "Processing...");
            _trackerManager.updateProgress(_vmImage.getUuid(), _progressStart, null, "Processing...");
            _trackerManager.updateProgress(_task.getUuid(), _progressStart, null, "Processing...");

            InstantiateVmEvent event = new InstantiateVmEvent(_hypervisorContext, _localHost.getRefId(),
                    _diskImage.getName(), _imageVmxPath, true, 0, 0, false, null, false,
                    _vcHypervisorContext, _localHost.getEsxUuid(), callback);
            _eventBusService.post(event);
        }

        @Override
        public void setResult(Object result) {
            InstantiatedVm instantiatedVm = (InstantiatedVm) result;
            _imageVmRefId = instantiatedVm.getRefId();
        }
    }

    private class SysprepVmState implements Workflow.StateRunnable,
            ObjectPropertyChangeListener, VmStateListener {
        private final int _progressStart;
        @SuppressWarnings("unused")
        private final int _progressEnd;

        private AsyncCallback _callback;
        private boolean _done = false;

        SysprepVmState(int progressStart, int progressEnd) {
            _progressStart = progressStart;
            _progressEnd = progressEnd;
        }

        @Override
        public void run(AsyncCallback callback) {
            LOGGER.info(String.format("SysprepVmState.run() - vm: %s", _diskImage.getName()));

            DiskImage diskImage = _diskImageService.getByUuid(_diskImage.getUuid());
            if (diskImage == null
                    || DiskImage.Status.Deleting == diskImage.getStatus()) {
                LOGGER.error(String.format("Unable to find DiskImage id=\"%s\" in db", _diskImage.getUuid()));
                throw new RuntimeException("Can't locate the entity");
            }

            diskImage.setVmRefId(_imageVmRefId);
            _diskImageService.commit(diskImage);

            _task.setDetails(String.format("Preparing image vm \"%s\"", _diskImage.getName()));
            _taskService.commit(_task);

            _trackerManager.updateProgress(_diskImage.getUuid(), _progressStart, null, "Preparing image...");
            _trackerManager.updateProgress(_vmImage.getUuid(), _progressStart, null, "Preparing image...");
            _trackerManager.updateProgress(_task.getUuid(), _progressStart, null, "Preparing image...");

            _callback = callback;

            addListeners();
        }

        private void addListeners() {
            // Start listening on VM state
            AddRemoveVmStateListenerEvent listenerEvent =
                    new AddRemoveVmStateListenerEvent(true, this);
            _eventBusService.post(listenerEvent);

            // Start listening on DiskImage Status change
            _diskImageService.addObjectPropertyChangeListener(this);

            REFERENCES.add(this);
        }

        private void removeListeners() {
            REFERENCES.remove(this);

            // Stop listening on DiskImage Status change
            _diskImageService.removeObjectPropertyChangeListener(this);

            // Stop listening on VM state
            AddRemoveVmStateListenerEvent listenerEvent =
                    new AddRemoveVmStateListenerEvent(false, this);
            _eventBusService.post(listenerEvent);
        }

        @Override
        public void setResult(Object result) {
        }

        @Override
        public void newObject(ObjectPropertyChangeEvent event) {
        }

        @Override
        public void deleteObject(ObjectPropertyChangeEvent event) {
            final DiskImage diskImage = (DiskImage) event.getSource();
            if (StringUtils.equals(_diskImage.getUuid(), diskImage.getUuid())) {
                removeListeners();

                synchronized(this) {
                    if (_done) {
                        return;
                    }
                    _done = true;
                }

                // The VmImage & DiskImage has been removed - fail this flow
                if (_callback != null) {
                    _callback.onFailure(new Exception("This image has been deleted"));
                }
            }
        }

        @Override
        public void propertyChange(ObjectPropertyChangeEvent event) {
            final DiskImage diskImage = (DiskImage) event.getSource();
            if (StringUtils.equals(_diskImage.getUuid(), diskImage.getUuid())) {
                switch (diskImage.getStatus()) {
                case Prepared:
                    break;

                case Error:
                    removeListeners();

                    synchronized(this) {
                        if (_done) {
                            return;
                        }
                        _done = true;
                    }

                    // The Sysprep'ing has failed - fail this flow
                    if (_callback != null) {
                        _callback.onFailure(new Exception("Failed to preparing image VM"));
                    }
                    break;

                default:
                    break;
                }
            }
        }

        @Override
        public void onNew(String vmRefId, State state) {
        }

        @Override
        public void onDelete(String vmRefId) {
            if (_imageVmRefId == null || !_imageVmRefId.equals(vmRefId)) {
                return;
            }

            removeListeners();

            synchronized(this) {
                if (_done) {
                    return;
                }
                _done = true;
            }

            _imageVmRefId = null;

            // The Sysprep'ing has failed - fail this flow
            if (_callback != null) {
                _callback.onFailure(new Exception("Failed to preparing image VM - VM has been deleted"));
            }
        }

        @Override
        public void onPowerStateChange(String vmRefId, VmStateListener.State state) {
            if (_imageVmRefId == null || !_imageVmRefId.equals(vmRefId)) {
                return;
            }

            if (state == VmStateListener.State.PoweredOff) {
                removeListeners();

                synchronized(this) {
                    if (_done) {
                        return;
                    }
                    _done = true;
                }

                if (_callback != null) {
                    _callback.onSuccess(null);
                }
            }
         }
    }

    private class TemplatizeVmState implements Workflow.StateRunnable {
        private final int _progressStart;

        TemplatizeVmState(int progressStart) {
            _progressStart = progressStart;
        }

        @Override
        public void run(AsyncCallback callback) {
            LOGGER.info(String.format("TemplatizeVmState.run() - vm: %s", _diskImage.getName()));

            _task.setDetails(String.format("Converting vm \"%s\" to image", _diskImage.getName()));
            _taskService.commit(_task);

            _trackerManager.updateProgress(_diskImage.getUuid(), _progressStart, null, "Converting to image...");
            _trackerManager.updateProgress(_vmImage.getUuid(), _progressStart, null, "Converting to image...");
            _trackerManager.updateProgress(_task.getUuid(), _progressStart, null, "Converting to image...");

            TemplatizeVmEvent event = new TemplatizeVmEvent(_hypervisorContext, _imageVmRefId, _diskImage.getGuid(),
                    _localHost.isCbrcEnabled(), _vcHypervisorContext, _localHost.getEsxUuid(), callback);
            _eventBusService.post(event);
        }

        @Override
        public void setResult(Object result) {
            TemplatizedVm templatizedVm = (TemplatizedVm) result;
            _vmCbrcEnabled = templatizedVm.isCbrcEnabled();
        }
    }
}

