/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.nimdesk.database.dao.UserSessionDao;
import com.nimdesk.database.vo.UserSessionVO;
import com.nimdesk.model.Host;
import com.nimdesk.model.Pool;
import com.nimdesk.model.Server;
import com.nimdesk.model.UserSession;
import com.nimdesk.model.VmInstance;
import com.nimdesk.service.ObjectPropertyChangeEvent;
import com.nimdesk.util.IdGenerator;
import com.nimdesk.util.Pagination;
import com.nimdesk.util.Pair;

@Service(UserSession.Service.BEAN_NAME)
public class UserSessionServiceImpl extends ObjectPropertyChangeSupport implements UserSession.Service, InitializingBean {
    private static final Logger LOGGER = Logger.getLogger(UserSessionServiceImpl.class);

    @Autowired
    private UserSessionDao _userSessionDao;

    @Autowired
    private Host.Service _hostService;

    @Autowired
    private Server.Service _serverService;

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(_userSessionDao, "A UserSessionDao must be set");
        Assert.notNull(_hostService, "A Host.Service must be set");
        Assert.notNull(_serverService, "A Server.Service must be set");
    }

    @Override
    public Long count() {
        return _userSessionDao.countAll();
    }

    @Override
    public UserSession newInstance() {
        UserSession userSession = new UserSessionVO();

        // Generate uuid
        userSession.setUuid(IdGenerator.generateUuid());

        return userSession;
    }

    @Override
    public List<? extends UserSession> getAll(Pagination pagination) {
        return _userSessionDao.findAll(pagination);
    }

    @Override
    public UserSession getByUuid(String uuid) {
        return _userSessionDao.findByUuid(uuid);
    }

    @Override
    public void commit(UserSession obj) {
        if (!(obj instanceof UserSessionVO)) {
            String error = String.format("commit: passing wrong type %s", obj.getClass().toString());
            LOGGER.error(error);
            throw new IllegalArgumentException(error);
        }

        UserSessionVO userSession = (UserSessionVO) obj;

        if (userSession.getId() == 0) {
            _userSessionDao.createNew(userSession);
            userSession.resetChangedProperties();
            fireCreateEvent(new ObjectPropertyChangeEvent(userSession, null));
        } else {
            _userSessionDao.update(userSession);
            Map<String, Pair<Object, Object>> changedProperties = userSession.resetChangedProperties();
            fireChangeEvent(new ObjectPropertyChangeEvent(userSession, changedProperties));
        }
    }

    @Override
    public boolean delete(UserSession obj) {
        if (!(obj instanceof UserSessionVO)) {
            String error = String.format("delete: passing wrong type %s", obj.getClass().toString());
            LOGGER.error(error);
            throw new IllegalArgumentException(error);
        }

        UserSessionVO userSession = (UserSessionVO) obj;

        try {
            if (userSession.getId() > 0) {
                return _userSessionDao.delete(userSession.getId());
            }
        } finally {
            fireDeleteEvent(new ObjectPropertyChangeEvent(userSession));
        }

        return true;
    }

    @Override
    public void startUserSession(String userUuid, Pool pool, VmInstance vmInstance) {
        Assert.hasText(userUuid, "Empty user uuid");
        Assert.notNull(pool, "Null Pool");
        Assert.notNull(vmInstance, "Null VmInstance");

        UserSession userSession = newInstance();
        userSession.setUserUuid(userUuid);
        userSession.setPoolName(pool.getName());
        userSession.setVmInstanceUuid(vmInstance.getUuid());
        userSession.setVmInstanceName(vmInstance.getName());
        userSession.setVmInstanceDns(vmInstance.getDnsName());
        userSession.setVmInstanceIp(vmInstance.getIpAddress());
        userSession.setHostUuid(vmInstance.getHostUuid());
        userSession.setStartTime(System.currentTimeMillis());
        commit(userSession);
    }

    @Override
    public void completeUserSession(VmInstance vmInstance) {
        List<? extends UserSession> userSessionList = _userSessionDao.findActiveByVmInstance(vmInstance.getUuid());
        if (userSessionList != null) {
            for (UserSession userSession : userSessionList) {
                userSession.setEndTime(System.currentTimeMillis());
                commit(userSession);
            }
        }
    }

    @Override
    public List<? extends UserSession> getUserSessionsByHost(String hostUuid) {
        return _userSessionDao.findByHost(hostUuid);
    }

    @Override
    public List<? extends UserSession> getUserSessionsByHostAndTime(String hostUuid, boolean before, long time) {
        return _userSessionDao.findByHostAndTime(hostUuid, before, time);
    }

    @Override
    public List<? extends UserSession> getLocalUserSessions() {
        Host localHost = _hostService.getLocalHost();
        if (localHost == null) {
            return null;
        }

        return _userSessionDao.findByHost(localHost.getUuid());
    }

    @Override
    public List<? extends UserSession> getLocalUserSessionsByTime(boolean before, long time) {
        Host localHost = _hostService.getLocalHost();
        if (localHost == null) {
            return null;
        }

        return _userSessionDao.findByHostAndTime(localHost.getUuid(), before, time);
    }

    @Override
    public boolean isLocalUserSession(UserSession userSession) {
        return _hostService.isLocalHost(userSession.getHostUuid());
    }

    @Override
    public void invalidateLocalActiveSessions() {
        Host localHost = _hostService.getLocalHost();
        if (localHost != null) {
            _userSessionDao.invalidateActiveSessions(localHost.getUuid());
        }
    }

    @Override
    public void deleteOldUserSessions(long time) {
        _userSessionDao.deleteOldUserSessions(time);
    }
}
