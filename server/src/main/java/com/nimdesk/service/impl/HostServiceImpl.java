/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.nimdesk.database.dao.HostDao;
import com.nimdesk.database.vo.HostVO;
import com.nimdesk.model.DiskImage;
import com.nimdesk.model.Host;
import com.nimdesk.model.Storage;
import com.nimdesk.model.VmInstance;
import com.nimdesk.service.ObjectPropertyChangeEvent;
import com.nimdesk.util.IdGenerator;
import com.nimdesk.util.Pagination;
import com.nimdesk.util.Pair;

@Service(Host.Service.BEAN_NAME)
public class HostServiceImpl extends ObjectPropertyChangeSupport implements Host.Service, InitializingBean {
    private static final Logger LOGGER = Logger.getLogger(HostServiceImpl.class);

    @Autowired
    private HostDao _hostDao;

    @Autowired
    private Storage.Service _storageService;

    @Autowired
    private DiskImage.Service _diskImageService;

    @Autowired
    private VmInstance.Service _vmInstanceService;

    // Cached local hosts
    private final Map<String, HostVO> _localHosts = new HashMap<String, HostVO>();
    private final List<HostVO> _localHostList = new ArrayList<HostVO>();

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(_hostDao, "A HostDao must be set");
        Assert.notNull(_storageService, "A Storage.Service must be set");
        Assert.notNull(_diskImageService, "A DiskImage.Service must be set");
        Assert.notNull(_vmInstanceService, "A VmInstance.Service must be set");
    }

    @Override
    public Long count() {
        return _hostDao.countAll();
    }

    @Override
    public Host newInstance() {
        Host host = new HostVO();

        // Generate uuid
        host.setUuid(IdGenerator.generateUuid());

        return host;
    }

    @Override
    public List<? extends Host> getAll(Pagination pagination) {
        return _hostDao.findAll(pagination);
    }

    @Override
    public Host getByUuid(String uuid) {
        return _hostDao.findByUuid(uuid);
    }

    @Override
    public void commit(Host obj) {
        if (!(obj instanceof HostVO)) {
            String error = String.format("commit: passing wrong type %s", obj.getClass().toString());
            LOGGER.error(error);
            throw new IllegalArgumentException(error);
        }

        HostVO host = (HostVO) obj;

        if (host.getId() == 0) {
            _hostDao.createNew(host);

            if (host.isLocal()) {
                synchronized(_localHosts) {
                    _localHosts.put(host.getUuid(), host);
                    _localHostList.clear(); // Invalidate the cached list
                }
            }

            host.resetChangedProperties();
            fireCreateEvent(new ObjectPropertyChangeEvent(host, null));
        } else {
            _hostDao.update(host);

            if (host.isLocal()) {
                synchronized(_localHosts) {
                    _localHosts.put(host.getUuid(), host);
                    _localHostList.clear(); // Invalidate the cached list
                }
            }

            Map<String, Pair<Object, Object>> changedProperties = host.resetChangedProperties();
            fireChangeEvent(new ObjectPropertyChangeEvent(host, changedProperties));
        }
    }

    @Override
    public boolean delete(Host obj) {
        if (!(obj instanceof HostVO)) {
            String error = String.format("delete: passing wrong type %s", obj.getClass().toString());
            LOGGER.error(error);
            throw new IllegalArgumentException(error);
        }

        HostVO host = (HostVO) obj;

        try {
            if (host.getId() > 0) {
                return _hostDao.delete(host.getId());
            }
        } finally {
            synchronized(_localHosts) {
                _localHosts.remove(host.getUuid());
                _localHostList.clear(); // Invalidate the cached list
            }

            fireDeleteEvent(new ObjectPropertyChangeEvent(host));
        }

        return true;
    }

    @Override
    public List<? extends Host> getLocalHosts() {
        synchronized(_localHosts) {
            if (!_localHosts.isEmpty()) {
                if (_localHostList.isEmpty()) {
                    _localHostList.addAll(_localHosts.values());
                }
                return _localHostList;
            }
        }

        List<HostVO> hosts = _hostDao.findLocalHosts();
        if (hosts == null || hosts.isEmpty()) {
            return hosts;
        }

        synchronized(_localHosts) {
            for (HostVO host : hosts) {
                _localHosts.put(host.getUuid(), host);
            }

            if (_localHostList.isEmpty()) {
                _localHostList.addAll(_localHosts.values());
            }
            return _localHostList;
        }
    }

    @Override
    public Host getLocalHost() {
        List<? extends Host> hosts = getLocalHosts();
        if (hosts == null || hosts.isEmpty()) {
            return null;
        }

        return hosts.get(0);
    }

    @Override
    public boolean isLocalHost(String hostUuid) {
        if (StringUtils.isEmpty(hostUuid)) {
            return false;
        }

        synchronized(_localHosts) {
            if (_localHosts.isEmpty()) {
                List<HostVO> hosts = _hostDao.findLocalHosts();
                if (hosts != null && !hosts.isEmpty()) {
                    for (HostVO host : hosts) {
                        _localHosts.put(host.getUuid(), host);
                    }
                }
            }

            return _localHosts.containsKey(hostUuid);
        }
    }

    @Override
    public List<? extends Host> getHostsForServer(String serverUuid) {
        return _hostDao.findByServer(serverUuid);
    }

    @Override
    public Long countByStatus(Host.Status... status) {
        return _hostDao.countByStatus(status);
    }

    @Override
    public void invalidateNonLocalHosts() {
        _hostDao.invalidateNonLocalHosts();
    }

    @Override
    public void removeHost(Host host) {
        List<? extends Storage> storages = _storageService.getHostStorages(host.getUuid());
        if (storages != null) {
            for (Storage storage : storages) {
                _storageService.delete(storage);
            }
        }

        List<? extends DiskImage> diskImages = _diskImageService.getDiskImagesOnHost(host.getUuid());
        if (diskImages != null) {
            for (DiskImage diskImage : diskImages) {
                _diskImageService.delete(diskImage);
            }
        }

        List<? extends VmInstance> vmInstances = _vmInstanceService.getVmInstancesOnHost(host.getUuid());
        if (vmInstances != null) {
            for (VmInstance vmInstance : vmInstances) {
                _vmInstanceService.delete(vmInstance);
            }
        }

        delete(host);
    }
}
