/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.service.impl.workflow;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.nimdesk.database.vo.VmInstanceVO;
import com.nimdesk.event.AsyncCallback;
import com.nimdesk.event.ConnectHostEvent;
import com.nimdesk.event.DeleteLdapComputerEvent;
import com.nimdesk.event.DeleteStorageDirectoryEvent;
import com.nimdesk.event.UninstantiateVmEvent;
import com.nimdesk.hypervisor.HypervisorContext;
import com.nimdesk.ldap.LdapHelper;
import com.nimdesk.model.EventLog;
import com.nimdesk.model.Host;
import com.nimdesk.model.LdapServer;
import com.nimdesk.model.Pool;
import com.nimdesk.model.Server;
import com.nimdesk.model.Storage;
import com.nimdesk.model.Task;
import com.nimdesk.model.VmInstance;
import com.nimdesk.service.EventBusService;
import com.nimdesk.service.Workflow;
import com.nimdesk.service.impl.ServiceLocator;
import com.nimdesk.util.Pair;

/**
 * Step 1: Connect to host
 * Step 2: Power off and unregister VM
 * Step 3: Delete VM directory from datastore
 * Step 4: Delete computer entry from Ldap
 */
public class DeleteDesktopFlow extends Workflow {
    private static final Logger LOGGER = Logger.getLogger(DeleteDesktopFlow.class);

    private final long _createTime = System.currentTimeMillis();

    private final EventBusService _eventBusService;
    private final Host.Service _hostService;
    private final Storage.Service _storageService;
    private final VmInstance.Service _vmInstanceService;
    private final LdapServer.Service _ldapServerService;
    private final Task.Service _taskService;
    private final EventLog.Service _eventLogService;

    private final Server _localServer;
    private final Host _localHost;
    private final Pool _pool;

    // Don't cache VmInstance itself as it could be updated by other components.
    private final String _vmInstanceUuid;
    private final String _vmInstanceName;
    private final String _vmInstanceRefId;
    private final String _vmInstanceStorageUuid;
    private final String _vmInstanceDnsName;

    private final Task _task;

    // ConnectHostState result
    private final boolean _isContextOwner;
    private HypervisorContext _hypervisorContext;

    // ConnectVcState result
    private HypervisorContext _vcHypervisorContext = null;

    // UninstantiateVmState result
    private String _vmxPath;

    public DeleteDesktopFlow(HypervisorContext context, HypervisorContext vcContext, Host localHost, Pool pool, VmInstance vmInstance,
            final AsyncCallback callback) {
        super();

        LOGGER.info(String.format("DeleteDesktopFlow: Deleting desktop \"%s\" from pool \"%s\"",
                vmInstance.getName(), (pool != null) ? pool.getName() : "(unknown)"));

        ServiceLocator serviceLocator = ServiceLocator.getInstance();

        _eventBusService = serviceLocator.getEventBusService();
        _hostService = serviceLocator.getHostService();
        _storageService = serviceLocator.getStorageService();
        _vmInstanceService = serviceLocator.getVmInstanceService();
        _ldapServerService = serviceLocator.getLdapServerService();
        _taskService = serviceLocator.getTaskService();
        _eventLogService = serviceLocator.getEventLogService();

        Server.Service serverService = serviceLocator.getServerService();
        _localServer = serverService.getLocalServer();
        if (_localServer == null) {
            throw new RuntimeException(String.format("Unable to find local server to delete VM \"%s\"", vmInstance.getName()));
        }

        if (localHost != null) {
            _localHost = localHost;
        } else {
            _localHost = _hostService.getLocalHost();
        }
        if (_localHost == null) {
            throw new RuntimeException(String.format("Unable to find host to delete VM \"%s\"", vmInstance.getName()));
        }
        if (!_localHost.getUuid().equals(vmInstance.getHostUuid())) {
            throw new IllegalArgumentException(String.format("VM \"%s\" is not on this host", vmInstance.getName()));
        }

        _pool = pool;
        _vmInstanceUuid = vmInstance.getUuid();
        _vmInstanceName = vmInstance.getName();
        _vmInstanceRefId = vmInstance.getVmRefId();
        _vmInstanceStorageUuid = vmInstance.getStorageUuid();
        _vmInstanceDnsName = vmInstance.getDnsName();

        _task = createTask(vmInstance, _pool, (callback != null) ? callback.getRequestor() : null);
        LOGGER.info("DeleteDesktopFlow: Created task");

        if (context != null) {
            _hypervisorContext = context;
            _vcHypervisorContext = vcContext;
            _isContextOwner = false;
        } else {
            append(new ConnectHostState(), null);
            if (!StringUtils.isEmpty(_localHost.getVCenterIp())
                    && !StringUtils.isEmpty(_localHost.getVCenterUsername())
                    && !StringUtils.isEmpty(_localHost.getVCenterPassword())) {
                append(new ConnectVCenterState(), null);
            }
            _isContextOwner = true;
        }
        if (!StringUtils.isEmpty(vmInstance.getVmRefId())) {
            // The following steps are needed only if VM exists.
            append(new UninstantiateVmState(), null);
            append(new DeleteVmDirectoryState(), null);
            if (_pool != null && _pool.isDomain() && !StringUtils.isEmpty(vmInstance.getDnsName())) {
                append(new DeleteLdapEntryState(), null);
            }
        } else {
            // Attempt to delete vm directory any way.
            append(new DeleteVmDirectoryState(), null);
        }

        setCallback(new AsyncCallback() {
            @Override
            @SuppressWarnings("unchecked")
            public void onSuccess(Object result) {
                disconnectHost();

                VmInstance vmInstance = _vmInstanceService.getByUuid(_vmInstanceUuid);
                if (vmInstance == null) {
                    LOGGER.warn(String.format("Unable to find VmInstance id=\"%s\" in db", _vmInstanceUuid));
                } else {
                    if (Pool.Type.Persistent != _pool.getType()) {
                        vmInstance.setUserUuid("");
                    }

                    if (vmInstance.isRecompose()
                            || Pool.Status.Provisioning == _pool.getStatus()
                            || Pool.Status.Recomposing == _pool.getStatus()) {
                        // Can not reset last error as it's needed to be passed across creation retry.
                        vmInstance.setVmRefId("");
                        vmInstance.setStatus(VmInstance.Status.New);
                        vmInstance.setUpdateTime(System.currentTimeMillis());
                        _vmInstanceService.commit(vmInstance);
                    } else {
                        _vmInstanceService.delete(vmInstance);
                    }
                }

                _task.setEndTime(System.currentTimeMillis());
                _task.setStatus(Task.Status.Completed);
                _task.setDetails("Succeeded");
                _taskService.commit(_task);

                String poolName = (_pool != null) ? _pool.getName() : "(unknown)";

                _eventLogService.addVmInstanceEventLog(EventLog.Severity.Info,
                        vmInstance, "info.desktop.delete.succeeded", getRequestor(), _task,
                        new Pair<String, String>("user", getRequestor()),
                        new Pair<String, String>("desktop", _vmInstanceName),
                        new Pair<String, String>("pool", poolName));

                LOGGER.info(String.format("DeleteDesktopFlow: Deleted desktop \"%s\" from pool \"%s\"",
                        _vmInstanceName, poolName));

                if (callback != null) {
                    callback.onSuccess(result);
                }
            }

            @Override
            @SuppressWarnings("unchecked")
            public void onFailure(Throwable t) {
                disconnectHost();

                if (_vmxPath == null) {
                    // If no vm can be found, we consider the deletion is successful.
                    onSuccess(null);
                    return;
                }

                VmInstance vmInstance = _vmInstanceService.getByUuid(_vmInstanceUuid);
                if (vmInstance == null) {
                    LOGGER.warn(String.format("Unable to find VmInstance id=\"%s\" in db", _vmInstanceUuid));
                } else {
                    if (vmInstance.isRecompose()) {
                        // If this is recompose, try to build a new desktop anyway.
                        vmInstance.setLastError(null);
                        vmInstance.setVmRefId("");
                        vmInstance.setStatus(VmInstance.Status.New);
                        vmInstance.setUpdateTime(System.currentTimeMillis());
                        _vmInstanceService.commit(vmInstance);
                    } else {
                        vmInstance.setVmRefId("");
                        vmInstance.setStatus(VmInstance.Status.Error);
                        vmInstance.setLastError(VmInstance.Error.DeleteError);
                        vmInstance.setUpdateTime(System.currentTimeMillis());
                        _vmInstanceService.commit(vmInstance);
                    }
                }

                _task.setEndTime(System.currentTimeMillis());
                _task.setStatus(Task.Status.Failed);
                _task.setDetails(t.getMessage());
                _taskService.commit(_task);

                String poolName = (_pool != null) ? _pool.getName() : "(unknown)";

                _eventLogService.addVmInstanceEventLog(EventLog.Severity.Error,
                        vmInstance, "error.desktop.delete.failed", getRequestor(), _task,
                        new Pair<String, String>("user", getRequestor()),
                        new Pair<String, String>("desktop", _vmInstanceName),
                        new Pair<String, String>("pool", poolName));

                LOGGER.error(String.format("DeleteDesktopFlow: Failed to delete desktop \"%s\" from pool \"%s\" - %s",
                        _vmInstanceName, poolName, t.getMessage()), t);

                if (callback != null) {
                    callback.onFailure(t);
                }
            }

            @Override
            public void progress(int percentage) {
                if (callback != null) {
                    callback.progress(percentage);
                }
            }

            @Override
            public String getRequestor() {
                return (callback != null) ? callback.getRequestor() : null;
            }
        });
    }

    public Task getTask() {
        return _task;
    }

    private void disconnectHost() {
        if (!_isContextOwner) {
            return;
        }

        if (_hypervisorContext != null) {
            _hypervisorContext.disconnect();
            _hypervisorContext = null;
        }
        if (_vcHypervisorContext != null) {
            _vcHypervisorContext.disconnect();
            _vcHypervisorContext = null;
        }
    }

    @SuppressWarnings("unchecked")
    private Task createTask(VmInstance vmInstance, Pool pool, String requestor) {
        if (vmInstance instanceof VmInstanceVO) {
            // Suppressing PoolManager to start another vmInstance deletion.
            ((VmInstanceVO) vmInstance).inOperation(true);
        }
        if (VmInstance.Status.Destroying != vmInstance.getStatus()
                && VmInstance.Status.Destroyed != vmInstance.getStatus()) {
            vmInstance.setAssigned(false);
            vmInstance.setLoginTime(0);
            vmInstance.setLogOffMode(null);
            vmInstance.setStatus(VmInstance.Status.Destroying);
            vmInstance.setUpdateTime(System.currentTimeMillis());
            _vmInstanceService.commit(vmInstance);
        }

        Task task = _taskService.newInstance();

        task.setType(Task.Type.DeleteDesktop);
        task.setStartTime(_createTime);
        task.setServerUuid(_localServer.getUuid());

        task.setTargetType(VmInstance.class.getSimpleName());
        task.setTargetUuid(vmInstance.getUuid());
        task.setTargetName(vmInstance.getName());
        task.setAssociateType(Pool.class.getSimpleName());
        if (pool != null) {
            task.setAssociateUuid(pool.getUuid());
            task.setAssociateName(pool.getName());
        }

        task.setStatus(Task.Status.InProgress);
        task.setDetails("Starting");

        _taskService.commit(task);

        _eventLogService.addVmInstanceEventLog(EventLog.Severity.Info,
                vmInstance, "info.desktop.delete.start", requestor, task,
                new Pair<String, String>("user", requestor),
                new Pair<String, String>("desktop", vmInstance.getName()),
                new Pair<String, String>("pool", (pool != null) ? pool.getName() : "(unknown)"));

        return task;
    }

    private class ConnectHostState implements Workflow.StateRunnable {
        @Override
        public void run(AsyncCallback callback) {
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace("ConnectHostState.run()");
            }

            _task.setDetails("Preparing");
            _taskService.commit(_task);

            ConnectHostEvent event = new ConnectHostEvent(_localHost.getAddress(), 0,
                    _localHost.getUsername(), _localHost.getPassword(), callback);
            _eventBusService.post(event);
        }

        @Override
        public void setResult(Object result) {
            _hypervisorContext = (HypervisorContext) result;
        }
    }

    private class ConnectVCenterState implements Workflow.StateRunnable {
        @Override
        public void run(AsyncCallback callback) {
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace("ConnectVCenterState.run()");
            }

            _task.setDetails("Preparing");
            _taskService.commit(_task);

            String vCenterAddress = _localHost.getVCenterAddress();
            if (StringUtils.isEmpty(vCenterAddress)) {
                vCenterAddress = _localHost.getVCenterIp();
            }

            ConnectHostEvent event = new ConnectHostEvent(vCenterAddress, 0,
                    _localHost.getVCenterUsername(), _localHost.getVCenterPassword(), callback);
            _eventBusService.post(event);
        }

        @Override
        public void setResult(Object result) {
            _vcHypervisorContext = (HypervisorContext) result;
        }
    }

    private class UninstantiateVmState implements Workflow.StateRunnable {
        @Override
        public void run(AsyncCallback callback) {
            LOGGER.info(String.format("UninstantiateVmState.run() - vmRefId: %s", _vmInstanceRefId));

            _task.setDetails(String.format("Uninstantiating vm \"%s\"", _vmInstanceName));
            _taskService.commit(_task);

            UninstantiateVmEvent event = new UninstantiateVmEvent(_hypervisorContext, _vmInstanceRefId,
                    _vcHypervisorContext, _localHost.getEsxUuid(), callback);
            _eventBusService.post(event);
        }

        @Override
        public void setResult(Object result) {
            _vmxPath = (String) result;
        }
    }

    private class DeleteVmDirectoryState implements Workflow.StateRunnable {
        @Override
        public void run(AsyncCallback callback) {
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace("DeleteVmDirectoryState.run()");
            }

            _task.setDetails(String.format("Deleting vm directory for \"%s\"", _vmInstanceName));
            _taskService.commit(_task);

            if (!StringUtils.isEmpty(_vmxPath)) {
                Storage storage = _storageService.getByUuid(_vmInstanceStorageUuid);
                if (storage != null) {
                    DeleteStorageDirectoryEvent event = new DeleteStorageDirectoryEvent(_hypervisorContext,
                            _vmxPath, storage, callback);
                    _eventBusService.post(event);

                    return;
                }
            }

            // If vmxPath is null or storage is not found, succeed.
            if (callback != null) {
                callback.onSuccess(null);
            }
        }

        @Override
        public void setResult(Object result) {
        }
    }

    private class DeleteLdapEntryState implements Workflow.StateRunnable {
        @Override
        public void run(AsyncCallback callback) {
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace("DeleteLdapEntryState.run()");
            }

            _task.setDetails(String.format("Deleting AD entry for \"%s\"", _vmInstanceName));
            _taskService.commit(_task);

            LdapServer ldapServer = _ldapServerService.getByDomain(_pool.getDomainOrWorkGroup());
            if (ldapServer == null) {
                LOGGER.warn(String.format("Unable to find ldap server for pool \"%s\"", _pool.getName()));

                if (callback != null) {
                    callback.onSuccess(null);
                }

                return;
            }

            DeleteLdapComputerEvent event =
                    new DeleteLdapComputerEvent(LdapHelper.composeUrl(ldapServer.getAddress(), 389, ldapServer.isSsl()),
                            ldapServer.getDomain(), ldapServer.getUsername(), ldapServer.getPassword(),
                            _vmInstanceDnsName, callback);
            _eventBusService.post(event);
        }

        @Override
        public void setResult(Object result) {
        }
    }
}
