/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.nimdesk.database.dao.TaskDao;
import com.nimdesk.database.vo.TaskVO;
import com.nimdesk.model.Host;
import com.nimdesk.model.Server;
import com.nimdesk.model.Task;
import com.nimdesk.service.ObjectPropertyChangeEvent;
import com.nimdesk.util.IdGenerator;
import com.nimdesk.util.Pagination;
import com.nimdesk.util.Pair;

@Service(Task.Service.BEAN_NAME)
public class TaskServiceImpl extends ObjectPropertyChangeSupport implements Task.Service, InitializingBean {
    private static final Logger LOGGER = Logger.getLogger(TaskServiceImpl.class);

    @Autowired
    private TaskDao _taskDao;

    @Autowired
    private Server.Service _serverService;

    @Autowired
    private Host.Service _hostService;

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(_taskDao, "A TaskDao must be set");
        Assert.notNull(_serverService, "A Server.Service must be set");
        Assert.notNull(_hostService, "A Host.Service must be set");
    }

    @Override
    public Long count() {
        return _taskDao.countAll();
    }

    @Override
    public Task newInstance() {
        Task task = new TaskVO();

        // Generate uuid
        task.setUuid(IdGenerator.generateUuid());

        return task;
    }

    @Override
    public List<? extends Task> getAll(Pagination pagination) {
        return _taskDao.findAll(pagination);
    }

    @Override
    public Task getByUuid(String uuid) {
        return _taskDao.findByUuid(uuid);
    }

    @Override
    public void commit(Task obj) {
        if (!(obj instanceof TaskVO)) {
            String error = String.format("commit: passing wrong type %s", obj.getClass().toString());
            LOGGER.error(error);
            throw new IllegalArgumentException(error);
        }

        TaskVO task = (TaskVO) obj;

        if (task.getId() == 0) {
            _taskDao.createNew(task);
            task.resetChangedProperties();
            fireCreateEvent(new ObjectPropertyChangeEvent(task, null));
        } else {
            _taskDao.update(task);
            Map<String, Pair<Object, Object>> changedProperties = task.resetChangedProperties();
            fireChangeEvent(new ObjectPropertyChangeEvent(task, changedProperties));
        }
    }

    @Override
    public boolean delete(Task obj) {
        if (!(obj instanceof TaskVO)) {
            String error = String.format("delete: passing wrong type %s", obj.getClass().toString());
            LOGGER.error(error);
            throw new IllegalArgumentException(error);
        }

        TaskVO task = (TaskVO) obj;

        try {
            if (task.getId() > 0) {
                return _taskDao.delete(task.getId());
            }
        } finally {
            fireDeleteEvent(new ObjectPropertyChangeEvent(task));
        }

        return true;
    }

    @Override
    public Long countAllPoolTasks() {
        return _taskDao.countAllPools();
    }

    @Override
    public Long countPoolTasks(String poolUuid) {
        return _taskDao.countByPool(poolUuid);
    }

    @Override
    public Long countAllVmImageTasks() {
        return _taskDao.countAllVmImages();
    }

    @Override
    public Long countVmImageTasks(String vmImageUuid) {
        return _taskDao.countByVmImage(vmImageUuid);
    }

    @Override
    public Long countAllVmInstanceTasks() {
        return _taskDao.countAllVmInstances();
    }

    @Override
    public Long countVmInstanceTasks(String vmInstanceUuid) {
        return _taskDao.countByVmInstance(vmInstanceUuid);
    }

    @Override
    public List<? extends Task> getAllPoolTasks(Pagination pagination) {
        return _taskDao.findAllPools(pagination);
    }

    @Override
    public List<? extends Task> getPoolTasks(String poolUuid, Pagination pagination) {
        return _taskDao.findByPool(poolUuid, pagination);
    }

    @Override
    public List<? extends Task> getAllVmImageTasks(Pagination pagination) {
        return _taskDao.findAllVmImages(pagination);
    }

    @Override
    public List<? extends Task> getVmImageTasks(String vmImageUuid, Pagination pagination) {
        return _taskDao.findByVmImage(vmImageUuid, pagination);
    }

    @Override
    public List<? extends Task> getAllVmInstanceTasks(Pagination pagination) {
        return _taskDao.findAllVmInstances(pagination);
    }

    @Override
    public List<? extends Task> getVmInstanceTasks(String vmInstanceUuid, Pagination pagination) {
        return _taskDao.findByVmInstance(vmInstanceUuid, pagination);
    }

    @Override
    public List<? extends Task> getLocalTasks() {
        Server localServer = _serverService.getLocalServer();
        if (localServer == null) {
            return null;
        }

        return _taskDao.findByServer(localServer.getUuid());
    }

    @Override
    public List<? extends Task> getLocalTasksByTime(boolean before, long time) {
        Server localServer = _serverService.getLocalServer();
        if (localServer == null) {
            return null;
        }

        return _taskDao.findByServerAndTime(localServer.getUuid(), before, time);
    }

    @Override
    public boolean isLocalTask(Task task) {
        return _serverService.isLocalServer(task.getServerUuid());
    }

    @Override
    public void invalidateLocalInProgressTasks() {
        Server localServer = _serverService.getLocalServer();
        if (localServer != null) {
            _taskDao.invalidateInProgressTasks(localServer.getUuid());
        }
    }

    @Override
    public void deleteOldTasks(long time) {
        _taskDao.deleteOldTasks(time);
    }
}

