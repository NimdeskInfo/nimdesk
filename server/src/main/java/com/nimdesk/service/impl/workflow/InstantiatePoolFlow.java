/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.service.impl.workflow;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.nimdesk.event.AddRemoveVmStateListenerEvent;
import com.nimdesk.event.AllocateStorageEvent;
import com.nimdesk.event.AsyncCallback;
import com.nimdesk.event.ConnectHostEvent;
import com.nimdesk.event.InstantiateVmEvent;
import com.nimdesk.event.LinkedCloneImageEvent;
import com.nimdesk.event.TemplatizeVmEvent;
import com.nimdesk.hypervisor.HypervisorContext;
import com.nimdesk.model.DiskImage;
import com.nimdesk.model.EventLog;
import com.nimdesk.model.Host;
import com.nimdesk.model.LdapServer;
import com.nimdesk.model.Pool;
import com.nimdesk.model.Server;
import com.nimdesk.model.Storage;
import com.nimdesk.model.Task;
import com.nimdesk.model.VmImage;
import com.nimdesk.service.EventBusService;
import com.nimdesk.service.ObjectPropertyChangeEvent;
import com.nimdesk.service.ObjectPropertyChangeListener;
import com.nimdesk.service.Workflow;
import com.nimdesk.service.impl.ServiceLocator;
import com.nimdesk.tracker.TrackerManager;
import com.nimdesk.util.ActionDelegate;
import com.nimdesk.util.Pair;
import com.nimdesk.util.Utils;
import com.nimdesk.vm.InstantiatedVm;
import com.nimdesk.vm.TemplatizedVm;
import com.nimdesk.vm.VmStateListener;

/**
 * Step 1: Connect to host
 * Step 2: Linked clone disk image of VmImage
 * Step 3: Instantiate VM (register, power on)
 * Step 4: Sysprep VM (fix machine name, join AD etc.)
 * Step 5: Templatize VM (shutdown, snapshot, unregister)
 */
public class InstantiatePoolFlow extends Workflow {
    private static final Logger LOGGER = Logger.getLogger(InstantiatePoolFlow.class);

    // Strong references to keep weak referenced StateRunnable (such as SysprepState) in memory.
    private static final Set<Object> REFERENCES = Collections.synchronizedSet(new HashSet<Object>());

    private final long _createTime = System.currentTimeMillis();

    private final EventBusService _eventBusService;
    private final Host.Service _hostService;
    private final Storage.Service _storageService;
    private final LdapServer.Service _ldapServerService;
    private final Pool.Service _poolService;
    private final VmImage.Service _vmImageService;
    private final DiskImage.Service _diskImageService;
    private final Task.Service _taskService;
    private final EventLog.Service _eventLogService;
    private final TrackerManager _trackerManager;

    private final Server _localServer;
    private final Host _localHost;

    private final DiskImage _diskImage;
    private final Task _task;

    private final String _poolUuid;
    private final String _poolName;

    private final VmImage _vmImage;
    private final DiskImage _srcDiskImage;

    // ConnectHostState result
    private HypervisorContext _hypervisorContext;

    // ConnectVcState result
    private HypervisorContext _vcHypervisorContext = null;

    // AllocateStorageState result
    private Storage _storage;

    // LinkedCloneDiskImageState result
    private String _vmxPath;

    // InstantiateVmState result
    private String _vmRefId;

    // TemplatizeVmState result
    private boolean _vmCbrcEnabled = false;

    public InstantiatePoolFlow(Pool pool, final AsyncCallback callback) {
        super();

        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("InstantiatePoolFlow()");
        }

        ServiceLocator serviceLocator = ServiceLocator.getInstance();

        _eventBusService = serviceLocator.getEventBusService();
        _hostService = serviceLocator.getHostService();
        _storageService = serviceLocator.getStorageService();
        _ldapServerService = serviceLocator.getLdapServerService();
        _poolService = serviceLocator.getPoolService();
        _vmImageService = serviceLocator.getVmImageService();
        _diskImageService = serviceLocator.getDiskImageService();
        _taskService = serviceLocator.getTaskService();
        _eventLogService = serviceLocator.getEventLogService();
        _trackerManager = serviceLocator.getTrackerManager();

        Server.Service serverService = serviceLocator.getServerService();
        _localServer = serverService.getLocalServer();
        if (_localServer == null) {
            throw new RuntimeException(String.format("Unable to find host to create pool \"%s\"", pool.getName()));
        }

        _localHost = _hostService.getLocalHost();
        if (_localHost == null) {
            throw new RuntimeException(String.format("Unable to find host to create pool \"%s\"", pool.getName()));
        }

        _poolUuid = pool.getUuid();
        _poolName = pool.getName();

        _vmImage = _vmImageService.getByUuid(pool.getVmImageUuid());
        if (_vmImage == null) {
            LOGGER.error(String.format("Pool (%s) doesn't have a valid image (uuid=%s)",
                    pool.getName(), pool.getVmImageUuid()));
            throw new RuntimeException("No valid image");
        }

        _srcDiskImage = _vmImageService.getCurrentDiskImage(_vmImage, _localHost.getUuid());
        if (_srcDiskImage == null) {
            LOGGER.error(String.format("Pool (%s) VM image (uuid=%s) doesn't have a valid disk image (uuid=%s)",
                    pool.getName(), _vmImage.getUuid(), _vmImage.getCurrentDiskImageGuid()));
            throw new RuntimeException("No valid image on this server");
        }

        _diskImage = createDiskImage(pool);
        _task = createTask(pool, (callback != null) ? callback.getRequestor() : null);

        append(new SanityCheckState(1), null);
        append(new ConnectHostState(3), null);
        if (!StringUtils.isEmpty(_localHost.getVCenterIp())
                && !StringUtils.isEmpty(_localHost.getVCenterUsername())
                && !StringUtils.isEmpty(_localHost.getVCenterPassword())) {
            append(new ConnectVCenterState(5), null);
        }
        append(new AllocateStorageState(8), null);
        append(new LinkedCloneDiskImageState(10, 30), null);
        append(new InstantiateVmState(30), null);
        append(new SysprepVmState(50, 90), null);
        append(new TemplatizeVmState(90), null);

        setCallback(new AsyncCallback() {
            @Override
            @SuppressWarnings("unchecked")
            public void onSuccess(Object result) {
                disconnectHost();

                Pool pool = _poolService.getByUuid(_poolUuid);
                if (pool == null) {
                    LOGGER.warn(String.format("Unable to find Pool id=\"%s\" in db", _poolUuid));
                }

                DiskImage diskImage = _diskImageService.getByUuid(_diskImage.getUuid());
                if (diskImage == null
                        || DiskImage.Status.Deleting == diskImage.getStatus()) {
                    LOGGER.error(String.format("Unable to find DiskImage id=\"%s\" in db", _diskImage.getUuid()));
                    throw new RuntimeException("Can't locate the entity");
                }

                diskImage.setCbrcEnabled(_vmCbrcEnabled);
                diskImage.setVmRefId(null);
                diskImage.setIpAddress(null);
                diskImage.setStatus(DiskImage.Status.Ready);
                _diskImageService.commit(diskImage);

                _task.setEndTime(System.currentTimeMillis());
                _task.setStatus(Task.Status.Completed);
                _task.setDetails("Succeeded");
                _taskService.commit(_task);

                _trackerManager.removeProgress(_diskImage.getUuid());
                _trackerManager.removeProgress(_task.getUuid());

                _eventLogService.addPoolEventLog(EventLog.Severity.Info,
                        pool, "info.pool.create.succeeded", getRequestor(), _task,
                        new Pair<String, String>("user", getRequestor()),
                        new Pair<String, String>("pool", _poolName));

                LOGGER.info(String.format("InstantiatePoolFlow: Instantiated pool \"%s\"", _poolName));

                if (callback != null) {
                    callback.onSuccess(result);
                }
            }

            @Override
            @SuppressWarnings("unchecked")
            public void onFailure(Throwable t) {
                disconnectHost();

                try {
                    _diskImage.setStatus(DiskImage.Status.Error);
                    _diskImageService.commit(_diskImage);
                } catch (Exception e) {
                    // Ignore error - diskImage could have been deleted.
                }

                Pool pool = _poolService.getByUuid(_poolUuid);
                if (pool == null || Pool.Status.Deleting == pool.getStatus()) {
                    LOGGER.warn(String.format("Unable to find Pool id=\"%s\" in db", _poolUuid));
                } else {
                    pool.setStatus(Pool.Status.Error);
                    pool.setUpdateTime(System.currentTimeMillis());
                    _poolService.commit(pool);
                }

                _task.setEndTime(System.currentTimeMillis());
                _task.setStatus(Task.Status.Failed);
                _task.setDetails(t.getMessage());
                _taskService.commit(_task);

                _trackerManager.removeProgress(_diskImage.getUuid());
                _trackerManager.removeProgress(_task.getUuid());

                _eventLogService.addPoolEventLog(EventLog.Severity.Error,
                        pool, "error.pool.create.failed", getRequestor(), _task,
                        new Pair<String, String>("user", getRequestor()),
                        new Pair<String, String>("pool", _poolName));

                LOGGER.error(String.format("InstantiatePoolFlow: Failed to instantiate from pool \"%s\" - %s",
                        _poolName, t.getMessage()), t);

                if (callback != null) {
                    callback.onFailure(t);
                }
            }

            @Override
            public void progress(int percentage) {
                if (callback != null) {
                    callback.progress(percentage);
                }
            }

            @Override
            public String getRequestor() {
                return (callback != null) ? callback.getRequestor() : null;
            }
        });
    }

    public Task getTask() {
        return _task;
    }

    private void disconnectHost() {
        if (_hypervisorContext != null) {
            _hypervisorContext.disconnect();
            _hypervisorContext = null;
        }
        if (_vcHypervisorContext != null) {
            _vcHypervisorContext.disconnect();
            _vcHypervisorContext = null;
        }
    }

    @SuppressWarnings("unchecked")
    private Task createTask(Pool pool, String requestor) {
        Task task = _taskService.newInstance();

        task.setType(Task.Type.InstantiatePool);
        task.setStartTime(_createTime);
        task.setServerUuid(_localServer.getUuid());

        task.setTargetType(Pool.class.getSimpleName());
        task.setTargetUuid(pool.getUuid());
        task.setTargetName(pool.getName());

        task.setStatus(Task.Status.InProgress);
        task.setDetails("Starting");

        _taskService.commit(task);

        _eventLogService.addPoolEventLog(EventLog.Severity.Info,
                pool, "info.pool.create.start", requestor, task,
                new Pair<String, String>("user", requestor),
                new Pair<String, String>("pool", pool.getName()));

        return task;
    }

    private DiskImage createDiskImage(Pool pool) {
        DiskImage diskImage = _diskImageService.newInstance();
        if (Pool.Type.NonPersistent == pool.getType()) {
            diskImage.setType(DiskImage.Type.PoolBase);
        } else {
            diskImage.setType(DiskImage.Type.PoolBase2);
        }
        diskImage.setGuid(pool.getBaseDiskImageGuid());
        diskImage.setHostUuid(_localHost.getUuid());
        diskImage.setStorageUuid("");
        diskImage.setCreateTime(_createTime);
        diskImage.setName(pool.getName() + "-P" + Utils.convertVersion(System.currentTimeMillis()));
        diskImage.setOwnerUuid(pool.getUuid());
        diskImage.setVersion(_srcDiskImage.getVersion());
        diskImage.setParentGuid(_srcDiskImage.getGuid());
        diskImage.setDiskSize(_srcDiskImage.getDiskSize());
        diskImage.setStatus(DiskImage.Status.Cloning);
        _diskImageService.commit(diskImage);

        _poolService.commit(pool);

        return diskImage;
    }

    private class SanityCheckState implements Workflow.StateRunnable {
        private final int _progressStart;

        SanityCheckState(int progressStart) {
            _progressStart = progressStart;
        }

        @Override
        public void run(AsyncCallback callback) {
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace("SanityCheckState.run()");
            }

            try {
                _trackerManager.updateProgress(_diskImage.getUuid(), _progressStart, null, "Checking...");
                _trackerManager.updateProgress(_task.getUuid(), _progressStart, null, "Checking...");

                Pool pool = _poolService.getByUuid(_poolUuid);
                if (pool == null || Pool.Status.Deleting == pool.getStatus()) {
                    LOGGER.error(String.format("Unable to find Pool id=\"%s\" in db", _poolUuid));
                    throw new Exception("No valid desktop pool");
                }

                switch (_vmImage.getStatus()) {
                    case Broken:
                        LOGGER.warn(String.format("Pool (%s) invalid image (uuid=%s) state \"%s\"",
                                pool.getName(), _vmImage.getUuid(), _vmImage.getStatus().toString()));
                        throw new Exception("The image is broken");

                    case Importing:
                        LOGGER.warn(String.format("Pool (%s) invalid image (uuid=%s) state \"%s\"",
                                pool.getName(), _vmImage.getUuid(), _vmImage.getStatus().toString()));
                        throw new Exception("The image on this server is not ready");
                }

                if (pool.isDomain()) {
                    LdapServer ldapServer = _ldapServerService.getByDomain(pool.getDomainOrWorkGroup());
                    if (ldapServer == null) {
                        LOGGER.error(String.format("Pool (%s) VM image (uuid=%s) doesn't have a ldap server (domain=%s)",
                                pool.getName(), _vmImage.getUuid(), pool.getDomainOrWorkGroup()));
                        throw new Exception("No valid LDAP server");
                    }
                }

                if (callback != null) {
                    callback.onSuccess(null);
                }
            } catch (Exception e) {
                if (callback != null) {
                    callback.onFailure(e);
                }
            }
        }

        @Override
        public void setResult(Object result) {
        }
    }

    private class ConnectHostState implements Workflow.StateRunnable {
        private final int _progressStart;

        ConnectHostState(int progressStart) {
            _progressStart = progressStart;
        }

        @Override
        public void run(AsyncCallback callback) {
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace("ConnectHostState.run()");
            }

            _task.setDetails("Preparing");
            _taskService.commit(_task);

            _trackerManager.updateProgress(_diskImage.getUuid(), _progressStart, null, "Preparing...");
            _trackerManager.updateProgress(_task.getUuid(), _progressStart, null, "Preparing...");

            ConnectHostEvent event = new ConnectHostEvent(_localHost.getAddress(), 0,
                    _localHost.getUsername(), _localHost.getPassword(), callback);
            _eventBusService.post(event);
        }

        @Override
        public void setResult(Object result) {
            _hypervisorContext = (HypervisorContext) result;
        }
    }

    private class ConnectVCenterState implements Workflow.StateRunnable {
        private final int _progressStart;

        ConnectVCenterState(int progressStart) {
            _progressStart = progressStart;
        }

        @Override
        public void run(AsyncCallback callback) {
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace("ConnectVCenterState.run()");
            }

            _task.setDetails("Preparing");
            _taskService.commit(_task);

            _trackerManager.updateProgress(_diskImage.getUuid(), _progressStart, null, "Preparing...");
            _trackerManager.updateProgress(_task.getUuid(), _progressStart, null, "Preparing...");

            String vCenterAddress = _localHost.getVCenterAddress();
            if (StringUtils.isEmpty(vCenterAddress)) {
                vCenterAddress = _localHost.getVCenterIp();
            }

            ConnectHostEvent event = new ConnectHostEvent(vCenterAddress, 0,
                    _localHost.getVCenterUsername(), _localHost.getVCenterPassword(), callback);
            _eventBusService.post(event);
        }

        @Override
        public void setResult(Object result) {
            _vcHypervisorContext = (HypervisorContext) result;
        }
    }

    private class AllocateStorageState implements Workflow.StateRunnable {
        private final int _progressStart;

        AllocateStorageState(int progressStart) {
            _progressStart = progressStart;
        }

        @Override
        public void run(AsyncCallback callback) {
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace("AllocateStorageState.run()");
            }

            _task.setDetails(String.format("Allocate storage for instantiating pool \"%s\"", _poolName));
            _taskService.commit(_task);

            _trackerManager.updateProgress(_diskImage.getUuid(), _progressStart, null, "Allocating storage...");
            _trackerManager.updateProgress(_task.getUuid(), _progressStart, null, "Allocating storage...");

            AllocateStorageEvent event = new AllocateStorageEvent(_hypervisorContext,
                    _storageService.getHostStorages(_localHost.getUuid()),
                    InstantiatePoolFlow.class.getName(), callback);
            _eventBusService.post(event);
        }

        @Override
        public void setResult(Object result) {
            _storage = (Storage) result;
        }
    }

    private class LinkedCloneDiskImageState implements Workflow.StateRunnable {
        private final int _progressStart;
        private final int _progressEnd;
        private int _lastProgress = 0;

        LinkedCloneDiskImageState(int progressStart, int progressEnd) {
            _progressStart = progressStart;
            _progressEnd = progressEnd;
        }

        @Override
        public void run(AsyncCallback callback) {
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace("ConnectHostState.run()");
            }

            DiskImage diskImage = _diskImageService.getByUuid(_diskImage.getUuid());
            if (diskImage == null
                    || DiskImage.Status.Deleting == diskImage.getStatus()) {
                LOGGER.error(String.format("Unable to find DiskImage id=\"%s\" in db", _diskImage.getUuid()));
                throw new RuntimeException("Can't locate the entity");
            }

            diskImage.setStorageUuid(_storage.getUuid());
            _diskImageService.commit(diskImage);

            _task.setDetails(String.format("Cloning image \"%s\"", _vmImage.getName()));
            _taskService.commit(_task);

            _trackerManager.updateProgress(_diskImage.getUuid(), _progressStart, null, "Creating VM...");
            _trackerManager.updateProgress(_task.getUuid(), _progressStart, null, "Creating VM...");

            _lastProgress = _progressStart;

            Storage srcStorage = _storageService.getByUuid(_srcDiskImage.getStorageUuid());

            Map<String, String> customProps = new HashMap<String, String>();
            customProps.put("guestinfo.nimdesk.type", "pool");
            customProps.put("guestinfo.nimdesk.iid", _diskImage.getUuid());
            customProps.put("guestinfo.nimdesk.serverUrl",
                    String.format("http://%s:8080/", _localServer.getPrivateAddress()));

            LinkedCloneImageEvent event = new LinkedCloneImageEvent(_hypervisorContext, _srcDiskImage,
                    srcStorage, null, _diskImage.getName(), _storage, _localHost.getVmNetwork(), customProps,
                    Workflow.wrapCallbackWithProgressUpdater(callback, new ActionDelegate<Integer>() {
                        @Override
                        public void action(Integer param) {
                            int percentage = Utils.convertFullPercentage(param, _progressStart, _progressEnd);
                            if (_lastProgress != percentage) {
                                _trackerManager.updateProgress(_diskImage.getUuid(), percentage);
                                _trackerManager.updateProgress(_task.getUuid(), percentage);
                                _lastProgress = percentage;
                            }
                        }
                    }));
            _eventBusService.post(event);
        }

        @Override
        public void setResult(Object result) {
            _vmxPath = (String) result;
        }
    }

    private class InstantiateVmState implements Workflow.StateRunnable {
        private final int _progressStart;

        InstantiateVmState(int progressStart) {
            _progressStart = progressStart;
        }

        @Override
        public void run(AsyncCallback callback) {
            LOGGER.info(String.format("InstantiateVmState.run() - vmx: %s", _vmxPath));

            Pool pool = _poolService.getByUuid(_poolUuid);
            if (pool == null || Pool.Status.Deleting == pool.getStatus()) {
                LOGGER.error(String.format("Unable to find Pool id=\"%s\" in db", _poolUuid));
                if (callback != null) {
                    callback.onFailure(new Exception("No valid desktop pool"));
                    return;
                }
            }

            _task.setDetails(String.format("Instantiating image \"%s\"", _diskImage.getName()));
            _taskService.commit(_task);

            _trackerManager.updateProgress(_diskImage.getUuid(), _progressStart, null, "Processing...");
            _trackerManager.updateProgress(_task.getUuid(), _progressStart, null, "Processing...");

            InstantiateVmEvent event = new InstantiateVmEvent(_hypervisorContext, _localHost.getRefId(),
                    _diskImage.getName(), _vmxPath, true, pool.getCpuCount(), pool.getMemoryMB(), pool.isReserveMemory(),
                    null, false, _vcHypervisorContext, _localHost.getEsxUuid(), callback);
            _eventBusService.post(event);
        }

        @Override
        public void setResult(Object result) {
            InstantiatedVm instantiatedVm = (InstantiatedVm) result;
            _vmRefId = instantiatedVm.getRefId();

            DiskImage diskImage = _diskImageService.getByUuid(_diskImage.getUuid());
            if (diskImage == null
                    || DiskImage.Status.Deleting == diskImage.getStatus()) {
                LOGGER.error(String.format("Unable to find DiskImage id=\"%s\" in db", _diskImage.getUuid()));
                throw new RuntimeException("Can't locate the entity");
            }

            diskImage.setVmRefId(_vmRefId);
            _diskImageService.commit(diskImage);
        }
    }

    private class SysprepVmState implements Workflow.StateRunnable,
            ObjectPropertyChangeListener, VmStateListener {
        private final int _progressStart;
        @SuppressWarnings("unused")
        private final int _progressEnd;

        private AsyncCallback _callback;
        private boolean _done = false;

        SysprepVmState(int progressStart, int progressEnd) {
            _progressStart = progressStart;
            _progressEnd = progressEnd;
        }

        @Override
        public void run(AsyncCallback callback) {
            LOGGER.info(String.format("SysprepVmState.run() - vm: %s", _diskImage.getName()));

            _task.setDetails(String.format("Preparing image vm \"%s\"", _diskImage.getName()));
            _taskService.commit(_task);

            _trackerManager.updateProgress(_diskImage.getUuid(), _progressStart, null, "Preparing image...");
            _trackerManager.updateProgress(_task.getUuid(), _progressStart, null, "Preparing image...");

            _callback = callback;

            addListeners();
        }

        private void addListeners() {
            // Start listening on VM state
            AddRemoveVmStateListenerEvent listenerEvent =
                    new AddRemoveVmStateListenerEvent(true, this);
            _eventBusService.post(listenerEvent);

            // Start listening on DiskImage Status change
            _diskImageService.addObjectPropertyChangeListener(this);

            REFERENCES.add(this);
        }

        private synchronized void removeListeners() {
            REFERENCES.remove(this);

            // Stop listening on DiskImage Status change
            _diskImageService.removeObjectPropertyChangeListener(this);

            // Stop listening on VM state
            AddRemoveVmStateListenerEvent listenerEvent =
                    new AddRemoveVmStateListenerEvent(false, this);
            _eventBusService.post(listenerEvent);
        }

        @Override
        public void setResult(Object result) {
        }

        @Override
        public void newObject(ObjectPropertyChangeEvent event) {
        }

        @Override
        public void deleteObject(ObjectPropertyChangeEvent event) {
            final DiskImage diskImage = (DiskImage) event.getSource();
            if (StringUtils.equals(_diskImage.getUuid(), diskImage.getUuid())) {
                removeListeners();

                synchronized(this) {
                    if (_done) {
                        return;
                    }
                    _done = true;
                }

                // The VmImage & DiskImage has been removed - fail this flow
                if (_callback != null) {
                    _callback.onFailure(new Exception("This image has been deleted"));
                }
            }
        }

        @Override
        public void propertyChange(ObjectPropertyChangeEvent event) {
            final DiskImage diskImage = (DiskImage) event.getSource();
            if (StringUtils.equals(_diskImage.getUuid(), diskImage.getUuid())) {
                switch (diskImage.getStatus()) {
                case Prepared:
                    // Wait for vm to be powered off before the next step.
                    break;

                case Error:
                    removeListeners();

                    synchronized(this) {
                        if (_done) {
                            return;
                        }
                        _done = true;
                    }

                    // The Sysprep'ing has failed - fail this flow
                    if (_callback != null) {
                        _callback.onFailure(new Exception("Failed to preparing image VM"));
                    }
                    break;

                default:
                    break;
                }
            }
        }

        @Override
        public void onNew(String vmRefId, State state) {
        }

        @Override
        public void onDelete(String vmRefId) {
            if (!_vmRefId.equals(vmRefId)) {
                return;
            }

            removeListeners();

            synchronized(this) {
                if (_done) {
                    return;
                }
                _done = true;
            }

            // The Sysprep'ing has failed - fail this flow
            if (_callback != null) {
                _callback.onFailure(new Exception("Failed to preparing image VM - VM has been deleted"));
            }
        }

        @Override
        public void onPowerStateChange(String vmRefId, VmStateListener.State state) {
            if (!_vmRefId.equals(vmRefId)) {
                return;
            }

            if (state == VmStateListener.State.PoweredOff) {
                removeListeners();

                synchronized(this) {
                    if (_done) {
                        return;
                    }
                    _done = true;
                }

                Pool pool = _poolService.getByUuid(_poolUuid);
                if (pool == null) {
                    LOGGER.error(String.format("Unable to find Pool id=\"%s\" in db", _poolUuid));
                    if (_callback != null) {
                        _callback.onFailure(new Exception("No valid desktop pool"));
                        return;
                    }
                } else {
                    if (Pool.Status.Deleting == pool.getStatus()) {
                        LOGGER.warn(String.format("Pool \"%s\" is being deleted", pool.getName()));
                        if (_callback != null) {
                            _callback.onFailure(new Exception("Pool is being deleted"));
                            return;
                        }
                    }
                }

                if (_callback != null) {
                    _callback.onSuccess(null);
                }
            }
         }
    }

    private class TemplatizeVmState implements Workflow.StateRunnable {
        private final int _progressStart;

        TemplatizeVmState(int progressStart) {
            _progressStart = progressStart;
        }

        @Override
        public void run(AsyncCallback callback) {
            LOGGER.info(String.format("TemplatizeVmState.run() - vm: %s", _diskImage.getName()));

            _task.setDetails(String.format("Templatizing vm \"%s\"", _diskImage.getName()));
            _taskService.commit(_task);

            _trackerManager.updateProgress(_diskImage.getUuid(), _progressStart, null, "Converting to image...");
            _trackerManager.updateProgress(_task.getUuid(), _progressStart, null, "Converting to image...");

            TemplatizeVmEvent event = new TemplatizeVmEvent(_hypervisorContext, _vmRefId, _diskImage.getGuid(),
                    _localHost.isCbrcEnabled(), _vcHypervisorContext, _localHost.getEsxUuid(), callback);
            _eventBusService.post(event);
        }

        @Override
        public void setResult(Object result) {
            TemplatizedVm templatizedVm = (TemplatizedVm) result;
            _vmCbrcEnabled = templatizedVm.isCbrcEnabled();
        }
    }
}

