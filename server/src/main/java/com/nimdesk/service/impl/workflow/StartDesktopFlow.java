/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.service.impl.workflow;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.nimdesk.database.vo.VmInstanceVO;
import com.nimdesk.event.AsyncCallback;
import com.nimdesk.event.AttachVmDiskEvent;
import com.nimdesk.event.ConnectHostEvent;
import com.nimdesk.event.DetachVmAllUserDisksEvent;
import com.nimdesk.event.InstantiateVmEvent;
import com.nimdesk.event.Link2LocalReplicaEvent;
import com.nimdesk.event.PowerOpVmEvent;
import com.nimdesk.hypervisor.HypervisorContext;
import com.nimdesk.model.DiskImage;
import com.nimdesk.model.Host;
import com.nimdesk.model.Pool;
import com.nimdesk.model.Server;
import com.nimdesk.model.Storage;
import com.nimdesk.model.Task;
import com.nimdesk.model.User;
import com.nimdesk.model.VmInstance;
import com.nimdesk.service.EventBusService;
import com.nimdesk.service.ObjectPropertyChangeEvent;
import com.nimdesk.service.ObjectPropertyChangeListener;
import com.nimdesk.service.Scheduler;
import com.nimdesk.service.Workflow;
import com.nimdesk.service.impl.ServiceLocator;
import com.nimdesk.util.Pair;
import com.nimdesk.vm.InstantiatedVm;

public class StartDesktopFlow extends Workflow {
    private static final Logger LOGGER = Logger.getLogger(StartDesktopFlow.class);

    private static final int STARTING_DESKTOP_CONCURRENCY = 10;

    private static final Object SERIALIZATION_LOCK = new Object();

    // Strong references to keep weak referenced StateRunnable (such as SanityCheckState) in memory.
    private static final List<SanityCheckState> _pendingStarts = new LinkedList<SanityCheckState>();
    private static ObjectPropertyChangeListener _vmInstanceChangeListener = null;
    private static final Map<String, Long> _startingDesktopUuids = new HashMap<String, Long>();
    private static Scheduler.ScheduleHandle _checkVmSchedule = null;

    private static final long VM_START_EXPIRE_PERIOD = 5 * 60 * 1000; // 5 min

    private final Scheduler _scheduler;
    private final EventBusService _eventBusService;
    private final Host.Service _hostService;
    private final Storage.Service _storageService;
    private final DiskImage.Service _diskImageService;
    private final VmInstance.Service _vmInstanceService;
    private final User.Service _userService;

    private final Server _localServer;
    private final Host _localHost;
    private final Pool _pool;
    private final VmInstance _vmInstance;
    private DiskImage _baseDiskImage = null;
    private User _user = null;

    // ConnectHostState result
    private HypervisorContext _hypervisorContext;

    // ConnectVcState result
    private HypervisorContext _vcHypervisorContext = null;

    // Relink2LocalReplicaState result
    private String _vmxPath;

    // InstantiateVmState result
    private String _vmRefId = null;
    private String _vmMacAddress;

    // AttachUserDataDiskState
    private String _uddOwner;

    public StartDesktopFlow(Host localHost, Pool pool, VmInstance vmInstance, final AsyncCallback callback) {
        super();

        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("StartDesktopFlow()");
        }

        ServiceLocator serviceLocator = ServiceLocator.getInstance();

        _scheduler = serviceLocator.getScheduler();
        _eventBusService = serviceLocator.getEventBusService();
        _hostService = serviceLocator.getHostService();
        _storageService = serviceLocator.getStorageService();
        _diskImageService = serviceLocator.getDiskImageService();
        _vmInstanceService = serviceLocator.getVmInstanceService();
        _userService = serviceLocator.getUserService();

        Server.Service serverService = serviceLocator.getServerService();
        _localServer = serverService.getLocalServer();
        if (_localServer == null) {
            throw new RuntimeException(String.format("Unable to find host to start desktop for pool \"%s\"", pool.getName()));
        }

        if (localHost != null) {
            _localHost = localHost;
        } else {
            _localHost = _hostService.getLocalHost();
        }
        if (_localHost == null) {
            throw new RuntimeException(String.format("Unable to find host to start desktop for pool \"%s\"", pool.getName()));
        }

        _pool = pool;

        _vmInstance = vmInstance;
        _uddOwner = vmInstance.getUserDataDiskOwner();
        /*_task = */createTask(_vmInstance, _pool, (callback != null) ? callback.getRequestor() : null);

        append(new SanityCheckState(), null);
        append(new ConnectHostState(), null);
        if (_pool.getType() == Pool.Type.Persistent
                && Storage.SHARED_STORAGE_UUID.equals(_vmInstance.getStorageUuid())) {
            if (StringUtils.isEmpty(_localHost.getSharedDatastoreRefId())) {
                throw new RuntimeException(String.format("Unable to start persistent desktop \"%s\" on the local host - no shared storage", _vmInstance.getName()));
            }

            // Check if we need to relink to this host.
            DiskImage oldDiskImage = _diskImageService.getByUuid(_vmInstance.getDiskImageUuid());
            if (!_diskImageService.isLocalDiskImage(oldDiskImage)) {
                _baseDiskImage = _diskImageService.getLocalDiskImageByGuid(oldDiskImage.getGuid());
                if (_baseDiskImage == null) {
                    throw new RuntimeException(String.format("Unable to start desktop \"%s\" on the local host", _vmInstance.getName()));
                }

                if (!StringUtils.isEmpty(_localHost.getVCenterIp())
                        && !StringUtils.isEmpty(_localHost.getVCenterUsername())
                        && !StringUtils.isEmpty(_localHost.getVCenterPassword())) {
                    append(new ConnectVCenterState(), null);
                }

                // Need to relink to this host.
                append(new Relink2LocalReplicaState(), null);
                append(new InstantiateVmState(), null);
            }

            if (_pool.getUserDataDiskSizeInMB() > 0
                    && !StringUtils.isEmpty(vmInstance.getUserUuid())
                    && !StringUtils.equals(vmInstance.getUserUuid(), vmInstance.getUserDataDiskOwner())) {
                append(new ShutdownVmState(), null);
                append(new DetachUserDataDiskState(), null);

                _user = _userService.getByUuid(vmInstance.getUserUuid());
                if (_user != null && User.Type.User == _user.getType()) {
                    append(new AttachUserDataDiskState(), null);
                }
            }
        }
        append(new PowerOnVmState(), null);

        setCallback(new AsyncCallback() {
            @Override
            public void onSuccess(Object result) {
                disconnectHost();

                VmInstance vmInstance = _vmInstanceService.getByUuid(_vmInstance.getUuid());
                if (vmInstance == null
                        || VmInstance.Status.Destroying == vmInstance.getStatus()
                        || VmInstance.Status.Destroyed == vmInstance.getStatus()) {
                    LOGGER.error(String.format("Unable to find VmInstance id=\"%s\" in db", _vmInstance.getUuid()));
                    throw new RuntimeException("Can't locate the entity");
                }

                boolean modified = false;

                if (!StringUtils.equals(_uddOwner, vmInstance.getUserDataDiskOwner())) {
                    vmInstance.setAssigned(false); // Let the agent handle the new disk.
                    vmInstance.setUserDataDiskOwner(_uddOwner);
                    modified = true;
                }
                if (!StringUtils.isEmpty(_vmRefId)) {
                    vmInstance.setDiskImageUuid(_baseDiskImage.getUuid());
                    vmInstance.setVmRefId(_vmRefId);
                    vmInstance.setMacAddress(_vmMacAddress);
                    modified = true;
                }
                if (modified) {
                    _vmInstanceService.commit(vmInstance);
                }

                LOGGER.info(String.format("StartDesktopFlow: Started desktop \"%s\" from pool \"%s\"",
                        _vmInstance.getName(), _pool.getName()));

                if (callback != null) {
                    callback.onSuccess(result);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                disconnectHost();

                // If failed to start, refresh it.
                VmInstance vmInstance = _vmInstanceService.getByUuid(_vmInstance.getUuid());
                if (vmInstance == null
                        || VmInstance.Status.Destroying == vmInstance.getStatus()
                        || VmInstance.Status.Destroyed == vmInstance.getStatus()) {
                    LOGGER.error(String.format("Unable to find VmInstance id=\"%s\" in db", _vmInstance.getUuid()));
                } else {
                    vmInstance.setUserDataDiskOwner(_uddOwner);

                    switch (_pool.getType()) {
                    case Persistent:
                        vmInstance.setStatus(VmInstance.Status.Error);
                        vmInstance.setLastError(VmInstance.Error.PowerOnError);
                        vmInstance.setUpdateTime(System.currentTimeMillis());
                        _vmInstanceService.commit(vmInstance);
                        break;

                    default:
                        _vmInstanceService.refreshVm(_pool, vmInstance, null);
                        break;
                    }
                }

                LOGGER.error(String.format("StartDesktopFlow: Failed to start desktop \"%s\" from pool \"%s\" - %s",
                        _vmInstance.getName(), _pool.getName(), t.getMessage()), t);

                if (callback != null) {
                    callback.onFailure(t);
                }
            }

            @Override
            public void progress(int percentage) {
                if (callback != null) {
                    callback.progress(percentage);
                }
            }

            @Override
            public String getRequestor() {
                return (callback != null) ? callback.getRequestor() : null;
            }
        });
    }

    private void disconnectHost() {
        if (_hypervisorContext != null) {
            _hypervisorContext.disconnect();
            _hypervisorContext = null;
        }
        if (_vcHypervisorContext != null) {
            _vcHypervisorContext.disconnect();
            _vcHypervisorContext = null;
        }
    }

    private Task createTask(VmInstance vmInstance, Pool pool, String requestor) {
        if (vmInstance instanceof VmInstanceVO) {
            // Suppressing PoolManager to start another vmInstance starting.
            ((VmInstanceVO) vmInstance).inOperation(true);
        }
        if (VmInstance.Status.Starting != vmInstance.getStatus()) {
            vmInstance.setStatus(VmInstance.Status.Starting);
            vmInstance.setUpdateTime(System.currentTimeMillis());
            _vmInstanceService.commit(_vmInstance);
        }

        return null;
    }

    private class SanityCheckState implements Workflow.StateRunnable {
        private String _startingDesktopUuid = null;
        private AsyncCallback _callback = null;

        @Override
        public void run(AsyncCallback callback) {
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace("SanityCheckState.run()");
            }

            _startingDesktopUuid = _vmInstance.getUuid();

            try {
                synchronized (SERIALIZATION_LOCK) {
                    int startingCount = _startingDesktopUuids.size();
                    if (startingCount >= STARTING_DESKTOP_CONCURRENCY) {
                        LOGGER.info(String.format("Pool \"%s\" (%s) has %d desktops in starting - more than limit %d",
                                _pool.getName(), _pool.getUuid(), startingCount, STARTING_DESKTOP_CONCURRENCY));

                        _callback = callback;

                        _pendingStarts.add(this);

                        if (_vmInstanceChangeListener == null) {
                            _vmInstanceChangeListener = new VmInstanceChangeListenerImpl();

                            // Start listening on VmInstance Status change
                            _vmInstanceService.addObjectPropertyChangeListener(_vmInstanceChangeListener);
                        }

                        return;
                    } else {
                        VmInstance vmInstance = _vmInstanceService.getByUuid(_startingDesktopUuid);
                        if (vmInstance == null
                                || VmInstance.Status.Destroying == vmInstance.getStatus()
                                || VmInstance.Status.Destroyed == vmInstance.getStatus()) {
                            LOGGER.error(String.format("Unable to find VmInstance id=\"%s\" in db", _startingDesktopUuid));
                            throw new RuntimeException("Can't locate the entity");
                        }

                        if (_vmInstanceChangeListener == null) {
                            _vmInstanceChangeListener = new VmInstanceChangeListenerImpl();

                            // Start listening on VmInstance Status change
                            _vmInstanceService.addObjectPropertyChangeListener(_vmInstanceChangeListener);
                        }

                        // Add uuid to _startingDesktopUuids to throttle desktop starting.
                        _startingDesktopUuids.put(_startingDesktopUuid, System.currentTimeMillis());

                        if (_checkVmSchedule == null) {
                            _checkVmSchedule = _scheduler.scheduleWithFixedDelay(new Runnable() {
                                @Override
                                public void run() {
                                    List<String> expiredVms = new ArrayList<String>();

                                    long currentTime = System.currentTimeMillis();
                                    synchronized (SERIALIZATION_LOCK) {
                                        for (Map.Entry<String, Long> entry : _startingDesktopUuids.entrySet()) {
                                            if (currentTime - entry.getValue() > VM_START_EXPIRE_PERIOD) {
                                                expiredVms.add(entry.getKey());
                                            }
                                        }
                                    }

                                    for (String vmInstanceUuid : expiredVms) {
                                        startNextLocalDesktop(vmInstanceUuid);
                                    }
                                }
                            }, 60L, 60L); // Interval at 1 min
                        }
                    }
                }

                if (callback != null) {
                    callback.onSuccess(null);
                }
            } catch (Exception e) {
                if (callback != null) {
                    callback.onFailure(e);
                }
            }
        }

        @Override
        public void setResult(Object result) {
        }
    }

    private class VmInstanceChangeListenerImpl implements ObjectPropertyChangeListener {
        @Override
        public void newObject(ObjectPropertyChangeEvent event) {
        }

        @Override
        public void deleteObject(ObjectPropertyChangeEvent event) {
            final VmInstance vmInstance = (VmInstance) event.getSource();
            if (!_vmInstanceService.isLocalVmInstance(vmInstance)) {
                // Not local vmInstance.
                return;
            }

            if (VmInstance.Status.Starting == vmInstance.getStatus()) {
                startNextLocalDesktop(vmInstance.getUuid());
            }
        }

        @Override
        public void propertyChange(ObjectPropertyChangeEvent event) {
            final VmInstance vmInstance = (VmInstance) event.getSource();
            if (!_vmInstanceService.isLocalVmInstance(vmInstance)) {
                // Not local vmInstance.
                return;
            }

            final Map<String, Pair<Object, Object>> changedProperties = event.getChangedProperties();
            if (changedProperties != null) {
                Pair<Object, Object> statuses = changedProperties.get(VmInstance.PROP_STATUS);
                if (statuses != null && VmInstance.Status.Starting == statuses.getFirst()) {
                    startNextLocalDesktop(vmInstance.getUuid());
                }
            }
        }
    }

    private void startNextLocalDesktop(String completedVmInstanceUuid) {
        SanityCheckState state = null;

        synchronized (SERIALIZATION_LOCK) {
            _startingDesktopUuids.remove(completedVmInstanceUuid);

            if (_startingDesktopUuids.size() >= STARTING_DESKTOP_CONCURRENCY) {
                // Still more than STARTING_DESKTOP_CONCURRENCY desktops starting.
                return;
            }

            if (!_pendingStarts.isEmpty()) {
                state = _pendingStarts.remove(0);
            }

            if (state != null && !StringUtils.isEmpty(state._startingDesktopUuid)) {
                _startingDesktopUuids.put(state._startingDesktopUuid, System.currentTimeMillis());
            }

            // If no more pending starting, remove listener.
            if (_pendingStarts.isEmpty() && _startingDesktopUuids.isEmpty()) {
                // Stop listening on VmInstance Status change
                _vmInstanceService.removeObjectPropertyChangeListener(_vmInstanceChangeListener);
                _vmInstanceChangeListener = null;

                if (_checkVmSchedule != null) {
                    _checkVmSchedule.cancel();
                    _checkVmSchedule = null;
                }
            }
        }

        if (state != null) {
            if (state._callback != null) {
                VmInstance vmInstance = _vmInstanceService.getByUuid(state._startingDesktopUuid);
                if (vmInstance == null
                        || VmInstance.Status.Destroying == vmInstance.getStatus()
                        || VmInstance.Status.Destroyed == vmInstance.getStatus()) {
                    state._callback.onFailure(new Exception("Can't locate the entity"));
                } else {
                    state._callback.onSuccess(null);
                }
            }
        }
    }

    private class ConnectHostState implements Workflow.StateRunnable {
        @Override
        public void run(AsyncCallback callback) {
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace("ConnectHostState.run()");
            }

            ConnectHostEvent event = new ConnectHostEvent(_localHost.getAddress(), 0,
                    _localHost.getUsername(), _localHost.getPassword(), callback);
            _eventBusService.post(event);
        }

        @Override
        public void setResult(Object result) {
            _hypervisorContext = (HypervisorContext) result;
        }
    }

    private class ConnectVCenterState implements Workflow.StateRunnable {
        @Override
        public void run(AsyncCallback callback) {
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace("ConnectVCenterState.run()");
            }

            String vCenterAddress = _localHost.getVCenterAddress();
            if (StringUtils.isEmpty(vCenterAddress)) {
                vCenterAddress = _localHost.getVCenterIp();
            }

            ConnectHostEvent event = new ConnectHostEvent(vCenterAddress, 0,
                    _localHost.getVCenterUsername(), _localHost.getVCenterPassword(), callback);
            _eventBusService.post(event);
        }

        @Override
        public void setResult(Object result) {
            _vcHypervisorContext = (HypervisorContext) result;
        }
    }

    private class Relink2LocalReplicaState implements Workflow.StateRunnable {
        @Override
        public void run(AsyncCallback callback) {
            LOGGER.info(String.format("Relink2LocalReplicaState.run() - vmInstance: %s", _vmInstance.getName()));

            VmInstance vmInstance = _vmInstanceService.getByUuid(_vmInstance.getUuid());
            if (vmInstance == null
                    || VmInstance.Status.Destroying == vmInstance.getStatus()
                    || VmInstance.Status.Destroyed == vmInstance.getStatus()) {
                LOGGER.error(String.format("Unable to find VmInstance id=\"%s\" in db", _vmInstance.getUuid()));
                throw new RuntimeException("Can't locate the entity");
            }

            Storage baseStorage = _storageService.getByUuid(_baseDiskImage.getStorageUuid());
            Storage cloneStorage = _storageService.getSharedStorage();

            Map<String, String> customProps = new HashMap<String, String>();
            customProps.put("guestinfo.nimdesk.type", "desktop");
            customProps.put("guestinfo.nimdesk.iid", _vmInstance.getUuid());
            customProps.put("guestinfo.nimdesk.serverUrl",
                    String.format("http://%s:8080/", _localServer.getPrivateAddress()));

            Link2LocalReplicaEvent event = new Link2LocalReplicaEvent(_hypervisorContext, _baseDiskImage,
                    baseStorage, _pool.getName(), _vmInstance.getName(), cloneStorage, _localHost.getVmNetwork(), customProps, callback);
            _eventBusService.post(event);
        }

        @Override
        public void setResult(Object result) {
            _vmxPath = (String) result;
        }
    }

    private class InstantiateVmState implements Workflow.StateRunnable {
        @Override
        public void run(AsyncCallback callback) {
            LOGGER.info(String.format("InstantiateVmState.run() - vmx: %s", _vmxPath));

            VmInstance vmInstance = _vmInstanceService.getByUuid(_vmInstance.getUuid());
            if (vmInstance == null
                    || VmInstance.Status.Destroying == vmInstance.getStatus()
                    || VmInstance.Status.Destroyed == vmInstance.getStatus()) {
                LOGGER.error(String.format("Unable to find VmInstance id=\"%s\" in db", _vmInstance.getUuid()));
                throw new RuntimeException("Can't locate the entity");
            }

            InstantiateVmEvent event = new InstantiateVmEvent(_hypervisorContext, _localHost.getRefId(),
                    _vmInstance.getName(), _vmxPath, true, 0, 0, false, vmInstance.getMacAddress(), false/*_localHost.isCbrcEnabled()*/,
                    _vcHypervisorContext, _localHost.getEsxUuid(), callback);
            _eventBusService.post(event);
        }

        @Override
        public void setResult(Object result) {
            InstantiatedVm instantiatedVm = (InstantiatedVm) result;

            _vmRefId = instantiatedVm.getRefId();
            _vmMacAddress = instantiatedVm.getMacAddress();
        }
    }

    private class ShutdownVmState implements Workflow.StateRunnable {
        @Override
        public void run(AsyncCallback callback) {
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace("ShutdownVmState.run()");
            }

            PowerOpVmEvent event = new PowerOpVmEvent(_hypervisorContext,
                    _vmRefId, PowerOpVmEvent.PowerOps.PowerOff, callback);;
            _eventBusService.post(event);
        }

        @Override
        public void setResult(Object result) {
        }
    }

    private class DetachUserDataDiskState implements Workflow.StateRunnable {
        @Override
        public void run(AsyncCallback callback) {
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace("DetachUserDataDiskState.run()");
            }

            VmInstance vmInstance = _vmInstanceService.getByUuid(_vmInstance.getUuid());
            if (vmInstance == null
                    || VmInstance.Status.Destroying == vmInstance.getStatus()
                    || VmInstance.Status.Destroyed == vmInstance.getStatus()) {
                LOGGER.error(String.format("Unable to find VmInstance id=\"%s\" in db", _vmInstance.getUuid()));
                throw new RuntimeException("Can't locate the entity");
            }

            Storage storage = _storageService.getSharedStorage();
            if (storage == null) {
                LOGGER.error(String.format("Unable to attach UDD for vmInstance \"%s\"", _vmInstance.getName()));
                throw new RuntimeException("No shared storage on the host");
            }

            String vmRefId = _vmRefId;
            if (StringUtils.isEmpty(vmRefId)) {
                vmRefId = vmInstance.getVmRefId();
            }

            DetachVmAllUserDisksEvent event = new DetachVmAllUserDisksEvent(_hypervisorContext, vmRefId, storage, callback);
            _eventBusService.post(event);
        }

        @Override
        public void setResult(Object result) {
            _uddOwner = null;
        }
    }

    private class AttachUserDataDiskState implements Workflow.StateRunnable {
        @Override
        public void run(AsyncCallback callback) {
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace("AttachUserDataDiskState.run()");
            }

            VmInstance vmInstance = _vmInstanceService.getByUuid(_vmInstance.getUuid());
            if (vmInstance == null
                    || VmInstance.Status.Destroying == vmInstance.getStatus()
                    || VmInstance.Status.Destroyed == vmInstance.getStatus()) {
                LOGGER.error(String.format("Unable to find VmInstance id=\"%s\" in db", _vmInstance.getUuid()));
                throw new RuntimeException("Can't locate the entity");
            }

            Storage storage = _storageService.getSharedStorage();
            if (storage == null) {
                LOGGER.error(String.format("Unable to attach UDD for vmInstance \"%s\"", _vmInstance.getName()));
                throw new RuntimeException("No shared storage on the host");
            }

            String vmRefId = _vmRefId;
            if (StringUtils.isEmpty(vmRefId)) {
                vmRefId = vmInstance.getVmRefId();
            }

            AttachVmDiskEvent event = new AttachVmDiskEvent(_hypervisorContext, vmRefId, _pool.getUserDataDiskSizeInMB(),
                    _user, storage, _pool.getName(), callback);
            _eventBusService.post(event);
        }

        @Override
        public void setResult(Object result) {
            if (result instanceof Boolean && ((Boolean) result).booleanValue()) {
                _uddOwner = _user.getUuid();
            }
        }
    }

    private class PowerOnVmState implements Workflow.StateRunnable {
        @Override
        public void run(AsyncCallback callback) {
            LOGGER.info(String.format("PowerOnVmState.run() - vmInstance: %s", _vmInstance.getName()));

            PowerOpVmEvent event = new PowerOpVmEvent(_hypervisorContext,
                    _vmInstance.getVmRefId(), PowerOpVmEvent.PowerOps.PowerOn, callback);;
            _eventBusService.post(event);
        }

        @Override
        public void setResult(Object result) {
        }
    }
}
