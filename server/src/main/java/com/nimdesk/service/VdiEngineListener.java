/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.nimdesk.Version;
import com.nimdesk.event.JoinClusterEvent;
import com.nimdesk.event.StartHostMonitorEvent;
import com.nimdesk.model.Application;
import com.nimdesk.model.DiskImage;
import com.nimdesk.model.Host;
import com.nimdesk.model.Server;
import com.nimdesk.model.Task;
import com.nimdesk.model.UserSession;
import com.nimdesk.model.VmImage;
import com.nimdesk.model.VmInstance;

public class VdiEngineListener implements ServletContextListener {
    private static final Logger LOGGER = Logger.getLogger(VdiEngineListener.class);

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        LOGGER.info(String.format("Starting Nimdesk server: %d.%d.%d", Version.PRODUCT_VERSION_MAJOR, Version.PRODUCT_VERSION_MINOR, Version.PRODUCT_VERSION_BUILD));

        ServletContext sc = sce.getServletContext();
        WebApplicationContext context = WebApplicationContextUtils.getRequiredWebApplicationContext(sc);

        SystemChecker systemChecker = context.getBean(SystemChecker.BEAN_NAME, SystemChecker.class);
        systemChecker.fastCheck();

        Server.Service serverService = context.getBean(Server.Service.BEAN_NAME, Server.Service.class);
        Host.Service hostService = context.getBean(Host.Service.BEAN_NAME, Host.Service.class);
        DiskImage.Service diskImageService = context.getBean(DiskImage.Service.BEAN_NAME, DiskImage.Service.class);
        VmInstance.Service vmInstanceService = context.getBean(VmInstance.Service.BEAN_NAME, VmInstance.Service.class);
        Task.Service taskService = context.getBean(Task.Service.BEAN_NAME, Task.Service.class);
        UserSession.Service userSessionService = context.getBean(UserSession.Service.BEAN_NAME, UserSession.Service.class);

        serverService.invalidateNonLocalServers();
        hostService.invalidateNonLocalHosts();
        diskImageService.invalidateNonLocalDiskImages();
        vmInstanceService.invalidateNonLocalVmInstances();
        taskService.invalidateLocalInProgressTasks();
        userSessionService.invalidateLocalActiveSessions();

        Server localServer = serverService.getLocalServer();

        EventBusService eventBusService = context.getBean(EventBusService.BEAN_NAME, EventBusService.class);

        Collection<? extends Host> hosts = hostService.getLocalHosts();
        ArrayList<Host> localHosts = new ArrayList<Host>();

        if (hosts != null) {
            // hosts must be synchronized - hostService.commit would modify it.
            synchronized(hosts) {
                for (Host host : hosts) {
                    localHosts.add(host);
                }
            }

            for (Host host : localHosts) {
                if (Host.Status.Connecting != host.getStatus()) {
                    host.setStatus(Host.Status.Connecting);
                    hostService.commit(host);
                }

                if (localServer != null) {
                    StartHostMonitorEvent event = new StartHostMonitorEvent(localServer, host, null);
                    eventBusService.post(event);
                }
            }
        }

        if (localServer != null) {
            // Update current version if needed.
            String curVersion = String.format("%d.%d.%d", Version.PRODUCT_VERSION_MAJOR, Version.PRODUCT_VERSION_MINOR, Version.PRODUCT_VERSION_BUILD);
            if (!StringUtils.equals(curVersion, localServer.getVersion())) {
                localServer.setVersion(curVersion);
                serverService.commit(localServer);
            }

            if (Server.Status.Disjoined != localServer.getStatus()) {
                List<? extends Server> servers = serverService.getAll(null);
                List<String> otherServers = new ArrayList<String>();
                for (Server server : servers) {
                    if (!server.isLocal() && Server.Status.Disjoined != server.getStatus()) {
                        otherServers.add(server.getPublicAddress());
                    }
                }

                JoinClusterEvent event = new JoinClusterEvent(localServer.getClusterName(), localServer.getClusterId(),
                        localServer.getPublicAddress(), otherServers.toArray(new String[otherServers.size()]), null);
                eventBusService.post(event);
            }
        }

        systemChecker.start();
    }

    /**
     * Do nothing.
     */
    @Override
    public void contextDestroyed(ServletContextEvent sce) {
    }
}
