/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.vdi;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.nimdesk.cluster.RemoteProxyFactory;
import com.nimdesk.cluster.RemoteService;
import com.nimdesk.database.vo.VmImageVO;
import com.nimdesk.event.AsyncCallback;
import com.nimdesk.model.DiskImage;
import com.nimdesk.model.Host;
import com.nimdesk.model.Pool;
import com.nimdesk.model.Server;
import com.nimdesk.model.Storage;
import com.nimdesk.model.VmImage;
import com.nimdesk.model.VmInstance;
import com.nimdesk.service.AsyncExecutor;
import com.nimdesk.service.ObjectPropertyChangeEvent;
import com.nimdesk.service.ObjectPropertyChangeListener;
import com.nimdesk.service.Scheduler;
import com.nimdesk.service.impl.ServiceLocator;
import com.nimdesk.service.impl.workflow.CopyImageFromPeerFlow;
import com.nimdesk.service.impl.workflow.DeleteDiskImageFlow;
import com.nimdesk.util.Pair;

public class ImageManager {
    private static final Logger LOGGER = Logger.getLogger(ImageManager.class);

    private static final int DISKIMAGE_COPY_LIMIT = 1;

    // Current active disk image copyings (diskImageUuid -> serverUuid)
    private final Map<String, String> _activeDiskImageCopying =
            Collections.synchronizedMap(new HashMap<String, String>());

    private final AsyncExecutor _asyncExecutor;
    private final RemoteProxyFactory _remoteProxyFactory;
    private final Scheduler _scheduler;

    private final ExecutorService _kickStartExecutor = Executors.newSingleThreadExecutor();

    private final Map<String, Scheduler.ScheduleHandle> _guidScheduleHandleMap =
            Collections.synchronizedMap(new HashMap<String, Scheduler.ScheduleHandle>());

    private final Set<String> _copyingDiskImageGuids =
            Collections.synchronizedSet(new HashSet<String>());

    private final Server.Service _serverService;
    private final Host.Service _hostService;
    private final Storage.Service _storageService;
    private final VmImage.Service _vmImageService;
    private final DiskImage.Service _diskImageService;
    private final Pool.Service _poolService;
    private final VmInstance.Service _vmInstanceService;

    private final Object _isClusterMasterLock = new Object();
    private String _masterAddress = "localhost";

    private final Object _addLocalDiskImageLock = new Object();

    // Must keep those listeners referenced here as they are added into a WeakHashMap.
    private final ObjectPropertyChangeListener _serverChangeListener = new ServerChangeListenerImpl();
    private final ObjectPropertyChangeListener _vmImageChangeListener = new VmImageChangeListenerImpl();
    private final ObjectPropertyChangeListener _diskImageChangeListener = new DiskImageChangeListenerImpl();

    public ImageManager(ServiceLocator serviceLocator) {
        _asyncExecutor = serviceLocator.getAsyncExecutor();
        _remoteProxyFactory = serviceLocator.getRemoteProxyFactory();
        _scheduler = serviceLocator.getScheduler();

        _serverService = serviceLocator.getServerService();
        _hostService = serviceLocator.getHostService();
        _storageService = serviceLocator.getStorageService();
        _vmImageService = serviceLocator.getVmImageService();
        _diskImageService = serviceLocator.getDiskImageService();
        _poolService = serviceLocator.getPoolService();
        _vmInstanceService = serviceLocator.getVmInstanceService();

        _serverService.addObjectPropertyChangeListener(_serverChangeListener);
        _vmImageService.addObjectPropertyChangeListener(_vmImageChangeListener);
        _diskImageService.addObjectPropertyChangeListener(_diskImageChangeListener);
    }

    public synchronized void start() {
        try {
            List<? extends DiskImage> deletingDiskImageList = _diskImageService.getLocalDiskImages(DiskImage.Status.Deleting);
            if (deletingDiskImageList != null) {
                LOGGER.info(String.format("Found %d deleting diskImages", deletingDiskImageList.size()));
                for (DiskImage diskImage : deletingDiskImageList) {
                    LOGGER.info(String.format("Disk Image \"%s\"(%s) is deleting", diskImage.getName(), diskImage.getUuid()));
                    deleteLocalDiskImage(diskImage);
                }
            }

            List<? extends VmImage> vmImageList = _vmImageService.getAll(null);
            if (vmImageList != null) {
                LOGGER.info(String.format("Found %d vmImages", vmImageList.size()));
                for (VmImage vmImage : vmImageList) {
                    LOGGER.info(String.format("Image \"%s\" status: \"%s\"", vmImage.getName(), vmImage.getStatus().toString()));
                    switch (vmImage.getStatus()) {
                    case Ready:
                        // Initiate copying.
                        DiskImage localDiskImage = _diskImageService.getLocalDiskImageByGuid(vmImage.getCurrentDiskImageGuid());
                        if (localDiskImage != null) {
                            LOGGER.info(String.format("Found local disk image for guid=%s", localDiskImage.getGuid()));
                            switch (localDiskImage.getStatus()) {
                            case Copying:
                            case Error:
                                // Re-initiate request copy
                                localDiskImage.setStatus(DiskImage.Status.RequestingCopy);
                                _diskImageService.commit(localDiskImage);
                                break;

                            case Ready:
                                kickStartPeerDiskImageCopy(localDiskImage);
                                break;
                            }
                        } else {
                            synchronized (_addLocalDiskImageLock) {
                                List<? extends DiskImage> diskImages = _diskImageService.getDiskImagesByGuid(vmImage.getCurrentDiskImageGuid());
                                if (diskImages == null || diskImages.isEmpty()) {
                                    LOGGER.warn(String.format("Unable to find disk images for guid=%s for image \"%s\"",
                                            vmImage.getCurrentDiskImageGuid(), vmImage.getName()));
                                } else {
                                    for (DiskImage diskImage : diskImages) {
                                        if (diskImage.getType().isReplicatable()
                                                && DiskImage.Status.Ready == diskImage.getStatus()) {
                                            createLocalDiskImage(diskImage);
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                        break;

                    case Importing:
                        DiskImage diskImage = _diskImageService.getByUuid(vmImage.getUpdatingDiskImageUuid());
                        if (diskImage != null && _diskImageService.isLocalDiskImage(diskImage)) {
                            _vmImageService.cancelUpdateVmImage(vmImage, null);
                        }
                        break;

                    case Deleting:
                        deleteVmImage(vmImage);
                        break;
                    }
                }
            }

            List<? extends Pool> poolList = _poolService.getAll(null);
            if (poolList != null) {
                LOGGER.info(String.format("Found %d pools", poolList.size()));
                for (Pool pool : poolList) {
                    if (Pool.Type.NonPersistent == pool.getType()) {
                        continue;
                    }

                    if (Pool.Status.Deleting == pool.getStatus()) {
                        continue;
                    }

                    if (StringUtils.isEmpty(pool.getBaseDiskImageGuid())) {
                        continue;
                    }

                    DiskImage localDiskImage = _diskImageService.getLocalDiskImageByGuid(pool.getBaseDiskImageGuid());
                    if (localDiskImage != null) {
                        LOGGER.info(String.format("Found local disk image for persistent pool=%s", pool.getName()));
                        switch (localDiskImage.getStatus()) {
                        case Copying:
                        case Error:
                            // Re-initiate request copy.
                            localDiskImage.setStatus(DiskImage.Status.RequestingCopy);
                            _diskImageService.commit(localDiskImage);
                            break;

                        case Ready:
                            kickStartPeerDiskImageCopy(localDiskImage);
                            break;
                        }
                    } else {
                        synchronized (_addLocalDiskImageLock) {
                            List<? extends DiskImage> diskImages = _diskImageService.getDiskImagesByGuid(pool.getBaseDiskImageGuid());
                            if (diskImages == null || diskImages.isEmpty()) {
                                LOGGER.warn(String.format("Unable to find disk images for guid=%s for pool \"%s\"",
                                        pool.getBaseDiskImageGuid(), pool.getName()));
                            } else {
                                for (DiskImage diskImage : diskImages) {
                                    if (diskImage.getType().isReplicatable()
                                            && DiskImage.Status.Ready == diskImage.getStatus()) {
                                        createLocalDiskImage(diskImage);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    break;
                }
            }

            List<? extends DiskImage> copyingDiskImages = _diskImageService.getLocalDiskImages(DiskImage.Status.Copying);
            if (copyingDiskImages != null) {
                for (DiskImage localDiskImage : copyingDiskImages) {
                    // Re-initiate request copy.
                    localDiskImage.setStatus(DiskImage.Status.RequestingCopy);
                    _diskImageService.commit(localDiskImage);
                }
            }
        } catch (Exception e) {
            // This will throw exception if the db hasn't been initialized.
            // It's okay to ignore the exception.
            LOGGER.warn("ImageManager start failed.", e);
        }
    }

    public synchronized void stop() {
    }

    public void setClusterMaster(boolean isMaster, String masterAddress) {
        synchronized(_isClusterMasterLock) {
            if (isMaster) {
                _masterAddress = null;
            } else {
                _masterAddress = masterAddress;
            }
        }
    }

    @SuppressWarnings("unused")
    private boolean isClusterMaster() {
        synchronized(_isClusterMasterLock) {
            return _masterAddress == null;
        }
    }

    @SuppressWarnings("unused")
    private String getClusterMaster() {
        synchronized(_isClusterMasterLock) {
            return _masterAddress;
        }
    }

    public boolean allowDiskImageCopy(String callerServerUuid, String diskImageUuid) {
        synchronized(_copyingDiskImageGuids) {
            if (_copyingDiskImageGuids.size() >= 1) {
                LOGGER.warn(String.format("Disk image copying is in progress..."));
                return false;
            }

            // 1. Find the peer disk image.
            DiskImage peerDiskImage = _diskImageService.getByUuid(diskImageUuid);
            if (peerDiskImage == null) {
                LOGGER.warn(String.format("Remote disk image for uuid \"%s\" is null", diskImageUuid));
                return false;
            }

            final String diskImageGuid = peerDiskImage.getGuid();

            // 2. Check if this disk image is being copied.
            if (_copyingDiskImageGuids.contains(diskImageGuid)) {
                LOGGER.warn(String.format("Local disk image for guid \"%s\" is being copied", diskImageGuid));
                return false;
            }

            // 3. Check if the disk image is still waiting for copy.
            DiskImage localDiskImage = _diskImageService.getLocalDiskImageByGuid(diskImageGuid);
            if (localDiskImage == null || DiskImage.Status.RequestingCopy != localDiskImage.getStatus()) {
                LOGGER.warn(String.format("Local disk image for guid \"%s\" is null or not requesting copy", diskImageGuid));
                return false;
            }

            // 4. Check if all ancestors are ready
            String parentGuid = localDiskImage.getParentGuid();
            while (!StringUtils.isEmpty(parentGuid)) {
                DiskImage localParentDiskImage = _diskImageService.getLocalDiskImageByGuid(parentGuid);
                if (localParentDiskImage != null
                        && DiskImage.Status.Ready == localParentDiskImage.getStatus()) {
                    parentGuid = localParentDiskImage.getParentGuid();
                    continue;
                } else {
                    LOGGER.warn(String.format("Local disk image for parent guid \"%s\" is not ready", parentGuid));
                    return false;
                }
            }

            // 5. Find the peer server and the peer host.
            Server peerServer = _serverService.getByUuid(callerServerUuid);
            if (peerServer == null) {
                LOGGER.warn(String.format("Unable to find peer server \"%s\"", callerServerUuid));
                return false;
            }

            List<? extends Host> peerHosts = _hostService.getHostsForServer(callerServerUuid);
            if (peerHosts == null || peerHosts.isEmpty()) {
                LOGGER.warn(String.format("Unable to find host for server \"%s\"", callerServerUuid));
                return false;
            }
            Host peerHost = peerHosts.get(0);

            // 6. Find the storage for the peer diskImage.
            Storage peerStorage = _storageService.getByUuid(peerDiskImage.getStorageUuid());
            if (peerStorage == null) {
                LOGGER.warn(String.format("Unable to find peer storage to copy image \"%s\"",
                        peerDiskImage.getName()));
                return false;
            }

            CopyImageFromPeerFlow wf = new CopyImageFromPeerFlow(peerServer, peerHost, peerStorage, peerDiskImage,
                    localDiskImage, new AsyncCallback() {
                        @Override
                        public void onSuccess(Object result) {
                            _copyingDiskImageGuids.remove(diskImageGuid);
                        }

                        @Override
                        public void onFailure(Throwable t) {
                            _copyingDiskImageGuids.remove(diskImageGuid);
                        }

                        @Override
                        public void progress(int percentage) {
                        }

                        @Override
                        public String getRequestor() {
                            return null;
                        }
            });

            _copyingDiskImageGuids.add(diskImageGuid);

            wf.run();
        }

        return true;
    }

    private void kickStartAllPeerDiskImageCopies() {
        List<? extends DiskImage> localDiskImages = _diskImageService.getLocalDiskImages();
        if (localDiskImages != null) {
            for (DiskImage localDiskImage : localDiskImages) {
                if (DiskImage.Status.Ready == localDiskImage.getStatus()
                        && localDiskImage.getType().isReplicatable()) {
                    kickStartPeerDiskImageCopy(localDiskImage);
                }
            }
        }
    }

    private void kickStartPeerDiskImageCopy(String diskImageGuid) {
        Scheduler.ScheduleHandle scheduleHandle = _guidScheduleHandleMap.remove(diskImageGuid);
        if (scheduleHandle != null) {
            try {
                scheduleHandle.cancel();
            } catch (Exception e) {
                // Ignore error.
            }
        }

        DiskImage localDiskImage = _diskImageService.getLocalDiskImageByGuid(diskImageGuid);
        if (localDiskImage == null || DiskImage.Status.Ready != localDiskImage.getStatus()) {
            LOGGER.info(String.format("Local disk image for guid \"%s\" is null or not ready", diskImageGuid));
            return;
        }

        kickStartPeerDiskImageCopy(localDiskImage);
    }

    private void kickStartPeerDiskImageCopy(final DiskImage localDiskImage) {
        Scheduler.ScheduleHandle scheduleHandle = _guidScheduleHandleMap.remove(localDiskImage.getGuid());
        if (scheduleHandle != null) {
            try {
                scheduleHandle.cancel();
            } catch (Exception e) {
                // Ignore error.
            }
        }

        _kickStartExecutor.execute(new Runnable() {
            @Override
            public void run() {
                synchronized(_activeDiskImageCopying) {
                    // We loosely maintain the limit.
                    int copyCount;
                    if ((copyCount = _activeDiskImageCopying.size()) >= DISKIMAGE_COPY_LIMIT) {
                        LOGGER.info(String.format("Current disk image copying count %d exceeds the limit %d",
                                copyCount, DISKIMAGE_COPY_LIMIT));
                        return;
                    }

                    if (DiskImage.Status.Ready != localDiskImage.getStatus()) {
                        LOGGER.info(String.format("Local disk image for guid \"%s\" is not ready", localDiskImage.getGuid()));
                        return;
                    }

                    final String diskImageGuid = localDiskImage.getGuid();

                    List<? extends DiskImage> diskImages =
                            _diskImageService.getDiskImagesByGuid(diskImageGuid, DiskImage.Status.RequestingCopy);
                    if (diskImages != null && !diskImages.isEmpty()) {
                        Collections.shuffle(diskImages);

                        final Server localServer = _serverService.getLocalServer();
                        boolean startedPeerCopy = false;

                        for (DiskImage diskImage : diskImages) {
                            LOGGER.info(String.format("Starting disk image copying for guid \"%s\" on host \"%s\"...",
                                    diskImageGuid, diskImage.getHostUuid()));

                            Host host = _hostService.getByUuid(diskImage.getHostUuid());
                            if (host == null) {
                                LOGGER.warn(String.format("Failed to get host for uuid \"%s\"", diskImage.getHostUuid()));
                                continue;
                            }

                            Server server = _serverService.getByUuid(host.getServerUuid());
                            if (server == null || Server.Status.Running != server.getStatus()) {
                                LOGGER.warn(String.format("Server \"%s\" is not running", host.getServerUuid()));
                                continue;
                            }

                            RemoteService remoteService = _remoteProxyFactory.getRemoteProxy(server);
                            if (remoteService == null) {
                                LOGGER.warn(String.format("Failed to get remote service for server \"%s\"",
                                        server.getPublicAddress()));
                                continue;
                            }

                            boolean succeeded = false;
                            try {
                                LOGGER.info(String.format("Allowing remote server \"%s\" to start disk image copying for uuid \"%s\" on host \"%s\"...",
                                        server.getPublicAddress(), localDiskImage.getUuid(), diskImage.getHostUuid()));

                                succeeded = remoteService.allowDiskImageCopy(localServer.getUuid(), localDiskImage.getUuid());
                                if (!succeeded) {
                                    LOGGER.warn(String.format("Failed to request DiskImage copy for uuid \"%s\" on server \"%s\". Try next...",
                                            localDiskImage.getUuid(), server.getPublicAddress()));
                                    continue;
                                }
                            } catch (Exception e) {
                                LOGGER.error(String.format("Failed to request DiskImage copy for uuid \"%s\" on server \"%s\". Try next...",
                                        localDiskImage.getUuid(), server.getPublicAddress()), e);
                                continue;
                            }

                            // This peer started disk image copy.
                            _activeDiskImageCopying.put(diskImage.getUuid(), server.getUuid());
                            startedPeerCopy = true;
                            break;
                        }

                        if (!startedPeerCopy) {
                            synchronized(_guidScheduleHandleMap) {
                                if (!_guidScheduleHandleMap.containsKey(diskImageGuid)) {
                                    // If failed, schedule to run it in one min.
                                    _guidScheduleHandleMap.put(diskImageGuid, _scheduler.schedule(new Runnable() {
                                        @Override
                                        public void run() {
                                            kickStartPeerDiskImageCopy(diskImageGuid);
                                        }
                                    }, 60));
                                }
                            }
                        }
                    }
                }
            }
        });
    }

    /**
     * 1. If remote node initiated deleting, delete vmImage.
     * 2. If this is a new vmImage, copy from peers.
     */
    private void processVmImageChange(VmImage vmImage, Map<String, Pair<Object, Object>> changedProperties) {
        if (!(vmImage instanceof VmImageVO)) {
            return;
        }

        Pair<Object, Object> statuses = null;
        if (changedProperties != null) {
            statuses = changedProperties.get(VmImage.PROP_STATUS);
        }

        if (statuses != null) {
            if (VmImage.Status.Deleting == vmImage.getStatus()) {
                // Some node initiated deleting vmImage.
                deleteVmImage(vmImage);

                return;
            }
        }
    }

    private void deleteVmImage(final VmImage vmImage) {
        // Delete all Unknown DiskImages.
        List<? extends DiskImage> unknownDiskImages =
                _diskImageService.getDiskImagesByOwner(vmImage.getUuid(), DiskImage.Status.Unknown);
        if (unknownDiskImages != null && !unknownDiskImages.isEmpty()) {
            for (DiskImage diskImage : unknownDiskImages) {
                _diskImageService.delete(diskImage);
            }
        }

        _vmImageService.deleteVmImage(vmImage, null);
    }

    private void createLocalDiskImage(DiskImage diskImage) {
        Host localHost = _hostService.getLocalHost();

        // Create a place holder entity.
        DiskImage localDiskImage = _diskImageService.newInstance();
        localDiskImage.setType(diskImage.getType());
        localDiskImage.setGuid(diskImage.getGuid());
        localDiskImage.setHostUuid(localHost.getUuid());
        localDiskImage.setStorageUuid("");
        localDiskImage.setCreateTime(diskImage.getCreateTime());
        localDiskImage.setName(diskImage.getName());
        localDiskImage.setOwnerUuid(diskImage.getOwnerUuid());
        localDiskImage.setVersion(diskImage.getVersion());
        localDiskImage.setParentGuid(diskImage.getParentGuid());
        localDiskImage.setDiskSize(diskImage.getDiskSize());
        localDiskImage.setStatus(DiskImage.Status.RequestingCopy);
        _diskImageService.commit(localDiskImage);

        LOGGER.info(String.format("Added new disk image (guid=%s) for cluster replication", localDiskImage.getGuid()));
    }

    private void deleteLocalDiskImage(DiskImage diskImage) {
        DeleteDiskImageFlow wf = new DeleteDiskImageFlow(diskImage, null);
        wf.run();
    }

    /**
     * 1. After CopyDiskImage completed, instantiate pool on this node.
     */
    private void processDiskImageChange(DiskImage diskImage, Map<String, Pair<Object, Object>> changedProperties) {
        if (!_diskImageService.isLocalDiskImage(diskImage)) {
            if (diskImage.getType().isReplicatable()
                    && DiskImage.Status.Ready == diskImage.getStatus()) {
                synchronized (_addLocalDiskImageLock) {
                    DiskImage localDiskImage = _diskImageService.getLocalDiskImageByGuid(diskImage.getGuid());
                    if (localDiskImage == null) {
                        createLocalDiskImage(diskImage);
                    }
                }
            }

            switch (diskImage.getStatus()) {
            case Ready:
            case Error:
                // If this disk image was copying from this node, kick start another one if it's succeeded or failed.
                if (_activeDiskImageCopying.remove(diskImage.getUuid()) != null) {
                    kickStartAllPeerDiskImageCopies();
                }
                break;

            case RequestingCopy:
                kickStartPeerDiskImageCopy(diskImage.getGuid());
                break;

            case Deleting:
                DiskImage localDiskImage = _diskImageService.getLocalDiskImageByGuid(diskImage.getGuid());
                if (localDiskImage != null) {
                    deleteLocalDiskImage(localDiskImage);
                    if (_activeDiskImageCopying.remove(diskImage.getUuid()) != null) {
                        kickStartAllPeerDiskImageCopies();
                    }
                }
                break;
            }
            return;
        }

        if (changedProperties != null) {
            Pair<Object, Object> statuses = changedProperties.get(DiskImage.PROP_STATUS);
            if (statuses != null) {
                if (DiskImage.Status.Deleting == statuses.getSecond()) {
                    deleteLocalDiskImage(diskImage);
                } else if (DiskImage.Status.Ready == statuses.getSecond()) {
                    if (diskImage.getType().isReplicatable()) {
                        kickStartPeerDiskImageCopy(diskImage);
                    }

                    if (diskImage.getType().isImageBase()) {
                        // Instantiate pool on this node after the disk image is copied.

                        // Find the VmImage
                        VmImage vmImage = _vmImageService.getByUuid(diskImage.getOwnerUuid());
                        if (vmImage != null) {
                            // Check if all the local disk images in the chain are ready.
                            boolean vmImageReady = true;

                            String diskImageGuid = vmImage.getCurrentDiskImageGuid();
                            while (!StringUtils.isEmpty(diskImageGuid)) {
                                DiskImage localDiskImage = _diskImageService.getLocalDiskImageByGuid(diskImageGuid);
                                if (localDiskImage != null && DiskImage.Status.Ready == localDiskImage.getStatus()) {
                                    diskImageGuid = localDiskImage.getParentGuid();
                                } else {
                                    vmImageReady = false;
                                    break;
                                }
                            }

                            if (vmImageReady) {
                                List<? extends Pool> pools = _poolService.getByVmImage(vmImage.getUuid());
                                if (pools != null) {
                                    for (Pool pool : pools) {
                                        _poolService.instantiatePool(pool, null);
                                    }
                                }
                            }
                        }
                    } else {
                        List<? extends VmInstance> newVmInstances =
                                _vmInstanceService.getLocalVmInstancesByPool(diskImage.getOwnerUuid());
                        if (newVmInstances != null && !newVmInstances.isEmpty()) {
                            Pool pool = _poolService.getByUuid(diskImage.getOwnerUuid());
                            for (VmInstance newVmInstance : newVmInstances) {
                                if (VmInstance.Status.New == newVmInstance.getStatus()) {
                                    _poolService.provisionLocalDesktop(pool, newVmInstance, null);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private class ServerChangeListenerImpl implements ObjectPropertyChangeListener {
        @Override
        public void newObject(ObjectPropertyChangeEvent event) {
            final Server server = (Server) event.getSource();
            if (Server.Status.Running != server.getStatus()) {
                updateActiveDiskImageCopying(server);
            }
        }

        @Override
        public void deleteObject(ObjectPropertyChangeEvent event) {
            final Server server = (Server) event.getSource();
            updateActiveDiskImageCopying(server);
        }

        @Override
        public void propertyChange(ObjectPropertyChangeEvent event) {
            final Server server = (Server) event.getSource();
            updateActiveDiskImageCopying(server);
        }

        private void updateActiveDiskImageCopying(Server server) {
            // Always kickStartPeerDiskImageCopy if server changes.
            boolean removedCopying = true;//false;

            synchronized(_activeDiskImageCopying) {
                List<String> diskImageUuidsToRemove = new ArrayList<String>();

                for (Map.Entry<String, String> entry : _activeDiskImageCopying.entrySet()) {
                    if (StringUtils.equals(entry.getValue(), server.getUuid())) {
                        diskImageUuidsToRemove.add(entry.getKey());
                    }
                }

                for (String diskImageUuid : diskImageUuidsToRemove) {
                    _activeDiskImageCopying.remove(diskImageUuid);
                }
            }

            if (removedCopying) {
                kickStartAllPeerDiskImageCopies();
            }
        }
    }

    private class VmImageChangeListenerImpl implements ObjectPropertyChangeListener {
        @Override
        public void newObject(ObjectPropertyChangeEvent event) {
            final VmImage vmImage = (VmImage) event.getSource();

            _asyncExecutor.run(new AsyncExecutor.Job() {
                @Override
                public Object run() {
                    processVmImageChange(vmImage, null);
                    return null;
                }
            }, null);
        }

        @Override
        public void deleteObject(ObjectPropertyChangeEvent event) {
        }

        @Override
        public void propertyChange(ObjectPropertyChangeEvent event) {
            final VmImage vmImage = (VmImage) event.getSource();
            final Map<String, Pair<Object, Object>> changedProperties = event.getChangedProperties();

            _asyncExecutor.run(new AsyncExecutor.Job() {
                @Override
                public Object run() {
                    processVmImageChange(vmImage, changedProperties);
                    return null;
                }
            }, null);
        }
    }

    private class DiskImageChangeListenerImpl implements ObjectPropertyChangeListener {
        @Override
        public void newObject(ObjectPropertyChangeEvent event) {
            final DiskImage diskImage = (DiskImage) event.getSource();

            _asyncExecutor.run(new AsyncExecutor.Job() {
                @Override
                public Object run() {
                    processDiskImageChange(diskImage, null);
                    return null;
                }
            }, null);
        }

        @Override
        public void deleteObject(ObjectPropertyChangeEvent event) {
            final DiskImage diskImage = (DiskImage) event.getSource();

            // Give this a try just in case.
            if (_activeDiskImageCopying.remove(diskImage.getUuid()) != null) {
                kickStartAllPeerDiskImageCopies();
            }

            if (diskImage.getType().isImageBase()) {
                VmImage vmImage = _vmImageService.getByUuid(diskImage.getOwnerUuid());
                if (vmImage != null) {
                    if (VmImage.Status.Deleting == vmImage.getStatus()) {
                        if (0 == _diskImageService.countByOwner(vmImage.getUuid())) {
                            _vmImageService.delete(vmImage);
                        }
                    }
                }
            }
        }

        @Override
        public void propertyChange(ObjectPropertyChangeEvent event) {
            final DiskImage diskImage = (DiskImage) event.getSource();
            final Map<String, Pair<Object, Object>> changedProperties = event.getChangedProperties();

            _asyncExecutor.run(new AsyncExecutor.Job() {
                @Override
                public Object run() {
                    processDiskImageChange(diskImage, changedProperties);
                    return null;
                }
            }, null);
        }
    }
}
