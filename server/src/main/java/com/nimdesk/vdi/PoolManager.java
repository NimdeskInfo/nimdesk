/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.vdi;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.nimdesk.cluster.RemoteProxyFactory;
import com.nimdesk.cluster.RemoteService;
import com.nimdesk.database.vo.VmInstanceVO;
import com.nimdesk.event.AddRemoveVmStateListenerEvent;
import com.nimdesk.event.AsyncCallback;
import com.nimdesk.event.DeleteLocalDesktopEvent;
import com.nimdesk.event.ProvisionDesktopsEvent;
import com.nimdesk.event.RebalanceDesktopsEvent;
import com.nimdesk.event.RecomposeDesktopsEvent;
import com.nimdesk.event.RequestDesktopEvent;
import com.nimdesk.model.DiskImage;
import com.nimdesk.model.Host;
import com.nimdesk.model.Pool;
import com.nimdesk.model.PoolUser;
import com.nimdesk.model.Server;
import com.nimdesk.model.Storage;
import com.nimdesk.model.UserSession;
import com.nimdesk.model.VmImage;
import com.nimdesk.model.VmInstance;
import com.nimdesk.service.AsyncExecutor;
import com.nimdesk.service.EventBusService;
import com.nimdesk.service.ObjectPropertyChangeEvent;
import com.nimdesk.service.ObjectPropertyChangeListener;
import com.nimdesk.service.Scheduler;
import com.nimdesk.service.impl.ServiceLocator;
import com.nimdesk.service.impl.workflow.RefreshDesktopFlow;
import com.nimdesk.service.impl.workflow.SnapshotDesktopFlow;
import com.nimdesk.service.impl.workflow.StartDesktopFlow;
import com.nimdesk.service.impl.workflow.StopDesktopFlow;
import com.nimdesk.tracker.TrackerManager;
import com.nimdesk.util.IdGenerator;
import com.nimdesk.util.Pair;
import com.nimdesk.vm.VmStateListener;

public class PoolManager {
    private static final Logger LOGGER = Logger.getLogger(PoolManager.class);

    private final EventBusService _eventBusService;
    private final AsyncExecutor _asyncExecutor;
    private final Scheduler _scheduler;
    private final RemoteProxyFactory _remoteProxyFactory;
    private final Server.Service _serverService;
    private final Host.Service _hostService;
    private final Storage.Service _storageService;
    private final VmImage.Service _vmImageService;
    private final DiskImage.Service _diskImageService;
    private final Pool.Service _poolService;
    private final PoolUser.Service _poolUserService;
    private final VmInstance.Service _vmInstanceService;
    private final UserSession.Service _userSessionService;
    private final TrackerManager _trackerManager;

    private final Object _isClusterMasterLock = new Object();
    private String _masterAddress = "localhost";

    // Must keep those listeners referenced here as they are added into a WeakHashMap.
    private final ObjectPropertyChangeListener _poolChangeListener = new PoolChangeListenerImpl();
    private final ObjectPropertyChangeListener _vmInstanceChangeListener = new VmInstanceChangeListenerImpl();

    // Monitor vm power state
    private final VmStateListener _vmStateListener = new VmStateListenerImpl();

    private static final long VM_SYSPREP_EXPIRE_PERIOD = 30 * 60 * 1000; // 30 min

    private Scheduler.ScheduleHandle _checkVmSchedule = null;
    private final Map<String, Long> _vmsInObtainingIp =
            Collections.synchronizedMap(new HashMap<String, Long>());

    private final Map<String, DesktopHandler> _poolDesktopHandlers =
            new HashMap<String, DesktopHandler>();

    private final Map<String, RequestDesktopEvent> _desktopRequestMap =
            new HashMap<String, RequestDesktopEvent>();

    public PoolManager(ServiceLocator serviceLocator) {
        _eventBusService = serviceLocator.getEventBusService();
        _asyncExecutor = serviceLocator.getAsyncExecutor();
        _scheduler = serviceLocator.getScheduler();
        _remoteProxyFactory = serviceLocator.getRemoteProxyFactory();
        _serverService = serviceLocator.getServerService();
        _hostService = serviceLocator.getHostService();
        _storageService = serviceLocator.getStorageService();
        _vmImageService = serviceLocator.getVmImageService();
        _diskImageService = serviceLocator.getDiskImageService();
        _poolService = serviceLocator.getPoolService();
        _poolUserService = serviceLocator.getPoolUserService();
        _vmInstanceService = serviceLocator.getVmInstanceService();
        _userSessionService = serviceLocator.getUserSessionService();
        _trackerManager = serviceLocator.getTrackerManager();

        _poolService.addObjectPropertyChangeListener(_poolChangeListener);
        _vmInstanceService.addObjectPropertyChangeListener(_vmInstanceChangeListener);
    }

    public void start() {
        // Let's register VmStateListener.
        AddRemoveVmStateListenerEvent listenerEvent =
                new AddRemoveVmStateListenerEvent(true, _vmStateListener);
        _eventBusService.post(listenerEvent);

        synchronized(_vmsInObtainingIp) {
            if (_checkVmSchedule != null) {
                _checkVmSchedule.cancel();
            }
        }
        _vmsInObtainingIp.clear();

        _checkVmSchedule = _scheduler.scheduleWithFixedDelay(new Runnable() {
            @Override
            public void run() {
                long currentTime = System.currentTimeMillis();

                // Check sysprep expired VMs
                List<String> expiredVms = new ArrayList<String>();
                synchronized(_vmsInObtainingIp) {
                    for (Map.Entry<String, Long> entry : _vmsInObtainingIp.entrySet()) {
                        if (currentTime - entry.getValue() > VM_SYSPREP_EXPIRE_PERIOD) {
                            expiredVms.add(entry.getKey());
                        }
                    }
                }
                for (String vmInstanceUuid : expiredVms) {
                    if (_vmsInObtainingIp.remove(vmInstanceUuid) != null) {
                        VmInstance vmInstance = _vmInstanceService.getByUuid(vmInstanceUuid);
                        if (vmInstance != null && VmInstance.Status.ObtainingIp == vmInstance.getStatus()) {
                            if (VmInstance.Error.SysprepTimeout == vmInstance.getLastError()) {
                                LOGGER.warn(String.format("VmInstance \"%s\" has expired sysprep stage. Has retried once, now error.", vmInstance.getName()));
                                vmInstance.setStatus(VmInstance.Status.Error);
                                vmInstance.setUpdateTime(System.currentTimeMillis());
                                _vmInstanceService.commit(vmInstance);
                            } else {
                                LOGGER.info(String.format("VmInstance \"%s\" has expired sysprep stage. Delete and retry...", vmInstance.getName()));
                                vmInstance.setLastError(VmInstance.Error.SysprepTimeout);
                                vmInstance.setRecompose(true);
                                _vmInstanceService.deleteVm(null, vmInstance, null);
                            }
                        }
                    }
                }

                // Refresh/power off logged off VMs
                List<? extends Pool> poolList = _poolService.getAll(null);
                if (poolList != null) {
                    for (Pool pool : poolList) {
                        refreshLoggedOffDesktops(pool);
                    }
                }
            }
        }, 60L, 60L); // Interval at 1 min

        try {
            List<? extends Pool> poolList = _poolService.getAll(null);
            if (poolList != null) {
                for (final Pool pool : poolList) {
                    switch (pool.getStatus()) {
                    case New:
                    case Provisioning:
                    case Recomposing:
                    case Ready:
                    case Error:
                        _asyncExecutor.run(new AsyncExecutor.Job() {
                            @Override
                            public Object run() {
                                startPool(pool);
                                return null;
                            }
                        }, null);
                        break;

                    case Deleting:
                        LOGGER.info(String.format("start: Deleting pool \"%s\"(%s)", pool.getName(), pool.getUuid()));
                        deletePool(pool);
                        break;
                    }
                }
            }
        } catch (Exception e) {
            // This will throw exception if the db hasn't been initialized.
            // It's okay to ignore the exception.
            LOGGER.warn("PoolManager start failed.", e);
        }
    }

    public void stop() {
        // Let's unregister VmStateListener.
        AddRemoveVmStateListenerEvent listenerEvent =
                new AddRemoveVmStateListenerEvent(false, _vmStateListener);
        _eventBusService.post(listenerEvent);

        synchronized(_vmsInObtainingIp) {
            if (_checkVmSchedule != null) {
                _checkVmSchedule.cancel();
                _checkVmSchedule = null;
            }
        }
        _vmsInObtainingIp.clear();
    }

    public void setClusterMaster(boolean isMaster, String masterAddress) {
        synchronized(_isClusterMasterLock) {
            if (isMaster) {
                _masterAddress = null;
            } else {
                _masterAddress = masterAddress;
            }
        }
    }

    private boolean isClusterMaster() {
        synchronized(_isClusterMasterLock) {
            return _masterAddress == null;
        }
    }

    @SuppressWarnings("unused")
    private String getClusterMaster() {
        synchronized(_isClusterMasterLock) {
            return _masterAddress;
        }
    }

    private void registerPool(final Pool pool) {
        LOGGER.info(String.format("Register pool \"%s\" (%s) with PoolManager", pool.getName(), pool.getUuid()));

        synchronized(_poolDesktopHandlers) {
            if (!_poolDesktopHandlers.containsKey(pool.getUuid())) {
                _poolDesktopHandlers.put(pool.getUuid(), new DesktopHandler(pool.getUuid()));
            }
        }

        // Attempt to process this pool right away
        _asyncExecutor.run(new AsyncExecutor.Job() {
            @Override
            public Object run() {
                if (Pool.Status.New == pool.getStatus()) {
                    enforcePoolSize(pool);
                } else {
                    enforcePoolLocalPreStartCount(pool/*, false*/);
                }
                return null;
            }
        }, null);
    }

    private void unregisterPool(Pool pool) {
        LOGGER.info(String.format("Unregister pool \"%s\" (%s) from PoolManager", pool.getName(), pool.getUuid()));

        synchronized(_poolDesktopHandlers) {
            _poolDesktopHandlers.remove(pool.getUuid());
        }
    }

    private void startPool(Pool pool) {
        switch (pool.getStatus()) {
        case New:
        case Provisioning:
        case Recomposing:
        case Ready:
        case Error:
            break;

        default:
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace(String.format("Pool \"%s\" (%s) is not ready", pool.getName(), pool.getUuid()));
            }
            return;
        }
        if (!pool.isEnabled()) {
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace(String.format("Pool \"%s\" (%s) is disabled", pool.getName(), pool.getUuid()));
            }
            return;
        }

        registerPool(pool);

        // Give this a try just in case a new pool hasn't been processed.
        processNewPool(pool);

        Host localHost = _hostService.getLocalHost();
        if (localHost == null) {
            LOGGER.warn("Unable to find local host");
            return;
        }
        if (Host.Status.Connected != localHost.getStatus()) {
            LOGGER.warn("Local host is not connected. Ignore powerOnPool");
            return;
        }

        List<? extends VmInstance> vmInstances = _vmInstanceService.getLocalVmInstancesByPool(pool.getUuid());
        if (vmInstances != null) {
            for (VmInstance vmInstance : vmInstances) {
                boolean toContinue = false;

                switch (vmInstance.getStatus()) {
                case New:
                case Provisioning:
                    _poolService.provisionLocalDesktop(pool, vmInstance, null);
                    toContinue = true;
                    break;

                case Creating:
                case ObtainingIp:
                case TakingSnapshot:
                    // Delete those umcompleted desktops. If pool is in provisioning stage, they will be recreated.

                case Destroying:
                case Destroyed:
                    try {
                        // Call DeleteDesktopFlow only if it's local VmInstance.
                        DeleteLocalDesktopEvent event = new DeleteLocalDesktopEvent(null, null, localHost, pool, vmInstance, null);
                        _eventBusService.post(event);
                    } catch (Exception e) {
                        LOGGER.error(String.format("Failed to delete VmInstance \"%s\"", vmInstance.getName()), e);
                    }
                    toContinue = true;
                    break;
                }

                if (toContinue) {
                    continue;
                }

                if (StringUtils.isEmpty(vmInstance.getVmRefId())) {
                    LOGGER.info(String.format("Re-provisioning VmInstance \"%s\" with empty vmRefId",
                            vmInstance.getName()));

                    _poolService.provisionLocalDesktop(pool, vmInstance, null);
                    continue;
                }

                switch (vmInstance.getStatus()) {
                case Refreshing:
                    // Refresh the vm and revoke current status.
                    refreshVmInstance(localHost, pool, vmInstance);
                    break;

                case Assigning:
                case Assigned:
                case InSession:
                case LoggedOff:
                    vmInstance.setLoginTime(0);
                    vmInstance.setLogOffMode(null);
                    switch (pool.getType()) {
                    case Persistent:
                        vmInstance.setStatus(VmInstance.Status.Stopping);
                        break;

                    default:
                        // processVmInstanceChange will refresh the vm.
                        vmInstance.setUserUuid("");
                        vmInstance.setStatus(VmInstance.Status.Refreshing);
                        break;
                    }
                    vmInstance.setUpdateTime(System.currentTimeMillis());
                    _vmInstanceService.commit(vmInstance);
                    break;

                case Starting:
                case Started:
                    if (Pool.Type.Persistent == pool.getType()) {
                        vmInstance.setStatus(VmInstance.Status.Stopping);
                        vmInstance.setUpdateTime(System.currentTimeMillis());
                        _vmInstanceService.commit(vmInstance);
                    } else {
                        powerOnVmInstance(localHost, pool, vmInstance);
                    }
                    break;

                case Stopping:
                    powerOffVmInstance(localHost, pool, vmInstance);
                    break;
                }
            }
        }
    }

    private void stopPool(Pool pool) {
        switch (pool.getStatus()) {
        case New:
        case Provisioning:
        case Recomposing:
        case Ready:
        case Error:
            break;

        default:
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace(String.format("Pool \"%s\" (%s) is not ready", pool.getName(), pool.getUuid()));
            }
            return;
        }

        Host localHost = _hostService.getLocalHost();
        if (localHost == null) {
            LOGGER.warn("Unable to find local host");
            return;
        }
        if (Host.Status.Connected != localHost.getStatus()) {
            LOGGER.warn("Local host is not connected. Ignore stopPool");
            return;
        }

        List<? extends VmInstance> vmInstances = _vmInstanceService.getLocalVmInstancesByPool(pool.getUuid());
        if (vmInstances != null) {
            for (VmInstance vmInstance : vmInstances) {
                switch (vmInstance.getStatus()) {
                case Refreshing:
                    // Refresh the vm and revoke current status.
                    if (Pool.Type.Persistent != pool.getType()) {
                        vmInstance.setUserUuid("");
                        vmInstance.setUpdateTime(System.currentTimeMillis());
                        _vmInstanceService.commit(vmInstance);
                    }
                    break;

                case LoggedOff:
                    vmInstance.setLoginTime(0);
                    vmInstance.setLogOffMode(null);

                case Starting:
                case Started:
                    if (Pool.Type.Persistent == pool.getType()) {
                        _vmInstanceService.powerOffVm(pool, vmInstance, null);
                    } else {
                        vmInstance.setUserUuid("");
                        _vmInstanceService.refreshVm(pool, vmInstance, null);
                    }
                    break;
                }
            }
        }
    }

    /**
     * Maintain pool size
     */
    private void enforcePoolSize(Pool pool) {
        // Only cluster master is enforcing pool size.
        if (!isClusterMaster()) {
            return;
        }

        if (!pool.isEnabled()) {
            LOGGER.info(String.format("Pool \"%s\" (%s) is disabled", pool.getName(), pool.getUuid()));
            return;
        }
        if (Pool.Status.Deleting == pool.getStatus()) {
            LOGGER.warn(String.format("Pool \"%s\" is being deleted", pool.getName()));
            return;
        }

        LOGGER.info(String.format("Enforcing size for pool \"%s\"(%s)...", pool.getName(), pool.getUuid()));

        DesktopHandler desktopHandler = null;
        synchronized(_poolDesktopHandlers) {
            desktopHandler = _poolDesktopHandlers.get(pool.getUuid());
        }

        if (desktopHandler != null) {
            desktopHandler.enforcePoolSize(pool);
        }
    }

    /**
    * Enforce pre-start policy
    */
   private void enforcePoolLocalPreStartCount(Pool pool/*, boolean allowMoreDesktops*/) {
       if (pool.getType() == Pool.Type.Persistent) {
           LOGGER.info(String.format("Pool \"%s\" (%s) is persistent, no pre-start enforced", pool.getName(), pool.getUuid()));
           return;
       }

       if (!pool.isEnabled()) {
           LOGGER.info(String.format("Pool \"%s\" (%s) is disabled", pool.getName(), pool.getUuid()));
           return;
       }
       if (Pool.Status.Deleting == pool.getStatus()) {
           LOGGER.warn(String.format("Pool \"%s\" is being deleted", pool.getName()));
           return;
       }

       LOGGER.info(String.format("Enforcing pre-start count for pool \"%s\"(%s)...", pool.getName(), pool.getUuid()));

       DesktopHandler desktopHandler = null;
       synchronized(_poolDesktopHandlers) {
           desktopHandler = _poolDesktopHandlers.get(pool.getUuid());
       }

       if (desktopHandler != null) {
           desktopHandler.enforcePoolLocalPreStartCount(pool);
       }
   }

   /**
    * 1. If pool is replicated from peers and is ready, instantiate on this node.
    */
   private void processNewPool(Pool pool) {
       // 1. Check if vmImage exists and ready - if not, wait for it to be copied.
       VmImage vmImage = _vmImageService.getByUuid(pool.getVmImageUuid());
       if (vmImage == null || VmImage.Status.Ready != vmImage.getStatus()) {
           LOGGER.warn(String.format("VmImage \"%s\" for pool \"%s\" is not ready",
                   pool.getVmImageUuid(), pool.getName()));
           return;
       }

       // 2. Check if all the local disk images in the chain are ready - if not,
       //    wait for it to be copied.
       boolean vmImageReady = true;

       String diskImageGuid = vmImage.getCurrentDiskImageGuid();
       while (!StringUtils.isEmpty(diskImageGuid)) {
           DiskImage localDiskImage = _diskImageService.getLocalDiskImageByGuid(diskImageGuid);
           if (localDiskImage != null && DiskImage.Status.Ready == localDiskImage.getStatus()) {
               diskImageGuid = localDiskImage.getParentGuid();
           } else {
               vmImageReady = false;
               break;
           }
       }

       if (!vmImageReady) {
           LOGGER.warn(String.format("DiskImage guid=\"%s\" for pool \"%s\" is not ready",
                   diskImageGuid, pool.getName()));
           return;
       }

       // 3. Instantiate pool on local node.
       LOGGER.info(String.format("Instantiating pool \"%s\"...", pool.getName()));
       //if (StringUtils.isEmpty(pool.getBaseDiskImageUuid())) {
           _poolService.instantiatePool(pool, null);
       //}

       return;
   }

   /**
    * 1. Adjust pool size (provision or destroy).
    * 2. Assign to pending requestor.
    */
   private void onVmInstanceDelete(VmInstance vmInstance) {
       reportDesktopRequest(vmInstance);

       //if (isClusterMaster()) {
           Pool pool = _poolService.getByUuid(vmInstance.getPoolUuid());
           if (pool != null) {
               if (Pool.Status.Deleting == pool.getStatus()) {
                   if (0 == _vmInstanceService.countByPool(pool.getUuid())) {
                       _poolService.delete(pool);
                   }
               } else {
                   //enforcePoolSize(pool);
               }
           }
       //}
   }

   /**
    * 1. If vmInstance in error state, delete it.
    * 2. Status ObtainingIp -> TakingSnapshot, taking snapshot.
    * 3. Status InSession -> Refreshing, apply refresh policy.
    * 4. Status * -> Ready/Assigned/AssignError, assign to pending requestor.
    */
   private void processVmInstanceChange(VmInstance vmInstance, Map<String, Pair<Object, Object>> changedProperties) {
       // If a desktop becomes Started or not Started (assigned etc.), check desktop requests.
       switch (vmInstance.getStatus()) {
       case Assigned:
       case InSession:
       case AssignError:
       case Destroying:
       case Error:
       case Unknown:
           reportDesktopRequest(vmInstance);
           break;
       }

       Pair<Object, Object> statuses = null;
       if (changedProperties != null) {
           statuses = changedProperties.get(VmInstance.PROP_STATUS);
       }

       Pool pool = null;

       if (statuses != null) {
           if (VmInstance.Status.InSession == statuses.getFirst()) {
               if (pool == null) {
                   pool = _poolService.getByUuid(vmInstance.getPoolUuid());
               }
               if (pool == null) {
                   LOGGER.warn(String.format("Failed to find pool for VmInstance \"%s\"", vmInstance.getName()));
               } else {
                   if (vmInstance.isRecompose()
                           || pool.getStatus() == Pool.Status.Recomposing) {
                       // Continue the deletion.
                       _vmInstanceService.deleteVm(pool, vmInstance, null);
                       return;
                   }
               }
           }

           VmInstance.Status newStatus = (VmInstance.Status) statuses.getSecond();

           switch (newStatus) {
           case Ready:
           case Error:
               // If a desktop becomes Ready or Error, check if provisioning process is completed.

               if (pool == null) {
                   pool = _poolService.getByUuid(vmInstance.getPoolUuid());
               }
               if (pool == null) {
                   LOGGER.warn(String.format("Failed to find pool for VmInstance \"%s\"", vmInstance.getName()));
               } else {
                   if (isClusterMaster()) {
                       if (Pool.Status.Provisioning == pool.getStatus()
                               || Pool.Status.Recomposing == pool.getStatus()) {
                           // Check if provisioning process is completed. If so, change status to Ready.

                           long provisioningDesktopCount = _vmInstanceService.countByPoolAndStatus(pool.getUuid(), VmInstance.PRESTARTING_STATUSES);
                           if (provisioningDesktopCount == 0) {
                               pool.setStatus(Pool.Status.Ready);
                               pool.setUpdateTime(System.currentTimeMillis());
                               _poolService.commit(pool);

                               _trackerManager.removeProgress(pool.getUuid());
                           } else {
                               long readyDesktopCount = _vmInstanceService.countByPoolAndStatus(pool.getUuid(), VmInstance.GREEN_STATUSES);
                               _trackerManager.updateProgress(pool.getUuid(), (int) readyDesktopCount, null, "Provisioning...");
                           }
                       }
                   }
               }

               break;
           }
       }

       if (!_vmInstanceService.isLocalVmInstance(vmInstance)) {
           // Not local vmInstance.
           return;
       }

       // From now on, only process local vmInstance.

       if (pool == null) {
           pool = _poolService.getByUuid(vmInstance.getPoolUuid());
       }
       if (pool != null && Pool.Status.Deleting == pool.getStatus()) {
           LOGGER.warn(String.format("Pool \"%s\" is being deleted", pool.getName()));
           _vmInstanceService.deleteVm(pool, vmInstance, null);
           return;
       }

       if (statuses != null) {
           if (VmInstance.Status.InSession == statuses.getSecond()) {
               // This is something else -> InSession transition. Create a userSession.
               _userSessionService.startUserSession(vmInstance.getUserUuid(), pool, vmInstance);
           } else if (VmInstance.Status.InSession == statuses.getFirst()) {
               // This is InSession -> something else transition. Complete a userSession.
               _userSessionService.completeUserSession(vmInstance);
           }

           else if (VmInstance.Status.ObtainingIp == statuses.getSecond()) {
               // This is something else -> ObtainingIp transition.
               _vmsInObtainingIp.put(vmInstance.getUuid(), System.currentTimeMillis());
           } else if (VmInstance.Status.ObtainingIp == statuses.getFirst()) {
               // This is ObtainingIp -> something else transition.
               _vmsInObtainingIp.remove(vmInstance.getUuid());
           }

           VmInstance.Status newStatus = (VmInstance.Status) statuses.getSecond();

           switch (newStatus) {
           case New:
               if (pool == null) {
                   LOGGER.warn(String.format("Failed to find pool for VmInstance \"%s\"", vmInstance.getName()));
               } else {
                   if (Pool.Status.Deleting == pool.getStatus()) {
                       LOGGER.warn(String.format("Pool \"%s\" is being deleted", pool.getName()));
                   } else {
                       _poolService.provisionLocalDesktop(pool, vmInstance, null);
                   }
               }

               break;

           case Ready:
           case InSession:
               // If a desktop becomes InSession or Ready, check and enforce pool size to make sure enough
               // pre-started desktops.
               if (pool == null) {
                   LOGGER.warn(String.format("Failed to find pool for VmInstance \"%s\"", vmInstance.getName()));
               } else {
                   final Pool tmpPool = pool;

                   _asyncExecutor.run(new AsyncExecutor.Job() {
                       @Override
                       public Object run() {
                           enforcePoolLocalPreStartCount(tmpPool/*, false*/);
                           return null;
                       }
                   }, null);
               }

               break;

           case Error:
               Pair<Object, Object> errors = null;
               if (changedProperties != null) {
                   errors = changedProperties.get(VmInstance.PROP_LASTERROR);
               }

               switch ((VmInstance.Status) statuses.getFirst()) {
               case Creating:
                   if (errors != null) {
                       LOGGER.warn(String.format("VmInstance \"%s\" has failed creating stage. Delete and retry...", vmInstance.getName()));
                       vmInstance.setRecompose(true);
                       _vmInstanceService.deleteVm(null, vmInstance, null);
                   } else {
                       LOGGER.warn(String.format("VmInstance \"%s\" has failed creating stage.", vmInstance.getName()));
                   }
                   break;

               case TakingSnapshot:
                   if (errors != null) {
                       LOGGER.warn(String.format("VmInstance \"%s\" has failed taking snapshot stage. Delete and retry...", vmInstance.getName()));
                       vmInstance.setRecompose(true);
                       _vmInstanceService.deleteVm(null, vmInstance, null);
                   } else {
                       LOGGER.warn(String.format("VmInstance \"%s\" has failed creating stage.", vmInstance.getName()));
                   }
                   break;

               case Starting:
                   if (errors != null) {
                       LOGGER.warn(String.format("VmInstance \"%s\" has failed starting. Retry...", vmInstance.getName()));
                       _vmInstanceService.powerOnVm(null, vmInstance, null);
                   } else {
                       if (pool == null) {
                           LOGGER.warn(String.format("VmInstance \"%s\" has failed starting.", vmInstance.getName()));
                           LOGGER.warn(String.format("Failed to find pool for VmInstance \"%s\"", vmInstance.getName()));
                       } else {
                           if (Pool.Type.Persistent != pool.getType()) {
                               // For non-persistent pool, refresh it.
                               LOGGER.warn(String.format("VmInstance \"%s\" has failed starting. Refreshing...", vmInstance.getName()));
                               _vmInstanceService.refreshVm(pool, vmInstance, null);
                           } else {
                               LOGGER.warn(String.format("VmInstance \"%s\" has failed starting.", vmInstance.getName()));
                           }
                       }
                   }
                   break;

               case Stopping:
                   if (errors != null) {
                       LOGGER.warn(String.format("VmInstance \"%s\" has failed stopping. Retry...", vmInstance.getName()));
                       _vmInstanceService.powerOffVm(null, vmInstance, null);
                   } else {
                       if (pool == null) {
                           LOGGER.warn(String.format("VmInstance \"%s\" has failed stopping.", vmInstance.getName()));
                           LOGGER.warn(String.format("Failed to find pool for VmInstance \"%s\"", vmInstance.getName()));
                       } else {
                           if (Pool.Type.Persistent != pool.getType()) {
                               // For non-persistent pool, refresh it.
                               LOGGER.warn(String.format("VmInstance \"%s\" has failed starting. Refreshing...", vmInstance.getName()));
                               _vmInstanceService.refreshVm(pool, vmInstance, null);
                           } else {
                               LOGGER.warn(String.format("VmInstance \"%s\" has failed starting.", vmInstance.getName()));
                           }
                       }
                   }
                   break;
               }

               break;
           }
       }

       boolean deleteVm = false;

       if (vmInstance instanceof VmInstanceVO) {
           // Check if remote node requested deleting this vmInstance.
           VmInstanceVO vmInstanceVO = (VmInstanceVO) vmInstance;

           if (statuses != null && !vmInstanceVO.inOperation()) {
               if (VmInstance.Status.Refreshing == statuses.getSecond()) {
                   refreshVmInstance(null, null, vmInstance);
                   return;
               } else if (VmInstance.Status.Starting == statuses.getSecond()) {
                   powerOnVmInstance(null, null, vmInstance);
                   return;
               } else if (VmInstance.Status.Stopping == statuses.getSecond()) {
                   powerOffVmInstance(null, null, vmInstance);
                   return;
               } else if (VmInstance.Status.Destroying == statuses.getSecond()) {
                   // Some node initiated deleting vmInstance.
                   deleteVm = true;
               }
           }
       }

       if (deleteVm) {
           if (pool == null) {
               pool = _poolService.getByUuid(vmInstance.getPoolUuid());
           }
           if (pool == null) {
               LOGGER.warn(String.format("Failed to find pool for VmInstance \"%s\"", vmInstance.getName()));
           }

           Host host = _hostService.getLocalHost();

           if (vmInstance instanceof VmInstanceVO) {
               ((VmInstanceVO) vmInstance).inOperation(true);
           }

           // Call DeleteDesktopFlow only if it's local VmInstance.
           DeleteLocalDesktopEvent event = new DeleteLocalDesktopEvent(null, null, host, pool, vmInstance, null);
           _eventBusService.post(event);

           return;
       }

       if (!StringUtils.isEmpty(vmInstance.getUserUuid())) {
           switch (vmInstance.getStatus()) {
           case Started:
               LOGGER.info(String.format("Assigning desktop \"%s\"(%s) to user \"%s\"...",
                       vmInstance.getName(), vmInstance.getIpAddress(), vmInstance.getUserUuid()));
               vmInstance.setStatus(vmInstance.isAssigned() ? VmInstance.Status.Assigned : VmInstance.Status.Assigning);
               vmInstance.setUpdateTime(System.currentTimeMillis());
               _vmInstanceService.commit(vmInstance);
               break;

           case Ready:
               if (pool == null) {
                   LOGGER.warn(String.format("Failed to find pool for VmInstance \"%s\" taking snapshot",
                           vmInstance.getName()));
               } else if (Pool.Type.Persistent != pool.getType()) {
                   LOGGER.info(String.format("Powering on desktop \"%s\" to assign to user \"%s\"...",
                           vmInstance.getName(), vmInstance.getUserUuid()));
                   _vmInstanceService.powerOnVm(pool, vmInstance, null);
               }
               break;
           }
       }

       if (changedProperties == null) {
           return;
       }

       // The following are dealing with property changes.

       // Taking snapshot.
       if (VmInstance.Status.TakingSnapshot == vmInstance.getStatus()) {
           if (statuses != null && VmInstance.Status.ObtainingIp == statuses.getFirst()) {
               // This is ObtainingIp -> TakingSnapshot transition... take snapshot
               if (pool == null) {
                   LOGGER.warn(String.format("Failed to find pool for VmInstance \"%s\" taking snapshot",
                           vmInstance.getName()));
                   return;
               }

               Host host = _hostService.getLocalHost();

               LOGGER.info(String.format("VmInstance \"%s\" is taking snapshot...", vmInstance.getName()));

               // Call SnapshotDesktopFlow only if it's local VmInstance.
               SnapshotDesktopFlow wf = new SnapshotDesktopFlow(host, pool, vmInstance, null);
               wf.run();
           }

           return;
       }

       // Check if user assignment failed and if so, reset.
       if (VmInstance.Status.AssignError == vmInstance.getStatus()) {
           // Return this VM to Started state to give others a chance.
           vmInstance.setLoginTime(0);
           vmInstance.setLogOffMode(null);
           vmInstance.setUserUuid("");
           vmInstance.setAssigned(false);
           vmInstance.setStatus(VmInstance.Status.Started);
           vmInstance.setUpdateTime(System.currentTimeMillis());
           _vmInstanceService.commit(vmInstance);
       }
   }

   public void handleProvisionDesktops(ProvisionDesktopsEvent event) {
       Pool pool = event.getPool();
       boolean rebalance = event.isRebalance();
       AsyncCallback callback = event.getCallback();
       RemoteService remoteService = null;

       try {
           synchronized(_isClusterMasterLock) {
               if (_masterAddress != null) {
                   remoteService = _remoteProxyFactory.getRemoteProxy(_masterAddress);
                   if (remoteService == null) {
                       LOGGER.warn(String.format("Failed to get remote service from master address \"%s\"", _masterAddress));
                       throw new Exception("Unable to contact cluster master");
                   }
               }
           }

           int provisionedDesktops;

           if (remoteService != null) {
               provisionedDesktops = remoteService.provisionDesktops(pool.getUuid(), rebalance);
               if (provisionedDesktops <= 0) {
                   LOGGER.warn(String.format("No available desktop allocated by cluster master"));
                   throw new Exception("No desktop allocated");
               }
           } else {
               provisionedDesktops = allocateDesktops(pool.getUuid(), rebalance);
               if (provisionedDesktops <= 0) {
                   LOGGER.warn(String.format("No available desktop allocated by local node"));
                   throw new Exception("No desktop allocated");
               }
           }

           LOGGER.info(String.format("Allocated %d desktops for pool \"%s\"",
                   provisionedDesktops, pool.getName()));

           if (callback != null) {
               callback.onSuccess(provisionedDesktops);
           }
       } catch (Exception e) {
           LOGGER.error(String.format("Failed to allocate desktops for pool \"%s\", rebalance=%s",
                   pool.getName(), rebalance ? "true" : "false"), e);

           if (callback != null) {
               callback.onFailure(e);
           }
       }
   }

   public void handleRecomposeDesktops(RecomposeDesktopsEvent event) {
       Pool pool = event.getPool();
       AsyncCallback callback = event.getCallback();
       RemoteService remoteService = null;

       try {
           synchronized(_isClusterMasterLock) {
               if (_masterAddress != null) {
                   remoteService = _remoteProxyFactory.getRemoteProxy(_masterAddress);
                   if (remoteService == null) {
                       LOGGER.warn(String.format("Failed to get remote service from master address \"%s\"", _masterAddress));
                       throw new Exception("Unable to contact cluster master");
                   }
               }
           }

           boolean recomposedDesktops;

           if (remoteService != null) {
               recomposedDesktops = remoteService.recomposeDesktops(pool.getUuid(), event.isForced());
               if (!recomposedDesktops) {
                   LOGGER.warn(String.format("No available desktop recomposed by cluster master"));
                   throw new Exception("No desktops recomposed");
               }
           } else {
               recomposedDesktops = recomposeDesktops(pool.getUuid(), event.isForced());
               if (!recomposedDesktops) {
                   LOGGER.warn(String.format("No available desktop recomposed by local node"));
                   throw new Exception("No desktops recomposed");
               }
           }

           LOGGER.info(String.format("Recomposed desktops for pool \"%s\"", pool.getName()));

           if (callback != null) {
               callback.onSuccess(null);
           }
       } catch (Exception e) {
           LOGGER.error(String.format("Failed to recompose desktops for pool \"%s\"",
                   pool.getName()), e);

           if (callback != null) {
               callback.onFailure(e);
           }
       }
   }

   public void handleRebalanceDesktops(RebalanceDesktopsEvent event) {
       Pool pool = event.getPool();
       AsyncCallback callback = event.getCallback();
       RemoteService remoteService = null;

       try {
           synchronized(_isClusterMasterLock) {
               if (_masterAddress != null) {
                   remoteService = _remoteProxyFactory.getRemoteProxy(_masterAddress);
                   if (remoteService == null) {
                       LOGGER.warn(String.format("Failed to get remote service from master address \"%s\"", _masterAddress));
                       throw new Exception("Unable to contact cluster master");
                   }
               }
           }

           boolean rebalancedDesktops;

           if (remoteService != null) {
               rebalancedDesktops = remoteService.rebalanceDesktops(pool.getUuid());
               if (!rebalancedDesktops) {
                   LOGGER.warn(String.format("No available desktop rebalanced by cluster master"));
                   throw new Exception("No desktops rebalanced");
               }
           } else {
               rebalancedDesktops = rebalanceDesktops(pool.getUuid());
               if (!rebalancedDesktops) {
                   LOGGER.warn(String.format("No available desktop rebalanced by local node"));
                   throw new Exception("No desktops rebalanced");
               }
           }

           LOGGER.info(String.format("Rebalanced desktops for pool \"%s\"", pool.getName()));

           if (callback != null) {
               callback.onSuccess(null);
           }
       } catch (Exception e) {
           LOGGER.error(String.format("Failed to rebalanced desktops for pool \"%s\"",
                   pool.getName()), e);

           if (callback != null) {
               callback.onFailure(e);
           }
       }
   }

   public void handleRequestDesktop(RequestDesktopEvent event) {
       Pool pool = event.getPool();
       String userUuid = event.getUserUuid();
       String requestUuid = String.format("%s:%s", userUuid, pool.getUuid());
       AsyncCallback callback = event.getCallback();
       RemoteService masterRemoteService = null;

       try {
           Pool pool2 = _poolService.getByUuid(pool.getUuid());
           if (pool2 == null || Pool.Status.Deleting == pool2.getStatus()) {
               LOGGER.warn(String.format("Unable to find pool id=\"%s\"", pool.getUuid()));
               throw new Exception("No valid desktop pool");
           }
           if (!pool2.isEnabled()) {
               LOGGER.info(String.format("Pool \"%s\" (%s) is disabled", pool.getName(), pool.getUuid()));
               throw new Exception("Desktop pool is disabled");
           }

           String vmInstanceUuid = null;

           synchronized(_isClusterMasterLock) {
               if (_masterAddress != null) {
                   masterRemoteService = _remoteProxyFactory.getRemoteProxy(_masterAddress);
                   if (masterRemoteService == null) {
                       LOGGER.warn(String.format("Failed to get remote service from master address \"%s\"", _masterAddress));
                       throw new Exception("Unable to contact cluster master");
                   }
               }
           }

           _trackerManager.updateLocalProgress(requestUuid, 0, "Assigning desktop...");

           // Try to get assigned desktop first.
           if (masterRemoteService != null) {
               vmInstanceUuid = masterRemoteService.getAssignedDesktop(pool.getUuid(), userUuid);
           } else {
               VmInstance vmInstance = getAssignedDesktop(pool.getUuid(), userUuid);
               if (vmInstance != null) {
                   vmInstanceUuid = vmInstance.getUuid();
               }
           }

           // If no desktop assigned, get to individual server to assign local desktop.
           if (StringUtils.isEmpty(vmInstanceUuid)) {
               List<? extends Server> servers = _serverService.getRunningServers();
               if (servers == null || servers.isEmpty()) {
                   LOGGER.warn("No available server found in the cluster");
                   throw new Exception("No available server");
               }

               Collections.shuffle(servers);

               for (Server server : servers) {
                   if (_serverService.isLocalServer(server.getUuid())) {
                       VmInstance vmInstance = assignLocalDesktopToUser(pool.getUuid(), userUuid);
                       if (vmInstance != null) {
                           vmInstanceUuid = vmInstance.getUuid();
                           break;
                       }
                   } else {
                       if (Server.Status.Running != server.getStatus()) {
                           continue;
                       }

                       RemoteService remoteService = _remoteProxyFactory.getRemoteProxy(server.getPublicAddress());
                       if (remoteService == null) {
                           LOGGER.warn(String.format("Failed to get remote service from server \"%s\"", server.getPublicAddress()));
                           continue;
                       }

                       vmInstanceUuid = remoteService.assignLocalDesktopToUser(pool.getUuid(), userUuid);
                       if (vmInstanceUuid == null) {
                           LOGGER.warn(String.format("No available desktop assigned by server \"%s\"", server.getPublicAddress()));
                           continue;
                       }

                       break;
                   }
               }
           }

           // Now try those vmInstances in unknown status (hosts not connected).
           if (StringUtils.isEmpty(vmInstanceUuid)
                   && Pool.Type.Persistent == pool.getType()) {
               if (masterRemoteService != null) {
                   vmInstanceUuid = masterRemoteService.assignDesktop(pool.getUuid(), userUuid);
               } else {
                   VmInstance vmInstance = assignDesktop(pool.getUuid(), userUuid);
                   if (vmInstance != null) {
                       vmInstanceUuid = vmInstance.getUuid();
                   }
               }
           }

           if (StringUtils.isEmpty(vmInstanceUuid)) {
               LOGGER.warn(String.format("No available desktop assigned by all servers in cluster"));
               throw new Exception("No available desktop");
           }

           _trackerManager.updateLocalProgress(requestUuid, 0, "Powering on desktop...");

           // Before we insert into _desktopRequestMap, make sure vmInstance is not updated in between.
           VmInstance vmInstance = null;
           boolean isAssigned = false;
           synchronized (_desktopRequestMap) {
               vmInstance = _vmInstanceService.getByUuid(vmInstanceUuid);
               if (vmInstance == null) {
                   LOGGER.warn(String.format("The assigned desktop uuid=\"%s\" can not be found on local server", vmInstanceUuid));
                   throw new Exception("No available desktop");
               }
               if (VmInstance.Status.Assigned != vmInstance.getStatus()) {
                   LOGGER.info(String.format("Assigning desktop \"%s\"(%s) to user \"%s\" from pool \"%s\"",
                           vmInstance.getName(), vmInstance.getIpAddress(), userUuid, pool.getName()));
                   _desktopRequestMap.put(vmInstanceUuid, event);
               } else {
                   isAssigned = true;
               }
           }

           if (!isAssigned) {
               if (VmInstance.Status.Started == vmInstance.getStatus() && vmInstance.isAssigned()) {
                   vmInstance.setStatus(VmInstance.Status.Assigned);
                   vmInstance.setUpdateTime(System.currentTimeMillis());
                   _vmInstanceService.commit(vmInstance);
               }
               return;
           }

           LOGGER.info(String.format("Assigned desktop \"%s\"(%s) to user \"%s\" from pool \"%s\"",
                   vmInstance.getName(), vmInstance.getIpAddress(), userUuid, pool.getName()));

           if (callback != null) {
               callback.onSuccess(vmInstance);
           }
       } catch (Exception e) {
           LOGGER.error(String.format("Failed to assign desktop for user \"%s\" from pool \"%s\"", userUuid, pool.getName()), e);

           if (callback != null) {
               callback.onFailure(e);
           }
       }
   }

   int allocateDesktops(String poolUuid, boolean rebalance) {
       // Only cluster master is enforcing pool size.
       if (!isClusterMaster()) {
           return 0;
       }

       LOGGER.info(String.format("Allocating desktops for pool id=\"%s\", rebalance=%s...", poolUuid, rebalance ? "true" : "false"));

       DesktopHandler desktopHandler = null;
       synchronized(_poolDesktopHandlers) {
           desktopHandler = _poolDesktopHandlers.get(poolUuid);
       }

       return (desktopHandler != null) ? desktopHandler.allocateDesktops(rebalance) : 0;
   }

   boolean recomposeDesktops(String poolUuid, boolean forced) {
       // Only cluster master is enforcing pool size.
       if (!isClusterMaster()) {
           return false;
       }

       LOGGER.info(String.format("Recomposing desktops for pool id=\"%s\"...", poolUuid));

       DesktopHandler desktopHandler = null;
       synchronized(_poolDesktopHandlers) {
           desktopHandler = _poolDesktopHandlers.get(poolUuid);
       }

       return (desktopHandler != null) ? desktopHandler.recomposeDesktops(forced) : false;
   }

   boolean rebalanceDesktops(String poolUuid) {
       // Only cluster master is enforcing pool size.
       if (!isClusterMaster()) {
           return false;
       }

       LOGGER.info(String.format("Rebalancing desktops for pool id=\"%s\"...", poolUuid));

       Pool pool = _poolService.getByUuid(poolUuid);
       if (pool == null) {
           LOGGER.error(String.format("Unable to find Pool id=\"%s\" in db", poolUuid));
           return false;
       }
       if (Pool.Status.Deleting == pool.getStatus()) {
           LOGGER.error(String.format("Pool \"%s\" is being deleted", pool.getName()));
           return false;
       }

       enforcePoolSize(pool);

       return true;
   }

   VmInstance getAssignedDesktop(String poolUuid, String userUuid) {
       LOGGER.info(String.format("Getting assigned desktop to user id=\"%s\" from pool id=\"%s\"...", userUuid, poolUuid));

       DesktopHandler desktopHandler = null;
       synchronized(_poolDesktopHandlers) {
           desktopHandler = _poolDesktopHandlers.get(poolUuid);
       }

       return (desktopHandler != null) ? desktopHandler.getAssignedDesktop(userUuid) : null;
   }

   VmInstance assignDesktop(String poolUuid, String userUuid) {
       LOGGER.info(String.format("Assigning desktop to user id=\"%s\" from pool id=\"%s\"...", userUuid, poolUuid));

       DesktopHandler desktopHandler = null;
       synchronized(_poolDesktopHandlers) {
           desktopHandler = _poolDesktopHandlers.get(poolUuid);
       }

       return (desktopHandler != null) ? desktopHandler.assignDesktop(userUuid) : null;
   }

   VmInstance assignLocalDesktopToUser(String poolUuid, String userUuid) {
       LOGGER.info(String.format("Assigning local desktop to user id=\"%s\" from pool id=\"%s\"...", userUuid, poolUuid));

       DesktopHandler desktopHandler = null;
       synchronized(_poolDesktopHandlers) {
           desktopHandler = _poolDesktopHandlers.get(poolUuid);
       }

       return (desktopHandler != null) ? desktopHandler.assignLocalDesktopTo(userUuid) : null;
   }

   void refreshLoggedOffDesktops(Pool pool) {
       //LOGGER.info(String.format("Refreshing local logged off desktops in pool \"%s\"(id=\"%s\")...", pool.getName(), pool.getUuid()));

       DesktopHandler desktopHandler = null;
       synchronized(_poolDesktopHandlers) {
           desktopHandler = _poolDesktopHandlers.get(pool.getUuid());
       }

       if (desktopHandler != null) {
           desktopHandler.refreshLoggedOffDesktops();
       }
   }

   private class DesktopHandler {
       private final String _poolUuid;

       DesktopHandler(String poolUuid) {
           _poolUuid = poolUuid;
       }

       synchronized int allocateDesktops(boolean rebalance) {
           Pool pool = _poolService.getByUuid(_poolUuid);
           if (pool == null) {
               LOGGER.warn(String.format("Unable to find pool id=\"%s\"", _poolUuid));
               return 0;
           }
           if (Pool.Status.Deleting == pool.getStatus()) {
               LOGGER.warn(String.format("Pool \"%s\" is being deleted", pool.getName()));
               return 0;
           }

           VmImage vmImage = _vmImageService.getByUuid(pool.getVmImageUuid());
           if (vmImage == null) {
               LOGGER.warn(String.format("Unable to find image for pool \"%s\"", pool.getName()));
               return 0;
           }

           List<HostAllocation> hostAllocations = getHostAllocations(pool, rebalance);
           if (hostAllocations == null || hostAllocations.isEmpty()) {
               LOGGER.error(String.format("No valid hosts found for desktop allocation for pool id=\"%s\"", _poolUuid));
               return 0;
           }

           boolean poolUpdated = false;
           if (Pool.Status.Provisioning != pool.getStatus()) {
               pool.setStatus(Pool.Status.Provisioning);
               poolUpdated = true;
           }
           if (StringUtils.isEmpty(pool.getInstantiateHostUuid())) {
               // Assign a host to instantiate the pool base.
               Random rand = new Random();
               HostAllocation hostAllocation = hostAllocations.get(rand.nextInt(hostAllocations.size()));
               pool.setInstantiateHostUuid(hostAllocation.host.getUuid());
               poolUpdated = true;
           }
           if (poolUpdated) {
               pool.setUpdateTime(System.currentTimeMillis());
               _poolService.commit(pool);
           }

           long totalDesktopCount = calculateHostAllocations(hostAllocations, pool);

           long readyDesktopCount = _vmInstanceService.countByPoolAndStatus(pool.getUuid(), VmInstance.GREEN_STATUSES);
           _trackerManager.updateProgress(pool.getUuid(), (int) readyDesktopCount, null, "Provisioning...");

           // Order hostAllocations from the highest createCount to the lowest so that
           // more desktops are created on the less loaded hosts.
           Collections.sort(hostAllocations, new Comparator<HostAllocation>() {
               @Override
               public int compare(HostAllocation o1, HostAllocation o2) {
                   if (o1.createCount > o2.createCount) {
                       return -1;
                   } else if (o1.createCount < o2.createCount) {
                       return 1;
                   } else {
                       return 0;
                   }
               }
           });

           long sequenceNum = pool.getSequence();
           int createdCount = 0;

           outer:
           for (HostAllocation hostAllocation : hostAllocations) {
               if (hostAllocation.createCount <= 0) {
                   continue;
               }

               for (int i = 0; i < hostAllocation.createCount; i++) {
                   if (totalDesktopCount >= pool.getMaxSize()) {
                       break outer;
                   }

                   Host host = hostAllocation.host;

                   VmInstance vmInstance = _vmInstanceService.newInstance();

                   vmInstance.setUpdateTime(0);
                   vmInstance.setPoolUuid(pool.getUuid());
                   vmInstance.setDiskImageUuid("");
                   vmInstance.setHostUuid(host.getUuid());
                   vmInstance.setStorageUuid("");
                   vmInstance.setVmRefId("");
                   vmInstance.setUserUuid("");
                   vmInstance.setStatus(VmInstance.Status.New);

                   String prefix = pool.getPrefix();
                   if (!prefix.endsWith("-")) {
                       prefix += "-";
                   }

                   int retry = 100;
                   while (true) {
                       try {
                           vmInstance.setName(prefix + sequenceNum);
                           ++sequenceNum;

                           _vmInstanceService.commit(vmInstance);
                           break;
                       } catch (Exception e) {
                           if (retry-- > 0) {
                               LOGGER.warn(String.format("Failed to allocate desktop \"%s\" for pool \"%s\" on host \"%s\"(%s). Retrying...",
                                       vmInstance.getName(), pool.getName(), host.getAddress(), host.getUuid()));
                               continue;
                           } else {
                               LOGGER.warn(String.format("Failed to allocate desktop \"%s\" for pool \"%s\" on host \"%s\"(%s). Retrying...",
                                       vmInstance.getName(), pool.getName(), host.getAddress(), host.getUuid()), e);
                               break outer;
                           }
                       }
                   }

                   LOGGER.info(String.format("Allocated desktop \"%s\" for pool \"%s\" on host \"%s\"(%s)",
                           vmInstance.getName(), pool.getName(), host.getAddress(), host.getUuid()));

                   ++createdCount;
                   ++totalDesktopCount;
               }
           }

           if (createdCount > 0) {
               pool.setSequence(sequenceNum);
               pool.setUpdateTime(System.currentTimeMillis());
               _poolService.commit(pool);
           }

           return createdCount;
       }

       synchronized boolean recomposeDesktops(boolean forced) {
           Pool pool = _poolService.getByUuid(_poolUuid);
           if (pool == null) {
               LOGGER.warn(String.format("Unable to find pool id=\"%s\"", _poolUuid));
               return false;
           }
           if (Pool.Status.Deleting == pool.getStatus()) {
               LOGGER.warn(String.format("Pool \"%s\" is being deleted", pool.getName()));
               return false;
           }

           VmImage vmImage = _vmImageService.getByUuid(pool.getVmImageUuid());
           if (vmImage == null) {
               LOGGER.warn(String.format("Unable to find image for pool \"%s\"", pool.getName()));
               return false;
           }

           boolean toRecompose = true;

           while (!forced) {
               long imageVersion = vmImage.getVersion();
               if (imageVersion <= 0) {
                   DiskImage diskImage = _diskImageService.getLocalDiskImageByGuid(vmImage.getCurrentDiskImageGuid());
                   if (diskImage != null) {
                       imageVersion = diskImage.getVersion();
                   }
               }

               if (imageVersion <= 0) {
                   // Give it another try anyway.
                   break;
               }

               // Check pool image version.
               String poolBaseDiskImageGuid = pool.getBaseDiskImageGuid();
               if (StringUtils.isEmpty(poolBaseDiskImageGuid)) {
                   break;
               }

               List<? extends DiskImage> diskImages = _diskImageService.getDiskImagesByGuid(poolBaseDiskImageGuid);
               if (diskImages == null || diskImages.isEmpty()) {
                   toRecompose = false;
                   break;
               }

               if (diskImages.get(0).getVersion() == imageVersion) {
                   toRecompose = false;
               }

               break;
           }

           if (toRecompose) {
               List<HostAllocation> hostAllocations = getHostAllocations(pool, false);
               if (hostAllocations == null || hostAllocations.isEmpty()) {
                   LOGGER.error(String.format("No valid hosts found for desktop allocation for pool id=\"%s\"", _poolUuid));
                   return false;
               }

               // Assign a host to instantiate the pool base.
               Random rand = new Random();
               HostAllocation hostAllocation = hostAllocations.get(rand.nextInt(hostAllocations.size()));
               pool.setInstantiateHostUuid(hostAllocation.host.getUuid());
               pool.setBaseDiskImageGuid(IdGenerator.generateUuid());
               pool.setStatus(Pool.Status.Recomposing);
               pool.setUpdateTime(System.currentTimeMillis());
               _poolService.commit(pool);

               _trackerManager.updateProgress(pool.getUuid(), 0, null, "Upgrading...");
           }

           return true;
       }

       private class HostAllocation {
           Host host;
           long hostCapacity;
           @SuppressWarnings("unused")
           List<? extends Storage> storages;
           long desktopCount;
           long createCount = 0;
       }

       private List<HostAllocation> getHostAllocations(Pool pool, boolean rebalance) {
           List<HostAllocation> hostAllocations = new ArrayList<HostAllocation>();

           List<? extends Host> hosts = _hostService.getAll(null);
           if (hosts != null) {
               for (Host host : hosts) {
                   if (Host.Status.Connected != host.getStatus()) {
                       // If rebalancing, exclude disconnected hosts.
                       continue;
                   }

                   Server server = _serverService.getByUuid(host.getServerUuid());
                   if (server == null || server.getStatus() != Server.Status.Running) {
                       continue;
                   }

                   List<? extends Storage> storages = _storageService.getHostStorages(host.getUuid());
                   long desktopCount = _vmInstanceService.countByPoolAndStatusOnHost(host.getUuid(), pool.getUuid());

                   HostAllocation hostAllocation = new HostAllocation();
                   hostAllocation.host = host;
                   hostAllocation.hostCapacity = getHostCapacity(host);
                   hostAllocation.storages = storages;
                   hostAllocation.desktopCount = desktopCount;

                   hostAllocations.add(hostAllocation);
               }
           }

           return hostAllocations;
       }

       private long calculateHostAllocations(List<HostAllocation> hostAllocations, Pool pool) {
           long totalDesktopCount = 0;

           long totalCapacity = 0;
           for (HostAllocation hostAllocation : hostAllocations) {
               totalCapacity += hostAllocation.hostCapacity;
           }

           long poolSize = pool.getMaxSize();

           for (HostAllocation hostAllocation : hostAllocations) {
               long deservedCount = (hostAllocation.hostCapacity * poolSize + totalCapacity - 1) / totalCapacity;

               hostAllocation.createCount = deservedCount - hostAllocation.desktopCount;

               totalDesktopCount += hostAllocation.desktopCount;

               LOGGER.info(String.format("Host \"%s\" should have %d desktops for pool \"%s\", right now %d desktops",
                       hostAllocation.host.getAddress(), deservedCount, pool.getName(), hostAllocation.desktopCount));
           }

           return totalDesktopCount;
       }

       // Return host capacity. Right now only consider memory as capacity.
       private long getHostCapacity(Host host) {
           return host.getMemoryCapacityMB();
       }

       synchronized void enforcePoolSize(Pool pool) {
           // Check desktop count for this pool
           long desktopCount = _vmInstanceService.countByPoolAndStatus(pool.getUuid());
           if (desktopCount > pool.getMaxSize()) {
               LOGGER.info(String.format("Pool \"%s\" (%s) has %d desktops - higher than max %d",
                       pool.getName(), pool.getUuid(), desktopCount, pool.getMaxSize()));

               if (Pool.Status.Provisioning == pool.getStatus()
                       || Pool.Status.Recomposing == pool.getStatus()) {
                   // Check if provisioning process is completed. If so, change status to Ready.
                   long provisioningDesktopCount = _vmInstanceService.countByPoolAndStatus(pool.getUuid(), VmInstance.PRESTARTING_STATUSES);
                   if (provisioningDesktopCount == 0) {
                       pool.setStatus(Pool.Status.Ready);
                       pool.setUpdateTime(System.currentTimeMillis());
                       _poolService.commit(pool);

                       _trackerManager.removeProgress(pool.getUuid());
                   }
               }

               long diff = desktopCount - pool.getMaxSize();

               // Delete desktops
               for (VmInstance.Status status : VmInstance.ORDERED_STATUSES) {
                   List<? extends VmInstance> vmInstances =
                           _vmInstanceService.getByPoolAndStatus(null, pool.getUuid(), status);

                   if (vmInstances != null && !vmInstances.isEmpty()) {
                       Collections.shuffle(vmInstances);

                       for (VmInstance vmInstance : vmInstances) {
                           _vmInstanceService.deleteVm(pool, vmInstance, null);

                           if (--diff == 0) {
                               break;
                           }
                       }
                   }

                   if (diff == 0) {
                       break;
                   }
               }
           } else if (desktopCount < pool.getMaxSize()) {
               // If total count of desktops <= max size, enforce pool count now.

               LOGGER.info(String.format("Pool \"%s\" (%s) has %d desktops - lower than max %d",
                       pool.getName(), pool.getUuid(), desktopCount, pool.getMaxSize()));

               // Provision desktops
               _poolService.provisionDesktops(pool, false, null);
           } else {
               if (Pool.Status.Provisioning == pool.getStatus()
                       || Pool.Status.Recomposing == pool.getStatus()) {
                   // Check if provisioning process is completed. If so, change status to Ready.
                   long provisioningDesktopCount = _vmInstanceService.countByPoolAndStatus(pool.getUuid(), VmInstance.PRESTARTING_STATUSES);
                   if (provisioningDesktopCount == 0) {
                       pool.setStatus(Pool.Status.Ready);
                       pool.setUpdateTime(System.currentTimeMillis());
                       _poolService.commit(pool);

                       _trackerManager.removeProgress(pool.getUuid());
                   }
               }
           }
       }

       synchronized void enforcePoolLocalPreStartCount(Pool pool) {
          long localDesktopCount = 0;

           switch (pool.getStatus()) {
           case Provisioning:
           case Recomposing:
               long localReadyDesktopCount = _vmInstanceService.countLocalByPoolAndStatus(pool.getUuid(), VmInstance.GREEN_STATUSES);
               localDesktopCount = _vmInstanceService.countLocalByPoolAndStatus(pool.getUuid());
               if (localDesktopCount == 0 || localReadyDesktopCount < localDesktopCount) {
                   //LOGGER.info(String.format("Pool \"%s\" (%s) is not ready for local host", pool.getName(), pool.getUuid()));
                   return;
               }
               break;

           case Ready:
               break;

           default:
               LOGGER.info(String.format("Pool \"%s\" (%s) is not ready", pool.getName(), pool.getUuid()));
               return;
           }

           if (localDesktopCount == 0) {
               localDesktopCount = _vmInstanceService.countLocalByPoolAndStatus(pool.getUuid());
           }
           long preStartTargetCount = (pool.getMinSize() * localDesktopCount + pool.getMaxSize() - 1) / pool.getMaxSize();

           // If total count of running desktops <= max size, enforce pre-start count now.

           long preStartedDesktopCount = _vmInstanceService.countLocalByPoolAndStatus(pool.getUuid(),
                   VmInstance.Status.Starting, VmInstance.Status.Started);
           if (preStartedDesktopCount > preStartTargetCount) {
               LOGGER.info(String.format("Pool \"%s\" (%s) has %d local pre-started desktops - higher than min %d",
                       pool.getName(), pool.getUuid(), preStartedDesktopCount, preStartTargetCount));

               long diff = preStartedDesktopCount - preStartTargetCount;

               // Refresh unassigned running desktops
               for (VmInstance.Status status : new VmInstance.Status[] { VmInstance.Status.Starting, VmInstance.Status.Started }) {
                   List<? extends VmInstance> vmInstances = _vmInstanceService.getLocalByPoolAndStatus(pool.getUuid(), status);

                   if (vmInstances != null && !vmInstances.isEmpty()) {
                       Collections.shuffle(vmInstances);

                       for (VmInstance vmInstance : vmInstances) {
                           _vmInstanceService.refreshVm(pool, vmInstance, null);

                           if (--diff == 0) {
                               break;
                           }
                       }
                   }

                   if (diff == 0) {
                       break;
                   }
               }
           } else if (preStartedDesktopCount < preStartTargetCount) {
               LOGGER.info(String.format("Pool \"%s\" (%s) has %d local pre-started desktops - lower than min %d",
                       pool.getName(), pool.getUuid(), preStartedDesktopCount, preStartTargetCount));

               long countToStart = preStartTargetCount - preStartedDesktopCount;

               List<? extends VmInstance> readyVmInstances =
                       _vmInstanceService.getLocalByPoolAndStatus(pool.getUuid(), VmInstance.Status.Ready);
               if (readyVmInstances != null && !readyVmInstances.isEmpty()) {
                   Collections.shuffle(readyVmInstances);

                   for (VmInstance vmInstance : readyVmInstances) {
                       _vmInstanceService.powerOnVm(pool, vmInstance, null);

                       if (--countToStart == 0) {
                           break;
                       }
                   }
               }
           }
       }

       synchronized VmInstance getAssignedDesktop(String userUuid) {
           Pool pool = _poolService.getByUuid(_poolUuid);
           if (pool == null) {
               LOGGER.warn(String.format("Unable to find pool \"%s\"", _poolUuid));
               return null;
           }
           if (Pool.Status.Deleting == pool.getStatus()) {
               LOGGER.warn(String.format("Pool \"%s\" is being deleted", pool.getName()));
               return null;
           }

           List<? extends VmInstance> desktops = _vmInstanceService.getAllInPoolByUser(pool.getUuid(), userUuid);
           if (desktops == null || desktops.isEmpty()) {
               return null;
           }

           // Try to find a desktop that's available.
           for (VmInstance desktop : desktops) {
               switch (desktop.getStatus()) {
               case Destroying:
               case Destroyed:
                   continue;
               }

               Host host = _hostService.getByUuid(desktop.getHostUuid());
               if (host == null || host.getStatus() != Host.Status.Connected) {
                   continue;
               }

               Server server = _serverService.getByUuid(host.getServerUuid());
               if (server == null || server.getStatus() != Server.Status.Running) {
                   continue;
               }

               _vmInstanceService.powerOnVm(pool, desktop, null);
               return desktop;
           }

           // For persistent desktop, pick the first desktop and reassign a host to relink.
           if (Pool.Type.Persistent == pool.getType()) {
               VmInstance readyInstance = null;
               for (VmInstance desktop : desktops) {
                   switch (desktop.getStatus()) {
                   case Destroying:
                   case Destroyed:
                       continue;
                   }

                   readyInstance = desktop;
                   break;
               }

               if (readyInstance == null) {
                   return null;
               }

               List<? extends Server> servers = _serverService.getRunningServers();
               if (servers == null || servers.isEmpty()) {
                   return null;
               }

               Collections.shuffle(servers);

               for (Server server : servers) {
                   List<? extends Host> hosts = _hostService.getHostsForServer(server.getUuid());
                   if (hosts == null || hosts.isEmpty()) {
                       continue;
                   }

                   for (Host host : hosts) {
                       if (host.getStatus() != Host.Status.Connected) {
                           continue;
                       }
                       if (StringUtils.isEmpty(host.getSharedDatastoreRefId())) {
                           continue;
                       }

                       readyInstance.setHostUuid(host.getUuid());
                       // Have the instance relink to local replica.
                       _vmInstanceService.powerOnVm(pool, readyInstance, null);
                       return readyInstance;
                   }
               }
           }

            return null;
       }

       synchronized VmInstance assignDesktop(String userUuid) {
           Pool pool = _poolService.getByUuid(_poolUuid);
           if (pool == null) {
               LOGGER.warn(String.format("Unable to find pool \"%s\"", _poolUuid));
               return null;
           }
           if (Pool.Status.Deleting == pool.getStatus()) {
               LOGGER.warn(String.format("Pool \"%s\" is being deleted", pool.getName()));
               return null;
           }

           List<? extends VmInstance> desktops = _vmInstanceService.getAllInPoolWithNoUser(pool.getUuid());
           if (desktops == null || desktops.isEmpty()) {
               return null;
           }

           Collections.shuffle(desktops);

           // Try to find a desktop that's available.
           for (VmInstance desktop : desktops) {
               switch (desktop.getStatus()) {
               case Destroying:
               case Destroyed:
                   continue;
               }

               Host host = _hostService.getByUuid(desktop.getHostUuid());
               if (host == null || host.getStatus() != Host.Status.Connected) {
                   continue;
               }

               Server server = _serverService.getByUuid(host.getServerUuid());
               if (server == null || server.getStatus() != Server.Status.Running) {
                   continue;
               }

               desktop.setUserUuid(userUuid);
               _vmInstanceService.commit(desktop);

               _vmInstanceService.powerOnVm(pool, desktop, null);
               return desktop;
           }

           // For persistent desktop, pick the first desktop and reassign a host to relink.
           if (Pool.Type.Persistent == pool.getType()) {
               VmInstance readyInstance = null;
               for (VmInstance desktop : desktops) {
                   switch (desktop.getStatus()) {
                   case Destroying:
                   case Destroyed:
                       continue;
                   }

                   readyInstance = desktop;
                   break;
               }

               if (readyInstance == null) {
                   return null;
               }

               List<? extends Server> servers = _serverService.getRunningServers();
               if (servers == null || servers.isEmpty()) {
                   return null;
               }

               Collections.shuffle(servers);

               for (Server server : servers) {
                   List<? extends Host> hosts = _hostService.getHostsForServer(server.getUuid());
                   if (hosts == null || hosts.isEmpty()) {
                       continue;
                   }

                   for (Host host : hosts) {
                       if (host.getStatus() != Host.Status.Connected) {
                           continue;
                       }

                       readyInstance.setUserUuid(userUuid);
                       readyInstance.setHostUuid(host.getUuid());
                       // Have the instance relink to local replica.
                       _vmInstanceService.powerOnVm(pool, readyInstance, null);
                       return readyInstance;
                   }
               }
           }

            return null;
       }

       synchronized VmInstance assignLocalDesktopTo(String userUuid) {
           Pool pool = _poolService.getByUuid(_poolUuid);
           if (pool == null) {
               LOGGER.warn(String.format("Unable to find pool \"%s\"", _poolUuid));
               return null;
           }
           if (Pool.Status.Deleting == pool.getStatus()) {
               LOGGER.warn(String.format("Pool \"%s\" is being deleted", pool.getName()));
               return null;
           }

           boolean persistentPool = (pool.getType() == Pool.Type.Persistent);
           VmInstance readyInstance = null;

           List<? extends VmInstance> desktops = _vmInstanceService.getLocalByPoolAndStatus(_poolUuid);
           if (desktops == null || desktops.isEmpty()) {
               return null;
           }

           Collections.shuffle(desktops);

           // Find one from the available desktops.
           for (VmInstance vmInstance : desktops) {
               if (!StringUtils.isEmpty(vmInstance.getUserUuid())) {
                   // Other user is already using this desktop

                   if (!persistentPool) {
                       switch (vmInstance.getStatus()) {
                       case Assigned:
                           if (System.currentTimeMillis() - vmInstance.getUpdateTime() > 300 * 1000) {
                               // If after 5 min still not in session, revoke the desktop now
                               vmInstance.setUserUuid("");
                               vmInstance.setAssigned(false);
                               vmInstance.setStatus(VmInstance.Status.Started);
                           }
                           break;

                       case LoggedOff:
                           if (StringUtils.equals(userUuid, vmInstance.getUserUuid())) {
                               vmInstance.setStatus(VmInstance.Status.Assigned);
                               vmInstance.setUpdateTime(System.currentTimeMillis());
                               _vmInstanceService.commit(vmInstance);
                               return vmInstance;
                           }
                           break;
                       }
                   } else {
                       //LOGGER.info(String.format("Desktop \"%s\" has been assigned to user id=\"%s\"",
                       //        vmInstance.getName(), vmInstance.getUserUuid()));
                       continue;
                   }
               }

               if (VmInstance.Status.Started == vmInstance.getStatus()) {
                   readyInstance = vmInstance;
                   break;
               } else {
                   // Is it potentially becoming Ready
                   switch (vmInstance.getStatus()) {
                   case New:
                   case Provisioning:
                   case Creating:
                   case ObtainingIp:
                   case TakingSnapshot:
                   case Stopping:
                   case Refreshing:
                   case Starting:
                       if (readyInstance == null) {
                           readyInstance = vmInstance;
                       }
                       break;

                   case LoggedOff:
                       if (readyInstance == null && Pool.Type.Persistent != pool.getType()) {
                           // Assign to another user only when it's not persistent pool.
                           readyInstance = vmInstance;
                       }
                       break;

                   case Ready:
                       if ((readyInstance == null || VmInstance.Status.Ready != readyInstance.getStatus())
                               && !StringUtils.isEmpty(vmInstance.getIpAddress())) {
                           readyInstance = vmInstance;
                       }
                       break;
                   }

                   continue;
               }
           }

           if (readyInstance != null) {
               boolean toPowerOn = false;
               boolean toRefresh = false;

               if (VmInstance.Status.Started == readyInstance.getStatus()) {
                   // This VM is in Started state. Change to Assigning and let agent prepare the assigning
                   LOGGER.info(String.format("Assigning desktop \"%s\"(%s) to user \"%s\"",
                           readyInstance.getName(), readyInstance.getIpAddress(), userUuid));
                   readyInstance.setStatus(VmInstance.Status.Assigning);
               } else if (VmInstance.Status.Ready == readyInstance.getStatus()
                       || (VmInstance.Status.Stopping == readyInstance.getStatus()
                           && Pool.Type.Persistent == pool.getType())) {
                   // If there's Ready vmInstance, use it...
                   LOGGER.info(String.format("Powering on desktop \"%s\" to assign to user \"%s\"",
                           readyInstance.getName(), userUuid));
                   toPowerOn = true;
               } else if (VmInstance.Status.LoggedOff == readyInstance.getStatus()) {
                   LOGGER.info(String.format("Refreshing desktop \"%s\" to assign to user \"%s\"",
                           readyInstance.getName(), userUuid));
                   toRefresh = true;
               }

               readyInstance.setUserUuid(userUuid);
               readyInstance.setUpdateTime(System.currentTimeMillis());
               _vmInstanceService.commit(readyInstance);

               if (toPowerOn) {
                   _vmInstanceService.powerOnVm(pool, readyInstance, null);
               } else if (toRefresh) {
                   _vmInstanceService.refreshVm(pool, readyInstance, null);
               }
           }

           return readyInstance;
       }

       synchronized void refreshLoggedOffDesktops() {
           Pool pool = _poolService.getByUuid(_poolUuid);
           if (pool == null) {
               LOGGER.warn(String.format("Unable to find pool \"%s\"", _poolUuid));
               return;
           }
           if (Pool.Status.Deleting == pool.getStatus()) {
               LOGGER.warn(String.format("Pool \"%s\" is being deleted", pool.getName()));
               return;
           }

           List<? extends VmInstance> desktops = _vmInstanceService.getLocalByPoolAndStatus(_poolUuid, VmInstance.Status.LoggedOff);
           if (desktops == null || desktops.isEmpty()) {
               return;
           }

           long currrentTime = System.currentTimeMillis();

           for (VmInstance desktop : desktops) {
               if (currrentTime - desktop.getUpdateTime() > 60 * 1000) {
                   if (Pool.Type.Persistent == pool.getType()) {
                       _vmInstanceService.powerOffVm(pool, desktop, null);
                   } else {
                       desktop.setUserUuid("");
                       _vmInstanceService.refreshVm(pool, desktop, null);
                   }
               }
           }
       }
   }

   private void reportDesktopRequest(VmInstance vmInstance) {
       RequestDesktopEvent event = null;
       synchronized (_desktopRequestMap) {
           event = _desktopRequestMap.remove(vmInstance.getUuid());
       }

       if (event != null) {
           Pool pool = event.getPool();
           String userUuid = event.getUserUuid();
           String requestUuid = String.format("%s:%s", userUuid, pool.getUuid());
           _trackerManager.removeProgress(requestUuid);

           if (event.getCallback() != null) {
               switch (vmInstance.getStatus()) {
               case Assigned:
               case InSession:
                   LOGGER.info(String.format("Assigned desktop \"%s\"(%s) to user \"%s\" from pool \"%s\"",
                           vmInstance.getName(), vmInstance.getIpAddress(), event.getUserUuid(), event.getPool().getName()));
                   event.getCallback().onSuccess(vmInstance);
                   break;

               case AssignError:
                   LOGGER.error(String.format("Failed to assigned desktop \"%s\"(%s) to user \"%s\" from pool \"%s\"",
                           vmInstance.getName(), vmInstance.getIpAddress(), event.getUserUuid(), event.getPool().getName()));
                   event.getCallback().onFailure(new Exception("Failed to assign desktop to the user"));
                   break;

               default:
                   LOGGER.error(String.format("Failed to start assigned desktop \"%s\" to user \"%s\" from pool \"%s\"",
                           vmInstance.getName(), event.getUserUuid(), event.getPool().getName()));
                   event.getCallback().onFailure(new Exception("Desktop failed to start"));
                   break;
               }
           }
       }
   }

   private void powerOnVmInstance(Host localHost, Pool pool, VmInstance vmInstance) {
       if (vmInstance == null || StringUtils.isEmpty(vmInstance.getVmRefId())) {
           return;
       }

       if (pool == null) {
           pool = _poolService.getByUuid(vmInstance.getPoolUuid());
       }
       if (pool == null) {
           LOGGER.warn(String.format("Failed to find pool for VmInstance \"%s\" power on", vmInstance.getName()));
           return;
       }
       if (Pool.Status.Deleting == pool.getStatus()) {
           LOGGER.warn(String.format("Pool \"%s\" is being deleted", pool.getName()));
           return;
       }

       if (localHost == null) {
           localHost = _hostService.getLocalHost();
       }
       if (localHost == null) {
           LOGGER.warn("Unable to find local host");
           return;
       }

       if (Host.Status.Connected != localHost.getStatus()) {
           LOGGER.warn("Local host is not connected. Ignore powerOnPool");
           return;
       }

       LOGGER.info(String.format("VmInstance \"%s\" is starting...", vmInstance.getName()));

       if (vmInstance instanceof VmInstanceVO) {
           ((VmInstanceVO) vmInstance).inOperation(true);
       }

       // Call StartDesktopFlow only if it's local VmInstance.
       StartDesktopFlow wf = new StartDesktopFlow(localHost, pool, vmInstance, null);
       wf.run();
   }

   private void powerOffVmInstance(Host localHost, Pool pool, VmInstance vmInstance) {
       if (vmInstance == null || StringUtils.isEmpty(vmInstance.getVmRefId())) {
           return;
       }

       if (pool == null) {
           pool = _poolService.getByUuid(vmInstance.getPoolUuid());
       }
       if (pool == null) {
           LOGGER.warn(String.format("Failed to find pool for VmInstance \"%s\" power off", vmInstance.getName()));
           return;
       }
       if (Pool.Status.Deleting == pool.getStatus()) {
           LOGGER.warn(String.format("Pool \"%s\" is being deleted", pool.getName()));
           return;
       }

       if (localHost == null) {
           localHost = _hostService.getLocalHost();
       }
       if (localHost == null) {
           LOGGER.warn("Unable to find local host");
           return;
       }

       if (Host.Status.Connected != localHost.getStatus()) {
           LOGGER.warn("Local host is not connected. Ignore powerOffPool");
           return;
       }

       LOGGER.info(String.format("VmInstance \"%s\" is stopping...", vmInstance.getName()));

       if (vmInstance instanceof VmInstanceVO) {
           ((VmInstanceVO) vmInstance).inOperation(true);
       }

       // Call StopDesktopFlow only if it's local VmInstance.
       StopDesktopFlow wf = new StopDesktopFlow(localHost, pool, vmInstance, null);
       wf.run();
   }

   private void refreshVmInstance(Host localHost, Pool pool, VmInstance vmInstance) {
       if (vmInstance == null || StringUtils.isEmpty(vmInstance.getVmRefId())) {
           return;
       }

       if (pool == null) {
           pool = _poolService.getByUuid(vmInstance.getPoolUuid());
       }
       if (pool == null) {
           LOGGER.warn(String.format("Failed to find pool for VmInstance \"%s\" refreshing", vmInstance.getName()));
           return;
       }
       if (Pool.Status.Deleting == pool.getStatus()) {
           LOGGER.warn(String.format("Pool \"%s\" is being deleted", pool.getName()));
           return;
       }

       if (localHost == null) {
           localHost = _hostService.getLocalHost();
       }
       if (localHost == null) {
           LOGGER.warn("Unable to find local host");
           return;
       }

       if (Host.Status.Connected != localHost.getStatus()) {
           LOGGER.warn("Local host is not connected. Ignore powerOnPool");
           return;
       }

       LOGGER.info(String.format("VmInstance \"%s\" is refreshing...", vmInstance.getName()));

       if (vmInstance instanceof VmInstanceVO) {
           ((VmInstanceVO) vmInstance).inOperation(true);
       }

       // Call RefreshDesktopFlow only if it's local VmInstance.
       RefreshDesktopFlow wf = new RefreshDesktopFlow(localHost, pool, vmInstance, null);
       wf.run();
   }

   private void recomposePool(final Pool pool) {
       List<? extends VmInstance> vmInstances =
               _vmInstanceService.getLocalVmInstancesByPool(pool.getUuid());
       if (vmInstances == null || vmInstances.isEmpty()) {
           return;
       }

       String poolBaseDiskImageGuid = pool.getBaseDiskImageGuid();
       if (StringUtils.isEmpty(poolBaseDiskImageGuid)) {
           LOGGER.warn(String.format("Pool \"%s\" doesn't have a valid base disk image (guid=%s)",
                   pool.getName(), poolBaseDiskImageGuid));
           return;
       }

       String localBaseDiskImageUuid = null;
       DiskImage baseDiskImage = _diskImageService.getLocalDiskImageByGuid(poolBaseDiskImageGuid);
       if (baseDiskImage == null) {
           LOGGER.warn(String.format("Pool \"%s\" doesn't have a valid base disk image (guid=%s)",
                   pool.getName(), poolBaseDiskImageGuid));
       } else {
           localBaseDiskImageUuid = baseDiskImage.getUuid();
       }

       final DiskImage oldBaseDiskImage[] = new DiskImage[] { null };
       final Object lock = new Object();
       final int countToRecompose[] = new int[] { 0 };

       synchronized (lock) {
           for (VmInstance vmInstance : vmInstances) {
               if (StringUtils.equals(localBaseDiskImageUuid, vmInstance.getDiskImageUuid())) {
                   continue;
               }
               if (oldBaseDiskImage[0] == null) {
                   oldBaseDiskImage[0] = _diskImageService.getByUuid(vmInstance.getDiskImageUuid());
               }

               if (VmInstance.Status.New == vmInstance.getStatus()) {
                   continue;
               }

               vmInstance.setRecompose(true);
               _vmInstanceService.commit(vmInstance);

               _vmInstanceService.deleteVm(pool, vmInstance, new AsyncCallback() {
                   @Override
                   public void onSuccess(Object result) {
                       checkCount();
                   }

                   @Override
                   public void onFailure(Throwable t) {
                       checkCount();
                   }

                   @Override
                   public void progress(int percentage) {
                   }

                   @Override
                   public String getRequestor() {
                       return null;
                   }

                   private void checkCount() {
                       int count;
                       synchronized (lock) {
                           count = (--countToRecompose[0]);
                       }

                       if (count == 0 && oldBaseDiskImage[0] != null) {
                           _diskImageService.deleteDiskImage(oldBaseDiskImage[0], null);
                       }
                   }
               });

               ++countToRecompose[0];
           }

           if (countToRecompose[0] == 0 && oldBaseDiskImage[0] != null) {
               _diskImageService.deleteDiskImage(oldBaseDiskImage[0], null);
           }
       }
   }

   private void deletePool(final Pool pool) {
       // Delete all Unknown VmInstances.
       List<? extends VmInstance> unknownVmInstances =
               _vmInstanceService.getByPoolAndStatus(null, pool.getUuid(), VmInstance.Status.Unknown);
       if (unknownVmInstances != null && !unknownVmInstances.isEmpty()) {
           for (VmInstance vmInstance : unknownVmInstances) {
               _vmInstanceService.delete(vmInstance);
           }
       }

       _poolService.deletePool(pool, null);
   }

    private class PoolChangeListenerImpl implements ObjectPropertyChangeListener {
        @Override
        public void newObject(ObjectPropertyChangeEvent event) {
            final Pool pool = (Pool) event.getSource();

            if (!pool.isEnabled()) {
                LOGGER.info(String.format("Pool \"%s\" (%s) is disabled", pool.getName(), pool.getUuid()));
                return;
            }

            if (Pool.Status.Deleting != pool.getStatus()) {
                // Register the pool if it's a new one replicated from cluster.
                registerPool(pool);
            }

            switch (pool.getStatus()) {
            case Provisioning:
            case Recomposing:
            case Ready:
                processNewPool(pool);
                break;

            case Deleting:
                deletePool(pool);
                break;
            }
        }

        @Override
        public void deleteObject(ObjectPropertyChangeEvent event) {
            final Pool pool = (Pool) event.getSource();
            unregisterPool(pool);
            _poolUserService.deleteByPool(pool.getUuid());
        }

        @Override
        public void propertyChange(ObjectPropertyChangeEvent event) {
            final Pool pool = (Pool) event.getSource();
            final Map<String, Pair<Object, Object>> changedProperties = event.getChangedProperties();

            _asyncExecutor.run(new AsyncExecutor.Job() {
                @Override
                public Object run() {
                    if (changedProperties != null) {
                        Pair<Object, Object> enables = changedProperties.get(Pool.PROP_ENABLED);
                        if (enables != null) {
                            if (pool.isEnabled()) {
                                LOGGER.info(String.format("Pool \"%s\" (%s) is enabled", pool.getName(), pool.getUuid()));
                                startPool(pool);
                            } else {
                                LOGGER.info(String.format("Pool \"%s\" (%s) is disabled", pool.getName(), pool.getUuid()));
                                stopPool(pool);
                            }
                            return null;
                        }

                        if (!pool.isEnabled()) {
                            LOGGER.info(String.format("Pool \"%s\" (%s) is disabled", pool.getName(), pool.getUuid()));
                            return null;
                        }

                        boolean recomposed = false;

                        Pair<Object, Object> statuses = changedProperties.get(Pool.PROP_STATUS);
                        if (statuses != null) {
                            switch (pool.getStatus()) {
                            case Provisioning:
                                processNewPool(pool);
                                break;

                            case Recomposing:
                                processNewPool(pool);
                                recomposePool(pool);
                                recomposed = true;
                                break;

                            case Deleting:
                                // Some node initiated deleting pool.
                                unregisterPool(pool);
                                deletePool(pool);
                                break;
                            }
                        }

                        if (!recomposed) {
                            Pair<Object, Object> baseImageGuids = changedProperties.get(Pool.PROP_BASEIMAGE_GUID);
                            if (baseImageGuids != null) {
                                processNewPool(pool);
                                recomposePool(pool);
                            }
                        }

                        Pair<Object, Object> minSizes = changedProperties.get(Pool.PROP_MINSIZE);
                        if (minSizes != null && Pool.Status.Deleting != pool.getStatus()) {
                            enforcePoolLocalPreStartCount(pool/*, false*/);
                        }
                    }

                    if (isClusterMaster()) {
                        if (Pool.Status.Deleting != pool.getStatus()) {
                            boolean toEnforcePoolSize = false;

                            if (changedProperties != null) {
                                // Enforece pool size.
                                Pair<Object, Object> maxSizes = changedProperties.get(Pool.PROP_MAXSIZE);
                                if (maxSizes != null) {
                                    toEnforcePoolSize = true;
                                }
                            }

                            if (toEnforcePoolSize) {
                                enforcePoolSize(pool);
                            }
                        }
                    }

                    return null;
                }
            }, null);
        }
    }

    private class VmInstanceChangeListenerImpl implements ObjectPropertyChangeListener {
        @Override
        public void newObject(ObjectPropertyChangeEvent event) {
            final VmInstance vmInstance = (VmInstance) event.getSource();
            _asyncExecutor.run(new AsyncExecutor.Job() {
                @Override
                public Object run() {
                    if (!_vmInstanceService.isLocalVmInstance(vmInstance)) {
                        // Not local vmInstance.
                        return null;
                    }

                    if (VmInstance.Status.New == vmInstance.getStatus()) {
                        // This is provision desktop request from cluster master.

                        Pool pool = _poolService.getByUuid(vmInstance.getPoolUuid());
                        if (pool == null) {
                            LOGGER.warn(String.format("Unable to find pool for VmInstance \"%s\"", vmInstance.getName()));
                            return null;
                        }
                        if (Pool.Status.Deleting == pool.getStatus()) {
                            LOGGER.warn(String.format("Pool \"%s\" is being deleted", pool.getName()));
                            return null;
                        }

                        _poolService.provisionLocalDesktop(pool, vmInstance, null);
                    }
                    return null;
                }
            }, null);
        }

        @Override
        public void deleteObject(ObjectPropertyChangeEvent event) {
            final VmInstance vmInstance = (VmInstance) event.getSource();
            _asyncExecutor.run(new AsyncExecutor.Job() {
                @Override
                public Object run() {
                    onVmInstanceDelete(vmInstance);
                    return null;
                }
            }, null);
        }

        @Override
        public void propertyChange(ObjectPropertyChangeEvent event) {
            final VmInstance vmInstance = (VmInstance) event.getSource();
            final Map<String, Pair<Object, Object>> changedProperties = event.getChangedProperties();
            _asyncExecutor.run(new AsyncExecutor.Job() {
                @Override
                public Object run() {
                    synchronized (vmInstance) {
                        processVmInstanceChange(vmInstance, changedProperties);
                    }
                    return null;
                }
            }, null);
        }
    }

    private class VmStateListenerImpl implements VmStateListener {
        @Override
        public void onNew(String vmRefId, VmStateListener.State state) {
            // No need to handle new VM
        }

        @Override
        public void onDelete(String vmRefId) {
            // Check if this is a desktop. If so, enforce the pool policy.
            VmInstance vmInstance = _vmInstanceService.getLocalVmInstanceByVmRefId(vmRefId);
            if (vmInstance == null) {
                return;
            }

            if (vmInstance.getStatus() != VmInstance.Status.Destroying
                    && vmInstance.getStatus() != VmInstance.Status.Destroyed) {
                // Delete VmInstance.
                _vmInstanceService.deleteVm(null, vmInstance, null);
            }
        }

        @Override
        public void onPowerStateChange(String vmRefId, VmStateListener.State state) {
            // The vm is allowed to be in powered off state. We will power it on when
            // assigning to user.

            // Check if this is a desktop. If so, enforce the pool policy.
            VmInstance vmInstance = _vmInstanceService.getLocalVmInstanceByVmRefId(vmRefId);
            if (vmInstance == null) {
                return;
            }

            if (!VmStateListener.State.PoweredOn.equals(state)) {
                switch (vmInstance.getStatus()) {
                case Started:
                case Starting:
                case Assigning:
                case Assigned:
                    Pool pool = _poolService.getByUuid(vmInstance.getPoolUuid());
                    if (pool == null) {
                        LOGGER.warn(String.format("Failed to find pool for VmInstance \"%s\" refreshing", vmInstance.getName()));
                        return;
                    }
                    if (Pool.Status.Deleting == pool.getStatus()) {
                        LOGGER.warn(String.format("Pool \"%s\" is being deleted", pool.getName()));
                        return;
                    }

                    // This desktop is supposed to be powered on. Let's power it on.
                    Host host = _hostService.getLocalHost();
                    if (host == null) {
                        LOGGER.warn("Unable to find local host");
                        return;
                    }

                    powerOnVmInstance(host, pool, vmInstance);

                    break;
                }
            }
        }
    }
}
