/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.vdi;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import com.nimdesk.cluster.RemoteProxyFactory;
import com.nimdesk.cluster.RemoteService;
import com.nimdesk.event.AsyncCallback;
import com.nimdesk.event.CloneImageForUpdateEvent;
import com.nimdesk.event.ClusterViewChangeEvent;
import com.nimdesk.event.DeleteLocalDesktopEvent;
import com.nimdesk.event.DeletePoolEvent;
import com.nimdesk.event.DeleteVmImageEvent;
import com.nimdesk.event.EventHandler;
import com.nimdesk.event.ImportImageFromVmEvent;
import com.nimdesk.event.ImportUpdatedImageEvent;
import com.nimdesk.event.InstantiatePoolEvent;
import com.nimdesk.event.ProvisionDesktopsEvent;
import com.nimdesk.event.ProvisionLocalDesktopEvent;
import com.nimdesk.event.RebalanceDesktopsEvent;
import com.nimdesk.event.RecomposeDesktopsEvent;
import com.nimdesk.event.RequestDesktopEvent;
import com.nimdesk.hypervisor.HypervisorContext;
import com.nimdesk.model.DiskImage;
import com.nimdesk.model.Host;
import com.nimdesk.model.Pool;
import com.nimdesk.model.Server;
import com.nimdesk.model.VmImage;
import com.nimdesk.model.VmInstance;
import com.nimdesk.service.AsyncExecutor;
import com.nimdesk.service.EventBusService;
import com.nimdesk.service.ObjectPropertyChangeEvent;
import com.nimdesk.service.ObjectPropertyChangeListener;
import com.nimdesk.service.impl.ServiceLocator;
import com.nimdesk.service.impl.workflow.CloneImageForUpdateFlow;
import com.nimdesk.service.impl.workflow.DeleteDesktopFlow;
import com.nimdesk.service.impl.workflow.DeleteImageFlow;
import com.nimdesk.service.impl.workflow.DeletePoolFlow;
import com.nimdesk.service.impl.workflow.ImportImageFromVmFlow;
import com.nimdesk.service.impl.workflow.ImportUpdatedImageFlow;
import com.nimdesk.service.impl.workflow.InstantiatePoolFlow;
import com.nimdesk.service.impl.workflow.ProvisionDesktopFlow;
import com.nimdesk.util.Pair;
import com.google.common.eventbus.AllowConcurrentEvents;
import com.google.common.eventbus.Subscribe;

public class VdiController implements EventHandler, InitializingBean, PoolDesktopHandler {
    private static final Logger LOGGER = Logger.getLogger(VdiController.class);

    @Autowired
    private EventBusService _eventBusService;

    @Autowired
    private AsyncExecutor _asyncExecutor;

    @Autowired
    private RemoteProxyFactory _remoteProxyFactory;

    @Autowired
    private Server.Service _serverService;

    @Autowired
    private Host.Service _hostService;

    @Autowired
    private VmImage.Service _vmImageService;

    @Autowired
    private DiskImage.Service _diskImageService;

    @Autowired
    private ServiceLocator _serviceLocator;

    private ImageManager _imageManager;
    private PoolManager _poolManager;

    // Must keep those listeners referenced here as they are added into a WeakHashMap.
    private final ObjectPropertyChangeListener _serverChangeListener = new ServerChangeListenerImpl();
    private final ObjectPropertyChangeListener _hostChangeListener = new HostChangeListenerImpl();

    public VdiController() {
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(_eventBusService, "An EventBusService must be set");
        Assert.notNull(_asyncExecutor, "An AsyncExecutor must be set");
        Assert.notNull(_serverService, "A Server.Service must be set");
        Assert.notNull(_hostService, "A Host.Service must be set");
        Assert.notNull(_vmImageService, "A VmImage.Service must be set");
        Assert.notNull(_diskImageService, "A DiskImage.Service must be set");
        Assert.notNull(_serviceLocator, "A ServiceLocator must be set");

        _imageManager = new ImageManager(_serviceLocator);
        _poolManager = new PoolManager(_serviceLocator);

        _serverService.addObjectPropertyChangeListener(_serverChangeListener);
        _hostService.addObjectPropertyChangeListener(_hostChangeListener);

        LOGGER.info("Register VdiController with event bus");
        _eventBusService.register(this);
    }

    ImageManager getImageManager() {
        return _imageManager;
    }

    PoolManager getPoolManager() {
        return _poolManager;
    }

    public boolean allowDiskImageCopy(String callerServerUuid, String diskImageUuid) {
        return _imageManager.allowDiskImageCopy(callerServerUuid, diskImageUuid);
    }

    @Override
    public int allocateDesktops(String poolUuid, boolean rebalance) {
        return _poolManager.allocateDesktops(poolUuid, rebalance);
    }

    public boolean recomposeDesktops(String poolUuid, boolean forced) {
        return _poolManager.recomposeDesktops(poolUuid, forced);
    }

    public boolean rebalanceDesktops(String poolUuid) {
        return _poolManager.rebalanceDesktops(poolUuid);
    }

    public VmInstance getAssignedDesktop(String poolUuid, String userUuid) {
        return _poolManager.getAssignedDesktop(poolUuid, userUuid);
    }

    public VmInstance assignDesktop(String poolUuid, String userUuid) {
        return _poolManager.assignDesktop(poolUuid, userUuid);
    }

    @Override
    public VmInstance assignLocalDesktopToUser(String poolUuid, String userUuid) {
        return _poolManager.assignLocalDesktopToUser(poolUuid, userUuid);
    }

    @Subscribe
    @AllowConcurrentEvents
    public void handle(final ImportImageFromVmEvent event) {
        VmImage vmImage = event.getVmImage();
        String vmName = event.getVmName();
        String vmRefId = event.getVmRefId();
        AsyncCallback callback = event.getCallback();

        try {
            LOGGER.info(String.format("Importing image \"%s\" from vm \"%s\"...", vmImage.getName(), vmName));

            ImportImageFromVmFlow wf = new ImportImageFromVmFlow(vmImage, vmName, vmRefId, callback);
            wf.run();
        } catch (Exception e) {
            LOGGER.error(String.format("Failed to import image \"%s\" from vm \"%s\"", vmImage.getName(), vmName), e);

            if (callback != null) {
                callback.onFailure(e);
            }
        }
    }

    @Subscribe
    @AllowConcurrentEvents
    public void handle(final CloneImageForUpdateEvent event) {
        Host host = event.getHost();
        VmImage vmImage = event.getVmImage();
        AsyncCallback callback = event.getCallback();

        try {
            LOGGER.info(String.format("Cloning image \"%s\" for update...", vmImage.getName()));

            if (host == null) {
                host = _hostService.getLocalHost();
            }

            CloneImageForUpdateFlow wf = new CloneImageForUpdateFlow(host, vmImage, callback);
            wf.run();
        } catch (Exception e) {
            LOGGER.error(String.format("Failed to clone image \"%s\" for update", vmImage.getName()), e);

            if (callback != null) {
                callback.onFailure(e);
            }
        }
    }

    @Subscribe
    @AllowConcurrentEvents
    public void handle(final ImportUpdatedImageEvent event) {
        VmImage vmImage = event.getVmImage();
        AsyncCallback callback = event.getCallback();

        try {
            LOGGER.info(String.format("Importing updated image \"%s\"...", vmImage.getName()));

            ImportUpdatedImageFlow wf = new ImportUpdatedImageFlow(vmImage, callback);
            wf.run();
        } catch (Exception e) {
            LOGGER.error(String.format("Failed to import updated image \"%s\"", vmImage.getName()), e);

            if (callback != null) {
                callback.onFailure(e);
            }
        }
    }

    @Subscribe
    @AllowConcurrentEvents
    public void handle(final DeleteVmImageEvent event) {
        VmImage vmImage = event.getVmImage();
        AsyncCallback callback = event.getCallback();

        try {
            LOGGER.info(String.format("Deleting image \"%s\"...", vmImage.getName()));

            DeleteImageFlow wf = new DeleteImageFlow(vmImage, callback);
            wf.run();
        } catch (Exception e) {
            LOGGER.error(String.format("Failed to delete image \"%s\"", vmImage.getName()), e);

            if (callback != null) {
                callback.onFailure(e);
            }
        }
    }

    @Subscribe
    @AllowConcurrentEvents
    public void handle(final InstantiatePoolEvent event) {
        Pool pool = event.getPool();
        AsyncCallback callback = event.getCallback();

        try {
            LOGGER.info(String.format("Instantiating pool \"%s\"...", pool.getName()));

            InstantiatePoolFlow wf = new InstantiatePoolFlow(pool, callback);
            wf.run();
        } catch (Exception e) {
            LOGGER.error(String.format("Failed to instantiate pool \"%s\"", pool.getName()), e);

            if (callback != null) {
                callback.onFailure(e);
            }
        }
    }

    @Subscribe
    @AllowConcurrentEvents
    public void handle(final DeletePoolEvent event) {
        Pool pool = event.getPool();
        AsyncCallback callback = event.getCallback();

        try {
            LOGGER.info(String.format("Deleting pool \"%s\"...", pool.getName()));

            DeletePoolFlow wf = new DeletePoolFlow(pool, callback);
            wf.run();
        } catch (Exception e) {
            LOGGER.error(String.format("Failed to delete pool \"%s\"", pool.getName()), e);

            if (callback != null) {
                callback.onFailure(e);
            }
        }
    }

    @Subscribe
    @AllowConcurrentEvents
    public void handle(final ProvisionLocalDesktopEvent event) {
        Host localHost = event.getHost();
        Pool pool = event.getPool();
        VmInstance vmInstance = event.getVmInstance();
        AsyncCallback callback = event.getCallback();

        try {
            LOGGER.info(String.format("Provisioning local desktop for pool \"%s\"...", pool.getName()));

            if (localHost == null) {
                LOGGER.warn("Unable to find local host");
                throw new Exception("Unable to find local host");
            }
            if (Host.Status.Connected != localHost.getStatus()) {
                LOGGER.warn("Local host is not connected. Ignore provisionLocalDesktop");
                throw new Exception("Local host is not connected. Ignore provisionLocalDesktop");
            }

            String poolBaseDiskImageGuid = pool.getBaseDiskImageGuid();
            DiskImage baseDiskImage;
            if (StringUtils.isEmpty(poolBaseDiskImageGuid)
                    || (baseDiskImage = _diskImageService.getLocalDiskImageByGuid(poolBaseDiskImageGuid)) == null) {
                LOGGER.warn(String.format("Pool \"%s\" doesn't have a valid base disk image (guid=%s)",
                        pool.getName(), poolBaseDiskImageGuid));
                throw new Exception("The pool is not available on this server");
            }
            if (DiskImage.Status.Ready != baseDiskImage.getStatus()) {
                LOGGER.warn(String.format("Pool \"%s\" base disk image (guid=%s) is not ready",
                        pool.getName(), poolBaseDiskImageGuid));
                throw new Exception("The pool is not valid on this server");
            }

            ProvisionDesktopFlow wf = new ProvisionDesktopFlow(localHost, pool, baseDiskImage, vmInstance, callback);
            wf.run();
        } catch (Exception e) {
            LOGGER.warn(String.format("Failed to provision local desktop for pool \"%s\"", pool.getName()));

            if (callback != null) {
                callback.onFailure(e);
            }
        }
    }

    @Subscribe
    @AllowConcurrentEvents
    public void handle(final DeleteLocalDesktopEvent event) {
        HypervisorContext context = event.getContext();
        HypervisorContext vcContext =  event.getVcContext();
        Host host = event.getHost();
        Pool pool = event.getPool();
        VmInstance vmInstance = event.getVmInstance();
        AsyncCallback callback = event.getCallback();

        String poolName = (pool != null) ? pool.getName() : "(unknown)";

        try {
            LOGGER.info(String.format("Deleting local desktop \"%s\" for pool \"%s\"...", vmInstance.getName(), poolName));

            DeleteDesktopFlow wf = new DeleteDesktopFlow(context, vcContext, host, pool, vmInstance, callback);
            wf.run();
        } catch (Exception e) {
            LOGGER.error(String.format("Failed to delete local desktop \"%s\" for pool \"%s\"", vmInstance.getName(), poolName), e);

            if (callback != null) {
                callback.onFailure(e);
            }
        }
    }

    @Subscribe
    @AllowConcurrentEvents
    public void handle(ClusterViewChangeEvent event) {
        boolean isMaster = event.isMaster();
        String masterAddress = event.getMasterAddress();

        _imageManager.setClusterMaster(isMaster, masterAddress);
        _poolManager.setClusterMaster(isMaster, masterAddress);
    }

    @Subscribe
    @AllowConcurrentEvents
    public void handle(final RequestDesktopEvent event) {
        _asyncExecutor.run(new AsyncExecutor.Job() {
            @Override
            public Object run() {
                LOGGER.info(String.format("User \"%s\" requests a desktop from pool \"%s\"", event.getUserUuid(), event.getPool().getName()));
                _poolManager.handleRequestDesktop(event);

                return null;
            }
        }, null);
    }

    @Subscribe
    @AllowConcurrentEvents
    public void handle(final ProvisionDesktopsEvent event) {
        _asyncExecutor.run(new AsyncExecutor.Job() {
            @Override
            public Object run() {
                LOGGER.info(String.format("Allocating desktops for pool \"%s\", rebalance=%s",
                        event.getPool().getName(), event.isRebalance() ? "true" : "false"));

                _poolManager.handleProvisionDesktops(event);

                return null;
            }
        }, null);
    }

    @Subscribe
    @AllowConcurrentEvents
    public void handle(final RecomposeDesktopsEvent event) {
        _asyncExecutor.run(new AsyncExecutor.Job() {
            @Override
            public Object run() {
                LOGGER.info(String.format("Recomposing desktops for pool \"%s\", forced=%s",
                        event.getPool().getName(), event.isForced() ? "true" : "false"));

                _poolManager.handleRecomposeDesktops(event);

                return null;
            }
        }, null);
    }

    @Subscribe
    @AllowConcurrentEvents
    public void handle(final RebalanceDesktopsEvent event) {
        _asyncExecutor.run(new AsyncExecutor.Job() {
            @Override
            public Object run() {
                LOGGER.info(String.format("Rebalancing desktops for pool \"%s\"",
                        event.getPool().getName()));

                _poolManager.handleRebalanceDesktops(event);

                return null;
            }
        }, null);
    }

    private void start() {
        LOGGER.info("Host connected - start VdiController");

        //TaskFactory.init();
        _imageManager.start();
        _poolManager.start();
    }

    private void stop() {
        LOGGER.info("Host disconnected - stop VdiController");

        _imageManager.stop();
        _poolManager.stop();
    }

    private class ServerChangeListenerImpl implements ObjectPropertyChangeListener {
        @Override
        public void newObject(ObjectPropertyChangeEvent event) {
        }

        @Override
        public void deleteObject(ObjectPropertyChangeEvent event) {
        }

        @Override
        public void propertyChange(ObjectPropertyChangeEvent event) {
            final Server server = (Server) event.getSource();
            if (server.isLocal() || Server.Status.Running != server.getStatus()) {
                return;
            }

            _asyncExecutor.run(new AsyncExecutor.Job() {
                @Override
                public Object run() {
                    synchronized(ServerChangeListenerImpl.this) {
                        RemoteService remoteService = _remoteProxyFactory.getRemoteProxy(server.getPublicAddress());
                        if (remoteService == null) {
                            LOGGER.warn(String.format("Failed to get remote service from server \"%s\"", server.getPublicAddress()));
                            return null;
                        }

                        return null;
                    }
                }
            }, null);
        }
    }

    private class HostChangeListenerImpl implements ObjectPropertyChangeListener {
        @Override
        public void newObject(ObjectPropertyChangeEvent event) {
        }

        @Override
        public void deleteObject(ObjectPropertyChangeEvent event) {
        }

        @Override
        public void propertyChange(ObjectPropertyChangeEvent event) {
            final Host host = (Host) event.getSource();
            final Map<String, Pair<Object, Object>> changedProperties = event.getChangedProperties();
            if (changedProperties == null) {
                return;
            }

            if (!host.isLocal()) {
                return;
            }

            _asyncExecutor.run(new AsyncExecutor.Job() {
                @Override
                public Object run() {
                    Pair<Object, Object> statuses = changedProperties.get(Host.PROP_STATUS);
                    if (statuses != null) {
                        switch ((Host.Status) statuses.getSecond()) {
                        case Connected:
                            start();
                            break;

                        case Disconnected:
                            stop();
                            break;
                        }
                    }

                    return null;
                }
            }, null);
        }
    }
}
