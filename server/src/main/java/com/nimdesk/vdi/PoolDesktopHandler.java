/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.vdi;

import com.nimdesk.model.VmInstance;

public interface PoolDesktopHandler {
    /**
     * Allocate desktops for the pool on the whole cluster.
     * The returned VmInstances may still be in provisioning stages.
     */
    int allocateDesktops(String poolUuid, boolean rebalance);

    /**
     * Assign a desktop from pool to the user specified by userUuid.
     * The returned VmInstance may still be in provisioning stage.
     */
    VmInstance assignLocalDesktopToUser(String poolUuid, String userUuid);
}
