/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.gateway.impl;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.webbitserver.BaseWebSocketHandler;
import org.webbitserver.WebServer;
import org.webbitserver.WebServers;
import org.webbitserver.WebSocketConnection;
import org.webbitserver.handler.StaticFileHandler;
import org.webbitserver.handler.logging.LoggingHandler;
import org.webbitserver.handler.logging.SimpleLogSink;

import com.nimdesk.gateway.WebSocketProxy;

public class WebSocketProxyImpl implements WebSocketProxy, InitializingBean {
    private static final Logger LOGGER = Logger.getLogger(WebSocketProxyImpl.class);

    private static final String KEY_TCPCONNECTION = "tcpConnection";

    private WebServer _webServer = null;
    private final Set<WebSocketConnection> _connections =
            Collections.synchronizedSet(new HashSet<WebSocketConnection>());

    @Override
    public void afterPropertiesSet() throws Exception {
        start();
    }

    public synchronized void start() {
        if (_webServer != null) {
            return;
        }

        try {
            _webServer = WebServers.createWebServer(18888)
                    .add(new LoggingHandler(new SimpleLogSink()))
                    .add("/websockify", new Proxy())
                    .add(new StaticFileHandler("/var/nimdesk/rdp"))
                    .start().get();
        } catch (Exception e) {
            LOGGER.error("Failed to start websocket server", e);
        }
    }

    private class Proxy extends BaseWebSocketHandler {
        @Override
        public void onOpen(WebSocketConnection connection) throws Exception {
            String uri = connection.httpRequest().uri();
            LOGGER.info(String.format("WebSocket proxy request uri: %s", uri));

            int pos = uri.indexOf("?");
            if (pos < 0) {
                connection.close();
                return;
            }

            String paramstr = uri.substring(pos + 1);

            String[] params = StringUtils.split(paramstr, '&');
            if (params == null) {
                connection.close();
                return;
            }

            String token = null;
            for (String param : params) {
                String[] namevalue = StringUtils.split(param, '=');
                if (namevalue != null && namevalue.length == 2) {
                    if (StringUtils.equals("token", namevalue[0])) {
                        token = namevalue[1];
                    }
                }
            }

            if (StringUtils.isEmpty(token)) {
                connection.close();
                return;
            }

            int port;
            try {
                port = Integer.parseInt(token);
            } catch (Exception e) {
                connection.close();
                return;
            }

            if (!connectTcp(connection, "50.23.173.172", port)) {
                LOGGER.error("Unable to connect TCP");
                connection.close();
                return;
            }

            _connections.add(connection);
        }

        @Override
        public void onClose(WebSocketConnection connection) throws Exception {
            if (_connections.remove(connection)) {
                TcpConnection tcpConnection = (TcpConnection) connection.data(KEY_TCPCONNECTION);
                if (tcpConnection != null) {
                    tcpConnection.close();
                }
            }
        }

        @Override
        public void onMessage(WebSocketConnection connection, String msg) throws Throwable {
            TcpConnection tcpConnection = (TcpConnection) connection.data(KEY_TCPCONNECTION);
            if (tcpConnection != null) {
                byte[] data = Base64.decode(msg);
                tcpConnection.send(data);
            }
        }

        @Override
        public void onMessage(WebSocketConnection connection, byte[] msg) throws Throwable {
        }

        private boolean connectTcp(final WebSocketConnection connection, String address, int port) {
            LOGGER.info(String.format("Received tcp connect request from WebSocket: %s:%d", address, port));

            TcpConnection tcpConnection;
            try {
                tcpConnection = new TcpConnection(address, port,  new TcpConnectionListener() {
                    @Override
                    public void onReceivedData(byte[] data, int length) {
                        String message = Base64.encode(data, length);
                        connection.send(message);
                    }

                    @Override
                    public void onClosed() {
                        connection.close();
                    }
                });
            } catch (Exception e) {
                LOGGER.error(String.format("Failed to connect to %s:%d", address, port), e);
                return false;
            }

            connection.data(KEY_TCPCONNECTION, tcpConnection);

            return true;
        }
    }

    private class TcpConnection {
        private final TcpConnectionListener _listener;

        private Socket _socket = null;
        private InputStream _ins;
        private OutputStream _outs;
        private final byte[] _buffer = new byte[1024 * 4];

        private final String _remoteAddress;
        private final int _port;

        TcpConnection(String address, int port, TcpConnectionListener listener) throws IOException {
            _listener = listener;

            try {
                _socket = new Socket();
                _socket.connect(new InetSocketAddress(address, port));

                _ins = _socket.getInputStream();
                _outs = _socket.getOutputStream();

                _remoteAddress = address;
                _port = port;

                new Thread() {
                    @Override
                    public void run() {
                        try {
                            while (true) {
                                int length = _ins.read(_buffer);
                                if (_listener != null && length > 0) {
                                    _listener.onReceivedData(_buffer, length);
                                }
                            }
                        } catch (Exception e) {
                            LOGGER.error(String.format("Failed received data from %s:%d",
                                    _remoteAddress, _port), e);
                        }

                        try {
                            _socket.close();
                        } catch (Exception e1) {
                            // Ignore exception
                        }

                        if (_listener != null) {
                            _listener.onClosed();
                        }

                        LOGGER.info(String.format("Stopped receiving data from %s:%d", _remoteAddress, _port));
                    }
                }.start();
            } catch (IOException e) {
                LOGGER.error(String.format("Failed to connect to %s:%d", address, port), e);
                if (_socket != null) {
                    try {
                        _socket.close();
                    } catch (Exception e1) {
                        // Ignore exception
                    }
                    _socket = null;
                }

                throw e;
            }
        }

        public void send(byte[] data) {
            try {
                _outs.write(data);
            } catch (Exception e) {
                LOGGER.error(String.format("Failed sending data to %s:%d", _remoteAddress, _port), e);

                try {
                    _socket.close();
                } catch (Exception e1) {
                    // Ignore exception
                }

                if (_listener != null) {
                    _listener.onClosed();
                }

                LOGGER.info(String.format("Stopped sending data to %s:%d", _remoteAddress, _port));
            }
        }

        public void close() {
            if (_socket != null) {
                try {
                    _socket.close();
                } catch (Exception e1) {
                    // Ignore exception
                }
                _socket = null;
            }
        }
    }

    private interface TcpConnectionListener {
        void onReceivedData(byte[] data, int length);
        void onClosed();
    }
}
