/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.gateway.impl;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import com.nimdesk.gateway.Gateway;

public class GatewayImpl implements Gateway {
    private static final Logger LOGGER = Logger.getLogger(GatewayImpl.class);

    private static final int PORT_RANGE_START = 20000;
    private static final int PORT_RANGE_END = 40000;

    private static final int CORE_GATEWAY_POOL_SIZE = 1;
    private static final int MAX_GATEWAY_POOL_SIZE = 200;

    private final ExecutorService _executor = new ThreadPoolExecutor(CORE_GATEWAY_POOL_SIZE,
            MAX_GATEWAY_POOL_SIZE, 300L, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>());

    @Override
    public int startSession(final String address, final int port) {
        LOGGER.info(String.format("Starting session for %s:%d...", address, port));

        final ServerSocket serverSock = bindServerPort();
        if (serverSock == null) {
            LOGGER.error(String.format("Failed to bind local port for for %s:%d", address, port));
            return 0;
        }

        final int localPort = serverSock.getLocalPort();

        LOGGER.info(String.format("Bind local port %d for session address %s:%d", localPort, address, port));

        _executor.execute(new Runnable() {
            @Override
            @SuppressWarnings("unused")
            public void run() {
                Socket clientSock = null;
                Socket targetSock = null;

                try {
                    serverSock.setSoTimeout(120 * 1000); // open for 2 minute
                    clientSock = serverSock.accept();
                    LOGGER.info(String.format("Accepted connection to local port %d", localPort));

                    targetSock = _connectTarget(address, port);
                    if (targetSock == null) {
                        return;
                    }

                    LOGGER.info(String.format("Connected to %s:%d", address, port));

                    final Socket[] sockets = new Socket[2];
                    sockets[0] = clientSock;
                    sockets[1] = targetSock;

                    // Thread for clientSock -> targetSock
                    new Thread() {
                        @Override
                        public void run() {
                            try {
                                InputStream ins1 = sockets[0].getInputStream();
                                OutputStream outs2 = sockets[1].getOutputStream();

                                while (true) {
                                    int c = ins1.read();
                                    outs2.write(c);
                                }
                            } catch (IOException e) {
                                LOGGER.error(String.format("Failed %s -> %s",
                                        sockets[0].getRemoteSocketAddress(), sockets[1].getRemoteSocketAddress()),
                                        e);
                            }

                            try {
                                sockets[0].close();
                            } catch (IOException e) {
                                // Ignore error
                            }
                            try {
                                sockets[1].close();
                            } catch (IOException e) {
                                // Ignore error
                            }

                            LOGGER.info(String.format("Stopped proxying %s -> %s",
                                    sockets[0].getRemoteSocketAddress(), sockets[1].getRemoteSocketAddress()));
                        }
                    }.start();

                    // Thread for targetSock -> clientSock
                    new Thread() {
                        @Override
                        public void run() {
                            try {
                                InputStream ins2 = sockets[1].getInputStream();
                                OutputStream outs1 = sockets[0].getOutputStream();

                                while (true) {
                                    int c = ins2.read();
                                    outs1.write(c);
                                }
                            } catch (IOException e) {
                                LOGGER.error(String.format("Failed %s -> %s",
                                        sockets[1].getRemoteSocketAddress(), sockets[0].getRemoteSocketAddress()),
                                        e);
                            }

                            try {
                                sockets[0].close();
                            } catch (IOException e) {
                                // Ignore error
                            }
                            try {
                                sockets[1].close();
                            } catch (IOException e) {
                                // Ignore error
                            }

                            LOGGER.info(String.format("Stopped proxying %s -> %s",
                                    sockets[1].getRemoteSocketAddress(), sockets[0].getRemoteSocketAddress()));
                        }
                    }.start();

                    LOGGER.info(String.format("Started proxying %s and %s",
                            sockets[0].getRemoteSocketAddress(), sockets[1].getRemoteSocketAddress()));

                } catch (IOException e) {
                    LOGGER.error(String.format("Failed to bind local port for for %s:%d", address, port), e);

                    if (clientSock != null) {
                        try {
                            clientSock.close();
                        } catch (IOException e1) {
                            // Ignore error
                        }
                    }

                    if (targetSock != null) {
                        try {
                            targetSock.close();
                        } catch (IOException e1) {
                            // Ignore error
                        }
                    }
                } finally {
                    if (serverSock != null) {
                        try {
                            serverSock.close();
                        } catch (IOException e1) {
                            // Ignore error
                        }
                    }
                }
            }
        });

        return localPort;
    }

    private synchronized ServerSocket bindServerPort() {
        ServerSocket serverSocket = null;

        // try 200 times
        Random rand = new Random();
        for (int index = 0; index < 200; index++) {
            try {
                int port = PORT_RANGE_START + rand.nextInt(PORT_RANGE_END - PORT_RANGE_START);
                serverSocket = new ServerSocket(port);
                break;
            } catch (IOException e) {
                continue;
            }
        }

        return serverSocket;
    }

    private Socket _connectTarget(String address, int port) {
        Socket sock = new Socket();

        try {
            sock.connect(new InetSocketAddress(address, port));
        } catch (IOException e) {
            LOGGER.error(String.format("Failed to connect to target %s:%d", address, port), e);
            try {
                sock.close();
            } catch (Exception e1) {
                // Ignore exception
            }
            return null;
        }

        return sock;
    }
}
