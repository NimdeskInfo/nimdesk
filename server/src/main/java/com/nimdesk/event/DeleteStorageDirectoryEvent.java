/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.event;

import com.nimdesk.hypervisor.HypervisorContext;
import com.nimdesk.model.Storage;

public class DeleteStorageDirectoryEvent implements EventBase {
    private final HypervisorContext _context;
    private final String _fileOrDirPath;
    private final Storage _storage;

    private final AsyncCallback _callback;

    public DeleteStorageDirectoryEvent(HypervisorContext context, String fileOrDirPath, Storage storage, AsyncCallback callback) {
        _context = context;
        _fileOrDirPath = fileOrDirPath;
        _storage = storage;

        _callback = callback;
    }

    public HypervisorContext getContext() {
        return _context;
    }

    public String getFileOrDirPath() {
        return _fileOrDirPath;
    }

    public Storage getStorage() {
        return _storage;
    }

    public AsyncCallback getCallback() {
        return _callback;
    }
}
