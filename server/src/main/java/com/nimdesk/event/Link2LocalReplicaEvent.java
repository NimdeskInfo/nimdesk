/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.event;

import java.util.Map;

import com.nimdesk.hypervisor.HypervisorContext;
import com.nimdesk.model.DiskImage;
import com.nimdesk.model.Storage;

public class Link2LocalReplicaEvent implements EventBase {
    private final HypervisorContext _context;
    private final DiskImage _baseImage;
    private final Storage _baseStorage;
    private final String _cloneImageParentDir;
    private final String _cloneImageName;
    private final Storage _cloneStorage;
    private final String _network;
    private final Map<String, String> _customProps;

    private final AsyncCallback _callback;

    public Link2LocalReplicaEvent(HypervisorContext context, DiskImage baseImage, Storage baseStorage,
            String cloneImageParentDir, String cloneImageName, Storage cloneStorage, String network,
            Map<String, String> customProps, AsyncCallback callback) {
        _context = context;
        _baseImage = baseImage;
        _baseStorage = baseStorage;
        _cloneImageParentDir = cloneImageParentDir;
        _cloneImageName = cloneImageName;
        _cloneStorage = cloneStorage;
        _network = network;
        _customProps = customProps;

        _callback = callback;
    }

    public HypervisorContext getContext() {
        return _context;
    }

    public DiskImage getBaseImage() {
        return _baseImage;
    }

    public Storage getBaseStorage() {
        return _baseStorage;
    }

    public String getCloneImageParentDir() {
        return _cloneImageParentDir;
    }

    public String getCloneImageName() {
        return _cloneImageName;
    }

    public Storage getCloneStorage() {
        return _cloneStorage;
    }

    public String getNetwork() {
        return _network;
    }

    public Map<String, String> getCustomProps() {
        return _customProps;
    }

    public AsyncCallback getCallback() {
        return _callback;
    }
}
