/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.event;

public class ClusterViewChangeEvent implements EventBase {
    private final boolean _isMaster;
    private final String _masterAddress;

    public ClusterViewChangeEvent(boolean isMaster, String masterAddress) {
        _isMaster = isMaster;
        _masterAddress = masterAddress;
    }

    public boolean isMaster() {
        return _isMaster;
    }

    public String getMasterAddress() {
        return _masterAddress;
    }
}
