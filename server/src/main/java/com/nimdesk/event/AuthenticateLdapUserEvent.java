/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.event;

import com.nimdesk.ldap.LdapContext;

public final class AuthenticateLdapUserEvent implements EventBase {
    private final LdapContext _context;
    private final String _username;
    private final String _password;

    private final AsyncCallback _callback;

    public AuthenticateLdapUserEvent(LdapContext context, String username, String password, AsyncCallback callback) {
        _context = context;
        _username = username;
        _password = password;

        _callback = callback;
    }

    public LdapContext getContext() {
        return _context;
    }

    public String getUsername() {
        return _username;
    }

    public String getPassword() {
        return _password;
    }

    public AsyncCallback getCallback() {
        return _callback;
    }
}
