/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.event;

import com.nimdesk.ldap.LdapContext;

public final class FindLdapUsersEvent implements EventBase {
    private final LdapContext _context;
    private final boolean _includeUsers;
    private final boolean _includeGroups;
    private final String _searchPrefix;

    private final AsyncCallback _callback;

    public FindLdapUsersEvent(LdapContext context, boolean includeUsers, boolean includeGroups, String searchPrefix, AsyncCallback callback) {
        _context = context;
        _includeUsers = includeUsers;
        _includeGroups = includeGroups;
        _searchPrefix = searchPrefix;

        _callback = callback;
    }

    public LdapContext getContext() {
        return _context;
    }

    public boolean isIncludeUsers() {
        return _includeUsers;
    }

    public boolean isIncludeGroups() {
        return _includeGroups;
    }

    public String getSearchPrefix() {
        return _searchPrefix;
    }

    public AsyncCallback getCallback() {
        return _callback;
    }
}
