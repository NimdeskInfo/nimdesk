/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.event;

import com.nimdesk.hypervisor.HypervisorContext;
import com.nimdesk.model.Storage;
import com.nimdesk.model.User;

public class AttachVmDiskEvent implements EventBase {
    private final HypervisorContext _context;
    private final String _vmRefId;
    private final long _sizeInMB;
    private final User _user;
    private final Storage _storage;
    private final String _diskName;

    private final AsyncCallback _callback;

    public AttachVmDiskEvent(HypervisorContext context, String vmRefId, long sizeInMB,
            User user, Storage storage, String diskName, AsyncCallback callback) {
        _context = context;
        _vmRefId = vmRefId;
        _sizeInMB = sizeInMB;
        _user = user;
        _storage = storage;
        _diskName = diskName;

        _callback = callback;
    }

    public HypervisorContext getContext() {
        return _context;
    }

    public String getVmRefId() {
        return _vmRefId;
    }

    public long getSizeInMB() {
        return _sizeInMB;
    }

    public User getUser() {
        return _user;
    }

    public Storage getStorage() {
        return _storage;
    }

    public String getDiskName() {
        return _diskName;
    }

    public AsyncCallback getCallback() {
        return _callback;
    }
}
