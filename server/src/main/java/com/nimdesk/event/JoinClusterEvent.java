/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.event;

public class JoinClusterEvent implements EventBase {
    private final String _clusterName;
    private final String _clusterId;
    private final String _bindAddress;
    private final String[] _initialHosts;

    private final AsyncCallback _callback;

    public JoinClusterEvent(String clusterName, String clusterId, String bindAddress, String[] initialHosts, AsyncCallback callback) {
        _clusterName = clusterName;
        _clusterId = clusterId;
        _bindAddress = bindAddress;
        _initialHosts = initialHosts;

        _callback = callback;
    }

    public String getClusterName() {
        return _clusterName;
    }

    public String getClusterId() {
        return _clusterId;
    }

    public String getBindAddress() {
        return _bindAddress;
    }

    public String[] getInitialHosts() {
        return _initialHosts;
    }

    public AsyncCallback getCallback() {
        return _callback;
    }
}
