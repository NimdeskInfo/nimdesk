/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.event;

import com.nimdesk.hypervisor.HypervisorContext;

public class TemplatizeVmEvent implements EventBase {
    private final HypervisorContext _context;
    private final String _vmRefId;
    private final String _snapshotName;
    private final boolean _enableCbrc;
    private final HypervisorContext _vcContext;
    private final String _hostEsxUuid;

    private final AsyncCallback _callback;

    public TemplatizeVmEvent(HypervisorContext context, String vmRefId, String snapshotName, boolean enableCbrc,
            HypervisorContext vcContext, String hostEsxUuid, AsyncCallback callback) {
        _context = context;
        _vmRefId = vmRefId;
        _snapshotName = snapshotName;
        _enableCbrc = enableCbrc;
        _vcContext = vcContext;
        _hostEsxUuid = hostEsxUuid;

        _callback = callback;
    }

    public HypervisorContext getContext() {
        return _context;
    }

    public String getVmRefId() {
        return _vmRefId;
    }

    public String getSnapshotName() {
        return _snapshotName;
    }

    public boolean isEnableCbrc() {
        return _enableCbrc;
    }

    public HypervisorContext getVcContext() {
        return _vcContext;
    }

    public String getHostEsxUuid() {
        return _hostEsxUuid;
    }

    public AsyncCallback getCallback() {
        return _callback;
    }
}
