/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.event;

import com.nimdesk.vm.VmStateListener;

public class AddRemoveVmStateListenerEvent implements EventBase {
    private final boolean _add;
    private final VmStateListener _listener;

    public AddRemoveVmStateListenerEvent(boolean add, VmStateListener listener) {
        _add = add;
        _listener = listener;
    }

    public boolean isAdd() {
        return _add;
    }

    public VmStateListener getListener() {
        return _listener;
    }
}
