/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.event;

import java.util.List;

import com.nimdesk.hypervisor.HypervisorContext;
import com.nimdesk.model.Storage;

public class AllocateStorageEvent implements EventBase {
    private final HypervisorContext _context;
    private final List<? extends Storage> _storages;
    private final String _purpose;

    private final AsyncCallback _callback;

    public AllocateStorageEvent(HypervisorContext context, List<? extends Storage> storages, String purpose, AsyncCallback callback) {
        _context = context;
        _storages = storages;
        _purpose = purpose;

        _callback = callback;
    }

    public HypervisorContext getContext() {
        return _context;
    }

    public List<? extends Storage> getStorages() {
        return _storages;
    }

    public String getPurpose() {
        return _purpose;
    }

    public AsyncCallback getCallback() {
        return _callback;
    }
}
