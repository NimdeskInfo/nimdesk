/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.event;

import com.nimdesk.hypervisor.HypervisorContext;

public class InstantiateVmEvent implements EventBase {
    private final HypervisorContext _context;
    private final String _hostRefId;
    private final String _vmName;
    private final String _vmxPath;
    private final boolean _poweronVm;
    private final long _cpuCount;
    private final long _memoryMB;
    private final boolean _reserveMemory;
    private final String _macAddress;
    private final boolean _enableCbrc;
    private final HypervisorContext _vcContext;
    private final String _hostEsxUuid;

    private final AsyncCallback _callback;

    public InstantiateVmEvent(HypervisorContext context, String hostRefId, String vmName, String vmxPath, boolean poweronVm,
            long cpuCount, long memoryMB, boolean reserveMemory, String macAddress, boolean enableCbrc, HypervisorContext vcContext, String hostEsxUuid,
            AsyncCallback callback) {
        _context = context;
        _hostRefId = hostRefId;
        _vmName = vmName;
        _vmxPath = vmxPath;
        _poweronVm = poweronVm;
        _cpuCount = cpuCount;
        _memoryMB = memoryMB;
        _reserveMemory = reserveMemory;
        _macAddress = macAddress;
        _enableCbrc = enableCbrc;
        _vcContext = vcContext;
        _hostEsxUuid = hostEsxUuid;

        _callback = callback;
    }

    public HypervisorContext getContext() {
        return _context;
    }

    public String getHostRefId() {
        return _hostRefId;
    }

    public String getVmName() {
        return _vmName;
    }

    public String getVmxPath() {
        return _vmxPath;
    }

    public boolean isPoweronVm() {
        return _poweronVm;
    }

    public long getCpuCount() {
        return _cpuCount;
    }

    public long getMemoryMB() {
        return _memoryMB;
    }

    public boolean isReserveMemory() {
        return _reserveMemory;
    }

    public String getMacAddress() {
        return _macAddress;
    }

    public boolean isEnableCbrc() {
        return _enableCbrc;
    }

    public HypervisorContext getVcContext() {
        return _vcContext;
    }

    public String getHostEsxUuid() {
        return _hostEsxUuid;
    }

    public AsyncCallback getCallback() {
        return _callback;
    }
}
