/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.event;

import com.nimdesk.hypervisor.HypervisorContext;
import com.nimdesk.model.Host;
import com.nimdesk.model.Pool;
import com.nimdesk.model.VmInstance;

public class DeleteLocalDesktopEvent implements EventBase {
    private final HypervisorContext _context;
    private final HypervisorContext _vcContext;
    private final Host _host;
    private final Pool _pool;
    private final VmInstance _vmInstance;

    private final AsyncCallback _callback;

    public DeleteLocalDesktopEvent(HypervisorContext context, HypervisorContext vcContext,
            Host host, Pool pool, VmInstance vmInstance, AsyncCallback callback) {
        _context = context;
        _vcContext = vcContext;
        _host = host;
        _pool = pool;
        _vmInstance = vmInstance;

        _callback = callback;
    }

    public HypervisorContext getContext() {
        return _context;
    }

    public HypervisorContext getVcContext() {
        return _vcContext;
    }

    public Host getHost() {
        return _host;
    }

    public Pool getPool() {
        return _pool;
    }

    public VmInstance getVmInstance() {
        return _vmInstance;
    }

    public AsyncCallback getCallback() {
        return _callback;
    }
}
