/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.event;

import java.util.Map;

import com.nimdesk.hypervisor.HypervisorContext;
import com.nimdesk.model.DiskImage;
import com.nimdesk.model.Host;
import com.nimdesk.model.Storage;

public final class CopyDiskImageEvent implements EventBase {
    private final HypervisorContext _srcContext;
    private final Host _srcHost;
    private final Storage _srcStorage;
    private final DiskImage _srcDiskImage;
    private final HypervisorContext _destContext;
    private final Host _destHost;
    private final Storage _destStorage;
    private final String _network;
    private final Map<String, String> _customProps;

    private final HypervisorContext _srcVcContext;
    private final String _srcHostEsxUuid;
    private final HypervisorContext _destVcContext;
    private final String _destHostEsxUuid;

    private final AsyncCallback _callback;

    public CopyDiskImageEvent(HypervisorContext srcContext, Host srcHost,
            Storage srcStorage, DiskImage srcDiskImage,
            HypervisorContext destContext, Host destHost, Storage destStorage,
            String network, Map<String, String> customProps,
            HypervisorContext srcVcContext, String srcHostEsxUuid, HypervisorContext destVcContext, String destHostEsxUuid,
            AsyncCallback callback) {
        _srcContext = srcContext;
        _srcHost = srcHost;
        _srcDiskImage = srcDiskImage;
        _srcStorage = srcStorage;
        _destContext = destContext;
        _destHost = destHost;
        _destStorage = destStorage;
        _network = network;
        _customProps = customProps;

        _srcVcContext = srcVcContext;
        _srcHostEsxUuid = srcHostEsxUuid;
        _destVcContext = destVcContext;
        _destHostEsxUuid = destHostEsxUuid;

        _callback = callback;
    }

    public HypervisorContext getSourceContext() {
        return _srcContext;
    }

    public Host getSourceHost() {
        return _srcHost;
    }

    public Storage getSourceStorage() {
        return _srcStorage;
    }

    public DiskImage getSourceDiskImage() {
        return _srcDiskImage;
    }

    public HypervisorContext getDestContext() {
        return _destContext;
    }

    public Host getDestHost() {
        return _destHost;
    }

    public Storage getDestStorage() {
        return _destStorage;
    }

    public String getNetwork() {
        return _network;
    }

    public Map<String, String> getCustomProps() {
        return _customProps;
    }

    public HypervisorContext getSrcVcContext() {
        return _srcVcContext;
    }

    public String getSrcHostEsxUuid() {
        return _srcHostEsxUuid;
    }

    public HypervisorContext getDestVcContext() {
        return _destVcContext;
    }

    public String getDestHostEsxUuid() {
        return _destHostEsxUuid;
    }

    public AsyncCallback getCallback() {
        return _callback;
    }
}
