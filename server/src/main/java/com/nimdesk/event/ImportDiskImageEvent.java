/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.event;

import java.util.Map;

import com.nimdesk.hypervisor.HypervisorContext;
import com.nimdesk.model.Storage;

public final class ImportDiskImageEvent implements EventBase {
    private final HypervisorContext _context;
    private final String _vmRefId;
    private final Storage _destStorage;
    private final String _destImageName;
    private final String _network;
    private final Map<String, String> _customProps;

    private final AsyncCallback _callback;

    public ImportDiskImageEvent(HypervisorContext context, String vmRefId, Storage destStorage, String destImageName,
            String network, Map<String, String> customProps, AsyncCallback callback) {
        _context = context;
        _vmRefId = vmRefId;
        _destStorage = destStorage;
        _destImageName = destImageName;
        _network = network;
        _customProps = customProps;

        _callback = callback;
    }

    public HypervisorContext getContext() {
        return _context;
    }

    public String getVmRefId() {
        return _vmRefId;
    }

    public Storage getDestStorage() {
        return _destStorage;
    }

    public String getDestImageName() {
        return _destImageName;
    }

    public String getNetwork() {
        return _network;
    }

    public Map<String, String> getCustomProps() {
        return _customProps;
    }

    public AsyncCallback getCallback() {
        return _callback;
    }
}
