/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.event;

public final class ConnectLdapEvent implements EventBase {
    private final String _providerUrl;
    private final String _domain;
    private final String _username;
    private final String _password;

    private final AsyncCallback _callback;

    public ConnectLdapEvent(String providerUrl, String domain, String username, String password, AsyncCallback callback) {
        _providerUrl = providerUrl;
        _domain = domain;
        _username = username;
        _password = password;

        _callback = callback;
    }

    public String getProviderUrl() {
        return _providerUrl;
    }

    public String getDomain() {
        return _domain;
    }

    public String getUsername() {
        return _username;
    }

    public String getPassword() {
        return _password;
    }

    public AsyncCallback getCallback() {
        return _callback;
    }
}
