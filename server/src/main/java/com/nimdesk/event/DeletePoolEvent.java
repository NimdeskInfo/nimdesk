/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.event;

import com.nimdesk.model.Pool;

public class DeletePoolEvent implements EventBase {
    private final Pool _pool;

    private final AsyncCallback _callback;

    public DeletePoolEvent(Pool pool, AsyncCallback callback) {
        _pool = pool;

        _callback = callback;
    }

    public Pool getPool() {
        return _pool;
    }

    public AsyncCallback getCallback() {
        return _callback;
    }
}
