/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.event;

import com.nimdesk.hypervisor.HypervisorContext;

public class PowerOpVmEvent implements EventBase {
    public static enum PowerOps {
        PowerOn,
        PowerOff,
        Suspend,
        Reset
    }

    private final HypervisorContext _context;
    private final String _vmRefId;
    private final PowerOps _powerOp;

    private final AsyncCallback _callback;

    public PowerOpVmEvent(HypervisorContext context, String vmRefId, PowerOps powerOp, AsyncCallback callback) {
        _context = context;
        _vmRefId = vmRefId;
        _powerOp = powerOp;

        _callback = callback;
    }

    public HypervisorContext getContext() {
        return _context;
    }

    public String getVmRefId() {
        return _vmRefId;
    }

    public PowerOps getPowerOp() {
        return _powerOp;
    }

    public AsyncCallback getCallback() {
        return _callback;
    }
}
