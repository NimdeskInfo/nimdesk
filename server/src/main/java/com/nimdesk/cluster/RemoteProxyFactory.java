/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.cluster;

import org.apache.log4j.Logger;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.beans.factory.BeanClassLoaderAware;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.remoting.rmi.RmiClientInterceptor;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.ClassUtils;

import com.nimdesk.model.Server;
import com.nimdesk.service.impl.ServerServiceImpl;

@Service("remoteProxyFactory")
public class RemoteProxyFactory implements InitializingBean, BeanClassLoaderAware {
    private static final Logger LOGGER = Logger.getLogger(ServerServiceImpl.class);

    private static RemoteProxyFactory _remoteProxyFactory = null;

    private ClassLoader _classLoader = ClassUtils.getDefaultClassLoader();

    public static RemoteProxyFactory getInstance() {
        return _remoteProxyFactory;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        _remoteProxyFactory = this;
    }

    @Override
    public void setBeanClassLoader(ClassLoader classLoader) {
        _classLoader = classLoader;
    }

    public RemoteService getRemoteProxy(Server server) {
        Assert.notNull(server, "Argument server must be set");
        Assert.isTrue(!server.isLocal(), "Server must not be local");

        return getRemoteProxy(server.getPublicAddress());
    }

    public RemoteService getRemoteProxy(String serverAddress) {
        try {
            RmiClientInterceptor rmiClientInterceptor = new RmiClientInterceptor();
            rmiClientInterceptor.setServiceUrl(String.format("rmi://%s:11099/remote-service", serverAddress));
            rmiClientInterceptor.setServiceInterface(RemoteService.class);
            rmiClientInterceptor.afterPropertiesSet();

            RemoteService remoteService = (RemoteService) new ProxyFactory(RemoteService.class,
                    rmiClientInterceptor).getProxy(_classLoader);

            return remoteService;
        } catch (Exception e) {
            LOGGER.error(String.format("Failed to get remote proxy for server \"%s\"", serverAddress), e);

            return null;
        }
    }
}
