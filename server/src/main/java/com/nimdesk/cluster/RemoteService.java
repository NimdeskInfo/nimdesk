/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.cluster;

import java.util.Properties;

import com.nimdesk.ui.server.ImportVmSpec;

public interface RemoteService {

    /**
     * Called when a node wants to join the cluster.
     */
    ClusterSpec verifyCluster(String adminName, String adminPassword, Properties properties);

    /**
     * Called when a node wants to leave the cluster.
     */
    void disjoinCluster(String serverUuid);

    /**
     * Called to import vm on the server.
     */
    void importImageFromVm(ImportVmSpec importVmSpec);

    /**
     * Called to import updated vm on the server.
     */
    void importUpdatedVmImage(String vmImageUuid);

    /**
     * Called by disk image sender to notify the receiver the disk image is ready
     * to copy.
     */
    boolean allowDiskImageCopy(String callerServerUuid, String diskImageUuid);

    /**
     * Called on cluster master to provision desktops on the whole cluster.
     */
    int provisionDesktops(String poolUuid, boolean rebalance);

    /**
     * Called on cluster master to recompose desktops on the whole cluster.
     */
    boolean recomposeDesktops(String poolUuid, boolean forced);

    /**
     * Called on cluster master to rebalance desktops on the whole cluster.
     */
    boolean rebalanceDesktops(String poolUuid);

    /**
     * Called on cluster master to get an assigned desktops on the whole cluster.
     */
    String getAssignedDesktop(String poolUuid, String userUuid);

    /**
     * Called on cluster master to get an unassigned desktops on the whole cluster.
     */
    String assignDesktop(String poolUuid, String userUuid);

    /**
     * Called on server when a node needs to assign a local desktop from it.
     */
    String assignLocalDesktopToUser(String poolUuid, String userUuid);
}
