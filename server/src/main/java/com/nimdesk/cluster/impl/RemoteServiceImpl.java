/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.cluster.impl;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.List;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.nimdesk.cluster.ClusterSpec;
import com.nimdesk.cluster.RemoteService;
import com.nimdesk.model.DiskImage;
import com.nimdesk.model.Host;
import com.nimdesk.model.Pool;
import com.nimdesk.model.Server;
import com.nimdesk.model.Storage;
import com.nimdesk.model.VmImage;
import com.nimdesk.model.VmInstance;
import com.nimdesk.spring.security.SecurityProviderManager;
import com.nimdesk.ui.server.DesktopPoolService;
import com.nimdesk.ui.server.ImportVmSpec;
import com.nimdesk.vdi.VdiController;

@Service("remoteService")
public class RemoteServiceImpl implements RemoteService, InitializingBean {
    private static final Logger LOGGER = Logger.getLogger(RemoteServiceImpl.class);

    @Autowired
    private SecurityProviderManager _authManager;

    @Autowired
    private Server.Service _serverService;

    @Autowired
    private Host.Service _hostService;

    @Autowired
    private Storage.Service _storageService;

    @Autowired
    private VmImage.Service _vmImageService;

    @Autowired
    private DiskImage.Service _diskImageService;

    @Autowired
    private Pool.Service _poolService;

    @Autowired
    private VdiController _vdiController;

    @Autowired
    private DesktopPoolService _desktopPoolService;

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(_authManager, "A SecurityProviderManager must be set");
        Assert.notNull(_serverService, "A Server.Service must be set");
        Assert.notNull(_hostService, "A Host.Service must be set");
        Assert.notNull(_storageService, "A Storage.Service must be set");
        Assert.notNull(_vmImageService, "A VmImage.Service must be set");
        Assert.notNull(_diskImageService, "A DiskImage.Service must be set");
        Assert.notNull(_poolService, "A Pool.Service must be set");
        Assert.notNull(_desktopPoolService, "A DesktopPoolService must be set");
    }

    @Override
    public ClusterSpec verifyCluster(String adminName, String adminPassword, Properties properties) {
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(adminName, adminPassword);
        Authentication authentication = null;

        try {
            authentication = _authManager.authenticate(token);
        } catch (RuntimeException e) {
            LOGGER.error(String.format("Unknown error while attempting to authenticate admin \"%s\"", adminName), e);
            throw e;
        }

        if (authentication == null) {
            LOGGER.error(String.format("Unable to authenticate admin \"%s\"", adminName));
            return null;
        }

        Server localServer = _serverService.getLocalServer();
        if (localServer == null) {
            LOGGER.error(String.format("Unable to find local server"));
            return null;
        }

        Host localHost = _hostService.getLocalHost();
        if (localHost == null) {
            LOGGER.error(String.format("Unable to find local host"));
            return null;
        }

        Properties retProperties = new Properties();

        if (properties != null) {
            String vCenterIp = properties.getProperty("vCenterIp");
            if (!StringUtils.isEmpty(vCenterIp)) {
                List<? extends Host> hosts = _hostService.getAll(null);
                if (hosts != null) {
                    for (Host host : hosts) {
                        if (vCenterIp.equals(host.getVCenterIp())
                                && !StringUtils.isEmpty(host.getVCenterUsername())
                                && !StringUtils.isEmpty(host.getVCenterPassword())) {
                            retProperties.put("vCenterIp", vCenterIp);
                            if (!StringUtils.isEmpty(host.getVCenterAddress())) {
                                retProperties.put("vCenterAddress", host.getVCenterAddress());
                            }
                            retProperties.put("vCenterUsername", host.getVCenterUsername());
                            retProperties.put("vCenterPassword", host.getVCenterPassword());
                            break;
                        }
                    }
                }
            }
        }

        if (localHost.isSharedDatastoreForAllDesktops()) {
            retProperties.put("sharedDsForAll", "1");
        }

        ClusterSpec spec = new ClusterSpec();
        spec.setClusterName(localServer.getClusterName());
        spec.setClusterId(localServer.getClusterId());
        spec.setSharedDatastoreName(localHost.getSharedDatastoreName());
        spec.setProperties(retProperties);

        return spec;
    }

    @Override
    public void disjoinCluster(String serverUuid) {
        LOGGER.info(String.format("Received disjoinCluster (serverUuid=%s)", serverUuid));

        _serverService.disjoinCluster(serverUuid, null);
    }

    @Override
    public void importImageFromVm(ImportVmSpec importVmSpec) {
        LOGGER.info(String.format("Received importImageFromVm (vmImage=%s, vmName=%s, vmRefId=%s)",
                importVmSpec.getImageName(), importVmSpec.getVmName(), importVmSpec.getVmRefId()));

        VmImage vmImage = _vmImageService.newInstance();
        vmImage.setName(importVmSpec.getImageName());
        vmImage.setDescription(importVmSpec.getImageDescription());
        vmImage.setOsName(importVmSpec.getVmOsName());
        vmImage.setImageSizeInKB(importVmSpec.getImageSizeInKB());
        _vmImageService.importVm(vmImage, importVmSpec.getVmName(), importVmSpec.getVmRefId(), null);
    }

    @Override
    public void importUpdatedVmImage(String vmImageUuid) {
        LOGGER.info(String.format("Received importUpdatedVmImage (vmImage id=%s)", vmImageUuid));

        VmImage vmImage = _vmImageService.getByUuid(vmImageUuid);
        if (vmImage == null || VmImage.Status.Deleting == vmImage.getStatus()) {
            LOGGER.error(String.format("Empty vmImage id=\"%s\" for image update", vmImageUuid));
            throw new RuntimeException("Invalid image");
        }

        _vmImageService.importUpdatedVmImage(vmImage, null);
    }

    @Override
    public boolean allowDiskImageCopy(String callerServerUuid, String diskImageUuid) {
        LOGGER.info(String.format("Received allowDiskImageCopy (callerServerUuid=%s, diskImageUuid=%s)",
                callerServerUuid, diskImageUuid));

        return _vdiController.allowDiskImageCopy(callerServerUuid, diskImageUuid);
    }

    @Override
    public int provisionDesktops(String poolUuid, boolean rebalance) {
        LOGGER.info(String.format("Received provisionDesktops (pool=%s, rebalance=%s)", poolUuid, rebalance ? "true" : "false"));

        return _vdiController.allocateDesktops(poolUuid, rebalance);
    }

    @Override
    public boolean recomposeDesktops(String poolUuid, boolean forced) {
        LOGGER.info(String.format("Received recomposeDesktops (pool=%s, forced=%s)", poolUuid, forced ? "true" : "false"));

        return _vdiController.recomposeDesktops(poolUuid, forced);
    }

    @Override
    public boolean rebalanceDesktops(String poolUuid) {
        LOGGER.info(String.format("Received rebalanceDesktops (pool=%s)", poolUuid));

        return _vdiController.rebalanceDesktops(poolUuid);
    }

    @Override
    public String getAssignedDesktop(String poolUuid, String userUuid) {
        LOGGER.info(String.format("Received getAssignedDesktop (pool=%s, user=%s)", poolUuid, userUuid));

        VmInstance vmInstance = _vdiController.getAssignedDesktop(poolUuid, userUuid);
        return (vmInstance != null) ? vmInstance.getUuid() : null;
    }

    @Override
    public String assignDesktop(String poolUuid, String userUuid) {
        LOGGER.info(String.format("Received assignDesktop (pool=%s, user=%s)", poolUuid, userUuid));

        VmInstance vmInstance = _vdiController.assignDesktop(poolUuid, userUuid);
        return (vmInstance != null) ? vmInstance.getUuid() : null;
    }

    @Override
    public String assignLocalDesktopToUser(String poolUuid, String userUuid) {
        LOGGER.info(String.format("Received assignLocalDesktopToUser (pool=%s, user=%s)", poolUuid, userUuid));

        VmInstance vmInstance = _vdiController.assignLocalDesktopToUser(poolUuid, userUuid);
        return (vmInstance != null) ? vmInstance.getUuid() : null;
    }
}
