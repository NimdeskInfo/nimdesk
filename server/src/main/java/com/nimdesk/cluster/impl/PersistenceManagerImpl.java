/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.cluster.impl;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.jgroups.persistence.CannotPersistException;
import org.jgroups.persistence.CannotRemoveException;
import org.jgroups.persistence.CannotRetrieveException;
import org.jgroups.persistence.PersistenceManager;

import com.nimdesk.database.vo.BaseVO;

@SuppressWarnings("deprecation")
public class PersistenceManagerImpl implements PersistenceManager {
    private static final Logger LOGGER = Logger.getLogger(PersistenceManagerImpl.class);

    private static ClusterReplicatorImpl _impl = null;

    static synchronized void init(ClusterReplicatorImpl impl) {
        _impl = impl;
    }

    static synchronized void uninit() {
        _impl = null;
    }

    public PersistenceManagerImpl(String filePath) {
    }

    @Override
    public void save(Serializable key, Serializable val) throws CannotPersistException {
        try {
            _impl.save((BaseVO) val);
        } catch (Exception e) {
            LOGGER.error(String.format("Failed to save object \"%s\" (type=%s)",
                    ((BaseVO) val).getUuid(), val.getClass().getSimpleName()), e);
        }
    }

    @Override
    public Serializable remove(Serializable key) throws CannotRemoveException {
        BaseVO val = _impl.getObjectFromKey((String) key);
        if (val != null) {
            try {
                _impl.remove(val);
            } catch (Exception e) {
                LOGGER.error(String.format("Failed to remove object \"%s\" (type=%s)",
                        val.getUuid(), val.getClass().getSimpleName()), e);
            }
        }

        return val;
    }

    @Override
    @SuppressWarnings("rawtypes")
    public void saveAll(Map map) throws CannotPersistException {
        if (map != null) {
            for (Object obj : map.values()) {
                if (obj instanceof BaseVO) {
                    try {
                        _impl.save((BaseVO) obj);
                    } catch (Exception e) {
                        LOGGER.error(String.format("Failed to save object \"%s\" (type=%s)",
                                ((BaseVO) obj).getUuid(), obj.getClass().getSimpleName()), e);
                    }
                }
            }
        }
    }

    @Override
    @SuppressWarnings("rawtypes")
    public Map retrieveAll() throws CannotRetrieveException {
        Map<String, BaseVO> map = new HashMap<String, BaseVO>();
        return map;
    }

    @Override
    public void clear() throws CannotRemoveException {
    }

    @Override
    public void shutDown() {
    }
}
