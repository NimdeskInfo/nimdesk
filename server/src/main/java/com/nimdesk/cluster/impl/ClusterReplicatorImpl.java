/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.cluster.impl;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.jgroups.Address;
import org.jgroups.Channel;
import org.jgroups.Event;
import org.jgroups.JChannelFactory;
import org.jgroups.MergeView;
import org.jgroups.PhysicalAddress;
import org.jgroups.View;
import org.jgroups.blocks.ReplicatedHashMap;
import org.jgroups.persistence.PersistenceFactory;
import org.jgroups.stack.IpAddress;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.ClassUtils;

import com.nimdesk.cluster.ClusterReplicator;
import com.nimdesk.database.vo.AdminUserVO;
import com.nimdesk.database.vo.ApplicationVO;
import com.nimdesk.database.vo.BaseVO;
import com.nimdesk.database.vo.DiskImageVO;
import com.nimdesk.database.vo.GlobalSettingVO;
import com.nimdesk.database.vo.HostVO;
import com.nimdesk.database.vo.LdapServerVO;
import com.nimdesk.database.vo.PoolUserVO;
import com.nimdesk.database.vo.PoolVO;
import com.nimdesk.database.vo.ServerVO;
import com.nimdesk.database.vo.StorageVO;
import com.nimdesk.database.vo.TaskVO;
import com.nimdesk.database.vo.UserGroupVO;
import com.nimdesk.database.vo.UserSessionVO;
import com.nimdesk.database.vo.UserVO;
import com.nimdesk.database.vo.VmImageVO;
import com.nimdesk.database.vo.VmInstanceVO;
import com.nimdesk.event.ClusterViewChangeEvent;
import com.nimdesk.model.AdminUser;
import com.nimdesk.model.Application;
import com.nimdesk.model.Base;
import com.nimdesk.model.DiskImage;
import com.nimdesk.model.GlobalSetting;
import com.nimdesk.model.Host;
import com.nimdesk.model.LdapServer;
import com.nimdesk.model.Pool;
import com.nimdesk.model.PoolUser;
import com.nimdesk.model.Server;
import com.nimdesk.model.Storage;
import com.nimdesk.model.Task;
import com.nimdesk.model.User;
import com.nimdesk.model.UserGroup;
import com.nimdesk.model.UserSession;
import com.nimdesk.model.VmImage;
import com.nimdesk.model.VmInstance;
import com.nimdesk.service.EventBusService;
import com.nimdesk.service.ObjectPropertyChangeEvent;
import com.nimdesk.service.ObjectPropertyChangeListener;
import com.nimdesk.tracker.HostUsageTracker;
import com.nimdesk.tracker.HostUsageTrackerVO;
import com.nimdesk.tracker.ProgressTracker;
import com.nimdesk.tracker.ProgressTrackerVO;
import com.nimdesk.tracker.TrackerManager;

@Service("clusterReplicator")
@SuppressWarnings("deprecation")
public class ClusterReplicatorImpl implements ClusterReplicator,
        TrackerManager.HostUsageChangeListener, TrackerManager.ProgressChangeListener, InitializingBean,
        ObjectPropertyChangeListener, ReplicatedHashMap.Notification<String, BaseVO> {
    private static final Logger LOGGER = Logger.getLogger(ClusterReplicatorImpl.class);

    private static final String JGROUPS_BIND_ADDR = "jgroups.bind_addr";
    private static final String JGROUPS_INITIAL_HOSTS = "jgroups.tcpping.initial_hosts";

    private static final String CHANNEL_PROPS = "tcp.xml";
    private static final String PERSIST_PROPS = "jgroups.properties";

    private static final int CORE_CLUSTER_POOL_SIZE = 2;
    private static final int MAX_CLUSTER_POOL_SIZE = 200;

    private final ExecutorService _executor = new ThreadPoolExecutor(CORE_CLUSTER_POOL_SIZE,
            MAX_CLUSTER_POOL_SIZE, 300L, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>());

    @Autowired
    private EventBusService _eventBusService;

    @Autowired
    private GlobalSetting.Service _globalSettingService;

    @Autowired
    private Server.Service _serverService;

    @Autowired
    private Host.Service _hostService;

    @Autowired
    private Storage.Service _storageService;

    @Autowired
    private LdapServer.Service _ldapServerService;

    @Autowired
    private VmImage.Service _vmImageService;

    @Autowired
    private Application.Service _applicationService;

    @Autowired
    private DiskImage.Service _diskImageService;

    @Autowired
    private Pool.Service _poolService;

    @Autowired
    private VmInstance.Service _vmInstanceService;

    @Autowired
    private AdminUser.Service _adminUserService;

    @Autowired
    private User.Service _userService;

    @Autowired
    private UserGroup.Service _userGroupService;

    @Autowired
    private PoolUser.Service _poolUserService;

    @Autowired
    private UserSession.Service _userSessionService;

    @Autowired
    private Task.Service _taskService;

    @Autowired
    private TrackerManager _trackerManager;

    private final ExecutorService _mergeViewExecutor = Executors.newSingleThreadExecutor();
    private ReplicatedHashMap<String, BaseVO> _map;
    private boolean _isMaster = false; // By default, assume current node is a mater.

    private final Map<Class<? extends BaseVO>, LocalPersistenceHandler> _localPersistenceHandlers =
            new HashMap<Class<? extends BaseVO>, LocalPersistenceHandler>();

    private final Map<Class<? extends BaseVO>, Integer> _typeIdMap =
            new HashMap<Class<? extends BaseVO>, Integer>();

    private final Map<Integer, Class<? extends BaseVO>> _idTypeMap =
            new HashMap<Integer, Class<? extends BaseVO>>();

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(_eventBusService, "An EventBusService must be set");
        Assert.notNull(_globalSettingService, "A GlobalSetting.Service must be set");
        Assert.notNull(_serverService, "A Server.Service must be set");
        Assert.notNull(_hostService, "A Host.Service must be set");
        Assert.notNull(_storageService, "A Storage.Service must be set");
        Assert.notNull(_ldapServerService, "An LdapServer.Service must be set");
        Assert.notNull(_vmImageService, "A VmImage.Service must be set");
        Assert.notNull(_diskImageService, "A DiskImage.Service must be set");
        Assert.notNull(_poolService, "A Pool.Service must be set");
        Assert.notNull(_vmInstanceService, "A VmInstance.Service must be set");
        Assert.notNull(_adminUserService, "An AdminUser.Service must be set");
        Assert.notNull(_userService, "A User.Service must be set");
        Assert.notNull(_userGroupService, "A UserGroup.Service must be set");
        Assert.notNull(_poolUserService, "A PoolUser.Service must be set");
        Assert.notNull(_userSessionService, "A UserSession.Service must be set");
        Assert.notNull(_taskService, "A Task.Service must be set");
        Assert.notNull(_trackerManager, "A TrackerManager must be set");

        setLocalPersistenceHandler(ServerVO.class, new ServerPersistenceHandler(), 1);
        setLocalPersistenceHandler(HostVO.class, new HostPersistenceHandler(), 2);
        setLocalPersistenceHandler(StorageVO.class, new StoragePersistenceHandler(), 3);
        setLocalPersistenceHandler(LdapServerVO.class, new LdapServerPersistenceHandler(), 4);
        setLocalPersistenceHandler(VmImageVO.class, new VmImagePersistenceHandler(), 5);
        setLocalPersistenceHandler(ApplicationVO.class, new ApplicationPersistenceHandler(), 6);
        setLocalPersistenceHandler(DiskImageVO.class, new DiskImagePersistenceHandler(), 7);
        setLocalPersistenceHandler(PoolVO.class, new PoolPersistenceHandler(), 8);
        setLocalPersistenceHandler(VmInstanceVO.class, new VmInstancePersistenceHandler(), 9);
        setLocalPersistenceHandler(AdminUserVO.class, new AdminUserPersistenceHandler(), 10);
        setLocalPersistenceHandler(UserVO.class, new UserPersistenceHandler(), 11);
        setLocalPersistenceHandler(UserGroupVO.class, new UserGroupPersistenceHandler(), 12);
        setLocalPersistenceHandler(PoolUserVO.class, new PoolUserPersistenceHandler(), 13);
        setLocalPersistenceHandler(UserSessionVO.class, new UserSessionPersistenceHandler(), 14);
        setLocalPersistenceHandler(TaskVO.class, new TaskPersistenceHandler(), 15);
        setLocalPersistenceHandler(HostUsageTrackerVO.class, new HostUsageTrackerPersistenceHandler(), 17);
        setLocalPersistenceHandler(ProgressTrackerVO.class, new ProgressTrackerPersistenceHandler(), 18);
        setLocalPersistenceHandler(GlobalSettingVO.class, new GlobalSettingPersistenceHandler(), 19);
    }

    private void setLocalPersistenceHandler(Class<? extends BaseVO> clazz, LocalPersistenceHandler handler, int typeId) {
        _localPersistenceHandlers.put(clazz, handler);
        _typeIdMap.put(clazz, typeId);
        _idTypeMap.put(typeId, clazz);
    }

    private String getObjectKey(BaseVO base) {
        String uuid = base.getUuid();
        Integer classId = _typeIdMap.get(base.getClass());

        if (classId != null) {
            return String.format("%d:%s", classId, uuid);
        } else {
            return String.format("0:%s", uuid);
        }
    }

    BaseVO getObjectFromKey(String key) {
        int typeId = 0;
        String uuid = null;

        int index = StringUtils.indexOf(key, ':');
        if (index >= 0) {
            try {
                uuid = StringUtils.substring(key, index + 1);
                typeId = Integer.parseInt(StringUtils.substring(key, 0, index));
            } catch (Exception e) {
                // Ignore error
                LOGGER.warn(String.format("Invalid object key \"%s\"", key), e);
                return null;
            }
        }

        Class<? extends BaseVO> clazz = _idTypeMap.get(typeId);
        if (clazz == null) {
            return null;
        }

        LocalPersistenceHandler handler = _localPersistenceHandlers.get(clazz);
        if (handler == null) {
            LOGGER.warn(String.format("Unable to find persistence handler for class %s", clazz.getName()));
            return null;
        }

        return (BaseVO) handler.getByUuid(uuid);
    }

    @Override
    public void newObject(ObjectPropertyChangeEvent event) {
        final BaseVO base = (BaseVO) event.getSource();
        if (base.fromRemote()) {
            // Don't handle if this comes from _map the 1st time.
            // This method is called when persisting remote object.
            // Change to non-remote thereafter.
            base.fromRemote(false);
            return;
        }

        if (base instanceof ProgressTrackerVO) {
            if (!((ProgressTrackerVO) base).isGlobal()) {
                return;
            }
        }

        LocalPersistenceHandler handler = _localPersistenceHandlers.get(base.getClass());
        if (handler != null) {
            _executor.execute(new Runnable() {
                @Override
                public void run() {
                    // If we are handling this type, put it on map.
                    base.fromLocal(true);
                    _map.put(getObjectKey(base), base);
                }
            });
        }
    }

    @Override
    public void deleteObject(ObjectPropertyChangeEvent event) {
        final BaseVO base = (BaseVO) event.getSource();
        if (base.fromRemote()) {
            // Don't handle if this comes from _map the 1st time.
            // This method is called when persisting remote object.
            // Change to non-remote thereafter.
            base.fromRemote(false);
            return;
        }

        if (base instanceof ProgressTrackerVO) {
            if (!((ProgressTrackerVO) base).isGlobal()) {
                return;
            }
        }

        final LocalPersistenceHandler handler = _localPersistenceHandlers.get(base.getClass());
        if (handler != null) {
            _executor.execute(new Runnable() {
                @Override
                public void run() {
                    // If we are handling this type, remove it from map.
                    base.fromLocal(true);
                    if (handler.canRemoveFromMap(base)) {
                        _map.remove(getObjectKey(base));
                    }
                }
            });
        }
    }

    @Override
    public void propertyChange(ObjectPropertyChangeEvent event) {
        final BaseVO base = (BaseVO) event.getSource();
        if (base.fromRemote()) {
            // Don't handle if this comes from _map the 1st time.
            // This method is called when persisting remote object.
            // Change to non-remote thereafter.
            base.fromRemote(false);
            return;
        }

        if (base instanceof ProgressTrackerVO) {
            if (!((ProgressTrackerVO) base).isGlobal()) {
                return;
            }
        }

        LocalPersistenceHandler handler = _localPersistenceHandlers.get(base.getClass());
        if (handler != null) {
            _executor.execute(new Runnable() {
                @Override
                public void run() {
                    // If we are handling this type, update it on map.
                    base.fromLocal(true);
                    _map.put(getObjectKey(base), base);
                }
            });
        }
    }

    @Override
    public void onUsageChange(String hostUuid, HostUsageTracker hostUsage) {
        final HostUsageTrackerVO hostUsageVO = (HostUsageTrackerVO) hostUsage;
        if (!hostUsageVO.fromLocal()) {
            return;
        }

        _executor.execute(new Runnable() {
            @Override
            public void run() {
                _map.put(getObjectKey(hostUsageVO), hostUsageVO);
            }
        });
    }

    @Override
    public void onUsageRemove(String hostUuid, HostUsageTracker hostUsage) {
        final HostUsageTrackerVO hostUsageVO = (HostUsageTrackerVO) hostUsage;
        if (!hostUsageVO.fromLocal()) {
            return;
        }

        _executor.execute(new Runnable() {
            @Override
            public void run() {
                _map.remove(getObjectKey(hostUsageVO));
            }
        });
    }

    @Override
    public void onProgressChange(String entityUuid, ProgressTracker progress) {
        final ProgressTrackerVO progressVO = (ProgressTrackerVO) progress;
        if (!progressVO.fromLocal()) {
            return;
        }

        _executor.execute(new Runnable() {
            @Override
            public void run() {
                _map.put(getObjectKey(progressVO), progressVO);
            }
        });
    }

    @Override
    public void onProgressRemove(String entityUuid, ProgressTracker progress) {
        final ProgressTrackerVO progressVO = (ProgressTrackerVO) progress;
        if (!progressVO.fromLocal()) {
            return;
        }

        _executor.execute(new Runnable() {
            @Override
            public void run() {
                _map.remove(getObjectKey(progressVO));
            }
        });
    }

    @Override
    public void start(String clusterName, String bindAddress, String[] initialHosts) throws Exception {
        LOGGER.info(String.format("System default: %s=%s", JGROUPS_BIND_ADDR, System.getProperty(JGROUPS_BIND_ADDR)));
        LOGGER.info(String.format("System default: %s=%s", JGROUPS_INITIAL_HOSTS, System.getProperty(JGROUPS_INITIAL_HOSTS)));

        if (!StringUtils.isEmpty(bindAddress)) {
            LOGGER.info(String.format("Set: %s=%s", JGROUPS_BIND_ADDR, bindAddress));
            System.setProperty(JGROUPS_BIND_ADDR, bindAddress);
        }

        StringBuilder sb = new StringBuilder();

        if (initialHosts != null && initialHosts.length > 0) {
            for (String initialHost : initialHosts) {
                if (sb.length() > 0) {
                    sb.append(",");
                }
                sb.append(initialHost)
                .append("[7800],")
                .append(initialHost)
                .append("[7801]");
            }
        } else {
            List<? extends Server> servers = _serverService.getAll(null);
            if (servers != null) {
                for (Server server : servers) {
                    if (server.isLocal()) {
                        continue;
                    }

                    if (sb.length() > 0) {
                        sb.append(",");
                    }
                    sb.append(server.getPublicAddress())
                      .append("[7800],")
                      .append(server.getPublicAddress())
                      .append("[7801]");
                }
            }
        }
        if (sb.length() > 0) {
            String hostList = sb.toString();
            LOGGER.info(String.format("Set: %s=%s", JGROUPS_INITIAL_HOSTS, hostList));
            System.setProperty(JGROUPS_INITIAL_HOSTS, hostList);
        }

        ClassLoader cl = ClassUtils.getDefaultClassLoader();

        PersistenceFactory.getInstance().createManager(cl.getResource(PERSIST_PROPS).getFile());
        PersistenceManagerImpl.init(this);

        _map = new ReplicatedHashMap<String, BaseVO>(clusterName, new JChannelFactory(),
                cl.getResource(CHANNEL_PROPS).getFile(), true, 10000) {
            @Override
            public void viewAccepted(View view) {
                if (!handleView(getChannel(), view)) {
                    super.viewAccepted(view);
                }
            }
        };
        _map.addNotifier(this);
        _map.setBlockingUpdates(true);
        _map.setTimeout(10000);

        // By defaut, assume we are the master.
        if (_map.getChannel() != null) {
            onCluterViewChange(_map.getChannel().getView());
        }
        LOGGER.info(String.format("Current node %s the cluster master", _isMaster ? "is" : "is not"));

        _globalSettingService.addObjectPropertyChangeListener(this);
        _serverService.addObjectPropertyChangeListener(this);
        _hostService.addObjectPropertyChangeListener(this);
        _storageService.addObjectPropertyChangeListener(this);
        _ldapServerService.addObjectPropertyChangeListener(this);
        _vmImageService.addObjectPropertyChangeListener(this);
        _applicationService.addObjectPropertyChangeListener(this);
        _diskImageService.addObjectPropertyChangeListener(this);
        _poolService.addObjectPropertyChangeListener(this);
        _vmInstanceService.addObjectPropertyChangeListener(this);
        _adminUserService.addObjectPropertyChangeListener(this);
        _userService.addObjectPropertyChangeListener(this);
        _userGroupService.addObjectPropertyChangeListener(this);
        _poolUserService.addObjectPropertyChangeListener(this);
        _userSessionService.addObjectPropertyChangeListener(this);
        _taskService.addObjectPropertyChangeListener(this);
        _trackerManager.addHostUsageChangeListener(this);
        _trackerManager.addProgressChangeListener(this);

        // Load globalSettings
        List<? extends GlobalSetting> globalSettings = _globalSettingService.getAll(null);
        if (globalSettings != null) {
            if (_isMaster) {
                for (GlobalSetting globalSetting : globalSettings) {
                    if (globalSetting instanceof GlobalSettingVO) {
                        GlobalSettingVO globalSettingVO = (GlobalSettingVO) globalSetting;
                        globalSettingVO.fromLocal(true);
                        _map.put(getObjectKey(globalSettingVO), globalSettingVO);
                    }
                }
            } else {
                for (GlobalSetting globalSetting : globalSettings) {
                    if (globalSetting instanceof GlobalSettingVO) {
                        if (!_map.containsKey(getObjectKey((GlobalSettingVO) globalSetting))) {
                            LOGGER.info(String.format("Delete orphaned local globalSetting \"%s\"(%s)",
                                    globalSetting.getKey(), globalSetting.getUuid()));
                            _globalSettingService.delete(globalSetting);
                        }
                    }
                }
            }
        }

        // Load local hosts
        List<? extends Host> hosts = _hostService.getLocalHosts();
        if (hosts != null) {
            for (Host host : hosts) {
                if (host instanceof HostVO) {
                    HostVO hostVO = (HostVO) host;
                    hostVO.fromLocal(true);
                    _map.put(getObjectKey(hostVO), hostVO);
                }
            }
        }

        // Load local storages
        List<? extends Storage> storages = _storageService.getLocalStorages();
        if (storages != null) {
            for (Storage storage : storages) {
                if (storage instanceof StorageVO) {
                    StorageVO storageVO = (StorageVO) storage;
                    storageVO.fromLocal(true);
                    _map.put(getObjectKey(storageVO), storageVO);
                }
            }
        }

        // Load vmImages
        List<? extends VmImage> vmImages = _vmImageService.getAll(null);
        if (vmImages != null) {
            if (_isMaster) {
                for (VmImage vmImage : vmImages) {
                    if (vmImage instanceof VmImageVO) {
                        VmImageVO vmImageVO = (VmImageVO) vmImage;
                        vmImageVO.fromLocal(true);
                        _map.put(getObjectKey(vmImageVO), vmImageVO);
                    }
                }
            } else {
                for (VmImage vmImage : vmImages) {
                    if (vmImage instanceof VmImageVO) {
                        if (!_map.containsKey(getObjectKey((VmImageVO) vmImage))) {
                            LOGGER.info(String.format("Delete orphaned local image \"%s\"(%s)",
                                    vmImage.getName(), vmImage.getUuid()));
                            vmImage.setStatus(VmImage.Status.Deleting);
                            _vmImageService.commit(vmImage);
                        }
                    }
                }
            }
        }

        // Load pools
        List<? extends Pool> pools = _poolService.getAll(null);
        if (pools != null) {
            if (_isMaster) {
                for (Pool pool : pools) {
                    if (pool instanceof PoolVO) {
                        PoolVO poolVO = (PoolVO) pool;
                        poolVO.fromLocal(true);
                        _map.put(getObjectKey(poolVO), poolVO);
                    }
                }
            } else {
                for (Pool pool : pools) {
                    if (pool instanceof PoolVO) {
                        if (!_map.containsKey(getObjectKey((PoolVO) pool))) {
                            LOGGER.info(String.format("Delete orphaned local pool \"%s\"(%s)",
                                    pool.getName(), pool.getUuid()));
                            _poolUserService.deleteByPool(pool.getUuid());

                            pool.setStatus(Pool.Status.Deleting);
                            _poolService.commit(pool);
                        }
                    }
                }
            }
        }

        // Load local diskImages
        List<? extends DiskImage> diskImages = _diskImageService.getLocalDiskImages();
        if (diskImages != null) {
            for (DiskImage diskImage : diskImages) {
                if (diskImage instanceof DiskImageVO) {
                    DiskImageVO diskImageVO = (DiskImageVO) diskImage;
                    diskImageVO.fromLocal(true);
                    _map.put(getObjectKey(diskImageVO), diskImageVO);
                }
            }
        }

        // Load local vmInstances
        List<? extends VmInstance> vmInstances = _vmInstanceService.getLocalVmInstances();
        if (vmInstances != null) {
            for (VmInstance vmInstance : vmInstances) {
                if (vmInstance instanceof VmInstanceVO) {
                    VmInstanceVO vmInstanceVO = (VmInstanceVO) vmInstance;
                    vmInstanceVO.fromLocal(true);
                    _map.put(getObjectKey(vmInstanceVO), vmInstanceVO);
                }
            }
        }

        // Load ldap servers
        List<? extends LdapServer> ldapServers = _ldapServerService.getAll(null);
        if (ldapServers != null) {
            if (_isMaster) {
                for (LdapServer ldapServer : ldapServers) {
                    if (ldapServer instanceof LdapServerVO) {
                        LdapServerVO ldapServerVO = (LdapServerVO) ldapServer;
                        ldapServerVO.fromLocal(true);
                        _map.put(getObjectKey(ldapServerVO), ldapServerVO);
                    }
                }
            } else {
                for (LdapServer ldapServer : ldapServers) {
                    if (ldapServer instanceof LdapServerVO) {
                        if (!_map.containsKey(getObjectKey((LdapServerVO) ldapServer))) {
                            LOGGER.info(String.format("Delete orphaned local LdapServer \"%s\", domain \"%s\"",
                                    ldapServer.getAddress(), ldapServer.getDomain()));
                            _ldapServerService.delete(ldapServer);
                        }
                    }
                }
            }
        }

        // Load adminUsers
        List<? extends AdminUser> adminUsers = _adminUserService.getAll(null);
        if (adminUsers != null) {
            if (_isMaster) {
                for (AdminUser adminUser : adminUsers) {
                    if (adminUser instanceof AdminUserVO) {
                        AdminUserVO adminUserVO = (AdminUserVO) adminUser;
                        adminUserVO.fromLocal(true);
                        _map.put(getObjectKey(adminUserVO), adminUserVO);
                    }
                }
            } else {
                for (AdminUser adminUser : adminUsers) {
                    if (adminUser instanceof AdminUserVO) {
                        if (!_map.containsKey(getObjectKey((AdminUserVO) adminUser))) {
                            LOGGER.info(String.format("Delete orphaned local adminUser \"%s\"(%s)",
                                    adminUser.getUsername(), adminUser.getUuid()));
                            _adminUserService.delete(adminUser);
                        }
                    }
                }
            }
        }

        // Load users
        List<? extends User> users = _userService.getAll(null);
        if (users != null) {
            if (_isMaster) {
                for (User user : users) {
                    if (user instanceof UserVO) {
                        UserVO userVO = (UserVO) user;
                        userVO.fromLocal(true);
                        _map.put(getObjectKey(userVO), userVO);
                    }
                }
            } else {
                for (User user : users) {
                    if (user instanceof UserVO) {
                        if (!_map.containsKey(getObjectKey((UserVO) user))) {
                            LOGGER.info(String.format("Delete orphaned local user \"%s@%s\"(%s)",
                                    user.getLogonName(), user.getDomain(), user.getUuid()));
                            _userService.delete(user);
                            if (user.getType() == User.Type.User) {
                                _userGroupService.deleteUserGroupsForUser(user.getUuid());
                            } else {
                                _userGroupService.deleteUserGroupsForGroup(user.getUuid());
                            }
                            _poolUserService.deleteByUser(user.getUuid());
                        }
                    }
                }
            }
        }

        // Load userGroups
        List<? extends UserGroup> userGroups = _userGroupService.getAll(null);
        if (userGroups != null) {
            if (_isMaster) {
                for (UserGroup userGroup : userGroups) {
                    if (userGroup instanceof UserGroupVO) {
                        UserGroupVO userGroupVO = (UserGroupVO) userGroup;
                        userGroupVO.fromLocal(true);
                        _map.put(getObjectKey(userGroupVO), userGroupVO);
                    }
                }
            } else {
                for (UserGroup userGroup : userGroups) {
                    if (userGroup instanceof UserGroupVO) {
                        if (!_map.containsKey(getObjectKey((UserGroupVO) userGroup))) {
                            _userGroupService.delete(userGroup);
                        }
                    }
                }
            }
        }

        // Load poolUsers
        List<? extends PoolUser> poolUsers = _poolUserService.getAll(null);
        if (poolUsers != null) {
            if (_isMaster) {
                for (PoolUser poolUser : poolUsers) {
                    if (poolUser instanceof PoolUserVO) {
                        PoolUserVO poolUserVO = (PoolUserVO) poolUser;
                        poolUserVO.fromLocal(true);
                        _map.put(getObjectKey(poolUserVO), poolUserVO);
                    }
                }
            } else {
                for (PoolUser poolUser : poolUsers) {
                    if (poolUser instanceof PoolUserVO) {
                        if (!_map.containsKey(getObjectKey((PoolUserVO) poolUser))) {
                            _poolUserService.delete(poolUser);
                        }
                    }
                }
            }
        }

        // Load local userSessions
        List<? extends UserSession> userSessions = _userSessionService.getLocalUserSessions();
        if (userSessions != null) {
            for (UserSession userSession : userSessions) {
                if (userSession instanceof UserSessionVO) {
                    UserSessionVO userSessionVO = (UserSessionVO) userSession;
                    userSessionVO.fromLocal(true);
                    _map.put(getObjectKey(userSessionVO), userSessionVO);
                }
            }
        }

        // Load local tasks
        List<? extends Task> tasks = _taskService.getLocalTasks();
        if (tasks != null) {
            for (Task task : tasks) {
                if (task instanceof TaskVO) {
                    TaskVO taskVO = (TaskVO) task;
                    taskVO.fromLocal(true);
                    _map.put(getObjectKey(taskVO), taskVO);
                }
            }
        }

        // Load local eventLogs

        // Load local Server. Do this at last.
        Server server = _serverService.getLocalServer();
        if (_map.getLocalAddress() != null) {
            server.setClusterAddress(_map.getLocalAddress().toString());
            _serverService.commit(server);
        }
        if (server instanceof ServerVO) {
            ServerVO serverVO = (ServerVO) server;
            serverVO.fromLocal(true);
            _map.put(getObjectKey(serverVO), serverVO);
        }
    }

    @Override
    public void stop() {
        _globalSettingService.removeObjectPropertyChangeListener(this);
        _serverService.removeObjectPropertyChangeListener(this);
        _hostService.removeObjectPropertyChangeListener(this);
        _storageService.removeObjectPropertyChangeListener(this);
        _ldapServerService.removeObjectPropertyChangeListener(this);
        _vmImageService.removeObjectPropertyChangeListener(this);
        _diskImageService.removeObjectPropertyChangeListener(this);
        _poolService.removeObjectPropertyChangeListener(this);
        _vmInstanceService.removeObjectPropertyChangeListener(this);
        _adminUserService.removeObjectPropertyChangeListener(this);
        _userService.removeObjectPropertyChangeListener(this);
        _userGroupService.removeObjectPropertyChangeListener(this);
        _poolUserService.removeObjectPropertyChangeListener(this);
        _userSessionService.removeObjectPropertyChangeListener(this);
        _taskService.removeObjectPropertyChangeListener(this);
        _trackerManager.removeHostUsageChangeListener(this);
        _trackerManager.removeProgressChangeListener(this);

        _map.removeNotifier(this);
        _map.stop();
        PersistenceManagerImpl.uninit();

        _isMaster = true;

        ClusterViewChangeEvent event = new ClusterViewChangeEvent(_isMaster, null);
        _eventBusService.post(event);
    }

    private void onCluterViewChange(View view) {
        if (view != null) {
            // Check whether this server is the master of the cluster.
            Vector<Address> members = view.getMembers();
            if (members != null && !members.isEmpty() && _map != null) {
                _isMaster = members.get(0).equals(_map.getLocalAddress());

                String masterAddress = null;
                if (!_isMaster) {
                    PhysicalAddress physicalAddr = (PhysicalAddress) _map.getChannel().downcall(new Event(Event.GET_PHYSICAL_ADDRESS, members.get(0)));
                    if (physicalAddr instanceof IpAddress) {
                        masterAddress = ((IpAddress) physicalAddr).getIpAddress().getHostAddress();
                        LOGGER.info(String.format("Cluster master address: %s", masterAddress));
                    }
                }

                ClusterViewChangeEvent event = new ClusterViewChangeEvent(_isMaster, masterAddress);
                _eventBusService.post(event);
            }
        }
    }

    @Override
    public void entrySet(String key, BaseVO value) {
    }

    @Override
    public void entryRemoved(String key) {
    }

    @Override
    public void viewChange(View view, Vector<Address> joinedMembers, Vector<Address> leftMembers) {
        onCluterViewChange(view);
        LOGGER.info(String.format("View changed. Current node %s the cluster master", _isMaster ? "is" : "is not"));

        for (Address joined : joinedMembers) {
            if (joined.isMulticastAddress()) {
                continue;
            }

            Server server = _serverService.getByClusterAddress(joined.toString());
            if (server != null && Server.Status.Disconnected == server.getStatus()) {
                server.setStatus(Server.Status.Running);
                _serverService.commit(server);
            }
        }

        for (Address left : leftMembers) {
            if (left.isMulticastAddress()) {
                continue;
            }

            Server server = _serverService.getByClusterAddress(left.toString());
            if (server != null
                    && Server.Status.Disconnected != server.getStatus()
                    && Server.Status.Disjoined != server.getStatus()) {
                server.setStatus(Server.Status.Disconnected);
                _serverService.commit(server);
            }
        }
    }

    @Override
    public void contentsSet(Map<String, BaseVO> new_entries) {
    }

    @Override
    public void contentsCleared() {
    }

    private boolean handleView(final Channel channel, View view) {
        if (view instanceof MergeView) {
            final MergeView mergeView = (MergeView) view;
            _mergeViewExecutor.submit(new Runnable() {
                @Override
                public void run() {
                    List<View> subgroups = mergeView.getSubgroups();
                    if (subgroups.isEmpty()) {
                        return;
                    }

                    // Resolve the conflict when merging groups.
                    Collections.sort(subgroups, new Comparator<View>() {
                        @Override
                        public int compare(View o1, View o2) {
                            if (o1.size() < o2.size()) {
                                return 1;
                            } else if (o1.size() > o2.size()) {
                                return -1;
                            } else if (o1.size() == 0) {
                                return 0;
                            }

                            Address least = null;
                            for (Address a : o1.getMembers()) {
                                if (least == null || a.compareTo(least) < 0) {
                                    least = a;
                                }
                            }
                            for (Address a : o2.getMembers()) {
                                if (a.compareTo(least) < 0) {
                                    return 1;
                                }
                            }
                            return -1;
                        }
                    });

                    Address me = channel.getLocalAddress();
                    if (subgroups.get(0).getMembers().contains(me)) {
                        LOGGER.info(String.format("Local server \"%s\" is in the major group", me));
                    } else {
                        LOGGER.info(String.format("Local server \"%s\" is in the minor group", me));
                        // Remove orphaned local desktops
                    }
                }
            });

            return true;
        }

        return false;
    }

    void save(final BaseVO obj) {
        if (obj.fromLocal()) {
            // If the object is from the local db, don't persist it again
            return;
        }

        final LocalPersistenceHandler handler = _localPersistenceHandlers.get(obj.getClass());
        if (handler == null) {
            LOGGER.warn(String.format("Unable to find persistence handler for class %s", obj.getClass().getName()));
            return;
        }

        _executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    // Set flag to avoid ObjectPropertyChange to process it again.
                    obj.fromRemote(true);
                    handler.save(obj);
                } catch (Throwable t) {
                    LOGGER.warn(String.format("Failed to save remote object for class %s", obj.getClass().getName()), t);
                }
            }
        });
    }

    void remove(final BaseVO obj) {
        if (obj.fromLocal()) {
            // If the object is from the local db, don't persist it again
            return;
        }

        final LocalPersistenceHandler handler = _localPersistenceHandlers.get(obj.getClass());
        if (handler == null) {
            LOGGER.warn(String.format("Unable to find persistence handler for class %s", obj.getClass().getName()));
            return;
        }

        _executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    // Set flag to avoid ObjectPropertyChange to process it again.
                    obj.fromRemote(true);
                    handler.remove(obj);
                } catch (Throwable t) {
                    LOGGER.warn(String.format("Failed to remove remote object for class %s", obj.getClass().getName()), t);
                }
            }
        });
    }

    private abstract class LocalPersistenceHandler {
        abstract void save(BaseVO obj);
        abstract void remove(BaseVO obj);
        abstract Base getByUuid(String uuid);

        boolean canRemoveFromMap(BaseVO obj) {
            return true;
        }
    }

    private class GlobalSettingPersistenceHandler extends LocalPersistenceHandler {
        @Override
        void save(BaseVO obj) {
            Assert.isInstanceOf(GlobalSettingVO.class, obj);

            GlobalSettingVO globalSetting = (GlobalSettingVO) obj;

            GlobalSettingVO oldGlobalSetting = (GlobalSettingVO) _globalSettingService.getByUuid(globalSetting.getUuid());
            if (oldGlobalSetting != null) {
                globalSetting.setId(oldGlobalSetting.getId());
            } else {
                // If not cluster master, remove the old entry first
                if (!_isMaster) {
                    oldGlobalSetting = (GlobalSettingVO) _globalSettingService.getByKey(globalSetting.getKey());
                    if (oldGlobalSetting != null) {
                        _globalSettingService.delete(oldGlobalSetting);
                    }
                }
                globalSetting.setId(0L);
            }
            _globalSettingService.commit(globalSetting);
        }

        @Override
        void remove(BaseVO obj) {
            Assert.isInstanceOf(GlobalSettingVO.class, obj);

            GlobalSettingVO globalSetting = (GlobalSettingVO) obj;

            GlobalSettingVO oldGlobalSetting = (GlobalSettingVO) _globalSettingService.getByUuid(globalSetting.getUuid());
            if (oldGlobalSetting != null) {
                globalSetting.setId(oldGlobalSetting.getId());
                _globalSettingService.delete(globalSetting);
            }
        }

        @Override
        Base getByUuid(String uuid) {
            return _globalSettingService.getByUuid(uuid);
        }
    }

    private class ServerPersistenceHandler extends LocalPersistenceHandler {
        @Override
        synchronized void save(BaseVO obj) {
            Assert.isInstanceOf(ServerVO.class, obj);

            ServerVO server = (ServerVO) obj;
            boolean isLocal = _serverService.isLocalServer(server.getUuid());

            Channel channel;
            View view;
            if (!isLocal) {
                if (Server.Status.Disjoined == server.getStatus()) {
                    // Ignore disjoined servers.
                    return;
                }

                if (_map != null
                    && (channel = _map.getChannel()) != null
                    && (view = channel.getView()) != null) {
                    // Check jgoups view to update its status
                    Vector<Address> members = view.getMembers();
                    if (members != null) {
                        boolean foundInView = false;

                        for (Address member : members) {
                            if (member.isMulticastAddress()) {
                                continue;
                            }

                            if (StringUtils.equals(member.toString(), server.getClusterAddress())) {
                                foundInView = true;
                                break;
                            }
                        }

                        if (foundInView) {
                            if (Server.Status.Disconnected == server.getStatus()) {
                                server.setStatus(Server.Status.Running);
                            }
                        } else {
                            if (Server.Status.Disconnected != server.getStatus()) {
                                server.setStatus(Server.Status.Disconnected);
                            }
                        }
                    }
                }
            }

            server.setLocal(false);

            ServerVO oldServer = (ServerVO) _serverService.getByUuid(server.getUuid());
            if (oldServer != null) {
                if (isLocal) {
                    // Don't change the local server.
                    return;
                }
                server.setId(oldServer.getId());
                server.trackPropertyChange(Base.PROP_STATUS, oldServer.getStatus(), server.getStatus());
            } else {
                if (isLocal) {
                    // The object doesn't exist. Remove from the map.
                    _map.remove(getObjectKey(server));
                    return;
                }

                // If a record exists with the same public address, invalidate it.
                oldServer = (ServerVO) _serverService.getByPublicAddress(server.getPublicAddress());
                if (oldServer != null) {
                    oldServer.setPublicAddress("");
                    _serverService.commit(oldServer);
                }

                server.setId(0L);
            }
            _serverService.commit(server);
        }

        @Override
        synchronized void remove(BaseVO obj) {
            Assert.isInstanceOf(ServerVO.class, obj);

            ServerVO server = (ServerVO) obj;
            if (_serverService.isLocalServer(server.getUuid())) {
                // Don't change the local server.
                return;
            }
        }

        @Override
        Base getByUuid(String uuid) {
            return _serverService.getByUuid(uuid);
        }

        @Override
        boolean canRemoveFromMap(BaseVO obj) {
            Assert.isInstanceOf(ServerVO.class, obj);

            ServerVO server = (ServerVO) obj;

            return Server.Status.Disjoined == server.getStatus();
        }
    }

    private class HostPersistenceHandler extends LocalPersistenceHandler {
        @Override
        synchronized void save(BaseVO obj) {
            Assert.isInstanceOf(HostVO.class, obj);

            HostVO host = (HostVO) obj;
            boolean isLocal = _hostService.isLocalHost(host.getUuid());

            host.setLocal(false);

            HostVO oldHost = (HostVO) _hostService.getByUuid(host.getUuid());
            if (oldHost != null) {
                if (isLocal) {
                    // Commit if remote node changes CBRC flag.
                    if (host.isCbrcEnabled() != oldHost.isCbrcEnabled()) {
                        oldHost.fromRemote(true);
                        oldHost.setCbrcEnabled(host.isCbrcEnabled());
                        _hostService.commit(oldHost);
                        return;
                    }

                    // Don't change the local host otherwise.
                    return;
                }
                host.setId(oldHost.getId());
                host.trackPropertyChange(Base.PROP_STATUS, oldHost.getStatus(), host.getStatus());
            } else {
                if (isLocal) {
                    // The object doesn't exist. Remove from the map.
                    _map.remove(getObjectKey(host));
                    return;
                }
                host.setId(0L);
            }
            _hostService.commit(host);
        }

        @Override
        synchronized void remove(BaseVO obj) {
            Assert.isInstanceOf(HostVO.class, obj);

            HostVO host = (HostVO) obj;
            if (_hostService.isLocalHost(host.getUuid())) {
                // Don't change the local host.
                return;
            }

            HostVO oldHost = (HostVO) _hostService.getByUuid(host.getUuid());
            if (oldHost != null) {
                host.setId(oldHost.getId());
                _hostService.delete(host);
            }
        }

        @Override
        Base getByUuid(String uuid) {
            return _hostService.getByUuid(uuid);
        }
    }

    private class StoragePersistenceHandler extends LocalPersistenceHandler {
        @Override
        synchronized void save(BaseVO obj) {
            Assert.isInstanceOf(StorageVO.class, obj);

            StorageVO storage = (StorageVO) obj;
            boolean isLocal = _storageService.isLocalStorage(storage);

            StorageVO oldStorage = (StorageVO) _storageService.getByUuid(storage.getUuid());
            if (oldStorage != null) {
                if (isLocal) {
                    // Don't change the local storage.
                    return;
                }
                storage.setId(oldStorage.getId());
            } else {
                if (isLocal) {
                    // The object doesn't exist. Remove from the map.
                    _map.remove(getObjectKey(storage));
                    return;
                }
                storage.setId(0L);
            }
            _storageService.commit(storage);
        }

        @Override
        synchronized void remove(BaseVO obj) {
            Assert.isInstanceOf(StorageVO.class, obj);

            StorageVO storage = (StorageVO) obj;
            if (_storageService.isLocalStorage(storage)) {
                // Don't change the local storage.
                return;
            }

            StorageVO oldStorage = (StorageVO) _storageService.getByUuid(storage.getUuid());
            if (oldStorage != null) {
                storage.setId(oldStorage.getId());
                _storageService.delete(storage);
            }
        }

        @Override
        Base getByUuid(String uuid) {
            return _storageService.getByUuid(uuid);
        }
    }

    private class LdapServerPersistenceHandler extends LocalPersistenceHandler {
        @Override
        synchronized void save(BaseVO obj) {
            Assert.isInstanceOf(LdapServerVO.class, obj);

            LdapServerVO ldapServer = (LdapServerVO) obj;

            LdapServerVO oldLdapServer = (LdapServerVO) _ldapServerService.getByUuid(ldapServer.getUuid());
            if (oldLdapServer != null) {
                ldapServer.setId(oldLdapServer.getId());
            } else {
                ldapServer.setId(0L);
            }
            _ldapServerService.commit(ldapServer);
        }

        @Override
        synchronized void remove(BaseVO obj) {
            Assert.isInstanceOf(LdapServerVO.class, obj);

            LdapServerVO ldapServer = (LdapServerVO) obj;

            LdapServerVO oldLdapServer = (LdapServerVO) _ldapServerService.getByUuid(ldapServer.getUuid());
            if (oldLdapServer != null) {
                ldapServer.setId(oldLdapServer.getId());
                _ldapServerService.delete(ldapServer);
            }
        }

        @Override
        Base getByUuid(String uuid) {
            return _ldapServerService.getByUuid(uuid);
        }
    }

    private class VmImagePersistenceHandler extends LocalPersistenceHandler {
        @Override
        synchronized void save(BaseVO obj) {
            Assert.isInstanceOf(VmImageVO.class, obj);

            VmImageVO vmImage = (VmImageVO) obj;

            VmImageVO oldVmImage = (VmImageVO) _vmImageService.getByUuid(vmImage.getUuid());
            if (oldVmImage != null) {
                if (VmImage.Status.Deleting == vmImage.getStatus()
                        && VmImage.Status.Deleting != oldVmImage.getStatus()) {
                    oldVmImage.fromRemote(true);
                    oldVmImage.setStatus(VmImage.Status.Deleting);
                    _vmImageService.commit(oldVmImage);
                    return;
                }

                if (oldVmImage.getUpdateTime() > vmImage.getUpdateTime()) {
                    // Local has later vmImage.
                    return;
                }

                // If local vmImage status is already Deleting, don't be changed by remote.
                if (VmImage.Status.Deleting == oldVmImage.getStatus()) {
                    vmImage.setStatus(oldVmImage.getStatus());
                }

                vmImage.setId(oldVmImage.getId());
                vmImage.trackPropertyChange(Base.PROP_STATUS, oldVmImage.getStatus(), vmImage.getStatus());
            } else {
                if (VmImage.Status.Deleting == vmImage.getStatus()) {
                    // Don't persist if it's being deleted.
                    return;
                }

                vmImage.setId(0L);
            }
            _vmImageService.commit(vmImage);
        }

        @Override
        synchronized void remove(BaseVO obj) {
            Assert.isInstanceOf(VmImageVO.class, obj);

            VmImageVO vmImage = (VmImageVO) obj;

            VmImageVO oldVmImage = (VmImageVO) _vmImageService.getByUuid(vmImage.getUuid());
            if (oldVmImage != null) {
                // Don't directly delete this object from db. Instead change its
                // status to Deleting and trigger local image clean up.
                if (VmImage.Status.Deleting != oldVmImage.getStatus()) {
                    oldVmImage.fromRemote(true);
                    oldVmImage.setStatus(VmImage.Status.Deleting);
                    _vmImageService.commit(oldVmImage);
                }
            }
        }

        @Override
        Base getByUuid(String uuid) {
            return _vmImageService.getByUuid(uuid);
        }

        @Override
        boolean canRemoveFromMap(BaseVO obj) {
            // Only the cluster master can remove it from map.
            return _isMaster;
        }
    }

    private class ApplicationPersistenceHandler extends LocalPersistenceHandler {
        @Override
        synchronized void save(BaseVO obj) {
            Assert.isInstanceOf(ApplicationVO.class, obj);

            ApplicationVO application = (ApplicationVO) obj;

            ApplicationVO oldApplication = (ApplicationVO) _applicationService.getByUuid(application.getUuid());
            if (oldApplication != null) {
                if (oldApplication.getUpdateTime() > application.getUpdateTime()) {
                    // Local has later application.
                    return;
                }

                application.setId(oldApplication.getId());
            } else {
                application.setId(0L);
            }
            _applicationService.commit(application);
        }

        @Override
        synchronized void remove(BaseVO obj) {
            Assert.isInstanceOf(ApplicationVO.class, obj);

            ApplicationVO application = (ApplicationVO) obj;

            ApplicationVO oldApplication = (ApplicationVO) _applicationService.getByUuid(application.getUuid());
            if (oldApplication != null) {
                application.setId(oldApplication.getId());
                _applicationService.delete(application);
            }
        }

        @Override
        Base getByUuid(String uuid) {
            return _applicationService.getByUuid(uuid);
        }
    }

    private class DiskImagePersistenceHandler extends LocalPersistenceHandler {
        @Override
        synchronized void save(BaseVO obj) {
            Assert.isInstanceOf(DiskImageVO.class, obj);

            DiskImageVO diskImage = (DiskImageVO) obj;
            boolean isLocal = _diskImageService.isLocalDiskImage(diskImage);

            DiskImageVO oldDiskImage = (DiskImageVO) _diskImageService.getByUuid(diskImage.getUuid());
            if (oldDiskImage != null) {
                if (isLocal) {
                    // Commit it if this is a requestingCopy or deleting request from another node.
                    if (diskImage.getStatus() != oldDiskImage.getStatus()) {
                        DiskImage.Status newStatus = diskImage.getStatus();
                        switch (newStatus) {
                        case RequestingCopy:
                        case Deleting:
                            oldDiskImage.fromRemote(true);
                            oldDiskImage.setStatus(newStatus);
                            _diskImageService.commit(oldDiskImage);
                            return;
                        }
                    }

                    // Don't change the local diskImage.
                    return;
                }
                diskImage.setId(oldDiskImage.getId());
            } else {
                if (isLocal) {
                    // The object doesn't exist. Remove from the map.
                    _map.remove(getObjectKey(diskImage));
                    return;
                }

                if (DiskImage.Status.Deleting == diskImage.getStatus()) {
                    // Don't persist if it's being deleted.
                    return;
                }

                diskImage.setId(0L);
            }
            _diskImageService.commit(diskImage);
        }

        @Override
        synchronized void remove(BaseVO obj) {
            Assert.isInstanceOf(DiskImageVO.class, obj);

            DiskImageVO diskImage = (DiskImageVO) obj;
            if (_diskImageService.isLocalDiskImage(diskImage)) {
                // Don't change the local diskImage.
                return;
            }

            DiskImageVO oldDiskImage = (DiskImageVO) _diskImageService.getByUuid(diskImage.getUuid());
            if (oldDiskImage != null) {
                diskImage.setId(oldDiskImage.getId());
                _diskImageService.delete(diskImage);
            }
        }

        @Override
        Base getByUuid(String uuid) {
            return _diskImageService.getByUuid(uuid);
        }

        @Override
        boolean canRemoveFromMap(BaseVO obj) {
            Assert.isInstanceOf(DiskImageVO.class, obj);

            DiskImageVO diskImage = (DiskImageVO) obj;

            if (_isMaster && DiskImage.Status.Unknown == diskImage.getStatus()) {
                return true;
            }

            // Only remove local diskImage from the map.
            return _diskImageService.isLocalDiskImage(diskImage);
        }
    }

    private class PoolPersistenceHandler extends LocalPersistenceHandler {
        @Override
        synchronized void save(BaseVO obj) {
            Assert.isInstanceOf(PoolVO.class, obj);

            PoolVO pool = (PoolVO) obj;

            PoolVO oldPool = (PoolVO) _poolService.getByUuid(pool.getUuid());
            if (oldPool != null) {
                if (Pool.Status.Deleting == pool.getStatus()
                        && Pool.Status.Deleting != oldPool.getStatus()) {
                    oldPool.fromRemote(true);
                    oldPool.setStatus(Pool.Status.Deleting);
                    _poolService.commit(oldPool);
                    return;
                }

                if (oldPool.getUpdateTime() > pool.getUpdateTime()) {
                    // Local has later pool.
                    return;
                }

                // If local pool status is already Deleting, don't be changed by remote.
                if (Pool.Status.Deleting == oldPool.getStatus()) {
                    pool.setStatus(oldPool.getStatus());
                }

                // Keep pool sequence number.
                if (oldPool.getSequence() > pool.getSequence()) {
                    pool.setSequence(oldPool.getSequence());
                }

                pool.setId(oldPool.getId());
                pool.trackPropertyChange(Pool.PROP_STATUS, oldPool.getStatus(), pool.getStatus());
                pool.trackPropertyChange(Pool.PROP_BASEIMAGE_GUID, oldPool.getBaseDiskImageGuid(), pool.getBaseDiskImageGuid());
                pool.trackPropertyChange(Pool.PROP_MAXSIZE, oldPool.getMaxSize(), pool.getMaxSize());
                pool.trackPropertyChange(Pool.PROP_MINSIZE, oldPool.getMinSize(), pool.getMinSize());
            } else {
                if (Pool.Status.Deleting == pool.getStatus()) {
                    // Don't persist if it's being deleted.
                    return;
                }

                pool.setId(0L);
                //pool.setBaseDiskImageUuid("");
            }
            _poolService.commit(pool);
        }

        @Override
        synchronized void remove(BaseVO obj) {
            Assert.isInstanceOf(PoolVO.class, obj);

            PoolVO pool = (PoolVO) obj;

            PoolVO oldPool = (PoolVO) _poolService.getByUuid(pool.getUuid());
            if (oldPool != null) {
                // Don't directly delete this object from db. Instead change its
                // status to Deleting and trigger local pool clean up.
                if (Pool.Status.Deleting != oldPool.getStatus()) {
                    oldPool.fromRemote(true);
                    oldPool.setStatus(Pool.Status.Deleting);
                    _poolService.commit(oldPool);
                }
            }
        }

        @Override
        Base getByUuid(String uuid) {
            return _poolService.getByUuid(uuid);
        }

        @Override
        boolean canRemoveFromMap(BaseVO obj) {
            // Only the cluster master can remove it from map.
            return _isMaster;
        }
    }

    private class VmInstancePersistenceHandler extends LocalPersistenceHandler {
        @Override
        void save(BaseVO obj) {
            Assert.isInstanceOf(VmInstanceVO.class, obj);

            VmInstanceVO vmInstance = (VmInstanceVO) obj;
            boolean isLocal = _vmInstanceService.isLocalVmInstance(vmInstance);

            VmInstanceVO oldVmInstance = (VmInstanceVO) _vmInstanceService.getByUuid(vmInstance.getUuid());
            if (oldVmInstance != null) {
                if (isLocal) {
                    // Commit it if this is a deleting, refreshing or user assigning request from another node.
                    if (vmInstance.getStatus() != oldVmInstance.getStatus()) {
                        VmInstance.Status newStatus = vmInstance.getStatus();
                        switch (newStatus) {
                        case Refreshing:
                        case Starting:
                        case Stopping:
                        case Destroying:
                        case Assigning:
                            if (oldVmInstance.getStatus() != newStatus) {
                                oldVmInstance.fromRemote(true);
                                // For the sake of safety, try recompose as much as it can.
                                oldVmInstance.setRecompose(oldVmInstance.isRecompose() || vmInstance.isRecompose());
                                oldVmInstance.setUserUuid(vmInstance.getUserUuid());
                                oldVmInstance.setStatus(newStatus);
                                oldVmInstance.setUpdateTime(System.currentTimeMillis());
                                _vmInstanceService.commit(oldVmInstance);
                            }
                            return;
                        }
                    }

                    if (vmInstance.getLogOffMode() != oldVmInstance.getLogOffMode()
                            && vmInstance.getUpdateTime() > oldVmInstance.getUpdateTime()) {
                        // Remote request to logoff/disconnect.
                        oldVmInstance.fromRemote(true);
                        oldVmInstance.setLogOffMode(vmInstance.getLogOffMode());
                        oldVmInstance.setRecompose(oldVmInstance.isRecompose() || vmInstance.isRecompose());
                        oldVmInstance.setUpdateTime(System.currentTimeMillis());
                        _vmInstanceService.commit(oldVmInstance);
                        return;
                    }

                    // Don't change the local vmInstance otherwise.
                    return;
                }
                vmInstance.setId(oldVmInstance.getId());
                vmInstance.trackPropertyChange(VmInstance.PROP_STATUS, oldVmInstance.getStatus(), vmInstance.getStatus());
            } else {
                if (isLocal && VmInstance.Status.New != vmInstance.getStatus()) {
                    // The object doesn't exist. Remove from the map.
                    _map.remove(getObjectKey(vmInstance));
                    return;
                }

                switch (vmInstance.getStatus()) {
                case Destroying:
                case Destroyed:
                    // Don't persist if it's being deleted.
                    return;
                }

                vmInstance.setId(0L);
            }
            _vmInstanceService.commit(vmInstance);
        }

        @Override
        void remove(BaseVO obj) {
            Assert.isInstanceOf(VmInstanceVO.class, obj);

            VmInstanceVO vmInstance = (VmInstanceVO) obj;
            if (_vmInstanceService.isLocalVmInstance(vmInstance)) {
                // Don't change the local vmInstance.
                return;
            }

            VmInstanceVO oldVmInstance = (VmInstanceVO) _vmInstanceService.getByUuid(vmInstance.getUuid());
            if (oldVmInstance != null) {
                vmInstance.setId(oldVmInstance.getId());
                _vmInstanceService.delete(vmInstance);
            }
        }

        @Override
        Base getByUuid(String uuid) {
            return _vmInstanceService.getByUuid(uuid);
        }

        @Override
        boolean canRemoveFromMap(BaseVO obj) {
            Assert.isInstanceOf(VmInstanceVO.class, obj);

            VmInstanceVO vmInstance = (VmInstanceVO) obj;

            if (_isMaster && VmInstance.Status.Unknown == vmInstance.getStatus()) {
                return true;
            }

            // Only remove local vmInstance from the map.
            return _vmInstanceService.isLocalVmInstance(vmInstance);
        }
    }

    private class AdminUserPersistenceHandler extends LocalPersistenceHandler {
        @Override
        synchronized void save(BaseVO obj) {
            Assert.isInstanceOf(AdminUserVO.class, obj);

            AdminUserVO adminUser = (AdminUserVO) obj;

            AdminUserVO oldAdminUser = (AdminUserVO) _adminUserService.getByUuid(adminUser.getUuid());
            if (oldAdminUser != null) {
                adminUser.setId(oldAdminUser.getId());
            } else {
                // If not cluster master, remove the old entry first
                if (!_isMaster) {
                    oldAdminUser = (AdminUserVO) _adminUserService.getByUsername(adminUser.getUsername());
                    if (oldAdminUser != null) {
                        _adminUserService.delete(oldAdminUser);
                    }
                }
                adminUser.setId(0L);
            }
            _adminUserService.commit(adminUser);
        }

        @Override
        synchronized void remove(BaseVO obj) {
            Assert.isInstanceOf(AdminUserVO.class, obj);

            AdminUserVO adminUser = (AdminUserVO) obj;

            AdminUserVO oldAdminUser = (AdminUserVO) _adminUserService.getByUuid(adminUser.getUuid());
            if (oldAdminUser != null) {
                adminUser.setId(oldAdminUser.getId());
                _adminUserService.delete(adminUser);
            }
        }

        @Override
        Base getByUuid(String uuid) {
            return _adminUserService.getByUuid(uuid);
        }
    }

    private class UserPersistenceHandler extends LocalPersistenceHandler {
        @Override
        synchronized void save(BaseVO obj) {
            Assert.isInstanceOf(UserVO.class, obj);

            UserVO user = (UserVO) obj;

            UserVO oldUser = (UserVO) _userService.getByUuid(user.getUuid());
            if (oldUser != null) {
                user.setId(oldUser.getId());
            } else {
                user.setId(0L);
            }
            _userService.commit(user);
        }

        @Override
        synchronized void remove(BaseVO obj) {
            Assert.isInstanceOf(UserVO.class, obj);

            UserVO user = (UserVO) obj;

            UserVO oldUser = (UserVO) _userService.getByUuid(user.getUuid());
            if (oldUser != null) {
                user.setId(oldUser.getId());
                _userService.delete(user);
            }
        }

        @Override
        Base getByUuid(String uuid) {
            return _userService.getByUuid(uuid);
        }
    }

    private class UserGroupPersistenceHandler extends LocalPersistenceHandler {
        @Override
        synchronized void save(BaseVO obj) {
            Assert.isInstanceOf(UserGroupVO.class, obj);

            UserGroupVO userGroup = (UserGroupVO) obj;

            UserGroupVO oldUserGroup = (UserGroupVO) _userGroupService.getByUuid(userGroup.getUuid());
            if (oldUserGroup != null) {
                userGroup.setId(oldUserGroup.getId());
            } else {
                userGroup.setId(0L);
            }
            _userGroupService.commit(userGroup);
        }

        @Override
        synchronized void remove(BaseVO obj) {
            Assert.isInstanceOf(UserGroupVO.class, obj);

            UserGroupVO userGroup = (UserGroupVO) obj;

            UserGroupVO oldUserGroup = (UserGroupVO) _userGroupService.getByUuid(userGroup.getUuid());
            if (oldUserGroup != null) {
                userGroup.setId(oldUserGroup.getId());
                _userGroupService.delete(userGroup);
            }
        }

        @Override
        Base getByUuid(String uuid) {
            return _userGroupService.getByUuid(uuid);
        }
    }

    private class PoolUserPersistenceHandler extends LocalPersistenceHandler {
        @Override
        void save(BaseVO obj) {
            Assert.isInstanceOf(PoolUserVO.class, obj);

            PoolUserVO poolUser = (PoolUserVO) obj;

            PoolUserVO oldPoolUser = (PoolUserVO) _poolUserService.getByUuid(poolUser.getUuid());
            if (oldPoolUser != null) {
                poolUser.setId(oldPoolUser.getId());
            } else {
                poolUser.setId(0L);
            }
            _poolUserService.commit(poolUser);
        }

        @Override
        void remove(BaseVO obj) {
            Assert.isInstanceOf(PoolUserVO.class, obj);

            PoolUserVO poolUser = (PoolUserVO) obj;

            PoolUserVO oldPoolUser = (PoolUserVO) _poolUserService.getByUuid(poolUser.getUuid());
            if (oldPoolUser != null) {
                poolUser.setId(oldPoolUser.getId());
                _poolUserService.delete(poolUser);
            }
        }

        @Override
        Base getByUuid(String uuid) {
            return _poolUserService.getByUuid(uuid);
        }
    }

    private class UserSessionPersistenceHandler extends LocalPersistenceHandler {
        @Override
        void save(BaseVO obj) {
            Assert.isInstanceOf(UserSessionVO.class, obj);

            UserSessionVO userSession = (UserSessionVO) obj;
            boolean isLocal = _userSessionService.isLocalUserSession(userSession);

            UserSessionVO oldUserSession = (UserSessionVO) _userSessionService.getByUuid(userSession.getUuid());
            if (oldUserSession != null) {
                if (isLocal) {
                    // Don't change the local server.
                    return;
                }
                userSession.setId(oldUserSession.getId());
            } else {
                if (isLocal) {
                    // The object doesn't exist. Remove from the map.
                    _map.remove(getObjectKey(userSession));
                    return;
                }
                userSession.setId(0L);
            }
            _userSessionService.commit(userSession);
        }

        @Override
        void remove(BaseVO obj) {
            Assert.isInstanceOf(UserSessionVO.class, obj);

            UserSessionVO userSession = (UserSessionVO) obj;
            if (_userSessionService.isLocalUserSession(userSession)) {
                // Don't change the local server.
                return;
            }

            UserSessionVO oldUserSession = (UserSessionVO) _userSessionService.getByUuid(userSession.getUuid());
            if (oldUserSession != null) {
                userSession.setId(oldUserSession.getId());
                _userSessionService.delete(userSession);
            }
        }

        @Override
        Base getByUuid(String uuid) {
            return _userSessionService.getByUuid(uuid);
        }
    }

    private class TaskPersistenceHandler extends LocalPersistenceHandler {
        @Override
        synchronized void save(BaseVO obj) {
            Assert.isInstanceOf(TaskVO.class, obj);

            TaskVO task = (TaskVO) obj;
            boolean isLocal = _taskService.isLocalTask(task);

            int retried = 0;

            while (retried <= 1) {
                TaskVO oldTask = (TaskVO) _taskService.getByUuid(task.getUuid());
                if (oldTask != null) {
                    if (isLocal) {
                        // Don't change the local server.
                        return;
                    }
                    task.setId(oldTask.getId());
                } else {
                    if (isLocal) {
                        // The object doesn't exist. Remove from the map.
                        _map.remove(getObjectKey(task));
                        return;
                    }
                    task.setId(0L);
                }
                try {
                    _taskService.commit(task);
                    break; // Succeed, break the loop.
                } catch (Throwable t) {
                    ++retried;
                    continue;
                }
            }
        }

        @Override
        synchronized void remove(BaseVO obj) {
            Assert.isInstanceOf(TaskVO.class, obj);

            TaskVO task = (TaskVO) obj;
            if (_taskService.isLocalTask(task)) {
                // Don't change the local server.
                return;
            }

            TaskVO oldTask = (TaskVO) _taskService.getByUuid(task.getUuid());
            if (oldTask != null) {
                task.setId(oldTask.getId());
                _taskService.delete(task);
            }
        }

        @Override
        Base getByUuid(String uuid) {
            return _taskService.getByUuid(uuid);
        }
    }

    private class HostUsageTrackerPersistenceHandler extends LocalPersistenceHandler {
        @Override
        void save(BaseVO obj) {
            Assert.isInstanceOf(HostUsageTrackerVO.class, obj);

            HostUsageTrackerVO hostUsageTracker = (HostUsageTrackerVO) obj;
            _trackerManager.updateHostUsage(hostUsageTracker.getUuid(), hostUsageTracker);
        }

        @Override
        void remove(BaseVO obj) {
            Assert.isInstanceOf(HostUsageTrackerVO.class, obj);

            HostUsageTrackerVO hostUsageTracker = (HostUsageTrackerVO) obj;
            _trackerManager.removeHostUsage(hostUsageTracker.getUuid());
        }

        @Override
        Base getByUuid(String uuid) {
            return _trackerManager.getHostUsage(uuid);
        }
    }

    private class ProgressTrackerPersistenceHandler extends LocalPersistenceHandler {
        @Override
        void save(BaseVO obj) {
            Assert.isInstanceOf(ProgressTrackerVO.class, obj);

            ProgressTrackerVO progressTracker = (ProgressTrackerVO) obj;
            _trackerManager.updateProgress(progressTracker.getUuid(), progressTracker);
        }

        @Override
        void remove(BaseVO obj) {
            Assert.isInstanceOf(ProgressTrackerVO.class, obj);

            ProgressTrackerVO progressTracker = (ProgressTrackerVO) obj;
            _trackerManager.removeProgress(progressTracker.getUuid());
        }

        @Override
        Base getByUuid(String uuid) {
            return _trackerManager.getProgress(uuid);
        }
    }
}
