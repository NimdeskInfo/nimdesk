/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.cluster;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.rmi.RmiServiceExporter;
import org.springframework.util.Assert;

import com.nimdesk.event.DisjoinClusterEvent;
import com.nimdesk.event.EventHandler;
import com.nimdesk.event.JoinClusterEvent;
import com.nimdesk.service.EventBusService;
import com.google.common.eventbus.AllowConcurrentEvents;
import com.google.common.eventbus.Subscribe;

public class ClusterController implements EventHandler, InitializingBean {
    private static final Logger LOGGER = Logger.getLogger(ClusterController.class);

    private static final int CORE_CLUSTER_POOL_SIZE = 1;
    private static final int MAX_CLUSTER_POOL_SIZE = 200;

    private final ExecutorService _executor = new ThreadPoolExecutor(CORE_CLUSTER_POOL_SIZE,
            MAX_CLUSTER_POOL_SIZE, 300L, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>());

    @Autowired
    private EventBusService _eventBusService;

    @Autowired
    private ClusterReplicator _clusterReplicator;

    @Autowired
    private RemoteService _remoteService;

    private RmiServiceExporter _rmiServiceExporter = null;
    private final Object _rmiServiceExporterLock = new Object();

    public ClusterController() {
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(_eventBusService, "An EventBusService must be set");
        Assert.notNull(_clusterReplicator, "A ClusterReplicator must be set");
        Assert.notNull(_remoteService, "A RemoteService must be set");

        LOGGER.info("Register ClusterController with event bus");
        _eventBusService.register(this);
    }

    @Subscribe
    @AllowConcurrentEvents
    public void handle(final JoinClusterEvent event) {
        _executor.execute(new Runnable() {
            @Override
            public void run() {
                LOGGER.info(String.format("Joining cluster \"%s\"...", event.getClusterName()));

                try {
                    synchronized (_rmiServiceExporterLock) {
                        if (_rmiServiceExporter != null) {
                            _rmiServiceExporter.destroy();
                            _rmiServiceExporter = null;
                        }

                        _rmiServiceExporter = new RmiServiceExporter();
                        _rmiServiceExporter.setServiceName("remote-service");
                        _rmiServiceExporter.setService(_remoteService);
                        _rmiServiceExporter.setServiceInterface(RemoteService.class);
                        _rmiServiceExporter.setRegistryPort(11099);

                        System.setProperty("java.rmi.server.hostname", event.getBindAddress());

                        _rmiServiceExporter.prepare();
                    }

                    try {
                        long clusterCreateTime = Long.parseLong(event.getClusterId());
                    } catch (NumberFormatException e) {
                        LOGGER.warn(String.format("Cluster id format error \"%s\"", event.getClusterId()));
                    }

                    String clusterName = event.getClusterName() + "-" + event.getClusterId();
                    _clusterReplicator.start(clusterName, event.getBindAddress(), event.getInitialHosts());

                    if (event.getCallback() != null) {
                        event.getCallback().onSuccess(null);
                    }
                } catch (Exception e) {
                    LOGGER.error(String.format("Failed to join cluster \"%s\"", event.getClusterName()), e);

                    if (event.getCallback() != null) {
                        event.getCallback().onFailure(e);
                    }
                }
            }
        });
    }

    @Subscribe
    @AllowConcurrentEvents
    public void handle(final DisjoinClusterEvent event) {
        _executor.execute(new Runnable() {
            @Override
            public void run() {
                LOGGER.info("Disjoining cluster...");

                try {
                    synchronized (_rmiServiceExporterLock) {
                        if (_rmiServiceExporter != null) {
                            _rmiServiceExporter.destroy();
                            _rmiServiceExporter = null;
                        }
                    }
                } catch (Exception e) {
                    if (event.getCallback() != null) {
                        event.getCallback().onFailure(e);
                    }
                }

                _clusterReplicator.stop();

                if (event.getCallback() != null) {
                    event.getCallback().onSuccess(null);
                }
            }
        });
    }
}
