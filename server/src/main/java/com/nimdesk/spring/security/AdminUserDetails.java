/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.spring.security;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.Assert;

import com.nimdesk.model.AdminUser;

public class AdminUserDetails implements UserDetails {

    private static final long serialVersionUID = 1L;


    private final AdminUser _adminUser;
    private final Set<GrantedAuthority> _authorities = new HashSet<GrantedAuthority>();

    AdminUserDetails(AdminUser adminUser)
            throws IllegalArgumentException {
        Assert.notNull(adminUser);

        _adminUser = adminUser;
        _authorities.add(new GrantedAuthorityImpl("ROLE_ADMIN"));
    }

    @Override
    public Collection<GrantedAuthority> getAuthorities() {
        return _authorities;
    }

    @Override
    public String getUsername() {
        return _adminUser.getUsername();
    }

    @Override
    public String getPassword() {
        return _adminUser.getPassword();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
