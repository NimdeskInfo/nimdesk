/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.monitor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import com.nimdesk.event.EventHandler;
import com.nimdesk.event.StartHostMonitorEvent;
import com.nimdesk.event.StartVmMonitorEvent;
import com.nimdesk.event.StopVmMonitorEvent;
import com.nimdesk.model.Host;
import com.nimdesk.model.Server;
import com.nimdesk.model.Storage;
import com.nimdesk.model.VmInstance;
import com.nimdesk.monitor.vmware.HostMonitor;
import com.nimdesk.monitor.vmware.MonitorMetric;
import com.nimdesk.rrd.RrdFile;
import com.nimdesk.rrd.RrdSystem;
import com.nimdesk.rrd.RrdTemplate;
import com.nimdesk.service.EventBusService;
import com.nimdesk.service.ObjectPropertyChangeEvent;
import com.nimdesk.service.ObjectPropertyChangeListener;
import com.nimdesk.tracker.TrackerManager;
import com.nimdesk.util.TimeZoneUtils;
import com.nimdesk.util.chart.ChartColorTool;
import com.nimdesk.util.chart.ChartData;
import com.nimdesk.util.chart.ChartLine;
import com.google.common.eventbus.AllowConcurrentEvents;
import com.google.common.eventbus.Subscribe;

public class MonitorControllerImpl implements MonitorController, EventHandler, InitializingBean, ObjectPropertyChangeListener {
    private static final Logger LOGGER = Logger.getLogger(MonitorControllerImpl.class);

    private static final int CORE_MONITOR_POOL_SIZE = 2;
    private static final int MAX_MONITOR_POOL_SIZE = 200;

    private final ExecutorService _executor = new ThreadPoolExecutor(CORE_MONITOR_POOL_SIZE,
            MAX_MONITOR_POOL_SIZE, 300L, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>());

    private final ExecutorService _monitorExecutor = new ThreadPoolExecutor(CORE_MONITOR_POOL_SIZE,
            MAX_MONITOR_POOL_SIZE, 300L, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>());

    @Autowired
    private EventBusService _eventBusService;

    @Autowired
    private Server.Service _serverService;

    @Autowired
    private Storage.Service _storageService;

    @Autowired
    private RrdSystem _rrdSystem;

    @Autowired
    private TrackerManager _trackerManager;

    private HostMonitor _hostMonitor = null;
    private final Object _hostMonitorMutext = new Object();

    public MonitorControllerImpl() {
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(_eventBusService, "An EventBusService must be set");
        Assert.notNull(_serverService, "A Server.Service must be set");
        Assert.notNull(_storageService, "A Storage.Service must be set");
        Assert.notNull(_rrdSystem, "A RrdSystem must be set");
        Assert.notNull(_trackerManager, "A TrackerManager must be set");

        LOGGER.info("Register MonitorController with event bus");
        _eventBusService.register(this);
    }

    @Override
    public void newObject(ObjectPropertyChangeEvent event) {
        Server localServer = _serverService.getLocalServer();
        if (localServer == null) {
            return;
        }

        Host host = (Host) event.getSource();
        if (host.isLocal()) {
            StartHostMonitorEvent startMonitorEvent = new StartHostMonitorEvent(localServer, host, null);
            _eventBusService.post(startMonitorEvent);
        }
    }

    @Override
    public void deleteObject(ObjectPropertyChangeEvent event) {
    }

    @Override
    public void propertyChange(ObjectPropertyChangeEvent event) {
    }

    @Subscribe
    @AllowConcurrentEvents
    public void handle(final StartHostMonitorEvent event) {
        synchronized(_hostMonitorMutext) {
            if (_hostMonitor == null) {
                _hostMonitor = new HostMonitor(_monitorExecutor, event.getHost(), _storageService, _rrdSystem, _trackerManager);
                _hostMonitor.start();
            }
        }

        List<? extends Storage> storages = _storageService.getHostStorages(event.getHost().getUuid());
        if (storages != null) {
            for (Storage storage : storages) {
                _hostMonitor.startStorage(storage);
            }
        }
    }

    @Subscribe
    @AllowConcurrentEvents
    public void handle(final StartVmMonitorEvent event) {
        _executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    synchronized(_hostMonitorMutext) {
                        if (_hostMonitor == null) {
                            throw new Exception("Host monitor has not been started");
                        }
                    }

                    _hostMonitor.startVmMonitor(event.getVmInstance());

                    if (event.getCallback() != null) {
                        event.getCallback().onSuccess(null);
                    }
                } catch (Exception e) {
                    LOGGER.error(String.format("Failed to start monitor vm \"%s\"", event.getVmInstance().getVmRefId()), e);

                    if (event.getCallback() != null) {
                        event.getCallback().onFailure(e);
                    }
                }
            }
        });
    }

    @Subscribe
    @AllowConcurrentEvents
    public void handle(final StopVmMonitorEvent event) {
        _executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    synchronized(_hostMonitorMutext) {
                        if (_hostMonitor == null) {
                            throw new Exception("Host monitor has not been started");
                        }
                    }

                    _hostMonitor.stopVmMonitor(event.getVmInstance());

                    if (event.getCallback() != null) {
                        event.getCallback().onSuccess(null);
                    }
                } catch (Exception e) {
                    LOGGER.error(String.format("Failed to stop monitor vm \"%s\"", event.getVmInstance().getVmRefId()), e);

                    if (event.getCallback() != null) {
                        event.getCallback().onFailure(e);
                    }
                }
            }
        });
    }

    static class ChartAttributes {
        public String title;
        public String yLabel;
        public int base;
        public List<String> metrics;

        public ChartAttributes(String title, String yLabel, int base, List<String> metrics) {
            this.title = title;
            this.yLabel = yLabel;
            this.base = base;
            this.metrics = metrics;
        }
    }

    private static Map<Long, ChartAttributes> _hostStatusMap = new HashMap<Long, ChartAttributes>();
    static {
        _hostStatusMap.put(1L, new ChartAttributes("CPU Usage",
                "Percentage",
                10,
                Arrays.asList(MonitorMetric.CpuUsage.name())));

        _hostStatusMap.put(2L, new ChartAttributes("Memory",
                "KBytes",
                1024,
                Arrays.asList(MonitorMetric.MemoryTotal.name(),
                        MonitorMetric.MemoryConsumed.name(),
                        MonitorMetric.MemoryActive.name())));

        _hostStatusMap.put(3L, new ChartAttributes("Disk IO Byte Rates",
                "KBps",
                1024,
                Arrays.asList(MonitorMetric.DiskReadRate.name(),
                        MonitorMetric.DiskWriteRate.name())));

        _hostStatusMap.put(4L, new ChartAttributes("Disk IOPS",
                "Number per second",
                10,
                Arrays.asList(MonitorMetric.DiskReads.name(),
                        MonitorMetric.DiskWrites.name())));

        _hostStatusMap.put(5L, new ChartAttributes("Disk IO Latency",
                "Millisecond",
                10,
                Arrays.asList(MonitorMetric.DiskReadLatency.name(),
                        MonitorMetric.DiskWriteLatency.name())));

        _hostStatusMap.put(6L, new ChartAttributes("Network Byte Rates",
                "KBps",
                1024,
                Arrays.asList(MonitorMetric.NetDataRateRx.name(),
                        MonitorMetric.NetDataRateTx.name())));

        _hostStatusMap.put(7L, new ChartAttributes("Network Packet Rates",
                "Packets per second",
                10,
                Arrays.asList(MonitorMetric.NetPacketsRx.name(),
                        MonitorMetric.NetPacketsTx.name())));

        _hostStatusMap.put(8L, new ChartAttributes("Datastores",
                "Bytes",
                1024,
                Arrays.asList(MonitorMetric.DatastoreCapacity.name(),
                        MonitorMetric.DatastoreUsed.name())));
    }

    @Override
    public ChartData getHostStatus(Host host, long statusId, String dateRange) {
        ChartAttributes chartAttribs = _hostStatusMap.get(statusId);
        if (chartAttribs == null) {
            LOGGER.error(String.format("Unable to find chart attributes for host \"%s\"", host.getName()));
            return null;
        }

        RrdFile rrdFile = null;
        try {
            rrdFile = _rrdSystem.getRrdFileById(host.getRefId());
        } catch (Exception e) {
            LOGGER.error(String.format("Failed to get RrdFile for host \"%s\"", host.getName()), e);
            return null;
        }
        if (rrdFile == null) {
            LOGGER.error(String.format("Failed to get RrdFile for host \"%s\"", host.getName()));
            return null;
        }

        long[] times = TimeZoneUtils.toEpoch(dateRange, TimeZone.getDefault());

        List<RrdTemplate.DataResult> results = new ArrayList<RrdTemplate.DataResult>();

        RrdTemplate rrdTemplate = new RrdTemplate(rrdFile);
        try {
            rrdTemplate.getData(times[0], times[1], chartAttribs.metrics, results);
        } catch (IOException e) {
            LOGGER.warn(String.format("Unable to get data for host \"%s\"", host.getName()));
            return null;
        }

        if (results.size() == 0) {
            LOGGER.warn(String.format("Empty data for host \"%s\"", host.getName()));
            return null;
        }

        // CPU usage maximum 100%
        return convertToChartData(chartAttribs.title, chartAttribs.yLabel, chartAttribs.base,
                0.0, (statusId == 1L) ? 100. : null, chartAttribs.metrics, times, results);
    }

    @Override
    public ChartData getHostDatastoreStatus(Host host, String datastoreRefId, String dateRange) {
        ChartAttributes chartAttribs = _hostStatusMap.get(8L);
        if (chartAttribs == null) {
            LOGGER.error(String.format("Unable to find chart attributes for datastore \"%s\"", datastoreRefId));
            return null;
        }

        RrdFile rrdFile = null;
        try {
            rrdFile = _rrdSystem.getRrdFileById(datastoreRefId);
        } catch (Exception e) {
            LOGGER.error(String.format("Failed to get RrdFile for datastore \"%s\"", datastoreRefId), e);
            return null;
        }
        if (rrdFile == null) {
            LOGGER.error(String.format("Failed to get RrdFile for datastore \"%s\"", datastoreRefId));
            return null;
        }

        long[] times = TimeZoneUtils.toEpoch(dateRange, TimeZone.getDefault());

        List<RrdTemplate.DataResult> results = new ArrayList<RrdTemplate.DataResult>();

        RrdTemplate rrdTemplate = new RrdTemplate(rrdFile);
        try {
            rrdTemplate.getData(times[0], times[1], chartAttribs.metrics, results);
        } catch (IOException e) {
            LOGGER.warn(String.format("Unable to get data for datastore \"%s\"", datastoreRefId));
            return null;
        }

        if (results.size() == 0) {
            LOGGER.warn(String.format("Empty data for datastore \"%s\"", datastoreRefId));
            return null;
        }

        return convertToChartData(chartAttribs.title, chartAttribs.yLabel, chartAttribs.base,
                0.0, null, chartAttribs.metrics, times, results);
    }

    private ChartData convertToChartData(String title, String yLabel, int base,
            Double min, Double max, List<String> metrics, long[] timeRange,
            List<RrdTemplate.DataResult> results) {
        ChartData chartData = new ChartData();

        chartData.setTitle(title);
        chartData.setYLabel(yLabel);
        chartData.setBase(base);
        if (min != null) {
            chartData.setMin(min);
        }
        if (max != null) {
            chartData.setMax(max);
        }

        chartData.setStartTime(new Date(timeRange[0] * 1000L));
        chartData.setEndTime(new Date(timeRange[1] * 1000L));

        List<Date> timestamps = new ArrayList<Date>();

        for (long timestamp : results.get(0).timestamps) {
            timestamps.add(new Date(timestamp * 1000L));
        }
        chartData.setTimestamps(timestamps);

        ChartColorTool colorTool = new ChartColorTool();

        HashMap<String, ChartLine> metricLineMap = new HashMap<String, ChartLine>();

        for (RrdTemplate.DataResult result : results) {
            String legendName = getLegendName(result.rrdDsName);

            ChartLine line = new ChartLine();
            line.setName(legendName);
            line.setColor(colorTool.getNext());

            List<Double> data = new ArrayList<Double>();
            for (int i = 0; i < result.values.length; i++) {
                data.add(result.values[i]);
            }
            line.setData(data);

            metricLineMap.put(result.rrdDsName, line);
        }

        List<ChartLine> lines = new ArrayList<ChartLine>();
        for (String metric : metrics) {
            ChartLine line = metricLineMap.get(metric);
            if (line != null) {
                lines.add(line);
            }
        }
        chartData.setLines(lines);

        return chartData;
    }

    private String getLegendName(String rrdDsName) {
        MonitorMetric metric;
        try {
            metric = MonitorMetric.valueOf(rrdDsName);
        } catch (Exception e) {
            return "";
        }

        return metric.getDisplayName();
    }

    private static Map<Long, ChartAttributes> _vmStatusMap = new HashMap<Long, ChartAttributes>();
    static {
        _vmStatusMap.put(1L, new ChartAttributes("CPU Usage",
                "Percentage",
                10,
                Arrays.asList(MonitorMetric.CpuUsage.name())));

        _vmStatusMap.put(2L, new ChartAttributes("Memory",
                "KBytes",
                1024,
                Arrays.asList(MonitorMetric.MemoryTotal.name(),
                        MonitorMetric.MemoryConsumed.name(),
                        MonitorMetric.MemoryActive.name())));

        _vmStatusMap.put(3L, new ChartAttributes("Disk IO Byte Rates",
                "KBps",
                1024,
                Arrays.asList(MonitorMetric.DiskReadRate.name(),
                        MonitorMetric.DiskWriteRate.name())));

        _vmStatusMap.put(4L, new ChartAttributes("Disk IOPS",
                "Number per second",
                10,
                Arrays.asList(MonitorMetric.DiskReads.name(),
                        MonitorMetric.DiskWrites.name())));

        _vmStatusMap.put(5L, new ChartAttributes("Disk IO Latency",
                "Millisecond",
                10,
                Arrays.asList(MonitorMetric.DiskReadLatency.name(),
                        MonitorMetric.DiskWriteLatency.name())));

        _vmStatusMap.put(6L, new ChartAttributes("Network Byte Rates",
                "KBps",
                1024,
                Arrays.asList(MonitorMetric.NetDataRateRx.name(),
                        MonitorMetric.NetDataRateTx.name())));

        _vmStatusMap.put(7L, new ChartAttributes("Network Packet Rates",
                "Packets per second",
                10,
                Arrays.asList(MonitorMetric.NetPacketsRx.name(),
                        MonitorMetric.NetPacketsTx.name())));

        _vmStatusMap.put(8L, new ChartAttributes("VM Storage",
                "Bytes",
                1024,
                Arrays.asList(MonitorMetric.VmStorageProvisioned.name(),
                        MonitorMetric.VmStorageUsed.name())));
    }

    @Override
    public ChartData getVmInstanceStatus(VmInstance vmInstance, long statusId, String dateRange) {
        ChartAttributes chartAttribs = _vmStatusMap.get(statusId);
        if (chartAttribs == null) {
            LOGGER.error(String.format("Unable to find chart attributes for vm \"%s\"", vmInstance.getName()));
            return null;
        }

        RrdFile rrdFile = null;
        try {
            rrdFile = _rrdSystem.getRrdFileById(vmInstance.getVmRefId());
        } catch (Exception e) {
            LOGGER.error(String.format("Failed to get RrdFile for vm \"%s\"", vmInstance.getName()), e);
            return null;
        }
        if (rrdFile == null) {
            LOGGER.error(String.format("Failed to get RrdFile for vm \"%s\"", vmInstance.getName()));
            return null;
        }

        long[] times = TimeZoneUtils.toEpoch(dateRange, TimeZone.getDefault());

        List<RrdTemplate.DataResult> results = new ArrayList<RrdTemplate.DataResult>();

        RrdTemplate rrdTemplate = new RrdTemplate(rrdFile);
        try {
            rrdTemplate.getData(times[0], times[1], chartAttribs.metrics, results);
        } catch (IOException e) {
            LOGGER.warn(String.format("Unable to get data for vm \"%s\"", vmInstance.getName()));
            return null;
        }

        if (results.size() == 0) {
            LOGGER.warn(String.format("Empty data for vm \"%s\"", vmInstance.getName()));
            return null;
        }

        // CPU usage maximum 100%
        return convertToChartData(chartAttribs.title, chartAttribs.yLabel, chartAttribs.base,
                0.0, (statusId == 1L) ? 100. : null, chartAttribs.metrics, times, results);
    }
}
