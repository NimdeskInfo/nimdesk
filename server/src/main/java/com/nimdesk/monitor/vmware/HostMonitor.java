/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.monitor.vmware;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ExecutorService;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleTrigger;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.util.Assert;

import com.nimdesk.model.Host;
import com.nimdesk.model.Storage;
import com.nimdesk.model.VmInstance;
import com.nimdesk.rrd.RrdSystem;
import com.nimdesk.tracker.TrackerManager;

public class HostMonitor {
    private static final Logger LOGGER = Logger.getLogger(HostMonitor.class);

    private static Scheduler _scheduler = null;
    static {
        try {
            _scheduler = new StdSchedulerFactory().getScheduler();
            if (!_scheduler.isStarted()) {
                _scheduler.start();
            }
        } catch (SchedulerException e) {
            LOGGER.error("Failed to start scheduler - " + e.getMessage());
        }
    }

    private final ExecutorService _monitorExecutor;
    private final Host _host;
    private final Storage.Service _storageService;
    private final RrdSystem _rrdSystem;
    private final TrackerManager _trackerManager;

    private final String _jobName;

    private final Set<String> _storageJobNames = new HashSet<String>();
    private final Set<String> _vmJobNames = new HashSet<String>();

    private final Object _counterMapLock = new Object();
    private Map<String, Integer> _counterToIdMap;
    private Map<Integer, String> _idToCounterMap;

    private boolean _isStarted = false;

    public HostMonitor(ExecutorService monitorExecutor, Host host,
            Storage.Service storageService, RrdSystem rrdSystem, TrackerManager trackerManager) {
        Assert.notNull(monitorExecutor);
        _monitorExecutor = monitorExecutor;

        Assert.notNull(host);
        _host = host;

        Assert.notNull(storageService);
        _storageService = storageService;

        Assert.notNull(rrdSystem);
        _rrdSystem = rrdSystem;

        Assert.notNull(trackerManager);
        _trackerManager = trackerManager;

        _jobName = "hostMonitor-" + _host.getRefId();
    }

    public synchronized void start() {
        LOGGER.info(String.format("Start monitoring host \"%s\" (%s)", _host.getName(), _host.getRefId()));

        if (_isStarted) {
            return;
        }

        if (_scheduler == null) {
            LOGGER.warn("Scheduler was not initialized correctly. Skip monitor starting");
            return;
        }

        JobDetail jobDetail = new JobDetail(_jobName, Scheduler.DEFAULT_GROUP, MonitorJob.class);
        JobDataMap map = jobDetail.getJobDataMap();
        map.put("monitorTask",
                new HostMonitorTask(this, _host, _trackerManager, _rrdSystem, _scheduler, _jobName, Scheduler.DEFAULT_GROUP));
        map.put("monitorExecutor", _monitorExecutor);

        Trigger trigger = new SimpleTrigger("simpleTrigger" + _host.getRefId(), Scheduler.DEFAULT_GROUP,
                new Date(), null, SimpleTrigger.REPEAT_INDEFINITELY, 60 * 1000);

        try {
            _scheduler.scheduleJob(jobDetail, trigger);
        } catch (SchedulerException e) {
            LOGGER.error("Failed to schedule a job: " + _jobName, e);
            return;
        }

        _isStarted = true;
    }

    public synchronized void stop() {
        LOGGER.info(String.format("Stop monitoring host \"%s\" (%s)", _host.getName(), _host.getRefId()));

        if (_scheduler == null) {
            LOGGER.warn("Scheduler was not initialized correctly. Skip monitor stopping");
        } else {
            try {
                _scheduler.deleteJob(_jobName, Scheduler.DEFAULT_GROUP);
            } catch (SchedulerException e) {
                LOGGER.error("Failed to delete a job: " + _jobName, e);
            }

            for (String jobName : _storageJobNames) {
                try {
                    _scheduler.deleteJob(jobName, Scheduler.DEFAULT_GROUP);
                } catch (SchedulerException e) {
                    LOGGER.error("Failed to delete a job: " + jobName, e);
                }
            }

            _storageJobNames.clear();
        }

        if (_monitorExecutor != null) {
            _monitorExecutor.shutdownNow();
        }

        _isStarted = false;
    }

    public synchronized void startStorage(Storage storage) {
        LOGGER.info(String.format("Start monitoring storage \"%s\" (%s)", storage.getName(), storage.getRefId()));

        if (!_isStarted) {
            LOGGER.warn("The host monitor must be started first");
            return;
        }

        if (_scheduler == null) {
            LOGGER.warn("Scheduler was not initialized correctly. Skip monitor starting");
            return;
        }

        String jobName = "storageMonitor-" + storage.getRefId();

        JobDetail jobDetail = new JobDetail(jobName, Scheduler.DEFAULT_GROUP, MonitorJob.class);
        JobDataMap map = jobDetail.getJobDataMap();
        map.put("monitorTask",
                new DatastoreMonitorTask(this, _host, storage, _storageService, _rrdSystem, _scheduler, jobName, Scheduler.DEFAULT_GROUP));
        map.put("monitorExecutor", _monitorExecutor);

        Trigger trigger = new SimpleTrigger("simpleTrigger" + storage.getRefId(), Scheduler.DEFAULT_GROUP,
                new Date(System.currentTimeMillis() + (new Random()).nextInt(30 * 1000)), null,
                SimpleTrigger.REPEAT_INDEFINITELY, 60 * 1000);

        try {
            _scheduler.scheduleJob(jobDetail, trigger);
        } catch (SchedulerException e) {
            LOGGER.error("Failed to schedule a job: " + jobName, e);
            return;
        }

        _storageJobNames.add(jobName);
    }

    public synchronized void stopStorage(Storage storage) {
        LOGGER.info(String.format("Stop monitoring storage \"%s\" (%s)", storage.getName(), storage.getRefId()));

        if (!_isStarted) {
            LOGGER.warn("The host monitor has already been stopped");
            return;
        }

        if (_scheduler == null) {
            LOGGER.warn("Scheduler was not initialized correctly. Skip monitor stopping");
        } else {
            String jobName = "storageMonitor-" + storage.getRefId();

            try {
                _scheduler.deleteJob(jobName, Scheduler.DEFAULT_GROUP);
            } catch (SchedulerException e) {
                LOGGER.error("Failed to delete a job: " + jobName, e);
                return;
            }

            _storageJobNames.remove(jobName);
        }
    }

    public synchronized void startVmMonitor(VmInstance vmInstance) {
        LOGGER.info(String.format("Start monitoring vmInstance \"%s\" (%s)", vmInstance.getName(), vmInstance.getVmRefId()));

        if (!_isStarted) {
            LOGGER.warn("The host monitor must be started first");
            return;
        }

        if (_scheduler == null) {
            LOGGER.warn("Scheduler was not initialized correctly. Skip monitor starting");
            return;
        }

        String jobName = "vmMonitor-" + vmInstance.getVmRefId();

        JobDetail jobDetail = new JobDetail(jobName, Scheduler.DEFAULT_GROUP, MonitorJob.class);
        JobDataMap map = jobDetail.getJobDataMap();
        map.put("monitorTask",
                new VmMonitorTask(this, _host, vmInstance, _rrdSystem, _scheduler, jobName, Scheduler.DEFAULT_GROUP));
        map.put("monitorExecutor", _monitorExecutor);

        Trigger trigger = new SimpleTrigger("simpleTrigger" + vmInstance.getVmRefId(), Scheduler.DEFAULT_GROUP,
                new Date(System.currentTimeMillis() + (new Random()).nextInt(30 * 1000)), null,
                SimpleTrigger.REPEAT_INDEFINITELY, 60 * 1000);

        try {
            _scheduler.scheduleJob(jobDetail, trigger);
        } catch (SchedulerException e) {
            LOGGER.error("Failed to schedule a job: " + jobName, e);
            return;
        }

        _vmJobNames.add(jobName);
    }

    public synchronized void stopVmMonitor(VmInstance vmInstance) {
        LOGGER.info(String.format("Stop monitoring vmInstance \"%s\" (%s)", vmInstance.getName(), vmInstance.getVmRefId()));

        if (!_isStarted) {
            LOGGER.warn("The host monitor has already been stopped");
            return;
        }

        if (_scheduler == null) {
            LOGGER.warn("Scheduler was not initialized correctly. Skip monitor stopping");
        } else {
            String jobName = "vmMonitor-" + vmInstance.getVmRefId();
            if (!_vmJobNames.contains(jobName)) {
                return;
            }

            try {
                _scheduler.deleteJob(jobName, Scheduler.DEFAULT_GROUP);
            } catch (SchedulerException e) {
                LOGGER.error("Failed to delete a job: " + jobName, e);
                return;
            }

            _vmJobNames.remove(jobName);
        }
    }

    public boolean updateIdCounterMaps(UpdateCounterMapsCallback callback) {
        synchronized(_counterMapLock) {
            if (_counterToIdMap != null && _idToCounterMap != null) {
                return true;
            }

            Map<String, Integer> counterToIdMap = new HashMap<String, Integer>();
            Map<Integer, String> idToCounterMap = new HashMap<Integer, String>();
            if (callback.update(counterToIdMap, idToCounterMap)) {
                _counterToIdMap = counterToIdMap;
                _idToCounterMap = idToCounterMap;
                return true;
            }

            return false;
        }
    }

    public Map<String, Integer> getCounterToIdMap() {
        return _counterToIdMap;
    }

    public Map<Integer, String> getIdToCounterMap() {
        return _idToCounterMap;
    }

    interface UpdateCounterMapsCallback {
        boolean update(Map<String, Integer> counterToIdMap, Map<Integer, String> idToCounterMap);
    }

    public static class MonitorJob implements Job {

        @Override
        public void execute(JobExecutionContext context) throws JobExecutionException {
            JobDataMap map = context.getMergedJobDataMap();
            AbstractMonitorTask monitorTask = (AbstractMonitorTask) map.get("monitorTask");
            ExecutorService monitorExecutor = (ExecutorService) map.get("monitorExecutor");

            Assert.notNull(monitorTask);
            Assert.notNull(monitorExecutor);

            try {
                context.getScheduler().pauseJob(context.getJobDetail().getName(), context.getJobDetail().getGroup());
            } catch (SchedulerException e) {
                LOGGER.error("Failed to pause job: " + context.getJobDetail().getName(), e);
            }

            try {
                monitorExecutor.execute(monitorTask);
            } catch (Exception e) {
                LOGGER.error("Failed to execute job: " + context.getJobDetail().getName(), e);

                try {
                    context.getScheduler().resumeJob(context.getJobDetail().getName(),
                            context.getJobDetail().getGroup());
                } catch (SchedulerException e1) {
                    LOGGER.error("Failed to resume job: " + context.getJobDetail().getName(), e1);
                }
            }
        }
    }
}
