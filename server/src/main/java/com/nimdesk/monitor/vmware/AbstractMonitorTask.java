/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.monitor.vmware;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.rrd4j.DsType;
import org.rrd4j.core.RrdDef;
import org.rrd4j.core.Sample;
import org.springframework.util.Assert;

import com.nimdesk.hypervisor.vmware.VmwareContext;
import com.nimdesk.model.Host;
import com.nimdesk.monitor.vmware.HostMonitor.UpdateCounterMapsCallback;
import com.nimdesk.rrd.RrdFile;
import com.nimdesk.rrd.RrdFile.AddRrdDatasourceCallback;
import com.nimdesk.rrd.RrdFile.UpdateDataCallback;
import com.nimdesk.rrd.RrdSystem;
import com.vmware.vim25.ArrayOfPerfCounterInfo;
import com.vmware.vim25.DynamicProperty;
import com.vmware.vim25.ManagedObjectReference;
import com.vmware.vim25.ObjectContent;
import com.vmware.vim25.ObjectSpec;
import com.vmware.vim25.PerfCounterInfo;
import com.vmware.vim25.PerfEntityMetricBase;
import com.vmware.vim25.PerfEntityMetricCSV;
import com.vmware.vim25.PerfMetricId;
import com.vmware.vim25.PerfMetricSeriesCSV;
import com.vmware.vim25.PerfProviderSummary;
import com.vmware.vim25.PerfQuerySpec;
import com.vmware.vim25.PropertyFilterSpec;
import com.vmware.vim25.PropertySpec;
import com.vmware.vim25.mo.ManagedEntity;
import com.vmware.vim25.mo.PerformanceManager;
import com.vmware.vim25.mo.PropertyCollector;
import com.vmware.vim25.mo.ServiceInstance;
import com.vmware.vim25.mo.util.MorUtil;

abstract class AbstractMonitorTask implements Runnable {
    private static final Logger LOGGER = Logger.getLogger(AbstractMonitorTask.class);

    protected final RrdSystem _rrdSystem;

    protected final HostMonitor _hostMonitor;
    protected final Host _host;

    private final Scheduler _scheduler;
    private final String _jobName;
    private final String _jobGroup;

    private VmwareContext _context = null;
    private ServiceInstance _serviceInstance = null;
    private PropertyCollector _propCollector;
    private PerformanceManager _perfMgr;

    private static final Map<String, MonitorMetric> _counterMetricMap = new HashMap<String, MonitorMetric>();
    static {
        for (MonitorMetric metric : MonitorMetric.values()) {
            _counterMetricMap.put(metric.getCounter(), metric);
        }
    }

    public AbstractMonitorTask(HostMonitor hostMonitor, Host host, RrdSystem rrdSystem,
            Scheduler scheduler, String jobName, String jobGroup) {
        _hostMonitor = hostMonitor;
        Assert.notNull(_hostMonitor, "A HostMonirot must be set");

        _host = host;
        Assert.notNull(_host, "A Host must be set");

        _rrdSystem = rrdSystem;
        Assert.notNull(_rrdSystem, "A RrdSystem must be set");

        _scheduler = scheduler;
        _jobName = jobName;
        _jobGroup = jobGroup;
    }

    protected VmwareContext getVmwareContext() {
        return _context;
    }

    protected abstract String getRrdId();

    protected abstract ManagedObjectReference getMor();

    protected abstract MonitorMetric[] getMetrics();

    protected Map<MonitorMetric, MultiInstanceValue> getMonitorResults() {
        _propCollector = _serviceInstance.getPropertyCollector();
        _perfMgr = _serviceInstance.getPerformanceManager();

        if (!_hostMonitor.updateIdCounterMaps(new UpdateCounterMapsCallback() {
            @Override
            public boolean update(Map<String, Integer> counterToIdMap, Map<Integer, String> idToCounterMap) {
                return buildPerfCounterMaps(counterToIdMap, idToCounterMap);
            }
        })) {
            LOGGER.warn("Failed to update id counter maps for host - " + _host.getName());
            return null;
        }
        Map<String, Integer> counterToIdMap = _hostMonitor.getCounterToIdMap();
        Map<Integer, String> idToCounterMap = _hostMonitor.getIdToCounterMap();

        Map<MonitorMetric, MultiInstanceValue> results = new HashMap<MonitorMetric, MultiInstanceValue>();

        getPerfStats(buildPerfQuerySpecs(getMor(), counterToIdMap, getMetrics()), idToCounterMap, results);

        return results;
    }

    class InstanceValue {
        public String instanceName;
        public double value;

        public InstanceValue(String instanceName, double value) {
            this.instanceName = instanceName;
            this.value = value;
        }
    }

    class MultiInstanceValue {
        public List<InstanceValue> values = new ArrayList<InstanceValue>();

        public void addValue(String instanceName, double value) {
            values.add(new InstanceValue(instanceName, value));
        }
    }

    @Override
    public void run() {
        try {
            if (!connect()) {
                return;
            }

            _serviceInstance = _context.getServiceInstance();
            if (_serviceInstance == null) {
                return;
            }

            Map<MonitorMetric, MultiInstanceValue> results = getMonitorResults();

            if (results != null) {
                addToRrd(results);
            }
        } catch (Exception e) {
            LOGGER.error(String.format("Failed to get monitor results for job: %s", _jobName), e);
        } finally {
            if (_context != null) {
                try {
                    _context.disconnect();
                } catch (Throwable t) {
                    // Ignore error.
                }
            }

            try {
                _scheduler.resumeJob(_jobName, _jobGroup);
            } catch (SchedulerException e) {
                LOGGER.error("Failed to resume job: " + _jobName, e);
            }
        }
    }

    private boolean connect() {
        try {
            if (_context != null) {
                _context.disconnect();
            }

            _context = VmwareContext.connect(_host.getAddress(), 0,
                    _host.getUsername(), _host.getPassword());

            return _context != null;
        } catch (MalformedURLException e) {
            LOGGER.error(String.format("Wrong address for host: %s - %s",
                    _host.getAddress(), e.getMessage()));
            // We can't do anything about this now.
            return false;
        } catch (Exception e) {
            LOGGER.warn(String.format("Unable to connect to server \"%s\": %s",
                    _host.getAddress(), e.getMessage()));
            return false;
        }
    }

    private boolean buildPerfCounterMaps(Map<String, Integer> counterToIdMap, Map<Integer, String> idToCounterMap) {
        Assert.notNull(counterToIdMap);
        Assert.notNull(idToCounterMap);

        PerfCounterInfo[] perfCounters = getPerfCounters();
        if (perfCounters == null) {
            return false;
        }

        for (PerfCounterInfo perfCounter : perfCounters) {
            String fullCounter = perfCounter.getGroupInfo().getKey() + "." + perfCounter.getNameInfo().getKey() + "." + perfCounter.getRollupType().name();
            counterToIdMap.put(fullCounter, new Integer(perfCounter.getKey()));
            idToCounterMap.put(new Integer(perfCounter.getKey()), fullCounter);
        }

        return true;
    }

    @SuppressWarnings("unused")
    private PerfMetricId[] getAvailablePerfMetricIds(ManagedObjectReference mor, Integer intervalId) {
        try {
            ManagedEntity me = MorUtil.createExactManagedEntity(_serviceInstance.getServerConnection(), mor);
            return _perfMgr.queryAvailablePerfMetric(me, null, null, intervalId);
        } catch (Exception e) {
            LOGGER.error("Failed to queryAvailablePerfMetric", e);
            return null;
        }
    }

    @SuppressWarnings("deprecation")
    private PerfCounterInfo[] getPerfCounters() {
        // Create Property Spec
        PropertySpec propertySpec = new PropertySpec();
        propertySpec.setAll(Boolean.FALSE);
        propertySpec.setPathSet(new String[] { "perfCounter" });
        propertySpec.setType("PerformanceManager");
        PropertySpec[] propertySpecs = new PropertySpec[] { propertySpec };

        // Now create Object Spec
        ObjectSpec objectSpec = new ObjectSpec();
        objectSpec.setObj(_perfMgr.getMOR());
        ObjectSpec[] objectSpecs = new ObjectSpec[] { objectSpec };

        // Create PropertyFilterSpec using the PropertySpec and ObjectPec created above.
        PropertyFilterSpec propertyFilterSpec = new PropertyFilterSpec();
        propertyFilterSpec.setPropSet(propertySpecs);
        propertyFilterSpec.setObjectSet(objectSpecs);

        PropertyFilterSpec[] propertyFilterSpecs = new PropertyFilterSpec[] { propertyFilterSpec };

        ObjectContent[] oCont = null;
        try {
            oCont = _propCollector.retrieveProperties(propertyFilterSpecs);
        } catch (Exception e) {
            LOGGER.error("Failed to getPerfCounters", e);
            return null;
        }

        if (oCont != null) {
            for (ObjectContent oc : oCont) {
                DynamicProperty[] dps = oc.getPropSet();
                if (dps != null && dps.length > 0) {
                    PerfCounterInfo[] pciArr = ((ArrayOfPerfCounterInfo) dps[0].getVal()).getPerfCounterInfo();
                    return pciArr;
                }
            }
        }

        return null;
    }

    private PerfQuerySpec[] buildPerfQuerySpecs(ManagedObjectReference mor,
            Map<String, Integer> perfCounterIdMap, MonitorMetric[] metrics) {
        PerfProviderSummary perfProviderSum = getPerfProviderSummary(mor);
        Calendar startTime = null;
        Calendar endTime = null;
        Integer refreshRate = null;

        if (perfProviderSum != null && perfProviderSum.isCurrentSupported()) {
            refreshRate = perfProviderSum.getRefreshRate();
        } else {
            // Default to retreive 5 minute sample
            //refreshRate = new Integer(300);
            Calendar current;
            try {
                current = _serviceInstance.currentTime();
            } catch (Exception e) {
                current = Calendar.getInstance();
            }
            endTime = (Calendar) current.clone();
            endTime.add(Calendar.MINUTE, -4); // The last sample may not be complete
            startTime = (Calendar) current.clone();
            startTime.add(Calendar.HOUR, -1);
        }

        ArrayList<PerfQuerySpec> perfQuerySpecList = new ArrayList<PerfQuerySpec>();
        for (MonitorMetric metric : metrics) {
            Integer counterId = perfCounterIdMap.get(metric.getCounter());
            if (counterId == null) {
                continue;
            }
            perfQuerySpecList.add(buildPerfQuerySpec(mor, counterId, startTime, endTime, refreshRate/*perfProviderSum.getRefreshRate()*/, null));
        }

        return perfQuerySpecList.toArray(new PerfQuerySpec[] {});
    }

    private static PerfQuerySpec buildPerfQuerySpec(ManagedObjectReference mor, int counterId, Calendar startTime, Calendar endTime, Integer refreshRate, PerfMetricId[] perfMetricIdArr) {
        PerfQuerySpec pqSpec = new PerfQuerySpec();
        //You can obtain this from the PerfProviderSummary.
        //By default 20 sec interval exists.
        //When connecting to ESX host directly, interval value 0 can be used.
        //But the value retrieved will be the same in that 20 second window.
        pqSpec.setIntervalId(refreshRate);
        pqSpec.setMaxSample(new Integer(1));

        if (startTime != null) {
            pqSpec.setStartTime(startTime);
        }
        if (endTime != null) {
            pqSpec.setEndTime(endTime);
        }

        if (perfMetricIdArr != null) {
            pqSpec.setMetricId(perfMetricIdArr);
        } else {
            PerfMetricId pmid = new PerfMetricId();
            pmid.setInstance("*");
            pmid.setCounterId(counterId);
            pqSpec.setMetricId(new PerfMetricId[] { pmid });
        }

        pqSpec.setEntity(mor);
        pqSpec.setFormat("csv");

        return pqSpec;
    }

    private void getPerfStats(PerfQuerySpec[] pqsArr, Map<Integer, String> idToCounterMap, Map<MonitorMetric, MultiInstanceValue> results) {
        PerfEntityMetricBase[] perfMetrics = getPerfStats(pqsArr);
        if (perfMetrics != null) {
            for (PerfEntityMetricBase perfMetric : perfMetrics) {
                PerfMetricSeriesCSV[] vals = ((PerfEntityMetricCSV) perfMetric).getValue();

                if (vals != null && vals.length > 0) {
                    int counterId = vals[0].getId().getCounterId();

                    String counterName = idToCounterMap.get(counterId);
                    if (counterName == null ) {
                        continue;
                    }

                    if (results.containsKey(counterName)) {
                        continue;
                    }

                    MonitorMetric metric = _counterMetricMap.get(counterName);
                    if (metric == null) {
                        LOGGER.warn("Unable to find metric for counter: " + counterName);
                        continue;
                    }

                    MultiInstanceValue multiValue = new MultiInstanceValue();
                    String unitScale = metric.getUnitScale();

                    for (int idx = 0; idx < vals.length; idx++) {
                        String[] values = StringUtils.split(vals[idx].getValue(), ",");
                        if (values != null && values.length > 0) {
                            try {
                                Double value = Double.valueOf(values[values.length - 1]);
                                if (StringUtils.isNotEmpty(unitScale)) {
                                    Double factor = Double.valueOf(unitScale.substring(1));

                                    if (unitScale.charAt(0) == '/') {
                                        value /= factor;
                                    } else if (unitScale.charAt(0) == '*') {
                                        value *= factor;
                                    }
                                }

                                multiValue.addValue(vals[idx].getId().getInstance(), value);
                            } catch (Exception e) {
                                // Ignore error
                            }
                        }
                    }

                    results.put(metric, multiValue);
                }
            }
        }
    }

    private PerfEntityMetricBase[] getPerfStats(PerfQuerySpec[] pqsArr) {
        try {
            return _perfMgr.queryPerf(pqsArr);
        } catch (Exception e) {
            LOGGER.error("Failed to getPerfStats", e);
        }

        return null;
    }

    private PerfProviderSummary getPerfProviderSummary(ManagedObjectReference mor) {
        try {
            ManagedEntity me = MorUtil.createExactManagedEntity(_serviceInstance.getServerConnection(), mor);
            return _perfMgr.queryPerfProviderSummary(me);
        } catch (Exception e) {
            LOGGER.error("Failed to getPerfProviderSummary", e);
        }

        return null;
    }

    private void addToRrd(final Map<MonitorMetric, MultiInstanceValue> results) {
        if (results.size() == 0) {
            LOGGER.warn("Unable to find any performance counter values for host - " + _host.getName());
            return;
        }

        RrdFile rrdFile = null;
        try {
            rrdFile = _rrdSystem.getRrdFileById(getRrdId());
        } catch (Exception e) {
            LOGGER.error("Failed to get RrdFile for host - " + _host.getName(), e);
            return;
        }
        if (rrdFile == null) {
            LOGGER.error("Failed to get RrdFile for host - " + _host.getName());
            return;
        }

        if (!rrdFile.getFile().exists()) {
            try {
                rrdFile.create(System.currentTimeMillis(), new AddRrdDatasourceCallback() {
                    @Override
                    public void addDatasources(RrdDef rrdDef) {
                        for (MonitorMetric metric : getMetrics()) {
                            String rrdDsName = metric.name();
                            rrdDef.addDatasource(rrdDsName, DsType.GAUGE, RrdFile.DEFAULT_HEARTBEAT, Double.NaN, Double.NaN);
                        }
                    }
                });
            } catch (Exception e) {
                LOGGER.error("Failed to refresh rrd file for host - " + _host.getName(), e);
                return;
            }
        }

        try {
            rrdFile.updateData(System.currentTimeMillis(), new UpdateDataCallback() {
                @Override
                public void updateData(Sample sample) {
                    for (Map.Entry<MonitorMetric, MultiInstanceValue> entry : results.entrySet()) {
                        MonitorMetric metric = entry.getKey();
                        for (InstanceValue value : entry.getValue().values) {
                            if (!StringUtils.isEmpty(value.instanceName)) {
                                continue;
                            }

                            String rrdDsName = metric.name();

                            try {
                                sample.setValue(rrdDsName, value.value);
                            } catch (Exception e) {
                                try {
                                    sample.setValue(rrdDsName, Double.NaN);
                                } catch (Exception e1) {
                                    LOGGER.error("Failed to update rrd data with ds: " + rrdDsName + " for host - " + _host.getName(), e);
                                }
                            }
                        }
                    }
                }
            });
        } catch (Exception e) {
            LOGGER.error("Failed to update rrd data for host - " + _host.getName(), e);
            return;
        }
    }
}
