/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.monitor.vmware;

public enum MonitorMetric {
    Uptime("sys.uptime.latest", "Uptime", "/86400"), // host, vm (day)

    CpuUsage("cpu.usage.average", "cpu", "/100"), // host,vm (percent)
    CpuUsageMHz("cpu.usagemhz.average", "cpu", ""), // host,vm (megaHertz)

    MemoryTotal("mem.granted.average", "Total Memory", ""), // host,vm (kiloBytes)
    MemoryConsumed("mem.consumed.average", "Consumed Memory", ""), // host,vm (kiloBytes)
    MemoryActive("mem.active.average", "Active Memory", ""), // host,vm (kiloBytes)

    DiskReads("disk.numberRead.summation", "Disk Read Number", ""), // host,vm (number)
    DiskWrites("disk.numberWrite.summation", "Disk Write Number", ""), // host,vm (number)
    DiskReadRate("disk.read.average", "Disk Read Byte Rate", ""), // host,vm (kiloBytesPerSecond)
    DiskWriteRate("disk.write.average", "Disk Write Byte Rate", ""), // host,vm (kiloBytesPerSecond)
    DiskReadLatency("disk.totalReadLatency.average", "Disk Read Latency", ""), // host,vm (millisecond)
    DiskWriteLatency("disk.totalWriteLatency.average", "Disk Write Latency", ""), // host,vm (millisecond)

    NetDataRateRx("net.received.average", "Network In Byte Rate", ""), // host, vm (kiloBytesPerSecond)
    NetDataRateTx("net.transmitted.average", "Network Out Byte Rate", ""), // host, vm (kiloBytesPerSecond)
    NetPacketsRx("net.packetsRx.summation", "Network In Packet", ""), // host,vm (number)
    NetPacketsTx("net.packetsTx.summation", "Network Out Packet", ""), // host, vm (number)

    VmStorageProvisioned("disk.provisioned.latest", "VM Storage Provisioned", ""), // vm (kiloBytes)
    VmStorageUsed("disk.used.latest", "VM Used Storage", ""), // vm (kiloBytes)
    VmStorageUnshared("disk.unshared.latest", "VM Unshared Storage", ""), // vm (kiloBytes)

    DatastoreCapacity("disk.capacity.latest", "Storage Capacity", ""), // datastore (kiloBytes)
    DatastoreUsed("disk.used.latest", "Storage Used Space", ""); // datastore (kiloBytes)

    private final String _counter;
    private final String _displayName;
    private final String _unitScale;

    MonitorMetric(String counter, String displayName, String unitScale) {
        _counter = counter;
        _displayName = displayName;
        _unitScale = unitScale;
    }

    public String getCounter() {
        return _counter;
    }

    public String getDisplayName() {
        return _displayName;
    }

    public String getUnitScale() {
        return _unitScale;
    }
}
