/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.monitor.vmware;

import java.util.Map;

import org.apache.log4j.Logger;
import org.quartz.Scheduler;

import com.nimdesk.hypervisor.vmware.mo.DatastoreMO;
import com.nimdesk.model.Host;
import com.nimdesk.model.Storage;
import com.nimdesk.rrd.RrdSystem;
import com.vmware.vim25.DatastoreSummary;
import com.vmware.vim25.ManagedObjectReference;

public class DatastoreMonitorTask extends AbstractMonitorTask {
    private static final Logger LOGGER = Logger.getLogger(DatastoreMonitorTask.class);

    private static final MonitorMetric[] DATASTORE_METRICS = new MonitorMetric[] {
        MonitorMetric.DatastoreCapacity,
        MonitorMetric.DatastoreUsed
    };

    private final Storage _storage;
    private final ManagedObjectReference _datastoreMor;
    private final Storage.Service _storageService;

    public DatastoreMonitorTask(HostMonitor hostMonitor, Host host, Storage storage, Storage.Service storageService, RrdSystem rrdSystem,
            Scheduler scheduler, String jobName, String jobGroup) {
        super(hostMonitor, host, rrdSystem, scheduler, jobName, jobGroup);

        _storage = storage;

        _datastoreMor = new ManagedObjectReference();
        _datastoreMor.set_value(_storage.getRefId());
        _datastoreMor.setType("Datastore");

        _storageService = storageService;
    }

    @Override
    protected String getRrdId() {
        return _storage.getRefId();
    }

    @Override
    protected ManagedObjectReference getMor() {
        return _datastoreMor;
    }

    @Override
    protected MonitorMetric[] getMetrics() {
        return DATASTORE_METRICS;
    }

    @Override
    protected Map<MonitorMetric, MultiInstanceValue> getMonitorResults() {
        Map<MonitorMetric, MultiInstanceValue> results = null;//new HashMap<MonitorMetric, MultiInstanceValue>();

        DatastoreMO datastore = new DatastoreMO(getVmwareContext(), _storage.getRefId(), _storage.getName());
        try {
            datastore.getDatastore().refreshDatastore();
        } catch (Exception e) {
            LOGGER.warn(String.format("Failed to refreshDatastore for datastore \"%s\"", _storage.getName()), e);
        }

        DatastoreSummary datastoreSummary = datastore.getDatastore().getSummary();
        if (datastoreSummary != null) {
            //MultiInstanceValue value = new MultiInstanceValue();
            long capacityInBytes = datastoreSummary.getCapacity();
            //value.addValue("", capacityInBytes);
            //results.put(MonitorMetric.DatastoreCapacity, value);

            //value = new MultiInstanceValue();
            long freeSpaceInBytes = datastoreSummary.getFreeSpace();
            //long usedInBytes = capacityInBytes - freeSpaceInBytes;
            //value.addValue("", usedInBytes);
            //results.put(MonitorMetric.DatastoreUsed, value);

            _storageService.updateStorageSizes(_storage, capacityInBytes, freeSpaceInBytes);
        }

        return results;
    }
}
