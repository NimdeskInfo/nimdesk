/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.monitor.vmware;

import java.util.Map;

import org.quartz.Scheduler;

import com.nimdesk.hypervisor.vmware.mo.HostMO;
import com.nimdesk.model.Host;
import com.nimdesk.rrd.RrdSystem;
import com.nimdesk.tracker.TrackerManager;
import com.vmware.vim25.HostListSummary;
import com.vmware.vim25.HostListSummaryQuickStats;
import com.vmware.vim25.ManagedObjectReference;

class HostMonitorTask extends AbstractMonitorTask {
    //private static final Logger LOGGER = Logger.getLogger(HostMonitorTask.class);

    private static final MonitorMetric[] HOST_METRICS = new MonitorMetric[] {
        MonitorMetric.Uptime,
        MonitorMetric.CpuUsage, MonitorMetric.CpuUsageMHz,
        MonitorMetric.MemoryTotal, MonitorMetric.MemoryConsumed, MonitorMetric.MemoryActive,
        MonitorMetric.DiskReads, MonitorMetric.DiskWrites,
        MonitorMetric.DiskReadRate, MonitorMetric.DiskWriteRate,
        MonitorMetric.DiskReadLatency, MonitorMetric.DiskWriteLatency,
        MonitorMetric.NetDataRateRx, MonitorMetric.NetDataRateTx,
        MonitorMetric.NetPacketsRx, MonitorMetric.NetPacketsTx
    };

    private final ManagedObjectReference _hostMor;
    private final TrackerManager _trackerManager;

    public HostMonitorTask(HostMonitor hostMonitor, Host host, TrackerManager trackerManager, RrdSystem rrdSystem,
            Scheduler scheduler, String jobName, String jobGroup) {
        super(hostMonitor, host, rrdSystem, scheduler, jobName, jobGroup);

        _hostMor = new ManagedObjectReference();
        _hostMor.set_value(host.getRefId());
        _hostMor.setType("HostSystem");

        _trackerManager = trackerManager;
    }

    @Override
    protected String getRrdId() {
        return _host.getRefId();
    }

    @Override
    protected ManagedObjectReference getMor() {
        return _hostMor;
    }

    @Override
    protected MonitorMetric[] getMetrics() {
        return HOST_METRICS;
    }

    @Override
    protected Map<MonitorMetric, MultiInstanceValue> getMonitorResults() {
        Map<MonitorMetric, MultiInstanceValue> results = null;//super.getMonitorResults();

        HostMO host = new HostMO(getVmwareContext(), _host.getRefId());
        HostListSummary hostSummary = host.getSummary();
        HostListSummaryQuickStats hostQuickStats;
        if (hostSummary != null
                && (hostQuickStats = hostSummary.getQuickStats()) != null) {
            _trackerManager.updateHostUsage(_host.getUuid(),
                    hostQuickStats.getOverallCpuUsage(), hostQuickStats.getOverallMemoryUsage());
        }

        return results;
    }
}
