/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.monitor.vmware;

import org.quartz.Scheduler;

import com.nimdesk.model.Host;
import com.nimdesk.model.VmInstance;
import com.nimdesk.rrd.RrdSystem;
import com.vmware.vim25.ManagedObjectReference;

public class VmMonitorTask extends AbstractMonitorTask {
    //private static final Logger LOGGER = Logger.getLogger(VmMonitorTask.class);

    private static final MonitorMetric[] VM_METRICS = new MonitorMetric[] {
        MonitorMetric.Uptime,
        MonitorMetric.CpuUsage, MonitorMetric.CpuUsageMHz,
        MonitorMetric.MemoryTotal, MonitorMetric.MemoryConsumed, MonitorMetric.MemoryActive,
        MonitorMetric.DiskReads, MonitorMetric.DiskWrites,
        MonitorMetric.DiskReadRate, MonitorMetric.DiskWriteRate,
        MonitorMetric.DiskReadLatency, MonitorMetric.DiskWriteLatency,
        MonitorMetric.NetDataRateRx, MonitorMetric.NetDataRateTx,
        MonitorMetric.NetPacketsRx, MonitorMetric.NetPacketsTx,
        MonitorMetric.VmStorageProvisioned, MonitorMetric.VmStorageUsed, MonitorMetric.VmStorageUnshared
    };

    private final VmInstance _vmInstance;
    private final ManagedObjectReference _vmMor;

    public VmMonitorTask(HostMonitor hostMonitor, Host host, VmInstance vmInstance, RrdSystem rrdSystem,
            Scheduler scheduler, String jobName, String jobGroup) {
        super(hostMonitor, host, rrdSystem, scheduler, jobName, jobGroup);

        _vmInstance = vmInstance;

        _vmMor = new ManagedObjectReference();
        _vmMor.set_value(vmInstance.getVmRefId());
        _vmMor.setType("VirtualMachine");
    }

    @Override
    protected String getRrdId() {
        return _vmInstance.getVmRefId();
    }

    @Override
    protected ManagedObjectReference getMor() {
        return _vmMor;
    }

    @Override
    protected MonitorMetric[] getMetrics() {
        return VM_METRICS;
    }
}
