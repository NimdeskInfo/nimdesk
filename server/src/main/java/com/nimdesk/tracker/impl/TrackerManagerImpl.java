/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.tracker.impl;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.nimdesk.tracker.HostUsageTracker;
import com.nimdesk.tracker.HostUsageTrackerVO;
import com.nimdesk.tracker.ProgressTracker;
import com.nimdesk.tracker.ProgressTrackerVO;
import com.nimdesk.tracker.TrackerManager;

public class TrackerManagerImpl implements TrackerManager {
    private static final Logger LOGGER = Logger.getLogger(TrackerManagerImpl.class);

    private static final Object PRESENT = new Object();

    /*
     * For ProgressTracker
     */

    private final Map<ProgressChangeListener, Object> _progressChangelisteners =
            Collections.synchronizedMap(new WeakHashMap<ProgressChangeListener, Object>());

    private final Map<String, ProgressTracker> _progressTrackerMap =
            Collections.synchronizedMap(new HashMap<String, ProgressTracker>());

    @Override
    public void addProgressChangeListener(ProgressChangeListener listener) {
        _progressChangelisteners.put(listener, PRESENT);
    }

    @Override
    public void removeProgressChangeListener(ProgressChangeListener listener) {
        _progressChangelisteners.remove(listener);
    }

    @Override
    public ProgressTracker getProgress(String entityUuid) {
        return _progressTrackerMap.get(entityUuid);
    }

    @Override
    public void updateProgress(String entityUuid, ProgressTracker newProgress) {
        if (newProgress == null) {
            return;
        }

        ProgressTracker progress;

        synchronized(_progressTrackerMap) {
            progress = _progressTrackerMap.get(entityUuid);
            if (progress == null) {
                _progressTrackerMap.put(entityUuid, newProgress);
            } else {
                // Don't change the local object if newProgress is from remote.
                if (((ProgressTrackerVO) progress).fromLocal()
                        && !((ProgressTrackerVO) newProgress).fromLocal()) {
                    return;
                }

                // Don't trigger anything if nothing is changed.
                if (progress.getPercentage() == newProgress.getPercentage()
                        && StringUtils.equals(progress.getDescription(), newProgress.getDescription())
                        && StringUtils.equals(progress.getDetails(), newProgress.getDetails())) {
                    return;
                }

                progress.setPercentage(newProgress.getPercentage());
                progress.setDescription(newProgress.getDescription());
                progress.setDetails(newProgress.getDetails());
            }
        }

        fireProgressChange(entityUuid, newProgress);
    }

    @Override
    public void updateProgress(String entityUuid, int percentage) {
        updateProgress(entityUuid, true, percentage, null, false, null, false);
    }

    @Override
    public void updateProgress(String entityUuid, int percentage, String description) {
        updateProgress(entityUuid, true, percentage, description, true, null, false);
    }

    @Override
    public void updateProgress(String entityUuid, int percentage, String description, String details) {
        updateProgress(entityUuid, true, percentage, description, true, details, true);
    }

    @Override
    public void updateLocalProgress(String entityUuid, int percentage, String description) {
        updateProgress(entityUuid, false, percentage, description, true, null, false);
    }

    @Override
    public void removeProgress(String entityUuid) {
        ProgressTracker progress = _progressTrackerMap.remove(entityUuid);

        if (progress != null) {
            fireProgressRemove(entityUuid, progress);
        }
    }

    private void updateProgress(String entityUuid, boolean global, int percentage,
            String description, boolean includeDescription,
            String details, boolean includeDetails) {
        ProgressTracker progress;

        synchronized(_progressTrackerMap) {
            progress = _progressTrackerMap.get(entityUuid);
            if (progress == null) {
                progress = new ProgressTrackerVO();
                progress.setUuid(entityUuid);
                ((ProgressTrackerVO) progress).fromLocal(true);
                _progressTrackerMap.put(entityUuid, progress);
            } else {
                // Don't trigger anything if nothing is changed.
                if (!includeDescription && !includeDetails && percentage == progress.getPercentage()) {
                    return;
                }
            }

            progress.setGlobal(global);
            progress.setPercentage(percentage);
            if (includeDescription) {
                progress.setDescription(description);
            }
            if (includeDetails) {
                progress.setDetails(details);
            }
        }

        fireProgressChange(entityUuid, progress);
    }

    private void fireProgressChange(String entityUuid, ProgressTracker progress) {
        Set<ProgressChangeListener> listenerSet = new HashSet<ProgressChangeListener>();

        synchronized(_progressChangelisteners) {
            listenerSet.addAll(_progressChangelisteners.keySet());
        }

        for (ProgressChangeListener listener : listenerSet) {
            try {
                listener.onProgressChange(entityUuid, progress);
            } catch (Throwable t) {
                LOGGER.error("Listener.onProgressChange failed", t);
            }
        }
    }

    private void fireProgressRemove(String entityUuid, ProgressTracker progress) {
        Set<ProgressChangeListener> listenerSet = new HashSet<ProgressChangeListener>();

        synchronized(_progressChangelisteners) {
            listenerSet.addAll(_progressChangelisteners.keySet());
        }

        for (ProgressChangeListener listener : listenerSet) {
            try {
                listener.onProgressRemove(entityUuid, progress);
            } catch (Throwable t) {
                LOGGER.error("Listener.onProgressRemove failed", t);
            }
        }
    }


    /**
     * For HostUsageTracker
     */

    private final Map<HostUsageChangeListener, Object> _hostUsageChangeListeners =
            Collections.synchronizedMap(new WeakHashMap<HostUsageChangeListener, Object>());

    private final Map<String, HostUsageTracker> _hostUsageTrackerMap =
            Collections.synchronizedMap(new HashMap<String, HostUsageTracker>());

    @Override
    public void addHostUsageChangeListener(HostUsageChangeListener listener) {
        _hostUsageChangeListeners.put(listener, PRESENT);
    }

    @Override
    public void removeHostUsageChangeListener(HostUsageChangeListener listener) {
        _hostUsageChangeListeners.remove(listener);
    }

    @Override
    public HostUsageTracker getHostUsage(String hostUuid) {
        return _hostUsageTrackerMap.get(hostUuid);
    }

    @Override
    public void updateHostUsage(String hostUuid, HostUsageTracker newHostUsage) {
        if (newHostUsage == null) {
            return;
        }

        /*
        if (!((HostUsageTrackerVO) newHostUsage).fromLocal()) {
            LOGGER.trace("HostUsageTracker not from local");
        }
        */

        HostUsageTracker hostUsage;

        synchronized(_hostUsageTrackerMap) {
            hostUsage = _hostUsageTrackerMap.get(hostUuid);
            if (hostUsage == null) {
                _hostUsageTrackerMap.put(hostUuid, newHostUsage);
            } else {
                // Don't change the local object if newHostUsage is from remote.
                if (((HostUsageTrackerVO) hostUsage).fromLocal()
                        && !((HostUsageTrackerVO) newHostUsage).fromLocal()) {
                    return;
                }

                // Don't trigger anything if nothing is changed.
                if (hostUsage.getCpuUsageMHz() == newHostUsage.getCpuUsageMHz()
                        && hostUsage.getMemoryUsageMB() == newHostUsage.getMemoryUsageMB()) {
                    return;
                }

                hostUsage.setCpuUsageMHz(newHostUsage.getCpuUsageMHz());
                hostUsage.setMemoryUsageMB(newHostUsage.getMemoryUsageMB());
            }
        }

        fireHostUsageChange(hostUuid, newHostUsage);
    }

    @Override
    public void updateHostUsage(String hostUuid, int cpuUsageMHz, int memoryUsageMB) {
        HostUsageTracker hostUsage;

        synchronized(_hostUsageTrackerMap) {
            hostUsage = _hostUsageTrackerMap.get(hostUuid);
            if (hostUsage == null) {
                hostUsage = new HostUsageTrackerVO();
                hostUsage.setUuid(hostUuid);
                ((HostUsageTrackerVO) hostUsage).fromLocal(true);
                _hostUsageTrackerMap.put(hostUuid, hostUsage);
            } else {
                // Don't trigger anything if nothing is changed.
                if (hostUsage.getCpuUsageMHz() == cpuUsageMHz
                        && hostUsage.getMemoryUsageMB() == memoryUsageMB) {
                    return;
                }
            }

            hostUsage.setCpuUsageMHz(cpuUsageMHz);
            hostUsage.setMemoryUsageMB(memoryUsageMB);
        }

        fireHostUsageChange(hostUuid, hostUsage);
    }

    @Override
    public void removeHostUsage(String hostUuid) {
        HostUsageTracker hostUsage = _hostUsageTrackerMap.remove(hostUuid);

        if (hostUsage != null) {
            fireHostUsageRemove(hostUuid, hostUsage);
        }
    }

    private void fireHostUsageChange(String hostUuid, HostUsageTracker hostUsage) {
        Set<HostUsageChangeListener> listenerSet = new HashSet<HostUsageChangeListener>();

        synchronized(_hostUsageChangeListeners) {
            listenerSet.addAll(_hostUsageChangeListeners.keySet());
        }

        for (HostUsageChangeListener listener : listenerSet) {
            try {
                listener.onUsageChange(hostUuid, hostUsage);
            } catch (Throwable t) {
                LOGGER.error("Listener.onUsageChange failed", t);
            }
        }
    }

    private void fireHostUsageRemove(String hostUuid, HostUsageTracker hostUsage) {
        Set<HostUsageChangeListener> listenerSet = new HashSet<HostUsageChangeListener>();

        synchronized(_hostUsageChangeListeners) {
            listenerSet.addAll(_hostUsageChangeListeners.keySet());
        }

        for (HostUsageChangeListener listener : listenerSet) {
            try {
                listener.onUsageRemove(hostUuid, hostUsage);
            } catch (Throwable t) {
                LOGGER.error("Listener.onHostUsageRemove failed", t);
            }
        }
    }
}
