/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.LocalSessionFactoryBean;
import org.springframework.util.Assert;

import com.nimdesk.Version;
import com.nimdesk.database.dao.SettingDao;
import com.nimdesk.database.vo.SettingVO;
import com.nimdesk.model.AdminUser;

/**
 *
 */
public class DbInitializer implements InitializingBean {
    private static final Logger LOGGER = Logger.getLogger(DbInitializer.class);

    private static final String DB_VERSION = "DB_VER";

    private final Map<Integer, DbUpdater> DBUPDATERS = new HashMap<Integer, DbUpdater>();

    private LocalSessionFactoryBean _sessionFactory = null;
    private DataSource _dataSource = null;

    @Autowired
    private AdminUser.Service _adminUserService;

    @Autowired
    private SettingDao _settingDao;

    public DbInitializer() {
    }

    public void setSessionFactory(LocalSessionFactoryBean sessionFactory) {
        _sessionFactory = sessionFactory;
    }

    public void setDataSource(DataSource dataSource) {
        _dataSource = dataSource;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(_sessionFactory, "LocalSessionFactoryBean must be set");
        Assert.notNull(_dataSource, "DataSource must be set");
        Assert.notNull(_adminUserService, "AdminUser.Service must be set");

        //DBUPDATERS.put(XXX, new VerXXXDbUpdater());
    }

    public void initialize() {
        if (!isSchemaLoaded()) {
            LOGGER.info("Load database schema");
            _sessionFactory.createDatabaseSchema();

            LOGGER.info("Initialize database");
            addDefaultSettings();
            addDefaultAdmin();
        } else {
            updateSchema();
        }
    }

    private boolean isSchemaLoaded() {
        Connection conn = null;
        PreparedStatement ps = null;

        try {
            conn = _dataSource.getConnection();
            ps = conn.prepareStatement("select count(*) from settings");
            ps.executeQuery();
            return true;
        } catch (Exception e) {
            return false;
        } finally {
            try {
                ps.close();
            } catch (Exception e) {
            }
            try {
                conn.close();
            } catch (Exception e) {
            }
        }
    }

    private void updateSchema() {
        int curDbVer = getDbVersion();

        if (curDbVer >= Version.DB_VERSION) {
            LOGGER.info(String.format("Current DB version is %d (up to date)", curDbVer));
            return;
        }

        LOGGER.info(String.format("Current DB version is %d; Updating to %d...", curDbVer, Version.DB_VERSION));

        Connection conn = null;

        try {
            conn = _dataSource.getConnection();

            for (++curDbVer; curDbVer <= Version.DB_VERSION; curDbVer++) {
                DbUpdater dbUpdater = DBUPDATERS.get(curDbVer);
                if (dbUpdater != null) {
                    dbUpdater.update(conn);
                }
            }

            setDbVersion(Version.DB_VERSION);

            LOGGER.info(String.format("Updated to DB version %d", Version.DB_VERSION));
        } catch (Exception e) {
            LOGGER.error("Failed to update DB version", e);
            return;
        } finally {
            if (conn != null) {
                try {

                } catch (Exception e) {
                    // Ignore error.
                }
            }
        }
    }

    private void addDefaultSettings() {
        setDbVersion(Version.DB_VERSION);
    }

    private void addDefaultAdmin() {
        AdminUser adminUser = _adminUserService.newInstance();
        adminUser.setUsername("admin");
        adminUser.setPassword("925d2e9bb3679c1b9dd58ab20bb974fdc61f3ff4db5e12bb54fab0600261c7b3"); // "Welcome123"
        try {
            _adminUserService.commit(adminUser);
        } catch (Exception e) {
            LOGGER.error("Failed to add default admin", e);
        }
    }

    private int getDbVersion() {
        SettingVO dbVerSetting = _settingDao.findByKey(DB_VERSION);
        if (dbVerSetting == null) {
            return 0;
        }

        try {
            return Integer.parseInt(dbVerSetting.getValue());
        } catch (Exception e) {
            LOGGER.error(String.format("Invalid db version setting %s", dbVerSetting.getValue()), e);
            return 0;
        }
    }

    private void setDbVersion(int dbVer) {
        SettingVO dbVerSetting = _settingDao.findByKey(DB_VERSION);
        if (dbVerSetting == null) {
            dbVerSetting = new SettingVO();
        }
        dbVerSetting.setKey(DB_VERSION);
        dbVerSetting.setValue(Integer.toString(dbVer));

        try {
            if (dbVerSetting.getId() == 0) {
                _settingDao.createNew(dbVerSetting);
            } else {
                _settingDao.update(dbVerSetting);
            }
        } catch (Exception e) {
            LOGGER.error("Failed to set db version setting", e);
        }
    }

    private void executeQuery(Connection conn, String sqlQuery) throws Exception {
        PreparedStatement ps = null;

        try {
            ps = conn.prepareStatement(sqlQuery);
            ps.executeQuery();
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e1) {
                }
            }
        }
    }

    @SuppressWarnings("unused")
    private void executeQueryNoException(Connection conn, String sqlQuery) {
        try {
            executeQuery(conn, sqlQuery);
        } catch (Exception e) {
            LOGGER.error(String.format("Failed to execute query \"%s\"", sqlQuery), e);
        }
    }

    private void executeUpdate(Connection conn, String sqlUpdate) throws Exception {
        PreparedStatement ps = null;

        try {
            ps = conn.prepareStatement(sqlUpdate);
            ps.executeUpdate();
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e1) {
                }
            }
        }
    }

    private void executeUpdateNoException(Connection conn, String sqlUpdate) {
        try {
            executeUpdate(conn, sqlUpdate);
        } catch (Exception e) {
            LOGGER.error(String.format("Failed to execute update \"%s\"", sqlUpdate), e);
        }
    }

    private interface DbUpdater {
        void update(Connection conn);
    }
}
