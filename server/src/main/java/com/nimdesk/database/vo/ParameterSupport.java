/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.database.vo;

import java.io.IOException;
import java.io.StringReader;
import java.util.Properties;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import org.apache.commons.io.output.StringBuilderWriter;
import org.apache.commons.lang.StringUtils;

@MappedSuperclass
public abstract class ParameterSupport extends BaseVO {

    private static final long serialVersionUID = -2862918717424337440L;

    @Column(name="params", nullable=true, length=BaseVO.PARAMS_LENGTH)
    private String parameters = null;

    private transient Properties _properties;
    private transient boolean _dirty = false;

    public synchronized String getParameters() {
        saveToString();
        return parameters;
    }

    public synchronized void setParameters(String parameters) {
        this.parameters = parameters;
        loadFromString();
    }

    public synchronized String getParameter(String key) {
        if (_properties == null) {
            loadFromString();
        }

        return _properties.getProperty(key);
    }

    public synchronized void setParameter(String key, String value) {
        if (_properties == null) {
            loadFromString();
        }

        if (value != null) {
            _properties.setProperty(key, value);
        } else {
            _properties.remove(key);
        }

        _dirty = true;
    }

    public void removeParameter(String key) {
        setParameter(key, null);
    }

    public void removeAllParamters() {
        this.parameters = null;
        if (_properties != null) {
            _properties.clear();
        }
        _dirty = false;
    }

    public Properties retrieveParameters() {
        if (_properties == null) {
            loadFromString();
        }

        return _properties;
    }

    public long getParameterLong(String key, long defaultValue) {
        long value = defaultValue;
        String paramValue = getParameter(key);
        if (!StringUtils.isEmpty(paramValue)) {
            try {
                value = Long.parseLong(paramValue);
            } catch (Exception e) {
                // Ignore error
            }
        }
        return value;
    }

    public void setParameterLong(String key, long parameter) {
        setParameter(key, Long.toString(parameter));
    }

    public int getParameterInt(String key, int defaultValue) {
        int value = defaultValue;
        String paramValue = getParameter(key);
        if (!StringUtils.isEmpty(paramValue)) {
            try {
                value = Integer.parseInt(paramValue);
            } catch (Exception e) {
                // Ignore error
            }
        }
        return value;
    }

    public void setParameterInt(String key, int parameter) {
        setParameter(key, Integer.toString(parameter));
    }

    public boolean getParameterBoolean(String key, boolean defaultValue) {
        boolean value = defaultValue;
        String paramValue = getParameter(key);
        if (!StringUtils.isEmpty(paramValue)) {
            value = ("1".equals(paramValue) || "y".equalsIgnoreCase(paramValue));
        }
        return value;
    }

    public void setParameterBoolean(String key, boolean parameter) {
        setParameter(key, parameter ? "1" : "0");
    }

    public void setParameterBoolean(String key, boolean parameter, boolean removeIfValue) {
        if (parameter == removeIfValue) {
            removeParameter(key);
        } else {
            setParameter(key, parameter ? "1" : "0");
        }
    }

    public String[] getParameterStringArray(String key) {
        String paramValue = getParameter(key);
        if (paramValue == null) {
            paramValue = "";
        }
        return StringUtils.split(paramValue, ',');
    }

    public void setParameterStringArray(String key, String[] parameter) {
        if (parameter != null && parameter.length == 0) {
            parameter = null;
        }
        setParameter(key, StringUtils.join(parameter, ','));
    }

    @Override
    public void prePersist() {
        super.prePersist();
        saveToString();
    }

    protected void loadFromString() {
        _dirty = false;

        _properties = new Properties();
        if (!StringUtils.isEmpty(parameters)) {
            try {
                _properties.load(new StringReader(parameters));
            } catch (IOException e) {
                // Log error;
            }
        }
    }

    protected void saveToString() {
        if (_dirty && _properties != null) {
            StringBuilderWriter writer = new StringBuilderWriter();
            try {
                if (!_properties.isEmpty()) {
                    _properties.store(writer, null);
                }
                parameters = writer.toString();
                _dirty = false;
            } catch (IOException e) {
                // TODO log error
            }
        }
    }
}
