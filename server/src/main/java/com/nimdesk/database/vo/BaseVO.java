/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.database.vo;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.springframework.beans.BeanUtils;

import com.nimdesk.model.Base;
import com.nimdesk.util.Pair;

@MappedSuperclass
public abstract class BaseVO implements Base, Serializable {

    public static final int UUID_LENGTH = 64;
    public static final int NAME_LENGTH = 32;
    public static final int DESCRIPTION_LENGTH = 1024;
    public static final int VERSION_LENGTH = 32;
    public static final int STATUS_LENGTH = 16;
    public static final int TYPE_LENGTH = 16;
    public static final int PARAMS_LENGTH = 64 * 1024;

    private static final long serialVersionUID = 1L;

    private transient Map<String, Pair<Object, Object>> _propertyChangeTracker = null;
    private transient boolean _fromLocal = false;
    private transient boolean _fromRemote = false;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id", unique=true, nullable=false)
    private Long id = 0L;

    @Column(name="uuid", unique=true, length=BaseVO.UUID_LENGTH)
    private String uuid;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getUuid() {
        return uuid;
    }

    @Override
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public void prePersist() {
    }

    public boolean fromLocal() {
        return _fromLocal;
    }

    public void fromLocal(boolean fromLocal) {
        _fromLocal = fromLocal;
    }

    public boolean fromRemote() {
        return _fromRemote;
    }

    public void fromRemote(boolean fromRemote) {
        _fromRemote = fromRemote;
    }

    public synchronized void trackPropertyChange(String propertyName, Object oldValue, Object newValue) {
        if (id <= 0) {
            // Don't track new objects.
            return;
        }

        if (oldValue == null) {
            if (newValue == null) {
                // Both are null - no need to track.
                return;
            }
        } else {
            if (oldValue.equals(newValue)) {
                // The same value - no need to track.
                return;
            }
        }

        if (_propertyChangeTracker == null) {
            _propertyChangeTracker = new HashMap<String, Pair<Object, Object>>();
        }

        Pair<Object, Object> oldNewPair = _propertyChangeTracker.get(propertyName);
        if (oldNewPair != null) {
            // Only update the new value
            oldNewPair.setSecond(newValue);
        } else {
            oldNewPair = new Pair<Object, Object>(oldValue, newValue);
            _propertyChangeTracker.put(propertyName, oldNewPair);
        }
    }

    public synchronized Map<String, Pair<Object, Object>> resetChangedProperties() {
        Map<String, Pair<Object, Object>> changeProperties = _propertyChangeTracker;
        _propertyChangeTracker = null;

        return changeProperties;
    }

    public synchronized void applyChangedProperties(BaseVO entity) {
        // For now just copy all properties. In the future, we'll only apply
        // changed properties.
        BeanUtils.copyProperties(this, entity);
    }
}
