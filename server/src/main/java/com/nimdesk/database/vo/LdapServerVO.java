/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.database.vo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.nimdesk.model.LdapServer;

@Entity
@Table(name="ldapservers")
@Cache(usage=CacheConcurrencyStrategy.READ_WRITE)
@NamedQueries({@NamedQuery(name=LdapServerVO.FIND_BY_DOMAIN,
                           query="from LdapServerVO ls where lower(ls.domain)=lower(:domain)")})
public class LdapServerVO extends BaseVO implements LdapServer {

    public static final String FIND_BY_DOMAIN = "LdapServerVO.findByDomain";

    private static final long serialVersionUID = -6114629096779717048L;

    @Column(name="address", nullable=false)
    private String address;

    @Column(name="port", nullable=false)
    private int port = 389;

    @Column(name="is_ssl", nullable=false, columnDefinition="tinyint")
    private boolean ssl = false;

    @Column(name="domain", unique=true, nullable=false)
    private String domain;

    @Column(name="username", nullable=false)
    private String username;

    @Column(name="password", nullable=false)
    private String password;

    @Override
    public String getAddress() {
        return address;
    }

    @Override
    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public int getPort() {
        return port;
    }

    @Override
    public void setPort(int port) {
        this.port = port;
    }

    @Override
    public boolean isSsl() {
        return ssl;
    }

    @Override
    public void setSsl(boolean ssl) {
        this.ssl = ssl;
    }

    @Override
    public String getDomain() {
        return domain;
    }

    @Override
    public void setDomain(String domain) {
        this.domain = domain;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public void setPassword(String password) {
        this.password = password;
    }
}
