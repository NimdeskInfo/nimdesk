/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.database.vo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.nimdesk.model.PoolUser;

@Entity
@Table(name="pool_user",
       uniqueConstraints={@UniqueConstraint(columnNames={"pool_uuid", "user_uuid"})})
@Cache(usage=CacheConcurrencyStrategy.READ_WRITE)
@NamedQueries({@NamedQuery(name=PoolUserVO.FIND_BY_POOL_USER,
                           query="from PoolUserVO pu where pu.poolUuid=:poolUuid and pu.userUuid=:userUuid"),
               @NamedQuery(name=PoolUserVO.FIND_BY_POOL_UUID,
                           query="from PoolUserVO pu where pu.poolUuid=:poolUuid"),
               @NamedQuery(name=PoolUserVO.FIND_BY_USER_UUID,
                           query="from PoolUserVO pu where pu.userUuid=:userUuid"),
               @NamedQuery(name=PoolUserVO.COUNT_BY_POOL_UUID,
                           query="select count(*) from PoolUserVO pu where pu.poolUuid=:poolUuid"),
               @NamedQuery(name=PoolUserVO.COUNT_BY_USER_UUID,
                           query="select count(*) from PoolUserVO pu where pu.userUuid=:userUuid"),
               @NamedQuery(name=PoolUserVO.DELETE_BY_POOL_UUID,
                           query="delete from PoolUserVO pu where pu.poolUuid=:poolUuid"),
               @NamedQuery(name=PoolUserVO.DELETE_BY_USER_UUID,
                           query="delete from PoolUserVO pu where pu.userUuid=:userUuid")})
public class PoolUserVO extends BaseVO implements PoolUser {

    public static final String FIND_BY_POOL_USER = "PoolUserVO.findByPoolUser";
    public static final String FIND_BY_POOL_UUID = "PoolUserVO.findByPoolUuid";
    public static final String FIND_BY_USER_UUID = "PoolUserVO.findByUserUuid";

    public static final String COUNT_BY_POOL_UUID = "PoolUserVO.countByPoolUuid";
    public static final String COUNT_BY_USER_UUID = "PoolUserVO.countByUserUuid";

    public static final String DELETE_BY_POOL_UUID = "PoolUserVO.deleteByPoolUuid";
    public static final String DELETE_BY_USER_UUID = "PoolUserVO.deleteByUserUuid";

    private static final long serialVersionUID = -3373579227714572875L;

    @Column(name="pool_uuid", nullable=false, length=BaseVO.UUID_LENGTH)
    private String poolUuid;

    @Column(name="user_uuid", nullable=false, length=BaseVO.UUID_LENGTH)
    private String userUuid;

    @Override
    public String getPoolUuid() {
        return poolUuid;
    }

    @Override
    public void setPoolUuid(String poolUuid) {
        this.poolUuid= poolUuid;
    }

    @Override
    public String getUserUuid() {
        return userUuid;
    }

    @Override
    public void setUserUuid(String userUuid) {
        this.userUuid = userUuid;
    }
}
