/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.database.vo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.nimdesk.model.AdminUser;

@Entity
@Table(name="adminusers")
@Cache(usage=CacheConcurrencyStrategy.READ_WRITE)
public class AdminUserVO extends ParameterSupport implements AdminUser {

    private static final long serialVersionUID = -1056739114266399783L;

    private static final String PARAM_FULLNAME = "fullname";
    private static final String PARAM_EMAIL = "email";
    private static final String PARAM_NOTIFY = "notify";

    @Column(name="username", unique=true, nullable=false)
    private String username;

    @Column(name="password", nullable=false)
    private String password;

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String getFullName() {
        return getParameter(PARAM_FULLNAME);
    }

    @Override
    public void setFullName(String fullName) {
        setParameter(PARAM_FULLNAME, fullName);
    }

    @Override
    public String getEmail() {
        return getParameter(PARAM_EMAIL);
    }

    @Override
    public void setEmail(String email) {
        setParameter(PARAM_EMAIL, email);
    }

    @Override
    public boolean isNotify() {
        return getParameterBoolean(PARAM_NOTIFY, false);
    }

    @Override
    public void setNotify(boolean notify) {
        setParameterBoolean(PARAM_NOTIFY, notify, false);
    }
}
