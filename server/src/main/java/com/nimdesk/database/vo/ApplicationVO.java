/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.database.vo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.nimdesk.model.Application;

@Entity
@Table(name="applications")
@Cache(usage=CacheConcurrencyStrategy.READ_WRITE)
public class ApplicationVO extends ParameterSupport implements Application {

    private static final long serialVersionUID = 362690095190099023L;

    private static final String PARAM_UPDATETIME = "updateTime";

    @Column(name="name", nullable=false, length=BaseVO.NAME_LENGTH)
    private String name;

    @Column(name="cmd_line", nullable=false, length=BaseVO.PARAMS_LENGTH)
    private String cmdLine;

    @Column(name="icon", nullable=true, length=BaseVO.PARAMS_LENGTH)
    private String icon;

    @Column(name="vm_image_uuid", nullable=false, length=BaseVO.UUID_LENGTH)
    private String vmImageUuid;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public long getUpdateTime() {
        return getParameterLong(PARAM_UPDATETIME, 0);
    }

    @Override
    public void setUpdateTime(long updateTime) {
        setParameterLong(PARAM_UPDATETIME, updateTime);
    }

    @Override
    public String getCmdLine() {
        return cmdLine;
    }

    @Override
    public void setCmdLine(String cmdLine) {
        this.cmdLine = cmdLine;
    }

    @Override
    public String getIcon() {
        return icon;
    }

    @Override
    public void setIcon(String icon) {
        this.icon = icon;
    }

    @Override
    public String getVmImageUuid() {
        return vmImageUuid;
    }

    @Override
    public void setVmImageUuid(String vmImageUuid) {
        this.vmImageUuid = vmImageUuid;
    }
}
