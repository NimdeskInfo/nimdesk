/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.database.vo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.nimdesk.model.UserGroup;

@Entity
@Table(name="user_group",
       uniqueConstraints={@UniqueConstraint(columnNames={"user_uuid", "group_uuid"})})
@Cache(usage=CacheConcurrencyStrategy.READ_WRITE)
@NamedQueries({@NamedQuery(name=UserGroupVO.FIND_BY_USER_GROUP,
                           query="from UserGroupVO ug where ug.userUuid=:userUuid and ug.groupUuid=:groupUuid"),
               @NamedQuery(name=UserGroupVO.FIND_BY_USER_UUID,
                           query="from UserGroupVO ug where ug.userUuid=:userUuid"),
               @NamedQuery(name=UserGroupVO.FIND_BY_GROUP_UUID,
                           query="from UserGroupVO ug where ug.groupUuid=:groupUuid")})
public class UserGroupVO extends BaseVO implements UserGroup {

    public static final String FIND_BY_USER_GROUP = "UserGroupVO.findByUserGroup";
    public static final String FIND_BY_USER_UUID = "UserGroupVO.findByUserUuid";
    public static final String FIND_BY_GROUP_UUID = "UserGroupVO.findByGroupUuid";

    private static final long serialVersionUID = 6300816977397762297L;

    @Column(name="user_uuid", nullable=false, length=BaseVO.UUID_LENGTH)
    private String userUuid;

    @Column(name="group_uuid", nullable=false, length=BaseVO.UUID_LENGTH)
    private String groupUuid;

    @Override
    public String getUserUuid() {
        return userUuid;
    }

    @Override
    public void setUserUuid(String userUuid) {
        this.userUuid = userUuid;
    }

    @Override
    public String getGroupUuid() {
        return groupUuid;
    }

    @Override
    public void setGroupUuid(String groupUuid) {
        this.groupUuid = groupUuid;
    }
}
