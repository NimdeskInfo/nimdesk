/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.database.vo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.nimdesk.model.UserSession;

@Entity
@Table(name="usersessions")
@Cache(usage=CacheConcurrencyStrategy.READ_WRITE)
@NamedQueries({@NamedQuery(name=UserSessionVO.INVALIDATE_ACTIVE_SESSIONS,
                           query="update UserSessionVO us set us.endTime=:endTime where us.endTime=0 and us.hostUuid=:hostUuid"),
               @NamedQuery(name=UserSessionVO.DELETE_OLD_USERSESSIONS,
                           query="delete from UserSessionVO us where us.startTime<=:startTime")})
public class UserSessionVO extends ParameterSupport implements UserSession {

    private static final long serialVersionUID = -22031164537491012L;

    public static final String INVALIDATE_ACTIVE_SESSIONS = "UserSessionVO.invalidateActiveSessions";

    public static final String DELETE_OLD_USERSESSIONS = "UserSessionVO.deleteOldUserSessions";

    private static final String PARAM_POOL_NAME = "pool";
    private static final String PARAM_VMINSTANCE_NAME = "vmInstanceName";
    private static final String PARAM_VMINSTANCE_DNS = "vmInstanceDns";
    private static final String PARAM_VMINSTANCE_IP = "vmInstanceIp";

    @Column(name="user_uuid", nullable=false, length=BaseVO.UUID_LENGTH)
    private String userUuid;

    @Column(name="vminstance_uuid", nullable=false, length=BaseVO.UUID_LENGTH)
    private String vmInstanceUuid;

    @Column(name="start_time", nullable=false)
    private long startTime = 0;

    @Column(name="end_time", nullable=false)
    private long endTime = 0;

    @Column(name="host_uuid", nullable=false, length=BaseVO.UUID_LENGTH)
    private String hostUuid;

    @Override
    public String getUserUuid() {
        return userUuid;
    }

    @Override
    public void setUserUuid(String userUuid) {
        this.userUuid = userUuid;
    }

    @Override
    public String getVmInstanceUuid() {
        return vmInstanceUuid;
    }

    @Override
    public void setVmInstanceUuid(String vmInstanceUuid) {
        this.vmInstanceUuid = vmInstanceUuid;
    }

    @Override
    public String getPoolName() {
        return getParameter(PARAM_POOL_NAME);
    }

    @Override
    public void setPoolName(String poolName) {
        setParameter(PARAM_POOL_NAME, poolName);
    }

    @Override
    public String getVmInstanceName() {
        return getParameter(PARAM_VMINSTANCE_NAME);
    }

    @Override
    public void setVmInstanceName(String vmInstanceName) {
        setParameter(PARAM_VMINSTANCE_NAME, vmInstanceName);
    }

    @Override
    public String getVmInstanceDns() {
        return getParameter(PARAM_VMINSTANCE_DNS);
    }

    @Override
    public void setVmInstanceDns(String vmInstanceDns) {
        setParameter(PARAM_VMINSTANCE_DNS, vmInstanceDns);
    }

    @Override
    public String getVmInstanceIp() {
        return getParameter(PARAM_VMINSTANCE_IP);
    }

    @Override
    public void setVmInstanceIp(String vmInstanceIp) {
        setParameter(PARAM_VMINSTANCE_IP, vmInstanceIp);
    }

    @Override
    public long getStartTime() {
        return startTime;
    }

    @Override
    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    @Override
    public long getEndTime() {
        return endTime;
    }

    @Override
    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    @Override
    public String getHostUuid() {
        return hostUuid;
    }

    @Override
    public void setHostUuid(String hostUuid) {
        this.hostUuid = hostUuid;
    }
}
