/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.database.vo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.nimdesk.model.GlobalSetting;

@Entity
@Table(name="globalsettings")
@Cache(usage=CacheConcurrencyStrategy.READ_WRITE)
public class GlobalSettingVO extends ParameterSupport implements GlobalSetting, Serializable {

    private static final long serialVersionUID = 6486605773406294131L;

    @Column(name="key", unique=true, nullable=false, length=BaseVO.NAME_LENGTH)
    private String key;

    public GlobalSettingVO() {
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public void setKey(String key) {
        this.key = key;
    }

    @Override
    public String getValue() {
        return getParameter(PARAM_VALUE);
    }

    @Override
    public void setValue(String value) {
        removeAllParamters();
        setParameter(PARAM_VALUE, value);
    }
}

