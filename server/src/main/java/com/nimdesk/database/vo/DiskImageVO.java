/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.database.vo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.nimdesk.model.Base;
import com.nimdesk.model.DiskImage;

@Entity
@Table(name="diskimages",
       uniqueConstraints={@UniqueConstraint(columnNames={"guid", "host_uuid"})})
@Cache(usage=CacheConcurrencyStrategy.READ_WRITE)
@NamedQueries({@NamedQuery(name=DiskImageVO.COUNT_BY_OWNER_UUID,
                           query="select count(*) from DiskImageVO di"),
               @NamedQuery(name=DiskImageVO.INVALIDATE_NON_LOCAL_IMAGES,
                           query="update DiskImageVO di set di.status='Unknown' where di.hostUuid!=:hostUuid")})
public class DiskImageVO extends ParameterSupport implements DiskImage {

    public static final String COUNT_BY_OWNER_UUID = "DiskImageVO.countByOwnerUuid";

    public static final String INVALIDATE_NON_LOCAL_IMAGES = "DiskImageVO.invalidateNonLocalImages";

    private static final long serialVersionUID = -7327180508670942240L;

    private static final String PARAM_VM_REFID = "vmRefId";
    private static final String PARAM_IP = "ip";
    private static final String PARAM_CBRC_ENABLED = "cbrc";

    @Column(name="guid", nullable=false, length=BaseVO.UUID_LENGTH)
    private String guid;

    @Column(name="host_uuid", nullable=false, length=BaseVO.UUID_LENGTH)
    private String hostUuid;

    @Column(name="storage_uuid", nullable=false, length=BaseVO.UUID_LENGTH)
    private String storageUuid;

    @Column(name="name", nullable=false, length=BaseVO.NAME_LENGTH)
    private String name;

    @Column(name="version", nullable=false)
    private long version;

    @Enumerated(EnumType.STRING)
    @Column(name="type", nullable=false, length=BaseVO.TYPE_LENGTH)
    private DiskImage.Type type;

    @Column(name="owner_uuid", nullable=false, length=BaseVO.UUID_LENGTH)
    private String ownerUuid;

    @Column(name="parent_guid", nullable=true, length=BaseVO.UUID_LENGTH)
    private String parentGuid;

    @Column(name="create_time", nullable=false)
    private long createTime = 0;

    @Enumerated(EnumType.STRING)
    @Column(name="status", nullable=false, length=BaseVO.STATUS_LENGTH)
    private DiskImage.Status status;

    @Column(name="disk_size")
    private long diskSize;

    @Override
    public String getGuid() {
        return guid;
    }

    @Override
    public void setGuid(String guid) {
        this.guid = guid;
    }

    @Override
    public String getHostUuid() {
        return hostUuid;
    }

    @Override
    public void setHostUuid(String hostUuid) {
        this.hostUuid = hostUuid;
    }

    @Override
    public String getStorageUuid() {
        return storageUuid;
    }

    @Override
    public void setStorageUuid(String storageUuid) {
        this.storageUuid = storageUuid;
    }

    @Override
    public String getVmRefId() {
        return getParameter(PARAM_VM_REFID);
    }

    @Override
    public void setVmRefId(String vmRefId) {
        setParameter(PARAM_VM_REFID, vmRefId);
    }

    @Override
    public String getIpAddress() {
        return getParameter(PARAM_IP);
    }

    @Override
    public void setIpAddress(String ipAddress) {
        setParameter(PARAM_IP, ipAddress);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public long getVersion() {
        return version;
    }

    @Override
    public void setVersion(long version) {
        this.version = version;
    }

    @Override
    public Type getType() {
        return type;
    }

    @Override
    public void setType(Type type) {
        this.type = type;
    }

    @Override
    public String getOwnerUuid() {
        return ownerUuid;
    }

    @Override
    public void setOwnerUuid(String ownerUuid) {
        this.ownerUuid = ownerUuid;
    }

    @Override
    public String getParentGuid() {
        return parentGuid;
    }

    @Override
    public void setParentGuid(String parentGuid) {
        this.parentGuid = parentGuid;
    }

    @Override
    public long getCreateTime() {
        return createTime;
    }

    @Override
    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    @Override
    public Status getStatus() {
        return status;
    }

    @Override
    public void setStatus(Status status) {
        Status oldStatus = this.status;
        this.status = status;

        trackPropertyChange(Base.PROP_STATUS, oldStatus, status);
    }

    @Override
    public long getDiskSize() {
        return diskSize;
    }

    @Override
    public void setDiskSize(long diskSize) {
        this.diskSize = diskSize;
    }

    @Override
    public boolean isCbrcEnabled() {
        return getParameterBoolean(PARAM_CBRC_ENABLED, false);
    }

    @Override
    public void setCbrcEnabled(boolean cbrcEnabled) {
        setParameterBoolean(PARAM_CBRC_ENABLED, cbrcEnabled, false);
    }
}

