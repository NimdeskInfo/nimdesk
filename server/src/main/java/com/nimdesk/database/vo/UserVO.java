/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.database.vo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.nimdesk.model.User;

@Entity
@Table(name="users",
       uniqueConstraints={@UniqueConstraint(columnNames={"logon_name", "domain"})})
@Cache(usage=CacheConcurrencyStrategy.READ_WRITE)
@NamedQueries({@NamedQuery(name=UserVO.FIND_BY_DN,
                           query="from UserVO u where u.distinguishedName=:distinguishedName"),
               @NamedQuery(name=UserVO.FIND_BY_LOGONNAME_DOMAIN,
                           query="from UserVO u where lower(u.logonName)=lower(:logonName) and lower(u.domain)=lower(:domain)"),
               @NamedQuery(name=UserVO.FIND_DOMAINS,
                           query="select distinct u.domain from UserVO u")})
public class UserVO extends BaseVO implements User {

    public static final String FIND_BY_DN = "UserVO.findByDn";
    public static final String FIND_BY_LOGONNAME_DOMAIN = "UserVO.findByLogonNameDomain";
    public static final String FIND_DOMAINS = "UserVO.findDomains";

    private static final long serialVersionUID = -2109286227298598790L;

    @Enumerated(EnumType.STRING)
    @Column(name="user_type", nullable=false, length=BaseVO.TYPE_LENGTH)
    private User.Type type;

    @Column(name="name", nullable=false)
    private String name;

    @Column(name="logon_name", nullable=false)
    private String logonName;

    @Column(name="password", nullable=false)
    private String password = "";

    @Column(name="domain", nullable=false)
    private String domain = "";

    @Column(name="distinguished_name", unique=true, nullable=false, length=1024)
    private String distinguishedName;

    @Override
    public User.Type getType() {
        return type;
    }

    @Override
    public void setType(User.Type type) {
        this.type = type;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getLogonName() {
        return logonName;
    }

    @Override
    public void setLogonName(String logonName) {
        this.logonName = logonName;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String getDomain() {
        return domain;
    }

    @Override
    public void setDomain(String domain) {
        this.domain = domain;
    }

    @Override
    public String getDistinguishedName() {
        return distinguishedName;
    }

    @Override
    public void setDistinguishedName(String distinguishedName) {
        this.distinguishedName = distinguishedName;
    }
}
