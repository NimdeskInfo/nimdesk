/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.database.vo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.nimdesk.model.Base;
import com.nimdesk.model.VmInstance;

@Entity
@Table(name="vminstances")
@Cache(usage=CacheConcurrencyStrategy.READ_WRITE)
@NamedQueries({@NamedQuery(name=VmInstanceVO.FIND_BY_DISKIMAGE_UUID,
                           query="from VmInstanceVO vi where vi.diskImageUuid=:diskImageUuid"),
               @NamedQuery(name=VmInstanceVO.FIND_BY_POOL_UUID,
                           query="from VmInstanceVO vi where vi.poolUuid=:poolUuid"),
               @NamedQuery(name=VmInstanceVO.FIND_BY_USER_UUID,
                           query="from VmInstanceVO vi where vi.userUuid=:userUuid"),
               @NamedQuery(name=VmInstanceVO.FIND_IN_POOL_BY_USER,
                           query="from VmInstanceVO vi where vi.poolUuid=:poolUuid and vi.userUuid=:userUuid"),
               @NamedQuery(name=VmInstanceVO.COUNT_BY_DISKIMAGE_UUID,
                           query="select count(*) from VmInstanceVO vi where vi.diskImageUuid=:diskImageUuid"),
               @NamedQuery(name=VmInstanceVO.COUNT_BY_POOL_UUID,
                           query="select count(*) from VmInstanceVO vi where vi.poolUuid=:poolUuid"),
               @NamedQuery(name=VmInstanceVO.COUNT_BY_USER_UUID,
                           query="select count(*) from VmInstanceVO vi where vi.userUuid=:userUuid"),
               @NamedQuery(name=VmInstanceVO.COUNT_ALL_INSESSION,
                           query="select count(*) from VmInstanceVO vi where vi.userUuid!=''"),
               @NamedQuery(name=VmInstanceVO.INVALIDATE_NON_LOCAL_VMINSTANCES,
                           query="update VmInstanceVO vi set vi.status='Unknown' where vi.hostUuid!=:hostUuid")})
public class VmInstanceVO extends ParameterSupport implements VmInstance {
    private static final Logger LOGGER = Logger.getLogger(VmInstanceVO.class);

    public static final String FIND_BY_DISKIMAGE_UUID = "VmInstanceVO.findByDiskImageUuid";
    public static final String FIND_BY_POOL_UUID = "VmInstanceVO.findByPoolUuid";
    public static final String FIND_BY_USER_UUID = "VmInstanceVO.findByUserUuid";
    public static final String FIND_IN_POOL_BY_USER = "VmInstanceVO.findInPoolByUser";

    public static final String COUNT_BY_DISKIMAGE_UUID = "VmInstanceVO.countByDiskImageUuid";
    public static final String COUNT_BY_POOL_UUID = "VmInstanceVO.countByPoolUuid";
    public static final String COUNT_BY_USER_UUID = "VmInstanceVO.countByUserUuid";
    public static final String COUNT_ALL_INSESSION = "VmInstanceVO.countAllInSession";

    public static final String INVALIDATE_NON_LOCAL_VMINSTANCES = "VmInstanceVO.invalidateNonLocalVmInstances";

    private static final long serialVersionUID = -680171167441507761L;

    private transient boolean _inOperation = false;

    private static final String PARAM_MAC = "mac";
    private static final String PARAM_IN_DOMAIN = "inDomain";
    private static final String PARAM_CBRC_ENABLED = "cbrc";
    private static final String PARAM_ASSIGNED = "assigned";
    private static final String PARAM_LOGOFF = "logOff";
    private static final String PARAM_UDD_OWNER = "uddOwner";
    private static final String PARAM_LAST_ERROR = "lastErr";
    private static final String PARAM_RECOMPOSE = "recompose";

    @Column(name="create_time", nullable=false)
    private long createTime = 0;

    @Column(name="name", unique=true, nullable=false, length=BaseVO.NAME_LENGTH)
    private String name;

    @Column(name="pool_uuid", nullable=false, length=BaseVO.UUID_LENGTH)
    private String poolUuid;

    @Column(name="disk_image_uuid", nullable=false, length=BaseVO.UUID_LENGTH)
    private String diskImageUuid;

    @Column(name="host_uuid", nullable=false, length=BaseVO.UUID_LENGTH)
    private String hostUuid;

    @Column(name="storage_uuid", nullable=false, length=BaseVO.UUID_LENGTH)
    private String storageUuid;

    @Column(name="vm_refId", nullable=false)
    private String vmRefId = "";

    @Column(name="dns_name", nullable=true)
    private String dnsName;

    @Column(name="ip_address", nullable=true)
    private String ipAddress;

    @Column(name="user", nullable=false)
    private String userUuid = "";

    @Column(name="login_time", nullable=false)
    private long loginTime = 0;

    @Enumerated(EnumType.STRING)
    @Column(name="status", nullable=false, length=BaseVO.STATUS_LENGTH)
    private VmInstance.Status status;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getPoolUuid() {
        return poolUuid;
    }

    @Override
    public void setPoolUuid(String poolUuid) {
        this.poolUuid = poolUuid;
    }

    @Override
    public long getUpdateTime() {
        return createTime;
    }

    @Override
    public void setUpdateTime(long updateTime) {
        // We have to guarantee the updateTime is always moving up.
        if (updateTime <= this.createTime) {
            ++this.createTime;
        } else {
            this.createTime = updateTime;
        }
    }

    @Override
    public String getDiskImageUuid() {
        return diskImageUuid;
    }

    @Override
    public void setDiskImageUuid(String diskImageUuid) {
        this.diskImageUuid = diskImageUuid;
    }

    @Override
    public String getHostUuid() {
        return hostUuid;
    }

    @Override
    public void setHostUuid(String hostUuid) {
        this.hostUuid = hostUuid;
    }

    @Override
    public String getStorageUuid() {
        return storageUuid;
    }

    @Override
    public void setStorageUuid(String storageUuid) {
        this.storageUuid = storageUuid;
    }

    @Override
    public String getVmRefId() {
        return vmRefId;
    }

    @Override
    public void setVmRefId(String vmRefId) {
        this.vmRefId = vmRefId;
    }

    @Override
    public boolean isCbrcEnabled() {
        return getParameterBoolean(PARAM_CBRC_ENABLED, false);
    }

    @Override
    public void setCbrcEnabled(boolean cbrcEnabled) {
        setParameterBoolean(PARAM_CBRC_ENABLED, cbrcEnabled, false);
    }

    @Override
    public String getDnsName() {
        return dnsName;
    }

    @Override
    public void setDnsName(String dnsName) {
        this.dnsName = dnsName;
    }

    @Override
    public String getIpAddress() {
        return ipAddress;
    }

    @Override
    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    @Override
    public String getMacAddress() {
        return getParameter(PARAM_MAC);
    }

    @Override
    public void setMacAddress(String macAddress) {
        setParameter(PARAM_MAC, macAddress);
    }

    @Override
    public boolean isInDomin() {
        return getParameterBoolean(PARAM_IN_DOMAIN, false);
    }

    @Override
    public void setInDomain(boolean inDomin) {
        setParameterBoolean(PARAM_IN_DOMAIN, inDomin, false);
    }

    @Override
    public String getUserUuid() {
        return userUuid;
    }

    @Override
    public void setUserUuid(String userUuid) {
        this.userUuid = userUuid;
    }

    @Override
    public boolean isAssigned() {
        return getParameterBoolean(PARAM_ASSIGNED, false);
    }

    @Override
    public void setAssigned(boolean assigned) {
        setParameterBoolean(PARAM_ASSIGNED, assigned, false);
    }

    @Override
    public LogOffMode getLogOffMode() {
        String logOffModeStr = getParameter(PARAM_LOGOFF);
        if (!StringUtils.isEmpty(logOffModeStr)) {
            try {
                return VmInstance.LogOffMode.valueOf(logOffModeStr);
            } catch (Exception e) {
                LOGGER.warn(String.format("Invalid VmInstance.LogOffMode value \"%s\"", logOffModeStr));
            }
        }

        return null;
    }

    @Override
    public void setLogOffMode(LogOffMode logOffMode) {
        String logOffModeStr = null;
        if (logOffMode != null) {
            logOffModeStr = logOffMode.name();
        }

        setParameter(PARAM_LOGOFF, logOffModeStr);
    }

    @Override
    public String getUserDataDiskOwner() {
        return getParameter(PARAM_UDD_OWNER);
    }

    @Override
    public void setUserDataDiskOwner(String userDataDiskOwner) {
        setParameter(PARAM_UDD_OWNER, userDataDiskOwner);
    }

    @Override
    public long getLoginTime() {
        return loginTime;
    }

    @Override
    public void setLoginTime(long loginTime) {
        this.loginTime = loginTime;
    }

    @Override
    public Status getStatus() {
        return status;
    }

    @Override
    public void setStatus(Status status) {
        Status oldStatus = this.status;
        this.status = status;

        trackPropertyChange(Base.PROP_STATUS, oldStatus, status);
    }

    @Override
    public Error getLastError() {
        String errorStr = getParameter(PARAM_LAST_ERROR);
        if (!StringUtils.isEmpty(errorStr)) {
            try {
                return VmInstance.Error.valueOf(errorStr);
            } catch (Exception e) {
                LOGGER.warn(String.format("Invalid VmInstance.Error value \"%s\"", errorStr));
                return VmInstance.Error.UnknownError;
            }
        }

        return null;
    }

    @Override
    public void setLastError(Error error) {
        Error oldError = getLastError();

        String errorStr = null;
        if (error != null) {
            errorStr = error.name();
        }

        setParameter(PARAM_LAST_ERROR, errorStr);

        trackPropertyChange(Base.PROP_LASTERROR, oldError, error);
    }

    @Override
    public boolean isRecompose() {
        return getParameterBoolean(PARAM_RECOMPOSE, false);
    }

    @Override
    public void setRecompose(boolean recompose) {
        setParameterBoolean(PARAM_RECOMPOSE, recompose, false);
    }

    public boolean inOperation() {
        return _inOperation;
    }

    public void inOperation(boolean inOperation) {
        _inOperation = inOperation;
    }
}

