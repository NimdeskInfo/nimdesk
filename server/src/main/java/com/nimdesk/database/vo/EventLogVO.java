/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.database.vo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.nimdesk.model.EventLog;

@Entity
@Table(name="eventlogs")
@Cache(usage=CacheConcurrencyStrategy.READ_WRITE)
@NamedQueries({@NamedQuery(name=EventLogVO.FIND_BY_TYPE_AND_UUID,
                           query="from EventLogVO e where e.targetType=:objType and e.targetUuid=:objUuid "),
               @NamedQuery(name=EventLogVO.FIND_BY_TYPE,
                           query="from EventLogVO e where e.targetType=:objType"),
               @NamedQuery(name=EventLogVO.DELETE_OLD_EVENTLOGS,
                           query="delete from EventLogVO e where e.dateTime<=:dateTime")})
public class EventLogVO extends ParameterSupport implements EventLog {

    public static final String FIND_BY_TYPE_AND_UUID = "EventLogVO.findByTypeAndUuid";
    public static final String FIND_BY_TYPE = "TaskVO.EventLogVO";

    public static final String DELETE_OLD_EVENTLOGS = "EventLogVO.deleteOldEventLogs";

    private static final long serialVersionUID = 1715827318064756638L;

    @Column(name="desc_template", nullable=false, length=BaseVO.NAME_LENGTH)
    private String descTemplate;

    @Enumerated(EnumType.STRING)
    @Column(name="severity", nullable=false, length=BaseVO.TYPE_LENGTH)
    private Severity severity;

    @Column(name="date_time", nullable=false)
    private long dateTime = 0;

    @Column(name="target_type", nullable=true, length=BaseVO.NAME_LENGTH)
    private String targetType;

    @Column(name="target_uuid", nullable=true, length=BaseVO.UUID_LENGTH)
    private String targetUuid;

    @Column(name="target_name", nullable=true, length=BaseVO.NAME_LENGTH)
    private String targetName;

    @Column(name="user", nullable=true, length=BaseVO.NAME_LENGTH)
    private String user;

    @Column(name="task_uuid", nullable=true, length=BaseVO.UUID_LENGTH)
    private String taskUuid;

    @Override
    public String getDescriptionTemplate() {
        return descTemplate;
    }

    @Override
    public void setDescriptionTemplate(String descTemplate) {
        this.descTemplate = descTemplate;
    }

    @Override
    public Severity getSeverity() {
        return severity;
    }

    @Override
    public void setSeverity(Severity severity) {
        this.severity = severity;
    }

    @Override
    public long getDateTime() {
        return dateTime;
    }

    @Override
    public void setDateTime(long dateTime) {
        this.dateTime = dateTime;
    }

    @Override
    public String getTargetType() {
        return targetType;
    }

    @Override
    public void setTargetType(String targetType) {
        this.targetType = targetType;
    }

    @Override
    public String getTargetUuid() {
        return targetUuid;
    }

    @Override
    public void setTargetUuid(String targetUuid) {
        this.targetUuid = targetUuid;
    }

    @Override
    public String getTargetName() {
        return targetName;
    }

    @Override
    public void setTargetName(String targetName) {
        this.targetName = targetName;
    }

    @Override
    public String getUser() {
        return user;
    }

    @Override
    public void setUser(String user) {
        this.user = user;
    }

    @Override
    public String getTaskUuid() {
        return taskUuid;
    }

    @Override
    public void setTaskUuid(String taskUuid) {
        this.taskUuid = taskUuid;
    }
}
