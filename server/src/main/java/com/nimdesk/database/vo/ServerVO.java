/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.database.vo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.nimdesk.model.Base;
import com.nimdesk.model.Server;

@Entity
@Table(name="servers")
@Cache(usage=CacheConcurrencyStrategy.READ_WRITE)
@NamedQueries({@NamedQuery(name=ServerVO.FIND_LOCAL_SERVER,
                           query="from ServerVO s where s.local=TRUE"),
               @NamedQuery(name=ServerVO.INVALIDATE_NON_LOCAL_SERVERS,
                           query="update ServerVO s set s.status='Disconnected' where s.local=FALSE")})
public class ServerVO extends ParameterSupport implements Server {

    public static final String FIND_LOCAL_SERVER = "ServerVO.findLocalServer";
    public static final String INVALIDATE_NON_LOCAL_SERVERS = "ServerVO.invalidateNonLocalServers";

    private static final String PARAM_VERSION = "version";

    private static final long serialVersionUID = -2307419760553326646L;

    @Column(name="cluster_name", nullable=false)
    private String clusterName;

    @Column(name="cluster_id", nullable=false)
    private String clusterId;

    @Column(name="cluster_address", nullable=false)
    private String clusterAddress = "";

    @Column(name="public_address", unique=true, nullable=false)
    private String publicAddress;

    @Column(name="private_address", nullable=true)
    private String privateAddress;

    @Column(name="vm_refId", nullable=false)
    private String vmRefId = "";

    @Column(name="is_local", nullable=false, columnDefinition="tinyint")
    private boolean local;

    @Enumerated(EnumType.STRING)
    @Column(name="status", nullable=false, length=BaseVO.STATUS_LENGTH)
    private Server.Status status;

    @Override
    public String getClusterName() {
        return clusterName;
    }

    @Override
    public void setClusterName(String clusterName) {
        this.clusterName = clusterName;
    }

    @Override
    public String getClusterId() {
        return clusterId;
    }

    @Override
    public void setClusterId(String clusterId) {
        this.clusterId = clusterId;
    }

    @Override
    public String getClusterAddress() {
        return clusterAddress;
    }

    @Override
    public void setClusterAddress(String clusterAddress) {
        this.clusterAddress = clusterAddress;
    }

    @Override
    public String getPublicAddress() {
        return publicAddress;
    }

    @Override
    public void setPublicAddress(String publicAddress) {
        this.publicAddress = publicAddress;
    }

    @Override
    public String getPrivateAddress() {
        return privateAddress;
    }

    @Override
    public void setPrivateAddress(String privateAddress) {
        this.privateAddress = privateAddress;
    }

    @Override
    public String getVmRefId() {
        return vmRefId;
    }

    @Override
    public void setVmRefId(String vmRefId) {
        this.vmRefId = vmRefId;
    }

    @Override
    public String getVersion() {
        return getParameter(PARAM_VERSION);
    }

    @Override
    public void setVersion(String version) {
        setParameter(PARAM_VERSION, version);
    }

    @Override
    public boolean isLocal() {
        return local;
    }

    @Override
    public void setLocal(boolean local) {
        this.local = local;
    }

    @Override
    public Status getStatus() {
        return status;
    }

    @Override
    public void setStatus(Status status) {
        Server.Status oldStatus = this.status;
        this.status = status;

        trackPropertyChange(Base.PROP_STATUS, oldStatus, status);
    }
}
