/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.database.vo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.nimdesk.model.Storage;

@Entity
@Table(name="storages",
       uniqueConstraints={@UniqueConstraint(columnNames={"host_uuid", "refId"})})
@Cache(usage=CacheConcurrencyStrategy.READ_WRITE)
public class StorageVO extends ParameterSupport implements Storage {

    private static final long serialVersionUID = 3062461340318918253L;

    private static final String PARAM_CAPACITY = "capacity";
    private static final String PARAM_FREESPACE = "free";

    @Column(name="name", nullable=false)
    private String name;

    @Column(name="host_uuid", nullable=false, length=BaseVO.UUID_LENGTH)
    private String hostUuid;

    @Column(name="refId", nullable=false)
    private String refId;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getHostUuid() {
        return hostUuid;
    }

    @Override
    public void setHostUuid(String hostUuid) {
        this.hostUuid = hostUuid;
    }

    @Override
    public String getRefId() {
        return refId;
    }

    @Override
    public void setRefId(String refId) {
        this.refId = refId;
    }

    @Override
    public long getCapacity() {
        return getParameterLong(PARAM_CAPACITY, 0);
    }

    @Override
    public void setCapacity(long capacity) {
        setParameterLong(PARAM_CAPACITY, capacity);
    }

    @Override
    public long getFreeSpace() {
        return getParameterLong(PARAM_FREESPACE, 0);
    }

    @Override
    public void setFreeSpace(long freeSpace) {
        setParameterLong(PARAM_FREESPACE, freeSpace);
    }
}

