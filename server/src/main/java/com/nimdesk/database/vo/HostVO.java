/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.database.vo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.nimdesk.model.Base;
import com.nimdesk.model.Host;
import com.nimdesk.model.HypervisorType;

@Entity
@Table(name="hosts")
@Cache(usage=CacheConcurrencyStrategy.READ_WRITE)
@NamedQueries({@NamedQuery(name=HostVO.FIND_LOCAL_HOST,
                           query="from HostVO h where h.local=TRUE"),
               @NamedQuery(name=HostVO.INVALIDATE_NON_LOCAL_HOSTS,
                           query="update HostVO h set h.status='Disconnected' where h.local=FALSE")})
public class HostVO extends ParameterSupport implements Host {

    public static final String FIND_LOCAL_HOST = "HostVO.findLocalHost";
    public static final String INVALIDATE_NON_LOCAL_HOSTS = "HostVO.invalidateNonLocalHosts";

    private static final String PARAM_USERNAME = "username";
    private static final String PARAM_PASSWORD = "password";
    private static final String PARAM_REFID = "refId";
    private static final String PARAM_VMNETWORK_REFID = "vmNetworkRefId";
    private static final String PARAM_CBRC_ENABLED = "cbrcEnabled";
    private static final String PARAM_CPU_CAPACITY = "cpu";
    private static final String PARAM_MEMORY_CAPACITY = "mem";
    private static final String PARAM_ESX_UUID = "esxUuid";
    private static final String PARAM_VCENTER_IP = "vcIp";
    private static final String PARAM_VCENTER_ADDRESS = "vcAddr";
    private static final String PARAM_VCENTER_USERNAME = "vcUsername";
    private static final String PARAM_VCENTER_PASSWORD = "vcPassword";
    private static final String PARAM_SHARED_DS_NAME = "sharedDsName";
    private static final String PARAM_SHARED_DS_REFID = "sharedDsRefId";
    private static final String PARAM_SHARED_DS_FOR_ALL = "sharedDsAll";

    private static final long serialVersionUID = 79310819338519949L;

    @Enumerated(EnumType.STRING)
    @Column(name="type", nullable=false, length=BaseVO.TYPE_LENGTH)
    private HypervisorType type;

    @Column(name="address", nullable=false)
    private String address;

    @Column(name="name", nullable=false)
    private String name;

    @Column(name="server_uuid", nullable=false, length=BaseVO.UUID_LENGTH)
    private String serverUuid;

    @Column(name="vm_network", nullable=false)
    private String vmNetwork;

    @Column(name="is_local", nullable=false, columnDefinition="tinyint")
    private boolean local;

    @Enumerated(EnumType.STRING)
    @Column(name="status", nullable=false, length=BaseVO.STATUS_LENGTH)
    private Host.Status status;

    @Override
    public HypervisorType getType() {
        return type;
    }

    @Override
    public void setType(HypervisorType type) {
        this.type = type;
    }

    @Override
    public String getAddress() {
        return address;
    }

    @Override
    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String getUsername() {
        return getParameter(PARAM_USERNAME);
    }

    @Override
    public void setUsername(String username) {
        setParameter(PARAM_USERNAME, username);
    }

    @Override
    public String getPassword() {
        return getParameter(PARAM_PASSWORD);
    }

    @Override
    public void setPassword(String password) {
        setParameter(PARAM_PASSWORD, password);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getServerUuid() {
        return serverUuid;
    }

    @Override
    public void setServerUuid(String serverUuid) {
        this.serverUuid = serverUuid;
    }

    @Override
    public String getRefId() {
        return getParameter(PARAM_REFID);
    }

    @Override
    public void setRefId(String refId) {
        setParameter(PARAM_REFID, refId);
    }

    @Override
    public String getVmNetwork() {
        return vmNetwork;
    }

    @Override
    public void setVmNetwork(String vmNetwork) {
        this.vmNetwork = vmNetwork;
    }

    @Override
    public String getVmNetworkRefId() {
        return getParameter(PARAM_VMNETWORK_REFID);
    }

    @Override
    public void setVmNetworkRefId(String vmNetworkRefId) {
        setParameter(PARAM_VMNETWORK_REFID, vmNetworkRefId);
    }

    @Override
    public boolean isCbrcEnabled() {
        return getParameterBoolean(PARAM_CBRC_ENABLED, false);
    }

    @Override
    public void setCbrcEnabled(boolean cbrcEnabled) {
        setParameterBoolean(PARAM_CBRC_ENABLED, cbrcEnabled, false);
    }

    @Override
    public boolean isLocal() {
        return local;
    }

    @Override
    public void setLocal(boolean local) {
        this.local = local;
    }

    @Override
    public Host.Status getStatus() {
        return status;
    }

    @Override
    public void setStatus(Host.Status status) {
        Host.Status oldStatus = this.status;
        this.status = status;

        trackPropertyChange(Base.PROP_STATUS, oldStatus, status);
    }

    @Override
    public int getCpuCapacityMHz() {
        return getParameterInt(PARAM_CPU_CAPACITY, -1);
    }

    @Override
    public void setCpuCapacityMHz(int cpuMHz) {
        if (cpuMHz < 0) {
            removeParameter(PARAM_CPU_CAPACITY);
        } else {
            setParameterInt(PARAM_CPU_CAPACITY, cpuMHz);
        }
    }

    @Override
    public int getMemoryCapacityMB() {
        return getParameterInt(PARAM_MEMORY_CAPACITY, -1);
    }

    @Override
    public void setMemoryCapacityMB(int memoryMB) {
        if (memoryMB < 0) {
            removeParameter(PARAM_MEMORY_CAPACITY);
        } else {
            setParameterInt(PARAM_MEMORY_CAPACITY, memoryMB);
        }
    }

    @Override
    public String getEsxUuid() {
        return getParameter(PARAM_ESX_UUID);
    }

    @Override
    public void setEsxUuid(String esxUuid) {
        setParameter(PARAM_ESX_UUID, esxUuid);
    }

    @Override
    public String getVCenterIp() {
        return getParameter(PARAM_VCENTER_IP);
    }

    @Override
    public void setVCenterIp(String vCenterIp) {
        setParameter(PARAM_VCENTER_IP, vCenterIp);
    }

    @Override
    public String getVCenterAddress() {
        return getParameter(PARAM_VCENTER_ADDRESS);
    }

    @Override
    public void setVCenterAddress(String vCenterAddress) {
        setParameter(PARAM_VCENTER_ADDRESS, vCenterAddress);
    }

    @Override
    public String getVCenterUsername() {
        return getParameter(PARAM_VCENTER_USERNAME);
    }

    @Override
    public void setVCenterUsername(String vCenterUsername) {
        setParameter(PARAM_VCENTER_USERNAME, vCenterUsername);
    }

    @Override
    public String getVCenterPassword() {
        return getParameter(PARAM_VCENTER_PASSWORD);
    }

    @Override
    public void setVCenterPassword(String vCenterPassword) {
        setParameter(PARAM_VCENTER_PASSWORD, vCenterPassword);
    }

    @Override
    public String getSharedDatastoreName() {
        return getParameter(PARAM_SHARED_DS_NAME);
    }

    @Override
    public void setSharedDatastoreName(String sharedDatastoreName) {
        setParameter(PARAM_SHARED_DS_NAME, sharedDatastoreName);
    }

    @Override
    public String getSharedDatastoreRefId() {
        return getParameter(PARAM_SHARED_DS_REFID);
    }

    @Override
    public void setSharedDatastoreRefId(String sharedDatastoreRefId) {
        setParameter(PARAM_SHARED_DS_REFID, sharedDatastoreRefId);
    }

    @Override
    public boolean isSharedDatastoreForAllDesktops() {
        return getParameterBoolean(PARAM_SHARED_DS_FOR_ALL, false);
    }

    @Override
    public void setSharedDatastoreForAllDesktops(boolean sharedDatastoreForAllDesktops) {
        setParameterBoolean(PARAM_SHARED_DS_FOR_ALL, sharedDatastoreForAllDesktops, false);
    }
}

