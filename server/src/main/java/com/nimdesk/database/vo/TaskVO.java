/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.database.vo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.nimdesk.model.Task;

@Entity
@Table(name="tasks")
@Cache(usage=CacheConcurrencyStrategy.READ_WRITE)
@NamedQueries({@NamedQuery(name=TaskVO.FIND_BY_TYPE_AND_UUID,
                           query="from TaskVO t where t.targetType=:objType and t.targetUuid=:objUuid " +
                                   "or t.associateType=:objType and t.associateUuid=:objUuid"),
               @NamedQuery(name=TaskVO.FIND_BY_TYPE,
                           query="from TaskVO t where t.targetType=:objType or t.associateType=:objType"),
               @NamedQuery(name=TaskVO.INVALIDATE_INPROGRESS_TASKS,
                           query="update TaskVO t set t.status='Failed' where t.serverUuid=:serverUuid and (t.status='Pending' or t.status='InProgress')"),
               @NamedQuery(name=TaskVO.DELETE_OLD_TASKS,
                           query="delete from TaskVO t where t.startTime<=:startTime")})
public class TaskVO extends ParameterSupport implements Task {

    private static final long serialVersionUID = -2258530453238715590L;

    public static final String FIND_BY_TYPE_AND_UUID = "TaskVO.findByTypeAndUuid";
    public static final String FIND_BY_TYPE = "TaskVO.findByType";

    public static final String INVALIDATE_INPROGRESS_TASKS = "TaskVO.invalidateInProgressTasks";

    public static final String DELETE_OLD_TASKS = "TaskVO.deleteOldTasks";

    private static final String PARAM_DETAILS = "details";

    @Enumerated(EnumType.ORDINAL)
    @Column(name="task_type", nullable=false)
    private Task.Type type;

    @Column(name="target_type", nullable=true, length=BaseVO.NAME_LENGTH)
    private String targetType;

    @Column(name="target_uuid", nullable=true, length=BaseVO.UUID_LENGTH)
    private String targetUuid;

    @Column(name="target_name", nullable=true, length=BaseVO.NAME_LENGTH)
    private String targetName;

    @Column(name="associate_type", nullable=true, length=BaseVO.NAME_LENGTH)
    private String associateType;

    @Column(name="associate_uuid", nullable=true, length=BaseVO.UUID_LENGTH)
    private String associateUuid;

    @Column(name="associate_name", nullable=true, length=BaseVO.NAME_LENGTH)
    private String associateName;

    @Column(name="start_time", nullable=false)
    private long startTime = 0;

    @Column(name="end_time", nullable=false)
    private long endTime = 0;

    @Enumerated(EnumType.STRING)
    @Column(name="status", nullable=false, length=BaseVO.STATUS_LENGTH)
    private Task.Status status;

    @Column(name="server_uuid", nullable=false, length=BaseVO.UUID_LENGTH)
    private String serverUuid;

    @Override
    public Task.Type getType() {
        return type;
    }

    @Override
    public void setType(Task.Type type) {
        this.type = type;
    }

    @Override
    public String getTargetType() {
        return targetType;
    }

    @Override
    public void setTargetType(String targetType) {
        this.targetType = targetType;
    }

    @Override
    public String getTargetUuid() {
        return targetUuid;
    }

    @Override
    public void setTargetUuid(String targetUuid) {
        this.targetUuid = targetUuid;
    }

    @Override
    public String getTargetName() {
        return targetName;
    }

    @Override
    public void setTargetName(String targetName) {
        this.targetName = targetName;
    }

    @Override
    public String getAssociateType() {
        return associateType;
    }

    @Override
    public void setAssociateType(String associateType) {
        this.associateType = associateType;
    }

    @Override
    public String getAssociateUuid() {
        return associateUuid;
    }

    @Override
    public void setAssociateUuid(String associateUuid) {
        this.associateUuid = associateUuid;
    }

    @Override
    public String getAssociateName() {
        return associateName;
    }

    @Override
    public void setAssociateName(String associateName) {
        this.associateName = associateName;
    }

    @Override
    public long getStartTime() {
        return startTime;
    }

    @Override
    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    @Override
    public long getEndTime() {
        return endTime;
    }

    @Override
    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    @Override
    public Task.Status getStatus() {
        return status;
    }

    @Override
    public void setStatus(Task.Status status) {
        this.status = status;
    }

    @Override
    public String getDetails() {
        return getParameter(PARAM_DETAILS);
    }

    @Override
    public void setDetails(String details) {
        setParameter(PARAM_DETAILS, details);
    }

    @Override
    public String getServerUuid() {
        return serverUuid;
    }

    @Override
    public void setServerUuid(String serverUuid) {
        this.serverUuid = serverUuid;
    }
}

