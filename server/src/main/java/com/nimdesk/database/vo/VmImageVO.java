/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.database.vo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.nimdesk.model.VmImage;

@Entity
@Table(name="vmimages")
@Cache(usage=CacheConcurrencyStrategy.READ_WRITE)
public class VmImageVO extends ParameterSupport implements VmImage {

    private static final long serialVersionUID = 7288948631114499941L;

    public static final String PROP_DISKIMAGE_GUID = "diskImageGuid";

    private static final String PARAM_DESCRIPTION = "description";
    private static final String PARAM_OS = "os";
    private static final String PARAM_IMAGE_SIZE = "imageSizeInKB";
    private static final String PARAM_VERSION = "version";
    private static final String PARAM_UPDATING_DISKIMAGE_UUID = "updatingDiskImageUuid";
    private static final String PARAM_UPDATING_DISKIMAGE_IP = "updatingDiskImageIp";
    private static final String PARAM_ORPHANED = "orphaned";

    @Column(name="name", nullable=false, length=BaseVO.NAME_LENGTH)
    private String name;

    @Column(name="update_time", nullable=false)
    private long updateTime = 0;

    @Column(name="disk_image_guid", nullable=true, length=BaseVO.UUID_LENGTH)
    private String currentDiskImageGuid;

    @Enumerated(EnumType.STRING)
    @Column(name="status", nullable=false, length=BaseVO.STATUS_LENGTH)
    private VmImage.Status status;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getDescription() {
        return getParameter(PARAM_DESCRIPTION);
    }

    @Override
    public void setDescription(String description) {
        setParameter(PARAM_DESCRIPTION, description);
    }

    @Override
    public String getOsName() {
        return getParameter(PARAM_OS);
    }

    @Override
    public void setOsName(String osName) {
        setParameter(PARAM_OS, osName);
    }

    @Override
    public long getImageSizeInKB() {
        return getParameterLong(PARAM_IMAGE_SIZE, 0);
    }

    @Override
    public void setImageSizeInKB(long imageSizeKB) {
        setParameterLong(PARAM_IMAGE_SIZE, imageSizeKB);
    }

    @Override
    public long getUpdateTime() {
        return updateTime;
    }

    @Override
    public void setUpdateTime(long updateTime) {
        long oldUpdateTime = this.updateTime;
        this.updateTime = updateTime;

        trackPropertyChange(PROP_UPDATETIME, oldUpdateTime, updateTime);
    }

    @Override
    public long getVersion() {
        return getParameterLong(PARAM_VERSION, 0);
    }

    @Override
    public void setVersion(long version) {
        setParameterLong(PARAM_VERSION, version);
    }

    @Override
    public String getCurrentDiskImageGuid() {
        return currentDiskImageGuid;
    }

    @Override
    public void setCurrentDiskImageGuid(String diskImageGuid) {
        String oldDiskImageGuid = this.currentDiskImageGuid;
        this.currentDiskImageGuid = diskImageGuid;

        trackPropertyChange(PROP_DISKIMAGE_GUID, oldDiskImageGuid, diskImageGuid);
    }

    @Override
    public Status getStatus() {
        return status;
    }

    @Override
    public void setStatus(Status status) {
        Status oldStatus = this.status;
        this.status = status;

        trackPropertyChange(PROP_STATUS, oldStatus, status);
    }

    @Override
    public String getUpdatingDiskImageUuid() {
        return getParameter(PARAM_UPDATING_DISKIMAGE_UUID);
    }

    @Override
    public void setUpdatingDiskImageUuid(String diskImageUuid) {
        setParameter(PARAM_UPDATING_DISKIMAGE_UUID, diskImageUuid);
    }

    @Override
    public String getUpdatingDiskImageIp() {
        return getParameter(PARAM_UPDATING_DISKIMAGE_IP);
    }

    @Override
    public void setUpdatingDiskImageIp(String diskImageIp) {
        setParameter(PARAM_UPDATING_DISKIMAGE_IP, diskImageIp);
    }

    @Override
    public boolean isOrphaned() {
        return getParameterBoolean(PARAM_ORPHANED, false);
    }

    @Override
    public void setOrphaned(boolean orphaned) {
        setParameterBoolean(PARAM_ORPHANED, orphaned, false);
    }
}
