/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.database.vo;

import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.nimdesk.model.Pool;

@Entity
@Table(name="pools")
@Cache(usage=CacheConcurrencyStrategy.READ_WRITE)
@NamedQueries({@NamedQuery(name=PoolVO.COUNT_BY_VMIMAGE_UUID,
                           query="select count(*) from PoolVO p where p.vmImageUuid=:vmImageUuid")})
public class PoolVO extends ParameterSupport implements Pool {

    private static final long serialVersionUID = -3023306380570560259L;

    public static final String COUNT_BY_VMIMAGE_UUID = "PoolVO.countByVmImageUuid";

    private static final String PARAM_SEQUENCE = "seq";
    private static final String PARAM_DESCRIPTION = "description";
    private static final String PARAM_OS = "os";
    private static final String PARAM_PREFIX = "prefix";
    private static final String PARAM_RESERVE_MEMORY = "reserveMem";
    private static final String PARAM_PRODUCTKEY = "productKey";
    private static final String PARAM_TIMEZONE = "timezone";
    private static final String PARAM_APPLICATIONS = "applications";
    private static final String PARAM_UDD_SIZE_MB = "uddSize";
    private static final String PARAM_UDD_DRIVE = "uddDrive";
    private static final String PARAM_BASE_DISKIMAGE_GUID = "baseDiskImageGuid";
    private static final String PARAM_INSTANTIATE_HOST_UUID = "instantiateHostUuid";
    private static final String PARAM_ORPHANED = "orphaned";

    @Enumerated(EnumType.STRING)
    @Column(name="pool_type", nullable=false, length=BaseVO.TYPE_LENGTH)
    private Pool.Type type;

    @Column(name="name", nullable=false, length=BaseVO.NAME_LENGTH)
    private String name;

    @Column(name="update_time", nullable=false)
    private long updateTime = 0;

    @Column(name="is_enabled", nullable=false, columnDefinition="tinyint")
    private boolean enabled;

    @Column(name="max_size", nullable=false)
    private long maxSize = 0;

    @Column(name="min_size", nullable=false)
    private long minSize = 0;

    @Column(name="cpu_count", nullable=false)
    private long cpuCount = 1;

    @Column(name="mem_mb", nullable=false)
    private long memoryMB = 256;

    @Column(name="is_domain", nullable=false, columnDefinition="tinyint")
    private boolean isDomain;

    @Column(name="domain_workgroup", nullable=false)
    private String domainOrWorkGroup;

    @Column(name="protocols", nullable=false)
    private long protocols = Pool.PROTOCOL_RDP;

    @Column(name="redirects", nullable=false)
    private long redirects = Pool.REDIRECT_NONE;

    @Column(name="vm_image_uuid", nullable=false, length=BaseVO.UUID_LENGTH)
    private String vmImageUuid;

    @Enumerated(EnumType.STRING)
    @Column(name="status", nullable=false, length=BaseVO.STATUS_LENGTH)
    private Pool.Status status;

    @Override
    public Type getType() {
        return type;
    }

    @Override
    public void setType(Type type) {
        this.type = type;
    }

    @Override
    public long getSequence() {
        return getParameterLong(PARAM_SEQUENCE, 1);
    }

    @Override
    public void setSequence(long sequence) {
        setParameterLong(PARAM_SEQUENCE, sequence);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getDescription() {
        return getParameter(PARAM_DESCRIPTION);
    }

    @Override
    public void setDescription(String description) {
        setParameter(PARAM_DESCRIPTION, description);
    }

    @Override
    public String getOsName() {
        return getParameter(PARAM_OS);
    }

    @Override
    public void setOsName(String osName) {
        setParameter(PARAM_OS, osName);
    }

    @Override
    public long getUpdateTime() {
        return updateTime;
    }

    @Override
    public void setUpdateTime(long updateTime) {
        // We have to guarantee the updateTime is always moving up.
        if (updateTime <= this.updateTime) {
            ++this.updateTime;
        } else {
            this.updateTime = updateTime;
        }
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    @Override
    public void setEnabled(boolean enabled) {
        boolean oldEnabled = this.enabled;
        this.enabled = enabled;

        trackPropertyChange(Pool.PROP_ENABLED, oldEnabled, enabled);
    }

    @Override
    public String getPrefix() {
        return getParameter(PARAM_PREFIX);
    }

    @Override
    public void setPrefix(String prefix) {
        //this.prefix = prefix;
        setParameter(PARAM_PREFIX, prefix);
    }

    @Override
    public long getMaxSize() {
        return maxSize;
    }

    @Override
    public void setMaxSize(long maxSize) {
        long oldMaxSize = this.maxSize;
        this.maxSize = maxSize;

        trackPropertyChange(Pool.PROP_MAXSIZE, oldMaxSize, maxSize);
    }

    @Override
    public long getMinSize() {
        return minSize;
    }

    @Override
    public void setMinSize(long minSize) {
        long oldMinSize = this.minSize;
        this.minSize = minSize;

        trackPropertyChange(Pool.PROP_MINSIZE, oldMinSize, minSize);
    }

    @Override
    public long getCpuCount() {
        return cpuCount;
    }

    @Override
    public void setCpuCount(long cpuCount) {
        this.cpuCount = cpuCount;
    }

    @Override
    public long getMemoryMB() {
        return memoryMB;
    }

    @Override
    public void setMemoryMB(long memoryMB) {
        this.memoryMB = memoryMB;
    }

    @Override
    public boolean isReserveMemory() {
        return getParameterBoolean(PARAM_RESERVE_MEMORY, false);
    }

    @Override
    public void setReserveMemory(boolean reserveMemory) {
        setParameterBoolean(PARAM_RESERVE_MEMORY, reserveMemory, false);
    }

    @Override
    public boolean isDomain() {
        return isDomain;
    }

    @Override
    public void setDomain(boolean isDomain) {
        this.isDomain = isDomain;
    }

    @Override
    public String getDomainOrWorkGroup() {
        return domainOrWorkGroup;
    }

    @Override
    public void setDomainOrWorkGroup(String domainOrWorkGroup) {
        this.domainOrWorkGroup = domainOrWorkGroup;
    }

    @Override
    public String getProductKey() {
        return getParameter(PARAM_PRODUCTKEY);
    }

    @Override
    public void setProductKey(String productKey) {
        setParameter(PARAM_PRODUCTKEY, productKey);
    }

    @Override
    public String getTimeZone() {
        return getParameter(PARAM_TIMEZONE);
    }

    @Override
    public void setTimeZone(String timezone) {
        setParameter(PARAM_TIMEZONE, timezone);
    }

    @Override
    public long getProtocols() {
        return protocols;
    }

    @Override
    public void setProtocols(long protocols) {
        this.protocols = protocols;
    }

    @Override
    public Long[] getAllProtocols() {
        ArrayList<Long> protocolList = new ArrayList<Long>();
        for (long proto : Pool.PROTOCOLS) {
            if ((proto & protocols) != 0) {
                protocolList.add(proto);
            }
        }

        return protocolList.toArray(new Long[protocolList.size()]);
    }

    @Override
    public void setAllProtocols(long... protocol) {
        long protocols = 0;
        for (long p : protocol) {
            protocols |= p;
        }
        setProtocols(protocols);
    }

    @Override
    public long getRedirects() {
        return redirects;
    }

    @Override
    public void setRedirects(long redirects) {
        this.redirects = redirects;
    }

    @Override
    public Long[] getAllRedirects() {
        ArrayList<Long> redirectList = new ArrayList<Long>();
        for (long r : Pool.REDIRECTS) {
            if ((r & redirects) != 0) {
                redirectList.add(r);
            }
        }

        return redirectList.toArray(new Long[redirectList.size()]);
    }

    @Override
    public void setAllRedirects(long... redirect) {
        long redirects = 0;
        for (long r : redirect) {
            redirects |= r;
        }
        setRedirects(redirects);
    }

    @Override
    public long getUserDataDiskSizeInMB() {
        return getParameterLong(PARAM_UDD_SIZE_MB, 0);
    }

    @Override
    public void setUserDataDiskSizeInMB(long userDataDiskSizeMB) {
        if (userDataDiskSizeMB <= 0) {
            removeParameter(PARAM_UDD_SIZE_MB);
        } else {
            setParameterLong(PARAM_UDD_SIZE_MB, userDataDiskSizeMB);
        }
    }

    @Override
    public String getUserDataDiskDriveLetter() {
        return getParameter(PARAM_UDD_DRIVE);
    }

    @Override
    public void setUserDataDiskDriveLetter(String driveLetter) {
        setParameter(PARAM_UDD_DRIVE, driveLetter);
    }

    @Override
    public String getVmImageUuid() {
        return vmImageUuid;
    }

    @Override
    public void setVmImageUuid(String vmImageUuid) {
        this.vmImageUuid = vmImageUuid;
    }

    @Override
    public String[] getApplicationUuids() {
        return getParameterStringArray(PARAM_APPLICATIONS);
    }

    @Override
    public void setApplicationUuids(String[] applicationUuids) {
        setParameterStringArray(PARAM_APPLICATIONS, applicationUuids);
    }

    @Override
    public Pool.Status getStatus() {
        return status;
    }

    @Override
    public void setStatus(Pool.Status status) {
        Pool.Status oldStatus = this.status;
        this.status = status;

        trackPropertyChange(Pool.PROP_STATUS, oldStatus, status);
    }

    @Override
    public String getBaseDiskImageGuid() {
        return getParameter(PARAM_BASE_DISKIMAGE_GUID);
    }

    @Override
    public void setBaseDiskImageGuid(String diskImageGuid) {
        String oldDiskImageGuid = getBaseDiskImageGuid();

        setParameter(PARAM_BASE_DISKIMAGE_GUID, diskImageGuid);

        trackPropertyChange(Pool.PROP_STATUS, oldDiskImageGuid, diskImageGuid);
    }

    @Override
    public String getInstantiateHostUuid() {
        return getParameter(PARAM_INSTANTIATE_HOST_UUID);
    }

    @Override
    public void setInstantiateHostUuid(String hostUuid) {
        setParameter(PARAM_INSTANTIATE_HOST_UUID, hostUuid);
    }

    @Override
    public boolean isOrphaned() {
        return getParameterBoolean(PARAM_ORPHANED, false);
    }

    @Override
    public void setOrphaned(boolean orphaned) {
        setParameterBoolean(PARAM_ORPHANED, orphaned, false);
    }
}

