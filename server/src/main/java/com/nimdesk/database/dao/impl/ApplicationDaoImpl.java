/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.database.dao.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.nimdesk.database.dao.ApplicationDao;
import com.nimdesk.database.vo.ApplicationVO;

@Repository("applicationDao")
public class ApplicationDaoImpl extends GenericDaoImpl<ApplicationVO, Long> implements ApplicationDao {
    private static final Logger LOGGER = Logger.getLogger(ApplicationDaoImpl.class);

    public ApplicationDaoImpl() {
        super(ApplicationVO.class);
    }

    @Override
    public List<ApplicationVO> findByVmImage(String vmImageUuid) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("findByVmImage(%s)", vmImageUuid));
        }

        return findByCriteria(null, Restrictions.eq("vmImageUuid", vmImageUuid));
    }
}
