/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.database.dao.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.nimdesk.database.dao.StorageDao;
import com.nimdesk.database.vo.StorageVO;

@Repository("storageDao")
public class StorageDaoImpl extends GenericDaoImpl<StorageVO, Long> implements StorageDao {
    private static final Logger LOGGER = Logger.getLogger(StorageDaoImpl.class);

    public StorageDaoImpl() {
        super(StorageVO.class);
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public List<StorageVO> findByHost(String hostUuid) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("findByHost(%s)", hostUuid));
        }

        return findByCriteria(null, Restrictions.eq("hostUuid", hostUuid));
    }

    @Override
    public List<StorageVO> findByHosts(String... hostUuid) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("findByHosts()"));
        }

        Disjunction disjunction = Restrictions.disjunction();
        for (String uuid : hostUuid) {
            disjunction.add(Restrictions.eq("hostUuid", uuid));
        }

        return findByCriteria(null, disjunction);
    }
}
