/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.database.dao.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.nimdesk.database.dao.PoolDao;
import com.nimdesk.database.vo.PoolVO;
import com.nimdesk.model.Pool;
import com.nimdesk.model.Pool.Status;

@Repository("poolDao")
public class PoolDaoImpl extends GenericDaoImpl<PoolVO, Long> implements PoolDao {
    private static final Logger LOGGER = Logger.getLogger(PoolDaoImpl.class);

    public PoolDaoImpl() {
        super(PoolVO.class);
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public Long countByVmImage(String vmImageUuid) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("countByVmImage(%s)", vmImageUuid));
        }

        return (Long) getSession()
                .getNamedQuery(PoolVO.COUNT_BY_VMIMAGE_UUID)
                .setString("vmImageUuid", vmImageUuid)
                .uniqueResult();
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public Long countByStatus(Pool.Status... status) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("countByStatus()");
        }

        if (status == null || status.length == 0) {
            return countAll();
        } else {
            Disjunction disjunction = Restrictions.disjunction();
            for (Pool.Status st : status) {
                disjunction.add(Restrictions.eq("status", st));
            }

            return countByCriteria(disjunction);
        }
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public List<PoolVO> findByStatus(Status... status) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("findByStatus()");
        }

        if (status == null || status.length == 0) {
            return findByCriteria(null);
        } else {
            Disjunction disjunction = Restrictions.disjunction();
            for (Pool.Status st : status) {
                disjunction.add(Restrictions.eq("status", st));
            }

            return findByCriteria(null, disjunction);
        }
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public List<PoolVO> findByVmImage(String vmImageUuid) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("findByVmImage(%s)", vmImageUuid));
        }

        return findByCriteria(null, Restrictions.eq("vmImageUuid", vmImageUuid));
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public PoolVO findByName(String poolName) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("findByName(%s)", poolName));
        }

        return findUniqueEntityByCriteria(Restrictions.eq("name", poolName));
    }
}

