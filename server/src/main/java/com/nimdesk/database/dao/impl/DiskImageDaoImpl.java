/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.database.dao.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.nimdesk.database.dao.DiskImageDao;
import com.nimdesk.database.vo.DiskImageVO;
import com.nimdesk.model.DiskImage;

@Repository("diskImageDao")
public class DiskImageDaoImpl extends GenericDaoImpl<DiskImageVO, Long> implements DiskImageDao {
    private static final Logger LOGGER = Logger.getLogger(DiskImageDaoImpl.class);

    public DiskImageDaoImpl() {
        super(DiskImageVO.class);
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public List<DiskImageVO> findByGuid(String guid, DiskImage.Status... status) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("findByGuid(%s)", guid));
        }

        if (status == null || status.length == 0) {
            return findByCriteria(null, Restrictions.eq("guid", guid));
        } else {
            Disjunction disjunction = Restrictions.disjunction();
            for (DiskImage.Status st : status) {
                disjunction.add(Restrictions.eq("status", st));
            }

            return findByCriteria(null, Restrictions.eq("guid", guid), disjunction);
        }
    }

    @Override
    public DiskImageVO findByGuidOnHost(String guid, String hostUuid) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("findByGuidOnHost(%s, %s)", guid, hostUuid));
        }

        return findUniqueEntityByCriteria(Restrictions.eq("guid", guid),
                Restrictions.eq("hostUuid", hostUuid));
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public List<DiskImageVO> findByOwner(String ownerUuid, DiskImage.Status... status) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("findByOwner(%s)", ownerUuid));
        }

        if (status == null || status.length == 0) {
            return findByCriteria(null, Restrictions.eq("ownerUuid", ownerUuid));
        } else {
            Disjunction disjunction = Restrictions.disjunction();
            for (DiskImage.Status st : status) {
                disjunction.add(Restrictions.eq("status", st));
            }

            return findByCriteria(null, Restrictions.eq("ownerUuid", ownerUuid), disjunction);
        }
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public List<DiskImageVO> findByOwnerOnHost(String ownerUuid, String hostUuid) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("findByOwnerOnHost(%s, %s)", ownerUuid, hostUuid));
        }

        return findByCriteria(null, Restrictions.eq("ownerUuid", ownerUuid),
                Restrictions.eq("hostUuid", hostUuid));
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public List<DiskImageVO> findByHosts(String... hostUuid) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("findByHosts()"));
        }

        Disjunction disjunction = Restrictions.disjunction();
        for (String uuid : hostUuid) {
            disjunction.add(Restrictions.eq("hostUuid", uuid));
        }

        return findByCriteria(null, disjunction);
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public List<DiskImageVO> findByHost(String hostUuid, DiskImage.Status... status) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("findByHosts(%s)", hostUuid));
        }

        if (status == null || status.length == 0) {
            return findByCriteria(null, Restrictions.eq("hostUuid", hostUuid));
        } else {
            Disjunction disjunction = Restrictions.disjunction();
            for (DiskImage.Status st : status) {
                disjunction.add(Restrictions.eq("status", st));
            }

            return findByCriteria(null, Restrictions.eq("hostUuid", hostUuid), disjunction);
        }
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public List<DiskImageVO> findByName(String name) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("findByName(%s)", name));
        }

        return findByCriteria(null, Restrictions.eq("name", name));
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public Long countByOwner(String ownerUuid) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("countByOwner(%s)", ownerUuid));
        }

        return countByCriteria(Restrictions.eq("ownerUuid", ownerUuid));
    }


    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    public void invalidateNonLocalDiskImages(String hostUuid) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("invalidateNonLocalDiskImages()");
        }

        getSession()
            .getNamedQuery(DiskImageVO.INVALIDATE_NON_LOCAL_IMAGES)
            .setString("hostUuid", hostUuid)
            .executeUpdate();
    }
}

