/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.database.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.StringType;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.nimdesk.database.dao.UserDao;
import com.nimdesk.database.vo.UserVO;
import com.nimdesk.model.User;

@Repository("userDao")
public class UserDaoImpl extends GenericDaoImpl<UserVO, Long> implements UserDao {
    private static final Logger LOGGER = Logger.getLogger(UserDaoImpl.class);

    public UserDaoImpl() {
        super(UserVO.class);
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public UserVO findByDistinguishedName(String distinguishedName) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("findByDistinguishedName(%s)", distinguishedName));
        }

        return (UserVO) getSession()
                .getNamedQuery(UserVO.FIND_BY_DN)
                .setString("distinguishedName", distinguishedName)
                .uniqueResult();
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public UserVO findByLogonNameDomain(String logonName, String domain) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("findByLogonNameDomain(%s, %s)", logonName, domain));
        }

        return (UserVO) getSession()
                .getNamedQuery(UserVO.FIND_BY_LOGONNAME_DOMAIN)
                .setString("logonName", logonName)
                .setString("domain", domain)
                .uniqueResult();
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public List<UserVO> findByNameFilter(boolean includeUsers, boolean includeGroups,
            String prefix, String domain) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("findByNameFilter()");
        }

        ArrayList<Criterion> criterionList = new ArrayList<Criterion>();
        criterionList.add(Restrictions.sqlRestriction("lower({alias}.name) like lower(?)",
                prefix + "%", new StringType()));
        criterionList.add(Restrictions.sqlRestriction("lower({alias}.domain)=lower(?)",
                domain, new StringType()));
        if (includeUsers && includeGroups) {
            // No need to add criterion
        } else if (includeUsers) {
            criterionList.add(Restrictions.eq("type", User.Type.User));
        } else if (includeGroups) {
            criterionList.add(Restrictions.eq("type", User.Type.Group));
        } else {
            return null;
        }

        return findByCriteria(null, criterionList.toArray(new Criterion[0]));
    }

    @Override
    @SuppressWarnings("unchecked")
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public List<String> findDomains() {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("findDomains()");
        }

        return getSession()
                .getNamedQuery(UserVO.FIND_DOMAINS)
                .list();
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public List<UserVO> findAllUsers() {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("findAllUsers()");
        }

        return findByCriteria(null, Restrictions.eq("type", User.Type.User));
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public List<UserVO> findAllGroups(String domain) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("findAllGroups(%s)", domain));
        }

        return findByCriteria(null, Restrictions.eq("type", User.Type.Group),
                Restrictions.sqlRestriction("lower({alias}.domain)=lower(?)",
                        domain, new StringType()));
    }
}
