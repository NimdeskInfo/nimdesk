/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.database.dao.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.SimpleExpression;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.nimdesk.database.dao.EventLogDao;
import com.nimdesk.database.vo.EventLogVO;
import com.nimdesk.model.Pool;
import com.nimdesk.model.VmImage;
import com.nimdesk.model.VmInstance;
import com.nimdesk.util.Pagination;

@Repository("eventLogDao")
public class EventLogDaoImpl extends GenericDaoImpl<EventLogVO, Long> implements EventLogDao {
    private static final Logger LOGGER = Logger.getLogger(EventLogDaoImpl.class);

    public EventLogDaoImpl() {
        super(EventLogVO.class);
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public Long countByPool(String poolUuid) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("countByPool(%s)", poolUuid));
        }

        SimpleExpression targetType = Restrictions.eq("targetType", Pool.class.getSimpleName());
        SimpleExpression targetUuid = Restrictions.eq("targetUuid", poolUuid);

        return countByCriteria(targetType, targetUuid);
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public Long countAllPools() {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("countAllPools");
        }

        return countByCriteria(Restrictions.eq("targetType", Pool.class.getSimpleName()));
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public Long countByVmImage(String vmImageUuid) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("countByVmImage(%s)", vmImageUuid));
        }

        SimpleExpression targetType = Restrictions.eq("targetType", VmImage.class.getSimpleName());
        SimpleExpression targetUuid = Restrictions.eq("targetUuid", vmImageUuid);

        return countByCriteria(targetType, targetUuid);
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public Long countAllVmImages() {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("countAllVmImages");
        }

        return countByCriteria(Restrictions.eq("targetType", VmImage.class.getSimpleName()));
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public Long countByVmInstance(String vmInstanceUuid) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("countByVmInstance(%s)", vmInstanceUuid));
        }

        SimpleExpression targetType = Restrictions.eq("targetType", VmInstance.class.getSimpleName());
        SimpleExpression targetUuid = Restrictions.eq("targetUuid", vmInstanceUuid);

        return countByCriteria(targetType, targetUuid);
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public Long countAllVmInstances() {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("countAllVmInstances");
        }

        return countByCriteria(Restrictions.eq("targetType", VmInstance.class.getSimpleName()));
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public List<EventLogVO> findByPool(String poolUuid, Pagination pagination) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("findByPool(%s)", poolUuid));
        }

        SimpleExpression targetType = Restrictions.eq("targetType", Pool.class.getSimpleName());
        SimpleExpression targetUuid = Restrictions.eq("targetUuid", poolUuid);

        return findByCriteria(pagination, targetType, targetUuid);
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public List<EventLogVO> findAllPools(Pagination pagination) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("findAllPools");
        }

        return findByCriteria(pagination,
                Restrictions.eq("targetType", Pool.class.getSimpleName()));
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public List<EventLogVO> findByVmImage(String vmImageUuid, Pagination pagination) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("findByVmImage(%s)", vmImageUuid));
        }

        SimpleExpression targetType = Restrictions.eq("targetType", VmImage.class.getSimpleName());
        SimpleExpression targetUuid = Restrictions.eq("targetUuid", vmImageUuid);

        return findByCriteria(pagination, targetType, targetUuid);
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public List<EventLogVO> findAllVmImages(Pagination pagination) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("findAllVmImages");
        }

        return findByCriteria(pagination,
                Restrictions.eq("targetType", VmImage.class.getSimpleName()));
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public List<EventLogVO> findByVmInstance(String vmInstanceUuid, Pagination pagination) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("findByVmInstance(%s)", vmInstanceUuid));
        }

        SimpleExpression targetType = Restrictions.eq("targetType", VmInstance.class.getSimpleName());
        SimpleExpression targetUuid = Restrictions.eq("targetUuid", vmInstanceUuid);

        return findByCriteria(pagination, targetType, targetUuid);
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public List<EventLogVO> findAllVmInstances(Pagination pagination) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("findAllVmInstances");
        }

        return findByCriteria(pagination,
                Restrictions.eq("targetType", VmInstance.class.getSimpleName()));
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public List<EventLogVO> findAllByTime(boolean before, long time) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("findAllByTime");
        }


        return findByCriteria(null, before ? Restrictions.le("dateTime", time) : Restrictions.ge("dateTime", time));
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    public void deleteOldEventLogs(long time) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("deleteOldEventLogs(%d)", time));
        }

        getSession()
            .getNamedQuery(EventLogVO.DELETE_OLD_EVENTLOGS)
            .setLong("dateTime", time)
            .executeUpdate();
    }
}
