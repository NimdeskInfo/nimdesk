/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.database.dao;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.nimdesk.database.vo.VmInstanceVO;
import com.nimdesk.model.VmInstance;
import com.nimdesk.model.VmInstance.Status;
import com.nimdesk.util.Pagination;

public interface VmInstanceDao extends GenericDao<VmInstanceVO, Long> {

    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    List<VmInstanceVO> findByDiskImage(String diskImageUuid, Pagination pagination);

    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    List<VmInstanceVO> findByPool(String poolUuid, Pagination pagination);

    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    List<VmInstanceVO> findUnassignedByPool(String poolUuid, Pagination pagination);

    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    List<VmInstanceVO> findByStatus(Pagination pagination, Status... status);

    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    List<VmInstanceVO> findByPoolAndStatus(Pagination pagination, String poolUuid, Status... status);

    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    List<VmInstanceVO> findByPoolAndStatusOnHost(String hostUuid, String poolUuid, Status... status);

    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    List<VmInstanceVO> findByUser(String userUuid, Pagination pagination);

    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    List<VmInstanceVO> findInPoolByUser(String poolUuid, String userUuid);

    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    List<VmInstanceVO> findInPoolWithNoUser(String poolUuid);

    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    List<VmInstanceVO> findByHosts(String... hostUuid);

    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    List<VmInstanceVO> findByPoolAndHosts(String poolUuid, String... hostUuid);

    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    VmInstanceVO findByHostVmRefId(String hostUuid, String vmRefId);

    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    Long countByDiskImage(String diskImageUuid);

    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    Long countByPoolAndStatus(String poolUuid, VmInstance.Status... status);

    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    Long countByPoolAndStatusOnHost(String hostUuid, String poolUuid, VmInstance.Status... status);

    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    Long countByUser(String userUuid);

    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    Long countAllInSession();

    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    Long countByHostAndStatus(String hostUuid, VmInstance.Status... status);

    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    Long countByStatus(VmInstance.Status... status);

    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void invalidateNonLocalVmInstances(String hostUuid);
}

