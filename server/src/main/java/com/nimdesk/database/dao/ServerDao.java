/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.database.dao;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.nimdesk.database.vo.ServerVO;
import com.nimdesk.model.Server;

public interface ServerDao extends GenericDao<ServerVO, Long> {

    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    ServerVO findLocalServer();

    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    List<ServerVO> findByStatus(Server.Status... status);

    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    ServerVO findByPublicAddress(String publicAddress);

    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    ServerVO findByClusterAddress(String clusterAddress);

    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    Long countByStatus(Server.Status... status);

    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void invalidateNonLocalServers();
}
