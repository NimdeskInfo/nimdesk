/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.database.dao;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.nimdesk.database.vo.DiskImageVO;
import com.nimdesk.model.DiskImage;

public interface DiskImageDao extends GenericDao<DiskImageVO, Long> {

    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    List<DiskImageVO> findByGuid(String guid, DiskImage.Status... status);

    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    DiskImageVO findByGuidOnHost(String guid, String hostUuid);

    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    List<DiskImageVO> findByOwner(String ownerUuid, DiskImage.Status... status);

    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    List<DiskImageVO> findByOwnerOnHost(String ownerUuid, String hostUuid);

    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    List<DiskImageVO> findByHosts(String... hostUuid);

    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    List<DiskImageVO> findByHost(String hostUuid, DiskImage.Status... status);

    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    List<DiskImageVO> findByName(String name);

    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    Long countByOwner(String ownerUuid);

    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void invalidateNonLocalDiskImages(String hostUuid);
}

