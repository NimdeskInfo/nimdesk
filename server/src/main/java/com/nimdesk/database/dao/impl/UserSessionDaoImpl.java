/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.database.dao.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.nimdesk.database.dao.UserSessionDao;
import com.nimdesk.database.vo.UserSessionVO;

@Repository("userSessionDao")
public class UserSessionDaoImpl extends GenericDaoImpl<UserSessionVO, Long> implements UserSessionDao {
    private static final Logger LOGGER = Logger.getLogger(UserSessionDaoImpl.class);

    public UserSessionDaoImpl() {
        super(UserSessionVO.class);
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public List<UserSessionVO> findActiveByVmInstance(String vmInstanceUuid) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("UserSessionVO");
        }

        return findByCriteria(null,
                Restrictions.eq("vmInstanceUuid", vmInstanceUuid),
                Restrictions.eq("endTime", 0L));
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public List<UserSessionVO> findByHost(String hostUuid) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("findByHost");
        }

        return findByCriteria(null, Restrictions.eq("hostUuid", hostUuid));
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public List<UserSessionVO> findByHostAndTime(String hostUuid, boolean before, long time) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("findByServerAndTime");
        }

        return findByCriteria(null, Restrictions.eq("hostUuid", hostUuid),
                Restrictions.gt("endTime", 0L),
                before ? Restrictions.le("endTime", time) : Restrictions.ge("endTime", time));
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    public void invalidateActiveSessions(String hostUuid) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("invalidateActiveSessions(%s)", hostUuid));
        }

        getSession()
            .getNamedQuery(UserSessionVO.INVALIDATE_ACTIVE_SESSIONS)
            .setLong("endTime", System.currentTimeMillis())
            .setString("hostUuid", hostUuid)
            .executeUpdate();
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    public void deleteOldUserSessions(long time) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("deleteOldUserSessions(%d)", time));
        }

        getSession()
            .getNamedQuery(UserSessionVO.DELETE_OLD_USERSESSIONS)
            .setLong("startTime", time)
            .executeUpdate();
    }
}
