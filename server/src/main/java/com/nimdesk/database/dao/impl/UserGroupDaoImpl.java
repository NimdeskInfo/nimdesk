/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.database.dao.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.nimdesk.database.dao.UserGroupDao;
import com.nimdesk.database.vo.UserGroupVO;

@Repository("userGroupDao")
public class UserGroupDaoImpl extends GenericDaoImpl<UserGroupVO, Long> implements UserGroupDao {
    private static final Logger LOGGER = Logger.getLogger(UserGroupDaoImpl.class);

    public UserGroupDaoImpl() {
        super(UserGroupVO.class);
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public UserGroupVO findByUserGroup(String userUuid, String groupUuid) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("findByUserGroup(%s, %s)", userUuid, groupUuid));
        }

        return (UserGroupVO) getSession()
                .getNamedQuery(UserGroupVO.FIND_BY_USER_GROUP)
                .setString("userUuid", userUuid)
                .setString("groupUuid", groupUuid)
                .uniqueResult();
    }

    @Override
    @SuppressWarnings("unchecked")
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public List<UserGroupVO> findByUser(String userUuid) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("findByUser(%s)", userUuid));
        }

        return getSession()
                .getNamedQuery(UserGroupVO.FIND_BY_USER_UUID)
                .setString("userUuid", userUuid)
                .list();
    }

    @Override
    @SuppressWarnings("unchecked")
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public List<UserGroupVO> findByGroup(String groupUuid) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("findByGroup(%s)", groupUuid));
        }

        return getSession()
                .getNamedQuery(UserGroupVO.FIND_BY_GROUP_UUID)
                .setString("groupUuid", groupUuid)
                .list();
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public Long countGroupsByUser(String userUuid) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("countGroupsByUser(%s)", userUuid));
        }

        return countByCriteria(Restrictions.eq("userUuid", userUuid));
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public Long countUsersByGroup(String groupUuid) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("countUsersByGroup(%s)", groupUuid));
        }

        return countByCriteria(Restrictions.eq("groupUuid", groupUuid));
    }
}
