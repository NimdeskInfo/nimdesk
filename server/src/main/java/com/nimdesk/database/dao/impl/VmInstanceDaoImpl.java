/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.database.dao.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.nimdesk.database.dao.VmInstanceDao;
import com.nimdesk.database.vo.VmInstanceVO;
import com.nimdesk.model.VmInstance;
import com.nimdesk.model.VmInstance.Status;
import com.nimdesk.util.Pagination;

@Repository("vmInstanceDao")
public class VmInstanceDaoImpl extends GenericDaoImpl<VmInstanceVO, Long> implements VmInstanceDao {
    private static final Logger LOGGER = Logger.getLogger(VmInstanceDaoImpl.class);

    public VmInstanceDaoImpl() {
        super(VmInstanceVO.class);
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public List<VmInstanceVO> findByDiskImage(String diskImageUuid, Pagination pagination) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("findByDiskImage(%s)", diskImageUuid));
        }

        return findByCriteria(pagination, Restrictions.eq("diskImageUuid", diskImageUuid));
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public List<VmInstanceVO> findByPool(String poolUuid, Pagination pagination) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("findByPool(%s)", poolUuid));
        }

        return findByCriteria(pagination, Restrictions.eq("poolUuid", poolUuid));
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public List<VmInstanceVO> findUnassignedByPool(String poolUuid, Pagination pagination) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("findUnassignedByPool(%s)", poolUuid));
        }


        return findByCriteria(pagination, Restrictions.eq("poolUuid", poolUuid),
                Restrictions.eq("userUuid", ""));
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public List<VmInstanceVO> findByStatus(Pagination pagination, Status... status) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("findByStatus()"));
        }

        if (status == null || status.length == 0) {
            return findByCriteria(pagination);
        } else {
            Disjunction disjunction = Restrictions.disjunction();
            for (VmInstance.Status st : status) {
                disjunction.add(Restrictions.eq("status", st));
            }

            return findByCriteria(pagination, disjunction);
        }
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public List<VmInstanceVO> findByPoolAndStatus(Pagination pagination, String poolUuid, Status... status) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("findByPoolAndStatus(%s)", poolUuid));
        }

        if (status == null || status.length == 0) {
            return findByCriteria(pagination, Restrictions.eq("poolUuid", poolUuid));
        } else {
            Disjunction disjunction = Restrictions.disjunction();
            for (VmInstance.Status st : status) {
                disjunction.add(Restrictions.eq("status", st));
            }

            return findByCriteria(pagination,
                    Restrictions.eq("poolUuid", poolUuid), disjunction);
        }
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public List<VmInstanceVO> findByPoolAndStatusOnHost(String hostUuid, String poolUuid, Status... status) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("findByPoolAndStatusOnHost(%s, %s)", hostUuid, poolUuid));
        }

        if (status == null || status.length == 0) {
            return findByCriteria(null, Restrictions.eq("hostUuid", hostUuid), Restrictions.eq("poolUuid", poolUuid));
        } else {
            Disjunction disjunction = Restrictions.disjunction();
            for (VmInstance.Status st : status) {
                disjunction.add(Restrictions.eq("status", st));
            }

            return findByCriteria(null, Restrictions.eq("hostUuid", hostUuid),
                    Restrictions.eq("poolUuid", poolUuid), disjunction);
        }
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public List<VmInstanceVO> findByUser(String userUuid, Pagination pagination) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("findByUser(%s)", userUuid));
        }

        return findByCriteria(pagination, Restrictions.eq("userUuid", userUuid));
    }

    @Override
    @SuppressWarnings("unchecked")
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public List<VmInstanceVO> findInPoolByUser(String poolUuid, String userUuid) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("findInPoolByUser(%s, %s)", poolUuid, userUuid));
        }

        return getSession()
                .getNamedQuery(VmInstanceVO.FIND_IN_POOL_BY_USER)
                .setString("poolUuid", poolUuid)
                .setString("userUuid", userUuid)
                .list();
    }

    @Override
    @SuppressWarnings("unchecked")
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public List<VmInstanceVO> findInPoolWithNoUser(String poolUuid) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("findInPoolWithNoUser(%s, %s)", poolUuid));
        }

        return getSession()
                .getNamedQuery(VmInstanceVO.FIND_IN_POOL_BY_USER)
                .setString("poolUuid", poolUuid)
                .setString("userUuid", "")
                .list();
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public List<VmInstanceVO> findByHosts(String... hostUuid) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("findByHosts()"));
        }

        Disjunction disjunction = Restrictions.disjunction();
        for (String uuid : hostUuid) {
            disjunction.add(Restrictions.eq("hostUuid", uuid));
        }

        return findByCriteria(null, disjunction);
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public List<VmInstanceVO> findByPoolAndHosts(String poolUuid, String... hostUuid) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("findByPoolAndHosts(%s)", poolUuid));
        }

        Disjunction disjunction = Restrictions.disjunction();
        for (String uuid : hostUuid) {
            disjunction.add(Restrictions.eq("hostUuid", uuid));
        }

        return findByCriteria(null, Restrictions.eq("poolUuid", poolUuid), disjunction);
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public VmInstanceVO findByHostVmRefId(String hostUuid, String vmRefId) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("findByHostVmRefId()"));
        }

        return findUniqueEntityByCriteria(Restrictions.eq("hostUuid", hostUuid),
                Restrictions.eq("vmRefId", vmRefId));
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public Long countByDiskImage(String diskImageUuid) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("countByDiskImage(%s)", diskImageUuid));
        }

        return (Long) getSession()
                .getNamedQuery(VmInstanceVO.COUNT_BY_DISKIMAGE_UUID)
                .setString("diskImageUuid", diskImageUuid)
                .uniqueResult();
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public Long countByPoolAndStatus(String poolUuid, VmInstance.Status... status) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("countByPoolAndStatus(%s)", poolUuid));
        }

        if (status == null || status.length == 0) {
            return countByCriteria(Restrictions.eq("poolUuid", poolUuid));
        } else {
            Disjunction disjunction = Restrictions.disjunction();
            for (VmInstance.Status st : status) {
                disjunction.add(Restrictions.eq("status", st));
            }

            return countByCriteria(Restrictions.eq("poolUuid", poolUuid), disjunction);
        }
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public Long countByPoolAndStatusOnHost(String hostUuid, String poolUuid, VmInstance.Status... status) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("countByPoolAndStatus(%s, %s)", hostUuid, poolUuid));
        }

        if (status == null || status.length == 0) {
            return countByCriteria(Restrictions.eq("hostUuid", hostUuid), Restrictions.eq("poolUuid", poolUuid));
        } else {
            Disjunction disjunction = Restrictions.disjunction();
            for (VmInstance.Status st : status) {
                disjunction.add(Restrictions.eq("status", st));
            }

            return countByCriteria(Restrictions.eq("hostUuid", hostUuid),
                    Restrictions.eq("poolUuid", poolUuid), disjunction);
        }
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public Long countByUser(String userUuid) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("countByUser(%s)", userUuid));
        }

        return (Long) getSession()
                .getNamedQuery(VmInstanceVO.COUNT_BY_USER_UUID)
                .setString("userUuid", userUuid)
                .uniqueResult();
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public Long countAllInSession() {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("countAllInSession");
        }

        return (Long) getSession()
                .getNamedQuery(VmInstanceVO.COUNT_ALL_INSESSION)
                .uniqueResult();
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public Long countByHostAndStatus(String hostUuid, VmInstance.Status... status) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("countByHostAndStatus(%s)", hostUuid));
        }

        if (status == null || status.length == 0) {
            return countByCriteria(Restrictions.eq("hostUuid", hostUuid));
        } else {
            Disjunction disjunction = Restrictions.disjunction();
            for (VmInstance.Status st : status) {
                disjunction.add(Restrictions.eq("status", st));
            }

            return countByCriteria(Restrictions.eq("hostUuid", hostUuid), disjunction);
        }
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public Long countByStatus(VmInstance.Status... status) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("countByStatus()");
        }

        if (status == null || status.length == 0) {
            return countAll();
        } else {
            Disjunction disjunction = Restrictions.disjunction();
            for (VmInstance.Status st : status) {
                disjunction.add(Restrictions.eq("status", st));
            }

            return countByCriteria(disjunction);
        }
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    public void invalidateNonLocalVmInstances(String hostUuid) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("invalidateNonLocalVmInstances()");
        }

        getSession()
            .getNamedQuery(VmInstanceVO.INVALIDATE_NON_LOCAL_VMINSTANCES)
            .setString("hostUuid", hostUuid)
            .executeUpdate();
    }
}

