/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.database.dao.impl;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.nimdesk.database.dao.GlobalSettingDao;
import com.nimdesk.database.vo.GlobalSettingVO;

@Repository("globalSettingDao")
public class GlobalSettingDaoImpl extends GenericDaoImpl<GlobalSettingVO, Long> implements GlobalSettingDao {
    private static final Logger LOGGER = Logger.getLogger(GlobalSettingDaoImpl.class);

    public GlobalSettingDaoImpl() {
        super(GlobalSettingVO.class);
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public GlobalSettingVO findByKey(String key) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("findByKey(%s)", key));
        }

        return findUniqueEntityByCriteria(Restrictions.eq("key", key));
    }
}
