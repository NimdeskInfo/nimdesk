/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.database.dao.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.nimdesk.database.dao.PoolUserDao;
import com.nimdesk.database.vo.PoolUserVO;

@Repository("poolUserDao")
public class PoolUserDaoImpl extends GenericDaoImpl<PoolUserVO, Long> implements PoolUserDao {
    private static final Logger LOGGER = Logger.getLogger(PoolUserDaoImpl.class);

    public PoolUserDaoImpl() {
        super(PoolUserVO.class);
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public PoolUserVO findByPoolUser(String poolUuid, String userUuid) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("findByPoolUser(%s, %s)", poolUuid, userUuid));
        }

        return (PoolUserVO) getSession()
                .getNamedQuery(PoolUserVO.FIND_BY_POOL_USER)
                .setString("poolUuid", poolUuid)
                .setString("userUuid", userUuid)
                .uniqueResult();
    }

    @Override
    @SuppressWarnings("unchecked")
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public List<PoolUserVO> findByPool(String poolUuid) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("findByPool(%s)", poolUuid));
        }

        return getSession()
                .getNamedQuery(PoolUserVO.FIND_BY_POOL_UUID)
                .setString("poolUuid", poolUuid)
                .list();
    }

    @Override
    @SuppressWarnings("unchecked")
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public List<PoolUserVO> findByUser(String userUuid) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("findByUser(%s)", userUuid));
        }

        return getSession()
                .getNamedQuery(PoolUserVO.FIND_BY_USER_UUID)
                .setString("userUuid", userUuid)
                .list();
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public Long countUsersByPool(String poolUuid) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("countUsersByPool(%s)", poolUuid));
        }

        return (Long) getSession()
                .getNamedQuery(PoolUserVO.COUNT_BY_POOL_UUID)
                .setString("poolUuid", poolUuid)
                .uniqueResult();
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public Long countPoolsByUser(String userUuid) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("countPoolsByUser(%s)", userUuid));
        }

        return (Long) getSession()
                .getNamedQuery(PoolUserVO.COUNT_BY_USER_UUID)
                .setString("userUuid", userUuid)
                .uniqueResult();
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public void deleteByPool(String poolUuid) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("deleteByPool(%s)", poolUuid));
        }

        getSession()
            .getNamedQuery(PoolUserVO.DELETE_BY_POOL_UUID)
            .setString("poolUuid", poolUuid)
            .executeUpdate();
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public void deleteByUser(String userUuid) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("deleteByUser(%s)", userUuid));
        }

        getSession()
            .getNamedQuery(PoolUserVO.DELETE_BY_USER_UUID)
            .setString("userUuid", userUuid)
            .executeUpdate();
    }
}
