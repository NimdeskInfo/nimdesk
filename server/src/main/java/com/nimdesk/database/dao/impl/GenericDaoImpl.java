/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.database.dao.impl;

import java.io.Serializable;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.StaleStateException;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.nimdesk.database.dao.GenericDao;
import com.nimdesk.database.vo.BaseVO;
import com.nimdesk.util.Pagination;

public abstract class GenericDaoImpl<T, ID extends Serializable> extends HibernateDaoSupport implements GenericDao<T, ID> {
    private static final Logger LOGGER = Logger.getLogger(GenericDaoImpl.class);

    private final Class<T> _modelClass;

    public GenericDaoImpl(Class<T> modelClass) {
        _modelClass = modelClass;
    }

    public Class<T> getModelClass() {
        return _modelClass;
    }

    @Autowired
    public void init(SessionFactory sessionFactory) {
        setSessionFactory(sessionFactory);
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public Long countAll() {
        Session session = null;

        try {
            session = getSession();
            Criteria criteria = session.createCriteria(getModelClass());
            criteria.setProjection(Projections.rowCount());

            return (Long) criteria.uniqueResult();
        } finally {
            if (session != null) {
                releaseSession(session);
            }
        }
    }

    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    protected Long countByCriteria(Criterion... criterion) {
        Session session = null;

        try {
            session = getSession();
            Criteria criteria = session.createCriteria(getModelClass());
            for (Criterion c : criterion) {
                criteria.add(c);
            }

            criteria.setProjection(Projections.rowCount());

            return (Long) criteria.uniqueResult();
        } finally {
            if (session != null) {
                releaseSession(session);
            }
        }
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public List<T> findAll(Pagination pagination) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("findAll " + _modelClass.getName());
        }

        List<T> results = null;

        try {
            results = findByCriteria(pagination);
        } catch (DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
        }

        return results;
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public T findById(ID id) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("findById " + _modelClass.getName());
        }

        T entity = null;
        try {
            entity = getHibernateTemplate().get(getModelClass(), id);
        } catch (DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
        }

        return entity;
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public T findByUuid(String uuid) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("findByUuid " + _modelClass.getName());
        }

        return findUniqueEntityByCriteria(Restrictions.eq("uuid", uuid));
    }

    @SuppressWarnings("unchecked")
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    protected List<T> findByCriteria(Pagination pagination, Criterion... criterion) {
        Session session = null;

        try {
            session = getSession();
            Criteria criteria = session.createCriteria(getModelClass());
            for (Criterion c : criterion) {
                criteria.add(c);
            }

            if (pagination != null) {
                String orderBy;
                if ((orderBy = pagination.getOrderBy()) != null) {
                    Order order = pagination.isAscending() ? Order.asc(orderBy) : Order.desc(orderBy);
                    criteria.addOrder(order);
                }

                if (pagination.getFirst() >=0) {
                    criteria.setFirstResult(pagination.getFirst());
                }

                if (pagination.getMax() > 0) {
                    criteria.setMaxResults(pagination.getMax());
                }
            }

            return criteria.list();
        } finally {
            if (session != null) {
                releaseSession(session);
            }
        }
    }

    @SuppressWarnings("unchecked")
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    protected T findUniqueEntityByCriteria(Criterion... criterion) {
        Session session = null;

        try {
            session = getSession();
            Criteria criteria = session.createCriteria(getModelClass());
            for (Criterion c : criterion) {
                criteria.add(c);
            }

            return (T) criteria.uniqueResult();
        } finally {
            if (session != null) {
                releaseSession(session);
            }
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    public ID createNew(T entity) {
        LOGGER.trace("createNew " + _modelClass.getName());

        ID id = null;
        try {
            if (entity instanceof BaseVO) {
                ((BaseVO) entity).prePersist();
            }
            id = (ID) getHibernateTemplate().save(entity);
        } catch (DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
        }

        return id;
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    public void update(T entity) {
        LOGGER.trace("update " + _modelClass.getName());

        try {
            boolean isBasevO = entity instanceof BaseVO;
            if (isBasevO) {
                ((BaseVO) entity).prePersist();
            }

            while (true) {
                try {
                    getHibernateTemplate().update(entity);
                    break;
                } catch (StaleStateException e) {
                    LOGGER.warn("Failed to update entity", e);

                    if (isBasevO) {
                        BaseVO baseVO = (BaseVO) entity;

                        @SuppressWarnings("unchecked")
                        T newEntity = findById((ID) baseVO.getId());

                        if (newEntity != null) {
                            baseVO.applyChangedProperties((BaseVO) newEntity);
                            getHibernateTemplate().update(newEntity);
                            break;
                        }
                    }
                    throw e;
                } catch (Exception e) {
                    LOGGER.error("Failed to update entity", e);
                }
            }
        } catch (DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    public T merge(T entity) {
        LOGGER.trace("merge " + _modelClass.getName());

        T persistedEntity = null;
        try {
            if (entity instanceof BaseVO) {
                ((BaseVO) entity).prePersist();
            }
            persistedEntity = getHibernateTemplate().merge(entity);
        } catch (DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
        }

        return persistedEntity;
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    public boolean delete(ID id) {
        LOGGER.trace("delete " + _modelClass.getName());

        try {
            T entity = findById(id);
            if (entity != null) {
                getHibernateTemplate().delete(entity);
            }
        } catch (DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
            return false;
        }

        return true;
    }
}
