/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.database.dao;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.nimdesk.database.vo.PoolUserVO;

public interface PoolUserDao extends GenericDao<PoolUserVO, Long> {

    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    PoolUserVO findByPoolUser(String poolUuid, String userUuid);

    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    List<PoolUserVO> findByPool(String poolUuid);

    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    List<PoolUserVO> findByUser(String userUuid);

    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    Long countUsersByPool(String poolUuid);

    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    Long countPoolsByUser(String userUuid);

    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void deleteByPool(String poolUuid);

    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void deleteByUser(String userUuid);
}
