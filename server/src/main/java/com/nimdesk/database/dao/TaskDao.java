/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.database.dao;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.nimdesk.database.vo.TaskVO;
import com.nimdesk.util.Pagination;

public interface TaskDao extends GenericDao<TaskVO, Long> {

    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    Long countByPool(String poolUuid);

    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    Long countAllPools();

    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    Long countByVmImage(String vmImageUuid);

    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    Long countAllVmImages();

    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    Long countByVmInstance(String vmInstanceUuid);

    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    Long countAllVmInstances();

    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    List<TaskVO> findByPool(String poolUuid, Pagination pagination);

    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    List<TaskVO> findAllPools(Pagination pagination);

    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    List<TaskVO> findByVmImage(String vmImageUuid, Pagination pagination);

    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    List<TaskVO> findAllVmImages(Pagination pagination);

    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    List<TaskVO> findByVmInstance(String vmInstanceUuid, Pagination pagination);

    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    List<TaskVO> findAllVmInstances(Pagination pagination);

    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    List<TaskVO> findByServer(String serverUuid);

    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    List<TaskVO> findByServerAndTime(String serverUuid, boolean before, long time);

    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void invalidateInProgressTasks(String serverUuid);

    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void deleteOldTasks(long time);
}
