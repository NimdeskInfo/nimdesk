/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.database.dao.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.nimdesk.database.dao.ServerDao;
import com.nimdesk.database.vo.ServerVO;
import com.nimdesk.model.Server;
import com.nimdesk.model.Server.Status;

@Repository("serverDao")
public class ServerDaoImpl extends GenericDaoImpl<ServerVO, Long> implements ServerDao {
    private static final Logger LOGGER = Logger.getLogger(ServerDaoImpl.class);

    public ServerDaoImpl() {
        super(ServerVO.class);
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public ServerVO findLocalServer() {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("findLocalServer()");
        }

        return (ServerVO) getSession()
                .getNamedQuery(ServerVO.FIND_LOCAL_SERVER)
                .uniqueResult();
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public List<ServerVO> findByStatus(Status... status) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("countByStatus()");
        }

        if (status == null || status.length == 0) {
            return findAll(null);
        } else {
            Disjunction disjunction = Restrictions.disjunction();
            for (Server.Status st : status) {
                disjunction.add(Restrictions.eq("status", st));
            }

            return findByCriteria(null, disjunction);
        }
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public ServerVO findByPublicAddress(String publicAddress) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("findByPublicAddress(%s)", publicAddress));
        }

        return findUniqueEntityByCriteria(Restrictions.eq("publicAddress", publicAddress));
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public ServerVO findByClusterAddress(String clusterAddress) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("findByClusterAddress(%s)", clusterAddress));
        }

        return findUniqueEntityByCriteria(Restrictions.eq("clusterAddress", clusterAddress));
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public Long countByStatus(Server.Status... status) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("countByStatus()");
        }

        if (status == null || status.length == 0) {
            return countAll();
        } else {
            Disjunction disjunction = Restrictions.disjunction();
            for (Server.Status st : status) {
                disjunction.add(Restrictions.eq("status", st));
            }

            return countByCriteria(disjunction);
        }
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    public void invalidateNonLocalServers() {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("invalidateNonLocalServers()");
        }

        getSession()
            .getNamedQuery(ServerVO.INVALIDATE_NON_LOCAL_SERVERS)
            .executeUpdate();
    }
}
