/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.database.dao.impl;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.nimdesk.database.dao.VmImageDao;
import com.nimdesk.database.vo.VmImageVO;
import com.nimdesk.model.VmImage;

@Repository("vmImageDao")
public class VmImageDaoImpl extends GenericDaoImpl<VmImageVO, Long> implements VmImageDao {
    private static final Logger LOGGER = Logger.getLogger(VmImageDaoImpl.class);

    public VmImageDaoImpl() {
        super(VmImageVO.class);
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public Long countByStatus(VmImage.Status... status) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("countByStatus()");
        }

        if (status == null || status.length == 0) {
            return countAll();
        } else {
            Disjunction disjunction = Restrictions.disjunction();
            for (VmImage.Status st : status) {
                disjunction.add(Restrictions.eq("status", st));
            }

            return countByCriteria(disjunction);
        }
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public VmImageVO findByName(String vmImageName) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("findByName(%s)", vmImageName));
        }

        return findUniqueEntityByCriteria(Restrictions.eq("name", vmImageName));
    }
}

