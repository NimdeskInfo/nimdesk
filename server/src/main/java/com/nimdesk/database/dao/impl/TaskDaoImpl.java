/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.database.dao.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.SimpleExpression;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.nimdesk.database.dao.TaskDao;
import com.nimdesk.database.vo.TaskVO;
import com.nimdesk.model.Pool;
import com.nimdesk.model.VmImage;
import com.nimdesk.model.VmInstance;
import com.nimdesk.util.Pagination;

@Repository("taskDao")
public class TaskDaoImpl extends GenericDaoImpl<TaskVO, Long> implements TaskDao {
    private static final Logger LOGGER = Logger.getLogger(TaskDaoImpl.class);

    public TaskDaoImpl() {
        super(TaskVO.class);
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public Long countByPool(String poolUuid) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("countByPool(%s)", poolUuid));
        }

        SimpleExpression targetType = Restrictions.eq("targetType", Pool.class.getSimpleName());
        SimpleExpression targetUuid = Restrictions.eq("targetUuid", poolUuid);
        SimpleExpression associateType = Restrictions.eq("associateType", Pool.class.getSimpleName());
        SimpleExpression associateUuid = Restrictions.eq("associateUuid", poolUuid);

        return countByCriteria(Restrictions.or(
                Restrictions.and(targetType, targetUuid),
                Restrictions.and(associateType, associateUuid)));
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public Long countAllPools() {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("countAllPools");
        }

        SimpleExpression targetType = Restrictions.eq("targetType", Pool.class.getSimpleName());
        SimpleExpression associateType = Restrictions.eq("associateType", Pool.class.getSimpleName());

        return countByCriteria(Restrictions.or(targetType, associateType));
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public Long countByVmImage(String vmImageUuid) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("countByVmImage(%s)", vmImageUuid));
        }

        SimpleExpression targetType = Restrictions.eq("targetType", VmImage.class.getSimpleName());
        SimpleExpression targetUuid = Restrictions.eq("targetUuid", vmImageUuid);
        SimpleExpression associateType = Restrictions.eq("associateType", VmImage.class.getSimpleName());
        SimpleExpression associateUuid = Restrictions.eq("associateUuid", vmImageUuid);

        return countByCriteria(Restrictions.or(
                Restrictions.and(targetType, targetUuid),
                Restrictions.and(associateType, associateUuid)));
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public Long countAllVmImages() {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("countAllVmImages");
        }

        SimpleExpression targetType = Restrictions.eq("targetType", VmImage.class.getSimpleName());
        SimpleExpression associateType = Restrictions.eq("associateType", VmImage.class.getSimpleName());

        return countByCriteria(Restrictions.or(targetType, associateType));
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public Long countByVmInstance(String vmInstanceUuid) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("countByVmInstance(%s)", vmInstanceUuid));
        }

        SimpleExpression targetType = Restrictions.eq("targetType", VmInstance.class.getSimpleName());
        SimpleExpression targetUuid = Restrictions.eq("targetUuid", vmInstanceUuid);
        SimpleExpression associateType = Restrictions.eq("associateType", VmInstance.class.getSimpleName());
        SimpleExpression associateUuid = Restrictions.eq("associateUuid", vmInstanceUuid);

        return countByCriteria(Restrictions.or(
                Restrictions.and(targetType, targetUuid),
                Restrictions.and(associateType, associateUuid)));
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public Long countAllVmInstances() {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("countAllVmInstances");
        }

        SimpleExpression targetType = Restrictions.eq("targetType", VmInstance.class.getSimpleName());
        SimpleExpression associateType = Restrictions.eq("associateType", VmInstance.class.getSimpleName());

        return countByCriteria(Restrictions.or(targetType, associateType));
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public List<TaskVO> findByPool(String poolUuid, Pagination pagination) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("findByPool(%s)", poolUuid));
        }

        SimpleExpression targetType = Restrictions.eq("targetType", Pool.class.getSimpleName());
        SimpleExpression targetUuid = Restrictions.eq("targetUuid", poolUuid);
        SimpleExpression associateType = Restrictions.eq("associateType", Pool.class.getSimpleName());
        SimpleExpression associateUuid = Restrictions.eq("associateUuid", poolUuid);

        return findByCriteria(pagination, Restrictions.or(
                Restrictions.and(targetType, targetUuid),
                Restrictions.and(associateType, associateUuid)));
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public List<TaskVO> findAllPools(Pagination pagination) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("findAllPools");
        }

        SimpleExpression targetType = Restrictions.eq("targetType", Pool.class.getSimpleName());
        SimpleExpression associateType = Restrictions.eq("associateType", Pool.class.getSimpleName());

        return findByCriteria(pagination, Restrictions.or(targetType, associateType));
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public List<TaskVO> findByVmImage(String vmImageUuid, Pagination pagination) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("findByVmImage(%s)", vmImageUuid));
        }

        SimpleExpression targetType = Restrictions.eq("targetType", VmImage.class.getSimpleName());
        SimpleExpression targetUuid = Restrictions.eq("targetUuid", vmImageUuid);
        SimpleExpression associateType = Restrictions.eq("associateType", VmImage.class.getSimpleName());
        SimpleExpression associateUuid = Restrictions.eq("associateUuid", vmImageUuid);

        return findByCriteria(pagination, Restrictions.or(
                Restrictions.and(targetType, targetUuid),
                Restrictions.and(associateType, associateUuid)));
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public List<TaskVO> findAllVmImages(Pagination pagination) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("findAllVmImages");
        }

        SimpleExpression targetType = Restrictions.eq("targetType", VmImage.class.getSimpleName());
        SimpleExpression associateType = Restrictions.eq("associateType", VmImage.class.getSimpleName());

        return findByCriteria(pagination, Restrictions.or(targetType, associateType));
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public List<TaskVO> findByVmInstance(String vmInstanceUuid, Pagination pagination) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("findByVmInstance(%s)", vmInstanceUuid));
        }

        SimpleExpression targetType = Restrictions.eq("targetType", VmInstance.class.getSimpleName());
        SimpleExpression targetUuid = Restrictions.eq("targetUuid", vmInstanceUuid);
        SimpleExpression associateType = Restrictions.eq("associateType", VmInstance.class.getSimpleName());
        SimpleExpression associateUuid = Restrictions.eq("associateUuid", vmInstanceUuid);

        return findByCriteria(pagination, Restrictions.or(
                Restrictions.and(targetType, targetUuid),
                Restrictions.and(associateType, associateUuid)));
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public List<TaskVO> findAllVmInstances(Pagination pagination) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("findAllVmInstances");
        }

        SimpleExpression targetType = Restrictions.eq("targetType", VmInstance.class.getSimpleName());
        SimpleExpression associateType = Restrictions.eq("associateType", VmInstance.class.getSimpleName());

        return findByCriteria(pagination, Restrictions.or(targetType, associateType));
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public List<TaskVO> findByServer(String serverUuid) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("findByServer");
        }

        return findByCriteria(null, Restrictions.eq("serverUuid", serverUuid));
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public List<TaskVO> findByServerAndTime(String serverUuid, boolean before, long time) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("findByServerAndTime");
        }

        return findByCriteria(null, Restrictions.eq("serverUuid", serverUuid),
                before ? Restrictions.le("startTime", time) : Restrictions.ge("startTime", time));
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    public void invalidateInProgressTasks(String serverUuid) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("invalidateInProgressTasks(%s)", serverUuid));
        }

        getSession()
            .getNamedQuery(TaskVO.INVALIDATE_INPROGRESS_TASKS)
            .setString("serverUuid", serverUuid)
            .executeUpdate();
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    public void deleteOldTasks(long time) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("deleteOldTasks(%d)", time));
        }

        getSession()
            .getNamedQuery(TaskVO.DELETE_OLD_TASKS)
            .setLong("startTime", time)
            .executeUpdate();
    }
}

