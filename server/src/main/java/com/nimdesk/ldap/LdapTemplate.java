/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.ldap;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import javax.naming.directory.Attribute;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.util.Assert;

public class LdapTemplate {
    private static final Logger LOGGER = Logger.getLogger(LdapTemplate.class);

    public static final DateFormat LDAP_DATEFORMATE;

    static {
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        LDAP_DATEFORMATE = new SimpleDateFormat("yyyyMMddHHmmss'.0Z'");
        LDAP_DATEFORMATE.setCalendar(cal);
    }

    private static String USER_ATTRIBS[] = {
        "cn", "displayName", "distinguishedName", "description", "sAMAccountName", "sAMAccountType", "memberOf",
        "userPrincipalName", "objectClass"
    };

    private static String GROUP_ATTRIBS[] = {
        "cn", "displayName", "distinguishedName", "description", "sAMAccountName", "sAMAccountType", "objectClass"
    };

    private static String COMPUTER_ATTRIBS[] = {
        "cn", "distinguishedName", "whenCreated", "whenChanged"
    };

    private static String CROSSREF_ATTRIBS[] = {
        "cn", "distinguishedName", "dnsRoot", "nETBIOSName"
    };

    private final LdapContextImpl _context;

    public LdapTemplate(LdapContext context) {
        Assert.isInstanceOf(LdapContextImpl.class, context, "Invalid ldap context");
        _context = (LdapContextImpl) context;
    }

    public LdapContext getContext() {
        return _context;
    }

    public void close() {
        _context.close();
    }

    public List<LdapUser> authenticate(String username, String password) {
        List<LdapUser> users = new ArrayList<LdapUser>();

        LdapEntry userEntry;
        try {
            userEntry = getUser(username);
        } catch (Exception e) {
            LOGGER.warn(String.format("Failed to search user \"%s\" in ldap", username));
            return null;
        }

        if (userEntry == null) {
            LOGGER.warn(String.format("User \"%s\" does not exist in ldap", username));
            return null;
        }

        String userDn = userEntry.getAttribute("distinguishedName");
        if (userDn == null) {
            LOGGER.warn(String.format("Unable to find dn for user \"%s\"", username));
            return null;
        }

        LdapContext context = null;
        try {
            context = LdapContextImpl.connect(_context.getProviderUrl(), userDn, password, _context.getDefaultSearchBase());
        } catch (Exception e) {
            LOGGER.warn(String.format("Failed to authenticate user \"%s\"", username));
            return null;
        } finally {
            if (context != null) {
                context.close();
            }
        }

        LdapUser user = new LdapUser();
        user.setUser(true);
        user.setName(userEntry.getAttribute("cn"));
        user.setLogonName(userEntry.getAttribute("sAMAccountName"));
        user.setDomain(getContext().getDomain());
        user.setDistinguishedName(userEntry.getAttribute("distinguishedName"));
        users.add(user);

        try {
            boolean domainUsersIncluded = false;

            List<LdapEntry> groups = listGroupsForUser(userEntry);
            if (groups != null) {
                for (LdapEntry groupEntry : groups) {
                    LdapUser group = new LdapUser();
                    group.setUser(false);
                    group.setName(groupEntry.getAttribute("cn"));
                    group.setLogonName(groupEntry.getAttribute("sAMAccountName"));
                    group.setDomain(getContext().getDomain());
                    group.setDistinguishedName(groupEntry.getAttribute("distinguishedName"));
                    users.add(group);

                    // Just in case ldap query finds it.
                    if ("Domain Users".equalsIgnoreCase(group.getName())) {
                        domainUsersIncluded = true;
                    }
                }
            }

            // "Domain Users" does not have "member" attribute set. So we have to get it explicitly.
            if (!domainUsersIncluded) {
                LdapEntry domainUsersGroup = getGroup("Domain Users");
                if (domainUsersGroup != null) {
                    LdapUser group = new LdapUser();
                    group.setUser(false);
                    group.setName(domainUsersGroup.getAttribute("cn"));
                    group.setLogonName(domainUsersGroup.getAttribute("sAMAccountName"));
                    group.setDomain(getContext().getDomain());
                    group.setDistinguishedName(domainUsersGroup.getAttribute("distinguishedName"));
                    users.add(group);
                }
            }
        } catch (Exception e) {
            LOGGER.warn(String.format("Failed to get groups for user \"%s\"", username));
        }

        return users;
    }

    public List<LdapEntry> listUsers() throws Exception {
        String queryFilter = "(&(objectClass=user)(!(objectClass=computer)))";
        return _context.search(queryFilter, USER_ATTRIBS);
    }

    public List<LdapEntry> listUsers(String prefix) throws Exception {
        String queryFilter =
                String.format("(&(objectClass=user)(!(objectClass=computer))(|(userPrincipalName=%s*)(name=%s*)(sAMAccountName=%s*)))",
                        prefix, prefix, prefix);
        return _context.search(queryFilter, USER_ATTRIBS);
    }

    public List<LdapEntry> listUsersInGroup(String groupName) throws Exception {
        String queryFilter =
                String.format("(&(objectClass=group)(!(objectClass=computer))(|(cn=%s)(sAMAccountName=%s)))",
                        groupName, groupName);
        List<LdapEntry> groups = _context.search(queryFilter, GROUP_ATTRIBS);

        if (groups.size() == 0 || groups.size() > 1) {
            throw new Exception(String.format("Group \"%s\" not recognized.", groupName));
        }

        LdapEntry group = groups.get(0);
        String groupDn = group.getAttribute("distinguishedName");

        queryFilter =
                String.format("(&(objectClass=user)(!(objectClass=computer))(memberOf=%s))", groupDn);
        return _context.search(queryFilter, USER_ATTRIBS);
    }

    public List<LdapEntry> listGroupsForUser(String userPrincipalName) throws Exception {
        LdapEntry user = getUser(userPrincipalName);

        String userDn = user.getAttribute("distinguishedName");
        userDn = userDn.replaceAll("\\\\", "\\\\\\\\");

        String queryFilter =
                String.format("(&(objectClass=group)(!(objectClass=computer))(member=%s))", userDn);
        return _context.search(queryFilter, GROUP_ATTRIBS);
    }

    private List<LdapEntry> listGroupsForUser(LdapEntry user) throws Exception {
        String userDn = user.getAttribute("distinguishedName");
        userDn = userDn.replaceAll("\\\\", "\\\\\\\\");

        String queryFilter =
                String.format("(&(objectClass=group)(!(objectClass=computer))(member=%s))", userDn);
        return _context.search(queryFilter, GROUP_ATTRIBS);
    }

    public LdapEntry getUser(String logonName) throws Exception {
        String queryFilter =
                String.format("(&(&(objectClass=user)(!(objectClass=computer)))(|(userPrincipalName=%s)(name=%s)(sAMAccountName=%s)))",
                        logonName, logonName, logonName);

        List<LdapEntry> results = _context.search(queryFilter, USER_ATTRIBS, 1);
        if (results.size() == 0) {
            return null;
        }

        return results.get(0);
    }

    public List<LdapEntry> listGroups() throws Exception {
        String queryfilter = "(&(objectClass=group)(!(objectClass=computer)))";
        return _context.search(queryfilter, GROUP_ATTRIBS);
    }

    public List<LdapEntry> listGroups(String prefix) throws Exception {
        String queryfilter =
                String.format("(&(objectClass=group)(!(objectClass=computer))(name=%s*))", prefix);
        return _context.search(queryfilter, GROUP_ATTRIBS);
    }

    public LdapEntry getGroup(String groupName) throws Exception {
        String queryFilter =
                String.format("(&(objectClass=group)(!(objectClass=computer))(name=%s))", groupName);
        List<LdapEntry> results = _context.search(queryFilter, GROUP_ATTRIBS, 1);
        if (results.size() == 0) {
            return null;
        }

        return results.get(0);
    }

    public List<LdapEntry> listUserGroups() throws Exception {
        String queryfilter = "(&(|(objectClass=user)(objectClass=group))(!(objectClass=computer)))";
        return _context.search(queryfilter, USER_ATTRIBS);
    }

    public List<LdapEntry> listUserGroups(String prefix) throws Exception {
        String queryfilter =
                String.format("(&(|(objectClass=user)(objectClass=group))(!(objectClass=computer))(|(userPrincipalName=%s*)(name=%s*)))",
                        prefix, prefix);
        return _context.search(queryfilter, USER_ATTRIBS);
    }

    public List<LdapEntry> listComputers(String prefix, String lastChangedDate) throws Exception {
        return listComputers(prefix, lastChangedDate, Integer.MAX_VALUE);
    }

    public List<LdapEntry> listComputers(String prefix, String lastChangedDate, int count)
            throws Exception {
        String whenChanged = StringUtils.isEmpty(lastChangedDate) ? "" : String.format("(whenChanged<=%s)", lastChangedDate);
        String queryFilter =
                String.format("(&(objectClass=computer)(name=%s*)%s", prefix, whenChanged);

        return _context.search(queryFilter, COMPUTER_ATTRIBS);
    }

    public LdapEntry getComputer(String name) throws Exception {
        String queryFilter =
                String.format("(&(&(objectClass=computer))(name=%s))", name);
        List<LdapEntry> results = _context.search(queryFilter, COMPUTER_ATTRIBS, 1);
        if (results.size() == 0) {
            return null;
        }

        return results.get(0);
    }

    public void deleteComputer(String name) throws Exception {
        LdapEntry entry = getComputer(name);
        if (entry != null) {
            String dn = entry.getAttribute("distinguishedName");
            _context.delete(dn);
        }
    }

    public boolean verifyDomain(String domain) throws Exception {
        String queryFilter =
                String.format("(&(objectClass=domain)(name=%s))", domain);
        List<LdapEntry> results = _context.search(queryFilter, null, 1);

        return results != null && !results.isEmpty();
    }

    public boolean verifyOu(String ou) throws Exception {
        String queryFilter =
                String.format("(&(objectClass=organizationalunit)(distinguishedName=%s))", ou);
        List<LdapEntry> results = _context.search(queryFilter, null, 1);

        return results != null && !results.isEmpty();
    }

    public String getNetbiosName(String domain) throws Exception {
        String queryFilter =
                String.format("(&(objectClass=crossref)(nETBIOSName=*)(dnsRoot=%s))", domain);
        String searchBase = "CN=Partitions,CN=Configuration," + _context.getDefaultSearchBase();
        List<LdapEntry> results = _context.search(searchBase, queryFilter, CROSSREF_ATTRIBS, 1);

        String netbios = null;
        if (results != null && results.size() > 0) {
            LdapEntry entry = results.get(0);
            Attribute attribute = entry.getRawAttribute("nETBIOSName");
            if (attribute != null) {
                netbios = (String) attribute.get();
            }
        }
        return netbios;
    }
}

