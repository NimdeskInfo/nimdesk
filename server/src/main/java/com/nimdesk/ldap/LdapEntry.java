/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.ldap;

import java.util.ArrayList;
import java.util.HashMap;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;

public class LdapEntry {
    private final HashMap<String, Attribute> _attributes = new HashMap<String, Attribute>();

    public LdapEntry() {
    }

    public void AddAttribute(Attribute attribute) {
        _attributes.put(attribute.getID(), attribute);
    }

    public Attribute getRawAttribute(String attributeId) {
        return _attributes.get(attributeId);
    }

    public String getAttribute(String attributeId) {
        Attribute attribute = getRawAttribute(attributeId);
        if (attribute == null) {
            return null;
        }

        try {
            NamingEnumeration<?> values = attribute.getAll();
            if (values.hasMore()) {
                return (String) values.next();
            }
        } catch (NamingException e) {
            // Ignore error and return
        }

        return null;
    }

    public String[] getMultiValuedAttribute(String attributeId)
            throws NamingException {
        Attribute attribute = getRawAttribute(attributeId);
        if (attribute == null) {
            return null;
        }

        ArrayList<String> arr = new ArrayList<String>();

        try {
            NamingEnumeration<?> values = attribute.getAll();
            while (values.hasMore()) {
                arr.add((String) values.next());
            }
        } catch (NamingException e) {
            // Ignore error and return
        }

        return arr.toArray(new String[arr.size()]);
    }
}
