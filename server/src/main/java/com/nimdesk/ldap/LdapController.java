/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.ldap;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import com.nimdesk.event.AuthenticateLdapUserEvent;
import com.nimdesk.event.ConnectLdapEvent;
import com.nimdesk.event.DeleteLdapComputerEvent;
import com.nimdesk.event.EventHandler;
import com.nimdesk.event.FindLdapUsersEvent;
import com.nimdesk.service.EventBusService;
import com.google.common.eventbus.AllowConcurrentEvents;
import com.google.common.eventbus.Subscribe;

/**
 *
 */
public class LdapController implements EventHandler {
    private static final Logger LOGGER = Logger.getLogger(LdapController.class);

    private static final int CORE_LDAP_POOL_SIZE = 2;
    private static final int MAX_LDAP_POOL_SIZE = 200;

    private final ExecutorService _executor = new ThreadPoolExecutor(CORE_LDAP_POOL_SIZE,
            MAX_LDAP_POOL_SIZE, 300L, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>());

    private final EventBusService _eventBusService;

    @Autowired
    public LdapController(EventBusService eventBusService) {
        Assert.notNull(eventBusService, "An EventBusService must be set");
        _eventBusService = eventBusService;

        LOGGER.info("Register LdapController with event bus");
        _eventBusService.register(this);
    }

    @Subscribe
    @AllowConcurrentEvents
    public void handle(final ConnectLdapEvent event) {
        _executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    LdapContext context = LdapContextImpl.connect(event.getProviderUrl(), event.getUsername(),
                            event.getPassword(), event.getDomain());

                    if (event.getCallback() != null) {
                        event.getCallback().onSuccess(context);
                    }
                } catch (Exception e) {
                    LOGGER.error(String.format("Failed to connect to ldap \"%s\"", event.getProviderUrl()), e);

                    if (event.getCallback() != null) {
                        event.getCallback().onFailure(e);
                    }
                }
            }
        });
    }

    @Subscribe
    @AllowConcurrentEvents
    public void handle(final FindLdapUsersEvent event) {
        _executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    LdapTemplate template = new LdapTemplate(event.getContext());

                    List<LdapEntry> entries;

                    if (event.isIncludeUsers()) {
                        if (!event.isIncludeGroups()) {
                            if (StringUtils.isEmpty(event.getSearchPrefix())) {
                                entries = template.listUsers();
                            } else {
                                entries = template.listUsers(event.getSearchPrefix());
                            }
                        } else {
                            if (StringUtils.isEmpty(event.getSearchPrefix())) {
                                entries = template.listUserGroups();
                            } else {
                                entries = template.listUserGroups(event.getSearchPrefix());
                            }
                        }
                    } else {
                        if (event.isIncludeGroups()) {
                            if (StringUtils.isEmpty(event.getSearchPrefix())) {
                                entries = template.listGroups();
                            } else {
                                entries = template.listGroups(event.getSearchPrefix());
                            }
                        } else {
                            throw new Exception("None of user or group specified to search");
                        }
                    }

                    ArrayList<LdapUser> userList = new ArrayList<LdapUser>();

                    for (LdapEntry entry : entries) {
                        LdapUser user = new LdapUser();
                        user.setName(entry.getAttribute("cn"));
                        user.setDomain(event.getContext().getDomain());
                        user.setDisplayName(entry.getAttribute("displayName"));
                        user.setDistinguishedName(entry.getAttribute("distinguishedName"));
                        user.setDescription(entry.getAttribute("description"));

                        String[] objectClass = entry.getMultiValuedAttribute("objectClass");
                        boolean isGroup = false;
                        for (String s : objectClass) {
                            if ("group".equals(s)) {
                                isGroup = true;
                                break;
                            }
                        }
                        if (isGroup) {
                            user.setUser(false);
                            user.setLogonName(entry.getAttribute("sAMAccountName"));
                        } else {
                            user.setUser(true);
                            user.setLogonName(entry.getAttribute("sAMAccountName"));
                            user.setEmail(entry.getAttribute("userPrincipalName"));
                        }

                        userList.add(user);
                    }

                    if (event.getCallback() != null) {
                        event.getCallback().onSuccess(userList);
                    }
                } catch (Exception e) {
                    LOGGER.error(String.format("Failed to find ldap users prefix=\"%s\"", event.getSearchPrefix()), e);

                    if (event.getCallback() != null) {
                        event.getCallback().onFailure(e);
                    }
                }
            }
        });
    }

    @Subscribe
    @AllowConcurrentEvents
    public void handle(final AuthenticateLdapUserEvent event) {
        _executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    LdapTemplate template = new LdapTemplate(event.getContext());
                    List<LdapUser> users = template.authenticate(event.getUsername(), event.getPassword());
                    if (users == null) {
                        throw new Exception("Failed to authenticate user");
                    }

                    if (event.getCallback() != null) {
                        event.getCallback().onSuccess(users);
                    }
                } catch (Exception e) {
                    LOGGER.error(String.format("Failed to authenticate ldap user \"%s\"", event.getUsername()), e);

                    if (event.getCallback() != null) {
                        event.getCallback().onFailure(e);
                    }
                }
            }
        });
    }

    @Subscribe
    @AllowConcurrentEvents
    public void handle(final DeleteLdapComputerEvent event) {
        _executor.execute(new Runnable() {
            @Override
            public void run() {
                LdapContext context = null;

                try {
                    context = LdapContextImpl.connect(event.getProviderUrl(), event.getAdminUsername(),
                            event.getAdminPassword(), event.getDomain());

                    LdapTemplate template = new LdapTemplate(context);
                    template.deleteComputer(event.getComputerName());

                    if (event.getCallback() != null) {
                        event.getCallback().onSuccess(context);
                    }
                } catch (Exception e) {
                    LOGGER.error(String.format("Failed to delete ldap computer \"%s\"", event.getComputerName()), e);

                    if (event.getCallback() != null) {
                        event.getCallback().onFailure(e);
                    }
                } finally {
                    if (context != null) {
                        context.close();
                    }
                }
            }
        });
    }
}
