/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.ldap;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.ReferralException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import org.apache.log4j.Logger;
import org.springframework.util.Assert;

public class LdapContextImpl implements LdapContext {
    private static final Logger LOGGER = Logger.getLogger(LdapContextImpl.class);

    //private static final String EMAIL_EXPRESSION = "^([a-zA-Z0-9_\\.\\-+])+@(([a-zA-Z0-9-])+\\.)+([a-zA-Z0-9]{2,4})+$";
    //private static final Pattern EMAIL_VALIDATOR = Pattern.compile(EMAIL_EXPRESSION);

    private final String _providerUrl;
    private DirContext _dirCtx = null;
    private final String _domain;
    private final String _defaultSearchBase;

    public static LdapContext connect(String providerUrl, String username, String password, String domain)
            throws Exception {
        Assert.hasText(providerUrl, "Empty ldap provider url");
        Assert.hasText(username, "Empty ldap user name");
        Assert.notNull(password, "Null ldap password");
        //Assert.isTrue(EMAIL_VALIDATOR.matcher(username).matches(), "Invalid user name");

        Hashtable<String, String> env = new Hashtable<String, String>();
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.PROVIDER_URL, providerUrl);
        env.put(Context.REFERRAL, "throw");
        env.put(Context.SECURITY_AUTHENTICATION, "simple");
        if (providerUrl.startsWith("ldaps")) {
            env.put(Context.SECURITY_PROTOCOL, "ssl");
            env.put("java.naming.ldap.factory.socket", "com.nimdesk.ldap.LdapSSLSocketFactory");
        }
        if (!username.toLowerCase().startsWith("cn=") && username.indexOf('@') < 0) {
            username = username + '@' + domain;
        }
        env.put(Context.SECURITY_PRINCIPAL, username);
        env.put(Context.SECURITY_CREDENTIALS, password);

        DirContext ctx;
        try {
            ctx = new InitialDirContext(env);
        } catch (NamingException e) {
            String error = String.format("Failed to connect to ldap \"%s\" with user \"%s\"", providerUrl, username);
            LOGGER.error(error, e);
            throw new Exception(error);
        }

        return new LdapContextImpl(providerUrl, ctx, domain);
    }

    protected LdapContextImpl(String providerUrl, DirContext ctx, String domain) {
        _providerUrl = providerUrl;
        _dirCtx = ctx;
        _domain = domain;
        _defaultSearchBase = LdapHelper.convertDomain(domain);
    }

    @Override
    public String getProviderUrl() {
        return _providerUrl;
    }

    @Override
    public String getDomain() {
        return _domain;
    }

    @Override
    public String getDefaultSearchBase() {
        return _defaultSearchBase;
    }

    @Override
    protected void finalize() {
        if (_dirCtx != null) {
            close();
        }
    }

    @Override
    public void close() {
        if (_dirCtx != null) {
            try {
                _dirCtx.close();
                _dirCtx = null;
            } catch (NamingException e) {
                LOGGER.error(String.format("Failed to close context for ldap \"%s\"", _providerUrl), e);
            }
        }
    }

    public List<LdapEntry> search(String queryFilter, String attrs[])
            throws NamingException {
        return search(queryFilter, attrs, Integer.MAX_VALUE);
    }

    public List<LdapEntry> search(String queryFilter, String attrs[], int count)
            throws NamingException {
        return search(_defaultSearchBase, queryFilter, attrs, count);
    }

    public List<LdapEntry> search(String searchBase, String queryFilter, String attrs[])
            throws NamingException {
        return search(searchBase, queryFilter, attrs, Integer.MAX_VALUE);
    }

    public List<LdapEntry> search(String searchBase, String queryFilter, String attrs[], int count)
            throws NamingException {
        List<LdapEntry> entries = new ArrayList<LdapEntry>();

        SearchControls sc = new SearchControls();
        sc.setReturningAttributes(new String[0]);
        sc.setReturningAttributes(attrs);
        sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
        //sc.setCountLimit(count);

        DirContext curCtx = _dirCtx;
        boolean moreReferrals = true;

        outer:
        while (moreReferrals) {
            try {
                NamingEnumeration<SearchResult> answer = curCtx.search(searchBase, queryFilter, sc);
                if (answer == null) {
                    break;
                }

                while (answer.hasMore()) {
                    LdapEntry entry = createEntry(answer.next());
                    entries.add(entry);
                    if (--count <= 0) {
                        break outer;
                    }
                }

                moreReferrals = false;
            } catch (ReferralException e) {
                if (LOGGER.isTraceEnabled()) {
                    LOGGER.trace("search - got referral: " + e.getReferralInfo());
                }

                moreReferrals = e.skipReferral();
                if (moreReferrals) {
                    curCtx = (DirContext) e.getReferralContext();
                }
            }
        }

        return entries;
    }

    @Override
    public void delete(String dn) throws NamingException {
        _dirCtx.unbind(dn);
    }

    private LdapEntry createEntry(SearchResult sr) {
        LdapEntry entry = new LdapEntry();

        Attributes attributes = sr.getAttributes();
        if (attributes != null) {
            NamingEnumeration<? extends Attribute> attriEnum = attributes.getAll();
            while (attriEnum.hasMoreElements()) {
                entry.AddAttribute(attriEnum.nextElement());
            }
        }

        return entry;
    }
}
