/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.util;

import com.eaio.uuid.UUID;

public final class IdGenerator {

    /**
     * Generate UUIDs that are globally unique.
     * @return
     */
    public static String generateUuid() {
        UUID uuid = new UUID();
        return uuid.toString();
    }

    /**
     * Generate UUIDs that are locally unique.
     * @return
     */
    public static String generateUid() {
        return java.util.UUID.randomUUID().toString();
    }
}
