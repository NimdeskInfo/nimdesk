/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.util;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;


public class TimeZoneUtils {
    public static String toString(String timezone, long epoch) {
        return toString(TimeZone.getTimeZone(timezone), epoch);
    }

    /** tm format yyyy-mm-dd hh:mm */
    public static long toEpochInSeconds(String tm, TimeZone timezone) {
        Calendar inst = Calendar.getInstance(timezone);
        String[] tokens = StringUtils.split(tm, " ");
        String[] dates = StringUtils.split(tokens[0], "-");
        String[] times = StringUtils.split(tokens[1], ":");
        inst.set( Integer.valueOf(dates[0]), Integer.valueOf(dates[1])-1, Integer.valueOf(dates[2]),
                  Integer.valueOf(times[0]), Integer.valueOf(times[1]), 0);
        return inst.getTimeInMillis()/1000;
    }

    /**
     * Convert an epoch (in seconds) to a string such as
     * 2010-02-14 01:01:22 PST
     *
     * @param timezone the timezone
     * @param epoch the epoch in seconds
     * @return
     */
    public static String toString(TimeZone timezone, long epoch) {
        if ( epoch == 0 )
            return "";

        Date date = new Date(epoch * 1000);
        SimpleDateFormat sdf = new SimpleDateFormat();
        sdf.setTimeZone(timezone);
        sdf.applyPattern("yyyy-MM-dd HH:mm:ss z");
        return sdf.format(date);
    }

    public static String toString(long sec)
        throws IllegalArgumentException
    {
        if ( sec < 0 ) {
            throw new IllegalArgumentException( "Invalid seconds" );
        }

        long hours = sec / 3600;
        long minutes = (sec - hours * 3600) / 60;
        long seconds = sec % 60;

        return String.format("%02d:%02d:%02d", hours, minutes, seconds);
    }

    private static final Pattern _N_PATTERN_UNIT = Pattern.compile("\\d+(hours?|days?|weeks?|months?|years?)", Pattern.CASE_INSENSITIVE);
    private static final Pattern _PATTERN_UNIT = Pattern.compile("hour|day|week|month|year", Pattern.CASE_INSENSITIVE);

    public static long[] toEpoch(String dateRange, TimeZone tz) {
        if (dateRange.equalsIgnoreCase("last hour")) {
            return _toEpochLastHour();
        } else if (dateRange.equalsIgnoreCase("last 24 hours")) {
            return _toEpochLastNHours(tz, 24);
        } else if (dateRange.equalsIgnoreCase("last calendar day") ||
                  dateRange.equalsIgnoreCase("last day")) {
            return _toEpochLastNDays(tz, 1);
        } else if (dateRange.equalsIgnoreCase("last 2 days")) {
            return _toEpochLastNDays(tz, 2);
        } else if (dateRange.equalsIgnoreCase("last 5 days")) {
            return _toEpochLastNDays(tz, 5);
        } else if (dateRange.equalsIgnoreCase("last 7 days")) {
            return _toEpochLastNDays(tz, 7);
        } else if (dateRange.equalsIgnoreCase("last 14 days")) {
            return _toEpochLastNDays(tz, 14);
        } else if (dateRange.equalsIgnoreCase("last 30 days")) {
            return _toEpochLastNDays(tz, 30);
        } else if (dateRange.equalsIgnoreCase("Last calendar week")) {
            return _toEpochLastNWeeks(tz, 1);
        } else if (dateRange.equalsIgnoreCase("Last 2 calendar weeks")) {
            return _toEpochLastNWeeks(tz, 2);
        } else if (dateRange.equalsIgnoreCase("Last calendar month")) {
            return _toEpochLastNMonths(tz, 1);
        } else if (dateRange.equalsIgnoreCase("Last 2 calendar months") ||
                  dateRange.equalsIgnoreCase("Last 2 months")) {
            return _toEpochLastNMonths(tz, 2);
        } else if (dateRange.equalsIgnoreCase("Last 3 calendar months") ||
                  dateRange.equalsIgnoreCase("Last 3 months")) {
            return _toEpochLastNMonths(tz, 3);
        } else if (dateRange.equalsIgnoreCase("last year") ||
                  dateRange.equalsIgnoreCase("Last calendar year")) {
            return _toEpochLastNYears(tz, 1);
        } else if (dateRange.equalsIgnoreCase("yesterday")) {
            // Backward compatible for graph: current time - 48 hours, current time - 24 hours.
            return _toEpochYesterday();
        } else if (dateRange.equalsIgnoreCase("lastweek")) {
            // Backward compatible for graph: current time - 14 day, current time - 7 day
            return _toEpochLastWeek();
        } else if (dateRange.equalsIgnoreCase("last 30 days")) {
            return _toEpochLastNDays(tz, 30);
        } else if (dateRange.equalsIgnoreCase("last 365 days")) {
            return _toEpochLastNDays(tz, 365);
        } else if (dateRange.equalsIgnoreCase("last week")) {
            return _toEpochLastNWeeks(tz, 1);
        } else if (dateRange.equalsIgnoreCase("last 2 weeks")) {
            return _toEpochLastNWeeks(tz, 2);
        } else if (dateRange.equalsIgnoreCase("thismonth")) {
            // Backward compatible for graph: current time, beginning of current month.
            return _toEpochThisMonth(tz);
        } else if (dateRange.equalsIgnoreCase("last month") ||
                dateRange.equalsIgnoreCase("lastmonth")) {
            return _toEpochLastNMonths(tz, 1);
        } else if (dateRange.indexOf("TO") > 0 || dateRange.indexOf("To") > 0) {
            return _toEpochCustomizeRange(dateRange, tz);
        } else if (_PATTERN_UNIT.matcher(dateRange).matches()) {
            return _toEpochNUnit(dateRange, 1, tz);
        } else if (_N_PATTERN_UNIT.matcher(dateRange).matches()) {
            return _toEpochNUnit(dateRange, tz);
        } else {
            throw new IllegalArgumentException("Uknown date range " + dateRange);
        }
    }

    private static int _extractNumber(String period) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < period.length(); i++) {
            char ch = period.charAt(i);
            if (Character.isDigit(ch)) {
                sb.append( ch );
            } else {
                break;
            }
        }

        if (sb.length() == 0) {
            return 0;
        }

        return Integer.valueOf( sb.toString() );
    }

    /**
     * dateRange is either hour, day, week, month, year.
     * return nth date to now.
     */
    private static long[] _toEpochNUnit(String dateRange, TimeZone tz) {
        int n = _extractNumber(dateRange);
        if (n == 0) {
            throw new IllegalArgumentException("Invalid dateRange " + dateRange);
        }

        return _toEpochNUnit(dateRange, n, tz);
    }

    private static long[] _toEpochNUnit(String dateRange, int n, TimeZone tz) {
        long endEpoch = System.currentTimeMillis() / 1000;
        long startEpoch = 0;
        if (dateRange.indexOf("hour") >= 0) {
            startEpoch = endEpoch - n * 3600;
        } else if (dateRange.indexOf("day") >= 0) {
            startEpoch = endEpoch - n * 24 * 3600;
        } else if (dateRange.indexOf("week") >= 0) {
            startEpoch = endEpoch - n * 7 * 24 * 3600;
        } else if (dateRange.indexOf("month") >= 0) {
            startEpoch = endEpoch - n * 30 * 24 * 3600;
        } else if (dateRange.indexOf("year") >= 0) {
            startEpoch = endEpoch - n * 365 * 24 * 3600;
        }

        return new long[] { startEpoch, endEpoch };
    }

    private static long[] _toEpochLastHour() {
        long epoch = System.currentTimeMillis() / 1000;

        long[] epochs = new long[2];
        epochs[0] = epoch-3600;
        epochs[1] = epoch-1;

        return epochs;
    }

    private static long[] _toEpochYesterday() {
        long epoch = System.currentTimeMillis() / 1000;
        long[] epochs = new long[2];
        epochs[0] = epoch - 48*3600;
        epochs[1] = epoch - 24*3600;
        return epochs;
    }

    private static long[] _toEpochLastWeek() {
        long epoch = System.currentTimeMillis() / 1000;
        long[] epochs = new long[2];
        epochs[0] = epoch - 14*24*3600;
        epochs[1] = epoch - 7*24*3600;
        return epochs;
    }

    private static long[] _toEpochThisMonth(TimeZone tz) {
        long[] epochs = new long[2];
        Calendar now = Calendar.getInstance(tz);
        Calendar firstDay = Calendar.getInstance(tz);
        firstDay.set(now.get(Calendar.YEAR), now.get(Calendar.MONTH), 1, 0, 0, 0);
        epochs[0] = firstDay.getTimeInMillis()/1000;
        epochs[1] = now.getTimeInMillis()/1000;

        return epochs;
    }

    private static long[] _toEpochLastNHours(TimeZone tz, int n) {
        long[] epochs = new long[2];
        epochs[1] = System.currentTimeMillis() / 1000;
        epochs[0] = epochs[1] - n * 3600;

        return epochs;
    }

    private static long[] _toEpochLastNDays(TimeZone tz, int n) {
        long[] epochs = new long[2];
        epochs[1] = System.currentTimeMillis()/1000;
        epochs[0] = epochs[1] - n * 24 * 3600;

        return epochs;
    }

    private static long[] _toEpochLastNWeeks(TimeZone tz, int n) {
        long[] epochs = new long[2];

        Calendar cal = new GregorianCalendar(tz);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        cal.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
        epochs[1] = cal.getTimeInMillis()/1000 - 1;

        for ( int i=0; i<n; i++ )
            cal.add(Calendar.WEEK_OF_YEAR, -1);
        epochs[0] = cal.getTimeInMillis()/1000;

        return epochs;
    }

    private static long[] _toEpochLastNMonths(TimeZone tz, int n) {
        long[] epochs = new long[2];

        Calendar cal = new GregorianCalendar(tz);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        epochs[1] = cal.getTimeInMillis()/1000 - 1;

        for ( int i=0; i<n; i++ )
            cal.add(Calendar.MONTH, -1);
        epochs[0] = cal.getTimeInMillis()/1000;

        return epochs;
    }

    private static long[] _toEpochLastNYears(TimeZone tz, int n) {
        long[] epochs = new long[2];

        Calendar cal = new GregorianCalendar(tz);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.set(Calendar.MONTH, 0);
        epochs[1] = cal.getTimeInMillis()/1000 - 1;

        for ( int i=0; i<n; i++ )
            cal.add(Calendar.YEAR, -1);
        epochs[0] = cal.getTimeInMillis()/1000;

        return epochs;
    }

    private static long[] _toEpochCustomizeRange(String dateRange, TimeZone tz) {
        String[] tokens = StringUtils.split(dateRange, " TO " );
        long[] epochs = new long[2];
        epochs[0] = toEpochInSeconds(tokens[0], tz);
        epochs[1] = toEpochInSeconds(tokens[1], tz);
        return epochs;
    }
}
