/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.util;

import java.io.File;
import java.net.InetSocketAddress;
import java.net.Socket;

import org.apache.commons.lang.StringUtils;

import com.google.common.io.Files;

/**
 *
 */
public class Utils {
    private static final char[] LETTERS = {
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
        'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'R',
        'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
        '2', '3', '4', '5', '6', '7', '8', '9'
    };

    public static String convertVersion(long ver) {
        return convertVersion(ver, 7);
    }

    public static String convertVersion(long ver, int length) {
        StringBuilder sb = new StringBuilder();

        do {
            int index = (int) (ver & 0x1F);
            ver >>= 5;

            sb.append(LETTERS[index]);

            if (ver == 0) {
                break;
            }
        } while (--length != 0);

        return sb.toString();
    }

    public static File createTempDirectory() {
        return Files.createTempDir();
    }

    public static boolean testConnectIpPort(String ipAddress, int port, int milliseconds) {
        if (StringUtils.isEmpty(ipAddress)) {
            return false;
        }

        Socket socket = null;

        try {
            socket = new Socket();
            socket.connect(new InetSocketAddress(ipAddress, port), milliseconds);
            return socket.isConnected();
        } catch (Exception e) {
            return false;
        } finally {
            if (socket != null) {
                try {
                    socket.close();
                } catch (Exception e) {
                    // Ignore error
                }
            }
        }
    }

    public static int convertProgress(int originalProcess, int originalStart, int originalEnd, int newStart, int newEnd) {
        if (originalEnd <= originalStart || newEnd <= newStart) {
            return newStart;
        }

        // N = Ns + (O - Os) * (Ne - Ns) / (Oe - Os)
        return newStart + (int) ((float) (originalProcess - originalStart) * (newEnd - newStart)) / (originalEnd - originalStart);
    }

    public static int convertFullPercentage(int percentage, int newStart, int newEnd) {
        if (newEnd <= newStart) {
            return newStart;
        }

        // N = Ns + P * (Ne - Ns) / 100
        return newStart + (int) (percentage * (newEnd - newStart) / 100f);
    }

    public static ActionDelegate<Integer> wrapProgressUpdater(final ActionDelegate<Integer> progressUpdater,
            final int originalStart, final int originalEnd, final int newStart, final int newEnd) {
        if (progressUpdater == null) {
            return null;
        }

        return new ActionDelegate<Integer>() {
            @Override
            public void action(Integer param) {
                progressUpdater.action(Utils.convertProgress(param, originalStart, originalEnd, newStart, newEnd));
            }
        };
    }

    public static ActionDelegate<Integer> wrapProgressUpdater(final ActionDelegate<Integer> progressUpdater,
            final int newStart, final int newEnd) {
        if (progressUpdater == null) {
            return null;
        }

        return new ActionDelegate<Integer>() {
            @Override
            public void action(Integer param) {
                progressUpdater.action(Utils.convertFullPercentage(param, newStart, newEnd));
            }
        };
    }
}

