/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.util.command;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;

import org.apache.log4j.Logger;

class RedirectCommandExecutor implements CommandExecutor {
    private static final Logger LOGGER = Logger.getLogger(RedirectCommandExecutor.class);

    private static final String TEMP_FILE_PREFIX = "appClue";
    private static final double MAX_CHARS_FROM_STDFILES = 4 * Math.pow(10,6);

    private final File _tempDirFile;
    private File _stdOutFile = null;
    private File _stdErrFile = null;
    private int _exitCode = -1;

    RedirectCommandExecutor(String tempDir) {
        _tempDirFile = new File(tempDir);
    }

    @Override
    public boolean execute(String command) {
        if (!setupFiles()) {
            dispose();
            return false;
        }

        try {
            Process process = Runtime.getRuntime().exec(command + " >\"" + _stdOutFile.getAbsolutePath() + "\" 2>\"" + _stdErrFile.getAbsolutePath() + "\"");
            _exitCode = process.waitFor();
            return _exitCode == 0;
        } catch (IOException e) {
            LOGGER.error("Failed to execute command " + command, e);
        } catch (InterruptedException e) {
            LOGGER.error("Execution interrupted for command " + command, e);
        }

        return false;
    }

    @Override
    public int getExitCode() {
        return _exitCode;
    }

    @Override
    public String getStdOut() {
        if (_stdOutFile == null) {
            return "";
        }

        BufferedReader reader = getStdOutReader();
        if (reader != null) {
            try {
                return readFromReader(reader, _stdOutFile.getAbsolutePath());
            } catch (IOException e) {
                LOGGER.error("Failed to read from output stream file " + _stdOutFile.getAbsolutePath());
            } finally {
                try {
                    reader.close();
                } catch (IOException e) {
                    LOGGER.error("Failed to close output stream reader " + _stdOutFile.getAbsolutePath());
                }
            }
        }

        return "";
    }

    @Override
    public BufferedReader getStdOutReader() {
        if (_stdOutFile == null) {
            return null;
        }

        BufferedReader reader = null;

        try {
            reader = new BufferedReader(new InputStreamReader(new FileInputStream(_stdOutFile)));
        } catch (FileNotFoundException e) {
            LOGGER.error("Failed to open output stream file " + _stdOutFile.getAbsolutePath(), e);
        }

        return reader;
    }

    @Override
    public String getStdErr() {
        if (_stdErrFile == null) {
            return "";
        }

        BufferedReader reader = getStdErrReader();
        if (reader != null) {
            try {
                return readFromReader(reader, _stdErrFile.getAbsolutePath());
            } catch (IOException e) {
                LOGGER.error("Failed to read from error stream file " + _stdErrFile.getAbsolutePath());
            } finally {
                try {
                    reader.close();
                } catch (IOException e) {
                    LOGGER.error("Failed to close error stream reader " + _stdErrFile.getAbsolutePath());
                }
            }
        }

        return "";
    }

    @Override
    public BufferedReader getStdErrReader() {
        if (_stdErrFile == null) {
            return null;
        }

        BufferedReader reader = null;

        try {
            reader = new BufferedReader(new InputStreamReader(new FileInputStream(_stdErrFile)));
        } catch (FileNotFoundException e) {
            LOGGER.error("Failed to open error stream file " + _stdErrFile.getAbsolutePath(), e);
        }

        return reader;
    }

    @Override
    public void dispose() {
        if (_stdOutFile != null) {
            _stdOutFile.delete();
            _stdOutFile = null;
        }
        if (_stdErrFile != null) {
            _stdErrFile.delete();
            _stdErrFile = null;
        }
    }

    private boolean setupFiles() {
        try {
            _stdOutFile = File.createTempFile(TEMP_FILE_PREFIX, ".out", _tempDirFile);
        } catch (IOException e) {
            LOGGER.error("Failed to create stdout file", e);
            return false;
        }

        String basePath = _stdOutFile.getPath().substring(0, _stdOutFile.getPath().length() - 4);
        try {
            _stdErrFile = createFile(basePath + ".err");
        } catch (IOException e) {
            LOGGER.error("Failed to create stdout file", e);
            return false;
        }

        return true;
    }

    private File createFile(String filePath) throws IOException {
        File file = new File(filePath);
        if (!file.createNewFile()) {
            file = null;
        }
        return file;
    }

    private String readFromReader(BufferedReader reader, String filePath) throws IOException {
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);

        int numChars = 0;
        String line;
        while ((line = reader.readLine()) != null) {
            printWriter.println(line);
            numChars += line.length() + 1;
            if (numChars > MAX_CHARS_FROM_STDFILES) {
                LOGGER.warn("Maximum size for reading into memory exceeded for " + filePath);
                printWriter.println("(truncated ...)");
                break;
            }
        }

        return stringWriter.toString();
    }
}
