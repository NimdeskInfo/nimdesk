/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.util.command;

import java.io.File;

import org.apache.log4j.Logger;

/**
 * CommandExecutorFactory<br/>
 *
 */
public class CommandExecutorFactory {
    private static final Logger LOGGER = Logger.getLogger(CommandExecutorFactory.class);

    private static final String TEMP_DIR = "/tmp/cmd";

    static {
        File tempDir = new File(TEMP_DIR);
        if (!tempDir.exists()) {
            tempDir.mkdir();
        }
    }

    /**
     * Get an instance of CommandExecutor.<br/>
     *
     * @return An instance of CommandExecutor
     */
    public static CommandExecutor newCommandExecutor() {
        LOGGER.debug("Create command executor");
        CommandExecutor cmdExecutor = new RedirectCommandExecutor(TEMP_DIR);
        return cmdExecutor;
    }
}
