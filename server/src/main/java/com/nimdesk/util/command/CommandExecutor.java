/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.util.command;

import java.io.BufferedReader;

/**
 * CommandExecutor
 *
 */
public interface CommandExecutor {

    /**
     * Execute a command.<br/>
     *
     * @param command The command
     * @return true if succeeded, else false
     */
    boolean execute(String command);

    /**
     * Get command exit code.<br/>
     *
     * @return Exit code
     */
    int getExitCode();

    /**
     * Get standard output of the execution.<br/>
     *
     * @return Output string
     */
    String getStdOut();

    /**
     * Get buffered standard output of the execution.<br/>
     *
     * @return Instance of BufferedReader
     * @see java.io.BufferedReader
     */
    BufferedReader getStdOutReader();

    /**
     * Get standard error of the execution.<br/>
     *
     * @return Error string
     */
    String getStdErr();

    /**
     * Get buffered standard error of the execution.<br/>
     *
     * @return Instance of BufferedReader
     * @see java.io.BufferedReader
     */
    BufferedReader getStdErrReader();

    /**
     * To dispose the resources used by the execution.<br/>
     */
    void dispose();
}
