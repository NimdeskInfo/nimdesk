/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.ui.controller;

import java.io.IOException;
import java.io.InputStream;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.util.ClassUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class DownloadController {
    private static final Logger LOGGER = Logger.getLogger(DownloadController.class);

    private static final String MIME_TYPE = "application/octet-stream";
    private static final String AGENT_INSTALLER_32 = "agent.msi";
    private static final String AGENT_INSTALLER_64 = "agent64.msi";

    @RequestMapping(value="/{type}", method=RequestMethod.GET)
    public void downloadAgent(@PathVariable String type,
            HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        LOGGER.info(String.format("downloadAgent: type=%s", type));

        response.setContentType(MIME_TYPE);

        InputStream ins = null;
        ServletOutputStream outs = null;

        try {
            ClassLoader cl = ClassUtils.getDefaultClassLoader();
            if (StringUtils.equalsIgnoreCase("agent64", type)) {
                ins = cl.getResourceAsStream(AGENT_INSTALLER_64);
                response.setHeader("Content-Disposition", "inline; filename=\"" + AGENT_INSTALLER_64 + "\"");
            } else {
                ins = cl.getResourceAsStream(AGENT_INSTALLER_32);
                response.setHeader("Content-Disposition", "inline; filename=\"" + AGENT_INSTALLER_32 + "\"");
            }

            outs = response.getOutputStream();

            byte bytes[] = new byte[1024];
            int bytesRead;
            while ((bytesRead = ins.read(bytes)) != -1) {
                outs.write(bytes, 0, bytesRead);
            }
        } finally {
            if (ins != null) {
                try {
                    ins.close();
                } catch (Exception e) {
                    // Ignore error
                }
            }
            if (outs != null) {
                try {
                    outs.close();
                } catch (Exception e) {
                    // Ignore error
                }
            }
        }
    }
}
