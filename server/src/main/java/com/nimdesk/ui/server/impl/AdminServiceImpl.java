/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.ui.server.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.nimdesk.Version;
import com.nimdesk.cluster.ClusterSpec;
import com.nimdesk.cluster.RemoteProxyFactory;
import com.nimdesk.cluster.RemoteService;
import com.nimdesk.event.AsyncCallback;
import com.nimdesk.event.EnableHostCbrcEvent;
import com.nimdesk.event.JoinClusterEvent;
import com.nimdesk.event.StartHostMonitorEvent;
import com.nimdesk.model.AdminUser;
import com.nimdesk.model.Host;
import com.nimdesk.model.HypervisorType;
import com.nimdesk.model.LdapServer;
import com.nimdesk.model.Pool;
import com.nimdesk.model.Server;
import com.nimdesk.model.Storage;
import com.nimdesk.model.VmImage;
import com.nimdesk.model.VmInstance;
import com.nimdesk.service.AsyncExecutor;
import com.nimdesk.service.EventBusService;
import com.nimdesk.ui.server.AdminService;
import com.nimdesk.ui.server.AdminUserSummary;
import com.nimdesk.ui.server.ClusterDesktopSummary;
import com.nimdesk.ui.server.EditAdminUserSpec;
import com.nimdesk.ui.server.EditHostSpec;
import com.nimdesk.ui.server.HostSummary;
import com.nimdesk.ui.server.ServerConfigSpec;
import com.nimdesk.ui.server.ServerSummary;
import com.nimdesk.ui.server.SystemSummary;
import com.nimdesk.vm.StorageAsset;

@Service(AdminService.BEAN_NAME)
public class AdminServiceImpl implements AdminService, InitializingBean {
    private static final Logger LOGGER = Logger.getLogger(AdminServiceImpl.class);

    @Autowired
    private RemoteProxyFactory _remoteProxyFactory;

    @Autowired
    private EventBusService _eventBusService;

    @Autowired
    private AsyncExecutor _asyncExecutor;

    @Autowired
    private AdminUser.Service _adminUserService;

    @Autowired
    private Server.Service _serverService;

    @Autowired
    private Host.Service _hostService;

    @Autowired
    private Storage.Service _storageService;

    @Autowired
    private LdapServer.Service _ldapServerService;

    @Autowired
    private VmImage.Service _vmImageService;

    @Autowired
    private Pool.Service _poolService;

    @Autowired
    private VmInstance.Service _vmInstanceService;

    @Autowired
    private PasswordEncoder _passwordEncoder;

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(_remoteProxyFactory, "A RemoteProxyFactory must be set");
        Assert.notNull(_eventBusService, "An EventBusService must be set");
        Assert.notNull(_asyncExecutor, "An AsyncExecutor must be set");
        Assert.notNull(_adminUserService, "An AdminUser.Service must be set");
        Assert.notNull(_serverService, "A Server.Service must be set");
        Assert.notNull(_hostService, "A Host.Service must be set");
        Assert.notNull(_storageService, "A Storage.Service must be set");
        Assert.notNull(_ldapServerService, "An LdapServer.Service must be set");
        Assert.notNull(_vmImageService, "A VmImage.Service must be set");
        Assert.notNull(_poolService, "A Pool.Service must be set");
        Assert.notNull(_vmInstanceService, "A VmInstance.Service must be set");
        Assert.notNull(_passwordEncoder, "A PasswordEncoder must be set");
    }

    @Override
    public void verifyJoiningCluster(final String peerAddress, final String peerUsername, final String peerPassword,
            final Properties properties, AsyncCallback callback) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("verifyJoiningCluster");
        }

        _asyncExecutor.run(new AsyncExecutor.Job() {
            @Override
            public Object run() {
                RemoteService remoteService = _remoteProxyFactory.getRemoteProxy(peerAddress);
                ClusterSpec spec = remoteService.verifyCluster(peerUsername, peerPassword, properties);

                if (spec == null) {
                    String error = String.format("Failed to verify cluster with peer server \"%s\"", peerAddress);
                    LOGGER.error(error);
                    throw new RuntimeException(error);
                }

                return spec;
            }
        }, callback);
    }

    private static final long MILLISECONDS_IN_ONE_DAY = 24 * 3600 * 1000;

    @Override
    public List<AdminUserSummary> getAdminUsers() {
        List<? extends AdminUser> adminUsers = _adminUserService.getAll(null);

        List<AdminUserSummary> adminUserSumList = new ArrayList<AdminUserSummary>();

        if (adminUsers != null) {
            for (AdminUser adminUser : adminUsers) {
                adminUserSumList.add(new AdminUserSummary(adminUser));
            }
        }

        return adminUserSumList;
    }

    @Override
    public boolean validateAdminUserPassword(AdminUser adminUser, String password) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("updateAdminUser");
        }

        return StringUtils.equals(_passwordEncoder.encodePassword(password, null), adminUser.getPassword());
    }

    @Override
    public void updateAdminUser(final EditAdminUserSpec editAdminUserSpec, AsyncCallback callback) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("updateAdminUser");
        }

        _asyncExecutor.run(new AsyncExecutor.Job() {
            @Override
            public Object run() {
                if (editAdminUserSpec.getAdminUser() == null) {
                    // This is to add new admin account.
                    AdminUser adminUser = _adminUserService.newInstance();
                    adminUser.setUsername(editAdminUserSpec.getUsername());
                    adminUser.setPassword(_passwordEncoder.encodePassword(editAdminUserSpec.getPassword(), null));
                    adminUser.setFullName(editAdminUserSpec.getFullName());
                    adminUser.setEmail(editAdminUserSpec.getEmail());
                    adminUser.setNotify(editAdminUserSpec.isNotify());
                    _adminUserService.commit(adminUser);
                } else {
                    // This is to edit existing admin account.
                    AdminUser adminUser = _adminUserService.getByUuid(editAdminUserSpec.getAdminUser().getUuid());
                    if (adminUser != null) {
                        adminUser.setPassword(_passwordEncoder.encodePassword(editAdminUserSpec.getPassword(), null));
                        adminUser.setFullName(editAdminUserSpec.getFullName());
                        adminUser.setEmail(editAdminUserSpec.getEmail());
                        adminUser.setNotify(editAdminUserSpec.isNotify());
                        _adminUserService.commit(adminUser);
                    }
                }

                // TODO: update cluster master.

                return null;
            }
        }, callback);
    }

    @Override
    public void deleteAdminUser(final AdminUser adminUser, AsyncCallback callback) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("deleteAdminUser");
        }

        _asyncExecutor.run(new AsyncExecutor.Job() {
            @Override
            public Object run() {
                _adminUserService.delete(adminUser);

                // TODO: update cluster master.

                return null;
            }
        }, callback);
    }

    @Override
    public ServerSummary getLocalServer() {
        Server server = _serverService.getLocalServer();
        if (server == null) {
            return null;
        }

        Host host = _hostService.getLocalHost();

        return new ServerSummary(server, host);
    }

    @Override
    public void setServerConfig(ServerConfigSpec serverConfig) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("setServerConfig");
        }

        String curVersion = String.format("%d.%d.%d", Version.PRODUCT_VERSION_MAJOR, Version.PRODUCT_VERSION_MINOR, Version.PRODUCT_VERSION_BUILD);

        // Create Server
        Server server = _serverService.newInstance();
        server.setVersion(curVersion);
        server.setClusterName(serverConfig.getClusterName());
        if (StringUtils.isEmpty(serverConfig.getClusterId())) {
            server.setClusterId(String.valueOf(System.currentTimeMillis()));
        } else {
            server.setClusterId(serverConfig.getClusterId());
        }
        server.setPublicAddress(serverConfig.getServerPublicAddress());
        if (!StringUtils.isEmpty(serverConfig.getServerPrivateAddress())) {
            server.setPrivateAddress(serverConfig.getServerPrivateAddress());
        } else {
            server.setPrivateAddress(serverConfig.getServerPublicAddress());
        }
        server.setLocal(true);
        server.setStatus(Server.Status.Running);
        _serverService.commit(server);

        // Create Host
        Host host = _hostService.newInstance();
        host.setType(HypervisorType.Vmware);
        host.setAddress(serverConfig.getHypervisorServer());
        host.setUsername(serverConfig.getHypervisorUsername());
        host.setPassword(serverConfig.getHypervisorPassword());
        host.setRefId(serverConfig.getHostMO().getRefId());
        host.setName(serverConfig.getHostMO().getName());
        host.setServerUuid(server.getUuid());
        host.setVmNetworkRefId(serverConfig.getNetworkMO().getRefId());
        host.setVmNetwork(serverConfig.getNetworkMO().getName());
        host.setCbrcEnabled(serverConfig.getHypervisorCacheEnabled());
        if (!StringUtils.isEmpty(serverConfig.getHostMO().getVCenterIp())) {
            host.setVCenterIp(serverConfig.getHostMO().getVCenterIp());
            host.setVCenterAddress(serverConfig.getVCenterAddress());
            host.setVCenterUsername(serverConfig.getVCenterUsername());
            host.setVCenterPassword(serverConfig.getVCenterPassword());
        }
        host.setLocal(true);
        host.setStatus(Host.Status.Connected);
        if (serverConfig.getSharedStorage() != null) {
            host.setSharedDatastoreName(serverConfig.getSharedStorage().getName());
            host.setSharedDatastoreRefId(serverConfig.getSharedStorage().getRefId());
            host.setSharedDatastoreForAllDesktops(serverConfig.isSharedStorageForAll());
        }
        _hostService.commit(host);

        // Create Storages
        Collection<StorageAsset> datastoreMOs = serverConfig.getDatastoreMOs();
        for (StorageAsset datastoreMO : datastoreMOs) {
            Storage storage = _storageService.newInstance();
            storage.setRefId(datastoreMO.getRefId());
            storage.setName(datastoreMO.getName());
            storage.setCapacity(datastoreMO.getCapacity());
            storage.setFreeSpace(datastoreMO.getFreeSpace());
            storage.setHostUuid(host.getUuid());
            _storageService.commit(storage);
        }

        if (serverConfig.getHypervisorCacheEnabled()) {
            EnableHostCbrcEvent enableCbrcEvent = new EnableHostCbrcEvent(host, true, null);
            _eventBusService.post(enableCbrcEvent);
        }

        StartHostMonitorEvent event = new StartHostMonitorEvent(server, host, null);
        _eventBusService.post(event);

        JoinClusterEvent joinClusterEvent = new JoinClusterEvent(server.getClusterName(), server.getClusterId(),
                    server.getPublicAddress(),
                    serverConfig.isExistingCluster() ? new String[] { serverConfig.getPeerAddress() } : null,
                            null);
        _eventBusService.post(joinClusterEvent);
    }

    @Override
    public void getSystemSummary(AsyncCallback callback) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("getSystemSummary");
        }

        _asyncExecutor.run(new AsyncExecutor.Job() {
            @Override
            public Object run() {
                SystemSummary systemSummary = new SystemSummary();

                // Cluster info
                Server server = _serverService.getLocalServer();
                if (server == null) {
                    return systemSummary;
                }

                systemSummary.setClusterName(server.getClusterName());

                // Server info
                long serverGreenCount = _serverService.countByStatus(Server.GREEN_STATUSES);
                long serverYellowCount = _serverService.countByStatus(Server.YELLOW_STATUSES);
                long serverRedCount = _serverService.countByStatus(Server.RED_STATUSES);
                systemSummary.setServerCounts(new SystemSummary.Counts(serverGreenCount, serverYellowCount, serverRedCount));

                // Hypervisor host info
                long hostGreenCount = _hostService.countByStatus(Host.GREEN_STATUSES);
                long hostYellowCount = _hostService.countByStatus(Host.YELLOW_STATUSES);
                long hostRedCount = _hostService.countByStatus(Host.RED_STATUSES);
                systemSummary.setHypervisorCounts(new SystemSummary.Counts(hostGreenCount, hostYellowCount, hostRedCount));

                // Ldap domain info
                long domainCount = _ldapServerService.count();
                systemSummary.setDomainCounts(new SystemSummary.Counts(domainCount, 0, 0));

                // Image info
                long imageGreenCount = _vmImageService.countByStatus(VmImage.GREEN_STATUSES);
                long imageYellowCount = _vmImageService.countByStatus(VmImage.YELLOW_STATUSES);
                long imageRedCount = _vmImageService.countByStatus(VmImage.RED_STATUSES);
                systemSummary.setImageCounts(new SystemSummary.Counts(imageGreenCount, imageYellowCount, imageRedCount));

                // Pool info
                long poolGreenCount = _poolService.countByStatus(Pool.GREEN_STATUSES);
                long poolYellowCount = _poolService.countByStatus(Pool.YELLOW_STATUSES);
                long poolRedCount = _poolService.countByStatus(Pool.RED_STATUSES);
                systemSummary.setPoolCounts(new SystemSummary.Counts(poolGreenCount, poolYellowCount, poolRedCount));

                // Desktop info
                long desktopGreenCount = _vmInstanceService.countByStatus(VmInstance.GREEN_STATUSES);
                long desktopYellowCount = _vmInstanceService.countByStatus(VmInstance.YELLOW_STATUSES);
                long desktopRedCount = _vmInstanceService.countByStatus(VmInstance.RED_STATUSES);
                systemSummary.setDesktopCounts(new SystemSummary.Counts(desktopGreenCount, desktopYellowCount, desktopRedCount));

                // User session info
                long userSessionCount = _vmInstanceService.countAllInSession();
                systemSummary.setUserSessionCounts(new SystemSummary.Counts(userSessionCount, 0, 0));

                return systemSummary;
            }
        }, callback);
    }

    @Override
    public void getClusterDesktopSummary(AsyncCallback callback) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("getClusterDesktopSummary");
        }

        _asyncExecutor.run(new AsyncExecutor.Job() {
            @Override
            public Object run() {
                ClusterDesktopSummary desktopSummary = new ClusterDesktopSummary();

                ClusterDesktopSummary.Counts totalCounts = new ClusterDesktopSummary.Counts();
                desktopSummary.setTotalCount(totalCounts);

                List<? extends Host> hosts = _hostService.getAll(null);
                Map<String, ClusterDesktopSummary.Counts> hostCountsUuidMap =
                        new HashMap<String, ClusterDesktopSummary.Counts>();
                for (Host host : hosts) {
                    String hostUuid = host.getUuid();

                    ClusterDesktopSummary.Counts counts = new ClusterDesktopSummary.Counts();
                    counts.setHostName(host.getAddress());
                    counts.setHostUuid(hostUuid);

                    counts.setTotalCount(_vmInstanceService.countByHost(hostUuid));
                    counts.setInSessionCount(_vmInstanceService.countByStatusOnHost(hostUuid, VmInstance.Status.InSession));
                    counts.setReadyCount(_vmInstanceService.countByStatusOnHost(hostUuid, VmInstance.READY_STATUSES));
                    counts.setProvisioningCount(_vmInstanceService.countByStatusOnHost(hostUuid, VmInstance.PRESTARTING_STATUSES));
                    counts.setErrorCount(_vmInstanceService.countByStatusOnHost(hostUuid, VmInstance.UNAVAILABLE_STATUES));

                    hostCountsUuidMap.put(hostUuid, counts);

                    totalCounts.setTotalCount(totalCounts.getTotalCount() + counts.getTotalCount());
                    totalCounts.setInSessionCount(totalCounts.getInSessionCount() + counts.getInSessionCount());
                    totalCounts.setReadyCount(totalCounts.getReadyCount() + counts.getReadyCount());
                    totalCounts.setProvisioningCount(totalCounts.getProvisioningCount() + counts.getProvisioningCount());
                    totalCounts.setErrorCount(totalCounts.getErrorCount() + counts.getErrorCount());
                }

                desktopSummary.setHostCountsMap(hostCountsUuidMap);

                return desktopSummary;
            }
        }, callback);
    }

    @Override
    public List<ServerSummary> getServers() {
        List<ServerSummary> serverSumList = new ArrayList<ServerSummary>();

        List<? extends Server> servers = _serverService.getAll(null);
        if (servers != null) {
            for (Server server : servers) {
                Host host = null;
                List<? extends Host> hosts = _hostService.getHostsForServer(server.getUuid());
                if (hosts != null && !hosts.isEmpty()) {
                    host = hosts.get(0);
                }
                serverSumList.add(new ServerSummary(server, host));
            }
        }

        return serverSumList;
    }

    @Override
    public ServerSummary createServerSummary(Server server) {
        List<? extends Host> hosts = _hostService.getHostsForServer(server.getUuid());
        if (hosts != null && !hosts.isEmpty()) {
            return new ServerSummary(server, hosts.get(0));
        } else {
            return new ServerSummary(server, null);
        }
    }

    @Override
    public void maintainServer(Server server, boolean enter, AsyncCallback callback) {
        _serverService.maintainServer(server.getUuid(), enter, callback);
    }

    @Override
    public void disjoinCluster(final Server disjoiningServer, final AsyncCallback callback) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("disjoinCluster - server=%s", disjoiningServer.getPublicAddress()));
        }

        _asyncExecutor.run(new AsyncExecutor.Job() {
            @Override
            public Object run() {
                try {
                    Server disjoiningServer2 = _serverService.getByUuid(disjoiningServer.getUuid());
                    if (disjoiningServer2 != null) {
                        disjoiningServer2.setStatus(Server.Status.Disjoined);
                        _serverService.commit(disjoiningServer2);
                    }

                    List<? extends Server> servers = _serverService.getAll(null);
                    if (servers == null || servers.isEmpty()) {
                        LOGGER.warn("No available server found in the cluster");
                        throw new Exception("No available server");
                    }

                    for (Server server : servers) {
                        if (_serverService.isLocalServer(server.getUuid())) {
                            _serverService.disjoinCluster(disjoiningServer.getUuid(), null);
                        } else {
                            RemoteService remoteService = _remoteProxyFactory.getRemoteProxy(server.getPublicAddress());
                            if (remoteService == null) {
                                LOGGER.warn(String.format("Failed to get remote service from server \"%s\"", server.getPublicAddress()));
                                continue;
                            }

                            try {
                                remoteService.disjoinCluster(disjoiningServer.getUuid());
                            } catch (Exception e) {
                                LOGGER.warn(String.format("Failed to call remote service on \"%s\" to disjoin server \"%s\" from cluster",
                                        server.getPublicAddress(), disjoiningServer.getPublicAddress()));
                            }
                        }
                    }

                    if (callback != null) {
                        callback.onSuccess(null);
                    }
                } catch (Exception e) {
                    LOGGER.error(String.format("Failed to disjoin server \"%s\" from cluster", disjoiningServer.getPublicAddress()), e);

                    if (callback != null) {
                        callback.onFailure(e);
                    }
                }

                return null;
            }
        }, null);
    }

    @Override
    public List<HostSummary> getHosts() {
        List<HostSummary> hostSumList = new ArrayList<HostSummary>();

        List<? extends Host> hosts = _hostService.getAll(null);
        if (hosts != null) {
            for (Host host : hosts) {
                Server server = _serverService.getByUuid(host.getServerUuid());
                List<? extends Storage> storages = _storageService.getHostStorages(host.getUuid());
                hostSumList.add(new HostSummary(host, server, storages));
            }
        }

        return hostSumList;
    }

    @Override
    public HostSummary createHostSummary(Host host) {
        Server server = _serverService.getByUuid(host.getServerUuid());
        List<? extends Storage> storages = _storageService.getHostStorages(host.getUuid());
        return new HostSummary(host, server, storages);
    }

    @Override
    public void updateHost(final EditHostSpec editHostSpec, AsyncCallback callback) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("updateHost");
        }

        _asyncExecutor.run(new AsyncExecutor.Job() {
            @Override
            public Object run() {
                // This is to edit existing admin account.
                Host host = _hostService.getByUuid(editHostSpec.getHost().getUuid());
                if (host != null) {
                    boolean oldCbrcEnable = host.isCbrcEnabled();

                    host.setUsername(editHostSpec.getUsername());
                    host.setPassword(editHostSpec.getPassword());
                    host.setCbrcEnabled(editHostSpec.isCbrcEnabled());
                    _hostService.commit(host);

                    if (oldCbrcEnable != editHostSpec.isCbrcEnabled()) {
                        EnableHostCbrcEvent enableCbrcEvent = new EnableHostCbrcEvent(host, editHostSpec.isCbrcEnabled(), null);
                        _eventBusService.post(enableCbrcEvent);
                    }
                }

                return null;
            }
        }, callback);
    }

    @Override
    public List<? extends LdapServer> getLdapServers() {
        return _ldapServerService.getAll(null);
    }

    @Override
    public LdapServer addLdapServer(String ldapServerAddress, int port, boolean ssl, String ldapDomain,
            String ldapUsername, String ldapPassword) {
        LdapServer ldapServer = _ldapServerService.newInstance();
        ldapServer.setAddress(ldapServerAddress);
        ldapServer.setPort(port);
        ldapServer.setSsl(ssl);
        ldapServer.setDomain(ldapDomain);
        ldapServer.setUsername(ldapUsername);
        ldapServer.setPassword(ldapPassword);
        _ldapServerService.commit(ldapServer);

        return ldapServer;
    }

    @Override
    public void updateLdapServer(LdapServer ldapServer) {
        _ldapServerService.commit(ldapServer);
    }

    @Override
    public void deleteLdapServer(LdapServer ldapServer) {
        _ldapServerService.delete(ldapServer);
    }
}

