/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.ui.server.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.nimdesk.cluster.RemoteProxyFactory;
import com.nimdesk.cluster.RemoteService;
import com.nimdesk.event.AsyncCallback;
import com.nimdesk.model.Application;
import com.nimdesk.model.DiskImage;
import com.nimdesk.model.Host;
import com.nimdesk.model.LdapServer;
import com.nimdesk.model.Pool;
import com.nimdesk.model.Server;
import com.nimdesk.model.Storage;
import com.nimdesk.model.User;
import com.nimdesk.model.VmImage;
import com.nimdesk.model.VmInstance;
import com.nimdesk.service.AsyncExecutor;
import com.nimdesk.service.EventBusService;
import com.nimdesk.ui.server.CreatePoolSpec;
import com.nimdesk.ui.server.DesktopPoolService;
import com.nimdesk.ui.server.DesktopSummary;
import com.nimdesk.ui.server.DiskImageSummary;
import com.nimdesk.ui.server.EditImageSpec;
import com.nimdesk.ui.server.EditPoolSpec;
import com.nimdesk.ui.server.ImageSummary;
import com.nimdesk.ui.server.ImportVmSpec;
import com.nimdesk.ui.server.PoolSummary;
import com.nimdesk.util.IdGenerator;
import com.nimdesk.util.Pagination;
import com.nimdesk.util.Pair;
import com.nimdesk.vm.ImportableVm;

/**
 *
 */
@Service(DesktopPoolService.BEAN_NAME)
public class DesktopPoolServiceImpl implements DesktopPoolService, InitializingBean {
    private static final Logger LOGGER = Logger.getLogger(DesktopPoolServiceImpl.class);

    @Autowired
    private EventBusService _eventBusService;

    @Autowired
    private AsyncExecutor _asyncExecutor;

    @Autowired
    private RemoteProxyFactory _remoteProxyFactory;

    @Autowired
    private Server.Service _serverService;

    @Autowired
    private Host.Service _hostService;

    @Autowired
    private Storage.Service _storageService;

    @Autowired
    private VmImage.Service _vmImageService;

    @Autowired
    private Application.Service _applicationService;

    @Autowired
    private DiskImage.Service _diskImageService;

    @Autowired
    private Pool.Service _poolService;

    @Autowired
    private VmInstance.Service _vmInstanceService;

    @Autowired
    private User.Service _userService;

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(_eventBusService, "An EventBusService must be set");
        Assert.notNull(_asyncExecutor, "An AsyncExecutor must be set");
        Assert.notNull(_remoteProxyFactory, "A RemoteProxyFactory must be set");
        Assert.notNull(_serverService, "A Server.Service must be set");
        Assert.notNull(_hostService, "A Host.Service must be set");
        Assert.notNull(_storageService, "A Storage.Service must be set");
        Assert.notNull(_vmImageService, "A VmImage.Service must be set");
        Assert.notNull(_applicationService, "An Application.Service must be set");
        Assert.notNull(_diskImageService, "A DiskImage.Service must be set");
        Assert.notNull(_poolService, "A Pool.Service must be set");
        Assert.notNull(_vmInstanceService, "A VmInstance.Service must be set");
        Assert.notNull(_userService, "A User.Service must be set");
    }

    @Override
    public boolean validateImageName(String imageName) {
        try {
            return _vmImageService.getVmImageByName(imageName) == null;
        } catch (Exception e) {
            // Ignore error
            return true;
        }
    }

    @Override
    public long getValidVmimageCount() {
        return _vmImageService.countByStatus(VmImage.Status.Importing,
                VmImage.Status.Updating, VmImage.Status.Ready);
    }

    @Override
    public void getVmImages(final int first, final int max, final String orderBy, final boolean ascending,
            AsyncCallback callback) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("getVmImages - first=%d, max=%d, orderBy=%s, ascending=%s",
                    first, max, orderBy, ascending ? "true" : "false"));
        }

        _asyncExecutor.run(new AsyncExecutor.Job() {
            @Override
            public Object run() {
                long count = _vmImageService.count();
                List<? extends VmImage> vmImageList =
                        _vmImageService.getAll(new Pagination(first, max, orderBy, ascending));

                List<ImageSummary> imageSumList = new ArrayList<ImageSummary>();

                for (VmImage vmImage : vmImageList) {
                    ImageSummary imageSummary = createVmImageSummary(vmImage);
                    imageSumList.add(imageSummary);
                }

                return new Pair<Long, List<ImageSummary>>(count, imageSumList);
            }
        }, callback);
    }

    @Override
    public ImageSummary createVmImageSummary(VmImage vmImage) {
        ImageSummary imageSummary = new ImageSummary(vmImage);

        // Old vmImages don't have version field - update now.
        if (vmImage.getVersion() <= 0) {
            DiskImage diskImage = _diskImageService.getLocalDiskImageByGuid(vmImage.getCurrentDiskImageGuid());
            if (diskImage != null) {
                vmImage.setVersion(diskImage.getVersion());
                _vmImageService.commit(vmImage);
            }
        }

        if (!StringUtils.isEmpty(vmImage.getUpdatingDiskImageUuid())) {
            DiskImage diskImage = _diskImageService.getByUuid(vmImage.getUpdatingDiskImageUuid());
            if (diskImage != null) {
                imageSummary.setUpdatingDiskImageName(diskImage.getName());

                Host host = _hostService.getByUuid(diskImage.getHostUuid());
                if (host != null) {
                    imageSummary.setUpdatingHost(host.getAddress());
                }
            }
        }

        return imageSummary;
    }

    @Override
    public void getDiskImagesByVmImage(final String vmImageUuid, AsyncCallback callback) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("getDiskImagesByVmImage - vmImageUuid=%s", vmImageUuid));
        }

        _asyncExecutor.run(new AsyncExecutor.Job() {
            @Override
            public Object run() {
                List<? extends DiskImage> diskImageList = _diskImageService.getDiskImagesByOwner(vmImageUuid);

                Map<String, Host> hostUuidMap = new HashMap<String, Host>();
                Map<String, Storage> storageUuidMap = new HashMap<String, Storage>();

                List<DiskImageSummary> diskImageSumList = new ArrayList<DiskImageSummary>();

                for (DiskImage diskImage : diskImageList) {
                    String hostUuid = diskImage.getHostUuid();
                    Host host = hostUuidMap.get(hostUuid);
                    if (host == null) {
                        host = _hostService.getByUuid(hostUuid);
                        if (host != null) {
                            hostUuidMap.put(hostUuid, host);
                        }
                    }

                    String storageUuid = diskImage.getStorageUuid();
                    Storage storage = storageUuidMap.get(storageUuid);
                    if (storage == null) {
                        storage = _storageService.getByUuid(storageUuid);
                        if (storage != null) {
                            storageUuidMap.put(storageUuid, storage);
                        }
                    }

                    DiskImageSummary diskImageSummary = new DiskImageSummary(diskImage, host, storage);
                    diskImageSumList.add(diskImageSummary);
                }

                return diskImageSumList;
            }
        }, callback);
    }

    @Override
    public DiskImageSummary createDiskImageSummary(DiskImage diskImage) {
        String hostUuid = diskImage.getHostUuid();
        Host host = _hostService.getByUuid(hostUuid);

        String storageUuid = diskImage.getStorageUuid();
        Storage storage = _storageService.getByUuid(storageUuid);

        return new DiskImageSummary(diskImage, host, storage);
    }

    @Override
    public void importImageFromVm(final ImportVmSpec importVmSpec, final AsyncCallback callback) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("importImageFromVm - imageName=%s, vmName=%s, vmRefId=%s",
                    importVmSpec.getImageName(), importVmSpec.getVmName(), importVmSpec.getVmRefId()));
        }

        _asyncExecutor.run(new AsyncExecutor.Job() {
            @Override
            public Object run() {
                ImportableVm vm = importVmSpec.getVm();
                Host host;

                if ((host = vm.getHost()) != null && !_hostService.isLocalHost(host.getUuid())) {
                    Server server = _serverService.getByUuid(host.getServerUuid());
                    if (server == null || server.getStatus() != Server.Status.Running) {
                        String error = String.format("The server is not running on host \"%s\"", host.getAddress());
                        LOGGER.error(error);

                        if (callback != null) {
                            callback.onFailure(new Exception(error));
                        }

                        return null;
                    }

                    RemoteService remoteService = _remoteProxyFactory.getRemoteProxy(server.getPublicAddress());
                    if (remoteService == null) {
                        String error = String.format("Cannot connect to server \"%s\"", server.getPublicAddress());
                        LOGGER.error(error);

                        if (callback != null) {
                            callback.onFailure(new Exception(error));
                        }

                        return null;
                    }

                    remoteService.importImageFromVm(importVmSpec);
                } else {
                    VmImage vmImage = _vmImageService.newInstance();
                    vmImage.setName(importVmSpec.getImageName());
                    vmImage.setDescription(importVmSpec.getImageDescription());
                    vmImage.setOsName(importVmSpec.getVmOsName());
                    vmImage.setImageSizeInKB(importVmSpec.getImageSizeInKB());

                    _vmImageService.importVm(vmImage, importVmSpec.getVmName(), importVmSpec.getVmRefId(), callback);
                }

                return null;
            }
        }, null);
    }

    @Override
    public void editImage(final EditImageSpec editImageSpec, final AsyncCallback callback) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("editImage - imageName=%s", editImageSpec.getName()));
        }

        _asyncExecutor.run(new AsyncExecutor.Job() {
            @Override
            public Object run() {
                VmImage vmImage = _vmImageService.getByUuid(editImageSpec.getVmImageUuid());
                if (vmImage == null || VmImage.Status.Deleting == vmImage.getStatus()) {
                    throw new RuntimeException(String.format("Null object for image \"%s\"", editImageSpec.getName()));
                }

                vmImage.setDescription(editImageSpec.getDescription());

                _vmImageService.commit(vmImage);

                if (callback != null) {
                    callback.onSuccess(null);
                }

                return null;
            }
        }, null);
    }

    @Override
    public void cloneUpdateImage(final ImageSummary image, final AsyncCallback callback) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("cloneUpdateImage - imageName=%s", image.getName()));
        }

        _asyncExecutor.run(new AsyncExecutor.Job() {
            @Override
            public Object run() {
                VmImage vmImage = _vmImageService.getByUuid(image.getUuid());
                if (vmImage == null || VmImage.Status.Deleting == vmImage.getStatus()) {
                    LOGGER.warn(String.format("Empty vmImage for image \"%s\"", image.getName()));
                    if (callback != null) {
                        callback.onSuccess(null);
                    }
                    return null;
                }

                _vmImageService.cloneUpdateVmImage(vmImage, callback);

                return null;
            }
        }, null);
    }

    @Override
    public void cancelUpdateImage(final ImageSummary image, final AsyncCallback callback) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("cancelUpdateImage - imageName=%s", image.getName()));
        }

        _asyncExecutor.run(new AsyncExecutor.Job() {
            @Override
            public Object run() {
                VmImage vmImage = _vmImageService.getByUuid(image.getUuid());
                if (vmImage == null) {
                    LOGGER.warn(String.format("Empty vmImage for image \"%s\"", image.getName()));
                    if (callback != null) {
                        callback.onSuccess(null);
                    }
                    return null;
                }

                _vmImageService.cancelUpdateVmImage(vmImage, callback);

                return null;
            }
        }, null);
    }

    @Override
    public void importUpdatedImage(final ImageSummary image, final AsyncCallback callback) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("importUpdatedImage - imageName=%s", image.getName()));
        }

        _asyncExecutor.run(new AsyncExecutor.Job() {
            @Override
            public Object run() {
                VmImage vmImage = _vmImageService.getByUuid(image.getUuid());
                if (vmImage == null || VmImage.Status.Deleting == vmImage.getStatus()) {
                    LOGGER.error(String.format("Empty vmImage for image \"%s\"", image.getName()));

                    if (callback != null) {
                        callback.onFailure(new Exception("Invalid image"));
                    }

                    return null;
                }

                DiskImage updatingDiskImage = _diskImageService.getByUuid(vmImage.getUpdatingDiskImageUuid());
                if (updatingDiskImage == null) {
                    LOGGER.error(String.format("Unable to find updating diskImage id=\"%s\" for vmImage \"%s\" in db",
                            vmImage.getUpdatingDiskImageUuid(), vmImage.getName()));

                    if (callback != null) {
                        callback.onFailure(new Exception("Can't locate the entity"));
                    }

                    return null;
                }

                if (!_hostService.isLocalHost(updatingDiskImage.getHostUuid())) {
                    Host host = _hostService.getByUuid(updatingDiskImage.getHostUuid());
                    if (host == null) {
                        LOGGER.error(String.format("Unable to find host for updating diskImage id=\"%s\" for vmImage \"%s\" in db",
                            vmImage.getUpdatingDiskImageUuid(), vmImage.getName()));

                        if (callback != null) {
                            callback.onFailure(new Exception("Can't locate the host"));
                        }

                        return null;
                    }

                    Server server = _serverService.getByUuid(host.getServerUuid());
                    if (server == null || server.getStatus() != Server.Status.Running) {
                        String error = String.format("The server is not running on host \"%s\"", host.getAddress());
                        LOGGER.error(error);

                        if (callback != null) {
                            callback.onFailure(new Exception(error));
                        }

                        return null;
                    }

                    RemoteService remoteService = _remoteProxyFactory.getRemoteProxy(server.getPublicAddress());
                    if (remoteService == null) {
                        String error = String.format("Cannot connect to server \"%s\"", server.getPublicAddress());
                        LOGGER.error(error);

                        if (callback != null) {
                            callback.onFailure(new Exception(error));
                        }

                        return null;
                    }

                    remoteService.importUpdatedVmImage(vmImage.getUuid());
                } else {
                    _vmImageService.importUpdatedVmImage(vmImage, callback);
                }

                return null;
            }
        }, null);
    }

    @Override
    public void deleteImage(final ImageSummary image, final AsyncCallback callback) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("deleleImage - imageName=%s", image.getName()));
        }

        final VmImage vmImage = image.getVmImage();

        _asyncExecutor.run(new AsyncExecutor.Job() {
            @Override
            public Object run() {
                if (vmImage == null) {
                    LOGGER.warn(String.format("Empty vmImage for image \"%s\"", image.getName()));
                    if (callback != null) {
                        callback.onSuccess(null);
                    }
                    return null;
                }

                vmImage.setStatus(VmImage.Status.Deleting);
                _vmImageService.commit(vmImage);

                return null;
            }
        }, null);
    }

    @Override
    public void recomposePoolsForImage(final ImageSummary image, final AsyncCallback callback) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("recomposePoolsForImage - imageName=%s", image.getName()));
        }

        _asyncExecutor.run(new AsyncExecutor.Job() {
            @Override
            public Object run() {
                List<? extends Pool> pools = _poolService.getByVmImage(image.getUuid());
                if (pools == null) {
                    throw new RuntimeException(String.format("Failed to find pools for image \"%s\"", image.getName()));
                }

                for (Pool pool : pools) {
                    _poolService.recomposeDesktops(pool, false, null);
                }

                if (callback != null) {
                    callback.onSuccess(null);
                }

                return null;
            }
        }, null);
    }

    @Override
    public void revertImage(final ImageSummary image, final AsyncCallback callback) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("revertImage - imageName=%s", image.getName()));
        }

        _asyncExecutor.run(new AsyncExecutor.Job() {
            @Override
            public Object run() {
                VmImage vmImage = _vmImageService.getByUuid(image.getUuid());
                if (vmImage == null || VmImage.Status.Deleting == vmImage.getStatus()) {
                    LOGGER.warn(String.format("Empty vmImage for image \"%s\"", image.getName()));
                    if (callback != null) {
                        callback.onSuccess(null);
                    }
                    return null;
                }

                _vmImageService.revertVmImage(vmImage, callback);

                return null;
            }
        }, null);
    }

    @Override
    public void retryDiskImageCopy(final DiskImageSummary diskImageSummary, final AsyncCallback callback) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("retryDiskImageCopy - imageName=%s", diskImageSummary.getName()));
        }

        _asyncExecutor.run(new AsyncExecutor.Job() {
            @Override
            public Object run() {
                try {
                    DiskImage diskImage = _diskImageService.getByUuid(diskImageSummary.getUuid());
                    if (diskImage == null) {
                        throw new RuntimeException(String.format("Null object for disk image \"%s\"", diskImageSummary.getName()));
                    }

                    diskImage.setStatus(DiskImage.Status.RequestingCopy);
                    _diskImageService.commit(diskImage);

                    if (callback != null) {
                        callback.onSuccess(diskImage);
                    }
                } catch (Exception e) {
                    LOGGER.error(String.format("Failed to retry copying disk image \"%s\"", diskImageSummary.getName()), e);

                    if (callback != null) {
                        callback.onFailure(e);
                    }
                }

                return null;
            }
        }, null);
    }

    @Override
    public boolean validatePoolName(String poolName) {
        try {
            if ("UDD".equals(poolName)) {
                return false;
            }

            return _poolService.getPoolByName(poolName) == null;
        } catch (Exception e) {
            // Ignore error
            return true;
        }
    }

    @Override
    public long getValidPoolCount() {
        return _poolService.countByStatus(Pool.Status.New,
                Pool.Status.Provisioning, Pool.Status.Recomposing, Pool.Status.Ready);
    }

    @Override
    public long getValidPoolDesktopCount() {
        long desktopCount = 0;

        List<? extends Pool> pools = _poolService.getByStatus(Pool.Status.New,
                Pool.Status.Provisioning, Pool.Status.Recomposing, Pool.Status.Ready);
        if (pools != null) {
            for (Pool pool : pools) {
                desktopCount += pool.getMaxSize();
            }
        }

        return desktopCount;
    }

    @Override
    public void getPools(final int first, final int max, final String orderBy, final boolean ascending,
            AsyncCallback callback) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("getPools - first=%d, max=%d, orderBy=%s, ascending=%s",
                    first, max, orderBy, ascending ? "true" : "false"));
        }

        _asyncExecutor.run(new AsyncExecutor.Job() {
            @Override
            public Object run() {
                long count = _poolService.count();
                List<? extends Pool> poolList =
                        _poolService.getAll(new Pagination(first, max, orderBy, ascending));

                List<PoolSummary> poolSumList = new ArrayList<PoolSummary>();

                HashMap<String, VmImage> vmImageMap = new HashMap<String, VmImage>();

                for (Pool pool : poolList) {
                    String vmImageUuid = pool.getVmImageUuid();
                    VmImage vmImage = vmImageMap.get(vmImageUuid);
                    if (vmImage == null) {
                        if (StringUtils.isEmpty(vmImageUuid)) {
                            LOGGER.warn(String.format("Empty image uuid for pool \"%s\"", pool.getName()));
                        } else {
                            vmImage = _vmImageService.getByUuid(vmImageUuid);
                            if (vmImage == null) {
                                LOGGER.warn(String.format("Unable to find image info for uuid \"%s\"", vmImageUuid));
                            }
                        }
                    }

                    PoolSummary poolSummary = createPoolSummary(pool, vmImage);
                    poolSumList.add(poolSummary);
                }

                return new Pair<Long, List<PoolSummary>>(count, poolSumList);
            }
        }, callback);
    }

    @Override
    public PoolSummary createPoolSummary(Pool pool, VmImage vmImage) {
        long poolVersion = 0;
        List<? extends DiskImage> diskImages = _diskImageService.getDiskImagesByGuid(pool.getBaseDiskImageGuid());
        if (diskImages != null && !diskImages.isEmpty()) {
            poolVersion = diskImages.get(0).getVersion();
        }

        PoolSummary poolSummary = new PoolSummary(pool, poolVersion, vmImage);

        return poolSummary;
    }

    @Override
    public void createPool(final CreatePoolSpec createPoolSpec, final AsyncCallback callback) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("createPool - poolName=%s, imageName=%s",
                    createPoolSpec.getName(), createPoolSpec.getImageName()));
        }

        _asyncExecutor.run(new AsyncExecutor.Job() {
            @Override
            public Object run() {
                try {
                    Pool pool = _poolService.newInstance();
                    pool.setType(createPoolSpec.getType());
                    pool.setName(createPoolSpec.getName());
                    pool.setDescription(createPoolSpec.getDescription());
                    pool.setOsName(createPoolSpec.getImage().getOperatingSystem());
                    pool.setEnabled(createPoolSpec.isEnabled());
                    pool.setPrefix(createPoolSpec.getPrefix());
                    pool.setMaxSize(createPoolSpec.getMaxSize());
                    pool.setCpuCount(createPoolSpec.getCpuCount());
                    pool.setMemoryMB(createPoolSpec.getMemoryMB());
                    pool.setReserveMemory(createPoolSpec.isReserveMemory());
                    pool.setProductKey(createPoolSpec.getProductKey());
                    pool.setTimeZone(createPoolSpec.getTimeZone().getIndex());
                    if (Pool.Type.Persistent == pool.getType()) {
                        pool.setUserDataDiskDriveLetter(createPoolSpec.getUddDrive());
                        pool.setUserDataDiskSizeInMB((long) (createPoolSpec.getUddSizeInGB() * 1024));
                    } else {
                        pool.setMinSize(createPoolSpec.getMinSize());
                    }
                    pool.setVmImageUuid(createPoolSpec.getImage().getUuid());
                    pool.setBaseDiskImageGuid(IdGenerator.generateUuid());

                    LdapServer ldapServer = createPoolSpec.getLdapServer();
                    if (LdapServer.WORKGROUP.equals(ldapServer)) {
                        pool.setDomain(false);
                        pool.setDomainOrWorkGroup(createPoolSpec.getWorkGroup());
                    } else {
                        pool.setDomain(true);
                        pool.setDomainOrWorkGroup(ldapServer.getDomain());
                    }

                    long protocols = 0;
                    if (createPoolSpec.isSupportRdp()) {
                        protocols |= Pool.PROTOCOL_RDP;
                    }
                    pool.setProtocols(protocols);

                    long redirects = 0;
                    if (createPoolSpec.isRedirectDrives()) {
                        redirects |= Pool.REDIRECT_DRIVE;
                    }
                    if (createPoolSpec.isRedirectPrinters()) {
                        redirects |= Pool.REDIRECT_PRINTER;
                    }
                    if (createPoolSpec.isRedirectComPorts()) {
                        redirects |= Pool.REDIRECT_COMPORT;
                    }
                    if (createPoolSpec.isRedirectSmartCards()) {
                        redirects |= Pool.REDIRECT_SMARTCARD;
                    }
                    pool.setRedirects(redirects);

                    pool.setUpdateTime(System.currentTimeMillis());
                    pool.setStatus(Pool.Status.New);

                    _poolService.commit(pool);

                    List<User> assignedUserList = createPoolSpec.getUsers();
                    if (assignedUserList != null && !assignedUserList.isEmpty()) {
                        _userService.assignUsersToPool(pool, assignedUserList);
                    }

                    if (callback != null) {
                        callback.onSuccess(pool);
                    }
                } catch (Exception e) {
                    LOGGER.error(String.format("Failed to create pool \"%s\"", createPoolSpec.getName()), e);

                    if (callback != null) {
                        callback.onFailure(e);
                    }
                }

                return null;
            }
        }, null);
    }

    @Override
    public void updatePool(final EditPoolSpec editPoolSpec, final AsyncCallback callback) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("updatePool - poolName=%s", editPoolSpec.getName()));
        }

        _asyncExecutor.run(new AsyncExecutor.Job() {
            @Override
            public Object run() {
                Pool pool = _poolService.getByUuid(editPoolSpec.getPoolUuid());
                if (pool == null) {
                    throw new RuntimeException(String.format("Null object for pool \"%s\"", editPoolSpec.getName()));
                }

                pool.setDescription(editPoolSpec.getDescription());
                pool.setEnabled(editPoolSpec.isEnabled());
                pool.setMaxSize(editPoolSpec.getMaxSize());
                if (Pool.Type.Persistent != pool.getType()) {
                    pool.setMinSize(editPoolSpec.getMinSize());
                }

                long protocols = 0;
                if (editPoolSpec.isSupportRdp()) {
                    protocols |= Pool.PROTOCOL_RDP;
                }
                pool.setProtocols(protocols);

                long redirects = 0;
                if (editPoolSpec.isRedirectDrives()) {
                    redirects |= Pool.REDIRECT_DRIVE;
                }
                if (editPoolSpec.isRedirectPrinters()) {
                    redirects |= Pool.REDIRECT_PRINTER;
                }
                if (editPoolSpec.isRedirectComPorts()) {
                    redirects |= Pool.REDIRECT_COMPORT;
                }
                if (editPoolSpec.isRedirectSmartCards()) {
                    redirects |= Pool.REDIRECT_SMARTCARD;
                }
                pool.setRedirects(redirects);

                pool.setUpdateTime(System.currentTimeMillis());
                _poolService.commit(pool);

                if (callback != null) {
                    callback.onSuccess(null);
                }

                return null;
            }
        }, null);
    }

    @Override
    public void deletePool(final PoolSummary poolSummary, final AsyncCallback callback) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("deletePool - poolName=%s", poolSummary.getName()));
        }

        _asyncExecutor.run(new AsyncExecutor.Job() {
            @Override
            public Object run() {
                try {
                    Pool pool = _poolService.getByUuid(poolSummary.getUuid());
                    if (pool == null) {
                        throw new RuntimeException(String.format("Null object for pool \"%s\"", poolSummary.getName()));
                    }

                    pool.setStatus(Pool.Status.Deleting);
                    pool.setUpdateTime(System.currentTimeMillis());
                    _poolService.commit(pool);

                    if (callback != null) {
                        callback.onSuccess(pool);
                    }
                } catch (Exception e) {
                    LOGGER.error(String.format("Failed to create pool \"%s\"", poolSummary.getName()), e);

                    if (callback != null) {
                        callback.onFailure(e);
                    }
                }

                return null;
            }
        }, null);
    }

    @Override
    public void recomposePool(final PoolSummary pool, final boolean force, final AsyncCallback callback) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("recomposePool - poolName=%s", pool.getName()));
        }

        _asyncExecutor.run(new AsyncExecutor.Job() {
            @Override
            public Object run() {
                _poolService.recomposeDesktops(pool.getPool(), force, null);

                if (callback != null) {
                    callback.onSuccess(null);
                }

                return null;
            }
        }, null);
    }

    @Override
    public void rebalancePool(final PoolSummary pool, final AsyncCallback callback) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("rebalancePool - poolName=%s", pool.getName()));
        }

        _asyncExecutor.run(new AsyncExecutor.Job() {
            @Override
            public Object run() {
                _poolService.rebalanceDesktops(pool.getPool(), null);

                if (callback != null) {
                    callback.onSuccess(null);
                }

                return null;
            }
        }, null);
    }

    @Override
    public void getDesktops(final int first, final int max, final String orderBy, final boolean ascending,
            boolean includeDerived, AsyncCallback callback) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("getVmInstances - first=%d, max=%d, orderBy=%s, ascending=%s",
                    first, max, orderBy, ascending ? "true" : "false"));
        }

        _asyncExecutor.run(new AsyncExecutor.Job() {
            @Override
            public Object run() {
                long count = _vmInstanceService.count();
                List<? extends VmInstance> vmInstanceList =
                        _vmInstanceService.getAll(new Pagination(first, max, orderBy, ascending));

                List<DesktopSummary> desktopSumList = new ArrayList<DesktopSummary>();

                HashMap<String, User> uuidUserMap = new HashMap<String, User>();
                HashMap<String, Pool> uuidPoolMap = new HashMap<String, Pool>();
                HashMap<String, Host> uuidHostMap = new HashMap<String, Host>();
                HashMap<String, Storage> uuidStorageMap = new HashMap<String, Storage>();

                for (VmInstance vmInstance : vmInstanceList) {
                    String userUuid = vmInstance.getUserUuid();
                    User user = null;
                    if (!StringUtils.isEmpty(userUuid)) {
                        user = uuidUserMap.get(userUuid);
                        if (user == null) {
                            user = _userService.getByUuid(userUuid);
                            if (user != null) {
                                uuidUserMap.put(userUuid, user);
                            }
                        }
                    }

                    String poolUuid = vmInstance.getPoolUuid();
                    Pool pool = uuidPoolMap.get(poolUuid);
                    if (pool == null) {
                        if (StringUtils.isEmpty(poolUuid)) {
                            LOGGER.warn(String.format("Empty pool uuid for vmInstance \"%s\"", vmInstance.getName()));
                        } else {
                            pool = _poolService.getByUuid(poolUuid);
                            if (pool == null) {
                                LOGGER.warn(String.format("Unable to find pool info for uuid \"%s\"", poolUuid));
                            } else {
                                uuidPoolMap.put(poolUuid, pool);
                            }
                        }
                    }

                    String hostUuid = vmInstance.getHostUuid();
                    Host host = uuidHostMap.get(hostUuid);
                    if (host == null) {
                        if (StringUtils.isEmpty((hostUuid))) {
                            LOGGER.warn(String.format("Empty host uuid for vmInstance \"%s\"", vmInstance.getName()));
                        } else {
                            host = _hostService.getByUuid(hostUuid);
                            if (host == null) {
                                LOGGER.warn(String.format("Unable to find host info for uuid \"%s\"", poolUuid));
                            } else {
                                uuidHostMap.put(hostUuid, host);
                            }
                        }
                    }

                    String storageUuid = vmInstance.getStorageUuid();
                    Storage storage = uuidStorageMap.get(storageUuid);
                    if (storage == null) {
                        if (StringUtils.isEmpty(storageUuid)) {
                            //LOGGER.warn(String.format("Empty storage uuid for vmInstance \"%s\"", vmInstance.getName()));
                        } else {
                            storage = _storageService.getByUuid(storageUuid);
                            if (storage == null) {
                                LOGGER.warn(String.format("Unable to find storage info for uuid \"%s\"", storageUuid));
                            } else {
                                uuidStorageMap.put(storageUuid, storage);
                            }
                        }
                    }

                    DesktopSummary desktopSummary = new DesktopSummary(vmInstance, user, pool, host, storage);
                    desktopSumList.add(desktopSummary);
                }

                return new Pair<Long, List<DesktopSummary>>(count, desktopSumList);
            }
        }, callback);
    }

    @Override
    public void refreshDesktop(final DesktopSummary desktop, final AsyncCallback callback) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("refreshDesktop - desktopName=%s", desktop.getName()));
        }

        final VmInstance vmInstance = desktop.getVmInstance();

        _asyncExecutor.run(new AsyncExecutor.Job() {
            @Override
            public Object run() {
                try {
                    if (vmInstance == null) {
                        throw new Exception(String.format("Empty vmInstance uuid for desktop \"%s\"", desktop.getName()));
                    }

                    Pool pool = null;
                    if ((pool = _poolService.getByUuid(vmInstance.getPoolUuid())) == null) {
                        throw new Exception(String.format("No pool found for desktop \"%s\"", desktop.getName()));
                    }

                    VmInstance vmInstance2 = _vmInstanceService.getByUuid(vmInstance.getUuid());
                    if (vmInstance2 == null
                            || VmInstance.Status.Destroying == vmInstance2.getStatus()
                            || VmInstance.Status.Destroyed == vmInstance2.getStatus()) {
                        throw new Exception(String.format("Unable to find desktop \"%s\"", desktop.getName()));
                    }

                    if (Pool.Type.Persistent != pool.getType()) {
                        vmInstance2.setUserUuid("");
                    }
                    _vmInstanceService.refreshVm(pool, vmInstance2, callback);
                } catch (Exception e) {
                    LOGGER.error("Failed to refresh desktop", e);

                    if (callback != null) {
                        callback.onFailure(e);
                    }
                }

                return null;
            }
        }, null);
    }

    @Override
    public void deleteDesktop(final DesktopSummary desktop, final boolean rebuild, final AsyncCallback callback) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("deleteDesktop - desktopName=%s", desktop.getName()));
        }

        final VmInstance vmInstance = desktop.getVmInstance();

        _asyncExecutor.run(new AsyncExecutor.Job() {
            @Override
            public Object run() {
                if (vmInstance == null) {
                    LOGGER.warn(String.format("Empty vmInstance for desktop \"%s\"", desktop.getName()));
                    if (callback != null) {
                        callback.onSuccess(null);
                    }
                    return null;
                }

                try {
                    VmInstance vmInstance2 = _vmInstanceService.getByUuid(vmInstance.getUuid());
                    if (vmInstance2 == null
                            || VmInstance.Status.Destroying == vmInstance2.getStatus()
                            || VmInstance.Status.Destroyed == vmInstance2.getStatus()) {
                        // vmInstance has already been deleted or is being deleted.
                        if (rebuild && VmInstance.Status.Destroying == vmInstance2.getStatus()) {
                            vmInstance2.setRecompose(true);
                            _vmInstanceService.commit(vmInstance2);
                        }

                        if (callback != null) {
                            callback.onSuccess(null);
                        }
                        return null;
                    }

                    vmInstance2.setRecompose(rebuild);
                    vmInstance2.setLastError(null);
                    _vmInstanceService.deleteVm(null, vmInstance2, callback);
                } catch (Exception e) {
                    LOGGER.error("Failed to delete desktop", e);

                    if (callback != null) {
                        callback.onFailure(e);
                    }
                }

                return null;
            }
        }, null);
    }

    @Override
    public void unassignDesktop(final DesktopSummary desktop, final AsyncCallback callback) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("unassignDesktop - desktopName=%s", desktop.getName()));
        }

        final VmInstance vmInstance = desktop.getVmInstance();

        _asyncExecutor.run(new AsyncExecutor.Job() {
            @Override
            public Object run() {
                if (vmInstance == null) {
                    LOGGER.warn(String.format("Empty vmInstance uuid for desktop \"%s\"", desktop.getName()));
                    if (callback != null) {
                        callback.onSuccess(null);
                    }
                    return null;
                }

                try {
                    VmInstance vmInstance2 = _vmInstanceService.getByUuid(vmInstance.getUuid());
                    if (vmInstance2 == null
                            || VmInstance.Status.Destroying == vmInstance2.getStatus()
                            || VmInstance.Status.Destroyed == vmInstance2.getStatus()) {
                        // vmInstance has already been deleted or is being deleted.
                        if (callback != null) {
                            callback.onSuccess(null);
                        }
                        return null;
                    }

                    switch (vmInstance2.getStatus()) {
                    case Assigning:
                    case Assigned:
                    case InSession:
                        throw new Exception("The desktop is being used");
                    }

                    _vmInstanceService.unassignVm(vmInstance2, callback);
                } catch (Exception e) {
                    LOGGER.error("Failed to unassign desktop", e);

                    if (callback != null) {
                        callback.onFailure(e);
                    }
                }

                return null;
            }
        }, null);
    }

    @Override
    public void disconnectDesktop(final VmInstance vmInstance, final boolean logoff, final boolean isNow,
            final AsyncCallback callback) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("disconnectDesktop - desktopName=%s", vmInstance.getName()));
        }

        _asyncExecutor.run(new AsyncExecutor.Job() {
            @Override
            public Object run() {
                if (vmInstance == null) {
                    //LOGGER.warn(String.format("Empty vmInstance for desktop \"%s\"", vmInstance.getName()));
                    if (callback != null) {
                        callback.onSuccess(null);
                    }
                    return null;
                }

                try {
                    VmInstance vmInstance2 = _vmInstanceService.getByUuid(vmInstance.getUuid());
                    if (vmInstance2 == null
                            || VmInstance.Status.InSession != vmInstance2.getStatus()) {
                        // vmInstance has already been deleted or is being deleted.
                        LOGGER.warn(String.format("Desktop \"%s\" is not in session", vmInstance.getName()));
                        if (callback != null) {
                            callback.onSuccess(null);
                        }
                        return null;
                    }

                    if (!logoff) {
                        vmInstance2.setLogOffMode(
                                isNow ? VmInstance.LogOffMode.DisconnectNow : VmInstance.LogOffMode.Disconnect);
                    } else {
                        vmInstance2.setLogOffMode(
                                isNow ? VmInstance.LogOffMode.LogOffNow : VmInstance.LogOffMode.LogOff);
                    }
                    vmInstance2.setUpdateTime(System.currentTimeMillis());
                    _vmInstanceService.commit(vmInstance2);
                } catch (Exception e) {
                    LOGGER.error("Failed to delete desktop", e);

                    if (callback != null) {
                        callback.onFailure(e);
                    }
                }

                return null;
            }
        }, null);
    }

    @Override
    public void repairDesktop(final DesktopSummary desktopSummary, final AsyncCallback callback) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("disconnectDesktop - desktopName=%s", desktopSummary.getName()));
        }

        _asyncExecutor.run(new AsyncExecutor.Job() {
            @Override
            public Object run() {
                try {
                    VmInstance vmInstance = _vmInstanceService.getByUuid(desktopSummary.getUuid());
                    if (vmInstance == null
                            || VmInstance.Status.Destroying == vmInstance.getStatus()
                            || VmInstance.Status.Destroyed == vmInstance.getStatus()) {
                        // vmInstance has already been deleted or is being deleted.
                        throw new Exception(String.format("Unable to find desktop \"%s\"", desktopSummary.getName()));
                    }

                    VmInstance.Error lastError = vmInstance.getLastError();
                    if (lastError == null) {
                        lastError = VmInstance.Error.UnknownError;
                    }

                    switch (lastError) {
                    case PowerOnError:
                        _vmInstanceService.powerOnVm(null, vmInstance, null);
                        break;

                    case PowerOffError:
                        _vmInstanceService.powerOffVm(null, vmInstance, null);
                        break;

                    case DeleteError:
                        _vmInstanceService.deleteVm(null, vmInstance, null);
                        break;

                    default:
                        throw new Exception("Unable to repair this desktop. Please rebuild it.");
                    }

                    if (callback != null) {
                        callback.onSuccess(null);
                    }
                } catch (Exception e) {
                    LOGGER.error(String.format("Failed to repair desktop \"%s\"", desktopSummary.getName()), e);

                    if (callback != null) {
                        callback.onFailure(e);
                    }
                }

                return null;
            }
        }, null);
    }
}

