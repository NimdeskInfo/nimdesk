/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.ui.server.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.nimdesk.event.AsyncCallback;
import com.nimdesk.event.RequestDesktopEvent;
import com.nimdesk.gateway.Gateway;
import com.nimdesk.model.Host;
import com.nimdesk.model.Pool;
import com.nimdesk.model.VmInstance;
import com.nimdesk.service.AsyncExecutor;
import com.nimdesk.service.EventBusService;
import com.nimdesk.ui.server.VdiService;

@Service(VdiService.BEAN_NAME)
public class VdiServiceImpl implements VdiService, InitializingBean {
    private static final Logger LOGGER = Logger.getLogger(VdiServiceImpl.class);

    @Autowired
    private EventBusService _eventBusService;

    @Autowired
    private AsyncExecutor _asyncExecutor;

    @Autowired
    private Host.Service _hostService;

    @Autowired
    private Pool.Service _poolService;

    @Autowired
    private VmInstance.Service _vmInstanceService;

    @Autowired
    private Gateway _gateway;

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(_eventBusService, "An EventBusService must be set");
        Assert.notNull(_asyncExecutor, "An AsyncExecutor must be set");
        Assert.notNull(_hostService, "A Host.Service must be set");
        Assert.notNull(_vmInstanceService, "A VmInstance.Service must be set");
        Assert.notNull(_poolService, "A Pool.Service must be set");
    }

    @Override
    public void requestDesktop(final Pool pool, final String userUuid, final AsyncCallback callback) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("requestDesktop - pool=\"%s\", userUuid=%s", pool.getName(), userUuid));
        }

        RequestDesktopEvent event = new RequestDesktopEvent(pool, userUuid, callback);
        _eventBusService.post(event);
    }

    @Override
    public void disconnectDesktop(Pool pool, VmInstance desktop, AsyncCallback callback) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("disconnectDesktop - pool=\"%s\", vmInstance=%s", pool.getName(), desktop.getName()));
        }

        desktop = _vmInstanceService.getByUuid(desktop.getUuid());
        if (desktop != null) {
            if (Pool.Type.Persistent == pool.getType()) {
                desktop.setLogOffMode(VmInstance.LogOffMode.DisconnectNow);
            } else {
                desktop.setLogOffMode(VmInstance.LogOffMode.LogOffNow);
            }
            desktop.setUpdateTime(System.currentTimeMillis());
            _vmInstanceService.commit(desktop);
        }
    }

    @Override
    public int startSession(String address, int port) {
        return _gateway.startSession(address, port);
    }
}
