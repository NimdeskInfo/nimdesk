/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.ui.server.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.nimdesk.event.AsyncCallback;
import com.nimdesk.model.EventLog;
import com.nimdesk.model.Pool;
import com.nimdesk.model.Task;
import com.nimdesk.model.VmImage;
import com.nimdesk.model.VmInstance;
import com.nimdesk.service.AsyncExecutor;
import com.nimdesk.ui.server.EventLogSummary;
import com.nimdesk.ui.server.TaskEventService;
import com.nimdesk.ui.server.TaskSummary;
import com.nimdesk.util.Pagination;
import com.nimdesk.util.Pair;

@Service(TaskEventService.BEAN_NAME)
public class TaskEventServiceImpl implements TaskEventService, InitializingBean {
    private static final Logger LOGGER = Logger.getLogger(TaskEventServiceImpl.class);

    @Autowired
    private AsyncExecutor _asyncExecutor;

    @Autowired
    private Task.Service _taskService;

    @Autowired
    private EventLog.Service _eventLogService;

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(_asyncExecutor, "An AsyncExecutor must be set");
        Assert.notNull(_taskService, "A Task.Service must be set");
        Assert.notNull(_eventLogService, "An EventLog.Service must be set");
    }

    @Override
    public void getAllTasks(int first, int max, String orderBy, boolean ascending,
            AsyncCallback callback) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("getAllTasks - first=%d, max=%d, orderBy=%s, ascending=%s",
                    first, max, orderBy, ascending ? "true" : "false"));
        }

        final Pagination pagination = new Pagination(first, max, orderBy, ascending);

        _asyncExecutor.run(new AsyncExecutor.Job() {
            @Override
            public Object run() {
                long count = _taskService.count();
                List<? extends Task> taskList =
                        _taskService.getAll(pagination);

                List<TaskSummary> taskSumList = new ArrayList<TaskSummary>();

                for (Task task : taskList) {
                    TaskSummary taskSummary = new TaskSummary(task, null);
                    taskSumList.add(taskSummary);
                }

                return new Pair<Long, List<TaskSummary>>(count, taskSumList);
            }
        }, callback);
    }

    @Override
    public void getAllPoolTasks(int first, int max, String orderBy, boolean ascending,
            AsyncCallback callback) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("getAllPoolTasks - first=%d, max=%d, orderBy=%s, ascending=%s",
                    first, max, orderBy, ascending ? "true" : "false"));
        }

        final Pagination pagination = new Pagination(first, max, orderBy, ascending);

        _asyncExecutor.run(new AsyncExecutor.Job() {
            @Override
            public Object run() {
                long count = _taskService.countAllPoolTasks();
                List<? extends Task> taskList =
                        _taskService.getAllPoolTasks(pagination);

                String poolType = Pool.class.getSimpleName();

                List<TaskSummary> taskSumList = new ArrayList<TaskSummary>();

                for (Task task : taskList) {
                    TaskSummary taskSummary = new TaskSummary(task, poolType);
                    taskSumList.add(taskSummary);
                }

                return new Pair<Long, List<TaskSummary>>(count, taskSumList);
            }
        }, callback);
    }

    @Override
    public void getPoolTasks(final String poolUuid, int first, int max, String orderBy, boolean ascending,
            AsyncCallback callback) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("getPoolTasks - pool=%s, first=%d, max=%d, orderBy=%s, ascending=%s",
                    poolUuid, first, max, orderBy, ascending ? "true" : "false"));
        }

        final Pagination pagination = new Pagination(first, max, orderBy, ascending);

        _asyncExecutor.run(new AsyncExecutor.Job() {
            @Override
            public Object run() {
                long count = _taskService.countPoolTasks(poolUuid);
                List<? extends Task> taskList =
                        _taskService.getPoolTasks(poolUuid, pagination);

                String poolType = Pool.class.getSimpleName();

                List<TaskSummary> taskSumList = new ArrayList<TaskSummary>();

                for (Task task : taskList) {
                    TaskSummary taskSummary = new TaskSummary(task, poolType);
                    taskSumList.add(taskSummary);
                }

                return new Pair<Long, List<TaskSummary>>(count, taskSumList);
            }
        }, callback);
    }

    @Override
    public void getAllImageTasks(int first, int max, String orderBy, boolean ascending,
            AsyncCallback callback) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("getAllImageTasks - first=%d, max=%d, orderBy=%s, ascending=%s",
                    first, max, orderBy, ascending ? "true" : "false"));
        }

        final Pagination pagination = new Pagination(first, max, orderBy, ascending);

        _asyncExecutor.run(new AsyncExecutor.Job() {
            @Override
            public Object run() {
                long count = _taskService.countAllVmImageTasks();
                List<? extends Task> taskList =
                        _taskService.getAllVmImageTasks(pagination);

                String imageType = VmImage.class.getSimpleName();

                List<TaskSummary> taskSumList = new ArrayList<TaskSummary>();

                for (Task task : taskList) {
                    TaskSummary taskSummary = new TaskSummary(task, imageType);
                    taskSumList.add(taskSummary);
                }

                return new Pair<Long, List<TaskSummary>>(count, taskSumList);
            }
        }, callback);
    }

    @Override
    public void getImageTasks(final String imageUuid, int first, int max, String orderBy, boolean ascending,
            AsyncCallback callback) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("getImageTasks - vmImage=%s, first=%d, max=%d, orderBy=%s, ascending=%s",
                    imageUuid, first, max, orderBy, ascending ? "true" : "false"));
        }

        final Pagination pagination = new Pagination(first, max, orderBy, ascending);

        _asyncExecutor.run(new AsyncExecutor.Job() {
            @Override
            public Object run() {
                long count = _taskService.countVmImageTasks(imageUuid);
                List<? extends Task> taskList =
                        _taskService.getVmImageTasks(imageUuid, pagination);

                String imageType = VmImage.class.getSimpleName();

                List<TaskSummary> taskSumList = new ArrayList<TaskSummary>();

                for (Task task : taskList) {
                    TaskSummary taskSummary = new TaskSummary(task, imageType);
                    taskSumList.add(taskSummary);
                }

                return new Pair<Long, List<TaskSummary>>(count, taskSumList);
            }
        }, callback);
    }

    @Override
    public void getAllDesktopTasks(int first, int max, String orderBy, boolean ascending,
            AsyncCallback callback) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("getAllDesktopTasks - first=%d, max=%d, orderBy=%s, ascending=%s",
                    first, max, orderBy, ascending ? "true" : "false"));
        }

        final Pagination pagination = new Pagination(first, max, orderBy, ascending);

        _asyncExecutor.run(new AsyncExecutor.Job() {
            @Override
            public Object run() {
                long count = _taskService.countAllVmInstanceTasks();
                List<? extends Task> taskList =
                        _taskService.getAllVmInstanceTasks(pagination);

                String desktopType = VmInstance.class.getSimpleName();

                List<TaskSummary> taskSumList = new ArrayList<TaskSummary>();

                for (Task task : taskList) {
                    TaskSummary taskSummary = new TaskSummary(task, desktopType);
                    taskSumList.add(taskSummary);
                }

                return new Pair<Long, List<TaskSummary>>(count, taskSumList);
            }
        }, callback);
    }

    @Override
    public void getDesktopTasks(final String desktopUuid, int first, int max, String orderBy, boolean ascending,
            AsyncCallback callback) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("getDesktopTasks - vmInstance=%s, first=%d, max=%d, orderBy=%s, ascending=%s",
                    desktopUuid, first, max, orderBy, ascending ? "true" : "false"));
        }

        final Pagination pagination = new Pagination(first, max, orderBy, ascending);

        _asyncExecutor.run(new AsyncExecutor.Job() {
            @Override
            public Object run() {
                long count = _taskService.countVmInstanceTasks(desktopUuid);
                List<? extends Task> taskList =
                        _taskService.getVmInstanceTasks(desktopUuid, pagination);

                String desktopType = VmInstance.class.getSimpleName();

                List<TaskSummary> taskSumList = new ArrayList<TaskSummary>();

                for (Task task : taskList) {
                    TaskSummary taskSummary = new TaskSummary(task, desktopType);
                    taskSumList.add(taskSummary);
                }

                return new Pair<Long, List<TaskSummary>>(count, taskSumList);
            }
        }, callback);
    }

    @Override
    public void getAllEvents(int first, int max, String orderBy, boolean ascending,
            AsyncCallback callback) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("getAllEvents - first=%d, max=%d, orderBy=%s, ascending=%s",
                    first, max, orderBy, ascending ? "true" : "false"));
        }

        final Pagination pagination = new Pagination(first, max, orderBy, ascending);

        _asyncExecutor.run(new AsyncExecutor.Job() {
            @Override
            public Object run() {
                long count = _eventLogService.count();
                List<? extends EventLog> eventList =
                        _eventLogService.getAll(pagination);

                List<EventLogSummary> eventSumList = new ArrayList<EventLogSummary>();

                for (EventLog eventLog : eventList) {
                    EventLogSummary eventSummary = new EventLogSummary(eventLog);
                    eventSumList.add(eventSummary);
                }

                return new Pair<Long, List<EventLogSummary>>(count, eventSumList);
            }
        }, callback);
    }

    @Override
    public void getAllPoolEvents(int first, int max, String orderBy, boolean ascending,
            AsyncCallback callback) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("getAllPoolEvents - first=%d, max=%d, orderBy=%s, ascending=%s",
                    first, max, orderBy, ascending ? "true" : "false"));
        }

        final Pagination pagination = new Pagination(first, max, orderBy, ascending);

        _asyncExecutor.run(new AsyncExecutor.Job() {
            @Override
            public Object run() {
                long count = _eventLogService.countAllPoolEvents();
                List<? extends EventLog> eventList =
                        _eventLogService.getAllPoolEvents(pagination);

                List<EventLogSummary> eventSumList = new ArrayList<EventLogSummary>();

                for (EventLog eventLog : eventList) {
                    EventLogSummary eventSummary = new EventLogSummary(eventLog);
                    eventSumList.add(eventSummary);
                }

                return new Pair<Long, List<EventLogSummary>>(count, eventSumList);
            }
        }, callback);
    }

    @Override
    public void getPoolEvents(final String poolUuid, int first, int max, String orderBy, boolean ascending,
            AsyncCallback callback) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("getPoolEvents - pool=%s, first=%d, max=%d, orderBy=%s, ascending=%s",
                    poolUuid, first, max, orderBy, ascending ? "true" : "false"));
        }

        final Pagination pagination = new Pagination(first, max, orderBy, ascending);

        _asyncExecutor.run(new AsyncExecutor.Job() {
            @Override
            public Object run() {
                long count = _eventLogService.countPoolEvents(poolUuid);
                List<? extends EventLog> eventList =
                        _eventLogService.getPoolEvents(poolUuid, pagination);

                List<EventLogSummary> eventSumList = new ArrayList<EventLogSummary>();

                for (EventLog eventLog : eventList) {
                    EventLogSummary eventSummary = new EventLogSummary(eventLog);
                    eventSumList.add(eventSummary);
                }

                return new Pair<Long, List<EventLogSummary>>(count, eventSumList);
            }
        }, callback);
    }

    @Override
    public void getAllImageEvents(int first, int max, String orderBy, boolean ascending,
            AsyncCallback callback) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("getAllImageEvents - first=%d, max=%d, orderBy=%s, ascending=%s",
                    first, max, orderBy, ascending ? "true" : "false"));
        }

        final Pagination pagination = new Pagination(first, max, orderBy, ascending);

        _asyncExecutor.run(new AsyncExecutor.Job() {
            @Override
            public Object run() {
                long count = _eventLogService.countAllVmImageEvents();
                List<? extends EventLog> eventList =
                        _eventLogService.getAllVmImageEvents(pagination);

                List<EventLogSummary> eventSumList = new ArrayList<EventLogSummary>();

                for (EventLog eventLog : eventList) {
                    EventLogSummary eventSummary = new EventLogSummary(eventLog);
                    eventSumList.add(eventSummary);
                }

                return new Pair<Long, List<EventLogSummary>>(count, eventSumList);
            }
        }, callback);
    }

    @Override
    public void getImageEvents(final String imageUuid, int first, int max, String orderBy, boolean ascending,
            AsyncCallback callback) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("getImageEvents - vmImage=%s, first=%d, max=%d, orderBy=%s, ascending=%s",
                    imageUuid, first, max, orderBy, ascending ? "true" : "false"));
        }

        final Pagination pagination = new Pagination(first, max, orderBy, ascending);

        _asyncExecutor.run(new AsyncExecutor.Job() {
            @Override
            public Object run() {
                long count = _eventLogService.countVmImageEvents(imageUuid);
                List<? extends EventLog> eventList =
                        _eventLogService.getVmImageEvents(imageUuid, pagination);

                List<EventLogSummary> eventSumList = new ArrayList<EventLogSummary>();

                for (EventLog eventLog : eventList) {
                    EventLogSummary eventSummary = new EventLogSummary(eventLog);
                    eventSumList.add(eventSummary);
                }

                return new Pair<Long, List<EventLogSummary>>(count, eventSumList);
            }
        }, callback);
    }

    @Override
    public void getAllDesktopEvents(int first, int max, String orderBy, boolean ascending,
            AsyncCallback callback) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("getAllDesktopEvents - first=%d, max=%d, orderBy=%s, ascending=%s",
                    first, max, orderBy, ascending ? "true" : "false"));
        }

        final Pagination pagination = new Pagination(first, max, orderBy, ascending);

        _asyncExecutor.run(new AsyncExecutor.Job() {
            @Override
            public Object run() {
                long count = _eventLogService.countAllVmInstanceEvents();
                List<? extends EventLog> eventList =
                        _eventLogService.getAllVmInstanceEvents(pagination);

                List<EventLogSummary> eventSumList = new ArrayList<EventLogSummary>();

                for (EventLog eventLog : eventList) {
                    EventLogSummary eventSummary = new EventLogSummary(eventLog);
                    eventSumList.add(eventSummary);
                }

                return new Pair<Long, List<EventLogSummary>>(count, eventSumList);
            }
        }, callback);
    }

    @Override
    public void getDesktopEvents(final String desktopUuid, int first, int max, String orderBy, boolean ascending,
            AsyncCallback callback) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("getDesktopEvents - vmInstance=%s, first=%d, max=%d, orderBy=%s, ascending=%s",
                    desktopUuid, first, max, orderBy, ascending ? "true" : "false"));
        }

        final Pagination pagination = new Pagination(first, max, orderBy, ascending);

        _asyncExecutor.run(new AsyncExecutor.Job() {
            @Override
            public Object run() {
                long count = _eventLogService.countVmInstanceEvents(desktopUuid);
                List<? extends EventLog> eventList =
                        _eventLogService.getVmInstanceEvents(desktopUuid, pagination);

                List<EventLogSummary> eventSumList = new ArrayList<EventLogSummary>();

                for (EventLog eventLog : eventList) {
                    EventLogSummary eventSummary = new EventLogSummary(eventLog);
                    eventSumList.add(eventSummary);
                }

                return new Pair<Long, List<EventLogSummary>>(count, eventSumList);
            }
        }, callback);
    }
}
