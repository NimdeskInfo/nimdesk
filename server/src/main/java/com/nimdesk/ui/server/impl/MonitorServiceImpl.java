/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.ui.server.impl;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.nimdesk.model.Host;
import com.nimdesk.model.VmInstance;
import com.nimdesk.monitor.MonitorController;
import com.nimdesk.ui.server.MonitorService;
import com.nimdesk.util.chart.ChartData;

@Service(MonitorService.BEAN_NAME)
public class MonitorServiceImpl implements MonitorService, InitializingBean {
    //private static final Logger LOGGER = Logger.getLogger(MonitorServiceImpl.class);

    @Autowired
    private MonitorController _monitorController;

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(_monitorController, "A MonitorController must be set");
    }

    @Override
    public ChartData getHostStatus(Host host, long statusId, String dateRange) {
        return _monitorController.getHostStatus(host, statusId, dateRange);
    }

    @Override
    public ChartData getHostDatastoreStatus(Host host, String datastoreRefId, String dateRange) {
        return _monitorController.getHostDatastoreStatus(host, datastoreRefId, dateRange);
    }

    @Override
    public ChartData getVmInstanceStatus(VmInstance vmInstance, long statusId, String dateRange) {
        return _monitorController.getVmInstanceStatus(vmInstance, statusId, dateRange);
    }
}
