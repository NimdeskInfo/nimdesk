/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.ui.server.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.nimdesk.event.AsyncCallback;
import com.nimdesk.event.AuthenticateLdapUserEvent;
import com.nimdesk.event.ConnectLdapEvent;
import com.nimdesk.event.FindLdapUsersEvent;
import com.nimdesk.ldap.LdapContext;
import com.nimdesk.ldap.LdapUser;
import com.nimdesk.model.Host;
import com.nimdesk.model.Pool;
import com.nimdesk.model.PoolUser;
import com.nimdesk.model.User;
import com.nimdesk.model.UserGroup;
import com.nimdesk.model.UserSession;
import com.nimdesk.model.VmInstance;
import com.nimdesk.service.AsyncExecutor;
import com.nimdesk.service.EventBusService;
import com.nimdesk.ui.server.LocalUserSpec;
import com.nimdesk.ui.server.UserAccountService;
import com.nimdesk.ui.server.UserSessionSummary;
import com.nimdesk.ui.server.UserSummary;
import com.nimdesk.util.Pagination;
import com.nimdesk.util.Pair;

@Service(UserAccountService.BEAN_NAME)
public class UserAccountServiceImpl implements UserAccountService, InitializingBean {
    private static final Logger LOGGER = Logger.getLogger(UserAccountServiceImpl.class);

    @Autowired
    private EventBusService _eventBusService;

    @Autowired
    private AsyncExecutor _asyncExecutor;

    @Autowired
    private Host.Service _hostService;

    @Autowired
    private User.Service _userService;

    @Autowired
    private UserGroup.Service _userGroupService;

    @Autowired
    private Pool.Service _poolService;

    @Autowired
    private PoolUser.Service _poolUserService;

    @Autowired
    private VmInstance.Service _vmInstanceService;

    @Autowired
    private UserSession.Service _userSessionService;

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(_eventBusService, "An EventBusService must be set");
        Assert.notNull(_asyncExecutor, "An AsyncExecutor must be set");
        Assert.notNull(_hostService, "A Host.Service must be set");
        Assert.notNull(_userService, "A User.Service must be set");
        Assert.notNull(_userGroupService, "A UserGroup.Service must be set");
        Assert.notNull(_poolService, "A Pool.Service must be set");
        Assert.notNull(_poolUserService, "A PoolUser.Service must be set");
        Assert.notNull(_vmInstanceService, "A VmInstance.Service must be set");
        Assert.notNull(_userSessionService, "A UserSession.Service must be set");
    }

    @Override
    public void listDomains(AsyncCallback callback) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("listDomains");
        }

        _asyncExecutor.run(new AsyncExecutor.Job() {
            @Override
            public Object run() {
                List<String> domainList = _userService.getAllDomains();
                return domainList;
            }
        }, callback);
    }

    @Override
    public void connectLdapService(String providerUrl, String domain, String username, String password,
            AsyncCallback callback) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("connectLdapService - url=%s, username=%s, domain=%s",
                    providerUrl, username, domain));
        }

        ConnectLdapEvent event = new ConnectLdapEvent(providerUrl, domain, username, password, callback);
        _eventBusService.post(event);
    }

    @Override
    public void findLdapUsers(LdapContext context, boolean includeUsers, boolean includeGroups, String prefix,
            AsyncCallback callback) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("findLdapUsers - includeUsers=%s, includeGroups=%s, prefix=%s",
                    includeUsers, includeGroups, prefix));
        }

        FindLdapUsersEvent event = new FindLdapUsersEvent(context, includeUsers, includeGroups, prefix, callback);
        _eventBusService.post(event);
    }

    @Override
    public void authenticateLdaUser(LdapContext context, String username, String password,
            AsyncCallback callback) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("authenticateLdaUser - username=%s, domain=%s",
                    username, context.getDomain()));
        }

        AuthenticateLdapUserEvent event = new AuthenticateLdapUserEvent(context, username, password, callback);
        _eventBusService.post(event);
    }

    @Override
    public void authenticateWorkGroupUser(final String username, final String password,
            AsyncCallback callback) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("authenticateWorkGroupUser - username=%s", username));
        }

        _asyncExecutor.run(new AsyncExecutor.Job() {
            @Override
            public Object run() {
                User user = _userService.getByLogonNameDomain(username, "");
                if (user == null) {
                    throw new RuntimeException(String.format("User \"%s\" doesn't exist", username));
                }
                if (StringUtils.isEmpty(user.getPassword())) {
                    throw new RuntimeException(String.format("No password set for user \"%s\"", username));
                }
                if (!StringUtils.equals(user.getPassword(), password)) {
                    throw new RuntimeException(String.format("Unable to authenticate user \"%s\"", username));
                }

                return user;
            }
        }, callback);
    }

    @Override
    public User syncLdapUserWithDb(LdapUser ldapUser) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("syncLdapUserWithDb - username=%s", ldapUser.getLogonName() + "@" + ldapUser.getDomain()));
        }

        User user = _userService.getByDistinguishedName(ldapUser.getDistinguishedName());
        if (user == null) {
            user = _userService.newInstance();
            user.setType(ldapUser.isUser() ? User.Type.User : User.Type.Group);
            user.setName(ldapUser.getName());
            if (!ldapUser.isUser() && StringUtils.isEmpty(ldapUser.getLogonName())) {
                user.setLogonName(ldapUser.getName());
            } else {
                user.setLogonName(ldapUser.getLogonName());
            }
            user.setDomain(ldapUser.getDomain());
            user.setDistinguishedName(ldapUser.getDistinguishedName());
            _userService.commit(user);
        }

        return user;
    }

    @Override
    public void getUsers(final int first, final int max, final String orderBy, final boolean ascending,
            AsyncCallback callback) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("getUsers - first=%d, max=%d, orderBy=%s, ascending=%s",
                    first, max, orderBy, ascending ? "true" : "false"));
        }

        _asyncExecutor.run(new AsyncExecutor.Job() {
            @Override
            public Object run() {
                long count = _userService.count();
                List<? extends User> userList =
                        _userService.getAll(new Pagination(first, max, orderBy, ascending));

                ArrayList<UserSummary> userSumList = new ArrayList<UserSummary>();

                for (User user : userList) {
                    UserSummary userSummary = new UserSummary(user);
                    userSumList.add(userSummary);
                }

                return new Pair<Long, List<UserSummary>>(count, userSumList);
            }
        }, callback);
    }

    @Override
    public void getUsersForPool(final Pool pool, AsyncCallback callback) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("getUsersForPool - pool %s (%s)", pool.getName(), pool.getUuid()));
        }

        _asyncExecutor.run(new AsyncExecutor.Job() {
            @Override
            public Object run() {
                List<? extends User> userList = _userService.getAllUsersForPool(pool);

                ArrayList<UserSummary> userSumList = new ArrayList<UserSummary>();

                for (User user : userList) {
                    UserSummary userSummary = new UserSummary(user);
                    userSumList.add(userSummary);
                }
                return userSumList;
            }
        }, callback);
    }

    @Override
    public void updateUsersForPool(final Pool pool, final List<UserSummary> entitledUsers,
            final List<UserSummary> distitledUsers, AsyncCallback callback) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("updateUsersForPool - pool %s (%s)", pool.getName(), pool.getUuid()));
        }

        _asyncExecutor.run(new AsyncExecutor.Job() {
            @Override
            public Object run() {
                ArrayList<User> assignedUserList = new ArrayList<User>();
                if (entitledUsers != null) {
                    for (UserSummary userSummary : entitledUsers) {
                        assignedUserList.add(userSummary.getUser());
                    }
                }
                _userService.assignUsersToPool(pool, assignedUserList);

                ArrayList<User> unassignedUserList = new ArrayList<User>();
                if (distitledUsers != null) {
                    for (UserSummary userSummary : distitledUsers) {
                        unassignedUserList.add(userSummary.getUser());
                    }
                }
                _userService.unassignUsersFromPool(pool, unassignedUserList);

                return null;
            }
        }, callback);
    }

    @Override
    public void getUser(final String username, final String domain, AsyncCallback callback) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("getUser - username=%s, domain=%s", username, domain));
        }

        _asyncExecutor.run(new AsyncExecutor.Job() {
            @Override
            public Object run() {
                User user = _userService.getByLogonNameDomain(username, domain);
                return user;
            }
        }, callback);
    }

    @Override
    public void createLocalUser(final LocalUserSpec localUserSpec, AsyncCallback callback) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("createLocalUser - username=%s", localUserSpec.getLogonName()));
        }

        _asyncExecutor.run(new AsyncExecutor.Job() {
            @Override
            public Object run() {
                User user = _userService.newInstance();
                user.setType(localUserSpec.isGroup() ? User.Type.Group : User.Type.User);
                user.setName(localUserSpec.getName());
                user.setLogonName(localUserSpec.isGroup() ? localUserSpec.getName() : localUserSpec.getLogonName());
                user.setPassword(localUserSpec.isGroup() ? "" : localUserSpec.getPassword());
                user.setDomain("");
                user.setDistinguishedName(user.getLogonName());
                _userService.commit(user);

                if (!localUserSpec.isGroup()) {
                    Collection<? extends User> groups = localUserSpec.getGroups();
                    if (groups != null) {
                        for (User group : groups) {
                            UserGroup userGroup = _userGroupService.newInstance();
                            userGroup.setUserUuid(user.getUuid());
                            userGroup.setGroupUuid(group.getUuid());
                            _userGroupService.commit(userGroup);
                        }
                    }
                }

                return user;
            }
        }, callback);
    }

    @Override
    public void updateLocalUser(final LocalUserSpec localUserSpec, AsyncCallback callback) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("updateLocalUser - username=%s", localUserSpec.getLogonName()));
        }

        _asyncExecutor.run(new AsyncExecutor.Job() {
            @Override
            public Object run() {
                User user = _userService.getByLogonNameDomain(localUserSpec.getLogonName(), "");
                if (user == null) {
                    return null;
                }

                user.setType(localUserSpec.isGroup() ? User.Type.Group : User.Type.User);
                user.setName(localUserSpec.getName());
                user.setLogonName(localUserSpec.isGroup() ? localUserSpec.getName() : localUserSpec.getLogonName());
                user.setPassword(localUserSpec.isGroup() ? "" : localUserSpec.getPassword());
                user.setDomain("");
                user.setDistinguishedName(user.getLogonName());
                _userService.commit(user);

                if (!localUserSpec.isGroup()) {
                    Map<String, User> oldGroupMap = new HashMap<String, User>();
                    List<? extends User> oldGroups = _userGroupService.getGroupsForUser(user.getUuid());
                    if (oldGroups != null) {
                        for (User oldGroup : oldGroups) {
                            oldGroupMap.put(oldGroup.getUuid(), oldGroup);
                        }
                    }

                    Collection<? extends User> groups = localUserSpec.getGroups();
                    if (groups != null) {
                        for (User group : groups) {
                            if (oldGroupMap.containsKey(group.getUuid())) {
                                oldGroupMap.remove(group.getUuid());
                                continue;
                            }

                            UserGroup userGroup = _userGroupService.newInstance();
                            userGroup.setUserUuid(user.getUuid());
                            userGroup.setGroupUuid(group.getUuid());
                            _userGroupService.commit(userGroup);
                        }
                    }

                    // The rest in oldGroupMap need to be deleted
                    for (User oldGroup : oldGroupMap.values()) {
                        _userGroupService.removeGroupsFromUser(user.getUuid(), oldGroup.getUuid());
                    }
                }

                return user;
            }
        }, callback);
    }

    @Override
    public void deleteUser(final User user, AsyncCallback callback) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("deleteUser - username=%s", user.getLogonName()));
        }

        _asyncExecutor.run(new AsyncExecutor.Job() {
            @Override
            public Object run() {
                if (user.getType() == User.Type.User) {
                    _userGroupService.deleteUserGroupsForUser(user.getUuid());
                } else {
                    _userGroupService.deleteUserGroupsForGroup(user.getUuid());
                }

                _poolUserService.deleteByUser(user.getUuid());

                _userService.delete(user);

                return null;
            }
        }, callback);
    }

    @Override
    public void getPoolsForUser(final User user, AsyncCallback callback) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("getPoolsForUser - user %s (%s)", user.getName(), user.getUuid()));
        }

        _asyncExecutor.run(new AsyncExecutor.Job() {
            @Override
            public Object run() {
                Map<String, Pool> uuidPoolMap = new HashMap<String, Pool>();

                List<? extends Pool> pools = _poolService.getAllPoolsForUser(user);
                if (pools != null) {
                    for (Pool pool : pools) {
                        if (!pool.isEnabled() || Pool.Status.Deleting == pool.getStatus()) {
                            continue;
                        }

                        if (!uuidPoolMap.containsKey(pool.getUuid())) {
                            uuidPoolMap.put(pool.getUuid(), pool);
                        }
                    }
                }

                if (user.getType() == User.Type.User) {
                    // Get all the groups this user belongs to
                    List<? extends User> groups = _userGroupService.getGroupsForUser(user.getUuid());
                    if (groups != null) {
                        for (User group : groups) {
                            pools = _poolService.getAllPoolsForUser(group);
                            if (pools != null) {
                                for (Pool pool : pools) {
                                    if (!pool.isEnabled() || Pool.Status.Deleting == pool.getStatus()) {
                                        continue;
                                    }

                                    if (!uuidPoolMap.containsKey(pool.getUuid())) {
                                        uuidPoolMap.put(pool.getUuid(), pool);
                                    }
                                }
                            }
                        }
                    }
                }

                return uuidPoolMap.values();
            }
        }, callback);
    }

    @Override
    public void getPoolsForUserWithGroups(final List<LdapUser> userWithGroups, AsyncCallback callback) {
        if (userWithGroups == null || userWithGroups.isEmpty()) {
            if (callback != null) {
                callback.onFailure(new Exception("No user or group information"));
                return;
            }
        }
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("getPoolsForUserWithGroups - user %s (%s)",
                    userWithGroups.get(0).getLogonName(), userWithGroups.get(0).getDistinguishedName()));
        }

        _asyncExecutor.run(new AsyncExecutor.Job() {
            @Override
            public Object run() {
                Map<String, Pool> uuidPoolMap = new HashMap<String, Pool>();

                for (LdapUser ldapUser : userWithGroups) {
                    User user = _userService.getByDistinguishedName(ldapUser.getDistinguishedName());
                    if (user == null) {
                        //LOGGER.info(String.format("Do not found user for dn \"%s\"", ldapUser.getDistinguishedName()));
                        continue;
                    }

                    LOGGER.info(String.format("Found user for dn \"%s\" - name=%s", ldapUser.getDistinguishedName(), user.getLogonName()));

                    List<? extends Pool> pools = _poolService.getAllPoolsForUser(user);
                    if (pools != null) {
                        for (Pool pool : pools) {
                            //LOGGER.info(String.format("Found pool \"%s\" for user \"%s\"", pool.getName(), user.getLogonName()));

                            if (!pool.isEnabled() || Pool.Status.Deleting == pool.getStatus()) {
                                continue;
                            }

                            if (!uuidPoolMap.containsKey(pool.getUuid())) {
                                uuidPoolMap.put(pool.getUuid(), pool);
                            }
                        }
                    }
                }

                return uuidPoolMap.values();
            }
        }, callback);
    }

    @Override
    public UserSessionSummary createUserSessionSummary(UserSession userSession) {
        User user = null;
        String userUuid = userSession.getUserUuid();
        if (!StringUtils.isEmpty(userUuid)) {
            user = _userService.getByUuid(userUuid);
        }

        VmInstance vmInstance = null;
        String vmInstanceUuid = userSession.getVmInstanceUuid();
        if (!StringUtils.isEmpty(vmInstanceUuid)) {
            vmInstance = _vmInstanceService.getByUuid(vmInstanceUuid);
        }

        Host host = null;
        if (vmInstance != null) {
            String hostUuid = vmInstance.getHostUuid();
            if (!StringUtils.isEmpty(hostUuid)) {
                host = _hostService.getByUuid(hostUuid);
            }
        }

        UserSessionSummary userSessionSummary = new UserSessionSummary(userSession, user, vmInstance, host);
        return userSessionSummary;
    }

    @Override
    public void getAllUserSessions(int first, int max, String orderBy, boolean ascending,
            AsyncCallback callback) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("getAllUserSessions - first=%d, max=%d, orderBy=%s, ascending=%s",
                    first, max, orderBy, ascending ? "true" : "false"));
        }

        final Pagination pagination = new Pagination(first, max, orderBy, ascending);

        _asyncExecutor.run(new AsyncExecutor.Job() {
            @Override
            public Object run() {
                long count = _userSessionService.count();
                List<? extends UserSession> userSessionList =
                        _userSessionService.getAll(pagination);

                Map<String, User> uuidUserMap = new HashMap<String, User>();
                Map<String, VmInstance> uuidVmInstanceMap = new HashMap<String, VmInstance>();
                Map<String, Host> uuidHostMap = new HashMap<String, Host>();

                List<UserSessionSummary> userSessionSumList = new ArrayList<UserSessionSummary>();

                for (UserSession userSession : userSessionList) {
                    User user = null;
                    String userUuid = userSession.getUserUuid();
                    if (!StringUtils.isEmpty(userUuid)) {
                        user = uuidUserMap.get(userUuid);
                        if (user == null) {
                            user = _userService.getByUuid(userUuid);
                            if (user != null) {
                                uuidUserMap.put(userUuid, user);
                            }
                        }
                    }

                    VmInstance vmInstance = null;
                    String vmInstanceUuid = userSession.getVmInstanceUuid();
                    if (!StringUtils.isEmpty(vmInstanceUuid)) {
                        vmInstance = uuidVmInstanceMap.get(vmInstanceUuid);
                        if (vmInstance == null) {
                            vmInstance = _vmInstanceService.getByUuid(vmInstanceUuid);
                            if (vmInstance != null) {
                                uuidVmInstanceMap.put(vmInstanceUuid, vmInstance);
                            }
                        }
                    }

                    Host host = null;
                    if (vmInstance != null) {
                        String hostUuid = vmInstance.getHostUuid();
                        if (!StringUtils.isEmpty(hostUuid)) {
                            host = uuidHostMap.get(hostUuid);
                            if (host == null) {
                                host = _hostService.getByUuid(hostUuid);
                                if (host != null) {
                                    uuidHostMap.put(hostUuid, host);
                                }
                            }
                        }
                    }

                    UserSessionSummary userSessionSummary = new UserSessionSummary(userSession, user, vmInstance, host);
                    userSessionSumList.add(userSessionSummary);
                }

                return new Pair<Long, List<UserSessionSummary>>(count, userSessionSumList);
            }
        }, callback);
    }
}
