/*
Copyright 2014 Nimdesk, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.nimdesk.ui.server.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.nimdesk.event.AsyncCallback;
import com.nimdesk.event.ConnectHostEvent;
import com.nimdesk.event.FindImportableVmsEvent;
import com.nimdesk.event.FindStandaloneHostEvent;
import com.nimdesk.event.GetHostAssetsEvent;
import com.nimdesk.hypervisor.HypervisorContext;
import com.nimdesk.model.Host;
import com.nimdesk.service.EventBusService;
import com.nimdesk.ui.server.HypervisorService;

@Service(HypervisorService.BEAN_NAME)
public class HypervisorServiceImpl implements HypervisorService, InitializingBean {
    private static final Logger LOGGER = Logger.getLogger(HypervisorServiceImpl.class);

    @Autowired
    private EventBusService _eventBusService;

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(_eventBusService, "An EventBusService must be set");
    }

    @Override
    public void verifyConnection(String hostname, int port, String username, String password, AsyncCallback callback) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("verifyConnection()");
        }

        ConnectHostEvent event = new ConnectHostEvent(hostname, port, username, password, callback);
        _eventBusService.post(event);
    }

    @Override
    public void findStandaloneHost(HypervisorContext ctx, AsyncCallback callback) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("findStandaloneHost()");
        }

        FindStandaloneHostEvent event = new FindStandaloneHostEvent(ctx, callback);
        _eventBusService.post(event);
    }

    @Override
    public void getHostAssets(HypervisorContext ctx, String hostRefId, AsyncCallback callback) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("findDatastores()");
        }

        GetHostAssetsEvent event = new GetHostAssetsEvent(ctx, hostRefId, callback);
        _eventBusService.post(event);
    }

    @Override
    public void findImportableVms(Host host, AsyncCallback callback) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("findImportableVms()");
        }

        FindImportableVmsEvent event = new FindImportableVmsEvent(host, callback);
        _eventBusService.post(event);
    }
}

